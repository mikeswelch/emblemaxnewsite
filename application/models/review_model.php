<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class review_model extends CI_Model {

	private $table= 'customer_review';
        
	function __construct() {
		parent::__construct();
	}

	function save($Data){
		$this->db->insert($this->table,$Data);  
		return $this->db->insert_id();
	}

	
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	
	function getData(){
		$sql = "SELECT * FROM customer_review WHERE eStatus = 'Active' ORDER BY iOrderNo DESC";
		return $this->db->query($sql);
	}
	function getCategoryTemp($limit){
		$sql = "Select * from customer_review WHERE eStatus = 'Active' ORDER BY iOrderNo DESC $limit";
		$query = $this->db->query($sql);
		return $query;           
        }
	function getReview($rootCategoryId,$limit){
	    $sql = "SELECT * FROM customer_review WHERE eStatus = 'Active' ORDER BY iOrderNo DESC $limit";
	    return $this->db->query($sql);
        }


}