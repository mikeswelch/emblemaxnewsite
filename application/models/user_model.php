<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class  user_model extends CI_Model {

	private $table= 'user';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Parth Devmorari
	 Purpose: Fatch all data from database
	 Created Date: 12-01-2012
	*/
	function list_all($var_limit,$ssql,$field,$sort) {
		
		if($field== '' || $sort == '')
		{			
			$sql = "SELECT * FROM user  WHERE 1 $ssql order by iUserId DESC $var_limit";			 
			$query = $this->db->query($sql);			
		}
		else
		{
		   $sql="select * from user where 1=1 $ssql order by $field $sort $var_limit";
	           $query=$this->db->query($sql);		
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	/*
	 Function Name: save
	 Developer Name: Parth Devmorari
	 Purpose: Save data in the database
	 Created Date: 12-01-2012
	*/
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	/*
	 Function Name: get_one_by_id
	 Developer Name: Parth Devmorari
	 Purpose: Fetch one data for edit
	 Created Date: 12-01-2012
	*/
	function get_one_by_id($id) {
		$this->db->where('iAdminId', $id);
		return $this->db->get($this->table);
	}
	/*
	 Function Name: update
	 Developer Name: Parth Devmorari
	 Purpose: Updated data save in the data base
	 Created Date: 12-01-2012
	*/
	function update($id, $data){
		$this->db->where('iAdminId', $id);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: Delete data in the database 
	 Created Date: 12-01-2012
	*/
	function delete($id) {
		$where = 'iAdminId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/*
	 Function Name: update_status
	 Developer Name: Parth Devmorari
	 Purpose: Updated status and save save in the data base
	 Created Date: 12-01-2012
	*/
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iAdminId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	/*
	 Function Name: multiple_update_status
	 Developer Name: Parth Devmorari
	 Purpose: Updated Multiple status at a time and save save in the data base
	 Created Date: 12-01-2012
	*/
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iAdminId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: delete_data
	 Developer Name: Parth Devmorari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-01-2012
	*/
	function delete_data($iAdminId){
	    $sql = "DELETE FROM  $this->table WHERE iAdminId IN ('".$iAdminId."')";
		    $query = $this->db->query($sql);
		    return $query;
	}
	/*
	 Function Name: count_all
	 Developer Name: Parth Devmorari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-01-2012
	*/
	function count_all($ssql){
	    $sql = "select count(iAdminId) AS tot from $this->table WHERE 1=1 $ssql";
	    
	    $query = $this->db->query($sql);
	    return $query;
	}
	/*
	 Function Name: displayalphasearch
	 Developer Name: Parth Devmorari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-01-2012
	*/
	function displayalphasearch(){
	    $sql_alp = "select vFirstName from $this->table where 1=1";
	    $query = $this->db->query($sql_alp);
	    return $query;
	}

	function logoutentry($iAdminId,$date,$iLoginHistoryId)
	{
		$sql = "update login_histories set dLogoutDate ='".$date."' WHERE iID='".$iAdminId."' AND iLoginHistoryId ='$iLoginHistoryId.'";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query;
	}
	
	function user_login_check($vUserName,$vPassword)
	{
		$sql = "select * from user WHERE vUserName = '".$vUserName."' and vPassword = '".$vPassword."' ";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: admin_edit_data
	 Developer Name: Parth Devmorari
	 Purpose: Edit administrator data 
	 Created Date: 12-01-2012
	*/
	function admin_edit_data($tLastLogin,$vFromIP,$iAdminId)
	{
		$sql_upd = "update administrators set dLastLogin='".$tLastLogin."', vFromIP='".$vFromIP."' WHERE iAdminId='".$iAdminId."'";
		$query = $this->db->query($sql_upd);
		return $query;
	}
	/*
	 Function Name: add_loginhistory
	 Developer Name: Parth Devmorari
	 Purpose: Insert login data in the database using this function
	 Created Date: 12-01-2012
	*/
	function add_loginhistory($iAdminId,$vFirstName,$vLastName,$vEmail,$vUserName,$vFromIP){
		
		$sql = "insert into login_histories (iLoginHistoryId, iId, eType, vFirstName, vLastName, vEmail, vUsername, vFromIP, dLoginDate) 
			VALUES (' ', '".$iAdminId."','Administrator','".$vFirstName."','".$vLastName."',
			'".$vEmail."','".$vUserName."','".$vFromIP."',NOW())";
		$query = $this->db->query($sql);
		return $this->db->insert_id();
	}
	
	function list_sysemail($ssql)
	{
		$sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
		$query = $this->db->query($sql);
		return $query;
	}
	function check_username($username)
	{
		$sql = "select count(iAdminId) AS tot from $this->table WHERE vUsername='".$username."'";
		$query = $this->db->query($sql);
	        return $query;
	}
	function getOldUserName($iAdminId)
	{
		$sql = "select vUserName from $this->table WHERE iAdminId='".$iAdminId."'";
		return $this->db->query($sql);
	}
	function get_email($vEmail)
	{
		$sql = "SELECT * FROM user  WHERE vEmail = '".$vEmail."'";
		$query = $this->db->query($sql);
		return $query;
	}

}