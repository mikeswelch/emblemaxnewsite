<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class registration_model extends CI_Model {

	private $table= 'user';
        
	function __construct() {
		parent::__construct();
	}
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
		
	function get_lang()
	{
		$sql = "SELECT * FROM language";
		$query = $this->db->query($sql);
		return $query;		
	}
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	function get_state_by_country($id)
	{
		$this->load->database();
		$sql = "select * from state_master WHERE iCountryId = $id ";
		return $this->db->query($sql)->result();
	}
	function list_sysemaildata($ssql)
	{
		$sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
		$query = $this->db->query($sql);
		return $query;
	}
	function save_newsletter($id,$vUsername,$vEmail,$eStatus,$dDate){
		$sql = "INSERT INTO newsletter (iUserId,vUsername,vEmail,eStatus,dAddedDate) VALUES ('".$id."','".$vUsername."','".$vEmail."','".$eStatus."','".$dDate."')";
		$query = $this->db->query($sql);
		return $this->db->insert_id();		
	}
	function saveAddress($data){
		$this->db->insert('user_address', $data);
		return $this->db->insert_id();
	}
	function getData($vUserName){
		$sql = "SELECT vUserName FROM user WHERE vUserName = '".$vUserName."'";
		return  $this->db->query($sql);
	}
	function getActivate($activeCode){
		$sql = "UPDATE user SET eStatus = 'Active' WHERE vActivationcode = '".$activeCode."'";
		return $this->db->query($sql);
	}
	function checkUserName($userName){
		$sql = "SELECT vUserName FROM user WHERE vUserName = '".$userName."'";
		return $this->db->query($sql);
	}
	function checkEmail($vEmail){
		$sql = "SELECT vUserName FROM user WHERE vEmail = '".$vEmail."'";
		return $this->db->query($sql);
	}
	function saveBillAddress($data){
		$this->db->insert('billing_address',$data);
		return $this->db->insert_id();
	}
	function saveShippAddress($data){
		$this->db->insert('shipping_address',$data);
		return $this->db->insert_id();
	}}
