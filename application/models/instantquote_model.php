<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class instantquote_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	function getProductColors($iProductId){
		//$sql = "SELECT DISTINCT c.vColor, c.* FROM color c LEFT JOIN product_color_size pcs ON c.iColorId = pcs.iColorId WHERE pcs.iProductId = '".$iProductId."' AND c.eStatus = 'Active'";
		$sql = "SELECT DISTINCT c.vColor, c.* FROM color c LEFT JOIN product_color pcs ON c.iColorId = pcs.iColorId WHERE pcs.iProductId = '".$iProductId."' AND c.eStatus = 'Active'";		
		return $this->db->query($sql);
	}
	
	function getAllSizes(){
		$sql = "SELECT * FROM size WHERE eStatus = 'Active' ORDER BY iOrder";
		return $this->db->query($sql);
	}
	function getProductDetail($iProductId){
		$sql = "SELECT * FROM product p LEFT JOIN product_translation pt ON p.iProductId = pt.iProductId WHERE p.iProductId = '".$iProductId."' AND p.eStatus = 'Active'";		
		return $this->db->query($sql);
	}
	function getAllPrintLocations(){
		$sql = "SELECT * FROM print_locations WHERE eStatus = 'Active' ORDER BY iOrder";
		return $this->db->query($sql);
	}
	function getProductColorSizeRelation($iProductId){
		//$sql = "SELECT * FROM product_color_size WHERE iProductId = '".$iProductId."' order by iSizeId";		
		$sql = "SELECT pcs.*,s.vSize,s.vTitle,s.iOrder FROM product_color_size pcs LEFT JOIN size s ON pcs.iSizeId=s.iSizeId WHERE iProductId='".$iProductId."'  Order by pcs.iColorId,s.iOrder";		
		return $this->db->query($sql);
	}
	function getProductColorArray($iProductId){
		$sql = "SELECT iColorId FROM product_color_size WHERE iProductId = '".$iProductId."'";
		return $this->db->query($sql);
	}
	function getProductSizeArray($iProductId){
		$sql = "SELECT iSizeId FROM product_color_size WHERE iProductId = '".$iProductId."'";
		return $this->db->query($sql);
	}
	function getAllNumberOfColors(){
		$sql = "SELECT * FROM number_of_colors WHERE eStatus ='Active'";
		return $this->db->query($sql);
	}
	function getProductPrintlocationNumberofColorsRelation($iProductId){
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$iProductId."'";
		return $this->db->query($sql);
	}
	function getAllProducts(){
		$sql = "SELECT * FROM product p LEFT JOIN product_translation pt ON p.iProductId = pt.iProductId WHERE p.eStatus = 'Active' ORDER BY p.iProductId";
		return $this->db->query($sql);
	}
	function getAllShippingMethods(){
		$sql = "SELECT * FROM turn_arround_time WHERE eStatus ='Active' ORDER BY iOrder";
		return $this->db->query($sql);
	}
	function getShipmentById($iShipmentId){
		$sql = "SELECT * FROM turn_arround_time WHERE iTurnAroundTimeId = '".$iShipmentId."'";
		return $this->db->query($sql);
	}
	function getCouponDetailByCode($couponCode){
		$sql = "SELECT * FROM coupon WHERE vCouponCode = '".$couponCode."'";
		return $this->db->query($sql);
	}
	function getProductById($iProductId){
		$sql = "SELECT p.iProductId,p.vImage, pt.vProductName FROM product p LEFT JOIN product_translation pt ON p.iProductId = pt.iProductId WHERE p.iProductId = '".$iProductId."'";
		return $this->db->query($sql);
        }
	function getTurnAroundTimeById($iTurnAroundTimeId){
		$sql = "SELECT vTitle,fCharge, eFeeCharge FROM turn_arround_time WHERE iTurnAroundTimeId = '".$iTurnAroundTimeId."'";
		return $this->db->query($sql);
	}
	function saveQuote($data){
		$this->db->insert('quote', $data);
		return $this->db->insert_id();
	}
	function getColorById($iColorId){
		$sql = "SELECT vColor FROM color WHERE iColorId = '".$iColorId."'";
		return $this->db->query($sql);
	}
	function getSizeById($iSizeId){
		$sql = "SELECT vSize FROM size WHERE iSizeId = '".$iSizeId."'";
		return $this->db->query($sql);
	}
	function getPrintLocationById($iPrintLocationId){
		$sql = "SELECT vPrintLocation FROM print_locations WHERE iPrintLocationId = '".$iPrintLocationId."'";
		return $this->db->query($sql);
	}
	function getProductByCategoryId($iCategoryId,$limit){
		$sql = "Select * from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId='".$iCategoryId ."' $limit";
		//echo $sql;exit;
		return $this->db->query($sql);
	}
	function getCategory($rootCategoryId,$limit){
		$sql = "Select *, (select count(*) from product_category pcs where pcs.iParentId = product_category.iCategoryId ) as numChilds, (select count(*) FROM product pro WHERE pro.iCategoryId = product_category.iCategoryId ) as numProducts from product_category_translation JOIN  product_category ON product_category_translation.iCategoryId = product_category.iCategoryId And  product_category.iParentId = '".$rootCategoryId."' AND product_category.eStatus='Active' $limit";
		//echo $sql;exit;
		return $this->db->query($sql);
        }
	function getCategoryByUrlText($vUrlText,$var_limit){
		$sql = "Select * from product_category_translation JOIN  product_category ON product_category_translation.iCategoryId = product_category.iCategoryId ".$vUrlText.' '.$var_limit;
		//echo $sql;exit;
		return $this->db->query($sql);
	}
	function getProductData(){
		$sql = "SELECT p.*,pt.* FROM product p LEFT JOIN product_translation pt ON (p.iProductId = pt.iProductId) WHERE p.eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function productByCategoryId($id){
		$sql = "SELECT p.*,pt.* FROM product p LEFT JOIN product_translation pt ON (p.iProductId = pt.iProductId) WHERE p.iCategoryId = '".$id."'";
		return $this->db->query($sql);
	}
	function getAllQtyRanges(){
		$sql = "SELECT * FROM quantity_range WHERE eStatus = 'Active' ORDER BY iStartRange DESC";
		return $this->db->query($sql);
	}
	function getNumberOfColorsById($iNumberColorId){
		$sql = "SELECT * FROM number_of_colors WHERE iNumberColorsId = '".$iNumberColorId."'";
		return $this->db->query($sql);
	}
	function getProductName($iProductId){		
		$sql = "SELECT * FROM `product_translation` WHERE `iProductId`='".$iProductId."'";
		return $this->db->query($sql);
	}
	function getColorName($iColorId){		
		$sql = "SELECT * FROM `color` WHERE `iColorId`='".$iColorId."'";
		//echo $sql ;exit;
		return $this->db->query($sql);
	}
	function list_sysemaildata($ssql){
		$sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
		$query = $this->db->query($sql);
		return $query;
	}
	function getPrintLocations($finalPrintLocationIds){
		$sql = "SELECT * FROM print_locations WHERE eStatus = 'Active' AND iPrintLocationId IN ($finalPrintLocationIds) ORDER BY iOrder";		
		return $this->db->query($sql);
	}
	function getQuantityRange(){
		$sql = "SELECT * FROM quantity_range WHERE eStatus='Active'";		
		return $this->db->query($sql);
	}
	function getEmbrodary($iProductId,$iQuantityRangeId){
		$sql = "SELECT * FROM `product_printlocation_numbercolors` WHERE `iProductId`='".$iProductId."' AND `iQuantityRangeId`='".$iQuantityRangeId."' AND `iNumberColorsId`='0'";		
		return $this->db->query($sql);
	}
	function getMinimumQunaityRange($iProductId,$iQuantityRangeId){
		$sql = "SELECT min(`iStartRange`)AS MinimumRange FROM quantity_range WHERE `eStatus`='Active'";		
		return $this->db->query($sql);
	}
	function getNumberOfColors($finalColors){
		$sql = "SELECT * FROM number_of_colors WHERE eStatus ='Active' AND iNumberColorsId IN($finalColors)";
		return $this->db->query($sql);
	}
	function geQuantityRange($totalQty){
		$sql = "SELECT * FROM `quantity_range` WHERE `iStartRange`>='".$totalQty."' LIMIT 0,3";		
		return $this->db->query($sql);
	}
	function getPriceDetail($range,$printLocation,$noOfColor,$iProductId){
		$sql = "SELECT * FROM `product_printlocation_numbercolors` WHERE `iProductId`='".$iProductId."' AND `iPrintLocationId`='".$printLocation."' AND `iQuantityRangeId`='".$range."' AND `iNumberColorsId`='".$noOfColor."'";		
		return $this->db->query($sql);
	}
	function getPrice($iProductId,$colorId,$sizeId){
		$sql = "SELECT * FROM `product_color_size` WHERE `iProductId`='".$iProductId."' AND `iColorId`='".$colorId."' AND iSizeId='".$sizeId."'";		
		return $this->db->query($sql);
	}
	function productColorImages($iProductId,$allProductColors){
		$sql = "SELECT * FROM product_color WHERE iProductId='".$iProductId."' AND iColorId IN ($allProductColors)";		
		return $this->db->query($sql);
	}	
	function productColors($iProductId){
		$sql = "SELECT * FROM product_color WHERE iProductId='".$iProductId."'";		
		return $this->db->query($sql);
	}
	
	function printLocationName($printIds){
		$sql = "SELECT * FROM print_locations WHERE eStatus = 'Active' AND iPrintLocationId IN ($printIds) ORDER BY iOrder";		
		return $this->db->query($sql);
	}
	
	
	
	
	
	
}