<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class banner_model extends CI_Model {
    
    private $table= 'banner';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
	
		if($field=='' || $order==''){		     	
			
			$sql="SELECT b.*, bg.vTitle FROM banner_group bg INNER JOIN banner b ON b.iBannerGroupId = bg.iBannerGroupId WHERE 1 $ssql order by iBannerId DESC $var_limit";
			//echo $sql;exit;
			$query = $this->db->query($sql);
		}
		else{
			$sql="SELECT bg.*, bgi.* FROM banner_group AS bg INNER JOIN $this->table AS bgi on bg.`iBannerGroupId` = bgi.`iBannerGroupId` $ssql ORDER BY $field $order $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function save($Data){
		$this->db->insert($this->table,$Data);
		return $this->db->insert_id();
	}

	function get_one_by_id($id) { 
		$this->db->where('iBannerId', $id);		
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iBannerId', $id);
		$query = $this->db->update($this->table,$data);
		return $query; 
	}

        function delete($id) {
		$where = 'iBannerId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}

	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iBannerId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($iBannerId, $status) {
		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iBannerId IN ('".$iBannerId."')";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql)
	{
		$sql = "SELECT count(b.iBannerId) AS tot FROM banner AS b LEFT JOIN banner_group AS bg ON (b.iBannerGroupId = bg.iBannerGroupId) $ssql";
		//$sql = "SELECT count(bgi.iBannerGroupId) AS tot FROM banner AS bgi LEFT JOIN banner_group AS bg ON (bgi.iBannerGroupId = bg.iBannerGroupId) $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		//$sql_alp = "SELECT b.*, bg.* FROM banner_group AS bg INNER JOIN $this->table AS b on b.`iBannerGroupId` = bg.`iBannerGroupId` where 1=1";
		$sql_alp ="SELECT b.*,bg.* FROM banner_group AS bg LEFT JOIN banner AS b on b.iBannerGroupId= bg.iBannerGroupId WHERE 1=1";		
		$query = $this->db->query($sql_alp);
		return $query;
	}
	function delete_data($iBannerId){
		$sql = "DELETE FROM  $this->table WHERE iBannerId IN ('".$iBannerId."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function delete_image($iBannerId)
	{
		$data['vImage'] = '';
		$sql = "UPDATE banner SET vImage = '' WHERE iBannerId = '".$iBannerId."'";
		return $this->db->query($sql);
	}
	function getBannerGroup(){
		$sql = "SELECT * FROM banner_group WHERE eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getSize($id){
		$sql = "SELECT iHeight,iWidth FROM banner_group WHERE iBannerGroupId = '".$id."'";
		return $this->db->query($sql);
	}
	
}
