<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model {

	private $table= 'user';
    
	
	function __construct() {
		parent::__construct();
	}
	function getReview(){
		$sql = "SELECT * FROM customer_review WHERE eStatus = 'Active' ORDER BY RAND() LIMIT 0,1";
		return $this->db->query($sql);
	}
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	function getPromotional(){
		$sql="SELECT p.*,pt.vProductName FROM product p LEFT JOIN product_translation pt ON (p.iProductId = pt.iProductId) WHERE p.ePromotion = 'Yes' AND p.eStatus = 'Active' ORDER BY RAND() LIMIT 0,10";
		return $this->db->query($sql);
	}
	function getStaticQuote(){
		$sql = "SELECT * FROM static_pages WHERE vFile = 'home_getQuickQuote' AND eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getStaticCustomDesign(){
		$sql = "SELECT * FROM static_pages WHERE vFile = 'home_customerDesignTshirt' AND eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getStaticBrowseApparel(){
		$sql = "SELECT * FROM static_pages WHERE vFile = 'home_browseApparel' AND eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getStaticFastTurnArround(){
		$sql = "SELECT * FROM static_pages WHERE vFile = 'home_fastTurnaround' AND eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getHomeBlocks(){
		$sql = "SELECT * FROM homepage_blocks WHERE eStatus = 'Active' order by iOrder ASC";
		return $this->db->query($sql);
	}

}
