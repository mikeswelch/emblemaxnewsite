<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class checkout_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	function user_login_check($vUserName,$vPassword){
		$sql = "select * from user WHERE vUserName = '".$vUserName."' and vPassword = '".$vPassword."' ";
		return $this->db->query($sql);
	}
	function getAllcountries(){
		$sql = "SELECT * FROM country_master WHERE eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function saveOrder($data){
		$this->db->insert('order', $data);
		return $this->db->insert_id();
	}
	function updateOrder($data,$iOrderId){
		$this->db->where('iOrderId', $iOrderId);
		return $this->db->update('order', $data);
	}
	function getOrderDetail($orderId){
	       $sql = "SELECT o.*,cm1.vCountry as vBillCountry, cm2.vCountry as vShipCountry, sm1.vState as vBillState, sm2.vState as vShipState FROM `order` o LEFT JOIN country_master cm1 ON o.iBillCountryId = cm1.iCountryId LEFT JOIN country_master cm2 ON o.iShipCountryId = cm2.iCountryId LEFT JOIN state_master sm1 ON o.iBillStateId = sm1.iStateId LEFT JOIN state_master sm2 ON o.iShipStateId = sm2.iStateId WHERE `iOrderId` = ".$orderId;
	       return $this->db->query($sql);
	}
	function saveOrderComment($data){
		$this->db->insert('order_comments', $data);
		return $this->db->insert_id();
	}
	function getUserData($vUserName,$vPassword){
		$sql = "SELECT * FROM user WHERE vUserName = '".$vUserName."' AND vPassword = '".$vPassword."'";
		return $this->db->query($sql);
	}
	function getUserAddress($iUserId){
		$sql = "SELECT * FROM user_address WHERE iUserId = '".$iUserId."' AND eStatus = 'Default'";
		return $this->db->query($sql);
	}
	function saveNewUser($data){
		$this->db->insert('user', $data);
		return $this->db->insert_id();
	}
	function getCountryCodeById($iCountryId){
		$sql = "SELECT vCountryCode FROM country_master WHERE iCountryId = '".$iCountryId."'";
		return $this->db->query($sql);
	}
	function getStateNameById($stateId){
		$sql = "SELECT vState FROM state_master WHERE iStateId = '".$stateId."'";
		return $this->db->query($sql);
	}
	function saveOrderDetail($data){
	  $this->db->insert('order_detail', $data);
	  return $this->db->insert_id();	  
     }
	function getPrintLocationById($iPrintLocationId){
		$sql = "SELECT vPrintLocation FROM print_locations WHERE iPrintLocationId = '".$iPrintLocationId."'";
		return $this->db->query($sql);
	}
	function getNumberOfColorsById($iNumberColorId){
		$sql = "SELECT * FROM number_of_colors WHERE iNumberColorsId = '".$iNumberColorId."'";
		return $this->db->query($sql);
	}
	function list_sysemaildata($ssql){
		$sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
		$query = $this->db->query($sql);
		return $query;
	}
	function getShippingMethods($iTurnAroundTimeId){
		$sql = "SELECT * FROM turn_arround_time WHERE iTurnAroundTimeId='".$iTurnAroundTimeId."'";
		return $this->db->query($sql);
	}
}