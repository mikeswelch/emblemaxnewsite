<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
	class homepage_blocks_model extends CI_Model {
	
	private $table= 'homepage_blocks';
        
	function __construct() {
		parent::__construct();
	}
	function list_all($var_limit,$ssql,$field,$sort) {
		if($field=='' || $sort==''){
			$sql = "SELECT * FROM homepage_blocks WHERE 1 $ssql  order by iBlockslistId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM homepage_blocks WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	function save($Data){
		$this->db->insert($this->table,$Data);
		return $this->db->insert_id();
	}
	function get_one_by_id($id) { 
		$this->db->where('iBlockslistId', $id);
		return $this->db->get($this->table);
	}
	function update($id, $data){
		$this->db->where('iBlockslistId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
     function delete($id) {
		$where = 'iBlockslistId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iBlockslistId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iBlockslistId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function count_all($ssql){
		$sql = "select count(iBlockslistId) AS tot from homepage_blocks e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
     }
	function displayalphasearch(){
		$sql_alp = "select vTitle from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	function delete_data($iBlockslistId){
		$sql = "DELETE FROM  $this->table WHERE iBlockslistId IN ('".$iBlockslistId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function delete_image($iBlockslistId){
		$data['vImage'] = '';
		$this->db->where('iBlockslistId', $iBlockslistId);
		return $this->db->update($this->table, $data);
	}
}