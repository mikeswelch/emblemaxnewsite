<?php

/*      Model Name: language_model
	Developer Name: Deepak Khamari
	Purpose: to be called when the Language of admin page loads
	Created Date: 12-11-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class changepassword_model extends CI_Model {
    
    private $table= 'freelancer_users';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: update
	 Developer Name: Deepak Khamari
	 Purpose: Updated data save in the data base
	 Created Date: 12-11-2012
	*/
	function update($id, $data){
		$this->db->where('iFreelancerUserId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
	function update_client($id, $data){
		$this->db->where('iUserId', $id);
		$query = $this->db->update('user',$data); 
		return $query; 
	}
}