<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class currency_model extends CI_Model {
    
    private $table= 'currency';
        
	function __construct() {
		parent::__construct();
	}
	/*function list_all($var_limit,$ssql) {
		$sql = "SELECT * FROM currency WHERE 1 $ssql order by icurrencyId DESC $var_limit";
		$query = $this->db->query($sql);
		return $query;
		
	}*/
	
	 
	 function list_all($var_limit,$ssql,$field,$sort) {
		
		if($field== '' || $sort == '')
		{
		    $sql = "SELECT i.*, (SELECT count(f.iFreelancerUserId) FROM freelancer_users f WHERE i.iCurrencyId = f.iCurrencyId) AS count FROM currency i WHERE 1 $ssql order by i.iCurrencyId DESC $var_limit";
		    $query = $this->db->query($sql);
		}
		else
		{
		   $sql="SELECT i.*,(SELECT count(f.iFreelancerUserId) FROM freelancer_users f WHERE i.iCurrencyId = f.iCurrencyId) AS count FROM currency i WHERE 1 $ssql order by $field $sort $var_limit";
	           $query=$this->db->query($sql);
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	} 
	 
	function count_all($ssql){
		$sql = "select count(iCurrencyId) AS tot from $this->table i WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	function displayalphasearch(){
		$sql_alp = "select vCurrency from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	function save($Data){
		
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	
	function get_one_by_id($id) {
		$this->db->where('iCurrencyId', $id);
		return $this->db->get($this->table);
	}
	function update($id, $data){
		$this->db->where('iCurrencyId', $id);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iCurrencyId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iCurrencyId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	function delete_data($iCurrencyId){
        $sql = "DELETE FROM  $this->table WHERE iCurrencyId IN ('".$iCurrencyId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function delete($id) {
		$where = 'iCurrencyId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	function count($iCurrencyId){
		$sql = "select count(iCurrencyId) AS tot from freelancer_users  WHERE 1=1 And iCurrencyId='".$iCurrencyId."'";
		$query = $this->db->query($sql);
		return $query;
        }
	/* duplicate check*/
	function check_currency($vCurrency)
	{
		$sql = "select count(iCurrencyId) AS tot from $this->table WHERE vCurrency='".$vCurrency."'";
		$query = $this->db->query($sql);
	        return $query;
	}
	/* duplicate check*/
	function getOldcurrency($iCurrencyId)
	{
		$sql = "select vCurrency from $this->table WHERE iCurrencyId='".$iCurrencyId."'";
		return $this->db->query($sql);
	}
}
