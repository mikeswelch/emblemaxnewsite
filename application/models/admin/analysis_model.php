<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class analysis_model extends CI_Model {

	private $table= 'transaction';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
		if($field=='' || $order == '')
		{
		$sql = "SELECT * FROM transaction  WHERE 1=1 $ssql order by iTransactionId DESC $var_limit";
		$query = $this->db->query($sql);
		}
		else
		{
		$sql="select * from transaction where 1=1 $ssql order by $field $sort $var_limit";		
		$query = $this->db->query($sql);	
			
		}
		return $query;
	}
	function displayalphasearch(){
		$sql_alp = "select iTransactionId from $this->table where 1=1";
		return $this->db->query($sql_alp);
	}
	function count_all($ssql){
	    $sql = "select count(iTransactionId) AS tot from $this->table AS u WHERE 1=1 $ssql";
	    return $this->db->query($sql);
	}
		
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	function get_one_by_id($id) {
		$this->db->where('iTransactionId', $id);
		return $this->db->get($this->table);
	}
	
	function update($id, $data){
		$this->db->where('iTransactionId', $id);
		return $this->db->update($this->table, $data);
	}
	
        function delete($id) {
		$where = 'iTransactionId = '.$id;
		return $this -> db -> delete($this->table, $where);
	}
	
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iTransactionId IN ('".$id."')";
		return $this->db->query($sql);
	}
    
	function delete_data($commonid){
		$sql = "DELETE FROM  $this->table WHERE iTransactionId IN ('".$commonid."')";
		return $this->db->query($sql);
	}
	/*
	 function Name: show_invoicedata
	 Developer Name: Deepak Khamari
	 Purpose: Retrive data to show on invoice 
	 Created Date: 12-11-2012//t.*,o.iTotalFreelancer,o.vFreelancerIds,o.dOrderDate,o.eOrderStatus,o.fOrderAmount
	 */
	function show_invoicedata($iTransactionId) {
		
		$sql = "SELECT * FROM transaction AS t LEFT JOIN transaction_detail AS td ON t.iTransactionId=td.iTransactionId LEFT JOIN business_users AS bu ON t.iBusinessUserId=bu.iBusinessUserId  WHERE  t.iTransactionId=".$iTransactionId;
		//echo $sql; exit; 
		$query = $this->db->query($sql);
		return $query;
	}
}
?>