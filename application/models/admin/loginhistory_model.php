<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class loginhistory_model extends CI_Model {
    
    private $table= 'login_histories';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Parth Devmorari
	 Purpose: Fatch all data from database
	 Created Date: 12-01-2012
	*/
	function list_all($var_limit,$ssqll,$field,$order) {
		if($field=='' || $order =='')
		{
		$sql = "SELECT * FROM login_histories  WHERE 1 $ssqll order by iLoginHistoryId DESC $var_limit";
		//echo $sql;exit; 
		$query = $this->db->query($sql);
		}
		else
		{
		 $sql = "SELECT * FROM login_histories  WHERE 1 $ssqll order by $field $order $var_limit";
		$query = $this->db->query($sql);
		  	
		}
		return $query;
        
		$this->db->order_by('iLoginHistoryId','desc');
		return $this->db->get($this->table);
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: Search data in the database as key wise
	 Created Date: 12-01-2012
	*/
	function search_data($key,$text)
	{
	    $this->db->like($key, $text);
	    $this->db->order_by($key);
	    return $this->db->get($this->table)->result();
	}
	/*
	 Function Name: delete_data
	 Developer Name: Parth Devmorari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-01-2012
	*/
	function delete_data($iLoginHistoryId){
        $sql = "DELETE FROM  $this->table WHERE iLoginHistoryId IN ('".$iLoginHistoryId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: displayalphasearch
	 Developer Name: Parth Devmorari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-01-2012
	*/
	function displayalphasearch(){
		$sql_alp = "select vFirstName from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	/*
	 Function Name: count_all
	 Developer Name: Parth Devmorari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-01-2012
	*/
	function count_all($ssqll){
		$sql = "select count(iLoginHistoryId) AS tot from $this->table WHERE 1=1 $ssqll";
		$query = $this->db->query($sql);
		return $query;
        }
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: Delete data in the database 
	 Created Date: 12-01-2012
	*/
        function delete($id) {
		$where = 'iLoginHistoryId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
        
    
}
