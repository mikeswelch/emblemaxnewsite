<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class newsletter_format_model extends CI_Model {
    
    private $table= 'newsletter_format';
        
	function __construct() {
		parent::__construct();
	}
	function list_all($var_limit,$ssql,$field,$sort) {
		if($field=='' || $sort==''){
		     	$sql = "SELECT * FROM $this->table WHERE 1 $ssql  order by iNformatId Desc $var_limit";
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM $this->table WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
		
	}
	function count_all($ssql){
		$sql = "select count(iNformatId) AS tot from $this->table  WHERE 1=1 $ssql";
		return  $this->db->query($sql);
        }
	function displayalphasearch(){
		$sql_alp = "select vTitle from $this->table where 1=1";
		return $this->db->query($sql_alp);
	}
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	function get_one_by_id($id) {
		$this->db->where('iNformatId', $id);
		return $this->db->get($this->table);
	}	
	function update($id, $data){
		$this->db->where('iNformatId', $id);
		return $this->db->update($this->table, $data);
	}
	function delete($id) {
		$where = 'iNformatId = '.$id;
		return $this -> db -> delete($this->table, $where);
	}
	function delete_data($iNformatId){
		$sql = "DELETE FROM  $this->table WHERE iNformatId IN ('".$iNformatId."')";
		return $this->db->query($sql);
	}
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iNformatId IN ('".$id."')";
		return $this->db->query($sql);
	}
	function chapter_user_list($chapter_id){
		$sql = "SELECT vFirstName,vLastName,vEmail FROM member order by iMemberId ASC";
		//echo $sql;exit;
		return $this->db->query($sql);
	}
	function upcoming_eventlist($chapter_id){
		$sql = "SELECT * FROM event WHERE chapter_id = '".$chapter_id."' AND devent_date > CURDATE() order by event_id";
		return $this->db->query($sql);
	}
	function alleventlist($chapter_id){
		$sql = "SELECT * FROM event WHERE chapter_id = '".$chapter_id."' order by event_id";
		return $this->db->query($sql);
	}
}
