<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class order_model extends CI_Model {

	private $table= '`order`';
        
	function __construct() {
		parent::__construct();
	}
	
	/*function filter_between($start,$end){
		
		
		$sql = "SELECT * FROM $this->table WHERE dPaymentDate between '$start' AND '$end' order by iOrderId DESC $var_limit";
		
		return $this->db->query($sql);		
	}*/
	
	function list_all($var_limit,$ssql,$field,$sort) {
		if($field=='' || $sort == ''){			
			//$sql = "SELECT sm.*,dp.*,od.*,o.eStatus AS paypalStatus,o.*,pt.* FROM order_detail od LEFT JOIN `order` o ON (o.iOrderId = od.iOrderId) LEFT JOIN product_translation pt ON (od.iProductId = pt.iProductId) LEFT JOIN designed_products dp ON (dp.iDesignedProductId = od.iDesignedProductId) LEFT JOIN shipping_method sm ON (o.iShippingMethodId = sm.iShippingMethodId) ORDER BY o.dAddedDate DESC $var_limit";
			$sql = "SELECT o.*,o.eStatus AS paypalStatus,o.fGrandTotal AS finalBillAmount,qt.* FROM `order` o LEFT JOIN quote qt ON (o.iQuoteId = qt.iQuoteId) WHERE 1  $ssql ORDER BY o.iOrderId DESC $var_limit";
			//echo $sql;exit;
			$query = $this->db->query($sql);
		}
		else
		{
			$sql = "SELECT o.*,o.eStatus AS paypalStatus,o.fGrandTotal AS finalBillAmount,qt.* FROM `order` o LEFT JOIN quote qt ON (o.iQuoteId = qt.iQuoteId) WHERE 1  $ssql ORDER BY o.iOrderId DESC $var_limit";
			$query = $this->db->query($sql);
		}		
		return $query;
	}
	function totTrancsation(){
		$sql = "SELECT fGrandTotal FROM `order`";
		return $this->db->query($sql);
		
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	function displayalphasearch(){
		$sql_alp = "select iOrderId from $this->table where 1=1";		
		return $this->db->query($sql_alp);
	}
	function count_all($ssql){
		$sql = "SELECT o.*,u.vFirstName,u.vLastName,u.vEmail,u.vPhone,u.vPhoto,u.vZipCode,u.dAddedDate,u.eGender,u.tAddress,td.* FROM $this->table o LEFT JOIN user u ON o.iUserId = u.iUserId LEFT JOIN order_detail AS td ON td.iOrderId = o.iOrderId WHERE 1=1 $ssql";
		return $this->db->query($sql);
	}
		
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	function get_one_by_id($id) {
		$this->db->where('iOrderId', $id);
		return $this->db->get($this->table);
	}
	
	function update($id, $data){
		$this->db->where('iOrderId', $id);
		return $this->db->update($this->table, $data);
	}
	
        function delete($id) {
		$where = 'iOrderId = '.$id;
		return $this -> db -> delete($this->table, $where);
	}
	
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iOrderId IN ('".$id."')";
		return $this->db->query($sql);
	}
    
	function delete_data($commonid){
		$sql = "DELETE FROM  $this->table WHERE iOrderId IN ('".$commonid."')";
		return $this->db->query($sql);
	}

	function show_orderdata($iOrderId) {		
		//$sql = "SELECT o.*,o.eStatus AS paypalStatus,o.fGrandTotal AS finalBillAmount,qt.* FROM `order` o LEFT JOIN quote qt ON (o.iQuoteId = qt.iQuoteId) WHERE o.iOrderId= '".$iOrderId."'";
		$sql = "SELECT o.*,o.eStatus AS paypalStatus,o.fGrandTotal AS finalBillAmount,qt.*,p.vImage FROM `order` o LEFT JOIN quote qt ON (o.iQuoteId = qt.iQuoteId) LEFT JOIN product p ON p.iProductId=qt.iProductId WHERE o.iOrderId='".$iOrderId."'";		
		$query = $this->db->query($sql);
		return $query;
	}

	function transactionAmount($var_limit){		
		$sql = "SELECT o.*,o.eStatus AS paypalStatus,o.fGrandTotal AS finalBillAmount,qt.* FROM `order` o LEFT JOIN quote qt ON (o.iQuoteId = qt.iQuoteId) ORDER BY o.iOrderId DESC $var_limit";
		//echo $sql ;exit;
		$query = $this->db->query($sql);
		return $query;
	}
	function exportData($ssql)
	{
	  $sql = "SELECT *,u.vFirstName AS FirstName,u.vLastName AS LastName FROM $this->table as t LEFT JOIN user AS u ON t.iUserId=u.iUserId WHERE 1=1 $ssql";
	  return $this->db->query($sql);
	  
	}
	function show_country($iBillCountryId){
		$sql = "SELECT vCountry FROM country_master WHERE iCountryId='".$iBillCountryId."'";
		return $this->db->query($sql);
	}
	function show_state($iBillStateId){
		$sql = "SELECT vState FROM state_master WHERE iStateId='".$iBillStateId."'";
		return $this->db->query($sql);
	}
	function shipping_country($iShippCountryId){
		$sql = "SELECT vCountry FROM country_master WHERE iCountryId='".$iShippCountryId."'";
		return $this->db->query($sql);
	}
	function shipping_state($iShippStateId){
		$sql = "SELECT vState FROM state_master WHERE iStateId='".$iShippStateId."'";
		return $this->db->query($sql);
	}
	function all_size()
	{
		$sql = "SELECT * FROM size";
		$query =  $this->db->query($sql);
		return $query;
	}
	function saveComments($Data){
		$this->db->insert('order_comments',$Data);
		return $this->db->insert_id();
	}
	function getComments($id){
		$sql = "SELECT oc.*,o.* FROM `order` o LEFT JOIN order_comments oc ON (o.iOrderId = oc.iOrderid) WHERE o.iOrderId = '".$id."' ORDER BY oc.idOrderComments DESC";
		$query = $this->db->query($sql);
		return $query;
		
	}
	function getAllComments($id){
		$sql = "SELECT * FROM  order_comments WHERE iOrderId = '".$id."' ORDER BY idOrderComments DESC";
		$query = $this->db->query($sql);
		return $query;
		
	}
	function getOrderDetail($orderId){
		$sql = "SELECT o.*,o.eStatus AS paypalStatus,o.fGrandTotal AS finalBillAmount,qt.*,p.vImage FROM `order` o LEFT JOIN quote qt ON (o.iQuoteId = qt.iQuoteId) LEFT JOIN product p ON p.iProductId=qt.iProductId WHERE o.iOrderId='".$orderId."' ";
		return $this->db->query($sql);
	}
	function getOrderedItems($orderId){
		$sql = "SELECT od.*,pt.vProductName FROM order_detail od LEFT JOIN product_translation pt ON od.iProductId = pt.iProductId WHERE od.iOrderId= '".$orderId."' ";
		return $this->db->query($sql);
	}
	function getShippingMethodDetail($iShipmentID){
		$sql = "SELECT * FROM shipping_method WHERE iShippingMethodId = '".$iShipmentID."'";
		return $this->db->query($sql);
	}	
	function list_sysemaildata($ssql){
	  $sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
	  return $this->db->query($sql);
	}
	function getProductName($iProductId){
	  $sql = "SELECT * FROM product_translation WHERE iProductId='".$iProductId."'";
	  return $this->db->query($sql);
	}
	function getColorById($iColorId){
		$sql = "SELECT vColor FROM color WHERE iColorId = '".$iColorId."'";
		return $this->db->query($sql);
	}
	function getSizeById($iSizeId){
		$sql = "SELECT vSize FROM size WHERE iSizeId = '".$iSizeId."'";
		return $this->db->query($sql);
	}
	function getPrintLocationById($iPrintLocationId){
		$sql = "SELECT vPrintLocation FROM print_locations WHERE iPrintLocationId = '".$iPrintLocationId."'";
		return $this->db->query($sql);
	}
	function getNumberOfColorsById($iNumberColorId){
		$sql = "SELECT * FROM number_of_colors WHERE iNumberColorsId = '".$iNumberColorId."'";
		return $this->db->query($sql);
	}
	function getShippingMethods($iTurnAroundTimeId){
		$sql = "SELECT * FROM turn_arround_time WHERE iTurnAroundTimeId='".$iTurnAroundTimeId."'";
		return $this->db->query($sql);
	}
	function generalData($vName){
        $configData = $this->db->query("SELECT * FROM configurations where vName='".$vName."'")->result_array();
        return $configData[0]['vValue'];
    }
}
?>
