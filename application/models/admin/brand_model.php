<?php

/*      Model Name: Faq
	Developer Name: Parth Devmorari
	Purpose: to be called when the Faq of admin page loads
	Created Date: 12-01-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class brand_model extends CI_Model {
    
    private $table= 'brand';
        
	function __construct() {
		parent::__construct();
	}

	function list_all($var_limit,$ssql,$field,$sort) {
	
		if($field=='' || $sort==''){
		     	
			$sql = "SELECT * FROM brand WHERE 1 $ssql  order by iOrderNo Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM brand WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}

	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}
	function limit_fetch(){
		$sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
		return $this->db->query($sql);
	}

	function get_one_by_id($id) { 
		$this->db->where('iBrandId', $id);
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iBrandId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

        function delete($id) {
		$where = 'iBrandId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}

	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iBrandId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iBrandId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql){
		$sql = "select count(iBrandId) AS tot from brand e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		$sql_alp = "select vTitle from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}

	function delete_data($fbrics_id){
		$sql = "DELETE FROM  $this->table WHERE iBrandId IN ('".$fbrics_id."')";
		$query = $this->db->query($sql);
		return $query;
	}
}