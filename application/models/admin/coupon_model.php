<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class coupon_model extends CI_Model {
    
    private $table= 'coupon';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
	
		if($field=='' || $order==''){		     	
			$sql = "SELECT * FROM coupon WHERE 1 $ssql  order by iCouponId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM coupon WHERE 1 $ssql order by vCouponCode $order $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}

	function get_one_by_id($id) { 
		$this->db->where('iCouponId', $id);		
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iCouponId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

        function delete($id) {
		$where = 'iCouponId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iCouponId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($id, $status) {
		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iCouponId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql){
		$sql = "select count(iCouponId) AS tot from $this->table e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		$sql_alp = "select vCouponCode from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}

	function delete_data($iCouponId){
		$sql = "DELETE FROM  $this->table WHERE iSizeId IN ('".$iCouponId."')";
		$query = $this->db->query($sql);
		return $query;
	}

	
}