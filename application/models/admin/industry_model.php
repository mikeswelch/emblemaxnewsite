<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class industry_model extends CI_Model {
    
    private $table= 'industry';
        
	function __construct() {
		parent::__construct();
	}
	/*function list_all($var_limit,$ssql) {
		$sql = "SELECT * FROM industry WHERE 1 $ssql order by iIndustryId DESC $var_limit";
		$query = $this->db->query($sql);
		return $query;
		
	}*/
	
	 
	 function list_all($var_limit,$ssql,$field,$sort) {
		
		if($field== '' || $sort == '')
		{
		    $sql = "SELECT i.*, (SELECT count(f.iFreelancerUserId) FROM freelancer_users f WHERE i.iIndustryId = f.iIndustryId) AS count FROM industry i WHERE 1 $ssql order by i.iIndustryId DESC $var_limit";
		  
		    $query = $this->db->query($sql);
		}
		else
		{
		   $sql="SELECT i.*,(SELECT count(f.iFreelancerUserId) FROM freelancer_users f WHERE i.iIndustryId = f.iIndustryId) AS count FROM industry i WHERE 1 $ssql order by $field $sort $var_limit";
	           $query=$this->db->query($sql);		
		}
		return $query;
	}
	 
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function count_all($ssql){
		$sql = "select count(iIndustryId) AS tot from $this->table i WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	function displayalphasearch(){
		$sql_alp = "select vIndustryName from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	function save($Data){
		
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	
	function get_one_by_id($id) {
		$this->db->where('iIndustryId', $id);
		return $this->db->get($this->table);
	}
	function update($id, $data){
		$this->db->where('iIndustryId', $id);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iIndustryId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iIndustryId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	function delete_data($iIndustryId){
        $sql = "DELETE FROM  $this->table WHERE iIndustryId IN ('".$iIndustryId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function delete($id) {
		$where = 'iIndustryId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	function count($iIndustryId){
		$sql = "select count(iIndustryId) AS tot from freelancer_users  WHERE 1=1 And iIndustryId='".$iIndustryId."'";
		$query = $this->db->query($sql);
		return $query;
        }
	/* duplicate check*/
	function check_industry($vIndustryName)
	{
		$sql = "select count(iIndustryId) AS tot from $this->table WHERE vIndustryName='".$vIndustryName."'";
		$query = $this->db->query($sql);
	        return $query;
	}
	/* duplicate check*/
	function getOldIndustry($iIndustryId)
	{
		$sql = "select vIndustryName from $this->table WHERE iIndustryId='".$iIndustryId."'";
		return $this->db->query($sql);
	}
}
