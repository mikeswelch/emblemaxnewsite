<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class homeslider_model extends CI_Model {
    
    private $table= 'home_slider';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
	
		if($field=='' || $order==''){		     	
			$sql = "SELECT * FROM home_slider WHERE 1 $ssql  order by iHomeSliderId Desc $var_limit";
			 
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM home_slider WHERE 1 $ssql order by iHomeSliderId $order $var_limit";
			//echo $sql;exit;
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}

	function get_one_by_id($id) { 
		$this->db->where('iHomeSliderId', $id);		
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iHomeSliderId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

        function delete($id) {
		$where = 'iHomeSliderId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}

	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iHomeSliderId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($id, $status) {
		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iHomeSliderId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql){
		$sql = "select count(iHomeSliderId) AS tot from $this->table e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		$sql_alp = "select vTitle from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}

	function delete_data($iHomeSliderId){
		$sql = "DELETE FROM  $this->table WHERE iHomeSliderId IN ('".$iHomeSliderId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function list_staticpages(){
		$sql = "SELECT * FROM static_pages order by vpagename ASC ";
		$query = $this->db->query($sql);
		return $query;
	}
	function delete_image($iHomeSliderId)
	{
		$data['vImage'] = '';
		$this->db->where('iHomeSliderId', $iHomeSliderId);
		return $this->db->update($this->table, $data);
	}
	
}