<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class staticpages_model extends CI_Model {
    
    private $table= 'static_pages';
        
	function __construct() {
		parent::__construct();
	}
	/*function list_all($var_limit,$ssql) {
		$sql = "SELECT * FROM static_pages WHERE 1=1 $ssql order by iSPageId DESC $var_limit";
		$query = $this->db->query($sql);
		return $query;
		
	}*/
	
           function list_all($var_limit,$ssql,$field,$sort) {
		
		if($field== '' || $sort == '')
		{
			
		$sql = "SELECT * FROM static_pages  WHERE 1 $ssql order by iSPageId DESC $var_limit";
		$query = $this->db->query($sql);
		}
		else
		{
		   $sql="select * from static_pages where 1=1 $ssql order by $field $sort $var_limit";
		  
	           $query=$this->db->query($sql);		
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
        
	function get_one_by_id($id) {
		$this->db->where('iSPageId', $id);
		return $this->db->get($this->table);
	}
	function update($id, $data){
                $this->db->where('iSPageId', $id);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	function count_all($ssql){
	    $sql = "select count(iSPageId) AS tot from $this->table WHERE 1=1 $ssql";
	    $query = $this->db->query($sql);
	    return $query;
	}
	
	function displayalphasearch(){
	    $sql_alp = "select vFile from $this->table where 1=1";
	    $query = $this->db->query($sql_alp);
	    return $query;
	}
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iSPageId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iSPageId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	 function delete($id)
	{
		$where = 'iSPageId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	
	function delete_data($iSPageId){
		$sql = "DELETE FROM  $this->table WHERE iSPageId IN ('".$iSPageId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	function list_all_page(){
		$sql = "SELECT * FROM language";			
		$query = $this->db->query($sql);
		//echo "<pre>";
		//print_r(($query[num_rows]));exit;
		return $query;
	       
	}
	
	
}
