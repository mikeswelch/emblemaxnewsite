<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class designcategory_model extends CI_Model {

	private $table= 'design_category';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
		if($field=='' || $order == '')
		{
			$sql = "SELECT a.*, b.* FROM design_category_translation AS a LEFT JOIN $this->table AS b on a.`iDesignCategoryId` = b.`iDesignCategoryId` WHERE a.vLanguageCode='en' $ssql order by b.iDesignCategoryId DESC $var_limit";
		}
		else
		{
			$sql="SELECT a.*, b.* FROM design_category_translation AS a LEFT JOIN $this->table AS b on a.`iDesignCategoryId` = b.`iDesignCategoryId` WHERE a.vLanguageCode='en' $ssql ORDER BY $field $order $var_limit";
		}
		
		$query = $this->db->query($sql);
		return $query;
	}	
	function displayalphasearch(){
		$sql_alp = "SELECT a.*, b.* FROM design_category_translation AS a LEFT JOIN $this->table AS b on a.`iDesignCategoryId` = b.`iDesignCategoryId` where a.vLanguageCode = 'en'";
		return $this->db->query($sql_alp);
	}
	function count_all($ssql){
		$sql = "SELECT count(dc.iDesignCategoryId) AS tot FROM $this->table AS dc LEFT JOIN design_category_translation AS dct ON (dc.iDesignCategoryId = dct.iDesignCategoryId) where dct.vLanguageCode ='en' $ssql ";
		//$sql = "SELECT count(iMenuId) AS tot FROM $this->table WHERE 1=1 $ssql";
		return $this->db->query($sql);
	}
	function designcategory_data(){
		$sql = "SELECT * FROM $this->table where iDesignParentCategoryId = 0";
		return $this->db->query($sql);
	}	
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	function saveData($data)
	{
		$this->db->insert('design_category_translation',$data);
		return $this->db->insert_id();
	}
	function get_one_by_id($id) {
		$sql = "SELECT * FROM $this->table WHERE iDesignCategoryId =".$id;
		$query = $this->db->query($sql);
		return $query;
	}
	function get_data_by_id($id,$lang)
	{
		$sql = "SELECT * FROM design_category_translation WHERE iDesignCategoryId =".$id. " AND vLanguageCode='".$lang."'";
		$query = $this->db->query($sql);
		return $query; 
	}	
	function update($id, $data){
		$this->db->where('iDesignCategoryId', $id);
		return $this->db->update($this->table, $data);
	}
	function menu_data(){
		$sql = "SELECT a.`iDesignCategoryId`, a.`vTitle`, b.`vTitle` AS 'ParentName'FROM $this->table AS a LEFT JOIN $this->table AS b on a.`iDesignParentCategoryId` = b.`iDesignCategoryId` ";
		return $this->db->query($sql);
	}
        function delete($id) {
		$where = 'iDesignCategoryId = '.$id;
		$query = $this ->db-> delete($this->table, $where);
		$query = $this ->db-> delete('design_category_translation', $where);
		return $query;
	}
	function updateDataCatTran($id, $data){
		$this->db->where('iDesignCategoryTranslationId', $id);
		$query = $this->db->update('design_category_translation',$data); 
		return $query; 
	}	
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iDesignCategoryId IN ('".$id."')";
		return $this->db->query($sql);
	}
    
	function delete_data($iDesignCategoryId){
		$sql = "DELETE FROM  $this->table WHERE iDesignCategoryId IN ('".$iDesignCategoryId."')";
		$sql = "DELETE FROM  design_category_translation WHERE iDesignCategoryId IN ('".$iDesignCategoryId."')";
		return $this->db->query($sql);
	}
	function get_prerent($id) {
		$sql = "SELECT * FROM $this->table WHERE iDesignCategoryId = '$id'";
		$data = $this->db->query($sql);
		return $data;
	}
	function title($id){
		$sql = "SELECT vTitle,iDesignParentCategoryId FROM $this->table WHERE iDesignCategoryId = '$id'";
		$path = $this->db->query($sql);
		return $path;
	}	
	function getParentCatNew($iDesignParentCategoryId=0, $old_cat="",$icatIdNot="0",$loop=1,$iDesignCategoryId,$categoryId)
	{	
		
		global $par_arr_new;
		$sql_query = "select dc.iDesignCategoryId AS iDesignCategoryId, dct.vTitle AS vTitle FROM $this->table dc LEFT JOIN design_category_translation dct ON dc.iDesignCategoryId = dct.iDesignCategoryId where dct.vLanguageCode = 'en' AND dc.iDesignParentCategoryId='$iDesignParentCategoryId' AND dc.iDesignCategoryId != '".$categoryId."'";
		$db_tech_prs = $this->db->query($sql_query)->result_array();
		$cnt = count($db_tech_prs);
		if($cnt>0)
		{
			for($i=0 ; $i<$cnt ; $i++)
			{
				$par_arr_new[] = array('iDesignCategoryId'=> $db_tech_prs[$i]['iDesignCategoryId'],'vTitle' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vTitle']);
				$this->getParentCatNew($db_tech_prs[$i]['iDesignCategoryId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iCategoryId,$categoryId);
			}
			$old_cat = "";
		}
		
        return $par_arr_new;
	}
	function path($id){
		$sql = "SELECT c.iDesignParentCategoryId,dct.* FROM design_category c LEFT JOIN design_category_translation dct ON (dct.iDesignCategoryId = c.iDesignCategoryId) WHERE c.iDesignCategoryId = '$id'";
		
		//echo $sql;exit;
		$path = $this->db->query($sql);
		return $path;
	}	
	function all_language()
	{
		$sql = "select * from language where eStatus ='Active'";
		$query = $this->db->query($sql);
		return $query;
	}
}
?>