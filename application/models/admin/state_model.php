<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class state_model extends CI_Model {
    
    private $table= 'state_master';
        
	function __construct() {
		parent::__construct();
	}

	function list_all($var_limit,$ssql,$field,$sort) {
	           
		if($field=='' || $sort==''){
			$sql = "SELECT s.*,c.vCountry,(SELECT count(u.iUserId) FROM user u WHERE s.iStateId = u.iStateId) AS count FROM state_master AS s left join country_master AS c on s.iCountryId=c.iCountryId WHERE 1 $ssql order by iStateId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT s.*,c.vCountry,(SELECT count(u.iUserId) FROM user u WHERE s.iStateId = u.iStateId) AS count FROM state_master AS s left join country_master AS c on s.iCountryId=c.iCountryId WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}

	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}

	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}

	function get_one_by_id($id) { 
		$this->db->where('iStateId', $id);
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iStateId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

        function delete($id) {
		$where = 'iStateId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}

	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iStateId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iStateId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql){
		$sql = "select count(iStateId) AS tot from state_master s left join country_master AS c on s.iCountryId=c.iCountryId WHERE 1=1 $ssql";
		$query = $this->db->query($sql); return $query;
	}

	function displayalphasearch(){
		$sql_alp = "select vState from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}

	function delete_data($iStateId){
		$sql = "DELETE FROM  $this->table WHERE iStateId IN ('".$iStateId."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function list_country() {
		$sql = "SELECT * FROM country_master  order by iCountryId ASC ";
		$query = $this->db->query($sql);
		return $query;
		
	}

	function count($iStateId){
		$sql = "select count(iStateId) AS tot from state_master  WHERE 1=1 And iStateId='".$iStateId."'";
		$query = $this->db->query($sql);
		return $query;
        }

	function check_statecode($code)
	{
		$sql = "select count(iStateId) AS tot from $this->table WHERE vstatecode='".$code."'";
		$query = $this->db->query($sql);
	        return $query;
	}

	function getOldCode($iStateId)
	{
		$sql = "select vstatecode from $this->table WHERE iStateId='".$iStateId."'";
		return $this->db->query($sql);
	}
}