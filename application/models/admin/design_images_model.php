<?php

/*      Model Name: client_model
	Developer Name: Deepak Khamari
	Purpose: to be called when the Client of admin page loads
	Created Date: 12-05-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class design_images_model extends CI_Model {
    
    private $table= 'design_images';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
	
		if($field=='' || $order==''){
		     	
			$sql = "SELECT * FROM design_images WHERE 1 $ssql  order by iDesignImageId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM design_images WHERE 1 $ssql order by vTitle $order $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}

	function get_one_by_id($id) { 
		$this->db->where('iDesignImageId', $id);		
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iDesignImageId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

        function delete($id) {
		$where = 'iDesignImageId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}

	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iDesignImageId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($id, $status) {
		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iDesignImageId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql){
		$sql = "select count(iDesignImageId) AS tot from $this->table e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		$sql_alp = "select vTitle from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}

	function delete_data($iDesignImageId){
		$sql = "DELETE FROM  $this->table WHERE iDesignImageId IN ('".$iDesignImageId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function delete_image($iDesignImageId)
	{
		$data['vImage'] = '';
		$this->db->where('iDesignImageId', $iDesignImageId);
		return $this->db->update($this->table, $data);
	}
	
	function list_dimages(){
		$sql = "SELECT iDesignCategoryId, vTitle FROM $this->table ORDER BY vTitle ASC";
		$query = $this->db->query($sql);
		return $query;
	}	
	function getParentCatNew($iParentId=0, $old_cat="",$icatIdNot="0",$loop=1,$iCategoryId)
	{	
		
		global $par_arr_new;
		$sql_query = "select dc.iDesignCategoryId AS iDesignCategoryId, dct.vTitle AS vTitle FROM design_category dc LEFT JOIN design_category_translation dct ON dc.iDesignCategoryId = dct.iDesignCategoryId where dct.vLanguageCode = 'en' AND dc.iDesignParentCategoryId='$iParentId'";
		//echo $sql_query;exit;
		
		$db_tech_prs = $this->db->query($sql_query)->result_array();
		$cnt = count($db_tech_prs);

		if($cnt>0)
		{
			for($i=0 ; $i<$cnt ; $i++)
			{
				$par_arr_new[] = array('iDesignCategoryId'=> $db_tech_prs[$i]['iDesignCategoryId'],'vTitle' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vTitle']);
				$this->getParentCatNew($db_tech_prs[$i]['iDesignCategoryId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iCategoryId);
			}
			$old_cat = "";
		}
		
        return $par_arr_new;
	}	
		
	
}