<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class productcategory_model extends CI_Model {
    
    private $table= 'product_category';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$sort) {
	
		if($field=='' || $sort==''){
			
		     	$sql = "select pc.*, pct.* FROM $this->table pc LEFT JOIN product_category_translation pct ON (pc.iCategoryId = pct.iCategoryId) where pct.vLanguageCode='en' $ssql  order by pct.iCategoryId Desc $var_limit";
			//$sql = "SELECT * FROM product_category WHERE 1 $ssql  order by iCategoryId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
		     
			$sql = "select pc.*, pct.* FROM $this->table pc LEFT JOIN product_category_translation pct ON pc.iCategoryId = pct.iCategoryId where pct.vLanguageCode='en' $ssql order by $field $sort $var_limit";
			//$sql = "SELECT * FROM product_category WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		//echo $sql;
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}
	
	function saveData($data)
	{
		$this->db->insert('product_category_translation',$data);
		  
		return $this->db->insert_id();
	}
	
	function get_one_by_id($id) {
		
		$sql = "SELECT * FROM $this->table WHERE iCategoryId =".$id;
		$query = $this->db->query($sql);
		return $query; 
		//$this->db->where('iCategoryId', $id);
		//return $this->db->get($this->table);
	}
	
	function get_data_by_id($id,$lang)
	{
		$sql = "SELECT * FROM product_category_translation WHERE iCategoryId =".$id. " AND vLanguageCode='".$lang."'";
		$query = $this->db->query($sql);
		return $query; 
	}
	
	function update($id, $data){
		$this->db->where('iCategoryId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

	function updateDataCatTran($id, $data){
		$this->db->where('iCategoryTranslationId', $id);
		$query = $this->db->update('product_category_translation',$data); 
		return $query; 
	}
	
	function delete($id) {
		$where = 'iCategoryId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		$query1 = $this -> db -> delete('product_category_translation', $where);
		$query2 = $this -> db -> delete('product_category_size', $where);
		
		return $query;
	}
	
	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iCategoryId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iCategoryId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function count_all($ssql){
		$sql = "SELECT count(pc.iCategoryId) AS tot FROM product_category_translation pct LEFT JOIN product_category AS pc ON (pct.iCategoryId = pc.iCategoryId) WHERE pct.vLanguageCode = 'en' $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	
	function displayalphasearch(){
		$sql_alp = "select pc.*, pct.* FROM $this->table pc LEFT JOIN product_category_translation pct ON pc.iCategoryId = pct.iCategoryId where pct.vLanguageCode = 'en'";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	
	function delete_data($iCategoryId){
		$sql = "DELETE FROM  $this->table WHERE iCategoryId IN ('".$iCategoryId."')";
		$query = $this->db->query($sql);
		$sql1 = "DELETE FROM  product_category_size WHERE iCategoryId IN ('".$iCategoryId."')";
		$query1 = $this->db->query($sql1);
		$sql2 = "DELETE FROM  product_category_translation WHERE iCategoryId IN ('".$iCategoryId."')";
		$query2 = $this->db->query($sql2);
		return $query;
	}
	
	function getParentCatNew($iParentId=0, $old_cat="",$icatIdNot="0",$loop=1,$iCategoryId)
	{	
		
		global $par_arr_new;
		$sql_query = "select pc.iCategoryId AS iCategoryId, pct.vCategory AS vCategory FROM $this->table pc LEFT JOIN product_category_translation pct ON pc.iCategoryId = pct.iCategoryId where pct.vLanguageCode = 'en' AND pc.iParentId='$iParentId'";
		$db_tech_prs = $this->db->query($sql_query)->result_array();
		$cnt = count($db_tech_prs);
		if($cnt>0)
		{
			for($i=0 ; $i<$cnt ; $i++)
			{
				$par_arr_new[] = array('iCategoryId'=> $db_tech_prs[$i]['iCategoryId'],'vCategory' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vCategory']);
				$this->getParentCatNew($db_tech_prs[$i]['iCategoryId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iCategoryId);
			}
			$old_cat = "";
		}
		
        return $par_arr_new;
	}
	
	function all_language()
	{
		$sql = "select * from language where eStatus ='Active'";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function get_prerent($id) {
		$sql = "SELECT * FROM product_category_translation WHERE iCategoryId = '$id'";
		$data = $this->db->query($sql);
		return $data;
	}	function deletesize($id)
	{
		$where = 'iCategoryId = '.$id;
		$query = $this -> db -> delete('product_category_size', $where);
		return $query;
	}
	function delete_image($iCategoryId)
	{
		$data['vImage'] = '';
		$this->db->where('iCategoryId', $iCategoryId);
		return $this->db->update($this->table,$data);
	}
	
}