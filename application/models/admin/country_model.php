<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class country_model extends CI_Model {
    
    private $table= 'country_master';
        
	function __construct() {
		parent::__construct();
	}
	/*function list_all($var_limit,$ssql) {
		$sql = "SELECT * FROM country_master WHERE 1 $ssql order by iCountryId DESC $var_limit";
		$query = $this->db->query($sql);
		return $query;
	}*/
	
	 function list_all($var_limit,$ssql,$field,$sort) {
		
		if($field== '' || $sort == '')
		{
		//$sql = "SELECT * FROM country_master  WHERE 1 $ssql order by iCountryId DESC $var_limit";
		//$sql = "SELECT c.*,(SELECT count(s.iStateId) FROM state_master s WHERE c.iCountryId = s.iCountryId) AS count ,(SELECT count(u.iUserId) FROM user u WHERE c.iCountryId = u.iCountryId) AS countuser FROM country_master c  WHERE 1 $ssql order by iCountryId DESC $var_limit";
		$sql = "SELECT c.*,(SELECT count(s.iStateId) FROM state_master s WHERE c.iCountryId = s.iCountryId) AS count ,(SELECT count(u.iUserId) FROM user u WHERE c.iCountryId = u.iCountryId) AS countuser FROM country_master c  WHERE 1 $ssql order by iCountryId DESC $var_limit";
		$query = $this->db->query($sql);
		}
		else
		{
		   //$sql="select * from country_master where 1=1 $ssql order by $field $sort $var_limit";
		   $sql = "SELECT c.*,(SELECT count(s.iStateId) FROM state_master s WHERE c.iCountryId = s.iCountryId) AS count ,(SELECT count(u.iUserId) FROM user u WHERE c.iCountryId = u.iCountryId) AS countuser FROM country_master c  WHERE 1 $ssql order by $field $sort $var_limit";
	           $query=$this->db->query($sql);		
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function count_all($ssql){
		$sql = "select count(iCountryId) AS tot from $this->table  WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	function displayalphasearch(){
		$sql_alp = "select vCountry from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	function save($Data){
		
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	
	function get_one_by_id($id) {
		$this->db->where('iCountryId', $id);
		return $this->db->get($this->table);
	}
	function update($id, $data){
		$this->db->where('iCountryId', $id);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iCountryId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iCountryId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	function delete_data($iCountryId){
        $sql = "DELETE FROM  $this->table WHERE iCountryId IN ('".$iCountryId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function delete($id) {
		$where = 'iCountryId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/* duplicate check*/
	function check_countrycode($code)
	{
		$sql = "select count(iCountryId) AS tot from $this->table WHERE vCountryCode='".$code."'";
		$query = $this->db->query($sql);
	        return $query;
	}
	/* duplicate check*/
	function getOldCode($iCountryId)
	{
		$sql = "select vCountryCode from $this->table WHERE iCountryId='".$iCountryId."'";
		return $this->db->query($sql);
	}
	/* Count of state*/
	function count($iCountryId){
		$sql = "select count(iCountryId) AS tot from state_master  WHERE 1=1 And iCountryId='".$iCountryId."'";
		$query = $this->db->query($sql);
		return $query;
        }
	/* Count of freelancer*/
	function count_freelancer($iCountryId){
		$sql = "select count(iCountryId) AS tot from freelancer_users  WHERE 1=1 And iCountryId='".$iCountryId."'";
		$query = $this->db->query($sql);
		return $query;
        }
}
