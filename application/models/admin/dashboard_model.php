<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class dashboard_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count administator record
	Created Date: 12-04-2012
	*/ 
	function count_administator(){
		$sql = "select count(iAdminId) AS tot from administrators where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Client record
	Created Date: 12-04-2012
	*/ 
	function count_user(){
		$sql = "select count(iUserId) AS tot from user where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Freelancer record
	Created Date: 12-04-2012
	*/ 
	function count_product(){
		$sql = "select count(iProductId) AS tot from product where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Brief record
	Created Date: 12-04-2012
	*/ 
	function count_category_types(){
		$sql = "select count(iCategoryTypeId) AS tot from category_types where 1=1";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Quotes records
	Created Date: 12-04-2012
	*/ 
	function count_faq(){
		$sql = "select count(iFAQId) AS tot from faq where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Onwardzbriefs records
	Created Date: 12-04-2012
	*/ 
	function count_transaction(){
		$sql = "select count(iOrderId) AS tot from `order` where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Skill record
	Created Date: 12-04-2012
	*/ 
	function count_contact_us(){
		$sql = "select count(iContactId) AS tot from contact_us where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: count Industries record
	Created Date: 12-04-2012
	*/ 
	function count_help(){
		$sql = "select count(iHelpId) AS tot from help where 1=1 ";
		$query = $this->db->query($sql);
		return $query;
        }
	
	//Pai chart data
	/*function paichat_data_cnt($ssql){
	    $sql = "SELECT count(iOrderId) AS month_wise_cnt FROM `order` WHERE 1=1 $ssql order by iOrderId DESC";
	    //$sql = "SELECT count(iTransactionId) AS day_by_cnt FROM transaction  WHERE 1=1 $ssql order by iTransactionId DESC";	    
	    return $query = $this->db->query($sql);
	}*/
	//Revanue chart
	/*function count_month_wise($ssql){
	    $sql = "SELECT count(iOrderId) AS month_wise_cnt FROM `order` WHERE 1=1 $ssql order by iOrderId DESC";
	    //$sql = "SELECT count(iTransactionId) AS month_wise_cnt FROM transaction  WHERE 1=1 $ssql order by iTransactionId DESC";
	    //echo $sql."</br>";
	    return $query = $this->db->query($sql);
	}*/
	//---------------------------
	/*Function Name: index
	Developer Name: Parth Devmorari
	Purpose: Fetch a last recode of last record
	Created Date: 12-04-2012
	*/ 
	function lastlogin(){
		$sql = "select *  from  login_histories  order by iLoginHistoryId DESC LIMIT 0, 1";
		$query = $this->db->query($sql);
		return $query;
        }     
}

