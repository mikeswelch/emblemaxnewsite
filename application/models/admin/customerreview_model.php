<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class customerreview_model extends CI_Model {
    
    private $table= 'customer_review';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {
	
		if($field=='' || $order==''){		     	
			$sql = "SELECT * FROM customer_review WHERE 1 $ssql  order by iCustomerReviewId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM customer_review WHERE 1 $ssql order by iCustomerReviewId $order $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function save($Data){
		$this->db->insert($this->table,$Data);
		return $this->db->insert_id();
	}

	function get_one_by_id($id) { 
		$this->db->where('iCustomerReviewId', $id);		
		return $this->db->get($this->table);
	}

	function update($id, $data){
		$this->db->where('iCustomerReviewId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}

        function delete($id) {
		$where = 'iCustomerReviewId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}

	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iCustomerReviewId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}

	function multiple_update_status($id, $status) {
		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iCustomerReviewId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}

	function count_all($ssql){
		$sql = "select count(iCustomerReviewId) AS tot from $this->table  WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		$sql_alp = "select vTitle from $this->table where 1=1 $ssql";
		$query = $this->db->query($sql_alp);
		return $query;
	}

	function delete_data($iNewsletterId){
		$sql = "DELETE FROM  $this->table WHERE iCustomerReviewId IN ('".$iNewsletterId."')";
		$query = $this->db->query($sql);
		return $query;
	}
    
        function getEmail(){
		$sql = "SELECT * FROM customer_review ORDER BY vEmail ASC";
		return $this->db->query($sql);		
	}

	function getEmailAddress($iNewsletterId){
		$sql = "select * from $this->table where iCustomerReviewId='".$iNewsletterId."'";
		return $this->db->query($sql);
	}
	function getEmailTemplate($ssql)
	{
		$sql = "SELECT * FROM emailtemplate WHERE 1 $ssql order by iEmailTemplateId DESC";
		return $this->db->query($sql);
	}
	function getReviewDetails($id){
		$sql = "select * from $this->table where iCustomerReviewId='".$id."'";
		return $this->db->query($sql);
	}
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	
}