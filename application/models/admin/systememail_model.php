<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class systememail_model extends CI_Model {
    
    private $table= 'emailtemplate';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Parth Devmorari
	 Purpose: Fatch all data from database
	 Created Date: 12-01-2012
	*/
	/*function list_all($var_limit,$ssql) {
		$sql = "SELECT * FROM emailtemplate WHERE 1 $ssql order by iEmailTemplateId ASC $var_limit";
		$query = $this->db->query($sql);
		return $query;
		$this->db->order_by('iEmailTemplateId','desc');
		return $this->db->get($this->table);
	}*/
	
	 function list_all($var_limit,$ssql,$field,$sort) {
		
		if($field== '' || $sort == '')
		{
			
		$sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC $var_limit";
		$query = $this->db->query($sql);
		}
		else
		{
		   $sql="select * from emailtemplate where 1=1 $ssql order by $field $sort $var_limit";
		  
	           $query=$this->db->query($sql);		
		}
		return $query;
	}
	function limit_fetch(){
		$sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
		return $this->db->query($sql);
	}
	
	/*
	 Function Name: get_one_by_id
	 Developer Name: Parth Devmorari
	 Purpose: Fetch one data for edit
	 Created Date: 12-01-2012
	*/
	function get_one_by_id($id) {
		$this->db->where('iEmailTemplateId', $id);
		return $this->db->get($this->table);
	}
	/*
	 Function Name: update
	 Developer Name: Parth Devmorari
	 Purpose: Updated data save in the data base
	 Created Date: 12-01-2012
	*/
	function update($id, $data){ #echo "edit";exit;
		$this->db->where('iEmailTemplateId	', $id);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	/*
	 Function Name: count_all
	 Developer Name: Parth Devmorari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-01-2012
	*/
	function count_all($ssql){
	    $sql = "select count(iEmailTemplateId) AS tot from $this->table WHERE 1=1 $ssql";
	    $query = $this->db->query($sql);
	    return $query;
	}
	/*
	 Function Name: displayalphasearch
	 Developer Name: Parth Devmorari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-01-2012
	*/
	function displayalphasearch(){
	    $sql_alp = "select vEmailTitle from $this->table where 1=1";
	    $query = $this->db->query($sql_alp);
	    return $query;
	}
	/*
	 Function Name: multiple_update_status
	 Developer Name: Parth Devmorari
	 Purpose: Updated Multiple status at a time and save save in the data base
	 Created Date: 12-01-2012
	*/
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iEmailTemplateId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: update_status
	 Developer Name: Parth Devmorari
	 Purpose: Updated status and save save in the data base
	 Created Date: 12-01-2012
	*/
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iEmailTemplateId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: Delete data in the database 
	 Created Date: 12-01-2012
	*/
	 function delete($id) {
		$where = 'iEmailTemplateId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/*
	 Function Name: delete_data
	 Developer Name: Parth Devmorari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-01-2012
	*/
	function delete_data($iEmailTemplateId){
		$sql = "DELETE FROM  $this->table WHERE iEmailTemplateId IN ('".$iEmailTemplateId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function list_all_page(){
		$sql = "SELECT * FROM language";			
		$query = $this->db->query($sql);
		//echo "<pre>";
		//print_r(($query[num_rows]));exit;
		return $query;
	       
	}
}