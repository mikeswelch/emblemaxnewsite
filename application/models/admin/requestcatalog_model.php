<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class requestcatalog_model extends CI_Model {
    
    private $table= 'request_catalog';
        
	function __construct() {
		parent::__construct();
	}

	function list_all($var_limit,$ssql,$field,$sort) {
	
		if($field=='' || $sort==''){
		     	
			$sql = "SELECT * FROM request_catalog WHERE 1 $ssql  order by iRequestCatalogId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM request_catalog WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}

	function count_all($ssql){
		$sql = "select count(iRequestCatalogId) AS tot from $this->table WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }

	function displayalphasearch(){
		$sql_alp = "select vName from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
        
	function getData($iRequestCatalogId){
		$sql = "SELECT * FROM request_catalog WHERE iRequestCatalogId = '".$iRequestCatalogId."'";
		$query = $this->db->query($sql);
		return $query;
	}
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	function list_state(){
		$this->db->order_by('iStateId','asc');
		return $this->db->get('state_master');
	}


}