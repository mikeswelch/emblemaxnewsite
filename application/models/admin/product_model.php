<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class product_model extends CI_Model {
    
    private $table= 'product';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order) {	
		if($field=='' || $order==''){		     	
			$sql = "select p.*, pt.* FROM $this->table p LEFT JOIN product_translation pt ON (p.iProductId = pt.iProductId) $ssql  order by pt.iProductId Desc $var_limit";	
			$query = $this->db->query($sql);}
		else{
			$sql = "select p.*, pt.* FROM $this->table p LEFT JOIN product_translation pt ON p.iProductId = pt.iProductId  $ssql order by $field $order $var_limit";			
			//$sql = "SELECT * FROM product WHERE 1 $ssql order by vProductName $order $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}	
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	function save($Data){
		$this->db->insert($this->table,$Data);
		return $this->db->insert_id();
	}
	function get_one_by_id($id) { 
		$this->db->where('iProductId', $id);		
		return $this->db->get($this->table);
	}
	function get_one_by_ids($id) { 
		$this->db->where('iProductColorId', $id);		
		return $this->db->get('product_color');
	}
	function get_data_by_id($id,$lang){
		$sql = "SELECT * FROM product_translation WHERE iProductId ='".$id."'";
		$query = $this->db->query($sql);
		return $query; 
	}
	function update($id, $data){
		$this->db->where('iProductId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
	function delete($id){
	  $where = 'iProductId = '.$id;
	  $query = $this -> db -> delete($this->table, $where);
	  $query1 = $this -> db -> delete('product_translation', $where);
	  $query2 = $this -> db -> delete('product_template_size', $where);		
	  return $query;
	}
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iProductId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	function multiple_update_status($id, $status) {		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iProductId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function count_all($ssql){
		$sql = "SELECT count(p.iProductId) AS tot FROM product_translation pt LEFT JOIN product AS p ON (pt.iProductId = p.iProductId) $ssql";		
		$query = $this->db->query($sql);
		return $query;
	}
	function displayalphasearch(){
		$sql_alp = "select pt.vProductName FROM $this->table p LEFT JOIN product_translation pt ON (p.iProductId = pt.iProductId)";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	function delete_data($iProductId){
		$sql = "DELETE FROM  $this->table WHERE iProductId IN ('".$iProductId."')";
		$query = $this->db->query($sql);
		$sql1 = "DELETE FROM  product_template_size WHERE iProductId IN ('".$iProductId."')";
		$query1 = $this->db->query($sql1);
		$sql2 = "DELETE FROM  product_translation WHERE iProductId IN ('".$iProductId."')";
		$query2 = $this->db->query($sql2);
		return $query;
	}
	
	function list_producttype(){
		$sql = "SELECT * FROM product_category  AS pc LEFT JOIN product_category_translation AS pct ON(pc.iCategoryId = pct.iCategoryId) order by vCategory ASC ";
		$query = $this->db->query($sql);
		return $query;
	}	
	function delete_image($iProductId){
		$data['vImage'] = '';
		$this->db->where('iProductId',$iProductId);
		return $this->db->update($this->table, $data);
	}	
	function getParentCatNew($iParentId=0, $old_cat="",$icatIdNot="0",$loop=1,$iCategoryId){			
		global $par_arr_new;
		$sql_query = "select pc.iCategoryId AS iCategoryId, pct.vCategory AS vCategory FROM product_category pc LEFT JOIN product_category_translation pct ON pc.iCategoryId = pct.iCategoryId where pc.iParentId='$iParentId'";
		$db_tech_prs = $this->db->query($sql_query)->result_array();
		$cnt = count($db_tech_prs);
		if($cnt>0)
		{
			for($i=0 ; $i<$cnt ; $i++)
			{
				$par_arr_new[] = array('iCategoryId'=> $db_tech_prs[$i]['iCategoryId'],'vCategory' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vCategory']);
				$this->getParentCatNew($db_tech_prs[$i]['iCategoryId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iCategoryId);
			}
			$old_cat = "";
		}
		
        	return $par_arr_new;
	}
	function get_sizeBycatId($id)
	{
		$sql = "SELECT * FROM product_category_size AS pcs LEFT JOIN size AS s ON(pcs.iSizeId = s.iSizeId) WHERE pcs.iCategoryId ='".$id."'";
		$data = $this->db->query($sql);
		return $data;
	}
	function get_width_lenght($size,$product,$cat)
	{
		$sql = "SELECT iWidth,iLength FROM product_template_size WHERE iSizeId ='".$size."' AND iProductId ='".$product."' AND iCategoryId ='".$cat."'";
		$data = $this->db->query($sql);
		return $data;
	}
	function deletesizetemplate($id)
	{
		$where = 'iProductId = '.$id;
		$query = $this -> db -> delete('product_template_size', $where);
		return $query;	
	}
	function saveSize($Data)
	{
		
		$this->db->insert('product_template_size',$Data);
		return $this->db->insert_id();
	}
	function all_colorsize_deatil($id)
	{
		$sql = "select * from product_color_size Where iProductId=$id";
		$query = $this->db->query($sql);
		return $query;
	}
	function all_unique_colorsize_deatil($id){
		$sql = "select iColorId from product_color_size Where iProductId=$id group by iColorId";		
		$query = $this->db->query($sql);
		return $query;
	}

	function save_color_deatil($Data){		
		$result = $this->db->insert('product_color_size',$Data);
		return $this->db->insert_id();
	}
	function get_prd_colorsize_detail($iProductId,$iColorId){
		
		$sql = "select * from product_color_size where iProductId=$iProductId And iColorId=$iColorId";
		$query = $this->db->query($sql);
		return $query;
	}

	function update_prd_colorsize_detail($id, $data){
		/*echo $id;
		echo "<pre>";print_r($data);
		echo 'query';exit;*/
		$this->db->where('iProductColorSizeId', $id);
		$query = $this->db->update('product_color_size',$data); 
		return $query; 
	}
	function deletesize($id)
	{
		$where = 'iProductId = '.$id;
		$query = $this -> db -> delete('product_template_size', $where);
		return $query;
	}

	function all_color()
	{
		$sql = "select * from color where eStatus ='Active'";
		$query = $this->db->query($sql);
		return $query;
	}
	function all_uniq_color($id){
		$sql="SELECT * FROM color WHERE  NOT EXISTS (SELECT iColorId FROM product_color where color.iColorId= product_color.iColorId AND  product_color.iProductId=$id)";
		$query = $this->db->query($sql);
		return $query;
	}
	function uniqColor($id){
		$sql="SELECT * FROM color WHERE `iColorId`  NOT IN ($id)";	 
		$query = $this->db->query($sql);
		return $query;
	}
	function allColor($id){
		$sql="SELECT * FROM product_color where iProductId=$id";
		//echo $sql;exit; 
		$query = $this->db->query($sql);
		return $query;
	}
	function all_size()
	{
		$sql = "select * from size where eStatus ='Active' ORDER BY iOrder";
		$query = $this->db->query($sql);
		return $query;
	}
	function all_color_id($ids)
	{
		$sql = "select * from color where eStatus ='Active' and iColorId IN ($ids)";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query;
	}

	function saveTransdata($data)
	{
		$this->db->insert('product_translation',$data);
		return $this->db->insert_id();
	}
	function saveData($data)
	{
		$this->db->insert('product_translation',$data);
		return $this->db->insert_id();
	}
	function updateDataCatTran($id, $data){
		$this->db->where('iProductTranslationId', $id);
		$query = $this->db->update('product_translation',$data); 
		return $query; 
	}
	function saveColorData($data){
		$this->db->insert('product_color',$data);
		return $this->db->insert_id();
	}
	function updatecolordata($id,$data){
		$this->db->where('iProductColorId', $id);
		$query = $this->db->update('product_color',$data); 
		return $query; 
	}
	function all_color_product($id)
	{
		$sql = "select * from product_color AS c LEFT JOIN color AS cc ON(c.iColorId = cc.iColorId) where c.iProductId = '".$id."' AND c.eStatus ='Active'";
		$query = $this->db->query($sql);
		return $query;
	}
	function getProductId($id)
	{
		$this->db->where('iProductColorId', $id);
		return $this->db->get('product_color');
		return $query; 
	}
	function deletecolorproduct($id){
		$where = 'iProductColorId = '.$id;
		$query = $this -> db -> delete('product_color', $where);
		return $query;
	}
	function getFabrics(){
		$sql = "SELECT * FROM fabrics WHERE eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getQuantityRange(){
		$sql = "SELECT * FROM quantity_range WHERE eStatus = 'Active'";
		return $this->db->query($sql);		
	}
	function getQuantityRangeId($currentQuantityRangeId){
		$sql = "SELECT * FROM quantity_range WHERE eStatus = 'Active' AND iQuantityRangeId='".$currentQuantityRangeId."'";
		return $this->db->query($sql);		
	}
	function getQuantityRangeById($id){
		$sql = "SELECT * FROM quantity_range WHERE eStatus = 'Active' AND  iQuantityRangeId=$id";
		return $this->db->query($sql);		
	}	
	function getBrands(){
		$sql = "SELECT * FROM brand WHERE eStatus = 'Active'";
		return $this->db->query($sql);
	}
	
	function getOccupations(){
		$sql = "SELECT * FROM occupations WHERE eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function getDecoServices(){
		$sql = "SELECT * FROM print_locations WHERE eStatus = 'Active' ORDER BY iOrder";
		return $this->db->query($sql);
	}
	function getColors(){
		$sql = "SELECT * FROM number_of_colors WHERE eStatus = 'Active'";
		return $this->db->query($sql);		
	}
	function savePrintDecorationData($data){
		$this->db->insert('product_printlocation_numbercolors',$data);
		return $this->db->insert_id();
	}
	function getDecorationData($iProductId,$quantity_id){
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$iProductId."' AND iQuantityRangeId = $quantity_id AND iNumberColorsId!='0'" ;		
		return $this->db->query($sql);		
	}
	
	function getEmbrodaryDecorationData($iProductId,$quantity_id){
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$iProductId."' AND iQuantityRangeId = $quantity_id AND iNumberColorsId='0'" ;		
		return $this->db->query($sql);		
	}	
	function getQuatityData($iProductId,$iNumberColorsId,$iQuantityRangeId){
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$iProductId."' AND iNumberColorsId ='".$iNumberColorsId."' AND iQuantityRangeId ='".$iQuantityRangeId."' ";
		return $this->db->query($sql);		
	}
	function deleteDecorationData($iProductId,$quid){		
		$where = "iProductId ='".$iProductId."'  AND iQuantityRangeId ='".$quid ."' AND iNumberColorsId !='0'";		
		$query = $this->db->delete('product_printlocation_numbercolors', $where);	
		return $query;		
	}
	function deleteEmbrodaryData($iProductId,$quid){		
		$where = "iProductId ='".$iProductId."'  AND iQuantityRangeId ='".$quid ."' AND iNumberColorsId ='0'";		
		$query = $this->db->delete('product_printlocation_numbercolors', $where);	
		return $query;		
	}
	function deleteAllEmbroddaryData($iProductId){
		$where = "iProductId ='".$iProductId."'  AND iNumberColorsId ='0'";		
		$query = $this->db->delete('product_printlocation_numbercolors', $where);	
		return $query;		
	}
	
	
	function deleteColorData($iProductId){		
		$where = 'iProductId = '.$iProductId;
		$query = $this->db->delete('product_color_size', $where);
		return $query;		
	}
	function deleteProductColorSizeId($iProductId){
		$where = 'iProductId = '.$iProductId;
		$query = $this->db->delete('product_color_size', $where);
		return $query;		
	}
	function get_image_by_id($id){
		$this->db->where('iProductColorId', $id);		
		return $this->db->get('product_color');		
	}
	function delete_colorImage($iProductColorId,$data)
	{
		if($data == 'vFrontImage')
		{
			$sql = "UPDATE product_color SET vFrontImage = '' WHERE iProductColorId=".$iProductColorId;			
		}
		if($data == 'vBackImage')
		{
			$sql = "UPDATE product_color SET vBackImage = '' WHERE iProductColorId=".$iProductColorId;
		}
		if($data == 'vRightSleeveImage')
		{
			$sql = "UPDATE product_color SET vRightSleeveImage = '' WHERE iProductColorId=".$iProductColorId;
		}
		if($data == 'vLeftSleeveImage')
		{
			$sql = "UPDATE product_color SET vLeftSleeveImage = '' WHERE iProductColorId=".$iProductColorId;
		}
		$query = $this->db->query($sql);
		return $query;
	}
	/* Getting all the embroidery price ::::::Jyotiranjan:::::*/
	function getEmbroideryPrice($iProductId,$quantity_id){		
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$iProductId."' AND iQuantityRangeId ='".$quantity_id."' AND iNumberColorsId='0'";
		return $this->db->query($sql);		
	}	
	function deletePDF($iProductId){
		$sql = "UPDATE product SET vSizeChart = '' WHERE iProductId = '".$iProductId."'";
		return $this->db->query($sql);
	}
	function getShowfield($iQuantityRangeId){
		$sql = "SELECT * FROM quantity_range WHERE iQuantityRangeId = '".$iQuantityRangeId."'";
		return $this->db->query($sql);
	}	
	function getColorName($iShowField){
		$sql = "SELECT vNumberColor FROM number_of_colors WHERE iNumberColorsId = '".$iShowField."' AND eStatus = 'Active'";
		return $this->db->query($sql);
	}
	function GetTemplateData($templateid){
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$templateid."' ";
		return $this->db->query($sql);		
	}
	function DeleteCurrentData($templateid){
		$sql = "SELECT * FROM product_printlocation_numbercolors WHERE iProductId = '".$templateid."' ";		
		return $this->db->query($sql);		
	}
	function GetColorDetails($iNumberColorsId){		
		$sql = "SELECT * FROM number_of_colors WHERE iNumberColorsId = '".$iNumberColorsId."' ";
		return $this->db->query($sql);		
	}
	function getembroideryData($iProductId,$coppyFromQuantityTempId){		
		$sql = "SELECT * FROM `product_printlocation_numbercolors` WHERE `iProductId`='".$iProductId."' AND `iNumberColorsId`='0' AND `iQuantityRangeId`='".$coppyFromQuantityTempId."'";
		//echo $sql;exit;
		return $this->db->query($sql);		
	}
	
	function deleteData($iProductId,$iNumberColorsId,$currentQuantityRangeId){
		$sql = "DELETE FROM `product_printlocation_numbercolors` WHERE `iProductId`='".$iProductId."' AND `iNumberColorsId`='".$iNumberColorsId."' AND `iQuantityRangeId`='".$currentQuantityRangeId."'";		
		return $this->db->query($sql);		
	}
	function checkColorId($iProductId,$iColorId){		
		$sql = "SELECT iProductColorId,eStatus FROM product_color WHERE iProductId='".$iProductId."' AND iColorId='".$iColorId."'";		
		return $this->db->query($sql);		
	}
	function deleteEmbrodaryies($iProductId,$currentQuantityRangeId){		
		$where = "iProductId ='".$iProductId."'  AND iQuantityRangeId ='".$currentQuantityRangeId ."' AND iNumberColorsId ='0'";		
		$query = $this->db->delete('product_printlocation_numbercolors', $where);	
		return $query;		
	}
	function getQuantityRangetem($iQuantityRangeId){
		$sql = "SELECT * FROM quantity_range WHERE eStatus = 'Active' AND iQuantityRangeId =$iQuantityRangeId";
		return $this->db->query($sql);		
	}
}
