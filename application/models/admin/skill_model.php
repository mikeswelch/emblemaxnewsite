<?php

/*      Model Name: skill_model
	Developer Name: Deepak Khamari
	Purpose: to be called when the Skill of admin page loads
	Created Date: 12-05-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class skill_model extends CI_Model {
    
    private $table= 'skills';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Deepak Khamari
	 Purpose: Fatch all data from database
	 Created Date: 12-05-2012
	*/
	function list_all($var_limit,$ssql,$field,$sort) {
	
		if($field=='' || $sort==''){
		     	
			$sql = "SELECT s.*, (SELECT count(f.iFreelancerUserId) FROM freelancer_skills f WHERE s.iSkillId = f.iSkillId) AS count FROM skills s WHERE 1 $ssql  order by s.iSkillId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT s.*, (SELECT count(f.iFreelancerUserId) FROM freelancer_skills f WHERE s.iSkillId = f.iSkillId) AS count FROM skills s WHERE 1 $ssql  order by $field $sort $var_limit";			
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	     $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	/*
	 Function Name: save
	 Developer Name: Deepak Khamari
	 Purpose: Save data in the database
	 Created Date: 12-05-2012
	*/
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}
	/*
	 Function Name: get_one_by_id
	 Developer Name: Deepak Khamari
	 Purpose: Fetch one data for edit
	 Created Date: 12-05-2012
	*/
	function get_one_by_id($id) { 
		$this->db->where('iSkillId', $id);
		return $this->db->get($this->table);
	}
	/*
	 Function Name: update
	 Developer Name: Deepak Khamari
	 Purpose: Updated data save in the data base
	 Created Date: 12-05-2012
	*/
	function update($id, $data){
		$this->db->where('iSkillId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: Delete data in the database 
	 Created Date: 12-05-2012
	*/
        function delete($id) {
		$where = 'iSkillId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/*
	 Function Name: update_status
	 Developer Name: Deepak Khamari
	 Purpose: Updated status and save save in the data base
	 Created Date: 12-05-2012
	*/
	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iSkillId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	/*
	 Function Name: multiple_update_status
	 Developer Name: Deepak Khamari
	 Purpose: Updated Multiple status at a time and save save in the data base
	 Created Date: 12-05-2012
	*/
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iSkillId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: count_all
	 Developer Name: Deepak Khamari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-05-2012
	*/
	function count_all($ssql){
		$sql = "select count(iSkillId) AS tot from $this->table e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	/*
	 Function Name: displayalphasearch
	 Developer Name: Deepak Khamari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-05-2012
	*/
	function displayalphasearch(){
		$sql_alp = "select vSkill from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	/*
	 Function Name: delete_data
	 Developer Name: Deepak Khamari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-05-2012
	*/
	function delete_data($iSkillId){
		$sql = "DELETE FROM  $this->table WHERE iSkillId IN ('".$iSkillId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function check_skill($newSkill){
		$sql = "SELECT * FROM skills WHERE vSkill = '".$newSkill."'";		
		$query = $this->db->query($sql);
		return $query;
	}
	function getOldSkill($iSkillId)
	{
		$sql = "select vSkill from $this->table WHERE iSkillId='".$iSkillId."'";
		return $this->db->query($sql);
	}
	/* Count of freelancer*/
	function count($iSkillId){
		$sql = "select count(iFreelancerUserId) AS tot from freelancer_skills  WHERE 1=1 And iSkillId='".$iSkillId."'";
		$query = $this->db->query($sql);
		return $query;
        }
}