<?php

/*      Model Name: language_model
	Developer Name: Deepak Khamari
	Purpose: to be called when the Language of admin page loads
	Created Date: 12-11-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class language_model extends CI_Model {
    
    private $table= 'language';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Deepak Khamari
	 Purpose: Fatch all data from database
	 Created Date: 12-11-2012
	*/
	function list_all($var_limit,$ssql,$field,$sort) {
	
		if($field=='' || $sort==''){
		     	
			$sql = "SELECT * FROM language WHERE 1 $ssql  order by iLanguageId Desc $var_limit";			
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM language WHERE 1 $ssql order by vLanguage $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	/*
	 Function Name: save
	 Developer Name: Deepak Khamari
	 Purpose: Save data in the database
	 Created Date: 12-11-2012
	*/
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}
	/*
	 Function Name: get_one_by_id
	 Developer Name: Deepak Khamari
	 Purpose: Fetch one data for edit
	 Created Date: 12-11-2012
	*/
	function get_one_by_id($id) { 
		$this->db->where('iLanguageId', $id);
		return $this->db->get($this->table);
	}
	/*
	 Function Name: update
	 Developer Name: Deepak Khamari
	 Purpose: Updated data save in the data base
	 Created Date: 12-11-2012
	*/
	function update($id, $data){
		$this->db->where('iLanguageId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: Delete data in the database 
	 Created Date: 12-11-2012
	*/
        function delete($id) {
		$where = 'iLanguageId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/*
	 Function Name: update_status
	 Developer Name: Deepak Khamari
	 Purpose: Updated status and save save in the data base
	 Created Date: 12-11-2012
	*/
	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iLanguageId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	/*
	 Function Name: multiple_update_status
	 Developer Name: Deepak Khamari
	 Purpose: Updated Multiple status at a time and save save in the data base
	 Created Date: 12-11-2012
	*/
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iLanguageId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: count_all
	 Developer Name: Deepak Khamari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-11-2012
	*/
	function count_all($ssql){
		$sql = "select count(iLanguageId) AS tot from $this->table  WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	/*
	 Function Name: displayalphasearch
	 Developer Name: Deepak Khamari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-11-2012
	*/
	function displayalphasearch(){
		$sql_alp = "select vLanguage from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	/*
	 Function Name: delete_data
	 Developer Name: Deepak Khamari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-11-2012
	*/
	function delete_data($iLanguageId){
		$sql = "DELETE FROM  $this->table WHERE iLanguageId IN ('".$iLanguageId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/* duplicate check*/
	function check_languagecode($code)
	{
		$sql = "select count(iLanguageId) AS tot from $this->table WHERE vLangCode='".$code."'";
		$query = $this->db->query($sql);
	        return $query;
	}
	/* duplicate check*/
	function getOldCode($iLanguageId)
	{
		$sql = "select vLangCode from $this->table WHERE iLanguageId='".$iLanguageId."'";
		return $this->db->query($sql);
	}
}