<?php

/*  Model Name: client_model
	Developer Name: Deepak Khamari
	Purpose: to be called when the Client of admin page loads
	Created Date: 12-05-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class user_model extends CI_Model {
    
    private $table= 'user';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Deepak Khamari
	 Purpose: Fatch all data from database
	 Created Date: 12-05-2012
	*/
	function list_all($var_limit,$ssql,$field,$order) {
	
		if($field=='' || $order==''){
		     	
			$sql = "SELECT * FROM user WHERE 1 $ssql  order by iUserId Desc $var_limit";
			$query = $this->db->query($sql);}
		else{
			$sql = "SELECT * FROM user WHERE 1 $ssql order by vFirstName $order $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	/*
	 Function Name: save
	 Developer Name: Deepak Khamari
	 Purpose: Save data in the database
	 Created Date: 12-05-2012
	*/
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}
	/*
	 Function Name: get_one_by_id
	 Developer Name: Deepak Khamari
	 Purpose: Fetch one data for edit
	 Created Date: 12-05-2012
	*/
	function get_one_by_id($id) { 
		$this->db->where('iUserId', $id);		
		return $this->db->get($this->table);
	}
	/*
	 Function Name: update
	 Developer Name: Deepak Khamari
	 Purpose: Updated data save in the data base
	 Created Date: 12-05-2012
	*/
	function update($id, $data){
		$this->db->where('iUserId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: Delete data in the database 
	 Created Date: 12-05-2012
	*/
        function delete($id) {
		$where = 'iUserId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/*
	 Function Name: update_status
	 Developer Name: Deepak Khamari
	 Purpose: Updated status and save save in the data base
	 Created Date: 12-05-2012
	*/
	function update_status($id, $status) {
		$data = array('eStatus' => $status);
		$this -> db -> where('iUserId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	/*
	 Function Name: multiple_update_status
	 Developer Name: Deepak Khamari
	 Purpose: Updated Multiple status at a time and save save in the data base
	 Created Date: 12-05-2012
	*/
	function multiple_update_status($id, $status) {
		
		$sql = "UPDATE $this->table SET eStatus = '".$status."' WHERE iUserId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: count_all
	 Developer Name: Deepak Khamari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-05-2012
	*/
	function count_all($ssql){
		$sql = "select count(iUserId) AS tot from $this->table e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	/*
	 Function Name: displayalphasearch
	 Developer Name: Deepak Khamari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-05-2012
	*/
	function displayalphasearch(){
		$sql_alp = "select vFirstName from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	/*
	 Function Name: delete_data
	 Developer Name: Deepak Khamari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-05-2012
	*/
	function delete_data($iUserId){
		$sql = "DELETE FROM  $this->table WHERE iUserId IN ('".$iUserId."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: list_country
	 Developer Name: Deepak Khamari
	 Purpose: List the Country in dropdown in Freelancer
	 Created Date: 12-05-2012
	*/
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	/*
	 Function Name: list_state_by_countryid
	 Developer Name: Deepak Khamari
	 Purpose: Get the state by country wise
	 Created Date: 12-04-2012
	
	function list_state_by_countryid(){
		
		$this->db->order_by('iStateId','asc');
		return $this->db->get('state_master');
	}*/
	
	
	function list_state_by_countryid($id){
		
		
		$this->db->where('iCountryId', $id);
		$this->db->order_by('iStateId','asc');
		return $this->db->get('state_master');
	}
	
	function delete_image($iUserId)
	{
		$data['vPhoto'] = '';
		$this->db->where('iUserId', $iUserId);
		return $this->db->update($this->table, $data);
	}
	function exportData($ssql)
	{
	  $sql = "SELECT * FROM user AS u LEFT JOIN country_master as cm ON(u.iCountryId=cm.iCountryId)
	  LEFT JOIN state_master AS sm ON(u.iStateId=sm.iStateId)
	  WHERE 1=1 $ssql ORDER BY iUserId ASC";
	  return $this->db->query($sql);
	}
	
}