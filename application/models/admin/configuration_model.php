<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class configuration_model extends CI_Model {
    
    private $table= 'configurations';
        
	function __construct() {
		parent::__construct();
	}
	function loadcofig()
	{
		$this->db->order_by('iSettingId','asc');
		return $this->db->get($this->table);
	}
	function update($data, $key){
		$this->db->where('vName', $key);
		$query = $this->db->update($this->table, $data);
		return $query; 
	}
	
}