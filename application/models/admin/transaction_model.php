<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class transaction_model extends CI_Model {

	private $table= '`order`';
        
	function __construct() {
		parent::__construct();
	}
	
	function list_all($var_limit,$ssql,$field,$order,$iUserId) {
		if($field=='' || $order == '')
		{
		//$sql = "SELECT * FROM `order` AS tr  LEFT JOIN currency AS c ON(tr.iCurrencyId=c.iCurrencyId) WHERE 1=1 $ssql AND iBusinessUserId=  '".$iBusinessUserId."' order by iTransactionId DESC $var_limit";
		$sql = "SELECT * FROM $this->table WHERE 1 $ssql AND iUserId='".$iUserId."' order by iOrderId Desc $var_limit";
		 
		$query = $this->db->query($sql);
		}
		else
		{
		$sql="select * from $this->table where 1 $ssql AND iUserId='".$iUserId."' order by $field $order $var_limit";
		
		$query = $this->db->query($sql);	
		}
		return $query;
	}
	function limit_fetch(){
	    $sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
	    return $this->db->query($sql);
	}
	
	function displayalphasearch(){
		$sql_alp = "select iOrderId from $this->table where 1=1";
		return $this->db->query($sql_alp);
	}
	
	function count_all($ssql,$iUserId){
		$sql = "select count(iOrderId) AS tot from $this->table WHERE 1=1 $ssql AND iUserId='".$iUserId."'";		
		return $this->db->query($sql);
	}
	
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	
	function get_one_by_id($id) {
		$this->db->where('iTransactionId', $id);
		return $this->db->get($this->table);
	}
	
	function update($id, $data){
		$this->db->where('iTransactionId', $id);
		return $this->db->update($this->table, $data);
	}
	
        function delete($id) {
		$where = 'iTransactionId = '.$id;
		return $this -> db -> delete($this->table, $where);
	}
	
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iTransactionId IN ('".$id."')";
		return $this->db->query($sql);
	}
    
	function delete_data($commonid){
		$sql = "DELETE FROM  $this->table WHERE iTransactionId IN ('".$commonid."')";
		return $this->db->query($sql);
	}
	/*
	 function Name: show_invoicedata
	 Developer Name: Deepak Khamari
	 Purpose: Retrive data to show on invoice 
	 Created Date: 12-11-2012//t.*,o.iTotalFreelancer,o.vFreelancerIds,o.dOrderDate,o.eOrderStatus,o.fOrderAmount
	 */
	function show_invoicedata($iOrderId) {
		
		
	        $sql = "SELECT * FROM $this->table AS t LEFT JOIN order_history AS td ON td.iOrderId=t.iOrderId LEFT JOIN color c ON td.iColorId = c.iColorId  LEFT JOIN product p ON td.iProductId = p.iProductId LEFT JOIN size s ON td.iSizeId = s.iSizeId  LEFT JOIN  user u ON t.iUserId = u.iUserId WHERE t.iOrderId=".$iOrderId;
		//echo $sql; exit; 
		$query = $this->db->query($sql);
		return $query;
	}
	function show_country($iCountryId){
		$sql = "SELECT vCountry FROM country_master WHERE iCountryId='".$iCountryId."'";
		return $this->db->query($sql);
	}
	function show_state($iStateId){
		$sql = "SELECT vState FROM state_master WHERE iStateId='".$iStateId."'";
		return $this->db->query($sql);
	}
	
	/*function exportData($ssql)
	{
	  $sql = "SELECT *,u.vFirstName AS FirstName,u.vLastName AS LastName FROM $this->table as t LEFT JOIN user AS u ON t.iUserId=u.iUserId WHERE 1=1 $ssql";
	  return $this->db->query($sql);
	  
	}*/
	
}
?>