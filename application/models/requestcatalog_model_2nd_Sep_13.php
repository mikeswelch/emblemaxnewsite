<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class requestcatalog_model extends CI_Model {
	private $table= 'request_catalog';
	function __construct() {
		parent::__construct();
	}
	function save($Data){
		$this->db->insert($this->table, $Data);
		return $this->db->insert_id();
	}
	
	function getsitename(){
		$sql="select vValue from configurations where vName='SITE_NAME'";
		return $this->db->query($sql);
	}
	function getemail(){
		$sql="select vValue from configurations where vName='EMAIL_ADMIN'";
		return $this->db->query($sql);
	}
	function list_sysemaildata($ssql){
               $sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
	       //echo $sql;
               return $this->db->query($sql);
        }
	
	

}