<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class designtshirt_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	function getParentCatNew($iParentId=0, $old_cat="",$icatIdNot="0",$loop=1,$iCategoryId){	
		global $par_arr_new;
		$sql_query = "select pc.iCategoryId AS iCategoryId, pct.vCategory AS vCategory FROM product_category pc LEFT JOIN product_category_translation pct ON pc.iCategoryId = pct.iCategoryId where pc.iParentId='$iParentId'";
		$db_tech_prs = $this->db->query($sql_query)->result_array();
		$cnt = count($db_tech_prs);
		if($cnt>0){
			for($i=0 ; $i<$cnt ; $i++){
				$par_arr_new[] = array('iCategoryId'=> $db_tech_prs[$i]['iCategoryId'],'vCategory' =>  $old_cat."--&nbsp;&nbsp;".$db_tech_prs[$i]['vCategory']);
				$this->getParentCatNew($db_tech_prs[$i]['iCategoryId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iCategoryId,$lang);
			}
			$old_cat = "";
		}
		return $par_arr_new;
	}
	function getAllProducts(){
		$sql = "SELECT pt.*, p.vImage, p.iCategoryId FROM product_translation pt LEFT JOIN product p ON pt.iProductId = p.iProductId WHERE p.eStatus = 'Active' ORDER BY pt.iProductTranslationId ";
		return $this->db->query($sql);
	}
	function getAllDesignImages(){
		$sql = "SELECT `iDesignImageId`, `iDesignCategoryId`, `vTitle`, `vImage` FROM `design_images` WHERE `eStatus` = 'Active'";
		return $this->db->query($sql);
	}
	function getDesignCategories(){
		$sql = "SELECT dc.iDesignCategoryId,dct.vTitle FROM design_category_translation dct LEFT JOIN design_category dc ON (dc.iDesignCategoryId = dct.iDesignCategoryId) WHERE dc.eStatus = 'Active' ORDER BY dc.iDesignCategoryId ASC";
		return $this->db->query($sql);
	}
	function getProductSize(){
		$sql = "SELECT pcs.*, s.vSize FROM product_color_size pcs LEFT JOIN size s ON pcs.iSizeId = s.iSizeId";
		return $this->db->query($sql);
	}
	function getColorImages($lang){
		$sql = "SELECT pc.*,c.* FROM product_color pc LEFT JOIN color c ON pc.iColorId = c.iColorId WHERE pc.eStatus='Active' ORDER BY pc.iProductColorId";
		return $this->db->query($sql);
	}
	function getAllSizes($productWithColorSizeStr){
		$sql = "SELECT pt.*, sz.*,siz.* FROM product_translation pt LEFT JOIN product_template_size sz ON pt.iProductId = sz.iProductId LEFT JOIN size siz ON (sz.iSizeId = siz.iSizeId) WHERE pt.iProductId IN ('".$productWithColorSizeStr."') ORDER BY pt.iProductTranslationId";
		return $this->db->query($sql);
	}
}