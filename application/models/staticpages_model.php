<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class staticpages_model extends CI_Model {

	private $table= 'static_pages';
	
	function __construct() {
		parent::__construct();
	}	
	
        function getStaticPage(){
            $sql = "SELECT tContent FROM static_pages where vFile='about_us'";	
            return $this->db->query($sql);
        }
	function getStaticPages(){
            $sql = "SELECT tContent FROM static_pages where vFile='privacy_policy'";	
            return $this->db->query($sql);
        }
	
	function get_lang(){
		$sql = "SELECT * FROM language";
		return $this->db->query($sql);
	}
	function getDesignCategories($lang){
		$sql = "SELECT dc.*,dct.* FROM design_category_translation dct LEFT JOIN design_category dc ON (dc.iDesignCategoryId = dct.iDesignCategoryId) WHERE dct.vLanguageCode= '".$lang."' ORDER BY dc.iDesignCategoryId ASC";
		return $this->db->query($sql);
	}
	
	function getShipping($lang){
            $sql = "SELECT tContent_".$lang." FROM static_pages where vFile='shipping_refund'";
            return $this->db->query($sql);
	}
	 

    function getCategoryTemp($sid)
	{
            $sql = "Select * from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId = $sid AND product.eStatus = 'Active'";
            $query = $this->db->query($sql);
	    return $query;           
    }
        function getContactus(){
            $sql = "SELECT * FROM static_pages where vFile='contact_us'";
            $query = $this->db->query($sql);
	    return $query;           
        }
	function getFaq(){
            $sql = "SELECT * FROM static_pages where vFile='faq'";
            $query = $this->db->query($sql);
	    return $query;   		
	}
	function getFAQCategory(){
	    $sql = "SELECT * FROM faq_category WHERE eStatus = 'Active' ORDER BY vCategoryName ASC";
	     return $this->db->query($sql);
	}
	function getFaqQueAns($iFaqcategoryid){
	    $sql = "SELECT * FROM faq WHERE iFaqcategoryid = '".$iFaqcategoryid."' AND eStatus = 'Active' ORDER BY iOrderNo ASC";
	    return $this->db->query($sql);
	}
	function getFaqQueAnss(){
	    $sql = "SELECT * FROM faq WHERE eStatus = 'Active' ORDER BY iOrderNo ASC";
	    return $this->db->query($sql);
	}
	function getTcData(){
	    $sql = "SELECT * FROM static_pages where vFile='terms_condition'";
	    return $this->db->query($sql);
	}
	function getCustomerServicesData(){
	    $sql = "SELECT * FROM static_pages where vFile='customer_services'";
	    return $this->db->query($sql);
	}
	
}
?>