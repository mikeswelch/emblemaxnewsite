<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class category_model extends CI_Model {

	private $table= 'user';
    
	
	function __construct() {
		parent::__construct();
	}
        
	function getCategory($rootCategoryId,$limit){
	    $sql = "Select *, (select count(*) from product_category pcs where pcs.iParentId = product_category.iCategoryId ) as numChilds, (select count(*) FROM product pro WHERE pro.iCategoryId = product_category.iCategoryId ) as numProducts from product_category_translation JOIN  product_category ON product_category_translation.iCategoryId = product_category.iCategoryId And  product_category.iParentId = '".$rootCategoryId."' AND product_category.eStatus='Active' $limit";
	    //echo $sql;exit;
	    return $this->db->query($sql);
        }
	
        function getCategoryTemp($sid,$limit){
            $sql = "Select * from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId = $sid AND product.eStatus = 'Active' $limit";
            $query = $this->db->query($sql);
			return $query;           
        }

        function getAllCategory($pid){
            $sql = "Select * from product_category_translation JOIN  product_category ON product_category_translation.iCategoryId = product_category.iCategoryId And product_category.iParentId = $pid AND product_category.eStatus='Active'";
            $query = $this->db->query($sql);
			return $query;           
        }
        function getAllCategoryTemp($sid){
            $sql = "Select * from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId = $sid AND product.eStatus = 'Active'";
            $query = $this->db->query($sql);
			return $query;           
        }
	
	function getCategoryByUrlText($vUrlText,$var_limit){
		$sql = "Select * from product_category_translation JOIN  product_category ON product_category_translation.iCategoryId = product_category.iCategoryId ".$vUrlText.' '.$var_limit;
		//echo $sql;exit;
		return $this->db->query($sql);
	}
	function getProductByCategoryId($iCategoryId,$limit){
		$sql = "Select * from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId='".$iCategoryId ."' $limit";
		//echo $sql;exit;
		return $this->db->query($sql);
	}
	function getFilterProductByCategoryId($iCategoryId,$limit,$range){
		$sql = "Select * from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId='".$iCategoryId ."' $range $limit";
		//echo $sql;exit;
		return $this->db->query($sql);
	}	
	function getMaxpriceProductByCategoryId($iCategoryId,$limit){
		$sql = "Select max(product.fPrice) max_pPrice from product_translation JOIN  product ON product_translation.iProductId = product.iProductId And product.iCategoryId='".$iCategoryId ."' $limit";
		//echo $sql;exit;
		return $this->db->query($sql);
	}		
}