<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model {

	private $table= 'user';
    
	
	function __construct() {
		parent::__construct();
	}
	function get_lang()
	{
		$sql = "SELECT * FROM language";
		$query = $this->db->query($sql);
		return $query;		
	}
}