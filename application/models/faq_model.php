<?php

/*      Model Name: Faq
	Developer Name: Parth Devmorari
	Purpose: to be called when the Faq of admin page loads
	Created Date: 12-01-2012
*/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');
    class faq_model extends CI_Model {
    
    private $table= 'faq';
        
	function __construct() {
		parent::__construct();
	}
	/*
	 Function Name: list_all
	 Developer Name: Parth Devmorari
	 Purpose: Fatch all data from database
	 Created Date: 12-01-2012
	*/
	function list_all($var_limit,$ssql,$field,$sort) {
	
		if($field=='' || $sort=='')
		{
		     	
			$sql = "SELECT * FROM faq WHERE 1 $ssql  order by iOrderNo Desc $var_limit";			
			$query = $this->db->query($sql);}
		else
		{
			$sql = "SELECT * FROM faq WHERE 1 $ssql order by $field $sort $var_limit";
			$query = $this->db->query($sql);	
		}
		return $query;
	}
	/*
	 Function Name: save
	 Developer Name: Parth Devmorari
	 Purpose: Save data in the database
	 Created Date: 12-01-2012
	*/
	function save($Data){
		
		$this->db->insert($this->table,$Data);
		  
		return $this->db->insert_id();
	}
	function limit_fetch(){
		$sql = "SELECT vValue FROM configurations WHERE vName = 'ADMIN_REC_LIMIT' OR vName = 'PAGELIMIT'";
		return $this->db->query($sql);
	}
	/*
	 Function Name: get_one_by_id
	 Developer Name: Parth Devmorari
	 Purpose: Fetch one data for edit
	 Created Date: 12-01-2012
	*/
	function get_one_by_id($id) { 
		$this->db->where('iFAQId', $id);
		return $this->db->get($this->table);
	}
	/*
	 Function Name: update
	 Developer Name: Parth Devmorari
	 Purpose: Updated data save in the data base
	 Created Date: 12-01-2012
	*/
	function update($id, $data){
		$this->db->where('iFAQId', $id);
		$query = $this->db->update($this->table,$data); 
		return $query; 
	}
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: Delete data in the database 
	 Created Date: 12-01-2012
	*/
        function delete($id) {
		$where = 'iFAQId = '.$id;
		$query = $this -> db -> delete($this->table, $where);
		return $query;
	}
	/*
	 Function Name: update_status
	 Developer Name: Parth Devmorari
	 Purpose: Updated status and save save in the data base
	 Created Date: 12-01-2012
	*/
	function update_status($id, $status) {
		$data = array('estatus' => $status);
		$this -> db -> where('iFAQId', $id);
		$query = $this->db->update($this->table, $data);
		return $query;
	}
	/*
	 Function Name: multiple_update_status
	 Developer Name: Parth Devmorari
	 Purpose: Updated Multiple status at a time and save save in the data base
	 Created Date: 12-01-2012
	*/
	function multiple_update_status($id, $status) {
		$sql = "UPDATE $this->table SET estatus = '".$status."' WHERE iFAQId IN ('".$id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	/*
	 Function Name: count_all
	 Developer Name: Parth Devmorari
	 Purpose: count how many data of administator in the database.
	 Created Date: 12-01-2012
	*/
	function count_all($ssql){
		$sql = "select count(iFAQId) AS tot from faq e WHERE 1=1 $ssql";
		$query = $this->db->query($sql);
		return $query;
        }
	/*
	 Function Name: displayalphasearch
	 Developer Name: Parth Devmorari
	 Purpose: show a fetch data form database of alphselected
	 Created Date: 12-01-2012
	*/
	function displayalphasearch(){
		$sql_alp = "select vQuestion from $this->table where 1=1";
		$query = $this->db->query($sql_alp);
		return $query;
	}
	/*
	 Function Name: delete_data
	 Developer Name: Parth Devmorari
	 Purpose: Delete Multiple data in the database
	 Created Date: 12-01-2012
	*/
	function delete_data($faq_id){
		$sql = "DELETE FROM  $this->table WHERE iFAQId IN ('".$faq_id."')";
		$query = $this->db->query($sql);
		return $query;
	}
	function getCategory(){
		$sql = "select * from faq_category WHERE eStatus = 'Active' Order By iFaqcategoryid ASC";
		$query = $this->db->query($sql);
		return $query;		
	}

}