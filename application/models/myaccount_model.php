<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class myaccount_model extends CI_Model {

	private $table= 'user';
    
	
	function __construct() {
		parent::__construct();
	}

	function get_lang()
	{
		$sql = "SELECT * FROM language";
		$query = $this->db->query($sql);
		return $query;		
	}
	
	function list_country(){
		$this->db->order_by('iCountryId','asc');
		return $this->db->get('country_master');
	}
	
	function list_states(){
		$this->db->order_by('iStateId','asc');
		return $this->db->get('state_master');
	}
	function getUser($fName){
	    $sql = "SELECT * FROM user WHERE vFirstName = '".$fName."'";
	    $query = $this->db->query($sql);
	    return $query;	    
	}
	function getUserData($iUserId){
	    $sql = "SELECT * FROM user WHERE iUserId = '".$iUserId."'";
	    $query = $this->db->query($sql);
	    return $query;		
	}
	function update_pass($password,$id){
		$sql = "Update user SET vPassword = '".$password."' WHERE iUserId = ".$id;
		$query = $this->db->query($sql);
		return $query; 			
	}
	function list_sysemail($ssql)
	{
		$sql = "SELECT * FROM emailtemplate  WHERE 1 $ssql order by iEmailTemplateId DESC";
		$query = $this->db->query($sql);
		return $query;
	}
	function get_user($iUserId){
	    $sql = "SELECT * FROM user WHERE iUserId = '".$iUserId."'";
	    $query = $this->db->query($sql);
	    return $query;		
	}
	function list_state_by_countryid($id){
		
		$this->db->where('iCountryId', $id);
		$this->db->order_by('iStateId','asc');
		return $this->db->get('state_master');
	}
	function edit_profile($Data,$iUserId){
		$this->db->where('iUserId', $iUserId);
		return $this->db->update($this->table, $Data);
	}
	function getNewsletter($iUserId){
		$sql = "SELECT * FROM newsletter WHERE iUserId = '".$iUserId."'";
		$query = $this->db->query($sql);
		return $query;
	}
	function edit_newsletter($iUserId){
		//$sql = "UPDATE newsletter SET eStatus = '".$newsletter."' WHERE iUserId = '".$iUserId."'";
		$sql = "DELETE FROM newsletter WHERE iUserId = '".$iUserId."'";
		$query = $this->db->query($sql);
		return $query;
	}
	function getUserId($iUserId){
		$sql = "SELECT * FROM newsletter WHERE iUserId = '".$iUserId."'  AND eStatus = 'No'";
		return $this->db->query($sql);
	}
	function updateNews($eStatus,$iUserId){
		$sql = "UPDATE newsletter SET eStatus = '".$eStatus."' WHERE iUserId = '".$iUserId."'";
		$query = $this->db->query($sql);
		return true; 	
	}
	function all_orders($iUserId)
	{
	       $sql = "SELECT o.*,u.*,od.*,p.*,o.eStatus AS status FROM `order` o LEFT JOIN user u ON (o.iUserId = u.iUserId) LEFT JOIN order_detail od ON (o.iOrderId = od.iOrderId) LEFT JOIN product_translation p ON (od.iProductId = p.iProductId) WHERE u.iUserid ='".$iUserId."' order by o.iOrderId Desc";
		$query = $this->db->query($sql);
		return $query;	
	}
	function getOrderData($id){
	    $sql = "SELECT sm.*,dp.*,od.*,o.*,pt.* FROM order_detail od LEFT JOIN `order` o ON (o.iOrderId = od.iOrderId) LEFT JOIN product_translation pt ON (od.iProductId = pt.iProductId) LEFT JOIN designed_products dp ON (dp.iDesignedProductId = od.iDesignedProductId) LEFT JOIN shipping_method sm ON (o.iShippingMethodId = sm.iShippingMethodId) WHERE o.iOrderId = '".$id."'";
	    $query = $this->db->query($sql);
	    return $query;	    
	}
	function list_state(){
		$this->db->order_by('iStateId','asc');
		return $this->db->get('state_master');
	}
	function getUserDetails($iUserId){
		$sql = "SELECT * FROM user WHERE iUserId = '".$iUserId."'";
		return $this->db->query($sql);
	}
	function getOrderDatas($id){
	   $sql = "SELECT o.*,u.*,od.*,p.*,o.eStatus AS status FROM `order` o LEFT JOIN user u ON (o.iUserId = u.iUserId) LEFT JOIN order_detail od ON (o.iOrderId = od.iOrderId) LEFT JOIN product_translation p ON (od.iProductId = p.iProductId) WHERE u.iUserid ='".$id."' order by o.iOrderId Desc";
	    $query = $this->db->query($sql);
	    return $query;	    
	}
	function getUserDetail($iUserId){
		$sql = "SELECT * FROM billing_address WHERE iBillingUserId = '".$iUserId."'";
		return $this->db->query($sql);
	}
	function updateAddress($Data,$id){
		$this->db->where('iUserAddressId', $id);
		return $this->db->update('user_address', $Data);
	}
	function getBillShippAdd($id){
		$sql = "SELECT * FROM billing_address WHERE iBillingUserId = '".$id."' AND eBillingType = 'Default'";
		return $this->db->query($sql);		
	}
	function getShippingAdd($id){
		$sql = "SELECT * FROM shipping_address WHERE iShippingUserId = '".$id."' AND eShippingType = 'Default'";
		return $this->db->query($sql);		
	}
	function save_newsletter($Data){
		$this->db->insert('newsletter',$Data);
		return $this->db->insert_id();
	}
        function updateBillAddress($id,$Data){
		$this->db->where('iBillingUserId', $id);
		return $this->db->update('billing_address',$Data); 		
	}
       function updateShippAddress($id,$Data){
		$this->db->where('iShippingUserId', $id);
		return $this->db->update('shipping_address',$Data); 		
	}
}