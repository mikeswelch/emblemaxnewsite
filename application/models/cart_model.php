<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class cart_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	function getProductById($iProductId){
	    $sql = "SELECT p.iProductId,p.vImage, pt.vProductName FROM product p LEFT JOIN product_translation pt ON p.iProductId = pt.iProductId WHERE p.iProductId = '".$iProductId."'";
	    return $this->db->query($sql);
        }
	function getCouponDetailByCode($couponCode){
		$sql = "SELECT * FROM coupon WHERE vCouponCode = '".$couponCode."'";
		return $this->db->query($sql);
	}
	function getTurnAroundTimeById($iTurnAroundTimeId){
		$sql = "SELECT vTitle,fCharge, eFeeCharge FROM turn_arround_time WHERE iTurnAroundTimeId = '".$iTurnAroundTimeId."'";
		return $this->db->query($sql);
	}
}