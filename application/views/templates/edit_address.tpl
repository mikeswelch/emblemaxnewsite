{include file="header.tpl"}
<div class="main-container col2-right-layout">
<div class="main-inner clearfix">
<div class="container">
<div class="row-fluid show-grid">
<div class="col-main-wap span9">
    
<div class="col-main">
<div class="col-main-inner">
<div class="my-account"><div class="page-title">
    <h1 style="float:left;">Edit Address</h1>
</div>
<!--<form action="{$site_url}myAccount/edit_address" method="post" id="form-validate" onsubmit="return checkAdd();">-->
<form action="#" method="post" id="form-validate" >
<input type="hidden" name="iUserId" id="iUserId" value="{$UserDetail[0]->iUserId}">
<input type="hidden" name="iAddressId" id="iAddressId" value="{$UserDetail[0]->iUserAddressId}"> 
    <div class="fieldset">
    <h2 class="legend">Contact Information</h2>
        <ul class="form-list row-fluid show-grid">
            <li class="fields">
                   <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>First Name</label>
    <div class="input-box">
        <input type="text" id="vFirstName" name="Data[vFirstName]" value="{if $UserDetail[0]->vBillingFirstName neq '' }{$UserDetail[0]->vBillingFirstName}{/if}" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
     <div class="validation-advice" id="vFirstNameDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Last Name</label>
    <div class="input-box">
        <input type="text" id="vLastName" name="Data[vLastName]" value="{if $UserDetail[0]->vBillingLastName neq '' }{$UserDetail[0]->vBillingLastName}{/if}" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="vLastNameDiv" style="display:none; float: left;"></div>
</div>
</div>
            </li>
            <li class="fields">
                <div class="field span6">
                    <label for="telephone" class="required"><em>*</em>Telephone</label>
                    <div class="input-box">
                        <input type="text" name="Data[vTelephone]" value="{if $UserDetail[0]->vBillingTelephone neq '' }{$UserDetail[0]->vBillingTelephone} {/if}" title="Telephone" class="input-text   required-entry" id="vTelephone" />
                    </div>
		    <div class="validation-advice" id="vTelephoneDiv" style="display:none; float: left;"></div>
                </div>
                <div class="field span6">
                    <label for="fax">Fax</label>
                    <div class="input-box">
                        <input type="text" name="Data[vFax]" id="vFax" title="Fax" value="{if $UserDetail[0]->vBillingFax neq '' }{$UserDetail[0]->vBillingFax}{/if}" class="input-text " />
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="fieldset">
        <h2 class="legend">Address</h2>
        <ul class="form-list row-fluid show-grid">
                    <li class="wide">
                <label for="street_1" class="required"><em>*</em>Street Address</label>
                <div class="input-box">
                    <input type="text" name="Data[tStreetAddress1]" value="{if $UserDetail[0]->tBillingStreetAddress1 neq '' }{$UserDetail[0]->tBillingStreetAddress1}{/if}" title="Street Address" id="tStreetAddress1" class="span12 input-text  required-entry" />
                </div>
		<div class="validation-advice" id="tStreetAddress1Div" style="display:none; float: left;"></div>
            </li>
                            <li class="wide">
                <div class="input-box">
                    <input type="text" name="Data[tStreetAddress2]" value="{if $UserDetail[0]->tBillingStreetAddress2 neq '' }{$UserDetail[0]->tBillingStreetAddress2}{else}{/if}" title="Street Address 2" id="tStreetAddress2" class="span12 input-text " />
                </div>
            </li>
                                <li class="fields">
                <div class="field span6">
                    <label for="city" class="required"><em>*</em>City</label>
                    <div class="input-box">
                        <input type="text" name="Data[vCity]" value="{if $UserDetail[0]->vBillingCity neq '' }{$UserDetail[0]->vBillingCity}{else}{/if}" title="vCity" id="vCity" class="span12 input-text " />
                    </div>
		    <div class="validation-advice" id="vCityDiv" style="display:none; float: left;"></div>
                </div>
            <div class="input-box" style="width:410px;">
                <label for="firstname" class="required"><em>*</em>Select Country</label>
    		<select name="" class="input-text span12 required-entry" id="iCountryId" name="Data[iCountryId]" onchange="getStates(this.value);" title="Country">
		 <option>--Select Country--</option>
		 {section name=i loop=$country}
    			<option value='{$country[i]->iCountryId}' {if $operation eq 'edit'}{if $UserDetail[0]->iBillingCountryId eq $country[i]->iCountryId}selected{/if}{/if}>{$country[i]->vCountry}</option>
    		 {/section}
    		</select>
            </div>
	    <div class="validation-advice" id="iCountryIdDiv" style="display:none; float: left;"></div>
            </li>
            <li>
            <div class="field name-lastname span6">
                <label for="lastname" class="required"><em>*</em>Select State</label>
                <div class="input-box">
                    <select id="states" name="Data[iStateId]"  title="State" class="input-text span12 required-entry"  >
                     <option value="">--Select State--</option>
                         {section name=i loop=$state}
                             <option value='{$state[i]->iStateId}'{if $operation eq 'edit'} {if $UserDetail[0]->iBillingStateId eq $state[i]->iStateId}selected{/if}{/if}>{$state[i]->vState}
                         {/section}
                             </option>
                    </select>
                </div>
		<div class="validation-advice" id="statesDiv" style="display:none; float: left;"></div>
            </div>       
            </li>
            <li class="fields">
                <div class="field span6">
                    <label for="zip" class="required"><em>*</em>Zip/Postal Code</label>
                    <div class="input-box">
                        <input type="text" name="Data[vZipCode]" value="{if $UserDetail[0]->vBillingZipCode neq '' }{$UserDetail[0]->vBillingZipCode}{/if}" title="Zip/Postal Code" id="vZipCode" class="input-text validate-zip-international  required-entry" />
                    </div>
		    <div class="validation-advice" id="vZipCodeDiv" style="display:none; float: left;"></div>
                </div>
            <div class="field name-firstname span6">
            
            </li>
            <li>
                                    <input type="hidden" name="default_billing" value="1" />
                            </li>
            <li>
                                    <input type="hidden" name="default_shipping" value="1" />
                            </li>
<label style="float:left;">
Default Billing Address<br/>
Default Shipping Address
</label></br>
</ul>
    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="{$site_url}myAccount/myDashboard"><small>&laquo; </small>Back</a></p>
        <button type='button' title="Save Address" class="button" ondblclick="return checkAdd();"><span><span>Save Address</span></span></button>
    </div>
</form>
</div>									</div>
								</div>
							</div>
 <div style="margin-top:98px;">{include file="right_myAccount.tpl"}</div>

	<div class="block mt-smartmenu">
		<div class="title-divider">
			<span>&nbsp;</span>
                </div>

</div>
						</div>
					</div>
                </div>
				
				<div class="mt-productcroller-container clearfix"></div>
				<!--Call logo scroller-->
									<div class="mt-footer-static-container-logoscroller clearfix">
						<div class="mt-footer-static-logo container">
							<div class="mt-logoscroller">								
								
								<div class="list_logocarousel">
								</div>								
							</div>
						</div>
     
					</div>
								
            </div>
        </div>
                        <p id="back-top" class="hidden-phone">
				<a href="#top"><span title="Back to Top"></span></a>
			</p>
			        </div>
    </div>
</div>
</div>
</div>
</div>
{literal}
<script type="text/javascript">
	var tooltip = $mtkb('#back-top span');
	$mtkb(tooltip).tooltip(true);
	$mtkb(".block-layered-nav .block-content p.block-subtitle").hide();
	$mtkb("#product-options-wrapper p.required").hide();	
	$mtkb('.toolbar.row-pager, .toolbar-bottom .sort-by, .toolbar-bottom .limiter, .toolbar-bottom .view-mode').hide();
	$mtkb('.toolbar-bottom .toolbar .row-pager').show();
	$mtkb('table#my-orders-table thead').addClass('hidden-phone');	
	
	$mtkb("table").removeClass('data-table');
	$mtkb("table").addClass('table table-bordered');	
	$mtkb("table#shopping-cart-totals-table").removeClass('table table-bordered');	
	$mtkb('#sitemap_top_links').addClass('row-fluid show-grid');
	$mtkb('#sitemap_top_links .links, .page-sitemap ul.links').addClass('clearfix');
	
	$mtkb("#review-form .form-list input.input-text,#review-form .form-list textarea").addClass('span4');
	$mtkb("#shipping-zip-form .form-list select, #billing-new-address-form select.validate-select, ul li.fields .input-box select.validate-select, ul li.fields div.field .input-box input.input-text, #wishlist-table textarea").addClass('span12');
	$mtkb(".buttons-set p.required").css("width","100%");
	$mtkb(".buttons-set p.back-link").addClass("pull-left");
	$mtkb(".buttons-set button.button").addClass("pull-right");
	$mtkb(".checkout-cart-index .col-main .cart-empty li.error-msg").addClass('span4 offset4');
	$mtkb("#multiship-addresses-table select").addClass('span4');	
	$mtkb("#multiship-addresses-table .qty").addClass('span2');	
	var msize = false;
	var msize1 = false;
	var checksize = function(){
        w = $mtkb(".container").width();  
        msize = (w > 730) ? false : true; 
        msize1 = (w >= 704) ? false : true; 
    }   
    
    $mtkb('.mtajaxcart').hide(); 
	$mtkb(".top-cart").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(300).slideDown(500, "easeOutCubic");
	}, function() {
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	});  
	$mtkb('.mt-search-form').hide(); 
	$mtkb(".top-search").hover(function() {
		if(!msize) {
			$mtkb(this).addClass('hover');
			$mtkb(".top-search .mt-search-form").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
		}
	}, function() {
		if(!msize) {
		$mtkb(".top-search .mt-search-form").stop(true, true).delay(100).slideUp(200, "easeInCubic");
		}
	}); 
	$mtkb('.mt-top-link').hide(); 
	$mtkb(".top-link").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
	}, function() {
		if(msize1){
			$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideDown(200, "easeInCubic");
		}
		else
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	}); 
    $mtkb(document).ready(function(){
        checksize();
	}); 
    $mtkb(window).resize(function(){
        checksize();
    });	
	
	$mtkb("#back-top").hide();  
</script>
{/literal}
{include file="footer.tpl"}
{literal}
<script type="text/javascript">    
function getStates(icountryid)
{
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}
</script>
<script type="text/javascript">
function checkAdd()
{
    var extra = '';
    var firstname =  $('#vFirstName').val();
    var lastname =  $('#vLastName').val();    
    var phone = $('#vTelephone').val();    
    var streetAddress1 = $('#tStreetAddress1').val();
    var streetAddress2 = $('#tStreetAddress2').val();
    var fax = $('#vFax').val();
    var city = $('#vCity').val();    
    var country = $('#iCountryId').val();    
    var state = $('#states').val();
    var zip = $('#vZipCode').val();   
    
    if( $('#vFirstName').val() == '' && $('#vLastName').val() ==''&& $('#vTelephone').val() ==''&& $('#tStreetAddress1').val() ==''&& $('#vCity').val() ==''&& $('#iCountryId').val() =='--Select Country--'&& $('#vZipCode').val() =='')
   {
			  $("#vFirstName").addClass("borderValidations");
			  $('#vFirstNameDiv').html('This is requried field');
			  $('#vFirstNameDiv').show();
			  
			  $("#vLastName").addClass("borderValidations");
			  $('#vLastNameDiv').html('This is requried field');
			  $('#vLastNameDiv').show();
			  
			  $("#vTelephone").addClass("borderValidations");
			  $('#vTelephoneDiv').html('This is requried field');
			  $('#vTelephoneDiv').show();
			  
			  $("#tStreetAddress1").addClass("borderValidations");
			  $('#tStreetAddress1Div').html('This is requried field');
			  $('#tStreetAddress1Div').show();
			  
			  $("#vCity").addClass("borderValidations");
			  $('#vCityDiv').html('This is requried field');
			  $('#vCityDiv').show();
			  
			  $("#iCountryId").addClass("borderValidations");
			  $('#iCountryIdDiv').html('This is requried field');
			  $('#iCountryIdDiv').show();
			  
			  $("#states").addClass("borderValidations");
			  $('#statesDiv').html('This is requried field');
			  $('#statesDiv').show();
			  
			  $("#vZipCode").addClass("borderValidations");
			  $('#vZipCodeDiv').html('This is requried field');
			  $('#vZipCodeDiv').show();
			//  return false;
			  
   }
   var validate = true;
    if( $('#vFirstName').val() == '')
    {
			  $("#vFirstName").addClass("borderValidations");
			  $('#vFirstNameDiv').html('This is requried field');
			  $('#vFirstNameDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vFirstNameDiv').hide();
			 $("#vFirstName").removeClass("borderValidations");
    }
    if( $('#vLastName').val() == '')
    {
			  $("#vLastName").addClass("borderValidations");
			  $('#vLastNameDiv').html('This is requried field');
			  $('#vLastNameDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vLastNameDiv').hide();
			 $("#vLastName").removeClass("borderValidations");
    }
    if( $('#vTelephone').val() == '')
    {
			  $("#vTelephone").addClass("borderValidations");
			  $('#vTelephoneDiv').html('This is requried field');
			  $('#vTelephoneDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vTelephoneDiv').hide();
			 $("#vTelephone").removeClass("borderValidations");
    }
    if( $('#tStreetAddress1').val() == '')
    {
			  $("#tStreetAddress1").addClass("borderValidations");
			  $('#tStreetAddress1Div').html('This is requried field');
			  $('#tStreetAddress1Div').show();
			  validate=false;
    }
    else
    {
			 $('#tStreetAddress1Div').hide();
			 $("#tStreetAddress1").removeClass("borderValidations");
    }
    if( $('#vCity').val() == '')
    {
			  $("#vCity").addClass("borderValidations");
			  $('#vCityDiv').html('This is requried field');
			  $('#vCityDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vCityDiv').hide();
			 $("#vCity").removeClass("borderValidations");
    }
    if( $('#iCountryId').val() == '--Select Country--')
    {
			  $("#iCountryId").addClass("borderValidations");
			  $('#iCountryIdDiv').html('This is requried field');
			  $('#iCountryIdDiv').show();
			  validate=false;
    }
    else
    {
			 $('#iCountryIdDiv').hide();
			 $("#iCountryId").removeClass("borderValidations");
    }
    if( $('#states').val() == '')
    {
			  $("#states").addClass("borderValidations");
			  $('#statesDiv').html('This is requried field');
			  $('#statesDiv').show();
			  validate=false;
    }
    else
    {
			 $('#statesDiv').hide();
			 $("#states").removeClass("borderValidations");
    }
    if( $('#vZipCode').val() == '')
    {
			  $("#vZipCode").addClass("borderValidations");
			  $('#vZipCodeDiv').html('This is requried field');
			  $('#vZipCodeDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vZipCodeDiv').hide();
			 $("#vZipCode").removeClass("borderValidations");
    }
    if (validate)
    {
	  extra+='?vFirstName='+firstname;
	  extra+='&vLastName='+lastname;
          extra+='&tStreetAddress1='+streetAddress1;
          extra+='&tStreetAddress2='+streetAddress2;
	  extra+='&vTelePhone='+phone;
	  extra+='&vZipCode='+zip;
	  extra+='&vCity='+city;
          extra+='&vFax='+fax;
          extra+='&iCountryId='+country;
	  extra+='&iStateId='+state;
	  
	var url = site_url + 'myAccount/edit_address';
        var pars = extra;
         $.post(url+pars,
	      function(data) {
		if(data == 'updated'){
                    window.location = site_url+'myAccount/myDashboard';
		}
	      });
    }
    else
    {
	 return false;
    }
}

</script>
{/literal}