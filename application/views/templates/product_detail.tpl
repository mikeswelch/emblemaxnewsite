{include file="header.tpl" title="Example Smarty Page"}
<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="breadcrumbs row-fluid">
   <div class="container">
     <ul class="span12" style="text-align:left;">
	<li class="home">
	     <a href="{$site_url}" title="Go to Home Page">Home</a>
	     <samp></samp>
	</li> 
	<li class="category5">
	     <strong>T-Shirt</strong>
	</li> 
     </ul> 

   </div>	
   </div>
	<div class="main container">
	<h2 class="pagetital subtit" style="width:150px !important; width:28% !important;">{$productData[0]->vProductName|@ucfirst} </h2>
         <div class="product_quto_main">
	    
	    	<div class="product_detail_left">
		<div class="images">
		    <div class="main_image multiple storePadHeight" style="text-align: center;">
			<img height="400px" width="300px" src="{$upload_path}product/{$productData[0]->iProductId}/{$productData[0]->vImage}">			
		    </div>
		    
		</div>
	    </div>
	    
	    
	    <div class="product_detail_right">
		<div id="Module_ApparelQuote_container">
		    <h4 class="quote_steps_head">{$productData[0]->vProductName|@ucfirst}</h4>
		    <h5 class="noneSizeSelected" style="margin-top: 20px;">{$productData[0]->tDescription|@ucfirst}</h5>
		    <div style="float: left; margin-top:35%;">
			<a href="{$site_url}instant-quote/{$productData[0]->iProductId}"><input type="button" value="Get A Quote" class="instnt_btn" style="float: left; background: #9e0546;border: 2px solid #d17a99; line-height: 25px;padding: 5px 15px;" title="GET A QUOTE"></a>
			<a href="#upload-artwork" id="clickUploadArtwork"></a>
			{if $productData[0]->vSizeChart neq ''}<a href="{$site_url}productdetail/download?id={$productData[0]->iProductId}"><input type="button" value="Size Chart" class="instnt_btn" id="upload_artwork_cart" style="float: left; margin-left: 20px;background: #9e0546; border: 2px solid #d17a99;line-height: 25px; padding: 5px 15px; " title="SIZE CHART"></a>{/if}
		    </div>
		</div>
	    </div>
	 </div>        </div>
    </div>
</div>
{include file="footer.tpl"}