{include file="header.tpl" title="Example Smarty Page"}
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}bootstrap/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/main.css">
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/smoothness/jquery-ui-1.9.2.custom.min.css" />
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/jquery.fancyProductDesigner.css" />
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/jquery.fancyProductDesigner-fonts.css" />
<script type="text/javascript" src="{$fancybox_path}lib/jquery-1.8.0.min.js"></script>
<script src="{$custom_tshirt_path}bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{$custom_tshirt_path}js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<script src="{$custom_tshirt_path}js/jquery.fancyProductDesigner.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{$fancybox_path}css/jquery.fancybox.css?v=2.1.0" media="screen" />
<script type="text/javascript" src="{$fancybox_path}source/jquery.fancybox.js?v=2.1.0"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>

<div class="container">
	<div class="main">

		<div id="boxmain">
		{literal} 
	<script type="text/javascript">
			var iDesignedProductId = 0;
			var prod_size_id = 0;
			var frontSaved = 0;
			var backSaved = 0;
			
			var prod_color_id =0;
			var prod_color = '';
			var prod_price = 0;
			var prod_qty =1;
			//var prod_id = "{/literal}{$all_products[0]->iProductId}{literal}";
			//var prod_name = "{/literal}{$all_products[0]->vProductName}{literal}";
			
			
			jQuery(document).ready(function() {
				//call the plugin and save it in a variable to have access to the API
				var fpd = $('#fpd').fancyProductDesigner({
					editorMode: false,
					fonts: ['Arial', 'Helvetica', 'Times New Roman', 'Verdana', 'Geneva', 'Fearless'],
					//these are the parameters for the text that is added via the "Add Text" button
					customTextParamters:{x: 210, y: 250, colors: "#000000", removable: true, resizable: true, draggable: true, rotatable: true}
				})
				.data('fancy-product-designer');
				
				//get current price when product is created and update it when price changes
				$('#fpd')
				.bind('productCreate', function(evt){
					$('#thsirt-output').html('Click the "Checkout" button to see the returning object with all properties.');
					$('#thsirt-price').text(fpd.getPrice());
				})
				.bind('priceChange', function(evt, price, currentPrice) {
					$('#thsirt-price').text(currentPrice);
				});
				
				//button to print the product
				$('#print-button').click(function(){
					fpd.print();
					return false;
				});
				
				//button to print the product
				$('#checkout-button').click(function(){
					//get only editable elements
					var product = fpd.getProduct(true);
					var output = '';
					//check if product is an array (different views)
					if(product instanceof Array) {
						//loop through all views
						for(var i=0; i < product.length; ++i) {
							output += _getProductOutput(product[i]);
						}
					}
					else {
						//just a single product without views
						output = _getProductOutput(product);
					}
					
					$('#thsirt-output').html(output);
					
					return false;
				});
				
				//format a product object for the output panel
				function _getProductOutput(product) {
					var output = '<strong>Product:</strong> '+product.title;
					
					output += '<br /><strong>Elements:</strong>';
					output += '<p>';
					$(product.elements).each(function(i, elem) {
						output += '<strong>Title:</strong> ' + elem.title;
						output += '<br />';
						output += '<strong>Parameters:</strong><br />';
						for (var prop in elem.parameters) {
							output += prop + ": " + elem.parameters[prop] + ', ';
						}
						output = output.substring(0, output.length-2);
						output += '<br /><br />';
					});
					output += '</p>';
					return output;
				};
				
				$('#save_productA').click(function(){
					fpd.createImage();
					return false;
				});
				
				$('#fpd').bind('imageCreate', function(evt, canvas, base64Image) {
					$('#saveLoading').html('<div style="color:#c03b37; font-size:15px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Saving...</div>');
					$.post(site_url+"design_tshirt/saveDesignedImage", { base64_image: base64Image, isFrontImage:isFrontImage, iDesignedProductId:iDesignedProductId}, function(data) {
						iDesignedProductId = data;
						if(isFrontImage) {
							frontSaved = 1;
							$('#saveLoading').html('<div style="color:#c03b37; font-size:15px;">Front Image Saved</div>');
						}else{
							backSaved = 1;
							$('#saveLoading').html('<div style="color:#c03b37; font-size:15px;">Back Image Saved</div>');
						}
					});
				});
						
				$('#addtocart-button').click(function(){
					if (frontSaved == 0 || backSaved == 0) {
						$.fancybox($('#validateImageSave').html(),{
							'modal':false,
							'scrolling' : 'no',
						});
					}else{
						$('#cart-btn').html('<div style="color:#c03b37; font-size:18px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');
						$.post(site_url+"design_tshirt/addToCart", {iDesignedProductId:iDesignedProductId, prod_price: prod_price, prod_qty: prod_qty, prod_id:prod_id, prod_name:prod_name, prod_color_id:prod_color_id, prod_color:prod_color, prod_size_id:prod_size_id}, function(data) {
							$('#ajaxCart').html(data);
							if(data) {
								$('#cart-btn').html('<a id="addtocart-button" style="cursor: pointer;" class="uploadimg addcat">{/literal}{$LBL_DESIGN_PAGE_ADD_TO_CART}{literal}</a>');
							}
							$.fancybox($('#addToCartBox').html(),{
								'modal':false,
								'closeBtn' : false ,
								'scrolling' : 'no',
								'helpers': { 'overlay': { 'closeClick': false } }
							});
						});	
					}
					
				});
				
			});
				
			function showProductDiv() {
				$('#product_category_section').show();
				$('.fpd-design-selection').hide();
			}
			
			function showDesignDiv() {
				$('.fpd-design-selection').show();
				$('#product_category_section').hide();
			}
			
			$(window).bind("load", function() {
				$('.fpd-design-selection').hide();
				$('#product_category_section').show();
				$('#category_section').html($('#selectDesignCategory').html());
				$('#product_category_section').html($('.fpd-productmain-selection').html());
				$('.fpd-productmain-selection').html('');
				//showProductColors("{/literal}{$all_products[0]->iProductId}{literal}","{/literal}{$all_products[0]->vProductName}{literal}");
				//loadProductByCategory("{/literal}{$category_id}{literal}");
				$(".imageUploadDiv").hide();
				$("#main-container").show();
				$("#ajax-loader").hide();
				
				//var show_design_image_div = '{/literal}{$show_design_image_div}{literal}';
				if (show_design_image_div == 1) {
					$('#desg').click();
				}
			});
			var isFrontImage = 1;
			function testFrontImage(front) {
				if (front == 1) {
					isFrontImage = 1;
				}else{
					isFrontImage = 0;
				}
			}
			function callFun(title) {
				var arr = title.split('-');
				prod_color_id =arr[1];
				prod_color = arr[3];
				
				$("img[title='thumb-main-"+arr[1]+"-"+prod_id+"']").click();
			}
			
			function showProductColors(iProductId,vProductName) {
				//alert(prod_price);
				prod_id = iProductId;
				prod_name = vProductName;
				
				var colorFlag = 0;
				
				$('.detail_productClass').hide();
				$('#detail_product'+iProductId).show();
				
				$('#colorBox').find('span').each(function() {
					var imgTag = $(this).html();
					var nameAttr = ($(imgTag).attr('name'));
					var arr = nameAttr.split('-');
					if (arr[2] == iProductId){
						$(this).show();
				
						if(!colorFlag) {
							callFun(nameAttr);
							colorFlag++;
						}
					}else{
						$(this).hide();
					}
				});
				
				var sizeFlag=0;
				$('#sizeBox').find('option').each(function() {
					
					var productId = $(this).attr('class');
					if (productId == iProductId) {
						$(this).show();
						if (!sizeFlag){
							$(this).attr('selected','selected');
							//alert($(this).attr('value'));
							var sizeVal = $(this).attr('value');
							var arr = sizeVal.split('|');
							prod_price = parseFloat(arr[1]);
							//alert(prod_price);
							prod_qty = 1;
							$('#qtyText').val('1');
							$('#fPrice').html(prod_price*prod_qty);
							
							sizeFlag++;
						}
					}else{
						$(this).hide();
					}
				});
			}
			
			function loadDesignByCategory(categoryId){
				$('#fpd-clearfix_id').find('li').each(function() {
					var imgTag = $(this).html();
					var imgCategoryId = $(imgTag).attr('title');
					if (imgCategoryId != categoryId && categoryId != 0) {
						$(this).hide();
					}else{
						$(this).show();
					}
				});
			}
		
			function loadProductByCategory(categoryId){
				alert(categoryId);
				$('#fpd-clearfix_id_product').find('li').each(function() {
					var imgTag = $(this).html();
					var imgCategoryId = $(imgTag).attr('name');
					
					if (imgCategoryId != categoryId && categoryId != 0) {
						$(this).hide();
					}else{
						$(this).show();
					}
				});
				
				$('#selectProductCategory').find('option').each(function(){
					if($(this).attr('value') == categoryId){
						$(this).attr('selected','selected');
					}
				});
			}
		 
			$("#imageUploadDiv").fancybox({
				'overlayShow'	: true,
				'openEffect'	: 'elastic',
				'openSpeed'	: 250,
				'closeEffect'	: 'elastic',
				'closeSpeed'	: 250,
				'scrolling' : 'no',
				'helpers' : { 
					'overlay' : {'closeClick': false}
				}
			});
			
			$("#detail_productA").fancybox({
				'overlayShow'	: false,
				'openEffect'	: 'elastic',
				'openSpeed'	: 250,
				'closeEffect'	: 'elastic',
				'closeSpeed'	: 250,
				'scrolling' : 'no',
				'helpers' : { 
					'overlay' : {'closeClick': false}
				}
			});
			
			$(function(){
				$("#prod").click(function(){
				    $(this).attr('class','activetab');
				    $("#desg").attr('class','');
				});
				$("#desg").click(function(){
				    $(this).attr('class','activetab');
				    $("#prod").attr('class','');
				});
			});
			fancyProductDesign();
			$(document).ready(function() { 
				  $('#frmUploadImage').ajaxForm(function(data) {
					alert(data);
					$('#containterMainDiv').html(data);
					
					fancyProductDesign();
				  });
			   });
			
			
			function validateCustomImage() {
				if (!$('#vUploadImage').val()){
					return false;
				}else{
					$('#uploadLoader').html('<div style="color:#c03b37; font-size:16px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Uploading...</div>');
					return true;
				}
			}
			function changeSize(size_data) {
				var arr = size_data.split('|');
				var price = arr[1];
				prod_size_id =  arr[0];
				prod_price = price;
				$('#fPrice').html(price*prod_qty);
			}
			
			function changeQty(qty){
				if (qty == '') {
					qty =1;
				}
				prod_qty = qty;
				$('#fPrice').html(parseFloat(prod_price)*parseFloat(qty));
			}
			
			function uploadDesignImage() {
				$(".imageUploadDiv").slideToggle(500);
			}
			
		</script>
		{/literal}
		<div style="display: none;">
			<div id="addToCartBox">
				<div class="addToCartBox">
					<p style="font-size: 20px; text-align: center;">Product added to the cart</p>
					<a class="uploadimg addcat" href="{$site_url}checkout">Proceed to Checkout</a>
					<a class="uploadimg addcat" href="{$site_url}marketplace">Continue Shopping</a>	
				</div>
			</div>	
		</div>
		<div style="display: none;">
			<div id="validateImageSave">
				<p style="font-size: 20px; color: red;">It seems like you have not saved your desined front and back tshirt yet.</p>
				<p>Please, follow the below steps and then add your product to the cart</p>
				<ul>
					<li>1. Select the product.</li>
					<li>2. Select the color from the color pallet.</li>
					<li>3. Add the design & text on your tshirt.</li>
					<li>4. Click on the Save button to save the Front Image.</li>
					<li>5. Select the back image for the same tshirt.</li>
					<li>6. Design the back image. (This is optional)</li>
					<li>7. Click on the Save button to save the Back Image.</li>
					<li>8. Select Size & Qty.</li>
					<li>9. Click on the Add to cart button to add your selection to the cart.</li>
				</ul>
			</div>	
		</div>
		
		<div class="imageUploadDiv" id="imageUploadDivID" style="background:#fff; padding:10px; height: 300px; width:300px; display: none;">
			<form></form>
			<p style="font-size: 16px;">UPLOAD YOUR DESIGN</p>
			<form id="frmUploadImage" name="frmUploadImage" enctype="multipart/form-data" method="post" action="{$site_url}design_tshirt/upload_art">
				<input type="file" name="vUploadImage" id="vUploadImage">
				<br><br><p style="color: green;">Please note:</p>
				<p>For best printing results, please insure that your image meets our minimum size requirements.</p>
				<p>File format: .jpg, .gif, .png</p>
				<p>We recommend a minimum size of 1000 x 1000 pixels.</p>
				<div id="uploadLoader"><input type="submit" class="mailsubmit uploadimg" value="Upload" style="float: left !important"></div>
			</form>
		</div>

		<div id="containterMainDiv">
			<div class="container_16 main2 padding50">
			
			<div id="ajax-loader" style="padding-top: 150px; text-align: center;height: 270px;"><img src="{$site_url}public/front-end/images/ajax-loader-designpage.gif"></div>
			<div id="main-container" class="container" style="display: none;">
				<div id="content">
				</div>
				<div style="display: none;">
					<div class="fpd-productmain-selection" style="display: none;">					
						<div id="selectProductCategory">
							<select onchange="loadProductByCategory(this.value);">
								<option value="0">All Products</option>
								{section name=i loop=$product_category}
								<option value="{$product_category[i].iCategoryId}">{$product_category[i].vCategory}</option>
								{/section}
							</select>
						</div>
						<a class="fpd-scroll-up ui-state-default" href="#"><span class="ui-icon ui-icon-carat-1-n"></span></a>
						
						<div class="prdboxside">
							<ul id="fpd-clearfix_id_product" class="fpd-clearfix">
								{section name=i loop=$all_products}
								<li>
									<img title="{$all_products[i]->vProductName}" src="{$upload_path}product/{$all_products[i]->iProductId}/193X198_{$all_products[i]->vImage}" width="100px" height="125px" onclick="showProductColors('{$all_products[i]->iProductId}','{$all_products[i]->vProductName}');" name="{$all_products[i]->iCategoryId}">
								</li>
								{/section}
							</ul>
						</div>
						<!--
						<div class="prdboxside">
							<ul id="fpd-clearfix_id_product" class="fpd-clearfix">
								<li><img title="Product" src="http://192.168.1.12/Izishirt/uploads/product/1/1/front-5.png" width="75px" height="85px" onclick="showProductColors('1','Product');" name="1"></li>
								<li><img title="Product" src="http://192.168.1.12/Izishirt/uploads/product/1/2/front-1.png" width="75px" height="85px" onclick="showProductColors('1','Product');" name="1"></li>
								<li><img title="Product" src="http://192.168.1.12/Izishirt/uploads/product/1/3/front-2.png" width="75px" height="85px" onclick="showProductColors('1','Product');" name="1"></li>
								<li><img title="Product" src="http://192.168.1.12/Izishirt/uploads/product/1/4/front-3.png" width="75px" height="85px" onclick="showProductColors('1','Product');" name="1"></li>
								<li><img title="Product" src="http://192.168.1.12/Izishirt/uploads/product/1/5/front-4.png" width="75px" height="85px" onclick="showProductColors('1','Product');" name="1"></li>
							</ul>
						</div>
						-->
						<a class="fpd-scroll-down ui-state-default" href="#"><span class="ui-icon ui-icon-carat-1-s"></span></a>
					</div>
				</div>
				
				<div id="fpd">		
					<div class="fpd-product" name="front" title="thumb-main--1" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/1/front-5.png">
						<img src="http://192.168.1.12/Izishirt/uploads/product/1/1/341X418_front-5.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
						<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/1/back-5.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/1/341X418_back-5.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
						</div>
						<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/3/341X418_front-2.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/3/341X418_front-2.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
						</div>
						<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/3/341X418_back-2.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/3/341X418_back-2.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
						</div>
					</div>
									<div class="fpd-product" name="front" title="thumb-main-1-1" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/2/front-1.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/2/341X418_front-1.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/2/back-1.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/1/2/341X418_back-1.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-6-1" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/3/front-2.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/3/341X418_front-2.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/3/back-2.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/1/3/341X418_back-2.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main--1" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/4/front-3.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/4/341X418_front-3.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/4/back-3.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/1/4/341X418_back-3.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main--1" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/5/front-4.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/1/5/341X418_front-4.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/1/5/back-4.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/1/5/341X418_back-4.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-3-2" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/2/6/front-6.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/2/6/341X418_front-6.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/2/6/back-6.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/2/6/341X418_back-6.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-6-2" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/2/7/front2.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/2/7/341X418_front2.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/2/7/back-2.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/2/7/341X418_back-2.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-6-3" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/3/8/front-5.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/3/8/341X418_front-5.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/3/8/back5.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/3/8/341X418_back5.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-5-4" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/4/9/front-2.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/4/9/341X418_front-2.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/4/9/back-2.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/4/9/341X418_back-2.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main--4" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/4/10/front-1.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/4/10/341X418_front-1.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/4/10/back-1.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/4/10/341X418_back-1.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main--5" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/5/11/front-4.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/5/11/341X418_front-4.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/5/11/back-4.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/5/11/341X418_back-4.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-3-5" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/5/12/front-3.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/5/12/341X418_front-3.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/5/12/back-3.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/5/12/341X418_back-3.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main-6-5" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/5/13/front-2.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/5/13/341X418_front-2.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/5/13/back-2.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/5/13/341X418_back-2.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main--6" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/6/14/front4.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/6/14/341X418_front4.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/6/14/back-4.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/6/14/341X418_back-4.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
									<div class="fpd-product" name="front" title="thumb-main--6" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/6/15/front3.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/6/15/341X418_front3.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/6/15/back-3.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/6/15/341X418_back-3.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
						<div class="fpd-product" name="front" title="thumb-main--4" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/4/16/front-4.png">
							<img src="http://192.168.1.12/Izishirt/uploads/product/4/16/341X418_front-4.png" title="Base Front" data-parameters='{literal}{"x": 123, "y": 81, "price": 20}{/literal}' />
							<div class="fpd-product" name="back" title="Back" data-thumbnail="http://192.168.1.12/Izishirt/uploads/product/4/16/back-4.png">
								<img src="http://192.168.1.12/Izishirt/uploads/product/4/16/341X418_back-4.png" title="Base Back" data-parameters='{literal}{"x": 123, "y": 81, "price": 40}{/literal}' />
							</div>
						</div>
					
					<div id="selectDesignCategory" style="display: none;">
						<select onchange="loadDesignByCategory(this.value);">
							<option value="0">All Design Images</option>
						</select>
						&nbsp;<a class="mailsubmit uploadimg" id="imageUploadDiv" style="cursor: pointer;" href="#imageUploadDivID">Upload Image</a>
					</div>
					
					<div class="fpd-design">
						<img src="http://192.168.1.12/Izishirt/uploads/design_images/125/195x131_black.jpg" title="20" data-parameters='{literal}{"x": 215, "y": 200, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10}{/literal}' />
						<img src="http://192.168.1.12/Izishirt/uploads/design_images/124/195x131_men_women.png" title="17" data-parameters='{literal}{"x": 215, "y": 200, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10}{/literal}' />
						<img src="http://192.168.1.12/Izishirt/uploads/design_images/123/195x131_crown.png" title="35" data-parameters='{literal}{"x": 215, "y": 200, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10}{/literal}' />
						<img src="http://192.168.1.12/Izishirt/uploads/design_images/122/195x131_heart_circle.png" title="56" data-parameters='{literal}{"x": 215, "y": 200, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10}{/literal}' />
						<img src="http://192.168.1.12/Izishirt/uploads/design_images/121/195x131_heart_blur.png" title="56" data-parameters='{literal}{"x": 215, "y": 200, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10}{/literal}' />
					</div>
				</div>
				
				<div class="colboxmain">
				<div id="colorBox">
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--1-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-1-1-Black" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/1/000000.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-6-1-Green" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/6/008000.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--1-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--1-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
		
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-3-2-Grey" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/3/808080.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-6-2-Green" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/6/008000.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-6-3-Green" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/6/008000.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-5-4-Yellow" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/5/FFFF00.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--4-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--5-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-3-5-Grey" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/3/808080.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb-6-5-Green" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color/6/008000.jpg" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--6-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--6-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
						<span class="colorbox" style="display: none;"><img onclick="callFun(this.name);" name="thumb--4-" width="25px" height="25px" src="http://192.168.1.12/Izishirt/uploads/color//" ></span>
				</div>
					
				<div id="sizeBox">
					<span class="sizehd">{$LBL_DESIGN_PAGE_SIZE}:</span> 
					<select onchange="changeSize(this.value);">
						<option class="1" value="7|300.00">XL</option>
						<option class="1" value="2|200.00">S</option>
						<option class="1" value="3|200.00">M</option>
						<option class="1" value="5|100.00">XXL</option>
						<option class="2" value="7|32.00">XL</option>
						<option class="2" value="2|23.00">S</option>
						<option class="2" value="3|13.00">M</option>
						<option class="2" value="5|100.00">XXL</option>
						<option class="3" value="7|50.00">XL</option>
						<option class="3" value="2|40.00">S</option>
						<option class="3" value="3|35.00">M</option>
						<option class="3" value="5|25.00">XXL</option>
						<option class="4" value="7|60.00">XL</option>
						<option class="4" value="2|50.00">S</option>
						<option class="4" value="3|40.00">M</option>
						<option class="4" value="5|30.00">XXL</option>
						<option class="5" value="7|60.00">XL</option>
						<option class="5" value="2|40.00">S</option>
						<option class="5" value="3|50.00">M</option>
						<option class="5" value="5|50.00">XXL</option>
						<option class="6" value="7|40.00">XL</option>
						<option class="6" value="3|30.00">M</option>
						<option class="6" value="5|26.00">XXL</option>
						<option class="6" value="6|25.00">XXXL</option>
					</select>
					<div id="qtyBox" style="margin-top:5px;">
						<span class="sizehd">Qty:</span> 
						<input onkeyup="changeQty(this.value);" style="width:93px; height:23px;" type="text" value="1" id="qtyText" name="qtyText">	
					</div>
				</div>
				<div class="cart-checkout-btn">
					<div class="add_to_cart_btn" id="cart-btn"><a id="addtocart-button" style="cursor: pointer;" class="uploadimg addcat">Add to cart</a></div>
					<div class="add_to_cart_btn" style="margin-top:6px;"><a href="{$site_url}checkout" id="checkout-button1" style="cursor: pointer;" class="uploadimg addcat">Proceed to Checkout</a></div>
				</div>
				<div class="prod_price">$ <span id='fPrice'>25.25</span></div>
				</div>
				  </div>
			</div>
			<div id="container"></div>
		</div>
		</div>
		
		<div class="clear"></div>
		</div>
		
	</div>	
</div>



{include file="footer.tpl"}