{include file="header.tpl" title="Example Smarty Page"}

<link rel="stylesheet" href="{$custom_tshirt_path}font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/plugins.css" />
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/jquery.fancyProductDesigner.css" />
<link rel="stylesheet" type="text/css" href="{$custom_tshirt_path}css/jquery.fancyProductDesigner-fonts.css" />

<script type="text/javascript" src="{$fancybox_path}lib/jquery-1.8.0.min.js"></script>
<script src="{$custom_tshirt_path}js/fabric.js" type="text/javascript"></script>
<script src="{$custom_tshirt_path}js/plugins.min.js" type="text/javascript"></script>
{literal}
	<script type="text/javascript">
		var designerBaseUrl = '{/literal}{$site_url}{literal}design_studio/';
	</script>
{/literal}
<script src="{$custom_tshirt_path}js/jquery.fancyProductDesigner.min.js" type="text/javascript"></script>

{literal}
<script type="text/javascript">
	    jQuery(document).ready(function(){
	    
	    	var yourDesigner = $('#clothing-designer').fancyProductDesigner({
	    		editorMode: false, 
	    		fonts: ['Arial', 'Fearless', 'Helvetica', 'Times New Roman', 'Verdana', 'Geneva'], 
	    		customTextParamters: {x: 210, y: 250, colors: false, removable: true, resizable: true, draggable: true, rotatable: true}
	    	}).data('fancy-product-designer');
	    	
	    	//print button
			$('#print-button').click(function(){
				yourDesigner.print();
				return false;
			});
			
			//create an image
			$('#image-button').click(function(){
				var image = yourDesigner.createImage();
				return false;
			});
			
			//create a pdf with jsPDF
			$('#pdf-button').click(function(){
				var image = new Image();
				image.src = yourDesigner.getProductDataURL('jpeg');
				image.onload = function() {
					var doc = new jsPDF();
					doc.addImage(this.src, 'JPEG', 0, 0, this.width * 0.2, this.height * 0.2);
					doc.save('Product.pdf');
				}		
				return false;
			});
			
			//checkout button with getProduct()
			$('#checkout-button').click(function(){
				var product = yourDesigner.getProduct();
				console.log(product);
				return false;
			});
			
			//event handler when the price is changing
			$('#clothing-designer')
			.bind('priceChange', function(evt, price, currentPrice) {
				$('#thsirt-price').text(currentPrice);
			});
		
			//recreate button
			$('#recreation-button').click(function(){
				var fabricJSON = JSON.stringify(yourDesigner.getFabricJSON());
				$('#recreation-form input:first').val(fabricJSON).parent().submit();
				return false;
			});
			
			//click handler for input upload
			$('#upload-button').click(function(){
				$('#design-upload').click();
				return false;
			});
			
			//save image on webserver
			$('#save-image-php').click(function() {
				$.post( "php/save_image.php", { base64_image: yourDesigner.getProductDataURL()} );
			});
			
			//send image via mail
			$('#send-image-mail-php').click(function() {
				$.post( "php/send_image_via_mail.php", { base64_image: yourDesigner.getProductDataURL()} );
			});
			
			//upload image
			/*document.getElementById('design-upload').onchange = function (e) {
				if(window.FileReader) {
					var reader = new FileReader();
			    	reader.readAsDataURL(e.target.files[0]); 
			    	reader.onload = function (e) {
			    		var image = new Image;
			    		image.src = e.target.result;
			    		image.onload = function() {
				    		yourDesigner.addElement('image', e.target.result, 'my custom design', {colors: $('#colorizable').is(':checked') ? '#000000' : false, zChangeable: true, removable: true, draggable: true, resizable: true, rotatable: true, x: 50, y: 50});  	
			    		};	    		               
					};
				}
				else {
					alert('FileReader API is not supported in your browser, please use Firefox, Safari, Chrome or IE10!')
				}
			};*/
	    });
    </script>	
    
{/literal}




<section id="midcontentpart">
	

	<div class="mainpart">
		
		<div id="main-container" class="container">
          	<div id="clothing-designer">
          		<div class="fpd-product" title="Shirt Front" data-thumbnail="{$custom_tshirt_path}images/yellow_shirt/front/preview.png">
	    			<img src="{$custom_tshirt_path}images/yellow_shirt/front/base.png" title="Base" data-parameters='{literal}{"x": 325, "y": 329, "colors": "#990000", "price": 20}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/yellow_shirt/front/shadows.png" title="Shadow" data-parameters='{literal}{"x": 325, "y": 329}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/yellow_shirt/front/body.png" title="Hightlights" data-parameters='{literal}{"x": 322, "y": 137}{/literal}' />
			  		<span title="Any Text" data-parameters='{literal}{"boundingBox": "Base", "x": 326, "y": 232, "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "colors": "#000000"}{/literal}' >Default Text</span>
			  		<div class="fpd-product" title="Shirt Back" data-thumbnail="{$custom_tshirt_path}images/yellow_shirt/back/preview.png">
		    			<img src="{$custom_tshirt_path}images/yellow_shirt/back/base.png" title="Base" data-parameters='{literal}{"x": 317, "y": 329, "colors": "Base", "price": 20}{/literal}' />
		    			<img src="{$custom_tshirt_path}images/yellow_shirt/back/body.png" title="Hightlights" data-parameters='{literal}{"x": 333, "y": 119}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/yellow_shirt/back/shadows.png" title="Shadow" data-parameters='{literal}{"x": 318, "y": 329}{/literal}' />
					</div>
				</div>
          		<div class="fpd-product" title="Sweater" data-thumbnail="{$custom_tshirt_path}images/sweater/preview.png">
	    			<img src="{$custom_tshirt_path}images/sweater/basic.png" title="Base" data-parameters='{literal}{"x": 332, "y": 311, "colors": "#D5D5D5,#990000,#cccccc", "price": 20}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/sweater/highlights.png" title="Hightlights" data-parameters='{literal}{"x": 332, "y": 311}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/sweater/shadow.png" title="Shadow" data-parameters='{literal}{"x": 332, "y": 309}{/literal}' />
				</div>
				<div class="fpd-product" title="Scoop Tee" data-thumbnail="{$custom_tshirt_path}images/scoop_tee/preview.png">
	    			<img src="{$custom_tshirt_path}images/scoop_tee/basic.png" title="Base" data-parameters='{literal}{"x": 314, "y": 323, "colors": "#98937f, #000000, #ffffff", "price": 15}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/scoop_tee/highlights.png" title="Hightlights" data-parameters='{literal}{"x":308, "y": 316}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/scoop_tee/shadows.png" title="Shadow" data-parameters='{literal}{"x": 308, "y": 316}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/scoop_tee/label.png" title="Label" data-parameters='{literal}{"x": 314, "y": 140}{/literal}' />
				</div>
				<div class="fpd-product" title="Hoodie" data-thumbnail="{$custom_tshirt_path}images/hoodie/preview.png">
	    			<img src="{$custom_tshirt_path}images/hoodie/basic.png" title="Base" data-parameters='{literal}{"x": 313, "y": 322, "colors": "#850b0b", "price": 40}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/hoodie/highlights.png" title="Hightlights" data-parameters='{literal}{"x": 311, "y": 318}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/hoodie/shadows.png" title="Shadow" data-parameters='{literal}{"x": 311, "y": 321}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/hoodie/zip.png" title="Zip" data-parameters='{literal}{"x": 303, "y": 216}{/literal}' />
				</div>
				<div class="fpd-product" title="Shirt" data-thumbnail="{$custom_tshirt_path}images/shirt/preview.png">
	    			<img src="{$custom_tshirt_path}images/shirt/basic.png" title="Base" data-parameters='{literal}{"x": 327, "y": 313, "colors": "#6ebed5", "price": 10}{/literal}' />
	    			<img src="{$custom_tshirt_path}images/shirt/collar_arms.png" title="Collars & Arms" data-parameters='{literal}{"x": 326, "y": 217, "colors": "#000000"}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/shirt/highlights.png" title="Hightlights" data-parameters='{literal}{"x": 330, "y": 313}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/shirt/shadow.png" title="Shadow" data-parameters='{literal}{"x": 327, "y": 312}{/literal}' />
			  		<span title="Any Text" data-parameters='{literal}{"boundingBox": "Base", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "colors": "#000000"}{/literal}' >Default Text</span>
				</div>
				<div class="fpd-product" title="Short" data-thumbnail="{$custom_tshirt_path}images/shorts/preview.png">
	    			<img src="{$custom_tshirt_path}images/shorts/basic.png" title="Base" data-parameters='{literal}{"x": 317, "y": 332, "colors": "#81b5eb", "price": 15}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/shorts/highlights.png" title="Hightlights" data-parameters='{literal}{"x": 318, "y": 331}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/shorts/pullstrings.png" title="Pullstrings" data-parameters='{literal}{"x": 307, "y": 195, "colors": "#ffffff"}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/shorts/midtones.png" title="Midtones" data-parameters='{literal}{"x": 317, "y": 332}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/shorts/shadows.png" title="Shadow" data-parameters='{literal}{"x": 320, "y": 331}{/literal}' />
				</div>
				<div class="fpd-product" title="Basecap" data-thumbnail="{$custom_tshirt_path}images/cap/preview.png">
	    			<img src="{$custom_tshirt_path}images/cap/basic.png" title="Base" data-parameters='{literal}{"x": 318, "y": 311, "colors": "#ededed", "price": 5}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/cap/highlights.png" title="Hightlights" data-parameters='{literal}{"x": 309, "y": 300}{/literal}' />
			  		<img src="{$custom_tshirt_path}images/cap/shadows.png" title="Shadows" data-parameters='{literal}{"x": 306, "y": 299}{/literal}' />
				</div>
		  		<div class="fpd-design">
		  			<div class="fpd-category" title="Swirls">
			  			<img src="{$custom_tshirt_path}images/designs/swirl.png" title="Swirl" data-parameters='{literal}{"zChangeable": true, "x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10, "boundingBox": "Base", "scale": 1}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/swirl2.png" title="Swirl 2" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/swirl3.png" title="Swirl 3" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/heart_blur.png" title="Heart Blur" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#bf0200", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/converse.png" title="Converse" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/crown.png" title="Crown" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/men_women.png" title="Men hits Women" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "boundingBox": "Base"}{/literal}' />
		  			</div>
		  			<div class="fpd-category" title="Retro">
			  			<img src="{$custom_tshirt_path}images/designs/retro_1.png" title="Retro One" data-parameters='{literal}{"x": 210, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.25, "price": 7, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/retro_2.png" title="Retro Two" data-parameters='{literal}{"x": 193, "y": 180, "colors": "#ffffff", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.46, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/retro_3.png" title="Retro Three" data-parameters='{literal}{"x": 240, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.25, "price": 8, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/heart_circle.png" title="Heart Circle" data-parameters='{literal}{"x": 240, "y": 200, "colors": "#007D41", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.4, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/swirl.png" title="Swirl" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/swirl2.png" title="Swirl 2" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base"}{/literal}' />
				  		<img src="{$custom_tshirt_path}images/designs/swirl3.png" title="Swirl 3" data-parameters='{literal}{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}{/literal}' />
				  	</div>					  		
		  		</div>
		  	</div>

    	</div>

	</div>
</section>





{include file="footer.tpl"}