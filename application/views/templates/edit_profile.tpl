{include file="header.tpl"}		
			<div class="main-container col2-right-layout">			 
			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
						            <div class="main container">

                <div class="main-inner">
                    
                    <div class="col-main">
                                                <div class="account-create">
    			<h2 class="pagetital subtit">Edit Profile</h2>
			<div style="clear:both;"></div>
            <div class="ordleftpart">
		  		<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Personal Information</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>First Name</label>
    <div class="input-box">
        <input type="text" id="vFirstName" name="Data[vFirstName]" value="{if $operation neq 'add'}{$user_data[0]->vFirstName}{/if}" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="firstNameDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Last Name</label>
    <div class="input-box">
        <input type="text" id="vLastName" name="Data[vLastName]" value="{if $operation neq 'add'}{$user_data[0]->vLastName}{/if}" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="lastNameDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Telephone</label>
    <div class="input-box">
        <input type="text" id="vPhone" name="Data[vPhone]" value="{if $operation neq 'add'}{$user_data[0]->vPhone}{/if}" title="Telephone" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="phoneDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em></em>Fax</label>
    <div class="input-box">
        <input type="text" id="vFax" name="Data[vFax]" value="{if $operation neq 'add'}{$user_data[0]->vFax}{/if}" title="Fax" maxlength="255" class="input-text span12 required-entry"  />
    </div>
</div>
</div>
                </li>
                <li>
                    <label for="email_address" class="required"><em>*</em>Email Address</label>
                    <div class="input-box">
                        <input type="text" name="Data[vEmail]" id="vEmail" value="{if $operation neq 'add'}{$user_data[0]->vEmail}{/if}" title="Email Address" class="input-text validate-email required-entry span12" />
                    </div>
		    <div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
                </li>
                                 <li class="control">
                    <div class="input-box">
                        <input type="checkbox" name="Data['vNewsleter']" title="Sign Up for Newsletter" value="" {if $newsletter[0]['iUserId'] neq '' and $newsletter[0]['eStatus'] eq 'Active'}checked{/if}  class="checkbox" id="vNews"/>
                    </div>
                    <label for="is_subscribed">Sign Up for Newsletter</label>
                </li>
                                
                                                            </ul>
</div>
<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Your Address</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
   
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Address</label>
    <div class="input-box">
        <input type="text" id="tAddress" name="Data[tAddress]" value="{if $operation neq 'add'}{$user_data[0]->tAddress}{/if}" title="Address" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="addressDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>City</label>
    <div class="input-box">
        <input type="text" id="vCity" name="Data[vCity]" value="{if $operation neq 'add'}{$user_data[0]->vCity}{/if}" title="City" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="cityDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Post Code</label>
    <div class="input-box">
        <input type="text" id="vZipCode" name="Data[vZipCode]" value="{if $operation neq 'add'}{$user_data[0]->vZipCode}{/if}" title="Post Code" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="zipcodeDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
                <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Select Country</label>
    <div class="input-box">
    		<select name="" class="input-text span12 required-entry" id="iCountryId" name"Data[iCountryId]" onchange="getStates(this.value);" title="Country">
		 <option value="">--Select Country--</option>
		 {section name=i loop=$db_country}
    			<option value='{$db_country[i]->iCountryId}' {if $operation eq 'edit'}{if $user_data[0]->iCountryId eq $db_country[i]->iCountryId}selected{/if}{/if}>{$db_country[i]->vCountry}</option>
    		 {/section}
    		</select>
        
    </div>
     <div class="validation-advice" id="countryDiv" style="display:none; float: left;"></div>
<input type="hidden" name="iUserId" id="iUserId" value="{$user_data[0]->iUserId}"> 
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Select State</label>
    <div class="input-box">
        <select id="states" name="Data[iStateId]"  title="State" class="input-text span12 required-entry"  >
	 <option>--Select State--</option>
    	     {section name=i loop=$db_state}
		 <option value='{$db_state[i]->iStateId}'{if $operation eq 'edit'} {if $user_data[0]->iStateId eq $db_state[i]->iStateId}selected{/if}{/if}>{$db_state[i]->vState}
	     {/section}
		 </option>
    	</select>
    </div>
    <div class="validation-advice" id="statesDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
   
                                                            </ul>
</div>
<div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="{$site_url}" class="back-link"><small>&laquo; </small>Back</a></p>
        <button type="button" title="Submit" class="button" onclick=" return EditProfile();"><span><span>Submit</span></span></button>
    </div>
		  </div>
{include file="right_myAccount.tpl"}
{literal}
<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
    </script>
</div>
{/literal}
		    </div>
					
                </div>				
				
									
								
            </div>
        </div>
			 
			 
        </div>
		
		<!--************mid-part-end************-->
<div class="botline">
		<div class="linebg">&nbsp;</div>
		</div>
{include file="footer.tpl"}


{literal}
<script type="text/javascript">
         
function getStates(icountryid)
{
  
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}
</script>

<script type="text/javascript">
  
function EditProfile()
{
    var iUserId = $('#iUserId').val();
    var extra = '';
    var firstname =  $('#vFirstName').val();   
    var lastname =  $('#vLastName').val();
    var email = $('#vEmail').val();
    var phone = $('#vPhone').val();
    var fax = $('#vFax').val();
    var address = $('#tAddress').val();
    var city = $('#vCity').val();
    var country = $('#iCountryId').val();
    var state = $('#states').val();
    var zip = $('#vZipCode').val();
    var password = $('#vPassword').val();
    var resetpass = $('#vPasswordAgain').val();    
    var newsletter  = $('#vNews').val();
 
    if ($('#vNews').is(":checked"))
    {
      newsletter = 'Active';
    }else{
        newsletter = 'Inactive';
    }
if (document.getElementById('vFirstName').value == "" || document.getElementById('vLastName').value == "" ||
	document.getElementById('vEmail').value == "" || document.getElementById('vPhone').value == "" ||
	document.getElementById('tAddress').value == "" || document.getElementById('vCity').value == "" ||
	document.getElementById('iCountryId').value == "" || document.getElementById('states').value == "" ||
	document.getElementById('vZipCode').value == "") {
	$("#vFirstName").addClass("borderValidations");
	$('#firstNameDiv').html('This is a required field.');
	$('#firstNameDiv').show();
	$("#vLastName").addClass("borderValidations");
	$('#lastNameDiv').html('This is a required field.');
	$('#lastNameDiv').show();
	$("#vPhone").addClass("borderValidations");
	$('#phoneDiv').html('This is a required field.');
	$('#phoneDiv').show();
	$("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('This is a required field.');
	$('#emailDiv').show();
        $("#tAddress").addClass("borderValidations");
	$('#addressDiv').html('This is a required field.');
	$('#addressDiv').show();
	$("#vCity").addClass("borderValidations");
	$('#cityDiv').html('This is a required field.');
	$('#cityDiv').show();
	$("#vZipCode").addClass("borderValidations");
	$('#zipcodeDiv').html('This is a required field.');
	$('#zipcodeDiv').show();
	$("#iCountryId").addClass("borderValidations");
	$('#countryDiv').html('This is a required field.');
	$('#countryDiv').show();
	$("#states").addClass("borderValidations");
	$('#statesDiv').html('This is a required field.');
	$('#statesDiv').show();	
	//return false;		 
    }    
    var validate = true;
    if( $('#vFirstName').val() == ''){
	$("#vFirstName").addClass("borderValidations");
	$('#firstNameDiv').html('This is a required field.');
	$('#firstNameDiv').show();
	validate = false;
    }
    else{
      $('#firstNameDiv').hide();
      $("#vFirstName").removeClass("borderValidations");
     }
        
    if( $('#vLastName').val() == ''){
        $("#vLastName").addClass("borderValidations");
	$('#lastNameDiv').html('This is a required field.');
	$('#lastNameDiv').show();
	validate = false;
    }
    
    else{
      $('#lastNameDiv').hide();
      $("#vLastName").removeClass("borderValidations");
    }
    
    if( $('#vPhone').val() == ''){
        $("#vPhone").addClass("borderValidations");
	$('#phoneDiv').html('This is a required field.');
	$('#phoneDiv').show();
	validate = false;
    }   
    else{
      $('#phoneDiv').hide();
      $("#vPhone").removeClass("borderValidations");
    }    
    var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var isvalid = emailRegexStr.test(email);
    if( $('#vEmail').val() == ''){
        $("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('This is a required field.');
	$('#emailDiv').show();
	validate = false;
    }
    else if (!isvalid) {
        $("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('Enter Your Email Address like demo@doman.com');
	$('#emailDiv').show();
	validate = false;
        }
   else{
      $('#emailDiv').hide();
      $("#vEmail").removeClass("borderValidations");
    }
    
    
    if( $('#tAddress').val() == ''){
        $("#tAddress").addClass("borderValidations");
	$('#addressDiv').html('This is a required field.');
	$('#addressDiv').show();
	validate = false;
       // $('#addressDiv').html('Enter Your Address'); $('#addressDiv').show(); return false;
    }
  else{
      $('#addressDiv').hide();
      $("#tAddress").removeClass("borderValidations");
    }
    
    if( $('#vCity').val() == ''){
        $("#vCity").addClass("borderValidations");
	$('#cityDiv').html('This is a required field.');
	$('#cityDiv').show();
	validate = false;
       //$('#cityDiv').html('Enter Your City'); $('#cityDiv').show(); return false;
    }
    else{
      $('#cityDiv').hide();
      $("#vCity").removeClass("borderValidations");
    }
    
    
    if( $('#vZipCode').val() == ''){
       $("#vZipCode").addClass("borderValidations");
	$('#zipcodeDiv').html('This is a required field.');
	$('#zipcodeDiv').show();
	validate = false;
       //$('#zipcodeDiv').html('Enter Your Zipcode'); $('#zipcodeDiv').show(); return false;
    }
    else{
      $('#zipcodeDiv').hide();
      $("#vZipCode").removeClass("borderValidations");
    }
    
    if( $('#iCountryId').val() == ''){
         $("#iCountryId").addClass("borderValidations");
	$('#countryDiv').html('This is a required field.');
	$('#countryDiv').show();
	validate = false;
       //$('#countryDiv').html('Select Country'); $('#countryDiv').show(); return false;
    }
    else{
      $('#countryDiv').hide();
      $("#iCountryId").removeClass("borderValidations");
    }
    
    if( $('#states').val() == ''){
        $("#states").addClass("borderValidations");
	$('#statesDiv').html('This is a required field.');
	$('#statesDiv').show();
	validate = false;
       //$('#statesDiv').html('Select State'); $('#statesDiv').show(); return false;
    }
    else{
      $('#statesDiv').hide();
      $("#states").removeClass("borderValidations");
    }
    if (validate)
    {

          extra+='?vFirstName='+firstname;
	  extra+='&iUserId='+iUserId;
	  extra+='&vLastName='+lastname;
          extra+='&vEmail='+email;
	  extra+='&vPhone='+phone;
	  extra+='&fax='+fax;
          extra+='&tAddress='+address;
	  extra+='&vCity='+city;
          extra+='&zip='+zip;
          extra+='&iCountryId='+country;
          extra+='&iStateId='+state;
	  extra+='&vNewsletter='+newsletter; 
          
	  var url = site_url + 'myAccount/edit_profile';
	  var pars = extra;
	 
	  $.post(url+pars,
          function(data) {
	  if(data == 'success'){
              window.location = site_url+'myAccount/myDashboard';
            }
	});
	  return true;
    }
    else
    {
	return false;
    }
}
</script>
{/literal}