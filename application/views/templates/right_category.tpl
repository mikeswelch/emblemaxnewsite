   <div class="ordrightpart">
    		<div class="block mt-smartmenu" style="border:1px solid #EAEAEA; padding:10px;">
		<div class="block-title">
			<strong><span>Category</span></strong>
		</div>
		<div class="title-divider">
			<span>&nbsp;</span>
		</div>
		<div class="block-content">
				<script type="text/javascript">
					$mtkb(document).ready(function(){	
						// applying the settings
						$mtkb("#mt-accordion li.active span.head").addClass("selected");
						$mtkb('#mt-accordion').Accordion({
							active: 'span.selected',
							header: 'span.head',
							alwaysOpen: false,
							animated: true,
							showSpeed: 400,
							hideSpeed: 800,
							event: 'click'
						});
					});	
				</script>
				
				
<ul id="mt-accordion" class="clearfix">
{if $categories|@count gt 0}
{section name=i loop=$categories}
	     {if $categories[i]->iParentId eq '0'}
	     <li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
                            <a href="#"><span>{$categories[i]->vCategory}</span></a><span class="head"><a href="#" style="float:right;"></a></span>
			  <ul class="level0">
				       {section name=j loop=$categories}
				       {if $categories[j]->iParentId neq '0' AND $categories[j]->iParentId eq $categories[i]->iCategoryId}
				       <li class="level1 nav-categories-footwear-man">
						    <a href="#"><span>{$categories[j]->vCategory}</span></a>
				       </li>
				       {/if}
				       {/section}
			  </ul>
	     </li>
	     {/if}
{/section}
{/if}
</ul>				

    </div> <!-- end class=block-content -->
	</div>
    </div>

{literal}
<script>
    var chId = 0;
    function showChild(childId, parentId){
        if(chId != parentId){
            chId = parentId;
            $(".childCls").hide();
            $("#child"+childId).slideDown('3000');
                
        }
        
    }
</script>
{/literal}