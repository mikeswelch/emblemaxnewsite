{include file="header.tpl"}
<div class="main-container col2-right-layout">
  
			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
			 <div class="main container">
			<div class="slideshow_static"></div>			
		       
			 <div class="main-inner">
                         {if $msg}
			 <div class="error_valid"><div id="error_msg" style="color:red;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;">{$msg}</div></div>			
			 {else}
			 <div class="error_valid"></div> 
			 {/if}
                    <div class="col-main">
<div class="account-create" style="width: 75%; float: left;">
    			<h2 class="pagetital">Change Password</h2>
			<div style="clear:both;"></div>
            <form action="#" method="post" id="form-validate" >
        <div class="fieldset">
			 
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Change Password</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
<div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Your Password</label>
    <div class="input-box">
        <input type="password" id="vPassword" name="Data[vPassword]" value="" title="Your Password" class="input-text span12 required-entry"  />
    </div>
     <div class="validation-advice" id="passNameDiv" style="display:none; float: left;"></div>
</div>
</div>
 </li>

<li class="fields">
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Your New Password</label>
    <div class="input-box">
        <input type="password" id="vNewPassword" name="Data[vNewPassword]" value="" title="Your New Password"  class="input-text span12 required-entry"  />
    </div>
     <div class="validation-advice" id="newpassNameDiv" style="display:none; float: left;"></div>
</div>
</li>
                <li class="fields">
                       <div class="customer-name">
 
<div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Your Confirme Password</label>
    <div class="input-box">
        <input type="password" id="vConfirmPassword" name="Data[vConfirmPassword]" value="" title="Confirm Password"  class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="confirmpassDiv" style="display:none; float: left;"></div>
</div>
  
</div>

</div>
                </li>
        </ul>
		 
</div>

{literal}
<script type="text/javascript">
//<![CDATA[
    function toggleRememberMepopup(event){
        if($('remember-me-popup')){
            var viewportHeight = document.viewport.getHeight(),
                docHeight      = $$('body')[0].getHeight(),
                height         = docHeight > viewportHeight ? docHeight : viewportHeight;
            $('remember-me-popup').toggle();
            $('window-overlay').setStyle({ height: height + 'px' }).toggle();
        }
        Event.stop(event);
    }

    document.observe("dom:loaded", function() {
        new Insertion.Bottom($$('body')[0], $('window-overlay'));
        new Insertion.Bottom($$('body')[0], $('remember-me-popup'));

        $$('.remember-me-popup-close').each(function(element){
            Event.observe(element, 'click', toggleRememberMepopup);
        })
        $$('#remember-me-box a').each(function(element) {
            Event.observe(element, 'click', toggleRememberMepopup);
        });
    });
//]]>
</script>
{/literal}
    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="{$site_url}home" class="back-link"><small>&laquo; </small>Back</a></p>
        <button type="button" title="Submit" class="button" onclick ="return CheckRegister();"><span><span>Submit</span></span></button>
    </div>
    </form>
<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
    </script>

</div>
<div style="padding-top:85px;">
{include file="right_myAccount.tpl"}
</div>
		    </div>									
	
                </div>
								
            </div>

        </div>

			 
        </div>
{include file="footer.tpl"}
{literal}
<script type="text/javascript">
function CheckRegister()
{
    var extra = '';
    var password =  $('#vPassword').val();   
    var newPassword =  $('#vNewPassword').val();
    var confirmPassword = $('#vConfirmPassword').val();
  
    if (document.getElementById('vPassword').value == "" ||
	document.getElementById('vNewPassword').value == "" ||
	document.getElementById('vConfirmPassword').value == "")
    {
        $("#vPassword").addClass("borderValidations");
	$('#passNameDiv').html('This is a required field.');
	$('#passNameDiv').show();
	$("#vNewPassword").addClass("borderValidations");
	$('#newpassNameDiv').html('This is a required field.');
	$('#newpassNameDiv').show();
	$("#vConfirmPassword").addClass("borderValidations");
	$('#confirmpassDiv').html('This is a required field.');
	$('#confirmpassDiv').show();
    }    
    var validate = true;
    if( $('#vPassword').val() == ''){
	$("#vPassword").addClass("borderValidations");
	$('#passNameDiv').html('This is a required field.');
	$('#passNameDiv').show();
	validate = false;
    }
    else{
      $('#passNameDiv').hide();
      $("#vPassword").removeClass("borderValidations");
     }
     
    if( $('#vNewPassword').val() == ''){
	$("#vNewPassword").addClass("borderValidations");
	$('#newpassNameDiv').html('This is a required field.');
	$('#newpassNameDiv').show();
	validate = false;
    }
    else if(newPassword.length < 6 ){
     $("#vNewPassword").addClass("borderValidations");
	$('#newpassNameDiv').html("Your password must be 6 characters. The one you entered had "+newPassword.length+" characters");
	$('#newpassNameDiv').show();
	validate = false;
    }
    
    else{
      $('#newpassNameDiv').hide();
      $("#vNewPassword").removeClass("borderValidations");
    }

    if( $('#vConfirmPassword').val() == ''){
	$("#vConfirmPassword").addClass("borderValidations");
	$('#confirmpassDiv').html('This is a required field.');
	$('#confirmpassDiv').show();
	validate = false;
    }    
    else if( $('#vNewPassword').val() !=  $('#vConfirmPassword').val()){
        $("#vConfirmPassword").addClass("borderValidations");
	$('#confirmpassDiv').html('Password Is Not Match Please Enter again');
	$('#confirmpassDiv').show();
	validate = false;
       //$('#passwordagainDiv').html('Password Is Not Match Please Enter again'); $('#passwordagainDiv').show(); return false;
    }
    else{
      $('#confirmpassDiv').hide();
      $("#vConfirmPassword").removeClass("borderValidations");
    }   
  
  if(validate){
    
    var password =  $('#vPassword').val();   
    var newPassword =  $('#vNewPassword').val();
    var confirmPassword = $('#vConfirmPassword').val();
    var extra = '';
    
	  extra+='?vPassword='+password;
	  extra+='&vNewPassword='+newPassword;
          extra+='&vConfirmPassword='+confirmPassword;
	  var url = site_url + 'myAccount/changepass';
          var pars = extra;
          $.post(url+pars,
	      function(data) {
		if(data == 'Invalid password'){
			  $("#vPassword").addClass("borderValidations");
			 $('#passNameDiv').html('You have entered invalid password');
			 $('#passNameDiv').show();
			 validate = false;			 
		}
		if(data == 'success'){
			 window.location = site_url+'myAccount/myDashboard'
		}
	      }
	 );
  }
     
    /*if( $('#vPassword').val() == ''){
	  $('.error_valid').html('<div id="error_msg" style="color:red;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;">Enter Your Current Password</div>'); $('#error_msg').show(); return false;
    }
    if( $('#vNewPassword').val() == ''){
	  $('.error_valid').html('<div id="error_msg" style="color:red;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;">Enter Your New Password</div>'); $('#error_msg').show(); return false;
    }
    if( $('#vConfirmPassword').val() == ''){
       $('.error_valid').html('<div id="error_msg" style="color:red;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;">Enter Your Confirm Password</div>'); $('#error_msg').show(); return false;
    }
    */
    return false;
}


</script>
{/literal}