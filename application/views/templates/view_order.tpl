{include file="header.tpl" title="order details"}
<div class="main-container col1-layout">
<div class="main container">
<div class="container">
<div class="row-fluid show-grid">
<div class="col-main-wap span9">
        <div class="col-main">
<div class="col-main-inner">
																				<div class="my-account"><div class="page-title title-buttons">
    <h1>Order {$orderData[0]->vInvoiceNum} - {$orderData[0]->eStatus}</h1>
    
   <!-- <a href="http://192.168.1.12/magentowebapp/mt_robel/sales/order/reorder/order_id/5/" class="link-reorder">Reorder</a>
    <span class="separator">|</span>
<a href="http://192.168.1.12/magentowebapp/mt_robel/sales/order/print/order_id/5/" class="link-print" onclick="this.target='_blank';">Print Order</a>-->
</div>
<dl class="order-info">
    <dt>About This Order:</dt>
    <dd>
                <ul id="order-info-tabs">
                                    <li class="current">Order Information</li>
                            </ul>
        
    </dd>
</dl>
<p class="order-date">Order Date: {$orderData[0]->dAddedDate|@date_format}</p>
<div class="col2-set order-info-box">
    <div class="col-1">
        <div class="box">
            <div class="box-title">
                <h2>Shipping Address</h2>
            </div>
            <div class="box-content">
                <address>{$orderData[0]->vShippName}<br/>
                    {$orderData[0]->vShippCity}<br />
                    {section name=i loop=$state}
                    {if $state[i]->iStateId eq $orderData[0]->iShippStateId} {$state[i]->vState} {/if}
                    {/section}<br />
                    , {$orderData[0]->vShippPostalcode}<br/>
                    {section name=j loop=$country}{if $country[j]->iCountryId eq $orderData[0]->iShippCountryId} {$country[j]->vCountry} {/if}{/section}<br/>
                    {$orderData[0]->vShippPhone}
                </address>
            </div>
        </div>
    </div>
    <div class="col-2">
        <div class="box">
            <div class="box-title">
                <h2>Shipping Method</h2>
            </div>
            <div class="box-content">
                                    Flat Rate - {if $orderData[0]->vShippingMethodTitle eq '' }N/A {else} {$orderData[0]->vShippingMethodTitle}{/if}
            </div>
        </div>
    </div>
</div>
<div class="col2-set order-info-box">
    <div class="col-1">
        <div class="box">
            <div class="box-title">
                <h2>Billing Address</h2>
            </div>
            <div class="box-content">
                <address>{$orderData[0]->vBillName}<br/>
                  {$orderData[0]->vBillCity}<br />
                    {section name=i loop=$state}
                    {if $state[i]->iStateId eq $orderData[0]->iBillStateId} {$state[i]->vState} {/if}
                    {/section}<br />
                    , {$orderData[0]->vBillPostalcode}<br/>
                    {section name=j loop=$country}{if $country[j]->iCountryId eq $orderData[0]->iBillCountryId} {$country[j]->vCountry} {/if}{/section}<br/>
                    {$orderData[0]->vBillPhone}
            </div>
        </div>
    </div>
    <div class="col-2">
        <div class="box box-payment">
            <div class="box-title">
                <h2>Payment Method</h2>
            </div>
            <div class="box-content">
                <p>Check / Money order</p>
            </div>
        </div>
    </div>
</div>
<div class="order-items order-details">
            <h2 class="table-caption">Items Ordered            </h2>

    <table class="data-table" id="my-orders-table" summary="Items Ordered">
    <col />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <thead>
        <tr style="color:#FFFFFF">
            <th>Product Name</th>
            <th>SKU</th>
            <th class="a-right">Price</th>
            <th class="a-center">Qty</th>
            <th class="a-right">Subtotal</th>
        </tr>
    </thead>
 
    <tfoot>
                <tr class="subtotal">
        <td colspan="4" class="a-right">
                        Subtotal                    </td>
        <td class="last a-right">
                        <span class="price">${$orderData[0]->fSubTotal}</span>                    </td>
    </tr>
            <tr class="shipping">
        <td colspan="4" class="a-right">
                        Shipping &amp; Handling                    </td>
        <td class="last a-right">
                        <span class="price">{if $orderData[0]->fShippmentCharge eq ''}N/A {else}${$orderData[0]->fShippmentCharge}{/if}</span></td>
    </tr>
            <tr class="grand_total">
        <td colspan="4" class="a-right">
                        <strong>Grand Total</strong>
                    </td>
        <td class="last a-right">
                        <strong><span class="price">${$orderData[0]->total}</span></strong>
                    </td>
    </tr>
        </tfoot>
   {if $orderData|@count gt 0}
    {section name=i loop=$orderData}
        <tbody>
            <tr class="border" id="order-item-row-8">
    <td><h3 class="product-name">{$orderData[0]->vProductName}</h3>
                                                                </td>
    <td>023</td>
    <td class="a-right">
                    <span class="price-excl-tax">
                                                    <span class="cart-price">
                
                                            <span class="price">${$orderData[i]->fPrice}</span>                    
                </span>


                            </span>
            <br />
                    </td>
    <td class="a-right">
        <span class="nobr" style="color: #002F5F;">
                            Ordered: <strong> {$orderData[i]->iQty}</strong><br />
                                        </span>
    </td>
    <td class="a-right">
                    <span class="price-excl-tax">
                                                    <span class="cart-price">
                                            <span class="price">${$orderData[i]->fSubTotal}</span>                    
                </span>


                            </span>
            <br />
                    </td>
    <!--
        <th class="a-right"><span class="price">$125.00</span></th>
            -->
</tr>
                    </tbody>
    {/section}
    {/if}
        </table>
                <div class="buttons-set">
        <p class="back-link"><a href="{$site_url}myAccount/myOrders"><small>&laquo; </small>Back to My Orders</a></p>
    </div>
</div></div>									</div>
								</div>
							</div>
 <div style="margin-top:68px;">{include file = "right_myAccount.tpl"}</div>
 
	</div> <!-- end class=mt-smartmenu --> 
<div class="block block-bestseller" id="block-bestseller">
	    <div class="title-divider">
        <span>&nbsp;</span>
    </div>
    <div class="block-content">
        <ol class="mini-products-list" id="bestseller-sidebar">
                    </ol>
  
    </div>
</div>
</div>
</div>
</div>
{include file="footer.tpl"}