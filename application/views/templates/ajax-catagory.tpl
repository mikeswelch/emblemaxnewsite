<div id='cat'>
  {if $all_category_data|@count gt 0}
  {section name=i loop=$all_category_data}
  <div class="products-grid row-fluid show-grid">
      <div class="item first span4">
          <div class="item-inner content">
              <div class="product-image">
                  <div class="product-thumb">
                      <a href="{$site_url}category/{$all_category_data[i]['vUrlText']}" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
                      <img style="width:270px; height:306px;" src="{$upload_path}productcategory/{$all_category_data[i]['iCategoryId']}/{$all_category_data[i]['vImage']}" alt="Cras in risus et risus" />
                      </a>
                  </div>
              </div>
              <div class="mask" onclick="setLocation('#')">							
                  <h3 class="product-name">
                  <a href="{$site_url}category/{$all_category_data[i]['vUrlText']}" title="Cras in risus et risus">{$all_category_data[i]['vCategory']}</a>
                  </h3>
              </div>	
              <div class="top-actions-inner">
                  <div class="mt-actions clearfix">
                  </div>					
              </div>
          </div>
      </div>
  </div>
  {/section}
  {else}
  <span>No Item Found</span>
  {/if}
  <!--
  <div class="pages_top">
      <ul>
          {for $start=1 to $pages}
              {if $current_page eq $start}
                  <li class="pgact">{$start}</li>
              {else}
              <li><a href="javascript:void(0);" style="cursor: pointer;" onclick="loadData({$start});">{$start}</a></li>
              {/if}
          {/for}		
      </ul>
  </div>
-->
<div id="ajax_paging">
  <div class="Pagingbox">
        {$pages}
   </div>
   {$recmsg}
 </div>
  
</div>

