<div class="col-main-inner" id="maindivHide">
    <p class="category-image" style="display:block;"></p>
    <div class="category-products">
	  <div class="toolbar"></div>
	  <div id="pageData">
		{if $all_category_data|@count gt 0}
		{section name=i loop=$all_category_data}
		<div class="products-grid row-fluid show-grid">
		    <div class="item first span4">
			  <div class="item-inner content">
				<div class="product-image">
				    <div class="product-thumb">
					  <a href="{$site_url}instant-quote/{$all_category_data[i]['iProductId']}" title="{$all_category_data[i]['vProductName']}" class="product-image visible-desktop" id="product_31">
					  <img style="width:200px; height:250px;" src="{$upload_path}product/{$all_category_data[i]['iProductId']}/{$all_category_data[i]['vImage']}" alt="Cras in risus et risus" />
					  </a>
				    </div>
				</div>
				<div class="mask" onclick="setLocation('#')">							
				    <h3 class="product-name">
				    <a href="{$site_url}instant-quote/{$all_category_data[i]['iProductId']}" title="{$all_category_data[i]['vProductName']} ">{$all_category_data[i]['vProductName']}</a>
				    </h3>
				</div>
				<div class="top-actions-inner">
				    <div class="mt-actions clearfix">
				    </div>					
				</div>
			  </div>
		    </div>
		</div>
		{/section}
		{else}
		<span style="margin-left: 50%;font-size: medium;">No Item Found</span>
		{/if}
		<div class="mt-row-page">
		  <div id="ajax_paging">
		  <div class="Pagingbox">
			   {$pages}
		   </div>
		   {$recmsg}
		  </div>
	   </div>
	  </div>
	  <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>
	  <div class="toolbar-bottom">
		<div class="toolbar"></div>
	  </div>
    </div>
</div>