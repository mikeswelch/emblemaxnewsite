{include file="header.tpl"}
<div class="main-container col2-right-layout">
    <div class="main">
	  <div class="main-inner clearfix">				
		<div class="breadcrumbs row-fluid">
		    <div class="container">
		    <ul class="span12" style="text-align:left;">
			  <li class="home">
				<a href="#" title="Go to Home Page">Home</a>
				<samp></samp>
			  </li> 
			  <li class="category5">
				<strong>Categories</strong>
			  </li> 
		    </ul> 
		    <!--		<div class ="mt-page-title hidden-phone span4"> 
		    <h2>Categories</h2> 
		    </div> 
		    -->
		    </div>	
		</div>
		<div class="container">
		    <div class="row-fluid show-grid">
			  <div class="col-main span9">
				<div class="col-main-inner">
				    <p class="category-image" style="display:block;"></p>
				    <div class="category-products">
					  <div class="toolbar">
						<div class="pager row-fluid show-grid">		
						    <div class="sort-by span4">
							  <div class="mt-sort-by clearfix">			
							  <p class="pull-left mt-toolbar-label hidden-tablet"><label>Sort By</label></p>
							  <select id="mt_sort_by" onchange="setLocation(this.value)">
							  <option value="posi" selected="selected">Position</option>
							  <option value="name">Name</option>
							  <option value="price">Price</option>
							  <option value="tier">Tier Price</option>
							  </select>
							  <p class="mt-sort-arrows pull-left">
								<a href="#" title="Set Descending Direction"><img src="images/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
							  </p>
							  </div>
						    </div>
						    <div class="limiter span4">
							  <div class="mt-limiter">
								<p class="pull-left mt-toolbar-label mt-slabel-1 hidden-tablet"><label>Show</label></p>
								<select id="mt_limiter" onchange="setLocation(this.value)">
								    <option value="df" selected="selected">9</option>
								    <option value="dfasf">15</option>
								    <option value="df">30</option>
								</select> 
								<p class="pull-left mt-toolbar-label mt-slabel-2 hidden-tablet">per page</p>
							  </div>
						    </div>
						    <div class="row-pager clearfix" style="display:none;">
							  <div class="mt-row-page">
							  </div>			
						    </div>
						    
						    <div class="view-mode span4">
							  <div class="mt-view pull-right clearfix">			
							  <p class="pull-left mt-toolbar-label hidden-tablet"><label>View as:</label></p>
							  <span class="grid">
								<strong title="Grid"></strong>
							  </span>
							  <span class="list">
								<a href="#" title="List"></a>
							  </span>
							  </div>			
						    </div>
						</div>
					   </div>
					   <div id="pageData">
						  {if $allProduct|@count gt 0}
						  {section name=i loop=$allProduct}
						  <div class="products-grid row-fluid show-grid">
							<div class="item first span4">
							    <div class="item-inner content">
								  <div class="product-image">
									<div class="product-thumb">
									    <a href="#" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
									    <img style="width:270px; height:306px;" src="{$upload_path}product/{$allProduct[i]['iProductId']}/{$allProduct[i]['vImage']}" alt="Cras in risus et risus" />
									    </a>
									</div>
									<div class="quotelink"><a href="{$site_url}instant-quote/{$allProduct[i]['iProductId']}">Get a Quote</a></div>
								  </div>
								  <div class="mask quotelinkmak" onclick="setLocation('#')">							
									<h3 class="product-name">
									<a href="#" title="Cras in risus et risus">{$allProduct[i]['vProductName']}</a>
									</h3>
								  </div>
								  <div class="top-actions-inner">
									<div class="mt-actions clearfix">
									</div>					
								  </div>		
							    </div>
							</div>
						  </div>
						  {/section}
						  {else}
						  <span>No Item Found</span>
						  {/if}
						  <div class="mt-row-page">
							 <div id="ajax_paging">
							 <div class="Pagingbox">
								  {$pages}
							  </div>
							  <!--{$recmsg}-->
							 </div>
						  </div>
					   </div>
					  <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>
					  <div class="toolbar-bottom">
						<div class="toolbar">
						    <div class="pager row-fluid show-grid">		
							  <div class="sort-by span4">
								<div class="mt-sort-by clearfix">			
								    <p class="pull-left mt-toolbar-label hidden-tablet"><label>Sort By</label></p>
								    <select id="mt_sort_by" onchange="setLocation(this.value)">
									  <option value="position" selected="selected">Position</option>
									  <option value="name">Name</option>
									  <option value="price">Price</option>
									  <option value="tier">Tier Price</option>
								    </select>
								    <p class="mt-sort-arrows pull-left">
								    <a href="#" title="Set Descending Direction"><img src="images/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
								    </p>
								</div>			
							  </div>
							  <div class="limiter span4">
								<div class="mt-limiter">
								    <p class="pull-left mt-toolbar-label mt-slabel-1 hidden-tablet"><label>Show</label></p>
								    <select id="mt_limiter" onchange="setLocation(this.value)">
									  <option value="http://robel.joomvision.com/categories.html?limit=9" selected="selected">9</option>
									  <option value="http://robel.joomvision.com/categories.html?limit=15">15</option>
									  <option value="http://robel.joomvision.com/categories.html?limit=30">30</option>
								    </select> 
								    <p class="pull-left mt-toolbar-label mt-slabel-2 hidden-tablet">per page</p>
								</div>
							  </div>
							  <div class="row-pager clearfix" style="display:none;">
								<!--<div class="mt-row-page">
								    <div class="pages">
									  <ol class="paging">
									  <li class="current">1</li>
									  <li><a href="#">2</a></li>
									  <li><a class="next i-next" href="#" title="Next"></a></li>
									  </ol>
								    </div>
								</div>-->
								<!--<div class="mt-row-page">
								    <div class="pages">
									  {if $pages|@count gt 0}
									  <ol class="paging">
										<li><a class="back i-back" title="Back" href="javascript:void(0)" onclick="changePagination('1',{$iCategoryId})"></a></li>
										{section name=i loop=$pages}
										<li id="no" class='link'><a href="javascript:void(0)" onclick="changePagination('{$smarty.section.i.index+1}',{$iCategoryId})">{$smarty.section.i.index+1}</a></li>
										{/section}
										<li><a class="next i-next" title="Next" href="javascript:void(0)" onclick="changePagination('{$pages}',{$iCategoryId})"></a></li>
									  </ol>
									  {/if}
								    </div>
								</div>-->
							  </div>
							  <div class="view-mode span4">
								<div class="mt-view pull-right clearfix">			
								    <p class="pull-left mt-toolbar-label hidden-tablet"><label>View as:</label></p>
								    <span class="grid">
									  <strong title="Grid"></strong>
								    </span>
								    <span class="list">
									  <a href="#" title="List"> </a>
								    </span>
								</div>			
							  </div>
						    </div>
						</div>
					  </div>
				    </div>
				</div>
			  </div>
			  <div class="col-right sidebar span3 visible-desktop"> 
				<div class="block mt-smartmenu">
				    <div class="block-title">
					  <strong><span>Category</span></strong>
				    </div>
				    <div class="title-divider">
					  <span>&nbsp;</span>
				    </div>
				    <div class="block-content">
					   {literal}	
					   <!-- <script type="text/javascript" src="js/mt.accordion.js"></script> -->
						  <script type="text/javascript">
							 $mtkb(document).ready(function(){	
							 // applying the settings
							 $mtkb("#mt-accordion li.active span.head").addClass("selected");
							 $mtkb('#mt-accordion').Accordion({
								active: 'span.selected',
								header: 'span.head',
								alwaysOpen: false,
								animated: true,
								showSpeed: 400,
								hideSpeed: 800,
								event: 'click'
								});
							 });	
						  </script>
					   {/literal}
					   <ul id="mt-accordion" class="clearfix">
						 {if $categories|@count gt 0}
						 {section name=i loop=$categories}
						 {if $categories[i]->iParentId eq '0'}
						 <li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
							<a href="{$site_url}category/{$categories[i]->vUrlText}"><span>{$categories[i]->vCategory}</span></a><span class="head"><a href="#" style="float:right;"></a></span>
							<ul class="level0">
							   {section name=j loop=$categories}
							   {if $categories[j]->iParentId neq '0' AND $categories[j]->iParentId eq $categories[i]->iCategoryId}
							   <li class="level1 nav-categories-footwear-man">
								 <a href="{$site_url}category/{$categories[j]->vUrlText}"><span>{$categories[j]->vCategory}</span></a>
							   </li>
							   {/if}
							   {/section}
							</ul>
						 </li>
						 {/if}
						 {/section}
						 {/if}
					   </ul>
				    </div> 
				</div>
				<input id="max-price" type="hidden" name="max-price" value="151">
				<input id="filter_url" type="hidden" name="filter_url" value="http://robel.joomvision.com/">  
				<input id="category" type="hidden"  value="5">
				<div class="block block-layered-nav">
				    <div class="block-title">
					  <strong><span>Price</span></strong>
				    </div>
				    <div class="title-divider"><span>&nbsp;</span></div>
				    <div class="block-content"> 
					  <p class="block-subtitle" style="display: none;">Shopping Options</p>
					  <dl id="narrow-by-list">  
						<dd style="height:11px; margin: 20px 0 20px 0;" id="slider" class="odd ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header" style="left: 0%; width: 100%;"></div><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></a></dd>
						<dd class="last even"> 
						    <span class="filter_min" id="filter_min">$0.00</span> 
						    <span class="filter_max" id="filter_max">$427.00</span>
						</dd> 
					  </dl>
					  <!-- <script type="text/javascript">decorateDataList('narrow-by-list')</script> -->
				    </div>
				    {literal}
				    <script type="text/javascript">
					  var Magen_filterslider = {
						params : '',
						currency : '$',
						category_id: 5,
						currency_code : 'USD',
						filter_min: 0,
						filter_max: 427
					  }
				    </script>
				    {/literal}
				</div>
			  </div>
		    </div>
		</div>
	  </div>
	  <div class="mt-productcroller-container clearfix"></div>
    </div>
</div>
{literal}
<script type='text/javascript'>
function searchPaginationProduct(start){
   var q = '{/literal}{$alp}{literal}';
   //alert(q);
    var url = site_url+'search';
    var dataString = 'start='+ start+'&q='+q+'&ajax=yes';
    //alert(url);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		//return false;
		$("#pageData").html(result);
	  }
    });
}
</script>
{/literal}
{include file="footer.tpl"}