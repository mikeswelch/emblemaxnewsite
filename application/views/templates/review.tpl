{include file="header.tpl"}
<div class="main-container col2-right-layout">
  			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
			 <div class="main container">
			<div class="slideshow_static"></div>
                        {if $msg}
			<ul class="messages" style="margin-top: 15px;">
			    <li class="success-msg">
			      <ul>
				 <li>
				 <span>{$msg}</span>
				 </li>
			      </ul>
			    </li>
			</ul>
			 {/if}
			
		       <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>
		       
                      <div class="main-inner">
                    <div class="col-main">
                         <div class="account-create"></div>			  
   			<h2 class="pagetital subtit">Reviews</h2>
			<div style="clear:both;"></div>
			 <div class="ordleftpartrev" id="ordleftpart">
			 <div class="privavypolicymain">
			 <div class="row-pager clearfix" style="display:none;">
			 <div class="mt-row-page">
			 </div>			
			 </div>

		         <div id="pageData" >
			     {if $reviewAll|@count gt 0}
			     {section name=i loop=$reviewAll}
			        <div class="reviewpart">
				    <div class="datepart" style="color: #9E053B">{$reviewAll[i]['dAddedDate']|date_format:"%D"}</div>
				    <div class="revtxt">{$reviewAll[i]['tDescription']}</div>
				    <div class="addname" style="float:right; color: #002A5C;float: right !important; text-align:right;">{$reviewAll[i]['vCustomerName']}<br />
				      {$reviewAll[i]['vCity']},{section name=j loop=$db_country}{if $db_country[j]['iCountryId'] eq $reviewAll[i]['iCountryId']}{$db_country[j]['vCountry']}{/if}{/section}
				    </div>
				    <h3 class="customerhd"></h3>
			       </div>
                             {/section}
			     {/if}
			 </div>
			 <div class="toolbar-bottom">
			       <div class="toolbar">
				   <div class="pager row-fluid show-grid">	

			 <div class="row-pager clearfix">
			     <div class="mt-row-page">
			         <div style="float:left; width:100%; padding:15px 0 0 0;"><div class="pages">
			         {if $pages|@count gt 0}
			         <ol class="paging">
			             <li><a class="previous i-previous" title="Previous" href="javascript:void(0)" onclick="changePagination('1')"></a></li>
			             {section name=i loop=$pages}
			             <li id="no" class='link'><a href="javascript:void(0)" onclick="changePagination('{$smarty.section.i.index+1}')">{$smarty.section.i.index+1}</a></li>
				     {/section}
			             <li><a class="next i-next" title="Next" href="javascript:void(0)" onclick="changePagination('{$pages}')"></a></li>
			         </ol>
			         {/if}
			         </div></div>
			      </div>
			 </div>
			 </div>
			 </div>
			 </div>
			 </div>
			 </div>
    			<h2 class="pagetital">Your Review</h2>
			<div style="clear:both;"></div>
            <form action="{$site_url}review/add" method="post" id="form-validate" action='#' onsubmit="return CheckRegister();">
        <div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Your Review</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
			 <div class="field name-firstname span6">
			 <label for="firstname" class="required"><em>*</em>Subject</label>
			 <div class="input-box">
			   <input type="text" id="vTitle" name="Data[vTitle]" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
			  </div>
			  <div class="validation-advice" style="display:none; float: left;" id="vTitleDiv"></div>
</div>
    
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Your Name</label>
    <div class="input-box">
        <input type="text" id="vCustomerName" name="Data[vCustomerName]" value="" title="Your Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" style="display:none; float: left;" id="vCustomerNameDiv"></div>
</div>
</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>City</label>
    <div class="input-box">
        <input type="text" id="vCity" name="Data[vCity]" value="" title="Vity" maxlength="255" class="input-text span12 required-entry"  />
   </div>
    <div class="validation-advice" style="display:none; float: left;" id="vCityDiv"></div>
</div>
<div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Select Country</label>
    <div class="input-box">
    		<select class="input-text span12 required-entry" id="country" name="Data[iCountryId]" onchange="getStates(this.value);" title="Country">
		    <option>--Select Country--</option>
		{section name=i loop=$db_country}
    			<option value='{$db_country[i]['iCountryId']}'>{$db_country[i]['vCountry']}</option>
                {/section}
    		</select>
    </div>
    <div class="validation-advice" style="display:none; float: left;" id="vCountryDiv"></div>
</div>
</div>
                </li>
                <li class="fields">
                       <div class="customer-name">
    
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Your Review</label>
    <div class="input-box">
       <textarea style="height:150px ; width: 530px;" name="Data[tDescription]" id="tDescription"></textarea>
     </div>
    <div class="validation-advice" style="display:none; float: left;" id="tDescriptionDiv"></div>
</div>
</div>
                </li>
        </ul>
</div>

{literal}
<script type="text/javascript">
//<![CDATA[
    function toggleRememberMepopup(event){
        if($('remember-me-popup')){
            var viewportHeight = document.viewport.getHeight(),
                docHeight      = $$('body')[0].getHeight(),
                height         = docHeight > viewportHeight ? docHeight : viewportHeight;
            $('remember-me-popup').toggle();
            $('window-overlay').setStyle({ height: height + 'px' }).toggle();
        }
        Event.stop(event);
    }

    document.observe("dom:loaded", function() {
        new Insertion.Bottom($$('body')[0], $('window-overlay'));
        new Insertion.Bottom($$('body')[0], $('remember-me-popup'));

        $$('.remember-me-popup-close').each(function(element){
            Event.observe(element, 'click', toggleRememberMepopup);
        })
        $$('#remember-me-box a').each(function(element) {
            Event.observe(element, 'click', toggleRememberMepopup);
        });
    });
//]]>
</script>
{/literal}
    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="{$site_url}home" class="back-link"><small>&laquo; </small>Back</a></p>
        <button type='submit' title="Submit" class="button"><span><span>Submit</span></span></button>
    </div>
    </form>
<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
    </script>
</div>
                    </div>
					
                </div>				
				
									
								
            </div>
        </div>
			 
			 
        </div>
{include file="footer.tpl"}
{literal}
<script type="text/javascript">
         
function getStates(icountryid)
{
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}

function CheckRegister()
{
    
    var extra = '';
    var title =  $('#vTitle').val();
    var name =  $('#vCustomerName').val();
    var city = $('#vCity').val();
    var country = $('#country').val();
    var description = $('#tDescription').val();
if( $('#vTitle').val() == '' && $('#vCustomerName').val() == '' && $('#vCity').val() == '' && $('#tDescription').val() == '')
    {
			  $("#vTitle").addClass("borderValidations");
			  $('#vTitleDiv').html('This is requried field');
			  $('#vTitleDiv').show();
			 
	            	  $("#vCustomerName").addClass("borderValidations");
			  $('#vCustomerNameDiv').html('This is requried field');
			  $('#vCustomerNameDiv').show();     
			 
			  $("#vCity").addClass("borderValidations");
			  $('#vCityDiv').html('This is requried field');
			  $('#vCityDiv').show();
			 
			  $("#country").addClass("borderValidations");
			  $('#vCountryDiv').html('This is requried field');
			  $('#vCountryDiv').show();
			  
			  $("#tDescription").addClass("borderValidations");
			  $('#tDescriptionDiv').html('This is requried field');
			  $('#tDescriptionDiv').show();       
			  
			  //return false;
    }
     
     var validate = true;
    if( $('#vTitle').val() == '')
    {
			  $("#vTitle").addClass("borderValidations");
			  $('#vTitleDiv').html('This is requried field');
			  $('#vTitleDiv').show();
			  validate = false;
    }
    else
    {
			 $('#vTitleDiv').hide();
			 $("#vTitle").removeClass("borderValidations");
    }
    if( $('#vCustomerName').val() == '')
    {
			  $("#vCustomerName").addClass("borderValidations");
			  $('#vCustomerNameDiv').html('This is requried field');
			  $('#vCustomerNameDiv').show();
			  //return false;
			  validate = false;
    }
    else
    {
			 $('#vCustomerNameDiv').hide();
			 $("#vCustomerName").removeClass("borderValidations");
    }
    if( $('#vCity').val() == '')
    {
			  $("#vCity").addClass("borderValidations");
			  $('#vCityDiv').html('This is requried field');
			  $('#vCityDiv').show();
			  //return false;
			 validate = false;
    }
    else
    {
			 $('#vCityDiv').hide();
			 $("#vCity").removeClass("borderValidations");
    }
    if( $('#country').val() == '--Select Country--')
    {
			  $("#country").addClass("borderValidations");
			  $('#vCountryDiv').html('This is requried field');
			  $('#vCountryDiv').show();
			  //return false;
			  validate = false;
    }
    else
    {
			 $('#vCountryDiv').hide();
			 $("#country").removeClass("borderValidations");
    }
    if( $('#tDescription').val() == '')
    {
			  $("#tDescription").addClass("borderValidations");
			  $('#tDescriptionDiv').html('This is requried field');
			  $('#tDescriptionDiv').show();
			  //return false;
			  validate = false;
			 
    }
    else
    {
			 $('#tDescriptionDiv').hide();
			 $("#tDescription").removeClass("borderValidations");
    } 
    if (validate)
    {
	return true;
    }
    else
    {
	 return false;
    }
  
  
}
</script>

<script type='text/javascript'>
function changePagination(pageId,liId){
    //alert(pageId);
    //alert(liId);
    var url = site_url+'review/ajaxpage';
    //alert(url);
    var dataString = 'pageId='+ pageId;
    //alert(dataString);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		$("#pageData").html(result);
	  }
    });
}
</script>
{/literal}