{include file="header.tpl"}
<div class="container">
    <div class="main">
       <div class="main-inner clearfix">
          <div class="container">
			<div class="row-fluid show-grid">
				<div class="col-main span9">
					<div class="col-main-inner">
						{if $msg}
						<ul class="messages" style="margin-top: 15px;">
						    <li class="success-msg">
						      <ul>
						         <li>
						         <span>{$msg}</span>
						         </li>
						      </ul>
						    </li>
						</ul>
						{/if}
						  <div class="mt-product-list">
							 <div id="options" class="row-fluid show-grid clearfix">
							 <div class="fillter clearfix"> </div>
							 </div>
							 <div class="conleftpart">
								<div class="toptenpart"></div>
								<div class="servicespart">
								    {if $getStaticQuote|@count gt '0'}
								    <div class="sevicesbox">
									   <div class="sevicon"><a href="{$site_url}instant-quote"><img src="{$front_image_path}quate.png" alt=""/></a></div>
									   <div class="boxarrow"><img src="{$front_image_path}box-arrow.png" alt="" /></div>
									   <div class="textboxsev">
										  <h2>{$getStaticQuote[0]->vpagename}</h2>
										  <p>{$getStaticQuote[0]->tContent}</p>
									   </div>
								    </div>
								    {/if}
								    {if $getStaticCustomDesign|@count gt '0'}
								    <div class="sevicesbox">
									   <div class="sevicon"><a href="#"><img src="{$front_image_path}custom-tshirt.png" alt="" /></a></div>
									   <div class="boxarrow"><img src="{$front_image_path}box-arrow.png" alt="" /></div>
									   <div class="textboxsev">
									   <h2>{$getStaticCustomDesign[0]->vpagename}</h2>
									   <p>{$getStaticCustomDesign[0]->tContent}</p>
									   </div>
								    </div>
								    {/if}
								    {if $getStaticBrowseApparel|@count gt '0'}
								    <div class="sevicesbox">
									   <div class="sevicon"><a href="http://emblemax.com/apparel"><img src="{$front_image_path}book.png" alt="" /></a></div>
									   <div class="boxarrow"><img src="{$front_image_path}box-arrow.png" alt="" /></div>
									   <div class="textboxsev">
										  <h2>{$getStaticBrowseApparel[0]->vpagename}</h2>
										  <p>{$getStaticBrowseApparel[0]->tContent}</p>
									   </div>
								    </div>
								    {/if}
								    {if $getStaticFastTurnArround|@count gt '0'}
								    <div class="sevicesbox lastsevbox">
									   
									   <div class="sevicon"><a href="#"><img src="{$front_image_path}arrow.png" alt="" /></a></div>
									   <div class="boxarrow"><img src="{$front_image_path}/box-arrow.png" alt="" /></div>
									   <div class="textboxsev">
									   <h2>{$getStaticFastTurnArround[0]->vpagename}</h2>
									   <p>{$getStaticFastTurnArround[0]->tContent}</p>
									   </div>
									   
								    </div>
								    {/if}
								    
								</div>
								<div class="prodpart">
								    {if $isPromotional|@count gt 0}
								    {section name=i loop=$isPromotional}
								    <div class="prodone"><img src="{$upload_path}/product/{$isPromotional[i]->iProductId}/193X198_{$isPromotional[i]->vImage}" alt="" /></div>
								    {/section}
								    {/if}
								</div>
							 </div>
							 <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
						  </div>
							</div>
							</div>
							<div class="col-right sidebar span3 visible-desktop">
							{include file="right.tpl"}	
							
							</div>
						</div>
					</div>
				</div>
				<!--Call logo scroller-->
			</div>
		</div>
		<div class="botline">
		    <div class="container">
			    <div class="linebg">
			    &nbsp;
			    </div>
		    </div>
	    </div>
{include file="footer.tpl"}
