<div id='cat'>
  {if $allProduct|@count gt 0}
  {section name=i loop=$allProduct}
  <div class="products-grid row-fluid show-grid">
     <div class="item first span4">
         <div class="item-inner content">
            <div class="product-image">
               <div class="product-thumb">
                   <a href="#" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
                   <img style="width:270px; height:306px;" src="{$upload_path}product/{$allProduct[i]['iProductId']}/{$allProduct[i]['vImage']}" alt="Cras in risus et risus" />
                   </a>
               </div>
               <div class="quotelink"><a href="{$site_url}instant-quote/{$allProduct[i]['iProductId']}">Get a Quote</a></div>
            </div>
            <div class="mask quotelinkmak" onclick="setLocation('#')">							
               <h3 class="product-name">
               <a href="#" title="Cras in risus et risus">{$allProduct[i]['vProductName']}</a>
               </h3>
            </div>
            <div class="top-actions-inner">
               <div class="mt-actions clearfix">
               </div>				
            </div>
         </div>
     </div>
  </div>
  {/section}
  {else}
  <span>No Item Found</span>
  {/if}
  <div id="ajax_paging">
  <div class="Pagingbox">
        {$pages}
   </div>
   <!--{$recmsg}-->
 </div>
</div>

