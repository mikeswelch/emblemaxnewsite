{include file="header.tpl"}

	<div class="col2-right-layout">
		<div class="main">
			<div class="main-inner clearfix">				
				<!--div id="category-image" class="category-image-top"><img src="{$front_image_path}img-cat1.jpg" alt="Categories" title="Categories" /></div-->
	
				<div class="breadcrumbs row-fluid">
					<div class="container">
						<ul class="span12" style="text-align:left;">
							<li class="home">
								<a href="#" title="Go to Home Page">Home</a>
								<samp></samp>
							</li> 
							<li class="category5">
								<strong>Categories</strong>
							</li>
							{if $parid eq 'sid'}
								<li class="category5">
									<strong>Sub Categories</strong>
								</li>
							{/if}
							{if $parid eq 'ssid'}
								<li class="category5">
									<strong>Sub Categories</strong>
								</li>
								<li class="category5">
									<strong>Template</strong>
								</li>
							{/if}
						</ul> 
					</div>	
				</div>
					
				<div class="container">
					<div class="row-fluid show-grid">
						<div class="col-main span9">
							<div class="col-main-inner">
								<p class="category-image" style="display:block;"></p>
									<div class="category-products">
										<div class="toolbar">
											<div class="pager row-fluid show-grid">		
												<div class="sort-by span4">
													<div class="mt-sort-by clearfix">			
														<p class="pull-left mt-toolbar-label hidden-tablet"><label>Sort By</label></p>
														<select id="mt_sort_by" onchange="setLocation1()">
															{if $per_page eq 'name'}
																<option value="posi" >Position</option>
																<option value="name" selected="selected">Name</option>
																<option value="price">Price</option>
																<option value="tier">Tier Price</option>
															{else}
																<option value="posi" selected="selected">Position</option>
																<option value="name">Name</option>
																<option value="price">Price</option>
																<option value="tier">Tier Price</option>
															{/if}
														</select>
														<p class="mt-sort-arrows pull-left">
															<a href="#" title="Set Descending Direction"><img src="{$front_image_path}/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
														</p>
													</div>			
												</div>

												<div class="limiter span4">
													<div class="mt-limiter">
														<p class="pull-left mt-toolbar-label mt-slabel-1 hidden-tablet"><label>Show</label></p>
														<select id="mt_limiter" onchange="setLocation1('?per_page='+this.value)">
															{if $per_page eq 15}
																<option value="15" selected="selected">15</option>
																<option value="9">9</option>
																<option value="30">30</option>
																
															{elseif $per_page eq 30}
																<option value="30" selected="selected">30</option>
																<option value="15">15</option>
																<option value="9">9</option>
															{else}
																<option value="9" selected="selected">9</option>
																<option value="15">15</option>
																<option value="30">30</option>
															{/if}
															
															
															
														</select> 
														<p class="pull-left mt-toolbar-label mt-slabel-2 hidden-tablet">per page</p>
													</div>
												</div>

												<div class="row-pager clearfix" style="display:none;">
													<div class="mt-row-page">
														<div class="pages">
																<ol class="paging">
																	<li class="current">1</li>
																	<li><a href="#">2</a></li>
																	<li><a class="next i-next" href="#" title="Next"></a></li>
																</ol>
														</div>
													</div>			
												</div>
		
												<div class="view-mode span4">
													<div class="mt-view pull-right clearfix">			
														<p class="pull-left mt-toolbar-label hidden-tablet"><label>View as:</label></p>
														<span class="grid">
															<strong title="Grid">
															</strong>
														</span>
														<span class="list">
															<a href="#" title="List"></a>
														</span>
													</div>			
												</div>
											</div>	
										</div>
<div id="parid" style="display:none" value="{$parid}"></div>
<div id="mypid" style="display:none" value="">{$pid}</div>
<div id="mysid" style="display:none" value="">{$sid}</div>
<div id='cat'>
										{if $all_category_data|@count gt 0}
											{section name=i loop=$all_category_data}
												<div class="products-grid row-fluid show-grid">
												{if $all_category_data[i]|@count gt 0}
													{section name=j loop=$all_category_data[i]}
														<div class="item first span4">
															<div class="item-inner content">
																<div class="product-image">
																	<div class="product-thumb">
																		{if $parid eq 'ssid'}
																			<a href="#" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
																			 <img src="{$upload_path}product/{$all_category_data[i][j]['iProductId']}/{$all_category_data[i][j]['vImage']}" alt="Cras in risus et risus" />								
																			</a>
																		{else}
																			<a href="{$site_url}category?{$parid}={$all_category_data[i][j]['vCategory']}" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
																			 <img src="{$upload_path}productcategory/{$all_category_data[i][j]['iCategoryId']}/{$all_category_data[i][j]['vImage']}" alt="Cras in risus et risus" />								
																			</a>
																		{/if}
																	</div>
																</div>
																<div class="mask" onclick="setLocation('#')">							
																	<h3 class="product-name">
																		{if $parid eq 'ssid'}
																			<a href="#" title="Cras in risus et risus">{$all_category_data[i][j]['vProductName']}</a>
																		{else}
																			<a href="{$site_url}?{$parid}={$all_category_data[i][j]['iCategoryId']}" title="Cras in risus et risus">{$all_category_data[i][j]['vCategory']}</a>
																		{/if}
																	</h3>
																</div>	
																<div class="top-actions-inner">
																	<div class="mt-actions clearfix">
																	</div>					
																</div>					
															</div>
														</div>
													{/section}
												{else}
													<span>No Item Found</span>
												{/if}
												</div>
											{/section}
										{else}
											<span>No Item Found</span>
										{/if}
										<div class="pages_top">
											<ul>
												{for $start=1 to $pages}
													{if $current_page eq $start}
														<li class="pgact">{$start}</li>
													{else}
													<li><a href="javascript:void(0)" style="cursor: pointer;" onclick="loadData({$start});">{$start}</a></li>
													{/if}
												{/for}		
											</ul>
										</div>
</div>										
						<script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>


	<div class="toolbar-bottom">
		<div class="toolbar">
	<div class="pager row-fluid show-grid">		
		<div class="sort-by span4">
			<div class="mt-sort-by clearfix">			
				<p class="pull-left mt-toolbar-label hidden-tablet"><label>Sort By</label></p>
				<select id="mt_sort_by" onchange="setLocation(this.value)">
									<option value="position" selected="selected">
						Position					</option>
									<option value="name">
						Name					</option>
									<option value="price">
						Price					</option>
									<option value="tier">
						Tier Price					</option>
								</select>
				<p class="mt-sort-arrows pull-left">
											<a href="#" title="Set Descending Direction"><img src="{$front_image_path}/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
									</p>
			</div>			
		</div>

		<div class="limiter span4">
			<div class="mt-limiter">
				<p class="pull-left mt-toolbar-label mt-slabel-1 hidden-tablet"><label>Show</label></p>
				<select id="mt_limiter" onchange="setLocation(this.value)">
									<option value="http://robel.joomvision.com/categories.html?limit=9" selected="selected">
						9					</option>
									<option value="http://robel.joomvision.com/categories.html?limit=15">
						15					</option>
									<option value="http://robel.joomvision.com/categories.html?limit=30">
						30					</option>
								</select> 
				<p class="pull-left mt-toolbar-label mt-slabel-2 hidden-tablet">per page</p>
			</div>
		</div>

		<!--div class="row-pager clearfix" style="display:none;">
			<div class="mt-row-page">
				<div class="pages">
					<ol class="paging">
						
						{for $start=1 to $pages}
							{if $current_page eq $start}
								<li class="current">{$start}</li>
							{else}
							<li><a href="#" style="cursor: pointer;" onclick="loadData({$start});">{$start}</a></li>
							{/if}
						{/for}
					</ol>
				</div>
			</div>			
		</div-->	
		
		<div class="view-mode span4">
			<div class="mt-view pull-right clearfix">			
				<p class="pull-left mt-toolbar-label hidden-tablet"><label>View as:</label></p>
					<span class="grid">
						<strong title="Grid"></strong>
					</span>
					<span class="list">
						<a href="#" title="List"> </a>
					</span>
			</div>			
		</div>
					
	</div>	
</div>
	</div>
</div>
{literal}
<script type="text/javascript"> 
	$mtkb(document).ready(function(){
		$mtkb(".funcybox_quicklook").fancybox({ 
			'autoScale'			: true, 
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'scrolling'			: 'no',
			'autoDimensions'	: true,
			'hideOnContentClick': true,
			'type'				: 'iframe',
			'onComplete': function(){
				$mtkb('#fancybox-frame').load(function() {   
					$mtkb('#fancybox-content').height($mtkb(this).contents().find('body').outerHeight()+0);
					$mtkb('#fancybox-content').width($mtkb(this).contents().find('body').outerWidth()+0); 
					$mtkb('#fancybox-wrap').css({'width':'auto'});
					$mtkb.fancybox.center();
					$mtkb.fancybox.resize(); 
				}); 
			} 
		}); 
		
	});
</script>

{/literal}
								</div>
							</div>
							<div class="col-right sidebar span3 visible-desktop"> 
	<div class="block mt-smartmenu">
		<div class="block-title">
			<strong><span>Category</span></strong>
		</div>
		<div class="title-divider">
			<span>&nbsp;</span>
		</div>
		<div class="block-content">
			<script type="text/javascript" src="{$front_js_path}/mt.accordion.js"></script>
{literal}			
				
				<script type="text/javascript">
					$mtkb(document).ready(function(){	
						// applying the settings
						$mtkb("#mt-accordion li.active span.head").addClass("selected");
						$mtkb('#mt-accordion').Accordion({
							active: 'span.selected',
							header: 'span.head',
							alwaysOpen: false,
							animated: true,
							showSpeed: 400,
							hideSpeed: 800,
							event: 'click'
						});
					});	
				</script>
{/literal}

				<ul id="mt-accordion" class="clearfix">
						{if $tree_cat_structure|@count gt 0}
							{section name=i loop=$tree_cat_structure}
								<li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-women parent">
									<a href="{$site_url}?pid={$tree_cat_structure[i]['info']['iCategoryId']}"><span>{$tree_cat_structure[i]['info']['vCategory']}</span></a><span class="head"><a href="#" style="float:right;"></a></span>
									{if $tree_cat_structure[i]|@count gt 0}
										<ul class="level0">
											{section name=j loop=$tree_cat_structure[i]}
												<li class="level1 nav-women-dresses">
													<a href="#"><span>{$tree_cat_structure[i]['sub_cat'][j]['vCategory']}</span></a>
												</li>
											{/section}
										</ul>
									{else}
									
									{/if}
								</li>
							{/section}
						{else}
						{/if}
				</ul>
						

					</div> <!-- end class=block-content -->
	</div> <!-- end class=mt-smartmenu --> 

 
		
	 
	<input id="max-price" type="hidden" name="max-price" value="151">
		<input id="filter_url" type="hidden" name="filter_url" value="http://robel.joomvision.com/">  
	<input id="category" type="hidden"  value="5">

</div>
						</div>
					</div>
				</div>
				
				<div class="mt-productcroller-container clearfix"></div>
				
									
								
			</div>
		</div>
		
		<!--************mid-part-end************-->
		
			<div class="botline">
												<div class="container">
													<div class="linebg">
													&nbsp;
													</div>
												</div>
											</div>	
		
		

	
	
{include file="footer.tpl"}

{literal}
<script>
	function setLocation1(){
		//{if $parid eq 'ssid'}
		var mid="";
		if ($("#parid").val() == 'ssid'){
			mid = '&sid='+$("#mysid").html();
		}else{
			mid = '&pid='+$("#mypid").html();
		}
		
		//console.log(mid);
		perpage = $("#mt_limiter").val();
		sort = $("#mt_sort_by").val();
		loc = '?per_page='+perpage +'&sort='+sort+mid;
		setLocation(loc);
	}
    function loadData(page)
    {
        var site_url = "{/literal}{$site_url}{literal}";
        $.ajax({
            type: "GET",
            url: site_url+"category/ajaxpage?page="+page,
            success: function(data){
                $('#cat').html(data);
            }
        });
    }
</script>
{/literal}