<section class="fpd-product-container fpd-border-color">
	<div class="fpd-menu-bar fpd-clearfix fpd-main-color">
		<div class="fpd-menu">
			<ul class="fpd-clearfix">
				<li><span class="fpd-save-product icon-save fpd-main-color fpd-tooltip" title="Save product"></span></li>
				<li><span class="fpd-download-image icon-download fpd-main-color fpd-tooltip" title="Download Product Image"></span></li>
				<li><span class="fpd-print icon-print fpd-main-color fpd-tooltip" title="Print"></span></li>
			</ul>
		</div>
	</div>
	<!-- Kinetic Stage -->
	<div class="fpd-product-stage fpd-content-color">
		<canvas></canvas>
	</div>
</section>