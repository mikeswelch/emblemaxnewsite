{include file="header.tpl"}
<div class="main-container col2-right-layout" style="min-height: 210px;">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0; min-height: 210px;">
        <div class="main container">
            <div class="slideshow_static"></div>
            <div class="main-inner">
                <div class="col-main">
                    <div class="account-create">
                        <h2 class="pagetital subtit">Order Placed Successfully</h2>
                        <div style="clear:both;"></div>
                        <div class="ordleftpart" style="width:100%;">
                            <div class="emptyCart">
                                <p style="font-size: 20px;">Thank you for your purchase!</p>
                                <p style="font-size: 15px;">Your order number is: {$invoiceNumber}</p>
                                <p style="font-size: 15px;">You will receive an order confirmation email with details of your order.</p>
                                <p style="font-size: 15px;">Click <a href="{$site_url}category">here</a> to continue shopping.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{include file="footer.tpl"}