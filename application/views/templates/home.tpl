{include file="header.tpl"}
<div class="container">
    <div class="main">
       <div class="main-inner clearfix">
          <div class="container">
			<div class="row-fluid show-grid">
				<div class="col-main span9">
					<div class="col-main-inner">
						{if $msg}
						<ul class="messages" style="margin-top: 15px;">
						    <li class="success-msg">
						      <ul>
						         <li>
						         <span>{$msg}</span>
						         </li>
						      </ul>
						    </li>
						</ul>
						{/if}
						  <div class="mt-product-list">
							 <div id="options" class="row-fluid show-grid clearfix">
							 <div class="fillter clearfix"> </div>
							 </div>
							 <div class="conleftpart">
								<div class="toptenpart"></div>
								<div class="servicespart">
								    {if $getHomeBlocks|@count gt '0'}
								    {section name=i loop=$getHomeBlocks}
								    <div class="sevicesbox">
									   <div class="sevicon"><a href="{$getHomeBlocks[i]->vLinkUrl}" title="{$getHomeBlocks[i]->vLinkUrl}" target="_blank"><img src="{$upload_path}/homepage_blocks/{$getHomeBlocks[i]->iBlockslistId}/{$getHomeBlocks[i]->vImage}" alt="" title="{$getHomeBlocks[i]->vTitle}"/></a></div>
									   <div class="boxarrow"><img src="{$front_image_path}box-arrow.png" alt="" /></div>
									   <div class="textboxsev">
										  <h2>{$getHomeBlocks[i]->vTitle}</h2>
										  <p>{$getHomeBlocks[i]->tDescription}</p>
									   </div>
								    </div>
								    {/section}
								    {/if}
								</div>
								<div class="prodpart">
								    {if $isPromotional|@count gt 0}
									   {section name=i loop=$isPromotional}
									   	<a href="{$site_url}productdetail/{$isPromotional[i]->iProductId}">
										  	<div class="prodone">
										  		<div style="height:198px;"> 
										  		<img src="{$upload_path}/product/{$isPromotional[i]->iProductId}/193X198_{$isPromotional[i]->vImage}" alt="" title="{$isPromotional[i]->vProductName}"/>
										  		 </div>
										<div class="img_box"> <a href="{$site_url}productdetail/{$isPromotional[i]->iProductId}">{if $isPromotional[i]->vBannerText neq ''}{$isPromotional[i]->vBannerText}{else}{$isPromotional[i]->vProductName}{/if}</a></div> </div>

										</a>
									   {/section}
								    {/if}
								</div>
							 </div>
							 <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
						  </div>
							</div>
							</div>
							<div class="col-right sidebar span3 visible-desktop">
							{include file="right.tpl"}
							</div>
						</div>
					</div>
				</div>
				<!--Call logo scroller-->
			</div>
		</div>
		<div class="botline">
		    <div class="container">
			    <div class="linebg">
			    &nbsp;
			    </div>
		    </div>
	    </div>
{include file="footer.tpl"}
