<div id="pageData">
       
       {if $products|@count gt 0}
       {section name=i loop=$products}
       <div class="products-grid row-fluid show-grid">
             <div class="item first span4">
                 <div class="item-inner content">
                       <div class="product-image">
                             <div class="product-thumb">
					    
                                 <a href="{$site_url}instant-quote/{$products[i]['iProductId']}" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
                                 <img style="width:100%; height:auto; position:relative; z-index:9999;" src="{$upload_path}product/{$products[i]['iProductId']}/{$products[i]['vImage']}" alt="Cras in risus et risus" />
                                 </a>
                                   
						<div class="one_hd" onclick="setLocation('#')">							
                                          <h3 >
                                          <a href="{$site_url}instant-quote/{$products[i]['iProductId']}" title="Cras in risus et risus">{$products[i]['vProductName']}</a>
                                          </h3>
                            </div>
                             </div>
                             
                       </div>
                       
                       <div class="top-actions-inner">
                             <div class="mt-actions clearfix">
                             </div>					
                       </div>		
                 </div>
             </div>
       </div>
       {/section}
       {else}
       <span>No Item Found</span>
       {/if}
       <div class="mt-row-page">
              <div id="ajax_paging">
              <div class="Pagingbox">
                       {$pages}
               </div>
               <!--{$recmsg}-->
              </div>
       </div>
</div>