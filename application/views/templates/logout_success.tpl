{include file="header.tpl"}
  
<div class="main-container col2-right-layout">
<form action="{$site_url}registration/check_register" method="post" id="form-validate" onsubmit="return CheckRegister();">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="main container">
 	    <div class="slideshow_static"></div>
                <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>			
                   <div class="main-inner">    
                    <div class="col-main">
                       <div class="account-create">
    			<h2 class="pagetital">You are now logged out</h2>
			<div style="clear:both;">
			    <p>You have logged out and will be redirected to our homepage in <span id="countdown">5</span> seconds.</p>
			</div>
		       </div>		
                    </div>					
                  </div>
 	        </div>	
 	    </div>
	</div>		 		 
</div>
{include file="footer.tpl"}
{literal}
<script language="javascript">

var time_left = 5;
var cinterval;
function time_dec(){
  time_left--;
  document.getElementById('countdown').innerHTML = time_left;
  if(time_left == 0){
    window.location = site_url+'home';
    clearInterval(cinterval);
  }
}
cinterval = setInterval('time_dec()', 1000);
</script>
{/literal}