
{include file="header.tpl" title="Example Smarty Page"}
<div class="main-container col2-right-layout">
   <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
      <div class="main container">
	  <div class="slideshow_static"></div>
                <div class="main-inner">
                    <div class="col-main">
			<div class="account-create">
    			<h2 class="pagetital subtit">Shopping Cart</h2>
			<div style="clear:both;"></div>
			<div class="success_msg">{$msg}</div>
            <div class="ordleftpart" style="width:100%;">
<fieldset>
{if $cart['item']|@count gt 0}
<form id="frmShoppingCart" name="frmShoppingCart" method="post" action="{$site_url}cart/update_cart">
<table id="shopping-cart-table" class="data-table cart-table" style="border-bottom:2px solid #EAEAEA">
    <col width="1" />
    <col />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <col width="1" />

    <thead class="hidden-phone">
      <tr class="">
	  <th class="" rowspan="1"><span class="nobr">Remove</span></th>
	  <th width="100" rowspan="1" class=""><span class="nobr">Image</span></th>
	  <th width="200" rowspan="1" class=""><span class="nobr">Product Name</span></th>
	  <th width="200" rowspan="1" class=""><span class="nobr">Selected Print locations</span></th>
	  <th width="200" rowspan="1" class=""><span class="nobr">Selected Color - Size</span></th>
	  <th width="100" colspan="1"><span class="nobr">Total Qty</span></th>
	  <th rowspan="1"><span class="nobr">Total</span></th>
      </tr>
    </thead>

    <tbody>
       {section name=i loop=$cart['item']}
       <tr>
	   <td><input name="remove[]" type="checkbox" value="" /></td>
	   <td width="100"><img width="100px" src="{$upload_path}product/{$cart['item'][i].iProductId}/{$cart['item'][i].vImage}" alt="" /></td>
	   <td width="200">{$cart['item'][i].vProductName}</td>
	   <td width="200">
	     {section name=j loop=$cart['item'][i]['vPrintLocationsDescription']}
	       {$cart['item'][i]['vPrintLocationsDescription'][j]}<br>
	     {/section}
	   </td>
	   <td width="200">
	     {section name=j loop=$cart['item'][i]['vColorSizeDescription']}
	       {$cart['item'][i]['vColorSizeDescription'][j]}<br>
	     {/section}
	   </td>
	   <td width="100">{$cart['item'][i].vTotalQty}</td>
	   <td width="100">${$cart['item'][i].fQuoteTotal}</td>
       </tr>
       {/section}
    </tbody>
</table>
</form>
<div class="grid_7 coupons">
  <form id="frmCouponDiscount" name="frmCouponDiscount" method="post" action="{$site_url}cart/apply_coupon">
     <label for="couponcode">Coupon code</label>
     <input type="text" name="couponcode" id="couponcode" value="">
     <input type="submit" name="submitcoupon" class="sub" id="submitcoupon" value="Apply">
     <div class="clear"></div>
   </form>
</div>
<div class="grid_9">
<span class="subtotal"><span class="subtotal_lbl">Sub Total:</span>&nbsp;<span class="subtotal_amt">${$cart['SUBTOTAL']}</span></span>
<span class="subtotal"><span class="subtotal_lbl">Turn Around Time Changes ({$cart['vTurnAroundTime']}):</span>&nbsp;<span class="subtotal_amt">${$cart['fTurnAroundTimeCharge']}</span></span>
<span class="subtotal"><span class="subtotal_lbl">Coupon Discount (Code : {$cart['vCouponCode']}):</span>&nbsp;<span class="subtotal_amt">${$cart['fCouponDiscount']}</span></span>
<div class="total">Total : <span class="bigprice">${$cart['GRANDTOTAL']}</span></div>
<div class="updaters">
<input type="button" name="updater" id="updater" value="Update" class="mailsubmit" onclick="updateCart();">
<a href="{$site_url}checkout"><input type="button" name="checkouter" id="checkouter" value="Checkout" class="mailsubmit"></a>
</div>
<a class="yellow" href="#">Continue Shopping</a>
</div>
{else}
     <div class="emptyCart">
        <p style="font-size: 20px;">Shopping Cart is Empty.</p>
        <p style="font-size: 15px;">Click <a href="{$site_url}category">here</a> to continue shopping.</p>
    </div>
{/if}
</fieldset>
</div>   
</div>
</div>
</div>
</div>
</div>
</div>
{include file="footer.tpl"}

{literal}
<script>
 function updateCart() {
  $("#frmShoppingCart").submit();
 }
</script>
{/literal}