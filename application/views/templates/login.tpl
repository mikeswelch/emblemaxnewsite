{include file="header.tpl"}

			<div class="container">
                
		
			 <div class="main-inner">
			  
                    <div class="col-main">
		   
     <div class="account-login row-fluid show-grid"><br/>
      <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7; border-radius: 5px 5px 5px 5px; color: #B94A48; font-size: 13px; font-weight: bold;  padding-top: 8px;  /*padding-left: 10px;*/ text-align: center;  text-shadow: 0 1px 1px #FFFFFF;  width: 940px; height: 30px; margin-left:130px;"></div>
        {if $msg}
                    <ul class="messages">
		    <li class="error-msg">
		    <ul>
		    <li>
		    <span>{$msg}.</span>
		    </li>
		    </ul>
		    </li>
		    </ul>
	   {/if}
	   {if $msgs}
                    <ul class="messages">
		    <li class="success-msg">
		    <ul>
		    <li>
		    <span>{$msgs}.</span>
		    </li>
		    </ul>
		    </li>
		    </ul>
            {/if}
    <!--<div class="page-title">
        <h1>Login or Create an Account</h1>
    </div>-->
        <form action="" method="post" id="login-form">
        <!--<div class="col2-set span6">
            <div class="new-users">
                <div class="content">
                    <h2>New Customers</h2>
                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                </div>
            </div>
            <div class="new-users">
                <div class="buttons-set">
                    <button type="button" title="Create an Account" class="button" onclick="window.location='http://robel.joomvision.com/customer/account/create/';"><span><span>Create an Account</span></span></button>
                </div>
            </div>
        </div>-->
        <div class="col2-set span12" style="padding-bottom:65px;">
	
            <div class="registered-users">
				<h2>USER LOGIN</h2>
                <div class="content">
                
                    <!--<p>If you have an account with us, please log in.</p>-->
                    <ul class="form-list row-fluid show-grid">
                        <li>
                            <!--<label for="email" class="required span5">Email Address<em>*</em></label>-->
			      <input type="text" name="Data[vUsername]" value="" id="user" class=" input-text required-entry validate-password span12" style="background:none; padding-top:14px; margin-top:-1px; border: none;" title="Email Address" placeholder="Username" />
			      <div class="validation-advice" id="userNameDiv" style="display:none; float: left;"></div>
                        </li>
                        <li>
                            <!--<label for="pass" class="required span5">Password<em>*</em></label>-->
				<input type="password" name="Data[vPassword]" class="input-text required-entry validate-password span12" id="pass" title="Password" placeholder="Password" style="background:none; padding-top:14px; margin-top:-1px;  border: none;"/>
				<div class="validation-advice" style="display:none; float: left;" id="passwordDiv"></div>
                        </li>
                                                                    </ul>
                    <div id="window-overlay" class="window-overlay" style="display:none;"></div>
<div id="remember-me-popup" class="remember-me-popup" style="display:none;">
    <div class="remember-me-popup-head">
        <h3>What's this?</h3>
        <a href="#" class="remember-me-popup-close" title="Close">Close</a>
    </div>
    <div class="remember-me-popup-body">
        <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
        <div class="remember-me-popup-close-button a-right">
            <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a>
        </div>
    </div>
</div>
                    <!--<p class="required"><em>*</em> Required Fields</p>-->
					<div class="buttons-set">
						<button type="button" class="button" title="Login" name="send" id="send2"  onclick="CheckLogin();" ><span style="background:#9e053b;"><span style="background:#9e053b;">Login</span></span></button>
						<a href="{$site_url}registration"><button type="button" title="Create an Account" class="button btn-register" onclick=""><span style="background:#9e053b;"><span style="background:#9e053b;">Register</span></span></button></a>
						<a href="{$site_url}authentication/forgotpassword" class="f-left">Forgot Your Password?</a>
					</div>
                </div>
            </div>
        </div>
            </form>
  
</div>
                    </div>
					
                </div>
			 
			 
        </div>
{include file="footer.tpl"}
  {literal} 
<script type="text/javascript">

function CheckLogin()
{
    var username =  $('#user').val();
    var password =  $('#pass').val();
   
    var enterUsername = 'Username Must Be Required';
    var enterPass = 'Password Must Be Required'; 
    var invalidUserPass = "Invalid Username Or Password";
    
    if (document.getElementById('user').value == "" && document.getElementById('pass').value== "")
    {
        $("#pass").addClass("borderValidation");
        $('#passwordDiv').html(enterPass);
	$('#passwordDiv').show();
	
	$("#user").addClass("borderValidation");
	$('#userNameDiv').html(enterUsername);
	$('#userNameDiv').show();
       
        return false;
    }
    
    if (document.getElementById('user').value == "")
    {
        $("#user").addClass("borderValidation");
	$('#userNameDiv').html(enterUsername);
	$('#userNameDiv').show();
       
        return false;
    }
    else{
      $('#userNameDiv').hide();
      $("#user").removeClass("borderValidation");
    }
    
    if (document.getElementById('pass').value== "")
    {
	$("#pass").addClass("borderValidation");
        $('#passwordDiv').html(enterPass);
	$('#passwordDiv').show();
        return false;
    }
  else{
     $('#passwordDiv').hide();
     $("#pass").removeClass("borderValidation");
    }
      var extra ='';
      extra+='?vUserName='+username;
	  extra+='&vPassword='+password;   
  
	  var url = site_url+'authentication/check_login';
	  var pars = extra;
	  $.post(url+pars,
          function(data){
	   $("#user").removeClass("borderValidation");
	   $("#pass").removeClass("borderValidation");
            if(data == 'success')
            {
                window.location = site_url +'myAccount/myDashboard';
            }
            if(data == 'Invalid')
            {
               $('#error_msg').html(invalidUserPass); $('#error_msg').show(); return false;
            }
	    if(data == 'InActive'){
	     $('#error_msg').html('Sorry , You are inactive please confirm your email and activate with your activation link'); $('#error_msg').show(); return false;
	    }
        });
}
</script>
{/literal}
