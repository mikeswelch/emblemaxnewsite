{include file="header.tpl"}
<!--************mid-part-start************-->
<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="main container">
            <div class="slideshow_static"></div>
            <div class="main-inner">
                <div class="col-main">
                    <div class="account-create">
    			<h2 class="pagetital subtit">Checkout</h2>
			<div style="clear:both;"></div>
                        <div class="ordleftpart" style="width:100%;">
                            {if $isLoggedIn eq 0}
                            <div class="fieldset">
                                <form id="frmLogin" name="frmLogin" action="{$site_url}checkout/login" method="post" onsubmit="return validateLoginForm();">
                                    <h2 class="legend">Already Customer</h2>
                                    {if $msg eq 'login failed'}<div style="text-align: center; color: red; font-size: 16px;">Incorrect username or password</div>{/if}
                                    <div class="boxtop">I am returning customer</div>
                                    <ul class="form-list row-fluid show-grid">
                                        <li class="fields">
                                            <div class="customer-name">
                                                <div class="field name-firstname span6">
                                                    <label for="firstname" class="required">User Name</label>
                                                    <div class="input-box">
                                                        <input type="text" id="vUsername" name="vUsername" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                    </div>
                                                    <div class="validation-advice" id="vUsernameDiv" style="display:none; float: left;">This is a required field.</div>
                                                </div>
                                                <div class="field name-lastname span6">
                                                    <label for="lastname" class="required">Password</label>
                                                    <div class="input-box">
                                                        <input type="password" id="vPassword" name="vPassword" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                    </div>
                                                    <div class="validation-advice" id="vPasswordDiv" style="display:none; float: left;">This is a required field.</div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="buttons-set">
                                        <button class="button pull-right" title="Login" type="submit"><span><span>Login</span></span></button>
                                    </div>
                                </form>
                            </div>
                            
                            <div class="fieldset">
                                <h2 class="legend">New Customer</h2>
                                <div class="checkerap">
                                    <div class="boxtop">Checkout Options</div>
                                    <div class="seltacount">
                                        <div class="regont">
                                                <input onclick="guestCheckout();" name="check_options" type="radio" value="" checked="checked" />
                                                <label>Guest Checkout </label>
                                        </div>
                                        <div class="regont">
                                                <input onclick="registerAccount();" name="check_options" type="radio" value="" />
                                                <label>Register Account</label>
                                        </div>
                                        <p class="bycretxt">By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have made.</p>
                                    </div>
                                </div>
                                <div class="buttons-set"></div>
                            </div>
                            {/if}
                            <div class="fieldset">
                                <form id="frmBillingAddress" name="frmBillingAddress">
                                <input type="hidden" name="iUserId" id="iUserId" value="{$iUserId}">
                                <h2 class="legend">Billing Address</h2>
                                <ul class="form-list row-fluid show-grid">
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>First Name</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillFirstname" name="vBillFirstname" value="{if $isLoggedIn eq 1}{$userAddress.vFirstName}{/if}" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillFirstnameDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Last Name</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillLastname" name="vBillLastname" value="{if $isLoggedIn eq 1}{$userAddress.vLastName}{/if}" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillLastnameDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required">Company</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillCompany" name="vBillCompany" value="{if $isLoggedIn eq 1}{$userAddress.vCompany}{/if}" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Address</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillAddress" name="vBillAddress" value="{if $isLoggedIn eq 1}{$userAddress.tStreetAddress1}{/if}" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillAddressDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>City</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillCity" name="vBillCity" value="{if $isLoggedIn eq 1}{$userAddress.vCity}{/if}" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillCityDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Post Code</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillPostcode" name="vBillPostcode" value="{if $isLoggedIn eq 1}{$userAddress.vZipCode}{/if}" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillPostcodeDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Country</label>
                                                <div class="input-box">
                                                    <select class="input-text span12 required-entry" id="iBillCountryId" name="iBillCountryId" onchange="getBillingStates(this.value);" title="Country">
                                                        <option value="">--Select Country--</option>
                                                        {section name=i loop=$db_country}
                                                        <option value='{$db_country[i]->iCountryId}' {if $isLoggedIn eq 1}{if $userAddress.iCountryId eq $db_country[i]->iCountryId}selected="selected"{/if}{/if}>{$db_country[i]->vCountry}</option>
                                                        {/section}
                                                    </select>
                                                </div>
                                                <div class="validation-advice" id="iBillCountryIdDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>State</label>
                                                <div class="input-box">
                                                    <select id="iBillStateId" name="iBillStateId" title="State" class="input-text span12 required-entry"  >
                                                        <option value="">--Select State--</option>
                                                    </select>
                                                </div>
                                                <div class="validation-advice" id="iBillStateIdDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>			 
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Email</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillEmail" name="vBillEmail" value="{if $isLoggedIn eq 1}{$userAddress.vEmail}{/if}" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillEmailDiv" style="display:none; float: left;">This is a required field.</div>
                                                <div class="validation-advice" id="vBillInvalidEmailDiv" style="display:none; float: left;">Invalid Email Address.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Phone</label>
                                                <div class="input-box">
                                                    <input type="text" id="vBillPhone" name="vBillPhone" value="{if $isLoggedIn eq 1}{$userAddress.vTelephone}{/if}" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vBillPhoneDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                    
                                    <li class="fields" id="newUser1" >
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Username</label>
                                                <div class="input-box">
                                                    <input type="text" id="vUsernameNew" name="vUsernameNew" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vUsernameNewDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields" id="newUser2" >
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Password</label>
                                                <div class="input-box">
                                                    <input type="password" id="vPasswordNew" name="vPasswordNew" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vPasswordNewDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Confirm Password</label>
                                                <div class="input-box">
                                                    <input type="password" id="vConfirmPasswordNew" name="vConfirmPasswordNew" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vConfirmPasswordNewDiv" style="display:none; float: left;">This is a required field.</div>
                                                <div class="validation-advice" id="vConfirmPasswordNotMatchDiv" style="display:none; float: left;">Password and Confirm Password not matched.</div>
                                            </div>
                                        </div>
                                    </li>
                                    
                                </ul>
                                </form>
                                <div style="clear:both;"></div>
                                <ul class="form-list row-fluid show-grid">
                                    <li class="control">
                                        <div class="input-box">
                                            <input type="checkbox" class="checkbox" id="same_as_billing" value="1" title="Ship to the same address" name="same_as_billing">
                                        </div>
                                        <label for="same_as_billing">Ship to the same address</label>
                                    </li>
                                </ul>
                            </div>

                            <div class="fieldset">
                                <form id="frmShippingAddress" name="frmShippingAddress">
                                <h2 class="legend">Shipping Address</h2>
                                <ul class="form-list row-fluid show-grid">
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>First Name</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipFirstname" name="vShipFirstname" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipFirstnameDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Last Name</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipLastname" name="vShipLastname" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipLastnameDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required">Company</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipCompany" name="vShipCompany" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Address</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipAddress" name="vShipAddress" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipAddressDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>City</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipCity" name="vShipCity" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipCityDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Post Code</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipPostcode" name="vShipPostcode" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipPostcodeDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                             <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Country</label>
                                                <div class="input-box">
                                                    <select class="input-text span12 required-entry" id="iShipCountryId" name="iShipCountryId" onchange="getShippingStates(this.value);" title="Country">
                                                        <option value="">--Select Country--</option>
                                                        {section name=i loop=$db_country}
                                                        <option value='{$db_country[i]->iCountryId}'>{$db_country[i]->vCountry}</option>
                                                        {/section}
                                                    </select>
                                                </div>
                                                <div class="validation-advice" id="iShipCountryIdDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>State</label>
                                                <div class="input-box">
                                                    <select id="iShipStateId" name="iShipStateId"  title="State" class="input-text span12 required-entry"  >
                                                        <option value="">--Select State--</option>
                                                    </select>
                                                </div>
                                                <div class="validation-advice" id="iShipStateIdDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Email</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipEmail" name="vShipEmail" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipEmailDiv" style="display:none; float: left;">This is a required field.</div>
                                                <div class="validation-advice" id="vShipEmailInvalidDiv" style="display:none; float: left;">Invalid Email Address.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Phone</label>
                                                <div class="input-box">
                                                    <input type="text" id="vShipPhone" name="vShipPhone" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="vShipPhoneDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                </form>
                            </div>
                            <div class="fieldset">
                                <h2 class="legend">Order Comments</h2>
                                <div class="boxtop">Notes about order, for example instructions for delivery.</div>
                                <div class="txtboxwrp">
                                    <textarea class="bigewizz ordnote" name="tComment" id="tComment"></textarea>
                                </div>
                                <div style="clear:both;"></div>
                                <ul class="form-list row-fluid show-grid">
                                    <li class="control">
                                        <div class="input-box">
                                            <input type="checkbox" class="checkbox" id="eAgree" value="1" title="Agree Terms and conditions" name="eAgree">
                                        </div>
                                        <label for="eAgree">I have read and agree to the <a href="#">Terms and conditions</a></label>
                                        <div class="validation-advice" id="eAgreeDiv" style="display:none; float: left;">You must agree to our terms and conditions.</div>
                                    </li>
                                </ul>
                            </div>

                            <div class="fieldset">
                                <h2 class="legend">Payment Method</h2>
                                <div class="checkerap">
                                    <div class="seltacount">
                                        <div class="regont">
                                                <input onclick="selectPaypalPayment();" id="payment_paypal" name="payment" type="radio" value="" checked="checked" />
                                                <label for="payment_paypal">Paypal</label>
                                        </div>
                                    </div>
							 <div class="seltacount">
                                        <div class="regont">
                                                <input onclick="selectAuthorizePayment();" id="payment_authorize" name="payment" type="radio" value="" />
                                                <label for="payment_paypal">Authorize.Net</label>
                                        </div>
                                    </div>
                                    <div class="seltacount">
                                        <div class="regont">
                                                <input onclick="selectCreditCardPayment();" name="payment" id="payment_cc" type="radio" value="" />
                                                <label for="payment_cc">Credit Card </label>
                                        </div>
                                    </div>
                                    {literal}
                                    <script>
                                        var paymentMethod = 'Paypal';
                                        function selectCreditCardPayment() {
                                            $('#CreditCard').show();
                                            paymentMethod = 'CreditCard';
                                        }
                                        function selectPaypalPayment() {
                                            $('#CreditCard').hide();
                                            paymentMethod = 'Paypal';
                                        }
					function selectAuthorizePayment() {
                                            $('#CreditCard').show();
                                            paymentMethod = 'CreditCard';
                                        }
                                    </script>
                                    {/literal}
                                </div>
				
                                <form id="frmCreditCard" name="frmCreditCard" >
                                <div id="paymentErrorMsg" style="color: red; text-align: center; font-size: 16px;"></div>
                                <ul class="form-list row-fluid show-grid" id="CreditCard" style="display: none;">
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>First Name</label>
                                                <div class="input-box">
                                                    <input type="text" id="firstnameOnCard" name="firstnameOnCard" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="firstnameOnCardDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Last Name</label>
                                                <div class="input-box">
                                                    <input type="text" id="lastnameOnCard" name="lastnameOnCard" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="lastnameOnCardDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Card Type</label>
                                                <div class="input-box">
                                                    <select id="cardType" name="cardType" class="input-text span12 required-entry">
                                                        <option value=""> Select Card Type</option>
                                                        <option value="Amex">American Express </option>
                                                        <option value="Discover">Discover</option>
                                                        <option value="MasterCard">MasterCard</option>
                                                        <option value="Visa">Visa</option>
                                                    </select> 
                                                </div>
                                                <div class="validation-advice" id="cardTypeDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>Number on Card</label>
                                                <div class="input-box">
                                                    <input type="text" id="cardNumber" name="cardNumber" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="cardNumberDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="customer-name">
                                            <div class="field name-firstname span6">
                                                <label for="firstname" class="required"><em>*</em>Expiry Date</label>
                                                <div class="input-box">
                                                    <input type="text" id="expiryDate" name="expiryDate" placeholder="MMYYYY" value="" title="First Name" maxlength="6" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="expiryDateDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                            <div class="field name-lastname span6">
                                                <label for="lastname" class="required"><em>*</em>CVV</label>
                                                <div class="input-box">
                                                    <input type="text" id="cvvNumber" name="cvvNumber" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
                                                </div>
                                                <div class="validation-advice" id="cvvNumberDiv" style="display:none; float: left;">This is a required field.</div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                </form>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="fieldset">            
                                <h2 class="legend">Confirm Order</h2>
                                <div class="checkerap">
                                    <table style="border-bottom:2px solid #EAEAEA" class="cart-table table table-bordered" id="shopping-cart-table">
                                        <colgroup>
                                            <col width="1">
                                            <col>
                                            <col width="1">
                                            <col width="1">
                                            <col width="1">
                                            <col width="1">
                                            <col width="1">
                                        </colgroup>
                                        <thead class="hidden-phone">
                                            <th width="100" rowspan="1" class=""><span class="nobr">Image</span></th>
                                            <th width="200" rowspan="1" class=""><span class="nobr">Product Name</span></th>
                                            <th width="200" rowspan="1" class=""><span class="nobr">Selected Print locations</span></th>
                                            <th width="200" rowspan="1" class=""><span class="nobr">Selected Color - Size</span></th>
                                            <th width="100" colspan="1"><span class="nobr">Total Qty</span></th>
                                            <th rowspan="1"><span class="nobr">Total</span></th>
                                        </thead>
                                        <tbody>
								    <div></div>
                                            {section name=i loop=$cart['item']}
                                               <tr>
                                                    <td width="100"><img width="100px" src="{$upload_path}product/{$cart['item'][i].iProductId}/{$cart['item'][i].vImage}" alt=""  /></td>
                                                    <td width="200">{$cart['item'][i].vProductName}</td>
                                                    <td width="200" >
                                                      {section name=j loop=$cart['item'][i]['vPrintLocationsDescription']}
                                                        {$cart['item'][i]['vPrintLocationsDescription'][j]}<br>
                                                      {/section}
                                                    </td>
                                                    <td width="200">
                                                      {section name=j loop=$cart['item'][i]['vColorSizeDescription']}
                                                        {$cart['item'][i]['vColorSizeDescription'][j]}<br>
                                                      {/section}
                                                    </td>
                                                    <td width="100">{$cart['item'][i].vTotalQty}</td>
                                                    <td width="100">${$cart['item'][i].fQuoteTotal}</td>
                                               </tr>
                                            {/section}
                                        </tbody>
                                    </table>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            <div class="grid_9">
                                <span class="subtotal"><span class="subtotal_lbl">Sub Total:</span>&nbsp;<span class="subtotal_amt">${$cart['SUBTOTAL']}</span></span>
                                <span class="subtotal"><span class="subtotal_lbl">Turn Around Time Changes ({$cart['vTurnAroundTime']}):</span>&nbsp;<span class="subtotal_amt">${$cart['fTurnAroundTimeCharge']}</span></span>
                                <span class="subtotal"><span class="subtotal_lbl">Coupon Discount (Code : {$cart['vCouponCode']}):</span>&nbsp;<span class="subtotal_amt">${$cart['fCouponDiscount']}</span></span>
                                <div class="total">Total : <span class="bigprice">${$cart['GRANDTOTAL']}</span></div>
						  <div id="teting"></div>						  
                                <div class="buttons-set" id="buttons_set">
                                    <button class="button pull-right" title="Submit"  id="ordernow"type="button" onclick="orderNow();"><span><span>Order Now</span></span></button>
                                </div>
                            </div>
                        </div>
                    </div>				
                </div>
            </div>
        </div>
{include file="footer.tpl"}
{literal}
<script>
    var isGuest = 1;
    function orderNow() {
      
        var validate = true;
        
        // Billing Address Validation
        if(!$('#vBillFirstname').val()) {
            $("#vBillFirstname").addClass("borderValidations");$("#vBillFirstname").focus(); $("#vBillFirstnameDiv").show(); validate = false;
        }else{
            $("#vBillFirstname").removeClass("borderValidations"); $('#vBillFirstnameDiv').hide();
        }
        
        if(!$('#vBillLastname').val()) {
            $("#vBillLastname").addClass("borderValidations"); $("#vBillLastname").focus(); $("#vBillLastnameDiv").show(); validate = false;
        }else{
            $("#vBillLastname").removeClass("borderValidations"); $('#vBillLastnameDiv').hide();
        }
       
        if(!$('#vBillAddress').val()) {
            $("#vBillAddress").addClass("borderValidations"); $("#vBillAddress").focus(); $("#vBillAddressDiv").show(); validate = false;
        }else{
            $("#vBillAddress").removeClass("borderValidations"); $('#vBillAddressDiv').hide();
        }
       
        if(!$('#vBillCity').val()) {
            $("#vBillCity").addClass("borderValidations"); $("#vBillCity").focus(); $("#vBillCityDiv").show(); validate = false;
        }else{
            $("#vBillCity").removeClass("borderValidations"); $('#vBillCityDiv').hide();
        }
        
        if(!$('#vBillPostcode').val()) {
            $("#vBillPostcode").addClass("borderValidations"); $("#vBillPostcode").focus(); $("#vBillPostcodeDiv").show(); validate = false;
        }else{
            $("#vBillPostcode").removeClass("borderValidations"); $('#vBillPostcodeDiv').hide();
        }
        
        if(!$('#iBillCountryId').val()) {
            $("#iBillCountryId").addClass("borderValidations"); $("#iBillCountryId").focus(); $("#iBillCountryIdDiv").show(); validate = false;
        }else{
            $("#iBillCountryId").removeClass("borderValidations"); $('#iBillCountryIdDiv').hide();
        }
        
        if(!$('#iBillStateId').val()) {
            $("#iBillStateId").addClass("borderValidations"); $("#iBillStateId").focus(); $("#iBillStateIdDiv").show(); validate = false;
        }else{
            $("#iBillStateId").removeClass("borderValidations"); $('#iBillStateIdDiv').hide();
        }
        
        if(!$('#vBillEmail').val()) {
            $("#vBillEmail").addClass("borderValidations"); $("#vBillEmail").focus(); $("#vBillEmailDiv").show(); validate = false;
        }else{
            $("#vBillEmail").removeClass("borderValidations"); $('#vBillEmailDiv').hide();
            var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (!emailRegexStr.test($('#vBillEmail').val())) {
                $("#vBillEmail").addClass("borderValidations"); $("#vBillEmail").focus(); $("#vBillInvalidEmailDiv").show(); validate = false;
            }else{
                $("#vBillEmail").removeClass("borderValidations"); $('#vBillInvalidEmailDiv').hide();
            }
        }
        
        if(!$('#vBillPhone').val()) {
            $("#vBillPhone").addClass("borderValidations"); $("#vBillPhone").focus(); $("#vBillPhoneDiv").show(); validate = false;
        }else{
            $("#vBillPhone").removeClass("borderValidations"); $('#vBillPhoneDiv').hide();
        }
        
        //New User Registration Validation
        if (!isGuest) {
            if(!$('#vUsernameNew').val()) {
                $("#vUsernameNew").addClass("borderValidations");$("#vUsernameNew").focus(); $("#vUsernameNewDiv").show(); validate = false;
            }else{
                $("#vUsernameNew").removeClass("borderValidations"); $('#vUsernameNewDiv').hide();
            }
            
            if(!$('#vPasswordNew').val()) {
                $("#vPasswordNew").addClass("borderValidations");$("#vPasswordNew").focus(); $("#vPasswordNewDiv").show(); validate = false;
            }else{
                $("#vPasswordNew").removeClass("borderValidations"); $('#vPasswordNewDiv').hide();
            }
            
            if(!$('#vConfirmPasswordNew').val()) {
                $("#vConfirmPasswordNew").addClass("borderValidations");$("#vConfirmPasswordNew").focus(); $("#vConfirmPasswordNewDiv").show(); validate = false;
            }else{
                $("#vConfirmPasswordNew").removeClass("borderValidations"); $('#vConfirmPasswordNewDiv').hide();
                if($('#vPasswordNew').val() != $('#vConfirmPasswordNew').val()) {
                    $("#vConfirmPasswordNew").addClass("borderValidations");$("#vConfirmPasswordNew").focus(); $("#vConfirmPasswordNotMatchDiv").show(); validate = false;
                }else{
                    $("#vConfirmPasswordNew").removeClass("borderValidations"); $('#vConfirmPasswordNotMatchDiv').hide();
                }
            }
        }
        
        // Shipping Address Validation
        if(!$('#vShipFirstname').val()) {
            $("#vShipFirstname").addClass("borderValidations");$("#vShipFirstname").focus(); $("#vShipFirstnameDiv").show(); validate = false;
        }else{
            $("#vShipFirstname").removeClass("borderValidations"); $('#vShipFirstnameDiv').hide();
        }
        
        if(!$('#vShipLastname').val()) {
            $("#vShipLastname").addClass("borderValidations"); $("#vShipLastname").focus(); $("#vShipLastnameDiv").show(); validate = false;
        }else{
            $("#vShipLastname").removeClass("borderValidations"); $('#vShipLastnameDiv').hide();
        }
       
        if(!$('#vShipAddress').val()) {
            $("#vShipAddress").addClass("borderValidations"); $("#vShipAddress").focus(); $("#vShipAddressDiv").show(); validate = false;
        }else{
            $("#vShipAddress").removeClass("borderValidations"); $('#vShipAddressDiv').hide();
        }
       
        if(!$('#vShipCity').val()) {
            $("#vShipCity").addClass("borderValidations"); $("#vShipCity").focus(); $("#vShipCityDiv").show(); validate = false;
        }else{
            $("#vShipCity").removeClass("borderValidations"); $('#vShipCityDiv').hide();
        }
        
        if(!$('#vShipPostcode').val()) {
            $("#vShipPostcode").addClass("borderValidations"); $("#vShipPostcode").focus(); $("#vShipPostcodeDiv").show(); validate = false;
        }else{
            $("#vShipPostcode").removeClass("borderValidations"); $('#vShipPostcodeDiv').hide();
        }
        
        if(!$('#iShipCountryId').val()) {
            $("#iShipCountryId").addClass("borderValidations"); $("#iShipCountryId").focus(); $("#iShipCountryIdDiv").show(); validate = false;
        }else{
            $("#iShipCountryId").removeClass("borderValidations"); $('#iShipCountryIdDiv').hide();
        }
        
        if(!$('#iShipStateId').val()) {
            $("#iShipStateId").addClass("borderValidations"); $("#iShipStateId").focus(); $("#iShipStateIdDiv").show(); validate = false;
        }else{
            $("#iShipStateId").removeClass("borderValidations"); $('#iShipStateIdDiv').hide();
        }
        
        if(!$('#vShipEmail').val()) {
            $("#vShipEmail").addClass("borderValidations"); $("#vShipEmail").focus(); $("#vShipEmailDiv").show(); validate = false;
        }else{
            $("#vShipEmail").removeClass("borderValidations"); $('#vShipEmailDiv').hide();
            var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (!emailRegexStr.test($('#vShipEmail').val())) {
                $("#vShipEmail").addClass("borderValidations"); $("#vShipEmail").focus(); $("#vShipEmailInvalidDiv").show(); validate = false;
            }else{
                $("#vShipEmail").removeClass("borderValidations"); $('#vShipEmailInvalidDiv').hide();
            }
        }
        
        if(!$('#vShipPhone').val()) {
            $("#vShipPhone").addClass("borderValidations"); $("#vShipPhone").focus(); $("#vShipPhoneDiv").show(); validate = false;
        }else{
            $("#vShipPhone").removeClass("borderValidations"); $('#vShipPhoneDiv').hide();
        }
        
        
        
        //Agreement Validation
        if(!$('#eAgree').is(':checked')){
            $("#eAgree").addClass("borderValidations"); $("#eAgree").focus(); $("#eAgreeDiv").show(); validate = false;
        }else{
            $("#eAgree").removeClass("borderValidations"); $('#eAgreeDiv').hide();
        }
        
        
        //Credit Card Validation
        if (paymentMethod == 'CreditCard') {
            if(!$('#firstnameOnCard').val()) {
                $("#firstnameOnCard").addClass("borderValidations");$("#firstnameOnCard").focus(); $("#firstnameOnCardDiv").show(); validate = false;
            }else{
                $("#firstnameOnCard").removeClass("borderValidations"); $('#firstnameOnCardDiv').hide();
            }
            
            if(!$('#lastnameOnCard').val()) {
                $("#lastnameOnCard").addClass("borderValidations");$("#lastnameOnCard").focus(); $("#lastnameOnCardDiv").show(); validate = false;
            }else{
                $("#lastnameOnCard").removeClass("borderValidations"); $('#lastnameOnCardDiv').hide();
            }
            
            if(!$('#cardType').val()) {
                $("#cardType").addClass("borderValidations");$("#cardType").focus(); $("#cardTypeDiv").show(); validate = false;
            }else{
                $("#cardType").removeClass("borderValidations"); $('#cardTypeDiv').hide();
            }
            
            if(!$('#cardNumber').val()) {
                $("#cardNumber").addClass("borderValidations");$("#cardNumber").focus(); $("#cardNumberDiv").show(); validate = false;
            }else{
                $("#cardNumber").removeClass("borderValidations"); $('#cardNumberDiv').hide();
            }
            
            if(!$('#expiryDate').val()) {
                $("#expiryDate").addClass("borderValidations");$("#expiryDate").focus(); $("#expiryDateDiv").show(); validate = false;
            }else{
                $("#expiryDate").removeClass("borderValidations"); $('#expiryDateDiv').hide();
            }
            
            if(!$('#cvvNumber').val()) {
                $("#cvvNumber").addClass("borderValidations");$("#cvvNumber").focus(); $("#cvvNumberDiv").show(); validate = false;
            }else{
                $("#cvvNumber").removeClass("borderValidations"); $('#cvvNumberDiv').hide();
            } 
        }
            
        //return false;
        if (validate) {
            
            var billing_address = $('#frmBillingAddress').serialize();
            var shipping_address = $('#frmShippingAddress').serialize();
            var tComment = $("#tComment").val();
            var creditcard_info = $('#frmCreditCard').serialize();

            var data = billing_address+'&'+shipping_address+'&tComment='+tComment+'&payment_method='+paymentMethod+'&'+creditcard_info+'&isGuest='+isGuest;
            var url = site_url+"checkout/save_order";
            var pars = data;
		  //alert(url+data);return false;
		  $('#ordernow').html('<div style="color:#c03b37; float:right; font-size:15px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Saving...</div>');
            $.ajax({
                type: "POST",
                url: url,
                data: pars,
                success: function(data){
				
				//$("#teting").html(data);
				//$('#buttons_set').html('<button class="button pull-right" title="Submit"  id="ordernow"type="button" onclick="orderNow();"><span><span>Order Now</span></span></button>');
				//return false;
                    var response=data.split('|');
                    if (response[0] == 'Failure') {
                        //$('#placeorder-btn').html('<input type="submit" name="updater" id="updater" value="Order Now" class="mailsubmit" onclick="placeOrder();">');
                        $('#paymentErrorMsg').html(response[1]);
                    }else if (response[0] == 'success' && response[1] == 'creditcard') {
                        var orderId = response[2];
                        window.location = site_url+"checkout/success?orderId="+orderId;
                    }else if (response[0] == 'pending' && response[1] == 'paypal') {
                        var orderId = response[2];
                        window.location = site_url+"checkout/paypal?orderId="+orderId;
                    }else if (response[0] == 'pending' && response[1] == 'Authorizedotnet') {
                        var orderId = response[2];
                        window.location = site_url+"checkout/Authorizedotnet?orderId="+orderId;
                    }
                }
            });
            
        }else{
            return false;
        }
    }
    $('#same_as_billing').click(function(){
        if ($(this).is(':checked')){
            $('#vShipFirstname').val($('#vBillFirstname').val());
            $('#vShipLastname').val($('#vBillLastname').val());
            $('#vShipCompany').val($('#vBillCompany').val());
            $('#vShipAddress').val($('#vBillAddress').val());
            $('#vShipCity').val($('#vBillCity').val());
            $('#vShipPostcode').val($('#vBillPostcode').val());
            $('#vShipEmail').val($('#vBillEmail').val());
            $('#vShipPhone').val($('#vBillPhone').val());
            var iCountryId = $('#iBillCountryId').val();
            $('#iShipCountryId').find('option').each(function() {
                    var countryId = $(this).attr('value');
                    if (countryId == iCountryId) {
                        $(this).attr('selected','selected');
                        getShippingStates(iCountryId);
                    }else{}
            });
        }else{
            $('#vShipFirstname').val('');
            $('#vShipLastname').val('');
            $('#vShipCompany').val('');
            $('#vShipAddress').val('');
            $('#vShipCity').val('');
            $('#vShipPostcode').val('');
            $('#iShipCountryId').val('');
            $('#iShipStateId').val('');
            $('#vShipEmail').val('');
            $('#vShipPhone').val('');
        }
        
    });
    
    $("#newUser1").hide();
    $("#newUser2").hide();
    function registerAccount() {
        isGuest = 0;
        $("#newUser1").show();
        $("#newUser2").show();
    }
    function guestCheckout() {
        isGuest = 1;
        $("#newUser1").hide();
        $("#newUser2").hide();
    }
    
    
</script>
<script type="text/javascript">
    var isLoggedIn = '{/literal}{$isLoggedIn}{literal}';
    if(isLoggedIn == '1') {
        iCountryId = '{/literal}{$userAddress.iCountryId}{literal}';
        getBillingStates(iCountryId);
    }
    function getBillingStates(icountryid){
        var extra ='';
        extra+='?icountryid='+icountryid;
        var url = site_url + 'registration/getstates';
        var pars = extra;
        $.post(url+pars,
        function(data) {
            if(data != ''){
                $('#iBillStateId').html(data);
                var iStateId = '{/literal}{$userAddress.iStateId}{literal}';
                $('#iBillStateId').find('option').each(function() {
			var stateId = $(this).attr('value');
			if (stateId == iStateId) {
                            $(this).attr('selected','selected');
			}else{}
		});
            }else{}
        });
    }
    function getShippingStates(icountryid){
        var extra ='';
        extra+='?icountryid='+icountryid;
        var url = site_url + 'registration/getstates';
        var pars = extra;
        $.post(url+pars,
        function(data) {
            if(data != ''){
                $('#iShipStateId').html(data);
                if ($("#same_as_billing").is(':checked')){
                    iStateId = $("#iBillStateId").val();
                    $('#iShipStateId').find('option').each(function() {
                            var stateId = $(this).attr('value');
                            if (stateId == iStateId) {
                                $(this).attr('selected','selected');
                            }else{}
                    });
                }
            }else{}
        });
    }
    function validateLoginForm() {
        var validate = true;
        if(!$('#vUsername').val()){
            $("#vUsername").addClass("borderValidations"); $("#vUsernameDiv").show();
            validate = false;
        }else{
          $("#vUsername").removeClass("borderValidations"); $('#vUsernameDiv').hide();
        }
        
        if(!$('#vPassword').val()){
            $("#vPassword").addClass("borderValidations"); $("#vPasswordDiv").show();
            validate = false;
        }else{
          $("#vPassword").removeClass("borderValidations"); $('#vPasswordDiv').hide();
        }
        
        return validate;
    }
</script>
{/literal}