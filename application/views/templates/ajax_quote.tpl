<div class="product_tab_left">
    <table>	   
	<tr>
	    <td>
		Product name :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		{$quoteSummary['vProductName']}
	    </td>
	</tr>
	<tr>
	    <td>
		Size & Color :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		{section name=i loop=$quoteSummary['vColorSizeDescription']}
		    {$quoteSummary['vColorSizeDescription'][i]}
		    <br>
		{/section}
	    </td>
	</tr>
	<tr>
	    <td>
		Print Locations :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		{section name=i loop=$quoteSummary['vPrintLocationsDescription']}
		    {$quoteSummary['vPrintLocationsDescription'][i]}
		    <br>
		{/section}
	    </td>
	</tr>
	<tr>
	    <td>
		Total Qty :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		{$quoteSummary['totalQty']}
	    </td>
	</tr>
    </table>
    <a class="link_quot margin_align" href="#getAQuoteID">Revise Quote</a><br>
    <!--<a class="link_quot">Need better deal? Order more to lower cost per peice</a>-->
    
    <br><br>    
    
    
</div>
<div class="prod_tab_right">
    <table class="product_detail_quote_table">
	<tr>
	    <td class="quote_subtotal">Cost Per Piece: </td><td class="quote_subtotal">$ {$costPerPiece}</td>
	</tr>
	<tr>
	    <td class="quote_subtotal">Sub Total : </td><td class="quote_subtotal">$ {$subTotal}</td>
	</tr>
	<!--<tr>
	    <td class="quote_subtotal">Shipment Charge : </td><td class="quote_subtotal">$ {$shipmentCharge}</td>
	</tr>-->
	<tr>
    	    <td class="quote_subtotal">Coupon Discount : </td><td class="quote_subtotal">$ {$couponDiscount}</td>
	</tr>
	<tr>
	    <td class="quote_subtotal">Total Cost : </td><td class="quote_subtotal">$ {$totalQuote}</td>
	</tr>
	<tr>
	    <td class="quote_subtotal"></td>
	    <td class="quote_subtotal" id="Quote"></td>
	</tr>
	
	
    </table>
	<a class="link_quot margin_align_right"  href="#save_quote" id="saveQuote">Save Quote</a>
    <div>
  <!--   <a style="color: #9E053B; font-size: 16px; text-align: center;" href="#save_quote" id="saveQuote">Save Quote</a>  -->
    </div>
    
</div>
<div class="note_quote_price">
	<p>Note: Pricing includes standard shipping, all set up fees, and professional art review.</p>
    </div>
	<div class="dont-forger_guot"><p >Don't forget to order more and save!</p>
		<table class="table_order_quote">    
			<tr>
			   <th>Quantity</th>
			   <th>Each</th>
			   <th>Total</th>	   
			</tr>
			{section name=i loop=$sortQtyRange}
			<tr>
			   <td>{$sortQtyRange[i]['iStartRange']}</td>
			   <td>${$sortQtyRange[i]['Each']}</td>
			   <td>${$sortQtyRange[i]['Total']}</td>	   
			</tr>
			{/section}   
		</table>  
	</div>