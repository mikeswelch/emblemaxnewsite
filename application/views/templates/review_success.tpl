{include file="header.tpl"}
  
<div class="main-container col2-right-layout">
<form action="{$site_url}registration/check_register" method="post" id="form-validate" onsubmit="return CheckRegister();">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="main container">
 	    <div class="slideshow_static"></div>
                <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>			
                   <div class="main-inner">    
                    <div class="col-main">
                       <div class="account-create">
    			<h2 class="pagetital">Your Review</h2>
			<div style="clear:both;">{if $msg}
			    <ul class="messages" style="margin-top: 15px;">
				<li class="success-msg">
				  <ul>
				     <li>
				     <span>{$msg}</span>
				     </li>
				  </ul>
				</li>
			    </ul>
			    {/if}
			</div>
		       </div>		
                    </div>					
                  </div>
                  <div class="buttons-set">
                    <p class="back-link"><a href="{$site_url}home" class="back-link"><small>&laquo; </small>Back</a></p>
                  </div>
	        </div>
	
 	    </div>
	</div>		 		 
</div>
{include file="footer.tpl"}