{include file="header.tpl"}
<div class="main-container col1-layout">
						            <div class="main container">
			<div class="slideshow_static">
                                            </div>
                <div class="main-inner row-fluid">
                    <div class="span12">
												<div class="col-main">
							<div class="col-main-inner">
																<div class="page-title">
    <h1>Forgot Your Password?</h1>
</div>
<form action="#" method="post" id="form-validate">
    <div class="fieldset">
        <h2 class="legend">Retrieve your password here</h2>
        <p>Please enter your email address below. You will receive a link to reset your password.</p>
        <ul class="form-list row-fluid show-grid">
            <li>
                <label for="email_address" class="required"><em>*</em>Email Address</label>
                <div class="input-box">
                    <input type="text" id="vEmail" name="Data[vEmail]" alt="email" id="email_address" class="input-text required-entry validate-email span3" value="" />
                </div>
		<div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
            </li>
                    </ul>
    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="#"><small>&laquo; </small>Back to Login</a></p>
        <button type="button" title="Submit" class="button" onclick="return CheckUser();"><span><span>Submit</span></span></button>
    </div>
</form>
<script type="text/javascript">
//<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
//]]>
</script>
							</div>
						</div>
					</div>
                </div>
							    </div>
</div>
{include file="footer.tpl"}
{literal}
<script type="text/javascript">
function CheckUser()
{
	var email = $('#vEmail').val();
	var validate = true;
	var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	var isvalid = emailRegexStr.test(email);
    
	if( $('#vEmail').val() == ''){
	$("#vEmail").addClass("borderValidations");
	    $("#emailDiv").html('This is a required field.');
	    $("#emailDiv").show();
	    validate = false;
	}
      else if (!isvalid) {
	    $("#vEmail").addClass("borderValidations");
	    $("#emailDiv").html('Enter Your Email Address like demo@domain.com');
	    $("#emailDiv").show();
	    validate = false;
	}
       else{
	  $("#emailDiv").hide();
	  $("#vEmail").removeClass("borderValidations");
	  }
       if (validate)
	{
	    var extra = '';
	    extra+='?vEmail='+email;
	    extra+='&mode=forgotpassword';     
	    var url = site_url + 'authentication/check_login';
	    var pars = extra;
	    $.post(url+pars,
	    function(data) {
		if(data == 'invalid'){
			window.location = site_url+'login';
		}
		if(data == 'success'){
			window.location = site_url+'login';
		}
	});
		return true;
	}
	else
	{
		return false;
	} 
}
</script>
{/literal}