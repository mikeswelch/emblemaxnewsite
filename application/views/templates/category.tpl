{include file="header.tpl"}
<div class="main-container col2-right-layout">
    <div class="main">
	  <div class="main-inner clearfix">				
		<div class="breadcrumbs row-fluid">
		  
		    <div class="container">
			 <ul class="span12" style="text-align:left;">
				<li class="home">
				    <a href="#" title="Go to Home Page">Home</a>
				    <samp></samp>
				</li> 
				<li class="category5">
				    <strong>Categories</strong>
				</li> 
			 </ul>
		    </div>	
		</div>
		<div class="container">
		    <div class="row-fluid show-grid">
			  <div class="col-main span9">		
				<div class="col-main-inner">
				    <p class="category-image" style="display:block;"></p>
				    <div class="category-products">
					  <div class="toolbar"></div>
					  <div id="pageData">
						{if $all_category_data|@count gt 0}
						{section name=i loop=$all_category_data}
						<div class="products-grid row-fluid show-grid">
						    <div class="item first span4">
							  <div class="item-inner content">
								<div class="product-image">
								    <div class="product-thumb">
								    <div class="product_img_box">
									  <a href="{$site_url}category/{$all_category_data[i]['vUrlText']}" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
									  <img  src="{$upload_path}productcategory/{$all_category_data[i]['iCategoryId']}/1X160_{$all_category_data[i]['vImage']}" alt="Cras in risus et risus" />
									  </a>
								    </div>
								    </div>
								</div>
								<div class="mask" onclick="setLocation('#')">							
								    <h3 class="product-name">
								    <a href="{$site_url}category/{$all_category_data[i]['vUrlText']}" title="Cras in risus et risus">{$all_category_data[i]['vCategory']}</a>
								    </h3>
								</div>	
								<div class="top-actions-inner">
								    <div class="mt-actions clearfix">
								    </div>					
								</div>
							  </div>
						    </div>
						</div>
						{/section}
						{else}
						<span>No Item Found</span>
						{/if}
						<div class="mt-row-page">
						  <div id="ajax_paging">
						  <div class="Pagingbox">
							   {$pages}
						   </div>
						   {$recmsg}
						  </div>
					   </div>
					  </div>
					 <!--  <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script> -->
					  <div class="toolbar-bottom">
						<div class="toolbar"></div>
					  </div>
				    </div>
				</div>
			  </div>
			  <div class="col-right sidebar span3 visible-desktop"> 
				<div class="block mt-smartmenu">
				    <div class="block-title">
					  <strong><span>Category</span></strong>
				    </div>
				    <div class="title-divider">
					  <span>&nbsp;</span>
				    </div>
				    <div class="block-content">
					  {literal}	
					  <!-- <script type="text/javascript" src="js/mt.accordion.js"></script> -->
					  <script type="text/javascript">
						$mtkb(document).ready(function(){	
						// applying the settings
						$mtkb("#mt-accordion li.active span.head").addClass("selected");
						$mtkb('#mt-accordion').Accordion({
						active: 'span.selected',
						header: 'span.head',
						alwaysOpen: false,
						animated: true,
						showSpeed: 400,
						hideSpeed: 800,
						event: 'click'
						});
						});	
					  </script>
					  {/literal}
					  <ul id="mt-accordion" class="clearfix">
						{if $categories|@count gt 0}
						{section name=i loop=$categories}
						{if $categories[i]->iParentId eq '0'}
						<li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
						    <a href="{$site_url}category/{$categories[i]->vUrlText}"><span>{$categories[i]->vCategory}</span></a><span class="head"><a href="#" style="float:right;"></a></span>
						    <ul class="level0">
							  {section name=j loop=$categories}
							  {if $categories[j]->iParentId neq '0' AND $categories[j]->iParentId eq $categories[i]->iCategoryId}
							  <li class="level1 nav-categories-footwear-man">
								<a href="{$site_url}category/{$categories[j]->vUrlText}"><span>{$categories[j]->vCategory}</span></a>
							  </li>
							  {/if}
							  {/section}
						    </ul>
						</li>
						{/if}
						{/section}
						{/if}
					  </ul>
				    </div> 
				</div>
			  </div>
		    </div>
		</div>
	  </div>
	  <div class="mt-productcroller-container clearfix"></div>
    </div>
</div>

{literal}
<script type='text/javascript'>
function changePagination(start){
    //alert(pageId);
    //alert(liId);
    var q = "{/literal}{$alp}{literal}";
    //alert(q);
    if(q){
	   var dataString = 'start='+ start+'&q='+q;
    }else{
	   var dataString = 'start='+ start;
    }
    var url = site_url+'category/ajaxpage';
    //alert(url);
    //var dataString = 'start='+ start;
    //alert(dataString);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		$("#pageData").html(result);
	  }
    });
}
</script>

{/literal}
{include file="footer.tpl"}