<br><br><br><br>
<table class="areas" style="">
 
<tbody>
{section name=i loop=$printLocations}
    <tr  id="screenprint1_{$printLocations[i]['iPrintLocationId']}">
	<td style="width: 15%; padding-left: 10px;"><span class="areaName" style="color:#002F5F">{$printLocations[i]['vPrintLocation']}</span></td>
	<td style="padding: 10px; width: 30%;">
	    <select name="decoration_type[{$printLocations[i]['iPrintLocationId']}]" style="" onchange="selectDecorationType(this.value,'{$printLocations[i]['iPrintLocationId']}')">
		<option value="Screenprinting" selected="selected">Screenprinting</option>
		{if $start gt 0}
		<option value="embroidery">Embroidery</option>
		{/if}
	    </select>
	</td>
	<td>
	    <div id="screenprint2_{$printLocations[i]['iPrintLocationId']}" style=" padding: 5px 0px 0px 10px;" >
		<span style="float: left; margin: 0px 5px 0px 0px;">Number of colors: </span>
		<select name="number_of_colors[numberofcolors_{$printLocations[i]['iPrintLocationId']}]" class="screenprinting_colors" style="">
		    {section name=j loop=$numberColors} 
		    <option value="printlocation-{$printLocations[i].iPrintLocationId}-numbercolor-{$numberColors[j].iNumberColorsId}">{$numberColors[j].vNumberColor}</option>
		    {/section}
		</select>
	    </div>
	</td>
    </tr>
    {/section}
    
</tbody>
</table>

{literal}
<script>
function selectDecorationType(decorationType, iPrintLocationId) {		    
		    if (decorationType == 'screenprinting') {
			$('#screenprint2_'+iPrintLocationId).show();
		    }else{
			$('#screenprint2_'+iPrintLocationId).hide();
		    }	    
		  }
				
				
				
				
</script>
{/literal}