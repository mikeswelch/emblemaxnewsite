{include file="header.tpl"}
			<div class="main-container col2-right-layout">
                
			 
			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
						            <div class="main container">
			<div class="slideshow_static">
                                            </div>
                <div class="main-inner">
                    
                    <div class="col-main">
                                                <div class="account-create">
    			<h2 class="pagetital subtit">My Dashboard</h2>		
			<div style="clear:both;"></div>
			{if $msg}
			<ul class="messages" style="margin-top: 15px;">
			    <li class="success-msg">
			      <ul>
				 <li>
				 <span>{$msg}</span>
				 </li>
			      </ul>
			    </li>
			</ul>
			{/if}	
            <div class="ordleftpart">
		  
		  <div class="col-main-wap span9">
		<div class="col-main">
		<div class="col-main-inner">
		<div class="my-account"><div class="dashboard">
    
        <div class="welcome-msg">
    <p class="hello"><strong>Hello, {$UserDetails[0]->vFirstName}&nbsp; {$UserDetails[0]->vLastName}</strong></p>
    <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
</div>
    <div class="box-account box-recent">
    <div class="box-head">
        <h2>Recent Orders</h2>
        <a href="#">View All</a>    </div>
    <table id="shopping-cart-table" class="data-table cart-table" style="border-bottom:2px solid #EAEAEA">
                <col width="1" />
                <col />
                <col width="1" />
                                        <col width="1" />
                                        <col width="1" />
                            <col width="1" />
                                        <col width="1" />

                            <thead class="hidden-phone">
                    <tr class="">
                        <th class="" rowspan="1"><span class="nobr">Order Id</span></th>
                        <th width="70" rowspan="1" class=""><span class="nobr">Date Added</span></th>
                        <th width="200" rowspan="1" class=""><span class="nobr">Product</span></th>
                        <!--<th colspan="1"><span class="nobr">Model</span></th>-->
                        <th rowspan="1"><span class="nobr">Status</span></th>
                        <th colspan="1"><span class="nobr">Price</span></th>
                        <th rowspan="1"><span class="nobr">Details</span></th>
                    </tr>
                                    </thead>
                
                <tbody>
               {if $orderData|@count gt 0}
		{section name=i loop=$orderData}
			<tr>
			   <td style="padding-bottom: 25px;">{$orderData[i]['vInvoiceNum']}</td>
			   <td width="7">{$orderData[i]['dAddedDate']|@date_format}</td>
			   <td width="200">{$orderData[i]['vProductName']}</td>
			   <!--<td>PR - 3</td>-->
			   <td>{$orderData[i]['status']}</td>
			   <td>${$orderData[i]['fGrandTotal']}</td>
			   <td><a href="{$site_url}myAccount/showOrderDetail?iOrderId={$orderData[i]['iOrderId']}" class="viewdet">View Details</a></td>
		       </tr>
                {/section}
                {else}
                <tr>
                        <td height="20px;" colspan="8" style="text-align:center; color:#38414B; font-size:14px; font-weight:bold;">No Records Found.</td>
                </tr>
                {/if}
                                </tbody>
            </table>
    <script type="text/javascript">decorateTable('my-orders-table')</script>
</div>
    <div class="box-account box-info">
        <div class="box-head">
            <h2>Account Information</h2>
        </div>
                        <div class="col2-set row-fluid show-grid">
    <div class="col-1 span6">
        <div class="box">
            <div class="box-title">
                <h3>Contact Information</h3>
                <a href="{$site_url}myAccount/myAccount">Edit</a>
            </div>
            <div class="box-content">
                <p>
                    {$UserDetails[0]->vFirstName}&nbsp;{$UserDetails[0]->vLastName}<br />
                    {$UserDetails[0]->vEmail}<br />
                    <a href="{$site_url}myAccount/changepass">Change Password</a>
                </p>
            </div>
        </div>
    </div>
        <div class="col-2 span6">
        <!--<div class="box">
            <div class="box-title">
                <h3>Newsletters</h3>
                <a href="#">Edit</a>
            </div>
            <div class="box-content">
                <p>You are currently not subscribed to any newsletter.</p>
            </div>
        </div>-->
                    </div>
    </div>
        <div class="col2-set row-fluid show-grid">
    <div class="box">
        <div class="box-title">
            <h3>Address Book</h3>
            <a href="{$site_url}myAccount/address">Manage Addresses</a>
        </div>
        <div class="box-content">
            <div class="col-1 span6">
                <h4>Default Billing Address</h4>
		{if $BillShipDetails|@count gt '0'}
		    <address>
			{$BillShipDetails[0]->vBillingFirstName}&nbsp;{$BillShipDetails[0]->vBillingLastName}<br/>
			{$BillShipDetails[0]->tBillingStreetAddress1}<br />
			{$BillShipDetails[0]->tBillingStreetAddress2}<br />
			{$BillShipDetails[0]->vBillingCity}<br />
			{section name=i loop=$state}
			<div>{if $state[i]->iStateId eq $BillShipDetails[0]->iBillingStateId} {$state[i]->vState} {/if}</div>
			{/section}
			
			{section name=j loop=$country}
			<div>{if $country[j]->iCountryId eq $BillShipDetails[0]->iBillingCountryId} {$country[j]->vCountry} {/if}</div>
			{/section}
			{$BillShipDetails[0]->vBillingZipCode}<br/>
			T:{$BillShipDetails[0]->vBillingTelephone}
			{if $BillShipDetails[0]->vBillingFax neq ''}<br/>F:{$BillShipDetails[0]->vBillingFax}{/if}<br/>
			<a href="{$site_url}myAccount/edit_address">Edit Address</a>
		    <address>                    
		    {else}
		    </address>
		    You have not set a default billing address.<br/>
		    <a href="{$site_url}myAccount/edit_address">Edit Address</a>
		    {/if}
                </address>
            </div>
            <div class="col-2 span6">
                <h4>Default Shipping Address</h4>
		{if $ShippingDetails|@count gt '0'}
		<address>
			{$ShippingDetails[0]->vShippingFirstName}&nbsp;{$ShippingDetails[0]->vShippingLastName}<br/>
			{$ShippingDetails[0]->tShippingStreetAddress1}<br />
			{$ShippingDetails[0]->tShippingStreetAddress2}<br />
			{$ShippingDetails[0]->vShippingCity}<br />
			{section name=i loop=$state}
			<div>{if $state[i]->iStateId eq $ShippingDetails[0]->iShippingStateId} {$state[i]->vState} {/if}</div>
			{/section}
			
			{section name=j loop=$country}
			<div>{if $country[j]->iCountryId eq $ShippingDetails[0]->iShippingCountryId} {$country[j]->vCountry} {/if}</div>
			{/section}
			{$ShippingDetails[0]->vShippingZipCode}<br/>
			T:{$ShippingDetails[0]->vShippingTelephone}
			{if $ShippingDetails[0]->vShippingFax neq ''}<br/>F:{$ShippingDetails[0]->vShippingFax}{/if}
			<address>
                        <a href="{$site_url}myAccount/edit_address">Edit Address</a>
		       {else}
		       </address>
		       You have not set a default Shipping address.<br/>
		       <a href="{$site_url}myAccount/edit_address">Edit Address</a>
		       {/if}
                </address>
            </div>
        </div>
    </div>
</div>
    </div>
        </div></div></div>
								</div>
							</div>
</div>
<div style="margin-top: 90px;">{include file="right_myAccount.tpl"}</div>
</div>
                    </div>
					
                </div>				
				
									
								
            </div>
        </div>
			 
			 
        </div>
		
{include file="footer.tpl"}
{literal}
<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
</script>
<script type="text/javascript">decorateDataList('narrow-by-list')</script>
{/literal}