<script type="text/javascript" src="{$front_js_path}jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery.ui.widget.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery.fiji.ticker.js"></script>

{literal}
	<script type="text/javascript">
		jq180 = jQuery.noConflict(true);
		
		jq180(function() {
			var i = 0;
			jq180("#ticker").ticker({
				initialTimeout: 1000,
				mouseOnTimeout: 4000,
				mouseOffTimeout: 3000,
				scrollTime: 1200,
				fadeTime: 1000,
				fixContainer: true,
				nextItem: function(lastItem) {
					return lastItem;
				}
			});
		})
	</script>
{/literal}
<div class="mt-col-right">
        <div class="mt-right-static" style="border:1px solid #e1e2e2;">
			{if $review|@count gt 0}
	   		<h3 class="customerhd">Customer Reviews</h3>
			<div class="reviewpart">
				<div class="datepart">{$review[0]['dAddedDate']|date_format:"%D"}</div>
				<div class="revtxt">{$review[0]['tDescription']|truncate:100}...</div>
				<div class="addname">{$review[0]['vCustomerName']}<br />
				{$review[0]['vCity']},{section name=i loop=$db_country}{if $db_country[i]['iCountryId'] eq $review[0]['iCountryId']}{$db_country[i]['vCountry']}{/if}{/section}
				</div>
				<div class="readmorelk"><a href="{$site_url}review">Read More...</a></div>
				<div class="readmorelk"><a class="subrev" href="{$site_url}review#reviewsubmission">Submit Your Review</a></div>
			</div>
			{/if}
			<div style="clear:both;"></div>
	   </div>
	   
	  <!-- //Twitter Part-->
	   
	   
	   <div class="span12">
	<div class="twitter">
	<div class="footer-static-title">
		<h3>twitter</h3>
	</div>
	        
		<!--<div id="ticker">
	{section name=i loop=$twitter_updates}        
			<div id="mt-twitter"><p>{$twitter_updates[i]->user->screen_name}<br /><a href="http://twitter.com/zootemplates" target="_blank"></a>
			{$twitter_updates[i]->text}<br /><a href="#" target="_blank">{$twitter_updates[i]->source}</a></p>
			    <div class="icontwitter clearfix"><span class="date pull-left">{$twitter_updates[i]->user->created_at}</span></div>
			</div>
			<div class=""></div>			 
	{/section}
		</div>-->
		<ul class="tweets" id="ticker" style="height: 200px; width: 297px !important; padding-bottom: 20px;">
                        {section name=i loop=$twitter_updates}
			<li>{$twitter_updates[i]->text}<a href="#">{$twitter_updates[i]->source}</a>
                            <span>{$twitter_updates[i]->created_at|date_format:"%A, %B %e, %Y"}</span>
                        </li>
                        {/section}
		</ul>

	<!--<a href="#" class="prev_text" style="width:81px; float:left; text-align:right; margin-top:-10px; color:#484848; font-weight:bold; background:url(images/Previous_img.png) no-repeat 15px 7px;">Previous</a><a href="#" class="prev_text" style=" float:right; width:41px; margin:-10px 10px 10px 0; color:#484848; font-weight:bold; background:url(images/Previous_right.png) no-repeat right 8px;">Next</a>-->				</div>
</div>
</div>
<!--<div class="revbtn"><a href="{$site_url}review" class="revlk">Submit Reviews</a></div>-->