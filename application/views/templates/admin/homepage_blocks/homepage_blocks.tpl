<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}homepage_blocks/homepage_blockslist">Homepage Blocks</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Homepage Blocks{else}Edit Homepage Blocks{/if}</li>
		</ul>
	</div>
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg">
		<div class="pagetitle">
			{if $operation eq 'add'}
			Add Homepage Blocks
			{else}
			Edit Homepage Blocks
			{/if} </div>
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBlockslistId" id="iBlockslistId" value="{if $operation neq 'add'}{$data->iBlockslistId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<!--{if $operation eq 'add'}
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="en" />
				{else}
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="{$lang}" />
				{/if}-->
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Blocks Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="data[vTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vTitle}{/if}" lang="*" title="Title" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Blocks Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vImage"  name="vImage" title="Image" value="" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}"{if $data->vImage eq ''} lang="*" {/if} onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
				{if $operation neq 'add'}{if $data->vImage neq ''}
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg" >View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('{$data->iBlockslistId}','product');" class="delete_btnimg">Delete</a></div>
						<div style="display:none;" >
							<div id="popfancy""><img src="{$upload_path}/homepage_blocks/{$data->iBlockslistId}/282X320_{$data->vImage}"></div>
						</div>
						</div>
					{/if}{/if}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Blocks Url</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLinkUrl" name="data[vLinkUrl]" class="inputbox" value="{if $operation neq 'add'}{$data->vLinkUrl}{/if}" lang="*" title="vLinkUrl" />
				</div>
				
				<div class="inputboxes">
				     <label for="textarea"><span class="red_star">*</span></span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="data[iOrder]" lang="*" title="Order Number">
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						<option value="{$initOrder}" >{$initOrder++}</option>
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}	
						<option value="{$initOrder}"  {if $data->iOrder eq $initOrder}selected{/if}>{$initOrder++}</option>
						{/while}
						{/if}
					</select>
				</div>
				<div class="inputboxes">
				     <label for="textarea"><span class="red_star"></span> Description</label>
					<span class="collan_dot">:</span>
				     <textarea value="" title="tDescription" class="inputbox" title="Description" name="data[tDescription]" id="tDescription">{if $operation neq 'add'}{$data->tDescription}{/if}</textarea>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
					{if $operation eq 'add'}
						<input type="submit" value="Add Homepage Blocks" class="submit_btn" title="Add Homepage Blocks" onclick="return validate(document.frmadd);"/>
					{else}
						<input type="submit" value="Edit Homepage Blocks" class="submit_btn" title="Edit Homepage Blocks" onclick="return validate(document.frmadd);"/>
					{/if}
					<a href="{$admin_url}homepage_blocks/homepage_blockslist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>	
	</form>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
         	
         	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});
function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}homepage_blocks/deleteimage?iBlockslistId={/literal}{$data->iBlockslistId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
{/literal} 
