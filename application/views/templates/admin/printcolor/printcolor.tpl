<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}printcolor/printcolorlist">Print Color</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Print Color{else}Edit Print Color{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Print Color
			{else}
			Print Color			
			{/if}
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iNumberColorsId" id="iNumberColorsId" value="{if $operation neq 'add'}{$data->iNumberColorsId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Print Color Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vNumberColor" name="Data[vNumberColor]" class="inputbox" value="{if $operation neq 'add'}{$data->vNumberColor}{/if}" lang="*" title="Print Color Title" />
				</div>
		
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Print Color" class="submit_btn" title="Add Print Color" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Print Color" class="submit_btn" title="Edit Print Color" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}printcolor/printcolorlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

<script type="text/javascript">

   function ImageDelete(id,file1){
	
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}color/deleteimage?id={/literal}{$data->iColorId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
{/literal} 
