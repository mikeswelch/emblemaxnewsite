{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
    <div id="breadcrumb">
	  <ul>
		  <li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		  <li><a href="{$admin_url}dashboard">Dashboard</a></li>
		  <li>/</li>
		  <li><a href="{$admin_url}country/countrylist">Country</a></li>
		  <li>/</li>
		  <li class="current">{if $operation eq 'add'}Add Country{else}Edit Country{/if}</li>
	  </ul>
    </div>
    <div class="centerpartbg">
           {if $operation eq 'add'}
            <div class="pagetitle">Add Country</div>
            {else}
            <div class="pagetitle">Edit Country</div>
            {/if}
            <div class="add_ad_contentbox">
                    <form id="frmadd" name="frmadd" method="post" action="">
				<input type="hidden" name="iCountryId" id="iCountryId" value="{if $operation neq 'add'}{$data->iCountryId}{/if}" />
				<input type="hidden" name="action" id="action" value="add" />
				
				<div class="inputboxes">
				    <label for="textfield"><span class="red_star">*</span> Country</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vCountry" name="Data[vCountry]" class="inputbox" value="{if $operation neq 'add'}{$data->vCountry}{/if}" lang="*" title="Country" style="width:240px"/>
				</div>
                          <div class="inputboxes">
					         <label for="textfield"><span class="red_star">*</span> Country Code</label>
						    <span class="collan_dot">:</span>
					         <input type="text" id="vCountryCode" name="Data[vCountryCode]" maxlength="2" class="inputbox" value="{if $operation neq 'add'}{$data->vCountryCode}{/if}" lang="*" title="Country Code" style="width:240px"/>
			              </div>					      				       
                            <div class="inputboxes">
                                    <label for="textfield"><span class="red_star"></span> Status</label>
							 <span class="collan_dot">:</span>
                                    <select id="eStatus" name="Data[eStatus]">
                                            <option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
                                            <option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
                                    </select>
                            </div>
                            <div class="add_can_btn">
                                    {if $operation eq 'add'}
                                    <input type="button" value="Add Country" class="submit_btn" title="Add Country" onclick="checkCountryCode('{$admin_url}');"/>
                                    {else}
                                    <input type="button" value="Edit Country" class="submit_btn" title="Edit Country" onclick="checkCountryCode('{$admin_url}');"/>
                                    {/if}
                                    <a href="{$admin_url}country/countrylist" class="cancel_btn">Cancel</a>
                            </div>
                    </form>
            </div>
    </div>
    <div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
