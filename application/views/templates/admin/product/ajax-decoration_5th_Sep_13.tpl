<table width="95%" id="Decore" disabled="disabled">
  <tr>
    <th></th>
    {section name=i loop=$decoration}
      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">{$decoration[i]['vPrintLocation']}</th>
    {/section}
    <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th>
  </tr>
  {for $foo=1 to $all_quantity_range[0]['iShowField']}
  <tr id="row-{$foo}">
    <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">{$foo}Colors</td>
	{section name=j loop=$decoration}
	 <td><input type="text" class="simple" name="Decoration[{$foo}][{$decoration[j]['iPrintLocationId']}]" id="{$foo}{$decoration[j]['vPrintLocation']}-o" style="width: 60px;" value="{$editDecorationNew[$foo][$decoration[j]['iPrintLocationId']]}" id="size"/></td>	 
	{/section}
	<td><input type="checkbox" id="{$foo}" class="allow_color" checked/></td>
  </tr>
  {/for}
  <tr>
    <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">Embroidery</td>
    {section name=j loop=$decoration}
    <td><input type="text" class="simple" name="embroidery[]" style="width: 60px;" value="{$embroideryPriceNew[$decoration[j]['iPrintLocationId']]}"/></td>
    <input type="hidden" class="simple" name="iPrintLocationId[]" style="width: 60px;" value="{$decoration[j]['iPrintLocationId']}"/>
    {/section}
  </tr>
</table>
{literal}
<script>
  $( document ).ready(function() {
    var rowCount = $('#Decore tr').length;
    for (var i = 1; i < rowCount; i++) {
	 $emptyLenght = $("#row-"+i).find('input[type=text]:empty').length;
	 var count = 0;
	 $("#row-"+i+" input:text").each(function(){
	   if (this.value == '') {
		count++;
	   }
	 });
	 if (count == $emptyLenght) {
	   $("#row-"+i+" input:checkbox").attr('checked', false);
	   $("#row-"+i).find("input").attr("disabled", true);
	   $("#row-"+i).find("input").css({'background-color': '#D1D1D1'})
	   $("input[class=allow_color]").attr('disabled',false);
	 }
    }
  });
  $('.allow_color').change(function() {
    var id = $(this).attr('id');
    if($(this).is(':checked')){
	 $("#row-"+id).find("input").attr("disabled", false);
	 $("#row-"+id).find("input").css({'background-color': '#fff'})
	 $("input[class=allow_color]").attr('disabled',false);
    }else{
	 $("#row-"+id).find("input").attr('value','');
	 $("#row-"+id).find("input").attr("disabled", true);
	 $("#row-"+id).find("input").css({'background-color': '#D1D1D1'})
	 $("input[class=allow_color]").attr('disabled',false);
    }
  });
 $('.simple').keydown(function(event) {
    if ( event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || 
	 (event.keyCode == 65 && event.ctrlKey === true) || 
	 (event.keyCode >= 35 && event.keyCode <= 39)) {
		 return;
    }
    else {
	 if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
		event.preventDefault(); 
	 }   
    }
  });
</script>
{/literal}