<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}product/productlist">Product Template</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Product{else}Edit Product Template{/if}</li>
		</ul>
	</div>
		{if $operation neq 'add'}
		<div class="pagetitle_tab">
			<ul>
				<li {if $isActive eq 'main'} class="active" {else if  $isActive neq 'qd' && $isActive neq 'ds'}class="active" {/if}><a href="{$admin_url}product/edit?iProductId={$data->iProductId}&sec=main">Product Detail</a></li>
				<!--<li><a href="{$admin_url}projectbrief/projectbrieflist?iUserId={$data->iUserId}">Project Brief</a></li>-->
				<li {if $isActive eq 'qd'} class="active" {/if}><a href="{$admin_url}product/edit?iProductId={$data->iProductId}&sec=qd">Quote Detail</a></li>
				<li {if $isActive eq 'ds'} class="active" {/if}><a href="{$admin_url}product/edit?iProductId={$data->iProductId}&sec=ds">Design Studio</a></li>
			</ul>
		</div>
		{else}
		<div class="pagetitle">Add Product Detail
			<!--<ul>
				<li class="active"><a href="{$admin_url}product/add?sec=main">Product Detail</a></li>
				
				<li><a href="{$admin_url}product/add?sec=qd"> Quote Detail</a></li>
				<li><a href="{$admin_url}product/add?sec=ds">Design Studio</a></li>
			</ul> -->
		</div>
		{/if}
	{if $sec eq 'main'}
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg" style="width:49%;float:left;">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Product Template

			{else}
			Edit Product Template
			{/if} </div>
		
			<div class="add_ad_contentbox">
				<input type="hidden" name="iProductId" id="iProductId" value="{if $operation neq 'add'}{$data->iProductId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="vImage" id="vImage_old" value="{$data->vImage}" />
                                 <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Category</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iCategoryId]" onchange="makesizediv(this.value)">
						
						<option value=''>--Select Product Category--</option>
						{section name=i loop=$db_producttype}
						<option value='{$db_producttype[i]['iCategoryId']}' {if $operation neq 'add'}{if $data->iCategoryId eq $db_producttype[i]['iCategoryId']}selected{/if}{/if}>{$db_producttype[i]['vCategory']}</option>
						{/section}
					</select>
					
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vProductName" name="data[vProductName]" class="inputbox" value="{if $operation neq 'add'}{$Data->vProductName}{/if}" lang="*" title="Product Name" />
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}"{if $data->vImage eq ''} lang="*" {/if} onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
				{if $operation eq 'add'}
				<div class="inputboxes" >
					<div class="msg_alert1" style="margin-left: 38% !important;">
						Recommended Size : 193 X 198
					</div>
				</div>			
				{/if}
				{if $operation neq 'add'}{if $data->vImage neq ''}
					<div class="view_del_user">
						
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg" >View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('{$data->iProductId}','product');" class="delete_btnimg">Delete</a></div>
						<div style="display:none;" >
							<div id="popfancy" style="padding-bottom: 50px;"><img src="{$upload_path}/product/{$data->iProductId}/193X198_{$data->vImage}"></div>
						</div>
				         </div>
					{/if}{/if}
 				{if $operation neq 'add'}
				<div class="inputboxes" >
					<div class="msg_alert1" style="margin-left: 39% !important;">
						Recommended Size : 193 X 198
					</div>
				</div>			
				{/if}
                                <div class="inputboxes">
				        <label for="textarea"><span class="red_star"></span> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tDescription" class="inputbox" title="Description" name="data[tDescription]" id="tDescription">{if $operation neq 'add'}{$Data->tDescription}{/if}</textarea>
				</div>        
				<!--div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Price</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fPrice" name="Data[fPrice]" class="inputbox" title="fPrice"  lang="*" value="{if $operation neq 'add'}{$data->fPrice}{/if}" onkeypress="return checkprise(event)">
				</div-->								
				<div class="inputboxes">
				        <label for="textarea"><span class="red_star">*</span></span> Order</label>
						<span class="collan_dot">:</span>
						<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						
						<option value="{$initOrder}" >{$initOrder++}</option>
						
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}	
						<option value="{$initOrder}"  {if $data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						{/while}
						{/if}
					</select>
				        
				</div> 
                                <div class="inputboxes">
				<label><span class="red_star"></span> Is Promotional ?</label>
					<span class="collan_dot">:</span>
						<input type="checkbox" value="Yes" name="Data[ePromotion]" id="eFeatured" {if $Data->iProductId neq '' and $data->ePromotion eq 'Yes'}checked{/if}/>							
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Product Template" class="submit_btn" title="Add Product" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Product Template" class="submit_btn" title="Edit Product" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
	<div class="pagetitle">Fabrics Section</div>
	<div class="add_ad_contentbox">
		<div class="inputboxes">
			<label for="textarea"><span class="red_star"></span> Fabrics</label>
				<span class="collan_dot">:</span>
				<div class="event_skill" style="width: 55%">
					{section name=j loop=$fabrics}
						<div class="event_slikk_inner">
							<input type="checkbox" value="{$fabrics[j]['iFabricsId']}" id="iFabricsId" name="iFabricsId[]" {if  $fabrics[j]['iFabricsId'] eq $iFabricsId[j]}checked{/if}/>
							{$fabrics[j]['vTitle']}
						</div>
					{/section}
				</div>
		</div>
	</div>
	<!--<div class="add_ad_contentbox">
                               <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Fabrics</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iFabricsId]" onchange="makesizediv(this.value)">
						
						<option value=''>--Select Fabrics--</option>
						{section name=i loop=$fabrics}
						<option value='{$fabrics[i]['iFabricsId']}' {if $operation neq 'add'}{if $data->iFabricsId eq $fabrics[i]['iFabricsId']}selected{/if}{/if}>{$fabrics[i]['vTitle']}</option>
						{/section}
					</select>
					
				</div>
	</div>-->	
	</div>
	
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
	<div class="pagetitle">Brands Section</div>
			<div class="add_ad_contentbox">
                               <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Brands</label>
					<span class="collan_dot">:</span>
					<div class="event_skill" style="width: 55%">
						{section name=j loop=$brand}
						<div class="event_slikk_inner">
							<input type="checkbox" value="{$brand[j]['iBrandId']}" id="iBrandId" name="iBrandId[]" {if $brand[j]['iBrandId'] eq $iBrandId[j]}checked{/if}/>
							{$brand[j]['vTitle']}
						</div>
						{/section}
					</div>
				</div>
			</div>
	</div>

	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px; margin-top: 20px;">
	<div class="pagetitle">Occupations Section</div>
			<div class="add_ad_contentbox">
                               <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Occupations</label>
					<span class="collan_dot">:</span>
					<div class="event_skill" style="width: 55%">
						{section name=j loop=$occupations}
						<div class="event_slikk_inner">
							<input type="checkbox" value="{$occupations[j]['iOccupationsId']}" id="iOccupationsId" name="iOccupationsId[]" {if $occupations[j]['iOccupationsId'] eq $iOccupationsId[j]}checked{/if}/>
							{$occupations[j]['vTitle']}
						</div>
						{/section}
					</div>
				</div>
			</div>
		
	</div>
	
	<!--div class="centerpartbg" style="float:right;width:49%;margin-right:6px;min-height:235px;margin-top:20px;">
	<div class="pagetitle">Size Selection</div>
	
		<div class="add_ad_contentbox" id="sizedivid">
				
				{if  $sizelist|@count gt 0 && $operation neq 'add'}
				<div class="inputboxes" style="padding-bottom:0px;">
				<label for="textfield"></label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Width</label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Length</label>
				</div>
				{section name=i loop=$sizelist}
				<div class="inputboxes">
				<label for="textfield">{$sizelist[i]->vTitle}&nbsp;({$sizelist[i]->vSize})</label>
				<span class="collan_dot">:</span>
				<input type="hidden" id="" name="SIZE[{$smarty.section.i.index}][iSizeId]" value="{$sizelist[i]->iSizeId}"/>
				<input type="text" id="Width{$smarty.section.i.index}" name="SIZE[{$smarty.section.i.index}][iWidth]" class="inputbox" title="Width" style="width:100px;" value="{$sizelist[i]->iWidth}" onkeypress="return checkprise(event)"/>
				<input type="text" id="Length{$smarty.section.i.index}" name="SIZE[{$smarty.section.i.index}][iLength]" class="inputbox" title="Length" style="width:100px;margin-left:10px;" value="{$sizelist[i]->iLength}" onkeypress="return checkprise(event)"/>
				</div>
				{/section}
				{else}
				<div style="text-align:center;color:#C44C22; font-size:14px; font-weight:bold;">No Size available for this category</div>
				{/if}
		</div>
	</div-->
	</form>
	{/if}
	{if $sec eq 'qd'}
	<div class="centerpartbg" style="width:100%;float:left;min-height:250px;">
		<div id="relaceformcolor">
			<div class="pagetitle" style="text-align:center;">
				Color Selection
			</div>
			<form id="frmaddscolor" name="frmaddscolor" method="post" enctype="multipart/form-data"  action="">
				{section name=j loop=$all_color}
					<div class="event_slikk_inner">
						<input type="checkbox" value="{$all_color[j]->iColorId}" id="iColorId" name="iColorId[]" class="aka" {if in_array($all_color[j]->iColorId,$final_all_colorids)}checked{/if}/>
						{$all_color[j]->vColor}
					</div>
				{/section}
			</form>
			
		</div>
	</div>
	<div class="centerpartbg" style="width:100%;float:left;margin-top:20px;min-height:250px;">
		<div id="relaceformcolor">
			<div class="pagetitle" style="text-align:center;">
				 Price by Size and Color
				
			</div>
			 <div id="inn" style="text-align: center"><h2>Please select colors</h2></div>
		</div>
	</div>

	{/if}
{if $sec eq 'ds'}
	{if $isActive neq 'ds'}
	
	{/if}
	<div class="centerpartbg" style="width:100%;float:left;min-height:250px;">
		<div id="relaceformcolor">
		<div class="pagetitle" style="text-align:center;">
			Add Color Image
		</div>
		<form id="frmaddcolor" name="frmaddcolor" method="post" enctype="multipart/form-data"  action="">
			<input type="hidden" name="iProductId" id="iProductId" value="{$data->iProductId}" />
			<input type="hidden" name="type" id="type" value="color" />
			<input type="hidden" name="mode" id="mode" value="add" />
			<input type="hidden" name="lang" id="lang" value="{$lang}" />
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Color :</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[iColorId]" lang="*" title="color">
						<option value=''>--Select Color--</option>
						{section name=i loop=$color}
						<option value='{$color[i]->iColorId}'>{$color[i]->vColor}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Front Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Back Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Left Sleeve</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vLeftSleeveImage"  name="vLeftSleeveImage" title="Left Sleeve Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Right Sleeve</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vRightSleeveImage"  name="vRightSleeveImage" title="Right Sleeve Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[eStatus]">
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
					<input type="submit" value="Add Color Image" class="submit_btn" title="Add Color Image" onclick="return validate(document.frmaddcolor);"/>
				</div>
			</div>
		</form>
		</div>
	</div>
	
	<div class="centerpartbg" style="width:100%;float:left;margin-top:20px;" id="colorformadd">
		<div class="pagetitle">
			Color Image list
		</div>
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{/if}
		
			<div class="add_ad_contentbox">
				<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}product/search_action" method="post">
				<input  type="hidden" name="iProductId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th>Color Name</th>
							<th>Color code</th>
							<th>Front Image</th>
							<th>Back Image</th>
							<th>Left Sleeve</th>
							<th>Right Sleeve</th>
							
							<th width="77px">Status</th>
							<th width="90px">Action</th>
						</tr>
					</thead>
					{if $data_color|@count gt 0}
					{section name=i loop=$data_color}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}" style="font-size:13px;">
						<tr>
							
							<td><a href="javascript:void(0);" onclick="editform({$data_color[i]->iProductColorId});">{$data_color[i]->vColor}</a></td>
							<td>{$data_color[i]->vColorCode}</td>
							<td>{if $data_color[i]->vFrontImage neq ''}<div class="view_btn_user"><a href="#Fancyhrefmain{$smarty.section.i.index}" id="fancyhrefmain" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="Fancyhrefmain{$smarty.section.i.index}" style="padding-bottom: 800px;"><img src="{$upload_path}product/{$data_color[i]->iProductId}/{$data_color[i]->iProductColorId}/200X200_{$data_color[i]->vFrontImage}"></div>
																												    
							</div>{/if}
							</td>
							
							<td>{if $data_color[i]->vBackImage neq ''}<div class="view_btn_user"><a href="#Fancyhrefback{$smarty.section.i.index}" id="fancyhrefback" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="Fancyhrefback{$smarty.section.i.index}" style="padding-bottom: 800px;"><img src="{$upload_path}product/{$data_color[i]->iProductId}/{$data_color[i]->iProductColorId}/200X200_{$data_color[i]->vBackImage}"></div>
							</div>{/if}
							</td>
							<td>{if $data_color[i]->vLeftSleeveImage neq ''}<div class="view_btn_user"><a href="#fancyhrefleftSleeeve{$smarty.section.i.index}" id="fancyhrefleftSleeeve" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="fancyhrefleftSleeeve{$smarty.section.i.index}" style="padding-bottom: 800px;"><img src="{$upload_path}product/{$data_color[i]->iProductId}/{$data_color[i]->iProductColorId}/200X200_{$data_color[i]->vLeftSleeveImage}"></div>
							</div>{/if}
							</td>
							<td>{if $data_color[i]->vRightSleeveImage neq ''}<div class="view_btn_user"><a href="#fancyhrerightSleeeve{$smarty.section.i.index}" id="fancyhrerightSleeeve" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="fancyhrerightSleeeve{$smarty.section.i.index}" style="padding-bottom: 800px;"><img src="{$upload_path}product/{$data_color[i]->iProductId}/{$data_color[i]->iProductColorId}/200X200_{$data_color[i]->vRightSleeveImage}"></div>
							</div>{/if}
							</td>
							
							<td>{$data_color[i]->eStatus}</td>
							<td><a href="javascript:void(0);" onclick="editform({$data_color[i]->iProductColorId});"><img src="{$admin_image_path}icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommoncolor({$data_color[i]->iProductColorId},'{$lang}','product/deletecolor',{$data_color[i]->iProductId})"></a> </td>
						</tr>
					</tbody>
					{/section}
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					{/if}
				</table>
			</form>
		</div>
			</div>
		{/if}
	</div>
	
	<div class="clear"></div>
	
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
         	
         	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrefmain").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrefleftSleeeve").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});


$("#fancyhrefback").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrerightSleeeve").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});


function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}product/deleteimage?id={/literal}{$data->iProductId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

function makesizediv(val)
{
	var site_url = '{/literal}{$admin_url}{literal}';
	if(val == '{/literal}{$data->iCategoryId}{literal}')
	{
	  var url = site_url+"product/makesizedefault?iCategoryId="+val+"&iProductId="+'{/literal}{$data->iProductId}{literal}';	
	}
	else{
	  var url = site_url+"product/makesize?iCategoryId="+val;
	}
	var pars = '';
	$.post(url+pars,
	    function(data) {
		$('#sizedivid').html(data);
	    });
}


function editform(id)
{
	
	var site_url = '{/literal}{$admin_url}{literal}';
	var url = site_url+"product/makeeditcolor?iProductColorId="+id+"&lang="+'{/literal}{$lang}{literal}';	
	var pars = '';
	//alert(url+pars);
	$.post(url+pars,
	    function(data) {
		//alert(data);
		$('#relaceformcolor').html(data);
	    });
}
$('.aka').change(function() {
	
	load_color_size();
});
load_color_size();
function load_color_size()
{
	
	var str = $("#frmaddscolor").serialize();
	
	var site_url = '{/literal}{$admin_url}{literal}';
	var url = site_url+"product/scdetail";
	
	var pid = '&iProductId='+'{/literal}{$data->iProductId}{literal}';
	var pars = str;
	$.ajax({
		type: "POST",
		url: site_url+"product/scdetail?"+pars+pid,
		success: function(data){
			
		    $('#inn').html(data);
		}
	});
}
$('.simple').keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || 
            (event.keyCode == 65 && event.ctrlKey === true) || 
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
 
</script>
{/literal} 

