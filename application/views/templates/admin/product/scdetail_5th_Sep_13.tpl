<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
  <input type="hidden" name="type" value="sizecolordetail"/>
<table width="95%">
  <tr>
    <th></th>
    {section name=i loop=$all_size_details}
      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">{$all_size_details[i]['vSize']}</th>
    {/section}
  </tr>
  {section name=i loop=$color_details}
    <tr id="col_{$color_details[i]['iColorId']}">
      <td style="padding:0 15px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">{$color_details[i]['vColor']}</td>
          {section name=j loop=$all_size_details}
            {if in_array($all_size_details[j]['iSizeId'],$final_all_colorsizes[$color_details[i]['iColorId']])}
              <td><input type="checkbox" name="sizedetail" value="sizedetail" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}" class="size_detail" checked><br/>
		  <input type="text" class="simple" name="Data[{$color_details[i]['iColorId']}][{$all_size_details[j]['iSizeId']}]" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}-o" style="width: 60px;background-color:#fff;" value="{$final_all_colorsizes_price[$color_details[i]['iColorId']][$all_size_details[j]['iSizeId']]['price']}"/></td>
            {else}
              <td><input type="checkbox" name="sizedetail" value="sizedetail" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}" class="size_detail"><br/>
		  <input type="text" class="simple" name="Data[{$color_details[i]['iColorId']}][{$all_size_details[j]['iSizeId']}]" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}-o" style="width: 60px;background-color:#D1D1D1;" disabled="disabled"/></td>
            {/if}
          {/section}
    </tr>
    <tr >
      {section name=j loop=$all_size_details}
      <td></td>
      {/section}
      <td>{if $smarty.section.i.index != 0}<input type="checkbox" name="copy[{$color_details[i]['iColorId']}]" value="{$color_details[$smarty.section.i.index -1]['iColorId']}" onclick="duplicate({$color_details[$smarty.section.i.index -1]['iColorId']},{$color_details[i]['iColorId']});" id="dup_{$color_details[$smarty.section.i.index -1]['iColorId']}">Copy Previous{/if}</td></tr>
  {/section}
</table>
<br/>

<div class="centerpartbg" style="width:100%;">
        <div id="relaceformcolor">
	  <div class="pagetitle" style="text-align:center;">
	  <select id="iQuantityRangeId" name="iQuantityRangeId" style="margin: 10px;float: left" title="Quantity Range" lang="*" onchange="load_decoration(this.value);">
	    <option value="">-------Select Quantity Range-----</option>
	    {section name=i loop=$all_quantity_range}
		<option value="{$all_quantity_range[i]['iQuantityRangeId']}" {if $all_quantity_range[i]['iQuantityRangeId'] eq $selected_quantity_range_id}selected="selected"{/if}>{$all_quantity_range[i]['vQuantityRange']}</option>
	    {/section}
	  </select>
	  <span style="margin-right: 300px;">Decoration Services </span>                       
	  </div>
	   <div id="ajax-load-deco" >
	  <table width="95%" id="Decore" disabled="disabled">
	    <tr>
	      <th></th>
	      {section name=i loop=$decoration}
		  <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">{$decoration[i]['vPrintLocation']}</th>
	      {/section}
		 <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th>
	    </tr>
	    {for $foo=1 to $all_quantity_range[0]['iShowField']}
	     <tr id="row-{$foo}">
		  <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">{$foo}Colors</td>
		  {section name=j loop=$decoration}
		  <td><input type="text" class="simple" name="Decoration[{$foo}][{$decoration[j]['iPrintLocationId']}]" id="{$foo}{$decoration[j]['vPrintLocation']}-o" style="width: 60px;" value="{$editDecorationNew[$foo][$decoration[j]['iPrintLocationId']]}" id="size"/></td>
		  {/section}
		  <td><input type="checkbox" id="{$foo}" class="allow_color" checked/></td>
	     </tr>
	    {/for}
	    <tr>
		  <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">Embroidery</td>
		  {section name=j loop=$decoration}
		  <td><input type="text" class="simple" name="embroidery[]" style="width: 60px;" value="{$embroideryPriceNew[$decoration[j]['iPrintLocationId']]}"/></td>
		  <input type="hidden" class="simple" name="iPrintLocationId[]" style="width: 60px;" value="{$decoration[j]['iPrintLocationId']}"/>
		  {/section}
	     </tr>
	    <tr>
	      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"></td>
	    </tr>
	  </table>
	  </div>
	  <input type="checkbox" name="CopyDecoration" value="yes"  id="CopyDecoration">Want to copy form Another Template
	  <select id="pro_temp" name="copy_pro_temp" style="display: none;">
	    <option value="">-------Select Product Template-----</option>
	    {section name=i loop=$all_product}
		    {if $all_product[i]['iProductId'] != $product_id }
		      <option value="{$all_product[i]['iProductId']}">{$all_product[i]['vUrlText']}</option>
		    {/if}
	    {/section}
	  </select>
        </div>
	<div class="btnmaincheck">
	 <input type="submit" name="color_deatil" value="save" class="submit_btn" onclick="return validate(document.frmadd);"/>
	 <a href="{$admin_url}product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a></div> </div>
</form>
</div>


{literal}
  <script>
    var myStringArray = {/literal}{$all_sizeid|json_encode}{literal};
     // alert(myStringArray+'===='+myStringArray.length);
    $('.size_detail').change(function() {
      var id = $(this).attr('id');
      if($(this).is(':checked')){
	   $("input[id="+id+"-o]").attr('disabled',false);
	   $("input[id="+id+"-o]").css({'background-color':'#fff'});
	   }else{
         $("input[id="+id+"-o]").val('');
         $("input[id="+id+"-o]").attr('disabled',true);
          $("input[id="+id+"-o]").css({'background-color': '#D1D1D1'})
      }
    });
    $( document ).ready(function() {
	 var rowCount = $('#Decore tr').length;
	 for (var i = 1; i < rowCount; i++) {
	   $emptyLenght = $("#row-"+i).find('input[type=text]:empty').length;
	   var count = 0;
	   $("#row-"+i+" input:text").each(function(){
		if (this.value == '') {
		  count++;
		}
	   });
	   if (count == $emptyLenght) {
		$("#row-"+i+" input:checkbox").attr('checked', false);
		$("#row-"+i).find("input").attr("disabled", true);
		$("#row-"+i).find("input").css({'background-color': '#D1D1D1'})
		$("input[class=allow_color]").attr('disabled',false);
	   }
	 }
    });
    $('.allow_color').change(function() {
	 var id = $(this).attr('id');
	 if($(this).is(':checked')){
	   $("#row-"+id).find("input").attr("disabled", false);
	   $("#row-"+id).find("input").css({'background-color': '#fff'})
	   $("input[class=allow_color]").attr('disabled',false);
	 }else{
	   $("#row-"+id).find("input").attr('value','');
	   $("#row-"+id).find("input").attr("disabled", true);
	   $("#row-"+id).find("input").css({'background-color': '#D1D1D1'})
	   $("input[class=allow_color]").attr('disabled',false);
	 }
    });
 $('.simple').keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || 
            (event.keyCode == 65 && event.ctrlKey === true) || 
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
 function duplicate_old(fid,id) {
  var act_id = '#col_'+id;
  $(act_id).toggle();
 }
  function duplicate(fid,tid) {
    var dupid = '#dup_'+fid;
    for (var i = 0; i < myStringArray.length; i++) {
      var fromid = '#color-'+fid+'-size-'+myStringArray[i]+'-o';
      var toid = '#color-'+tid+'-size-'+myStringArray[i]+'-o';
      var chekid = '#color-'+tid+'-size-'+myStringArray[i];
      if($(dupid).is(':checked')){
	var fval = $(fromid).val();
        $(toid).val(fval);
	if (fval != '') {
	  $(toid).attr('disabled',false);
	  $(chekid).attr('checked',true);
	  $(toid).css({'background-color':'#fff'});	  //code
  	}

      }else{
        $(toid).val('');
	$(toid).attr('disabled',true);
	$(chekid).attr('checked',false);	
      }
    //alert(myStringArray[i]);
    //Do something
    }
    //alert(this.value);
    //var act_id = '#col_'+id;
    //$(act_id).toggle();
  }
  
  function disable(table_id)
    {
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    for(var i=0; i<inputs.length; ++i)
        inputs[i].disabled=true;
    }
    
  function enable(table_id){
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    for(var i=0; i<inputs.length; ++i)
      inputs[i].disabled=false;
  }
  $(function() {
    $('#CopyDecoration').click(function(){
      if(this.checked){
	disable('Decore');
	 $("#pro_temp").show();
      }else{
	enable('Decore');
	 $("#pro_temp").hide();   
      }
    });
  });
  function load_decoration(id){
    //alert(id);return false;
    var site_url = '{/literal}{$admin_url}{literal}';
    var pid = '&iProductId='+'{/literal}{$product_id}{literal}'+'&qid='+id;
    $.ajax({
	    type: "POST",
	    url: site_url+"product/load_decoration?"+pid,
	    success: function(data){
		   $('#ajax-load-deco').html(data);
	    }
    });
  }
  </script>
{/literal}
