<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="" >
  <input type="hidden" name="type" value="sizecolordetail"/>
<table width="95%" style="margin: 15px 0 0 0;">
  <tr>
    <th></th>
    {section name=i loop=$all_size_details}
      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">{$all_size_details[i]['vSize']}</th>
    {/section}
  </tr>
  
  {section name=i loop=$color_details}
    <tr id="col_{$color_details[i]['iColorId']}">
      <td style="padding:0 15px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">{$color_details[i]['vColor']}</td>
          {section name=j loop=$all_size_details}
            {if in_array($all_size_details[j]['iSizeId'],$final_all_colorsizes[$color_details[i]['iColorId']])}
              <td><input type="checkbox" name="sizedetail" value="sizedetail" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}" class="size_detail" checked><br/>
		  <input type="text" class="simple" name="Data[{$color_details[i]['iColorId']}][{$all_size_details[j]['iSizeId']}]" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}-o" style="width: 60px;background-color:#fff;" value="{$final_all_colorsizes_price[$color_details[i]['iColorId']][$all_size_details[j]['iSizeId']]['price']}"/></td>
            {else}
              <td><input type="checkbox" name="sizedetail" value="sizedetail" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}" class="size_detail"><br/>
		  <input type="text" class="simple" name="Data[{$color_details[i]['iColorId']}][{$all_size_details[j]['iSizeId']}]" id="color-{$color_details[i]['iColorId']}-size-{$all_size_details[j]['iSizeId']}-o" style="width: 60px;background-color:#D1D1D1;" disabled="disabled"/></td>
            {/if}
          {/section}
    </tr>
    <tr >
      {section name=j loop=$all_size_details}
      <td></td>
      {/section}
      <td>{if $smarty.section.i.index != 0}<input type="checkbox" name="copy[{$color_details[i]['iColorId']}]" value="{$color_details[$smarty.section.i.index -1]['iColorId']}" onclick="duplicate({$color_details[$smarty.section.i.index -1]['iColorId']},{$color_details[i]['iColorId']});" id="dup_{$color_details[$smarty.section.i.index -1]['iColorId']}">Copy Previous{/if}</td></tr>
  {/section}
</table>
<br/>

<div class="" style="width:100%;">
        <div id="relaceformcolor">
	  <div class="pagetitle" style="text-align:center;">
	  <select id="iQuantityRangeId" name="iQuantityRangeId" style="margin: 10px;float: left" title="Quantity Range" lang="*" onchange="load_decoration(this.value);">
	    <option value="">-------Select Quantity Range-----</option>
	    {section name=i loop=$all_quantity_range}
		<option value="{$all_quantity_range[i]['iQuantityRangeId']}" {if $all_quantity_range[i]['iQuantityRangeId'] eq $selected_quantity_range_id}selected="selected"{/if}>{$all_quantity_range[i]['vQuantityRange']}</option>
	    {/section}
	  </select>
	  <span style="margin-right: 300px;">Screen Printing Services </span>                       
	  </div>
	   <div id="ajax-load-deco" >
	   <table width="95%" id="Decore" disabled="disabled" class="divtext">		
	    <tr>
	      <th></th>
	      {section name=i loop=$decoration}
		  <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">{$decoration[i]['vPrintLocation']}</th>
	      {/section}
		 <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th>
	    </tr>
	    <!-- {count($range_color)} -->
	    
	    {for $foo=0 to count($range_color)-1}
	     <tr id="row-{$foo}">
		  <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">{$range_color[$foo][0]->vNumberColor}</td>
		  {section name=j loop=$decoration}
		  <td><input type="text" class="simple" name="Decoration[{$range_color[$foo][0]->iNumberColorsId}][{$decoration[j]['iPrintLocationId']}]" id="vartical_{$smarty.section.j.index+1}{$foo+1}" style="width: 60px;" value="{$editDecorationNew[{$range_color[$foo][0]->iNumberColorsId}][$decoration[j]['iPrintLocationId']]}" id="size"/></td>
		  {/section}
		  <td><input type="checkbox" id="{$foo+1}" class="allow_color" checked/></td>
		</tr>
	    {/for}
	    <!-- 
	    <tr id="row-emb">
		 <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">Embroidery</td>
		  {section name=j loop=$decoration}
		  <td><input type="text" class="simple" name="embroidery[{$decoration[j]['iPrintLocationId']}]" id="emb_{$smarty.section.j.index+1}" style="width: 60px;" value="{if $embroideryPriceNew[$decoration[j]['iPrintLocationId']] != 0.00}{$embroideryPriceNew[$decoration[j]['iPrintLocationId']]}{/if}"  /></td>
		  <input type="hidden" class="simple" name="iPrintLocationId[]" style="width: 60px;" value="{$decoration[j]['iPrintLocationId']}" />
		  {/section}
		  <td><input type="checkbox" id="embroidery" class="embroidery" checked/></td>
	     </tr>
	    <tr>
	      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"></td>
	    </tr>
	    <tr>
		
		<!-- <th style="float:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th> -->
	      <td class="allow_text">Allow Location</td>
	      {section name=j loop=$decoration start=0}		  
	      <td><div class="check_box"><input type="checkbox" id="vartical_{$smarty.section.j.index+1}" class="allow_colors" onclick="testignofcheckbox(this.id)"/ ></div></td>
	      {/section}
	      <td></td>
	    </tr>
	  </table>
	  </div>
	  <div style="clear: both;"></div>
	  <br><br>	  
	 <input type="checkbox" name="CopyDecoration" value="yes"  id="CopyDecoration">Want to copy form Another Template
	  <select id="pro_temp" name="copy_pro_temp" style="display: none;">
	    <option value="">-------Select Product Template-----</option>
	    {section name=i loop=$all_product}
		    {if $all_product[i]['iProductId'] != $product_id }
		      <option value="{$all_product[i]['iProductId']}">{$all_product[i]['vUrlText']}</option>
		    {/if}
	    {/section}
	  </select>
	  <br><br>
	  <input type="checkbox" name="CopyQuantityRange" value="yes"  id="CopyQuantityRange" style="margin-left: 35px;">Want to copy form Another Quantity Range	  
	  <select id="qty_temp" name="copy_qty_temp" style="display: none;">
	    <option value="">-------Select Quantity Range-----</option>	    
	    {section name=i loop=$all_quantity_range}	   
		<option value="{$all_quantity_range[i]['iQuantityRangeId']}" >{$all_quantity_range[i]['vQuantityRange']}</option>		
	    {/section}    
	  </select>  
        </div>
	   <div class="btnmaincheck">
	 <input type="submit" name="color_deatil" value="save" class="submit_btn" onclick="return validate(document.frmadd);"/>
	 <a href="{$admin_url}product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a></div>
	   <br><br><br><br>
	   
	   <!-- All Are working perect-->
	   <!-- Embrodary codign start -->
	   
	   
	   
	  <div class="pagetitle" style="text-align:center;">
		<select id="iQuantityRangeId_Embrodary" name="Embrodary_iQuantityRangeId" style="margin: 10px;float: left" title="Quantity Range" lang="*" onchange="loadEmrodary(this.value);">
		<option value="">----Embrodary Quantity Range-----</option>
		{section name=i loop=$all_quantity_range}
		 <option value="{$all_quantity_range[i]['iQuantityRangeId']}" {if $all_quantity_range[i]['iQuantityRangeId'] eq $selected_quantity_range_id}selected="selected"{/if}>{$all_quantity_range[i]['vQuantityRange']}</option>
		{/section}
	   </select>
	   <span style="margin-right: 300px;">Embroidery Services </span>
	   
	    <div id="ajax-load-embrodary" >
		<table width="95%" id="Embrodary_decoration" disabled="disabled" class="divtext">		
	    <tr>
	      <th></th>
	      {section name=i loop=$decoration}
		  <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">{$decoration[i]['vPrintLocation']}</th>
	      {/section}
		 <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th>
	    </tr>
	    <!-- {count($range_color)} -->
	 
	    
	    <tr id="row-emb">
		 <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">Embroidery</td>
		  {section name=j loop=$decoration}
		  <td><input type="text" class="simple" name="embroidery[{$decoration[j]['iPrintLocationId']}]" id="emb_{$smarty.section.j.index+1}" style="width: 60px;" value="{if $embroideryPriceNew[$decoration[j]['iPrintLocationId']] != 0.00}{$embroideryPriceNew[$decoration[j]['iPrintLocationId']]}{/if}"  /></td>
		  <input type="hidden" class="simple" name="iPrintLocationId[]" style="width: 60px;" value="{$decoration[j]['iPrintLocationId']}" />
		  {/section}
		  <td><input type="checkbox" id="embroidery" class="embroidery" checked/></td>
	     </tr>
	    <tr>
	      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"></td>
	    </tr>
	    
	    <tr>		
		<!-- <th style="float:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th> -->
	      <td class="allow_text">Allow Location</td>
	      {section name=j loop=$decoration start=0}		  
	      <td><div class="check_box"><input type="checkbox" id="embrodary_{$smarty.section.j.index+1}" class="allow_colors1" onclick="embrodaryCheckBox(this.id)"/ ></div></td>
	      {/section}
	      <td></td>
	    </tr>
	  </table>	
	  </div>
	 	 <div style="clear: both;"></div>
	  <br><br>	  
	 <input type="checkbox" name="CopyEmbrodary" value="yes"  id="CopyEmbrodary">
	   <span style="font-family: 'latoblack';font-size: 13px;line-height: 16px;color: #000;">Want to copy form Another Template</span>
	  <select id="embr_pro_temp" name="copy_embr_pro_temp" style="display: none;margin-top: 11px;">
	    <option value="">-------Select Product Template-----</option>
	    {section name=i loop=$all_product}
		    {if $all_product[i]['iProductId'] != $product_id }
		      <option value="{$all_product[i]['iProductId']}">{$all_product[i]['vUrlText']}</option>
		    {/if}
	    {/section}
	  </select>  
	 <br>
	  
	  <input type="checkbox" name="CopyEmrodaryQuantityRange" value="yes"  id="CopyEmrodaryQuantityRange" style="margin-left: 18%;">
	    <span style="font-family: 'latoblack';font-size: 13px;line-height: 16px;color: #000;">Want to copy form Another Quantity Range</span>
	  <select id="Embr_qty_temp" name="copy_embr_qty_temp" style="display: none;margin-top: 10px;">
	    <option value="">-------Select Quantity Range-----</option>	    
	    {section name=i loop=$all_quantity_range}	   
		<option value="{$all_quantity_range[i]['iQuantityRangeId']}" >{$all_quantity_range[i]['vQuantityRange']}</option>		
	    {/section}    
	  </select>  

	  


	    

	   

	    
	    

	    <div class="btnmaincheck">
		<input type="submit" name="color_deatil" value="save" class="submit_btn" onclick="return validate(document.frmadd);"/>
		<a href="{$admin_url}product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a>
	    </div>   
	  </div>
	  
</form>
</div>


{literal}
<script>
   /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: function is used to show text box when user click on  vertical checkbox
     */   
   function embrodaryCheckBox(id){    
	var str=id;
	var text=str.substring(10);	
     var id_check=(document.getElementById(id).checked);	
	var someObj={};	
	  var embroidery_checkbox=(document.getElementById('embroidery').checked);
	   if (id_check && embroidery_checkbox){
		 $("#emb_"+text).removeAttr("disabled");
		 $("#emb_"+text).css({'background-color': '#fff'})		
	   }else{
		 $("#emb_"+text).attr("disabled");
		 $("#emb_"+text).attr('value','');
		 $("#emb_"+text).css({'background-color': '#D1D1D1'})
	   }
	 }  
  function testignofcheckbox(id){    
	var str=id;	
	var text=str.substring(9);	
     var id_check=(document.getElementById(id).checked);
	
	var someObj={};
	someObj.fruitsGranted=[];
	someObj.fruitsDenied=[];	
	$(".allow_color:input:checkbox").each(function(){
	    var $this = $(this);	
	    if($this.is(":checked")){
		   someObj.fruitsGranted.push($this.attr("id"));
	    }else{
		   someObj.fruitsDenied.push($this.attr("id"));
	    }
	});	
	var all_cheked_checkbox=someObj.fruitsGranted;	
	var length=all_cheked_checkbox.length;
	
	 for(i=0;i<length;i++){	
	  var another_cheked=(document.getElementById(all_cheked_checkbox[i]).checked); //1	 
	  var create_id=all_cheked_checkbox[i];// 1
	  var current_checkbox=id; //vartical_1
	  var check_current=(document.getElementById(current_checkbox).checked);	 
	    if (check_current && another_cheked) {		
		var str=id;	
		var current_check=str.substring(9);		   //1
		var another=all_cheked_checkbox[i];	  
		 $("#vartical_"+current_check+another).removeAttr('disabled');		 
		 $("#vartical_"+current_check+another).css({'background-color': '#fff'})    
		 $("input[class=allow_color]").attr('disabled',false);
	    }else{
		  var str=id;	
		  var current_check=str.substring(9);		   //1
		  var another=all_cheked_checkbox[i];		  
		 $("#vartical_"+current_check+another).attr("disabled", true);
		 $("#vartical_"+current_check+another).attr('value','');
		 $("#vartical_"+current_check+another).css({'background-color': '#D1D1D1'})    
		 $("input[class=allow_color]").attr('disabled',false);
	    }  
	 }
	 /*
	  var embroidery_checkbox=(document.getElementById('embroidery').checked);
	   if (id_check && embroidery_checkbox){
		 $("#emb_"+text).removeAttr("disabled");
		 $("#emb_"+text).css({'background-color': '#fff'})		
	   }else{
		 $("#emb_"+text).attr("disabled");
		 $("#emb_"+text).attr('value','');
		 $("#emb_"+text).css({'background-color': '#D1D1D1'})
	   }
	   */
  }
  
</script>
  <script> 
	   var count = 0;
	   var emptyLenght = $("#row-emb").find('input[type=text]:empty').length;	   
	   $("#row-emb input:text").each(function(){
	    
		if (this.value == '') {
		  count++;
		}
	   });
	  console.log(count);
   	   if (count == emptyLenght) {
		$("#row-emb input:checkbox").attr('checked', false);
		$("#row-emb").find("input").attr("disabled", true);
		$("#row-emb").find("input").css({'background-color':'#D1D1D1'})
		$("input[class=embroidery]").attr('disabled',false);		
		//$("input[class=embroidery]").attr('disabled',false);	
	   }   
    var myStringArray = '{/literal}{$all_sizeid|json_encode}{literal}';
        
     // alert(myStringArray+'===='+myStringArray.length);
    $('.size_detail').change(function(){
      var id = $(this).attr('id');	 
      if($(this).is(':checked')){
	   $("input[id="+id+"-o]").attr('disabled',false);
	   $("input[id="+id+"-o]").css({'background-color':'#fff'});
	   }else{
         $("input[id="+id+"-o]").val('');
         $("input[id="+id+"-o]").attr('disabled',true);
          $("input[id="+id+"-o]").css({'background-color': '#D1D1D1'})
      }
    });
    $( document ).ready(function() {	 
	 var ele=[];
	 $('input:text[id^="color-"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	 var all_textboxid=ele;
	 var length=all_textboxid.length; 
	 //var test=$("#"+all_textboxid[3]).val();
	 for(i=0;i<length;i++){
		var textboxid=(all_textboxid[i]); //color-45-size-10-o
		var makecheckboxid=textboxid.substr(0,16);   // color-45-size-10
		if ($("#"+all_textboxid[i]).val()!="0.00" && $("#"+all_textboxid[i]).val() != ''){
		  $("#"+makecheckboxid).attr('checked', true);
		  $("#"+textboxid).removeAttr("disabled",false);
		  $("#"+textboxid).css({'background-color': '#fff'}); 
		}else{	   
		  $("#"+makecheckboxid).attr('checked',false);
		  $("#"+textboxid).attr('disabled', true);
		  $("#"+textboxid).val('');
		  $("#"+textboxid).css({'background-color': '#D1D1D1'}); 
		}
	 } 
    });   
    
    $( document ).ready(function() {
	 var rowCount = $('#Decore tr').length;	 
	 for (var i = 1; i < rowCount; i++) {
	   $emptyLenght = $("#row-"+i).find('input[type=text]:empty').length;
	   
	   var count = 0;
	   $("#row-"+i+" input:text").each(function(){
		if (this.value == '') {
		  count++;
		}
	   });
	   if (count == $emptyLenght){
		$("#row-"+i+" input:checkbox").attr('checked', false);
		$("#row-"+i).find("input").attr("disabled", true);
		$("#row-"+i).find("input").css({'background-color': '#D1D1D1'})
		$("input[class=allow_color]").attr('disabled',false);
	   }
	 }
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: get all the vertical checkbox's id
     */   
    $( document ).ready(function() {	 
	 var someObj={};
	 someObj.fruitsGranted=[];
	 someObj.fruitsDenied=[];	
	   $(".allow_color:input:checkbox").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.fruitsGranted.push($this.attr("id"));
		  }else{
			 someObj.fruitsDenied.push($this.attr("id"));
		  }
	   });	
	
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : check and unchekck the horizontal checkbox
     */    
    $(document ).ready(function(){
	   var ele=[];
	   $('input:checkbox[id^="vartical_"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_checkboxid=ele;
	   var length=all_checkboxid.length;
	   //alert(all_checkboxid);	   
	   for(i=0;i<length;i++){
		var text=[];
	       $('input:text[id^="vartical_"]').each(function(){
		    var $this=  $(this);
		    text.push($this.attr("id"));
		    
		   });
	     }
		text.sort();		  
		  var all_text=text;
		  var textlength=all_text.length;		  
		  for(j=0;j<textlength;j++){		    
		    if ($("#"+all_text[j]).val()!=""){			
			 var current_textbox=all_text[j].substr(9,1);   // vartical_21 return 2			
			 $("#vartical_"+current_textbox).attr('checked', true);
		    }
		  }
		  
		  var emb=[];
	   $('input:checkbox[id^="embrodary_"]').each(function(){
	    var $this=  $(this);
	    emb.push($this.attr("id"));	    
	   });
	   emb.sort();
	   var all_embrodaryCheckbox=emb;
	   var embrodaryLength=all_embrodaryCheckbox.length;
	  
	   for(i=0;i<embrodaryLength;i++){
		var embtext=[];
	       $('input:text[id^="emb_"]').each(function(){
		    var $this=  $(this);
		    embtext.push($this.attr("id"));
		    
		   });
	     }
		embtext.sort();		  
		  var all_embText=embtext;
		  var emblength=all_embText.length;		  
		  for(j=0;j<emblength;j++){		    
		    if ($("#"+all_embText[j]).val()!=""){			
			 var current_embbox=all_embText[j].substr(4,1);  
			 $("#embrodary_"+current_embbox).attr('checked', true);			 
		    }
		  } 
		  
		  
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : check and unchekck the horizontal checkbox for embroiedary 
     */       
    $(document ).ready(function(){
	  var ele=[];
	   $('input:checkbox[id^="embrodary_"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_checkboxid=ele;
	   var length=all_checkboxid.length;	   
	   for(i=0;i<length;i++){
		var text=[];
	       $('input:text[id^="emb_"]').each(function(){
		    var $this=  $(this);
		    text.push($this.attr("id"));		    
		  });
	   }
	   var all_text=text;	   
	   var textlength=all_text.length;
	   for(j=0;j<textlength;j++){		    
		if ($("#"+all_text[j]).val()!=""){
		  var current_textbox=all_text[j].substr(4,1);   // vartical_21 return 2		  
		  $("#embrodary_"+current_textbox).attr('checked', true);
		}
	   }	   
   });   
    
    $( document ).ready(function() {
	 var someObj={};
	 someObj.fruitsGranted=[];
	 someObj.fruitsDenied=[];	
	   $(".simple:input:text").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.fruitsGranted.push($this.attr("id"));
		  }else{
			 someObj.fruitsDenied.push($this.attr("id"));
		  }
	   });	   
	   var all_cheked_checkbox=someObj.fruitsGranted;	   
	   var length=all_cheked_checkbox.length;
	   
	   for(i=0;i<length;i++){
		$checked_id=all_cheked_checkbox[i];
		$("#vartical_"+$checked_id).attr('checked', true);		
	   } 
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : disabled or enabled text box for all colors
     */    
    $( document ).ready(function(){ 
	 var ele=[];
	   $('input:text[id^="varti"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_textboxid=ele;
	   var length=all_textboxid.length;
	   
	   //alert(length);
	   for(i=0;i<length;i++){
	   var str=all_textboxid[i];//'vartical_31'
	   var horizontal_textbox=str.substring(10); //vartical_21 return 1
	   var vertical_textbox=str.substr(9,1);   // vartical_21 return 2
	   //alert(str);	   
	   var check_horizontal_textbox=(document.getElementById(horizontal_textbox).checked);	   
	   var vertical="vartical_"+vertical_textbox;
	   var check_vertical_textbox=(document.getElementById(vertical).checked);	   
		if (check_horizontal_textbox && check_vertical_textbox){		
		  $("#vartical_"+vertical_textbox+horizontal_textbox).removeAttr("disabled",false);
		  $("#vartical_"+str).css({'background-color': '#fff'})    		
		}else{		
		  $("#vartical_"+vertical_textbox+horizontal_textbox).attr("disabled",true);
		  $("#vartical_"+vertical_textbox+horizontal_textbox).attr('value','');
		  $("#vartical_"+vertical_textbox+horizontal_textbox).css({'background-color': '#D1D1D1'})    
		  
		}	   
	  }  
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : disabled or enabled text box for all colors
     */
    $( document ).ready(function(){
	  var ele=[];
	  $('input:text[id^="emb_"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_textboxid=ele;	   
	   var length=all_textboxid.length;
	   for(i=0;i<length;i++){
		var embroidery_checkbox=(document.getElementById('embroidery').checked); 
		var textboxid=all_textboxid[i].substr(4,1); //vartical_21 return 2		
		var vertical="embrodary_"+textboxid;	  
		var check_vertical_textbox=(document.getElementById(vertical).checked);
		
		if (embroidery_checkbox && check_vertical_textbox){   
		  $("#emb_"+textboxid).removeAttr("disabled",false);
		  $("#emb_"+textboxid).css({'background-color': '#fff'})  
		}else{
		   $("#emb_"+textboxid).attr("disabled",true);
		   $("#emb_"+textboxid).attr('value','');
		   $("#emb_"+textboxid).css({'background-color': '#D1D1D1'})   
		}
	   } 
    });   
    
    $('.allow_color').change(function() {	 
	 var id = $(this).attr('id');	 
	 var check_currentid=(document.getElementById(id).checked);
	 var someObj={};
	 someObj.fruitsGranted=[];
	 someObj.fruitsDenied=[];	
	   $(".allow_colors:input:checkbox").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.fruitsGranted.push($this.attr("id"));
		  }else{
			 someObj.fruitsDenied.push($this.attr("id"));
		  }
	  });	
	 var all_cheked_checkbox=someObj.fruitsGranted; //all bottom of the checkbox 
	 var length=all_cheked_checkbox.length;
		for(i=0;i<length;i++){	
		 var another_cheked=(document.getElementById(all_cheked_checkbox[i]).checked); //1		 
		 var create_id=all_cheked_checkbox[i];// vartical_1		 
		 var current_checkbox=id; //1
		 
		 var check_current=(document.getElementById(current_checkbox).checked);		 
		   if (check_current && another_cheked){		    
		    var another=all_cheked_checkbox[i];
		    var str=another;
		    var another_check=str.substring(9);		   //1  another+current	    
			$("#vartical_"+another_check+current_checkbox).removeAttr("disabled");
			$("#vartical_"+another_check+current_checkbox).css({'background-color': '#fff'})    
			$("input[class=allow_color]").attr('disabled',false);
		   }else{
		      var another=all_cheked_checkbox[i];
			 var str=another;
			 var another_check=str.substring(9);	
			$("#vartical_"+another_check+current_checkbox).attr("disabled");
			$("#vartical_"+another_check+current_checkbox).attr('value','');
			$("#vartical_"+another_check+current_checkbox).css({'background-color': '#D1D1D1'})    
			$("input[class=allow_color]").attr('disabled',false);
		   }	 	 
	    }	 
    });    
    $('.simple').keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || 
            (event.keyCode == 65 && event.ctrlKey === true) || 
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
 
   $('.embroidery').change(function(){	  
      var id = $(this).attr('id');
	 var check_currentid=(document.getElementById(id).checked);
	 var someObj={};
	 someObj.checked=[];
	 someObj.unchecked=[];	
	   $(".allow_colors1:input:checkbox").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.checked.push($this.attr("id"));
		  }else{
			 someObj.unchecked.push($this.attr("id"));
		  }
	  });
	    var all_cheked_checkbox=someObj.checked; //all bottom of the checkbox
	    var length=all_cheked_checkbox.length;
	    
	    for(i=0;i<length;i++){			
		var another_cheked=(document.getElementById(all_cheked_checkbox[i]).checked); //1		
		var current_checkbox=id; //1		
		var check_current=(document.getElementById(current_checkbox).checked);
		  if (check_current && another_cheked){
		    var another=all_cheked_checkbox[i];
		    var str=another;		    
		    var another_check=str.substring(10);	    
		    $("#emb_"+another_check).removeAttr("disabled");
		    $("#emb_"+another_check).css({'background-color': '#fff'})    
		    $("input[class=simple]").attr('disabled',false);
		  }else{
		    var another=all_cheked_checkbox[i];
		    var str=another;
		    var another_check=str.substring(10);
		    $("#emb_"+another_check).attr("disabled");
		    $("#emb_"+another_check).attr('value','');
		    $("#emb_"+another_check).css({'background-color': '#D1D1D1'})    
		    $("input[class=simple]").attr('disabled',false);	  
		  }	
	   }   
  });
   
 function duplicate_old(fid,id) {
  var act_id = '#col_'+id;
  $(act_id).toggle();
 }
  function duplicate(fid,tid) {
    
    var data=myStringArray;
    var jsn = JSON.parse(data);    
    var dupid = '#dup_'+fid;    
    for (var i = 0; i < jsn .length; i++) { 
	 //alert(myStringArray[1]);return false;
      var fromid = '#color-'+fid+'-size-'+jsn[i]+'-o';	 
      var toid = '#color-'+tid+'-size-'+jsn[i]+'-o';	 
      var chekid = '#color-'+tid+'-size-'+jsn[i];
	 if($(dupid).is(':checked')){
	var fval = $(fromid).val();
        $(toid).val(fval);
	if (fval != '') {
	  $(toid).attr('disabled',false);
	  $(chekid).attr('checked',true);
	  $(toid).css({'background-color':'#fff'});	  //code
  	}

      }else{
        $(toid).val('');
	$(toid).attr('disabled',true);
	$(chekid).attr('checked',false);	
      }
    //alert(myStringArray[i]);
    //Do something
    }
    //alert(this.value);
    //var act_id = '#col_'+id;
    //$(act_id).toggle();
  }
  
  function disable(table_id)
    {
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    for(var i=0; i<inputs.length; ++i)
        inputs[i].disabled=true;
    }
    
  function enable(table_id){
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    for(var i=0; i<inputs.length; ++i)
      inputs[i].disabled=false;
  }
  $(function() {
    $('#CopyDecoration').click(function(){
      if(this.checked){
	disable('Decore');
	$("#CopyQuantityRange").attr('disabled',true);
	 $("#pro_temp").show();
      }else{
	enable('Decore');
	 $("#pro_temp").hide();
	 $("#CopyQuantityRange").removeAttr('disabled',true);
      }
    });
  });
  $(function() {
    $('#CopyQuantityRange').click(function(){
      if(this.checked){
	disable('Decore');
	$("#CopyDecoration").attr('disabled',true);
	 $("#qty_temp").show();
      }else{
	enable('Decore');
	 $("#qty_temp").hide();
	 $("#CopyDecoration").removeAttr('disabled',true);
      }
    });
  });
  
  $(function() {
    $('#CopyEmbrodary').click(function(){
      if(this.checked){
	disable('Embrodary_decoration');
	$("#CopyEmrodaryQuantityRange").attr('disabled',true);
	 $("#embr_pro_temp").show();
      }else{
	enable('Embrodary_decoration');	
	 $("#embr_pro_temp").hide();
	 $("#CopyEmrodaryQuantityRange").removeAttr('disabled',true);
      }
    });
  });
  
  $(function() {
    $('#CopyEmrodaryQuantityRange').click(function(){
      if(this.checked){
	disable('Embrodary_decoration');
	 $("#Embr_qty_temp").show();
	 $("#CopyEmbrodary").attr('disabled',true);
      }else{
	enable('Embrodary_decoration');
	 $("#Embr_qty_temp").hide();
	  $("#CopyEmbrodary").removeAttr('disabled',true);
      }
    });
  });
  
  
  
  var loaderimage='{/literal}{$admin_image_path}ajax-loader.gif{literal}';
  //alert(loaderimage);
  
  function load_decoration(id){
    if (id!='') {
	var site_url = '{/literal}{$admin_url}{literal}';
	var pid = '&iProductId='+'{/literal}{$product_id}{literal}'+'&qid='+id;
	$('#ajax-load-deco').html('<img src="'+loaderimage+'">');
	$.ajax({
		type: "POST",
		url: site_url+"product/load_decoration?"+pid,	    
		success: function(data){
		    $('#ajax-load-deco').html(data);
	    }
	});
    }else{
	 return false;
    }
    
  }
  var id=$("#iQuantityRangeId").val();
  
  function loadEmrodary(id){    
    if (id!=''){	 
	 var site_url = '{/literal}{$admin_url}{literal}';
	 var pid = '&iProductId='+'{/literal}{$product_id}{literal}'+'&qid='+id;
	 //alert(site_url+"product/loadEmbrodaryDecoration?"+pid);return false;
	 $('#ajax-load-embrodary').html('<img src="'+loaderimage+'">');
	 $.ajax({
		 type: "POST",
		 url: site_url+"product/loadEmbrodaryDecoration?"+pid,	    
		 success: function(data){
			$('#ajax-load-embrodary').html(data);
		}
	 });
    }
  }
  
  
  
  
  
  
  
  </script>
{/literal}
