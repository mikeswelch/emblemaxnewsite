<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}product/productlist">Product Template</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Product{else}Edit Product Template{/if}</li>
		</ul>
	</div>
	{if $operation neq 'add'}
			<div class="pagetitle_tab">
				<ul>
				{section name=i loop=$language}
				{if $language[i]->vLangCode eq $lang}
				<li class="active"><a href="#">{$language[i]->vLanguage}</a></li>
				{else}
				<li><a href="{$admin_url}product/edit?iProductId={$data->iProductId}&lang={$language[i]->vLangCode}">{$language[i]->vLanguage}</a></li>
				{/if}
				{/section}
				</ul>
			</div>
	{else}
			<div class="pagetitle_tab">
				<ul>
				{section name=i loop=$language}
				{if $language[i]->vLangCode eq $lang}
				<li class="active"><a href="#">{$language[i]->vLanguage}</a></li>
				{else}
				<li class="inactive"><a href="#">{$language[i]->vLanguage}</a></li>
				{/if}
				{/section}
				</ul>
			</div>
	{/if}
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg" style="width:49%;float:left;">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Product Template
			{else}
			Edit Product Template
			
			
			{/if} </div>
		
		
			<div class="add_ad_contentbox">
				<input type="hidden" name="iProductId" id="iProductId" value="{if $operation neq 'add'}{$data->iProductId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="vImage" id="vImage_old" value="{$data->vImage}" />
				{if $operation eq 'add'}
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="en" />
				{else}
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="{$lang}" />
				{/if}
                                 <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Category</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iCategoryId]" onchange="makesizediv(this.value)">
						
						<option value=''>--Select Product Category--</option>
						{section name=i loop=$db_producttype}
						<option value='{$db_producttype[i]['iCategoryId']}' {if $operation neq 'add'}{if $data->iCategoryId eq $db_producttype[i]['iCategoryId']}selected{/if}{/if}>{$db_producttype[i]['vCategory']}</option>
						{/section}
					</select>
					
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vProductName" name="data[vProductName]" class="inputbox" value="{if $operation neq 'add'}{$Data->vProductName}{/if}" lang="*" title="Product Name" />
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}"{if $data->vImage eq ''} lang="*" {/if} onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
				{if $operation neq 'add'}{if $data->vImage neq ''}
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('{$data->iProductId}','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="popfancy"><img src="{$upload_path}/product/{$data->iProductId}/1_{$data->vImage}"></div>
						</div>
				         </div>
					{/if}{/if}
                                <div class="inputboxes">
				        <label for="textarea"><span class="red_star"></span> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tDescription" class="inputbox" title="Description" name="data[tDescription]" id="tDescription">{if $operation neq 'add'}{$Data->tDescription}{/if}</textarea>
				</div>        
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Price</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fPrice" name="Data[fPrice]" class="inputbox" title="fPrice"  lang="*" value="{if $operation neq 'add'}{$data->fPrice}{/if}" onkeypress="return checkprise(event)">
				</div>								
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Product Template" class="submit_btn" title="Add Product" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Product Template" class="submit_btn" title="Edit Product" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
	<div class="centerpartbg" style="float:left;width:49%;margin-left:20px;min-height:100px;">
	<div class="pagetitle">SEO Search</div>
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					<label for="textfield"> Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vSeoTitle" name="data[vSeoTitle]" class="inputbox" value="{if $operation neq 'add'}{$Data->vSeoTitle}{/if}" title="Seo Title" />
				</div>				
		                <div class="inputboxes">
				        <label for="textarea"> Keyword</label>
						<span class="collan_dot">:</span>
				        <input type="text" id="vSeoKeyword" name="data[vSeoKeyword]" class="inputbox" value="{if $operation neq 'add'}{$Data->vSeoKeyword}{/if}" title="Seo Keyword" />
				</div>
				<div class="inputboxes">
				        <label for="textarea"> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tSeoDescription" class="inputbox" title="Seo Description" name="data[tSeoDescription]" id="tSeoDescription">{if $operation neq 'add'}{$Data->tSeoDescription}{/if}</textarea>
				</div>
			</div>
		
	</div>
	<div class="centerpartbg" style="float:right;width:49%;margin-right:6px;min-height:235px;margin-top:20px;">
	<div class="pagetitle">Size Selection</div>
	
		<div class="add_ad_contentbox" id="sizedivid">
				
				{if  $sizelist|@count gt 0 && $operation neq 'add'}
				<div class="inputboxes" style="padding-bottom:0px;">
				<label for="textfield"></label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Width</label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Length</label>
				</div>
				{section name=i loop=$sizelist}
				<div class="inputboxes">
				<label for="textfield">{$sizelist[i]->vTitle}&nbsp;({$sizelist[i]->vSize})</label>
				<span class="collan_dot">:</span>
				<input type="hidden" id="" name="SIZE[{$smarty.section.i.index}][iSizeId]" value="{$sizelist[i]->iSizeId}"/>
				<input type="text" id="Width{$smarty.section.i.index}" name="SIZE[{$smarty.section.i.index}][iWidth]" class="inputbox" title="Width" style="width:100px;" value="{$sizelist[i]->iWidth}" onkeypress="return checkprise(event)"/>
				<input type="text" id="Length{$smarty.section.i.index}" name="SIZE[{$smarty.section.i.index}][iLength]" class="inputbox" title="Length" style="width:100px;margin-left:10px;" value="{$sizelist[i]->iLength}" onkeypress="return checkprise(event)"/>
				</div>
				{/section}
				{else}
				<div style="text-align:center;color:#C44C22; font-size:14px; font-weight:bold;">No Size available for this category</div>
				{/if}

			</div>
	</div>
	</form>
	<div class="centerpartbg" style="width:50%;float:left;margin-top:20px;min-height:250px;margin-left:25%;">
		<div id="relaceformcolor">
		<div class="pagetitle" style="text-align:center;">
			Add Color Image
		</div>
		<form id="frmaddcolor" name="frmaddcolor" method="post" enctype="multipart/form-data"  action="">
			<input type="hidden" name="iProductId" id="iProductId" value="{$data->iProductId}" />
			<input type="hidden" name="type" id="type" value="color" />
			<input type="hidden" name="mode" id="mode" value="add" />
			<input type="hidden" name="lang" id="lang" value="{$lang}" />
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					
					<label for="textfield"><span class="red_star">*</span> Color :</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[iColorId]" lang="*" title="color">
						
						<option value=''>--Select Color--</option>
						{section name=i loop=$color}
						<option value='{$color[i]->iColorId}'>{$color[i]->vColor}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Front Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Back Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[eStatus]">
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<input type="submit" value="Add Color Image" class="submit_btn" title="Add Color Image" onclick="return validate(document.frmaddcolor);"/>
				</div>
			</div>
		</form>
		</div>
	</div>
	
	<div class="centerpartbg" style="width:100%;float:left;margin-top:20px;" id="colorformadd">
		<div class="pagetitle">
			Color Image list
		</div>
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{/if}
		
			<div class="add_ad_contentbox">
				<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}product/search_action" method="post">
				<input  type="hidden" name="iProductId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th>Color Name</th>
							<th>Color code</th>
							<th width="77px">Status</th>
							<th width="90px">Action</th>
						</tr>
					</thead>
					{if $data_color|@count gt 0}
					{section name=i loop=$data_color}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}" style="font-size:13px;">
						<tr>
							<td><a href="javascript:void(0);" onclick="editform({$data_color[i]->iProductColorId});">{$data_color[i]->vColor}</a></td>
							<td>{$data_color[i]->vColorCode}</td>											
							<td>{$data_color[i]->eStatus}</td>
							<td><a href="javascript:void(0);" onclick="editform({$data_color[i]->iProductColorId});"><img src="{$admin_image_path}icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommoncolor({$data_color[i]->iProductColorId},'{$lang}','product/deletecolor',{$data_color[i]->iProductId})"></a> </td>
						</tr>
					</tbody>
					{/section}
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					{/if}
				</table>
			</form>
		</div>
			</div>
	</div>
	
	<div class="clear"></div>
	
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
         	
         	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrefmain").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});
$("#fancyhrefback").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});
function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}product/deleteimage?id={/literal}{$data->iProductId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

function makesizediv(val)
{
	var site_url = '{/literal}{$admin_url}{literal}';
	if(val == '{/literal}{$data->iCategoryId}{literal}')
	{
	var url = site_url+"product/makesizedefault?iCategoryId="+val+"&iProductId="+'{/literal}{$data->iProductId}{literal}';	
	}
	else{
	var url = site_url+"product/makesize?iCategoryId="+val;
	}
	var pars = '';
	$.post(url+pars,
	    function(data) {
		$('#sizedivid').html(data);
	    });
}


function editform(id)
{
	//alert(id);
	var site_url = '{/literal}{$admin_url}{literal}';
	var url = site_url+"product/makeeditcolor?iProductColorId="+id+"&lang="+'{/literal}{$lang}{literal}';	
	var pars = '';
	//alert(url+pars);
	$.post(url+pars,
	    function(data) {
		//alert(data);
		$('#relaceformcolor').html(data);
	    });
}
</script>
{/literal} 
