<div class="pagetitle" style="text-align:center;">
			Edit Color Image
		</div>
			<form id="frmaddcolor" name="frmaddcolor" method="post" enctype="multipart/form-data"  action="">
			<input type="hidden" name="iProductColorId" id="iProductColorId" value="{$data[0]->iProductColorId}" />
                        <input type="hidden" name="iProductId" id="iProductId" value="{$data[0]->iProductId}" />
			<input type="hidden" name="operation" id="operation" value="{$operation}" />
			
                        <input type="hidden" name="type" id="type" value="color" />
			{if $operation eq 'edit'}
			<input type="hidden" name="mode" id="mode" value="edit" />
			{else}
			<input type="hidden" name="mode" id="mode" value="edit" />
			{/if}
			<input type="hidden" name="lang" id="lang" value="{$lang}" />
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Color :</label>
					 <span class="collan_dot">:</span>					 
					<select id="eStatus" name="Datacolor[iColorId]" lang="*" title="color">
			                        <option value=''>--Select Color--</option>
						{section name=i loop=$color}
						<option value='{$color[i]->iColorId}' {if $color[i]->iColorId eq $data[0]->iColorId}selected{/if}>{$color[i]->vColor|@ucfirst}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Front Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
			                {else}
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image"  onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
                                {if $operation neq 'add'}{if $data[0]->vFrontImage neq ''}
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#Fancyhrefmain" id="fancyhrefmain" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteFrontImage('{$data[0]->iProductColorId}','product','vFrontImage');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="Fancyhrefmain" style="padding-bottom: 900px;"><img src="{$upload_path}product/{$data[0]->iProductId}/{$data[0]->iProductColorId}/200X200_{$data[0]->vFrontImage}"></div>
						</div>
				         </div>
				{/if}{/if}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Back Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image"   onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
                                {if $operation neq 'add'}{if $data[0]->vBackImage neq ''}
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#Fancyhrefback" id="fancyhrefback" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteBackImage('{$data->iProductId}','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="Fancyhrefback" style="padding-bottom: 900px;"><img src="{$upload_path}/product/{$data[0]->iProductId}/{$data[0]->iProductColorId}/200X200_{$data[0]->vBackImage}"></div>
						</div>
				         </div>
				{/if}{/if}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Left Sleeve Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vLeftSleeveImage"  name="vLeftSleeveImage" title="Left Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vLeftSleeveImage"  name="vLeftSleeveImage" title="Left Image"  onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
                                {if $operation neq 'add'}{if $data[0]->vLeftSleeveImage neq ''}
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#FancyhrefleftSleeeve" id="fancyhrefleftSleeeve" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteLeftImage('{$data->iProductId}','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="FancyhrefleftSleeeve"><img src="{$upload_path}/product/{$data[0]->iProductId}/{$data[0]->iProductColorId}/200X200_{$data[0]->vLeftSleeveImage}"></div>
						</div>
				         </div>
				{/if}{/if}	
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Right Sleeve Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vRightSleeveImage"  name="vRightSleeveImage" title="Right Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vRightSleeveImage"  name="vRightSleeveImage" title="Right Image"   onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
                                {if $operation neq 'add'}{if $data[0]->vRightSleeveImage neq ''}
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#FancyhrerightSleeeve" id="fancyhrerightSleeeve" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteRightImage('{$data->iProductId}','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="FancyhrerightSleeeve"><img src="{$upload_path}/product/{$data[0]->iProductId}/{$data[0]->iProductColorId}/200X200_{$data[0]->vRightSleeveImage}"></div>
						</div>
				         </div>
				{/if}{/if}
						
                                
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[eStatus]">
						<option value="Active" {if $data[0]->eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $data[0]->eStatus eq Inactive}selected{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
						<input type="submit" value="Add Color Image" class="submit_btn" title="Add Color Image" onclick="return validate(document.frmaddcolor);"/>
						{else}
						<input type="submit" value="Edit Color Image" class="submit_btn" title="Edit Color Image" onclick="return validate(document.frmaddcolor);"/>
						{/if}
				</div>
			</div>
</form>

{literal}
<script type="text/javascript">
         	
function DeleteFrontImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}product/deleteColorimage?id={/literal}{$data[0]->iProductColorId}{literal}&productId={/literal}{$data[0]->iProductId}{literal}&img=vFrontImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}

function DeleteBackImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}product/deleteColorimage?id={/literal}{$data[0]->iProductColorId}{literal}&productId={/literal}{$data[0]->iProductId}{literal}&img=vBackImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}
function DeleteRightImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}product/deleteColorimage?id={/literal}{$data[0]->iProductColorId}{literal}&productId={/literal}{$data[0]->iProductId}{literal}&img=vRightSleeveImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}
function DeleteLeftImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}product/deleteColorimage?id={/literal}{$data[0]->iProductColorId}{literal}&productId={/literal}{$data[0]->iProductId}{literal}&img=vLeftSleeveImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}
{/literal}