{if  $sizelist|@count gt 0}
<div class="inputboxes" style="padding-bottom:0px;">
<label for="textfield"></label>
<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Width</label>
<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Length</label>
</div>
{section name=i loop=$sizelist}
<div class="inputboxes">
<label for="textfield">{$sizelist[i]->vTitle}&nbsp;({$sizelist[i]->vSize})</label>
<span class="collan_dot">:</span>
<input type="hidden" id="" name="SIZE[{$smarty.section.i.index}][iSizeId]" value="{$sizelist[i]->iSizeId}"/>
<input type="text" id="Width{$smarty.section.i.index}" name="SIZE[{$smarty.section.i.index}][iWidth]" class="inputbox" title="Width" style="width:100px;" value="{$sizelist[i]->iWidth}" onkeypress="return checkprise(event)"/>
<input type="text" id="Length{$smarty.section.i.index}" name="SIZE[{$smarty.section.i.index}][iLength]" class="inputbox" title="Length" style="width:100px;margin-left:10px;" value="{$sizelist[i]->iLength}" onkeypress="return checkprise(event)"/>
</div>
{/section}
{else}
<div style="text-align:center;color:#C44C22; font-size:14px; font-weight:bold;">No Size available for this category</div>
{/if}
