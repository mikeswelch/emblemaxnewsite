{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		<li><a href="{$admin_url}dashboard">Dashboard</a></li>
		<li>/</li>
		<li class="current">System Email</li>
	</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">System Email</div>
        {if $var_msg neq ''}
         <div class="status success" id="errormsgdiv"> 
            	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
            	<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />
                  {$var_msg}</p> 
         </div>     
        <div></div>
        {elseif $var_msg neq ''}
        <div class="status success" id="errormsgdiv"> 
            	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
            	<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />
                  {$var_msg}</p> 
         </div>     
        <div></div>
        {/if}
	<div class="admin_top_part">
		<div class="search_top_admini">
			<form name="frmsearch" id="frmsearch" action="{$admin_url}systememail/systememaillist" method="post">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<label>Search:</label>
				<div id="newstatus" class="statusleft">
					{if $keyword == 'Active' || $keyword == 'Inactive'}
						<select name="keyword" id="keyword">
							<option value="Active"{if $keyword != ''}{if $keyword eq 'Active'}selected="selected"{/if}{/if}>Active</option>
							<option value="Inactive"{if $keyword != ''}{if $keyword eq 'Inactive'}selected="selected"{/if}{/if}>Inactive</option>
						</select>
					{else}
						<input type="Text" id="keyword" name="keyword" value="{if $keyword != ''}{$keyword}{/if}"  class="search_input" />
					{/if}
				</div>
				<select name="option" id="option" onchange="estatusdd(this.value);">
					<option value="vEmailTitle"{if $option eq 'vEmailTitle'}selected{/if}>Email Title</option>
					<option value="vFromEmail"{if $option eq 'vFromEmail'}selected{/if}>From Email</option>
					<option value="vEmailCode"{if $option eq 'vEmailCode'}selected{/if}>Email Code</option>
				</select>
				<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
	    
			</form>
		</div>
		<div class="clear"></div>
		<div class="action_apply_btn">
			<select class="act_sl_box" id="newaction" name="newaction">
				<option value="">Select Action</option>
				<option value="Active"{if $action == 'Active'}selected="selected"{/if}>Make Active</option>
				<option value="Inactive"{if $action == 'Inactive'}selected="selected"{/if}>Make Inactive</option>
				<option value="Deletes"{if $action == 'Deletes'}selected="selected"{/if}>Make Delete</option>
				<option value="Show All"{if $action == 'Show All'}selected="selected"{/if}>Show All</option>
			</select>
			<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'systememail/systememaillist',document.frmlist,'System Email');"/>
		</div>
		<div class="pagination">
			{$AlphaBox}
		</div>
		<div class="clear"></div>
	</div>
	<div class="administator_table">
	  <form name="frmlist" id="frmlist"  action="{$admin_url}systememail/search_action" method="post">
	  <input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
          <input type="hidden" name="action" id="action" value="" />
          <input  type="hidden" name="iEmailTemplateId" value=""/>
	  <input  type="hidden" name="commonId" value=""/>
		<table cellpadding="0" cellspacing="1" width="100%">
			<thead>
				<tr>
					<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
					
					<th><a href={$admin_url}systememail/systememaillist?field=vEmailTitle&order={$order}><span style="color:white;">Email Title</span>{if $field eq 'vEmailTitle'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
					<th><a href={$admin_url}systememail/systememaillist?field=vFromEmail&order={$order}><span style="color:white;">From Email</span>{if $field eq 'vFromEmail'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
					<th width="60px"><a href={$admin_url}systememail/systememaillist?field=estatus&order={$order}><span style="color:white;">Status</span> {if $field eq 'estatus'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
					<th width="90px">Action</th>
           					
				</tr>
			</thead>
			{if $data|@count gt 0}
			{section name=i loop=$data}
			{if $smarty.section.i.index % 2 eq 0}
			{assign var='class' value='admin_antry_table_sec'}
			{else}
			{assign var='class' value='admin_antry_table'}
			{/if}
			<tbody class="{$class}">
				<tr>
					<td><input name="iEmailTemplateId[]" type="checkbox" id="iId" value="{$data[i]->iEmailTemplateId}"></td>
					<td><a href="{$admin_url}systememail/edit?iEmailTemplateId={$data[i]->iEmailTemplateId}">{$data[i]->vEmailTitle}</a></td>
					<td>{$data[i]->vFromEmail}</td>
					<td>{$data[i]->eStatus}</td>
					<td>
					<a href="{$admin_url}systememail/edit?iEmailTemplateId={$data[i]->iEmailTemplateId}"><img src="{$admin_image_path}icon_edit.png" alt="Edit" title="Edit"></a>
					<a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iEmailTemplateId}','Active');"><img src="{$admin_image_path}icon_approve.png" alt="Active" title="Active"></a>
					<a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iEmailTemplateId}','Inactive');"><img src="{$admin_image_path}icon_unapprove.png" alt="Inactive" title="Inactive"></a>
					<a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon({$data[i]->iEmailTemplateId},'systememail/delete','{$ssql}')"></a>
					</td>
				</tr>
			</tbody> 
			{/section}
			{else}
					   <tr><td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td></tr>
			{/if}
		</table>
		</form>
	</div>
		
	<div class="bottomBox_admini">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td class="reclord_found">{$recmsg}</td>
				
				<td>
					<div class="bottom_admin_paging">
						{$page_link}
					</div>
				</td>
			</tr>
		</table>
		
		<div></div>
	</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script >
function estatusdd(val1){
	//alert(val1);
	var keyword=$('#keyword').val();
	if(keyword =='Active' || keyword =='Inactive')
	{
		keyword='';
	}
	if(val1 == 'eStatus')
	{
		$('#newstatus').html('<select name="keyword" id="keyword"><option value="Active"{if $keyword != ""}{if $keyword eq "Active"}selected="selected"{/if}{/if}>Active</option><option value="Inactive"{if $keyword != ""}{if $keyword eq "Inactive"}selected="selected"{/if}{/if}>Inactive</option></select>');
	}
	else{
		$('#newstatus').html('<input type="Text" id="keyword" name="keyword" value="'+keyword+'"  class="search_input" />');
	}
}
</script>
{/literal}