<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
    <div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		<li><a href="{$admin_url}dashboard">Dashboard</a></li>
		<li>/</li>
		<li><a href="{$admin_url}systememail/systememaillist">System Email</a></li>
		<li>/</li>
		<li class="current">Edit System Email</li>
	</ul>
    </div>
    <div class="centerpartbg">
           
            <div class="pagetitle">Edit System Email</div>
            
            <div class="add_ad_contentbox">
                    <form id="frmadd" name="frmadd" method="post" action="{$action}">
                            <input type="hidden" name="iEmailTemplateId" id="iEmailTemplateId" value="{$data->iEmailTemplateId}" />
                            <input type="hidden" name="action" id="action" value="add" />
			    
                            <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span> Email Title</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vEmailTitle" name="Data[vEmailTitle]" class="inputbox" value="{$data->vEmailTitle}" lang="*" title="Email Title" style="width:252px" />
			    </div>
			    
			    <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span>From Email</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vFromEmail" name="Data[vFromEmail]" class="inputbox" value="{$data->vFromEmail}" lang="*" title="From Email" style="width:252px" />
			    </div>
			    
			    <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span> Email Subject</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vEmailSubject" name="Data[vEmailSubject]" class="inputbox" value="{$data->vEmailSubject}" lang="*" title="Email Subject" style="width:252px" />
			    </div>
			    
			    <div class="inputboxes worddocument_input">
			             <label  for="textarea" style="width:220px;"><span class="red_star"></span> Email Message </label>
				     <span >:</span>
				     <div class="worddocument" style="margin-left:250px; margin-top:-15px;"><textarea id="tEmailMessage_{$language_data[0]->vLangCode}"  name="Data[tEmailMessage_{$language_data[0]->vLangCode}]" class="inputbox" title="Email Message" >{$data->tEmailMessage_{$language_data[0]->vLangCode}}</textarea></div>
			    </div>
{literal}
<script type="text/javascript">
var id = '{/literal}{$language_data[0]->vLangCode}{literal}';
	CKEDITOR.replace('tEmailMessage_'+id);
	
</script>
{/literal}
			    
			   <div style="clear:both;"></div><br>
			    <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span> Email Footer</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vEmailFooter" name="Data[vEmailFooter]" class="inputbox" value="{$data->vEmailFooter}" lang="*" title="Email Footer" style="width:252px" />
			    </div>
					       
                            <div class="inputboxes">
                                    <label for="textfield" style="width:220px;"><span class="red_star"></span> Status</label>
				    <span class="collan_dot">:</span>
                                    <select id="eStatus" name="Data[eStatus]">
                                            <option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
                                            <option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
                                    </select>
                            </div>
                          
                            <div class="add_can_btn" >                                    
                                    <input type="submit" value="Edit System Email" class="submit_btn" title="Edit System Email" onclick="return validate(document.frmadd);" style="margin-left: 45px;"/>
                                    <a href="{$admin_url}systememail/systememaillist" class="cancel_btn">Cancel</a>
                            </div>
                    </form>
            </div>
    </div>
    <div class="clear"></div>
</div>
{include file="admin/footer.tpl"}

