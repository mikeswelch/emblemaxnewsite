{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}contactus/contactuslist">Contact Us</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Contact Us{else}Edit Contact Us{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			 Contact Us Page
			 </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iContactId" id="iContactId" value="{if $operation neq 'add'}{$data->iContactId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					<label for="textfield"> Name</label>
					<span class="collan_dot">:</span>
					{if $operation neq 'add'}<div style="color:#A9A9A9">{$data->vName}</div>{/if}
				</div>
				
				<div class="inputboxes">
					<label for="textfield"> Email</label>
					<span class="collan_dot">:</span>
					{if $operation neq 'add'}<div style="color:#A9A9A9">{$data->vEmail}</div>{/if}
				</div>
				<div class="inputboxes">
					<label for="textfield">Question</label>
					<span class="collan_dot">:</span>
					{if $operation neq 'add'}<div style="color:#A9A9A9">{$data->tQuestion}</div>{/if}
				</div>
				<div class="inputboxes">
				      <label for="textfield"> Added Date</label>
				      <span class="collan_dot">:</span>
				      {if $operation neq 'add'}<div style="color:#A9A9A9">{$data->dAddedDate}</div>{/if}
			        
				</div>
				<div class="inputboxes">
					<label for="textfield"></span> Status</label>
					<span class="collan_dot">:</span>
					 {if $operation neq 'add'}<div style="color:#A9A9A9">{$data->eStatus}</div>{/if}
					<!--<select id="estatus" name="Data[eStatus]">
						<option value="Read" {if $operation neq 'add'} {if $data->eStatus eq Read}selected{/if}{/if}>Read</option>
						<option value="Unread" {if $operation neq 'add'} {if $data->eStatus eq Unread}selected{/if}{/if} >Unread</option>
					</select>-->
				</div>
				<div >
					<a href="{$admin_url}contactus/contactuslist" style=" margin-left: 90px;text-align: center;text-decoration: none;width: 64px;" class="cancel_btn" >Back</a> </div>

					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
<!--{literal}
<script type="text/javascript">
	
	var opts = {     
	formElements:{"dAddedDate":"d-ds-M-ds-Y"}                  
	};
	datePickerController.createDatePicker(opts);
	</script>
{/literal} -->
