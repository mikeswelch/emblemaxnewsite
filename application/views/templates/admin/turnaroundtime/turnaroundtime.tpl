<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}turnaroundtime/turnaroundtimelist">Turn Aound Time</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Turn Aound Time{else}Edit Turn Aound Time{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Tun Around Time
			{else}
			Edit Tun Around Time		
			{/if}
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iTurnAroundTimeId" id="iTurnAroundTimeId" value="{if $operation neq 'add'}{$data->iTurnAroundTimeId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vTitle}{/if}" lang="*" title="Title" />
				</div>
		
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Note</label>
					<span class="collan_dot">:</span>
					<textarea id="tNote" class="inputbox" name="Data[tNote]" title="Note" value="">{if $operation neq 'add'}{$data->tNote}{/if}</textarea>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Charge</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fCharge" name="Data[fCharge]" class="inputbox" value="{if $operation neq 'add'}{$data->fCharge}{/if}" lang="*" title="Charge" onkeypress="return checkprise(event)"/>
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Charge Fee</label>
					<span class="collan_dot">:</span>
					<select id="eType" name="Data[eFeeCharge]" lang="*" title="Charge Fee">
						<option value=''>-- Select Discount Type--</option>
						<option value="%" {if $operation neq 'add'} {if $data->eFeeCharge eq '%'}selected{/if}{/if}>&#37;</option>
						<option value="$" {if $operation neq 'add'} {if $data->eFeeCharge eq '$'}selected{/if}{/if} >&#36;</option>
					</select>
				 </div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrder" name="Data[iOrder]"  title="Order" lang="*" style="width:250px" >
						<option value=''>-- Select Order --</option>
						{for $ord=1 to $Order}
							<option value='{$ord}'{if $operation neq 'add'}{if $ord eq $data->iOrder}selected{/if}{/if} >{$ord}</option>
						{/for}

					</select>
				</div>				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq InActive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Turn Around Time" class="submit_btn" title="Add Turn Around Time" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Turn Around Time" class="submit_btn" title="Edit Turn Around Time" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}turnaroundtime/turnaroundtimelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

<script type="text/javascript">

   function ImageDelete(id,file1){
	
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}color/deleteimage?id={/literal}{$data->iColorId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
{/literal} 
