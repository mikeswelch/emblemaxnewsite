<script type="text/javascript" src="{$TPATH_ADMIN_CKEDITOR_URL}/ckeditor.js"></script>
<script>
//var stateArr = '{literal}{$stateArr}{/literal}';
stateArr = new Array({$stateArr});
</script>
<script language="JavaScript" src="{$tconfig["tsite_javascript"]}jquery.js"></script>
<div id="content">
<div class="container" id="tabs">
	<div class="conthead">
		{if $mode eq add}
		<h2 class="left">Edit Static Page</h2>
		{else}
		<h2 class="left">Edit Static Page</h2>
		{/if}
	</div>
        <div class="contentbox" id="tabs-1">
            
		<form id="frmadd" name="frmadd" action="index.php?file=to-staticpage_a" method="post">
				<input type="hidden" name="iPageId" id="iPageId" value="{$iPageId}" />
				<input type="hidden" name="action" id="action" value="{$mode}" />
				<!--<p>
					<label for="textfield"><strong>Pagecode:</strong></label>
					<select id="vPageCode" name="Data[vPageCode]" lang="*" title="Existing Pagecode" onchange="showdropdownvalue(this.value);">
						<option value=''>--Page Code--</option>
						{section name=i loop=$db_Pagecode}
						<option value='{$db_Pagecode[i].vPageCode}' {if $db_Pagecode[i].vPageCode eq $db_static_pages[0].vPageCode}selected{/if}>{$db_Pagecode[i].vPageCode}</option>
						{/section}
						<option value="0">New Pagecode Album</option>
					</select>
					<input type="text" id="vNewCategory" name="vNewCategory" class="inputbox" title="New Category" style="display:none;"/>

				</p>
				<p id="newcat" style="display:none;">
					<label for="textfield"><strong>New PageCode:</strong></label>
					<div id="newtext"></div>
				</p>-->
                                <div class="inputboxes">
					<label for="textfield"><strong>Pagecode:</strong></label>
					<select id="vPageCode" name="Data[vPageCode]" lang="*" title="PageCode">
						<option value=''>--Page Code--</option>
						{section name=i loop=$db_Pagecode}
						<option value='{$db_Pagecode[i].vPageCode}' {if $db_Pagecode[i].vPageCode eq $db_static_pages[0].vPageCode}selected{/if}>{$db_Pagecode[i].vPageCode}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><strong>DisplayName:</strong></label>
					<input type="text" id="vDisplayName" name="Data[vDisplayName]" class="inputbox" value="{$db_static_pages[0].vDisplayName}" lang="*" title="Display Name"/>
				</div>
                               
                                <div class="inputboxes">
					<label for="textfield"><strong>Meta Title:</strong></label>
					<input type="text" id="tMetaTitle"  name="Data[tMetaTitle]" class="inputbox"  lang="*" title="MetaTitle" value="{$db_static_pages[0].tMetaTitle}"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><strong>Meta Keyword:</strong></label>
					<input type="text" id="tMetaKeyword" name="Data[tMetaKeyword]" class="inputbox" lang="*" title="MetaKeyword" value="{$db_static_pages[0].tMetaKeyword}"/>
				</div>
                                <div class="inputboxes">
					<label for="textfield"><strong>Meta Description:</strong></label>
					<input type="text" id="tMetaDesc"  name="Data[tMetaDesc ]" class="inputbox" lang="*" title="MetaDescription " value="{$db_static_pages[0].tMetaDesc}"/>
				</div>
                                
                                 
                                <div class="inputboxes">
					<label for="textfield"><strong>Content:</strong></label>
				</div>
				<p>
					<textarea id="lContents" name="Data[lContents]" class="text2">{$db_static_pages[0].lContents}</textarea>
				</p>
                                  
				<div class="inputboxes">
					<label for="textfield"  style="width:60px;"><strong>Status:</strong></label>
					<select id="eStatus " name="Data[eStatus ]">
						<option value="Active" {if $db_static_pages[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_static_pages[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</div>
				{if $mode eq add}
				<input type="submit" value="Add Page" class="btn" onclick="return validate(document.frmadd);" title="Add Page"/>
                               {else}
                                <input type="submit" value="Edit Page" class="btn" onclick="return validate(document.frmadd);" title="Edit Page"/>
   				{/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
                                
		        </form>
        </div>
</div>
</div>
{literal}
<script>
function redirectcancel(){
    var admin_url = '{/literal}{$admin_url}{literal}';
    var file = 'to-staticpage';
   // alert(admin_url+"/index.php?file=u-user&mode=view");
    window.location=admin_url+"/index.php?file=to-staticpage&mode=view";
    return false;
}


/*

window.onload = function()
{

var iCountryId = document.getElementById('iCountryId').value;
var mode='{/literal}{$mode}{literal}';

if (mode== "edit")
{
var iStateId = '{/literal}{$db_userinfo[0]["user_state"]}{literal}';
//alert(iStateId);
getRelativeCombo(iCountryId,iStateId,'iStateId','-- Select State --',stateArr);
}
}
*/
</script>
<script type="text/javascript">
	CKEDITOR.replace( 'lContents' );
	
	
</script>
{/literal}
