<div id="content">
<div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		<li><a href="{$admin_url}dashboard">Dashboard</a></li>
		<li>/</li>
		<li class="current">Country</li>
	</ul>
</div>
<div class="container">
    <div class="conthead">
        <h2>Edit General Settings</h2>
    </div>
    
    <div class="contentbox">
    {if $var_msg neq ''}
     <div class="status success" id="errormsgdiv"> 
        	<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
        	<p><img src="{$tconfig.tpanel_img}icons/icon_success.png" title="Success" />
              {$var_msg}</p> 
     </div>     
    <div></div>
    {/if}
        <form  name="frmadd" id="frmadd"  method="post" action="index.php?file=to-generalsettings_a">
    		<input type="hidden" name="action" id="action" value="{$mode}" />
            {section name=i loop=$db_res}
            {if $db_res[i].eType neq $currType}
            {/if}
             <div class="inputboxes">
                <label for="textfield"><strong>{$db_res[i].tDescription}</strong></label>
                <input type="text" id="{$db_res[i].vName}" name="Data[{$db_res[i].vName}]" class="inputbox" value="{$db_res[i].vValue}" lang="*" title="{$db_res[i].tDescription}"/> <br />
	     </div>
            {assign var="currType" value="$db_res[i].eType"}
            {/section}
      	<input type="submit" value="Edit Settings" class="btn" title="Edit Settings" onclick="return validate(document.frmadd);"/>
        </form>
    </div>
</div>
</div>
{literal}
<script>
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}
</script>
{/literal}        