<script>
stateArr = new Array({$stateArr});
</script>
<script type="text/javascript" src="{$TPATH_ADMIN_CKEDITOR_URL}/ckeditor.js"></script>
<div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		<li><a href="{$admin_url}dashboard">Dashboard</a></li>
		<li>/</li>
		<li class="current">Country</li>
	</ul>
</div>
<div id="content">
<div class="container" id="tabs">
	<div class="conthead">
		{if $mode eq add}
		<h2 class="left">Add System Email</h2>
		{else}
		
		<h2 class="left">Edit System Email</h2>
		{/if}
	</div>
	<div class="contentbox" id="tabs-1">
            
		<form id="frmadd" name="frmadd" action="index.php?file=to-systememails_a" method="post">
				<input type="hidden" name="iEmailTemplateId" id="iEmailTemplateId" value="{$iEmailTemplateId}" />
				<input type="hidden" name="action" id="action" value="{$mode}" />
				
				
				<div class="inputboxes">
					<label for="textfield">Email Title:</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vEmailTitle" name="Data[vEmailTitle]" class="inputbox" value="{$db_system_email[0].vEmailTitle}" lang="*" title="Email Title"/>
				</div> 
				
				<div class="inputboxes">
					<label for="textfield">Email Subject</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vEmailSubject" name="Data[vEmailSubject]" class="inputbox" value="{$db_system_email[0].vEmailSubject}" lang="*" title="Email Subject"/>
				</div> 
				<div class="inputboxes">
					<label for="textfield">From Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFromName" name="Data[vFromName]" class="inputbox" value="{$db_system_email[0].vFromName}" lang="*" title="From Name"/>
				</div> 
				<div class="inputboxes">
					<label for="textfield">From Email</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFromEmail" name="Data[vFromEmail]" class="inputbox" value="{$db_system_email[0].vFromEmail}" lang="*" title="From Email"/>
				</div> 
				
				<div class="inputboxes">
					<label for="textfield">Status:</label>
					
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $db_system_email[0].eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive" {if $db_system_email[0].eStatus eq Inactive}selected{/if}>Inactive</option>
					</select>
				</div> 
				<div class="inputboxes">
					<label for="textfield"><strong>Email Message:</strong></label>
				</div>
				<p>
					<textarea id="tEmailMessage" name="tEmailMessage">{$db_system_email[0].tEmailMessage|stripslashes}</textarea>
				</p>
				
                {if $mode eq add}
   				<input type="submit" value="Add System Email" class="btn" onclick="return validate(document.frmadd);" title="Add System Email"/>
   				{else}
   				<input type="submit" value="Edit System Email" class="btn" onclick="return validate(document.frmadd);" title="Edit System Email"/>
   				{/if}
				<input type="button" value="Cancel" class="btnalt" title="Cancel" onclick="redirectcancel();"/>
			</form>
	</div>    
</div>
</div>
{literal}
<script>
function redirectcancel(){
    var admin_url = '{/literal}{$admin_url}{literal}';
    var file = 'u-user';
    window.location=admin_url+"/index.php?file=to-systememails&mode=view";
    return false;
}
</script>
<script type="text/javascript">
	CKEDITOR.replace( 'tEmailMessage' );
</script>

{/literal}
