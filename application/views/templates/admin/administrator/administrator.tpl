{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}administrator/adminlist">Administrator</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Administrator{else}Edit Administrator{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">{if $operation eq 'add'}Add Administrators{else}Edit Administrator{/if}	</div>
		<div class="add_ad_contentbox">
			<form id="frmadd" name="frmadd" method="post" action="{$action}">
				<input type="hidden" name="iAdminId" id="iAdminId" value="{if $operation neq 'add'}{$data->iAdminId}{/if}" />
				<input type="hidden" name="action" id="action" value="add" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> First Name</label>
					<span class="collan_dot">:</span>
					<input type="text" lang="*" title="First Name" value="{if $operation neq 'add'}{$data->vFirstName}{/if}" class="inputbox" name="Data[vFirstName]" id="vFirstName">
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Last Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" value="{if $operation neq 'add'}{$data->vLastName}{/if}" title="Last Name" lang="*"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> E-mail</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vEmail"  name="Data[vEmail]" lang="{literal}*{E}{/literal}" class="inputbox" title="E-mail" value="{if $operation neq 'add'}{$data->vEmail}{/if}"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> User Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vUserName" name="Data[vUserName]" class="inputbox" title="User Name" lang="{literal}*{P}6:0{/literal}" value="{if $operation neq 'add'}{$data->vUsername}{/if}"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vPassword"  name="Data[vPassword]" class="inputbox" title="Password" value="{if $operation neq 'add'}{$data->vPassword}{/if}" lang="{literal}*{P}6:0{/literal}"/>
				</div>
				{if $iAdminId neq $data->iAdminId}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				{/if}
				<div class="add_can_btn"> {if $operation eq 'add'}
					<input type="button" value="Add Administrator" class="submit_btn" title="Add Administrator" onclick="checkUsername('{$admin_url}');"/>
					{else}
					<input type="button" value="Edit Administrator" class="submit_btn" title="Edit Administrator" onclick="checkUsername('{$admin_url}');"/>
					{/if} 
					<a href="{$admin_url}administrator/adminlist" class="cancel_btn">
						Cancel
					</a> 
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}