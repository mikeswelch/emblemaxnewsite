<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
    <div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		<li><a href="{$admin_url}dashboard">Dashboard</a></li>
		<li>/</li>
		<li><a href="{$admin_url}staticpages/staticpageslist">Static Pages</a></li>
		<li>/</li>
		<li class="current">{if $operation eq 'add'}Add Static Page{else}Edit Static Page{/if}</li>
	</ul>
    </div>
    <div class="centerpartbg">
           {if $operation eq 'add'}
            <div class="pagetitle">Add Static Page</div>
            {else}
            <div class="pagetitle">Edit Static Page</div>
            {/if}
            <div class="add_ad_contentbox">
		  	
                    <form id="frmadd" name="frmadd" method="post" action="{$action}">
                            <input type="hidden" name="iSPageId" id="iSPageId" value="{if $operation neq 'add'}{$data->iSPageId}{/if}" />
			    
                            <input type="hidden" name="action" id="action" value="add" />
			    
			   <!-- <div class="inputboxes">
				       	<label for="textfield" style="width:180px;"><span class="red_star">*</span> Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFile" readonly="readonly" name="Data[vFile]" class="inputbox" value="{if $operation neq 'add'}{$data->vFile}{/if}" lang="*" title="Name" style="width:252px"/>
			    </div>-->
			    <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFile" name="Data[vFile]" class="inputbox" value="{if $operation neq 'add'}{$data->vFile}{/if}"  lang="*"  title="Name" />
				</div>
			    
			    <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						<option value="{$initOrder}" >{$initOrder++}</option>
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}
							<option value="{$initOrder}"  {if $data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						{/while}
						{/if}
					
					</select>
				</div>

			    <div id="content">
					    <div class="inputboxes worddocument_input">
			    		<label  for="textarea" style="width:180px;"><span class="red_star"></span> Content</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tContent_{$language_data[0]->vLangCode}" class="content_lang" name="Data[tContent]" class="inputbox" title="Content_en" value="">{if $operation neq 'add'}{$data->tContent}{/if}</textarea></div>
					
			    </div>
{literal}
<script type="text/javascript">
var id = '{/literal}{$language_data[0]->vLangCode}{literal}';	
	CKEDITOR.replace('tContent_'+id);	
	
</script>
{/literal}
			    </div>
			    
			    <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
			    </div>       
                           <div class="clear"></div><br>
			    
			    <div class="add_can_btn">
				    {if $operation eq 'add'}
				    <input type="submit" value="Add Page" class="submit_btn" title="Add Page" onclick="return validate(document.frmadd);"/>
				    {else}
				    <input type="submit" value="Edit Page" class="submit_btn" title="Edit Page" onclick="return validate(document.frmadd);"/>
				    {/if}
				    <a href="{$admin_url}staticpages/staticpageslist" class="cancel_btn">Cancel</a>
			    </div>
                    </form>
                    
            </div>
    </div>
    <div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
