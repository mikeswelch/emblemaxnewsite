{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div class="centerpartbg">
		<div class="quick_link">
			
			<ul>
				<!--
				<li> <a href="#"><img src="{$admin_image_path}icon_lrg_calendar.png" alt="Calendar" /><br />
					Calendar</a> </li>
				-->
				<li> <a href="user/userlist"><img src="{$admin_image_path}admin.png" alt="Admin" /><br />
					Admin</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon_lrg_user.png" alt="Users" /><br />
					Users</a> </li>
				<li> <a href="chapter/chapterlist"><img src="{$admin_image_path}chapter.png" alt="Chapters" /><br />
					Chapters</a> </li>
				<li> <a href="event/eventlist"><img src="{$admin_image_path}event.png" alt="Events" /><br />
					Events</a> </li>
			</ul>
		</div>
				<div id="dashbord_admin">
						<div id="content_admin">
							<!-- Charts box start -->
							<div class="container_admin med left ui-tabs ui-widget ui-widget-content ui-corner-all" id="graphs">
								<div class="conthead">
									<h2>Sales Charts</h2>
								</div>
								<!-- Tabbed navigation start -->
								<div class="contentbox">
									<ul class="tablinks tabfade ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
										<li class="nomar ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#graphs-1">Bar</a></li>
										<li class="ui-state-default ui-corner-top"><a href="#graphs-2">Pie</a></li>
										<li class="ui-state-default ui-corner-top"><a href="#graphs-3">Line</a></li>
										<li class="ui-state-default ui-corner-top"><a href="#graphs-4">Area</a></li>
									</ul>
									<!-- Tabbed navigation end -->
									
									<div class="graph_img"><img src="{$admin_image_path}graph.png"  /></div>
									<!--
									<div class="tabcontent ui-tabs-panel ui-widget-content ui-corner-bottom" id="graphs-1">
										<div style="height: 250px; width: 701px;" class="visualize visualize-bar" role="img" aria-label="Chart representing data from the table:  Registered Members">
											<ul style="width: 701px; height: 250px;" class="visualize-labels-x">
												<li style="left: 0px; width: 116.833px;"><span style="border: medium none;" class="line"></span><span class="label">Jan</span></li>
												<li style="left: 116.833px; width: 116.833px;"><span class="line"></span><span class="label">Feb</span></li>
												<li style="left: 233.667px; width: 116.833px;"><span class="line"></span><span class="label">March</span></li>
												<li style="left: 350.5px; width: 116.833px;"><span class="line"></span><span class="label">April</span></li>
												<li style="left: 467.333px; width: 116.833px;"><span class="line"></span><span class="label">May</span></li>
												<li style="left: 584.167px; width: 116.833px;"><span class="line"></span><span class="label">June</span></li>
											</ul>
											<ul style="width: 701px; height: 250px;" class="visualize-labels-y">
												<li style="bottom: 250px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-top: 0px;">190</span></li>
												<li style="bottom: 218.75px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">168</span></li>
												<li style="bottom: 187.5px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">144</span></li>
												<li style="bottom: 156.25px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">120</span></li>
												<li style="bottom: 125px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">96</span></li>
												<li style="bottom: 93.75px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">72</span></li>
												<li style="bottom: 62.5px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">48</span></li>
												<li style="bottom: 31.25px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">24</span></li>
												<li style="bottom: 0px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-top: -15px;">0</span></li>
											</ul>
											<canvas width="701" height="250"></canvas>
											<div class="visualize-info">
												<div class="visualize-title"> Registered Members</div>
												<ul class="visualize-key">
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(20, 136, 174);"></span><span class="visualize-key-label">2011</span></li>
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(159, 186, 52);"></span><span class="visualize-key-label">2010</span></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="tabcontent ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="graphs-2">
										<div style="height: 250px; width: 701px;" class="visualize visualize-pie" role="img" aria-label="Chart representing data from the table:  Registered Members">
											<canvas width="701" height="250"></canvas>
											<ul class="visualize-labels">
												<li style="left: 400px; top: 175px;" class="visualize-label-pos"><span style="right: 0px; bottom: 0px; font-size: 13.125px; margin-right: -21px; margin-bottom: -8px;" class="visualize-label">75.12%</span></li>
												<li style="left: 302px; top: 75px;" class="visualize-label-pos"><span style="left: 0px; top: 0px; font-size: 13.125px; margin-left: -21px; margin-top: -8px;" class="visualize-label">24.88%</span></li>
											</ul>
											<div class="visualize-info">
												<div class="visualize-title"> Registered Members</div>
												<ul class="visualize-key">
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(20, 136, 174);"></span><span class="visualize-key-label">2011</span></li>
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(159, 186, 52);"></span><span class="visualize-key-label">2010</span></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="tabcontent ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="graphs-3">
										<div style="height: 250px; width: 701px;" class="visualize visualize-line" role="img" aria-label="Chart representing data from the table:  Registered Members">
											<ul style="width: 701px; height: 250px;" class="visualize-labels-x">
												<li style="left: 0px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-left: 0px;">Jan</span></li>
												<li style="left: 140.2px;"><span class="line"></span><span class="label" style="margin-left: -11px;">Feb</span></li>
												<li style="left: 280.4px;"><span class="line"></span><span class="label" style="margin-left: -18px;">March</span></li>
												<li style="left: 420.6px;"><span class="line"></span><span class="label" style="margin-left: -13px;">April</span></li>
												<li style="left: 560.8px;"><span class="line"></span><span class="label" style="margin-left: -12.5px;">May</span></li>
												<li style="left: 701px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-left: -27px;">June</span></li>
											</ul>
											<ul style="width: 701px; height: 250px;" class="visualize-labels-y">
												<li style="bottom: 250px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-top: 0px;">190</span></li>
												<li style="bottom: 218.75px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">168</span></li>
												<li style="bottom: 187.5px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">144</span></li>
												<li style="bottom: 156.25px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">120</span></li>
												<li style="bottom: 125px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">96</span></li>
												<li style="bottom: 93.75px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">72</span></li>
												<li style="bottom: 62.5px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">48</span></li>
												<li style="bottom: 31.25px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">24</span></li>
												<li style="bottom: 0px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-top: -15px;">0</span></li>
											</ul>
											<canvas width="701" height="250"></canvas>
											<div class="visualize-info">
												<div class="visualize-title"> Registered Members</div>
												<ul class="visualize-key">
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(20, 136, 174);"></span><span class="visualize-key-label">2011</span></li>
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(159, 186, 52);"></span><span class="visualize-key-label">2010</span></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="tabcontent ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="graphs-4">
										<div style="height: 250px; width: 701px;" class="visualize visualize-area" role="img" aria-label="Chart representing data from the table:  Registered Members">
											<ul style="width: 701px; height: 250px;" class="visualize-labels-x">
												<li style="left: 0px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-left: 0px;">Jan</span></li>
												<li style="left: 140.2px;"><span class="line"></span><span class="label" style="margin-left: -11px;">Feb</span></li>
												<li style="left: 280.4px;"><span class="line"></span><span class="label" style="margin-left: -18px;">March</span></li>
												<li style="left: 420.6px;"><span class="line"></span><span class="label" style="margin-left: -13px;">April</span></li>
												<li style="left: 560.8px;"><span class="line"></span><span class="label" style="margin-left: -12.5px;">May</span></li>
												<li style="left: 701px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-left: -27px;">June</span></li>
											</ul>
											<ul style="width: 701px; height: 250px;" class="visualize-labels-y">
												<li style="bottom: 250px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-top: 0px;">190</span></li>
												<li style="bottom: 218.75px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">168</span></li>
												<li style="bottom: 187.5px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">144</span></li>
												<li style="bottom: 156.25px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">120</span></li>
												<li style="bottom: 125px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">96</span></li>
												<li style="bottom: 93.75px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">72</span></li>
												<li style="bottom: 62.5px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">48</span></li>
												<li style="bottom: 31.25px;"><span class="line"></span><span class="label" style="margin-top: -7.5px;">24</span></li>
												<li style="bottom: 0px;"><span style="border: medium none;" class="line"></span><span class="label" style="margin-top: -15px;">0</span></li>
											</ul>
											<canvas width="701" height="250"></canvas>
											<div class="visualize-info">
												<div class="visualize-title"> Registered Members</div>
												<ul class="visualize-key">
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(20, 136, 174);"></span><span class="visualize-key-label">2011</span></li>
													<li><span class="visualize-key-color" style="background: none repeat scroll 0% 0% rgb(159, 186, 52);"></span><span class="visualize-key-label">2010</span></li>
												</ul>
											</div>
										</div>
									</div>-->
									
								</div>
							</div>
							<!-- Website stats start -->
							<div class="container_admin sml right">
								<div class="conthead">
									<h2>Website Stats</h2>
								</div>
								<div class="contentbox">
									<ul class="summarystats">
										<li>
											<p class="statcount">{$tot_administator}</p>
											<p>Administator</p>
											<p class="statview"><a href="administrator/adminlist" title="view">view</a></p>
										</li>
										<li>
											<p class="statcount">{$tot_user}</p>
											<p>User</p>
											<p class="statview"><a href="user/userlist" title="view">view</a></p>
										</li>
										<li>
											<p class="statcount">{$tot_chapter}</p>
											<p>Chapter</p>
											<p class="statview"><a href="chapter/chapterlist" title="view">view</a></p>
										</li>
										<li>
											<p class="statcount">{$tot_chapterevent}</p>
											<p>Chapter Event</p>
											<p class="statview"><a href="event/eventlist" title="view">view</a></p>
										</li>
									</ul>
									<p><strong>Last Login Information</strong></p>
									<table>
										<tbody>
											<tr>
												<td>Last Login :</td>
												<td>{$lastlogin->dLoginDate}</td>
											</tr>
											<tr>
												<td>From IP :</td>
												<td>{$lastlogin->vFromIP}</td>
											</tr>
											<tr>
												<td>User Name :</td>
												<td>{$lastlogin->vUserName}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- Website stats end -->
							<!-- Clear finsih for all floated content boxes -->
							<div style="clear: both;"></div>
						</div>
					</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
