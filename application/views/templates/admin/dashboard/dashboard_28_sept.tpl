{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li class="current">Dashboard</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="quick_link">
			<ul>
				<!--
				<li> <a href="#"><img src="{$admin_image_path}icon_lrg_calendar.png" alt="Calendar" /><br />
					Calendar</a> </li>
				-->
				{if $sess_Type == 'Super Admin'}
				<li> <a href="user/userlist"><img src="{$admin_image_path}admin.png" alt="Admin" /><br />
					Admin</a> </li>
				{/if}
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon_lrg_user.png" alt="Users" /><br />
					Users</a> </li>
				{if $sess_Type == 'Super Admin'}
				<li> <a href="chapter/chapterlist"><img src="{$admin_image_path}chapter.png" alt="Chapters" /><br />
					Chapters</a> </li>
				{/if}
				<li> <a href="event/eventlist"><img src="{$admin_image_path}event.png" alt="Events" /><br />
					Events</a> </li>
			</ul>
		</div>
		<div id="dashbord_admin">
			<div id="content_admin">
				<!-- Charts box start -->
				<div class="left_welcome_text">
					<div class="welcome_text">
						{if $login_Type eq 'Super Admin'}
						<div class="conthead">
							<h2>Welcome To Super Admin</h2>
						</div>
						<!-- Tabbed navigation start -->
						<div class="contentbox">
							<p>Welcome to the new BYU Management Society Website! As a chapter administrator you have the ability to update your chapter website, manage events and attendees, set membership dues, and pull reports for your leadership team. We hope you enjoy the new website administration tools and give us critical and constructive feedback so we can make improvements and enhancements in the weeks and months to come!
								
								Additional Video Tutorials and Administrative Support Tools will be available in the coming weeks for chapter administrators and board members to utilize. A FAQ and Help section will also be in place to give you a direct lifeline to find answers to your questions and be in touch with the Global Society for additional requests. 
								
								Feel free to explore the administrative tools, complete the information for your chapter and enjoy!</p>
						</div>
						{/if}
						{if $login_Type eq 'Chapter Admin'}
						<div class="conthead">
							<h2>Welcome To Chapter Admin</h2>
						</div>
						<!-- Tabbed navigation start -->
						<div class="contentbox">
							<p>Welcome to the new BYU Management Society Website! As a chapter administrator you have the ability to update your chapter website, manage events and attendees, set membership dues, and pull reports for your leadership team. We hope you enjoy the new website administration tools and give us critical and constructive feedback so we can make improvements and enhancements in the weeks and months to come!
								
								Additional Video Tutorials and Administrative Support Tools will be available in the coming weeks for chapter administrators and board members to utilize. A FAQ and Help section will also be in place to give you a direct lifeline to find answers to your questions and be in touch with the Global Society for additional requests. 
								
								Feel free to explore the administrative tools, complete the information for your chapter and enjoy!</p>
						</div>
						{/if}
					</div>
					<div class="recent_dashed_box">
							<table cellpadding="0" cellspacing="1" width="100%">
								<tr>
									<th width="25%">Page Name</th>
									<th width="40%">Date</th>
									<th width="35%">Author Name</th>
								</tr>
								{if $revertArr|@count gt 0}
                                {section name=i loop=$revertArr}
                                <tr>
									<td><a href="{$admin_url}staticpages/edit?iSPageId={$revertArr[i]->iSPageId}">{$revertArr[i]->vFile}</a></td>
									<td>{$revertArr[i]->dAddedDate|date_format} @ {$revertArr[i]->dAddedDate|date_format:"%r"}</td>
									<td>{$revertArr[i]->Name}</td>
								</tr>
                                {/section}
                                {else}
                                <tr>
									<td colspan="3" class="nomathfounddashboard">No match founds</td>
								</tr>
                                {/if}
							</table>
					</div>
				</div>
				<div style="width:10px;"></div>
				<!-- Website stats start -->
				<div class="container_admin sml right" style="float:left;">
					<div class="conthead">
						<h2>Website Stats</h2>
					</div>
					<div class="contentbox">
						<ul class="summarystats">
							<li>
								<p class="statcount">{$tot_administator}</p>
								<p>Administator</p>
								<p class="statview"><a href="administrator/adminlist" title="View">View</a></p>
							</li>
							<li>
								<p class="statcount">{$tot_user}</p>
								<p>User</p>
								<p class="statview"><a href="user/userlist" title="View">View</a></p>
							</li>
							<li>
								<p class="statcount">{$tot_chapter}</p>
								<p>Chapter</p>
								<p class="statview"><a href="chapter/chapterlist" title="View">View</a></p>
							</li>
							<li>
								<p class="statcount">{$tot_chapterevent}</p>
								<p>Chapter Event</p>
								<p class="statview"><a href="event/eventlist" title="View">View</a></p>
							</li>
						</ul>
						<p><strong>Last Login Information</strong></p>
						<table>
							<tbody>
								<tr>
									<td>Last Login :</td>
									<td>{$lastlogin->dLoginDate}</td>
								</tr>
								<tr>
									<td>From IP :</td>
									<td>{$lastlogin->vFromIP}</td>
								</tr>
								<tr>
									<td>User Name :</td>
									<td>{$lastlogin->vUserName}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Website stats end -->
				<!-- Clear finsih for all floated content boxes -->
				<div style="clear: both;"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"} 