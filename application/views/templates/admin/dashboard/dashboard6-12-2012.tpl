{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li class="current">Dashboard</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="quick_link">
			<ul>
				<!--
				<li> <a href="#"><img src="{$admin_image_path}icon_lrg_calendar.png" alt="Calendar" /><br />
					Calendar</a> </li>
				-->
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon7.png" alt="Users" /><br />
					Contact us</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon6.png" alt="Users" /><br />
					Messages </a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon5.png" alt="Users" /><br />
					General<br />
					Settings</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon4.png" alt="Users" /><br />
					Onwardz<br />
					Quotes</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon3.png" alt="Users" /><br />
					Manage<br />
					Analysis</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon2.png" alt="Users" /><br />
					Manage<br />
					Freelancers</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon1.png" alt="Users" /><br />
					Manage<br />
					Clients</a> </li>
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon8.png" alt="Users" /><br />
					Users</a> </li>
				<li> <a href="administrator/adminlist"><img src="{$admin_image_path}admin.png" alt="Admin" /><br />
					Admin</a> </li>
				<!--<li> <a href="event/eventlist"><img src="{$admin_image_path}event.png" alt="Events" /><br />
					Events</a> </li> -->
			</ul>
		</div>
		<div id="dashbord_admin">
			<div id="content_admin">
				<!-- Charts box start -->
				<div class="left_welcome_text">
					<div class="welcome_text">
						<div class="conthead">
							<h2>Welcome To Admin</h2>
						</div>
						<!-- Tabbed navigation start -->
						<div class="contentbox">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
								dummy text ever
								since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
								It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
								It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently
								with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</div>
					</div>
					<div class="chart_box">
						<div class="conthead">
							<h2>Revenue of Year 2012</h2>
						</div>
						<div class="table">
							<div id="chart4Div"></div>
						</div>
					</div>
				</div>
				<div style="width:10px;"></div>
				<!-- Website stats start -->
				<div class=" sml left">
					<div class="container_admin">
						<div class="conthead">
							<h2>Website Stats</h2>
						</div>
						<div class="contentbox">
							<ul class="summarystats">
								<li>
									<p class="statcount">{$tot_administator}</p>
									<p>Administator</p>
									<p class="statview"><a href="administrator/adminlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{$user}</p>
									<p>User</p>
									<p class="statview"><a href="user/userlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{$event}</p>
									<p>Event</p>
									<p class="statview"><a href="event/eventlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{$faq}</p>
									<p>Faq</p>
									<p class="statview"><a href="faq/faqlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{$blog}</p>
									<p>Blog</p>
									<p class="statview"><a href="blog/bloglist" title="View">View</a></p>
								</li>
								<li class="last">
									<p class="statcount">{$news}</p>
									<p>News</p>
									<p class="statview"><a href="news/newslist" title="View">View</a></p>
								</li>
							</ul>
						</div>
					</div>
					<div class="container_admin">
						<div class="conthead">
							<h2>Last Login Information</h2>
						</div>
						<div class="contentbox_login">
							<div class="login_his_table">
								<table width="80%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td width="30%">Last Login</td>
											<td width="5%">:</td>
											<td>{$lastlogin->dLoginDate}</td>
										</tr>
										<tr>
											<td>From IP</td>
											<td width="5%">:</td>
											<td>{$lastlogin->vFromIP}</td>
										</tr>
										<tr>
											<td>User Name</td>
											<td width="5%">:</td>
											<td>{$lastlogin->vUserName}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- Website stats end -->
				<!-- Clear finsih for all floated content boxes -->
				<div style="clear: both;"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{literal}
<script type="text/javascript">
				   var chart2 = new FusionCharts("{/literal}{$admin_js_path}{literal}FCF_StackedColumn2D.swf", "ChId1", "450", "350");
				   chart2.setDataURL("{/literal}{$admin_js_path}{literal}ProductSales.xml");
				   chart2.render("chart4Div");
				</script>
{/literal}
{include file="admin/footer.tpl"} 