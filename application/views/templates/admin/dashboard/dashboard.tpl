{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li class="current">Dashboard</li>
		</ul>
	</div>
	<!--pie chart-->
	<div class="centerpartbg">
		<div class="quick_link">
			<ul>
				<!--
				<li> <a href="#"><img src="{$admin_image_path}icon_lrg_calendar.png" alt="Calendar" /><br />
					Calendar</a> </li>
				-->
				<li> <a href="administrator/adminlist"><img src="{$admin_image_path}admin.png" alt="Admin" /><br />
					Admin</a> </li>
				<!--<li> <a href="user/userlist"><img src="{$admin_image_path}icon8.png" alt="Users" /><br />
					Users</a> </li>-->
				<li> <a href="user/userlist"><img src="{$admin_image_path}icon1.png" alt="Users" /><br />
					Manage<br />
					Users</a> </li>				
				<!--<li> <a href="#"><img src="{$admin_image_path}icon3.png" alt="Users" /><br />
					Manage<br />
					Analysis</a> </li>-->				
				<li> <a href="configuration/loadconfiguration"><img src="{$admin_image_path}icon5.png" alt="Users" /><br />
					General<br />
					Settings</a> </li>
				<!--<li> <a href="brief/brieflist"><img src="{$admin_image_path}icon6.png" alt="Messages" /><br />
					Messages </a> </li>-->
				<li> <a href="contactus/contactuslist"><img src="{$admin_image_path}icon7.png" alt="Contact Us" /><br />
					Contact us</a> </li>
				<li> <a href="all_transaction"><img src="{$admin_image_path}trans-icon.png" alt="Transaction" /><br />
					Transaction</a> </li>
				<!--<li> <a href="event/eventlist"><img src="{$admin_image_path}event.png" alt="Events" /><br />
					Events</a> </li> -->
			</ul>
		</div>
		<div id="dashbord_admin">
			<div id="content_admin">
				<!-- Charts box start -->
				<div class="left_welcome_text">
					<div class="chart_box">
						<div class="conthead">
							<h2>Revenue of Year 2012</h2>
						</div>
						<div class="table" align="center">
							<div id="chart4Div"></div>
						</div>
					</div>
					<div class="welcome_text">
						<div class="conthead">
							<h2>Pai Chart</h2>
						</div>
						<!-- Tabbed navigation start -->
						<div class="contentbox">
							<div id="container" class="paichart"></div>
						</div>
					</div>
				</div>
				<!-- Website stats start -->
				<div class=" sml left">
					<div class="container_admin">
						<div class="conthead">
							<h2>Statstics</h2>
						</div>
						<div class="contentbox">
							<ul class="summarystats">
								<li>
									<p class="statcount">{if $tot_administator != ''}{$tot_administator}{else}0{/if}</p>
									<p>Administator</p>
									<p class="statview"><a href="administrator/adminlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{if $users != ''}{$users}{else}0{/if}</p>
									<p>Users</p>
									<p class="statview"><a href="user/userlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{if $products != ''}{$products}{else}0{/if}</p>
									<p>Products</p>
									<p class="statview"><a href="product/productlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{if $category_types != ''}{$category_types}{else}0{/if}</p>
									<p>Category</p>
									<p class="statview"><a href="category/categorylist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{if $faq != ''}{$faq}{else}0{/if}</p>
									<p>Faq</p>
									<p class="statview"><a href="faq/faqlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount">{if $trasaction != ''}{$trasaction}{else}0{/if}</p>
									<p>Transaction</p>
									<p class="statview"><a href="all_transaction" title="View">View</a></p>
								</li>
								<li class="last">
									<p class="statcount">{if $contact_us != ''}{$contact_us}{else}0{/if} </p>
									<p>Contact Us</p>
									<p class="statview"><a href="contactus/contactuslist" title="View">View</a></p>
								</li>
								<!--<li>
									<p class="statcount">{if $help != ''}{$help}{else}0{/if}</p>
									<p>Help</p>
									<p class="statview"><a href="help/helplist" title="View">View</a></p>
								</li>-->
								
							</ul>
						</div>
					</div>
					<div class="container_admin">
						<div class="conthead">
							<h2>Last Login Information</h2>
						</div>
						<div class="contentbox_login">
							<div class="login_his_table">
								<table width="80%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td width="30%">Last Login</td>
											<td width="5%">:</td>
											<td>{$lastlogin->dLoginDate}</td>
										</tr>
										<tr>
											<td>From IP</td>
											<td width="5%">:</td>
											<td>{$lastlogin->vFromIP}</td>
										</tr>
										<tr>
											<td>User Name</td>
											<td width="5%">:</td>
											<td>{$lastlogin->vUserName}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- Website stats end -->
				<!-- Clear finsih for all floated content boxes -->
				<div style="clear: both;"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{literal}
<script type="text/javascript">
	var chart2 = new FusionCharts("{/literal}{$admin_js_path}{literal}FCF_StackedColumn2D.swf", "ChId1", "580", "300");
	chart2.setDataURL("{/literal}{$admin_js_path}{literal}ProductSales.xml");
	chart2.render("chart4Div");
</script>
<script type="text/javascript">
	
	var data_week = '{/literal}{$arr_week}{literal}';
	data_week = eval(data_week);
	
	var data_y = new Array();
	for(var i=0; i<7; i++)
	{
	    data_y[i] = parseInt(data_week[1][i], 10);
	} 
	
	$(document).ready(function () {    
	
        RenderPieChart('container', [
		[data_week[0][0],data_week[1][0]],
		[data_week[0][1],data_week[1][1]],
		[data_week[0][2],data_week[1][2]],                         
		[data_week[0][3],data_week[1][3]],
		[data_week[0][4],data_week[1][4]],
		[data_week[0][5],data_week[1][5]],
		[data_week[0][6],data_week[1][6]]
        ]);     

	/*$('#btnPieChart').live('click', function(){
		var data = [
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]],
			{
			    name: data_week[0][6],
			    y: data_week[0][6],
			    sliced: true,
			    selected: true
			},
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]]
		];
	
	       RenderPieChart('container', data);
	});*/

	function RenderPieChart(elementId, dataList) {
		new Highcharts.Chart({
		chart: {
			renderTo: elementId,
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
			}, title: {
			    text: ''
			},
		tooltip: {
		    formatter: function () {
			return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
		    }
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function () {
				return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
			    }
			}
		    }
		},
		series: [{
		    type: 'pie',
		    name: 'Browser share',
		    data: dataList
		}]
	    });
	};
});
</script>
{/literal}
{include file="admin/footer.tpl"} 