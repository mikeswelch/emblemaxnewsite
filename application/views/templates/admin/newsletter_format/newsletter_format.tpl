{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}newsletter_format/newsletter_formatlist">Newsletter Format</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Newsletter Format{else}Update Newsletter Format{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg"> {if $operation eq 'add'}
		<div class="pagetitle">Add Newsletter Format</div>
		{else}
		<div class="pagetitle">Update Newsletter Format</div>
		{/if}
		<div class="edit_event_contentbox">
			<form id="frmadd" name="frmadd" method="post"  action="{$action}"  enctype="multipart/form-data">
				<input type="hidden" name="iNformatId" id="iNformatId" value="{if $operation neq 'add'}{$data->iNformatId}{/if}" />
				<input type="hidden" name="action" id="action" value="add" />
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>&nbsp; Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vTitle}{/if}" lang="*" title="Title" style="width:252px"/>
				</div>
				<div class="inputboxes worddocument_input">
					<label  for="textarea" ><span class="red_star">*</span>&nbsp; Content</label>
					<span class="collan_dot">:</span> 
					<div class="worddocument"><textarea id="tContent" name="Data[tContent]" class="inputbox" title="Content">{if $operation neq 'add'}{$data->tContent}{/if}</textarea></div>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>&nbsp; Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="clear"></div>
				<div class="add_can_btn">
					{if $operation eq 'add'}
					<input type="submit" value="Add Newsletter Format" class="submit_btn" title="Add Newsletter Format" onclick="return validate(document.frmadd);"/>
					{else}
					<input type="submit" value="Update Newsletter Format" class="submit_btn" title="Update Newsletter Format" onclick="return validate(document.frmadd);"/>
					{/if}
					<a href="{$admin_url}newsletter_format/newsletter_formatlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
				</div>
			</form>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
	CKEDITOR.replace('tContent');
</script>
{/literal}