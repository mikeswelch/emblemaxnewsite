{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li class="current"><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Newsletter Format</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Newsletter Format</div>
		{if $var_msg neq ''}
		 <div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />
			  {$var_msg}</p> 
		 </div>     
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />
			  {$var_msg}</p> 
		</div>
		<div></div>
		{/if}
		<div class="admin_top_part">
			<div class="addnew_btn"> <a href="{$admin_url}newsletter_format/add" style="text-decoration:none;">Add New </a> </div>
			
			<div class="search_top_admini">
				<form name="frmsearch" id="frmsearch" action="{$admin_url}newsletter_format/newsletter_formatlist" method="post">
					<label>Search:</label>
					<div id="newstatus" class="statusleft">
						{if $keyword == 'Active' || $keyword == 'Inactive'}
							<select name="keyword" id="keyword">
								<option value="Active"{if $keyword != ''}{if $keyword eq 'Active'}selected="selected"{/if}{/if}>Active</option>
								<option value="Inactive"{if $keyword != ''}{if $keyword eq 'Inactive'}selected="selected"{/if}{/if}>Inactive</option>
							</select>
						{else}
							<input type="Text" id="keyword" name="keyword" value="{if $keyword != ''}{$keyword}{/if}"  class="search_input" />
						{/if}
					</div>
					
					<select name="option" id="option" onchange="estatusdd(this.value);">
						<option  value='vTitle'{if $option eq 'vTitle'}selected{/if}>Title</option>
						<option  value="dAddedDate"{if $option eq 'dAddedDate'}selected{/if}>Added Date</option>
						<option value="dLastSentDate"{if $option eq 'dLastSentDate'}selected{/if}>Last Sent Date</option>
						<option value="eSendStatus"{if $option eq 'eSendStatus'}selected{/if}>Send Status</option>
						<option value="eStatus"{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
					<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
				</form>
			
			<div class="clear"></div>
			<div class="action_apply_btn">
							<select class="act_sl_box" name="newaction" id="newaction">
								<option value="">Select Action</option>
								<option value="Active">Make Active</option>
								<option value="Inactive">Make Inactive</option>
								<option value="Deletes">Make Delete</option>
								<option value="Show All">Show All</option>
							</select>
							<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'newsletter_formatlist',document.frmlist,'Newsletter Format');"/>
						</div>
			</div>
			<div class="pagination">
				{$AlphaBox}
			</div>
			<div class="clear"></div>
			
		</div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}newsletter_format/search_action" method="post">
			<input type="hidden" name="action" id="action" value="" />
			<input  type="hidden" name="iNformatId" value=""/>
			<table cellpadding="0" cellspacing="1" width="100%">
				<thead>
					<tr>
						<th><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
						<th><a href={$admin_url}newsletter_format/newsletter_formatlist?field=vTitle&order={$order}><span style="color:white;">Title</span>{if $field eq 'vTitle'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
						<th>Send Email</th>
						<th><a href={$admin_url}newsletter_format/newsletter_formatlist?field=dAddedDate&order={$order}><span style="color:white;">Added Date</span>{if $field eq 'dAddedDate'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
						<th><a href={$admin_url}newsletter_format/newsletter_formatlist?field=dLastSentDate&order={$order}><span style="color:white;">Last Sent Date</span>{if $field eq 'dLastSentDate'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
						<th><a href={$admin_url}newsletter_format/newsletter_formatlist?field=eSendStatus&order={$order}><span style="color:white;">Send Status</span>{if $field eq 'eSendStatus'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
						<th><a href={$admin_url}newsletter_format/newsletter_formatlist?field=eStatus&order={$order}><span style="color:white;">Status</span>{if $field eq 'eStatus'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
						<th>Action</th>
					</tr>
				</thead>
				{section name=i loop=$data}
				{if $smarty.section.i.index % 2 eq 0}
				{assign var='class' value='admin_antry_table_sec'}
				{else}
				{assign var='class' value='admin_antry_table'}
				{/if}
				<tbody class="{$class}">
					<tr>
						<td><input name="iNformatId[]" type="checkbox" id="iId" value="{$data[i]->iNformatId}"></td>
						<td><a href="{$admin_url}newsletter_format/edit?iNformatId={$data[i]->iNformatId}">{$data[i]->vTitle}</a></td>
						<td><a href="{$admin_url}newsletter_format/sendemail?iNformatId={$data[i]->iNformatId}">Send Email</a></td>
						<td>{$data[i]->dAddedDate}</td>
						<td>{$data[i]->dLastSentDate}</td>
						<td>{$data[i]->eSendStatus}</td>
						<td>{$data[i]->eStatus}</td>
						<td>
							<a href="{$admin_url}newsletter_format/edit?iNformatId={$data[i]->iNformatId}"><img src="{$admin_image_path}icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iNformatId}','Active');"><img src="{$admin_image_path}icon_approve.png" alt="Active" title="Active"></a>
							<a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iNformatId}','Inactive');"><img src="{$admin_image_path}icon_unapprove.png" alt="Inactive" title="Inactive"></a>
							<a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon('{$data[i]->iNformatId}','newsletter_format/delete','{$ssql}')"></a> </td>
						
					</tr>
				</tbody>
				{/section}
			</table>
			</form>
		</div>
		
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found">{$recmsg}</td>
					<td>
						
					</td>
					<td>
						<div class="bottom_admin_paging">
							{$page_link} 
						</div>
					</td>
				</tr>
			</table>
			
			<div>
				
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
	var alphavalue = val;
	window.location= admin_url+"newsletter_format/newsletter_formatlist?alp="+alphavalue;
	return false;
}
function MakeAction(loopid,type){
	document.frmlist.iNformatId.value = loopid;
	document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function deletenewsletter(a)
{
	
if(confirm("Are you sure you really want to delete this Newsletter Format?"))
{
window.open(admin_url+"/newsletter_format/delete/"+a,'_self',false);
}
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function estatusdd(val1){
	
	var keyword=$('#keyword').val();
	if(val1 == 'dAddedDate')
	{
		$('#newstatus').html('<input name="keyword" type="text" class="search_input" id="dDate" value="" >');
		$('#dDate').Zebra_DatePicker();	
	}
        if(keyword =='Active' || keyword =='Inactive')
	{
		keyword='';
	}
	if(val1 == 'eStatus')
	{
		$('#newstatus').html('<select name="keyword" id="keyword"><option value="Active"{if $keyword != ""}{if $keyword eq "Active"}selected="selected"{/if}{/if}>Active</option><option value="Inactive"{if $keyword != ""}{if $keyword eq "Inactive"}selected="selected"{/if}{/if}>Inactive</option></select>');
	}
	else{
		$('#newstatus').html('<input type="Text" id="keyword" name="keyword" value="'+keyword+'"  class="search_input" />');
	}
}
</script>
{/literal}