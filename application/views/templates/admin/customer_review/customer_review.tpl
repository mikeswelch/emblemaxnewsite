<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}customer_review/customerreviewlist">Customer Reviews</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Review Details{else}Edit Review Details{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			{if $operation eq 'add'}
			Add Review Details
			{else}
			Edit Review Details
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iCustomerReviewId" id="iCustomerReviewId" value="{if $operation neq 'add'}{$data[0]->iCustomerReviewId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" lang="*" class="inputbox" value="{$data[0]->vTitle}" title="Title" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Customer Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vCustomerName" name="Data[vCustomerName]" lang="*" class="inputbox" value="{$data[0]->vCustomerName}" title="Customer Name" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> City</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vCustomerName" name="Data[vCity]" lang="*" class="inputbox" value="{$data[0]->vCity}" title="City" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Country</label>
					<span class="collan_dot">:</span>
					<select class="half" id="iCountryId" name="Data[iCountryId]" lang="*"  title="Country" >
						<option value=''>-- Select Country --</option>
						{section name=i loop=$country}
						<option value='{$country[i]->iCountryId}' {if $operation eq 'edit'}{if $data[0]->iCountryId eq $country[i]->iCountryId}selected{/if}{/if}>{$country[i]->vCountry}</option>
						{/section}
					</select>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Added Date</label>
					<span class="collan_dot">:</span>
					<input type="text" id="dAddedDate123" name="Data[dAddedDate]" lang="*" class="inputbox" value="{$data[0]->dAddedDate}" title="Added Date" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						{if $operation eq 'add'}
							<option value=''>--Select Order--</option>
							{while ($totalRec+1) >= $initOrder}
							<option value="{$initOrder}" >{$initOrder++}</option>
							{/while}
						{else}
						{$initOrder|@print_r}
							<option value=''>--Select Order--</option>
							{while ($totalRec) >= $initOrder}
							<option value="{$initOrder}"  {if $data[0]->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
							{/while}
						{/if}	
					</select>
				</div>
			
				<div class="inputboxes">
					<label for="textfield"><span class="red_star" >*</span>Review</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tDescription" name="Data[tDescription]" class="inputbox" title="Review">{if $operation neq 'add'}{$data[0]->tDescription}{/if}</textarea></div>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data[0]->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data[0]->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Review" class="submit_btn" title="Add Review" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Review" class="submit_btn" title="Edit Review" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}customer_review/customerreviewlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
$('#dAddedDate123').Zebra_DatePicker();	
</script>
{/literal}