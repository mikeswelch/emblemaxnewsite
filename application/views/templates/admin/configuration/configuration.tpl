 {include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Configuration</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Edit Configuration</div>
		<div class="add_ad_contentbox configuration_input">
			<form id="frmadd" name="frmadd" method="post" action="{$admin_url}configuration/edit">
				{section name=i loop=$db_config}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">{if  $db_config[i]->tDescription eq 'Gmail Plus Link' || $db_config[i]->tDescription eq 'FaceBook Link'||$db_config[i]->tDescription eq 'Twitter Link' || $db_config[i]->tDescription eq 'LinkedIn Link' || $db_config[i]->tDescription eq 'Vimeo Link' || $db_config[i]->tDescription eq 'Wordpress Blog Link' || $db_config[i]->tDescription eq 'Flickr Link'}{''}{else}*{/if}</span> {$db_config[i]->tDescription}</label>
					<span class="collan_dot">:</span> {if $db_config[i]->tDescription eq 'Admin Email Id'||$db_config[i]->tDescription eq 'Mail Footer'}
					<input type="text"  id="{$db_config[i]->vName}" name="Data[{$db_config[i]->vName}]" class="inputbox" value="{$db_config[i]->vValue}" lang="{literal}*{E}{/literal}" title="{$db_config[i]->tDescription}"/>
					<br />
					{else if $db_config[i]->tDescription eq 'FaceBook Link'||$db_config[i]->tDescription eq 'Twitter Link' || $db_config[i]->tDescription eq 'LinkedIn Link' || $db_config[i]->tDescription eq 'Vimeo Link' || $db_config[i]->tDescription eq 'Flickr Link' || $db_config[i]->tDescription eq 'Wordpress Blog'}
					<input type="text"  id="{$db_config[i]->vName}" name="Data[{$db_config[i]->vName}]" class="inputbox" value="{$db_config[i]->vValue}"  title="{$db_config[i]->tDescription}"/>
					<br />
					{else if $db_config[i]->tDescription eq 'Date time set'}
					<input type="text"  id="{$db_config[i]->vName}" name="Data[{$db_config[i]->vName}]" class="inputbox" value="{$db_config[i]->vValue}" lang="*" title="{$db_config[i]->tDescription}"/>
					<br />
					<br>
					<div class="dated_conf_txt">[ Date must be enter in   yyyy-mm-dd  format]</div>
					{else}
					<input type="text"  id="{$db_config[i]->vName}" name="Data[{$db_config[i]->vName}]" class="inputbox" value="{$db_config[i]->vValue}" lang="*" title="{$db_config[i]->tDescription}"/>
					<br />
					{/if} </div>
				{/section}
				<div class="add_can_btn">
					<input type="submit" value="Edit Configuration" class="submit_btn" title="Edit Configuration" onclick="return validate(document.frmadd);"/>
					<!--<a href="{$admin_url}configuration/configurationlist"><input type="button" value="Cancel" class="cancel_btn" title="Cancel"/></a>-->
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
$('#DELIVERY_DATE').Zebra_DatePicker();		
</script>
{/literal}