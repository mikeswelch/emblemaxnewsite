<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}faq/faqlist">Faq</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Faq{else}Edit Faq{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Faq
			{else}
			Edit Faq
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iFAQId" id="iFAQId" value="{if $operation neq 'add'}{$data->iFAQId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="preview" id="preview" value="1" />


				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> FAQ Category</label>
					<span class="collan_dot">:</span>
					<select id="iFaqcategoryid" name="Data[iFaqcategoryid]">
						<option value=''>--Select FAQ Category--</option>
						
						{section name=i loop=$faqcategory}
						<option value="{$faqcategory[i]->iFaqcategoryid}"{if $data->iFaqcategoryid eq $faqcategory[i]->iFaqcategoryid}selected{/if}>{$faqcategory[i]->vCategoryName}</option>
						{/section}
					</select>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Question</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vQuestion" name="Data[vQuestion]" class="inputbox" value="{if $operation neq 'add'}{$data->vQuestion}{/if}" lang="*" title="Question" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Answer</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tAnswer" name="Data[tAnswer]" class="inputbox" title="Answer">{if $operation neq 'add'}{$data->tAnswer}{/if}</textarea></div>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						
						<option value="{$initOrder}" >{$initOrder++}</option>
						
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}
							<option value="{$initOrder}"  {if $data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						{/while}
						{/if}
					
					</select>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Faq" class="submit_btn" title="Add Faq" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Faq" class="submit_btn" title="Edit Faq" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}faq/faqlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}

{literal}
<script type="text/javascript">
	CKEDITOR.replace('tAnswer');
</script>
{/literal}