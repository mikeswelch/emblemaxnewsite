
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}language/languagelist">Language</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Language{else}Edit Language{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Language
			{else}
			Edit Language
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iLanguageId" id="iLanguageId" value="{if $operation neq 'add'}{$data->iLanguageId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Language</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLanguage" name="Data[vLanguage]" class="inputbox" value="{if $operation neq 'add'}{$data->vLanguage}{/if}" lang="*" title="Language" style="width:250px"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Language Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLangCode" name="Data[vLangCode]" class="inputbox" maxlength="2" value="{if $operation neq 'add'}{$data->vLangCode}{/if}"  title="Language Code" style="width:250px"/>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="button" value="Add Language" class="submit_btn" title="Add Language" onclick="checkLanguageCode('{$admin_url}');"/>
						{else}
							<input type="button" value="Edit Language" class="submit_btn" title="Edit Language" onclick="checkLanguageCode('{$admin_url}');"/>
						{/if}
						<a href="{$admin_url}language/languagelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
	</script>
{/literal} 
