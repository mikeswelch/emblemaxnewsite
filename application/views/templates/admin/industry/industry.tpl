{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
    <div id="breadcrumb">
	    <ul>
		    <li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		    <li><a href="{$admin_url}dashboard">Dashboard</a></li>
		    <li>/</li>
		    <li><a href="{$admin_url}industry/industrylist">Industry</a></li>
		    <li>/</li>
		    <li class="current">{if $operation eq 'add'}Add Industry{else}Edit Industry{/if}</li>
	    </ul>
    </div>
    <div class="centerpartbg">
           {if $operation eq 'add'}
            <div class="pagetitle">Add Industry</div>
            {else}
            <div class="pagetitle">Edit Industry</div>
            {/if}
            <div class="add_ad_contentbox">
                    <form id="frmadd" name="frmadd" method="post" action="">
                            <input type="hidden" name="iIndustryId" id="iIndustryId" value="{if $operation neq 'add'}{$data->iIndustryId}{/if}" />
                            <input type="hidden" name="action" id="action" value="add" />
                            
                            
                            <div class="inputboxes">
					         <label for="textfield"><span class="red_star">*</span> Industry Name</label>
						    <span class="collan_dot">:</span>
					         <input type="text" id="vIndustryName" name="Data[vIndustryName]" class="inputbox" value="{if $operation neq 'add'}{$data->vIndustryName}{/if}" lang="*" title="Industry" style="width:240px"/>
			    </div>
                          
			   
                            <div class="inputboxes">
                                    <label for="textfield"><span class="red_star"></span> Status</label>
							 <span class="collan_dot">:</span>
                                    <select id="eStatus" name="Data[eStatus]">
                                            <option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
                                            <option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
                                    </select>
                            </div>
                            <div class="add_can_btn">
                                    {if $operation eq 'add'}
                                    <input type="button" value="Add Industry" class="submit_btn" title="Add Industry" onclick="checkIndustry('{$admin_url}');"/>
                                    {else}
                                    <input type="button" value="Edit Industry" class="submit_btn" title="Edit Industry" onclick="checkIndustry('{$admin_url}');"/>
                                    {/if}
                                    <a href="{$admin_url}industry/industrylist" class="cancel_btn">Cancel</a>
                            </div>
                    </form>
            </div>
    </div>
    <div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
