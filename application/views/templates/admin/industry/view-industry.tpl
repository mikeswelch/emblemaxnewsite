{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Industry</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Industry</div>
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />{$var_msg}</p>
		</div>
		<div></div>
		{/if}
		<div class="admin_top_part">
			<div class="addnew_btn"> <a href="{$admin_url}industry/add" style="text-decoration:none;">Add New </a> </div>
			<form name="frmsearch" id="frmsearch" method="post" action="{$admin_url}industry/industrylist">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<div class="search_top_admini">
					<label>Search:</label>
					<div id="newstatus" class="statusleft">
						{if $keyword == 'Active' || $keyword == 'Inactive'}
							<select name="keyword" id="keyword">
								<option value="Active"{if $keyword != ''}{if $keyword eq 'Active'}selected="selected"{/if}{/if}>Active</option>
								<option value="Inactive"{if $keyword != ''}{if $keyword eq 'Inactive'}selected="selected"{/if}{/if}>Inactive</option>
							</select>
						{else}
							<input type="Text" id="keyword" name="keyword" value="{if $keyword != ''}{$keyword}{/if}"  class="search_input" />
						{/if}
					</div>
					<select name="option" id="option" onchange="estatusdd(this.value);">
						<option value="i.vIndustryName"{if $option eq 'i.vIndustryName'}selected{/if}>Industry Name</option>
						<option value="i.eStatus"{if $option eq 'i.eStatus'}selected{/if}>Status</option>
					</select>
					<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
				</div>
			</form>
			<div class="clear"></div>
			<div class="action_apply_btn">
				<select class="act_sl_box" name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active"{if $action == 'Active'}selected="selected"{/if}>Make Active</option>
					<option value="Inactive"{if $action == 'Inactive'}selected="selected"{/if}>Make Inactive</option>
					<option value="Deletes"{if $action == 'Deletes'}selected="selected"{/if}>Make Delete</option>
					<option value="Show All"{if $action == 'Show All'}selected="selected"{/if}>Show All</option>
				</select>
				<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'industrylist',document.frmlist,'industry');"/>
			</div>
			<div class="pagination"> {$AlphaBox} </div>
			<div class="clear"></div>
		</div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}industry/search_action" method="post">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="commonId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><a href="{$admin_url}industry/industrylist?field=vIndustryName"><span style="color:white;">Industry</span>{if $field eq 'vIndustryName'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th><a href="{$admin_url}industry/industrylist?field=count"><span style="color:white;">Count Of Freelancer</span>{if $field eq 'count'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="60px"><a href="{$admin_url}industry/industrylist?field=estatus"><span style="color:white;">Status</span> {if $field eq 'estatus'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="90px">Action</th>
						</tr>
					</thead>
					{if $data|@count gt 0}
					{section name=i loop=$data}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}">
						<tr>
							<td><input name="iIndustryId[]" type="checkbox" id="iId" value="{$data[i]->iIndustryId}"></td>
							<td><a href="{$admin_url}industry/edit?iIndustryId={$data[i]->iIndustryId}">{$data[i]->vIndustryName}</a></td>
							<td><a href="{$admin_url}freelancer/freelancerlist?option=iIndustryId&keyword={$data[i]->iIndustryId}&keyword1={$data[i]->vIndustryName}">{$data[i]->count}</a></td>
							<td>{$data[i]->eStatus}</td>
							<td><a href="{$admin_url}industry/edit?iIndustryId={$data[i]->iIndustryId}"><img src="{$admin_image_path}icon_edit.png" alt="Edit" title="Edit"></a> <a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iIndustryId}','Active','iIndustryId');"><img src="{$admin_image_path}icon_approve.png" alt="Active" title="Active"></a> <a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iIndustryId}','Inactive','iIndustryId');"><img src="{$admin_image_path}icon_unapprove.png" alt="Inactive" title="Inactive"></a> <a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon({$data[i]->iIndustryId},'industry/delete','{$ssql}')"></a> </td>
						</tr>
					</tbody>
					{/section}
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					{/if}
				</table>
			</form>
		</div>
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found" width="33%" align="left">{$recmsg}</td>
					<td width="33%"><div class="bottom_admin_paging"> {$page_link} </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script >
function estatusdd(val1){	
	var keyword=$('#keyword').val();
	if(keyword =='Active' || keyword =='Inactive')
	{
		keyword='';
	}
	if(val1 == 'i.eStatus')
	{
		$('#newstatus').html('<select name="keyword" id="keyword"><option value="Active"{if $keyword != ""}{if $keyword eq "Active"}selected="selected"{/if}{/if}>Active</option><option value="Inactive"{if $keyword != ""}{if $keyword eq "Inactive"}selected="selected"{/if}{/if}>Inactive</option></select>');
	}
	else{
		$('#newstatus').html('<input type="Text" id="keyword" name="keyword" value="'+keyword+'"  class="search_input" />');
	}
}
</script>
{/literal}