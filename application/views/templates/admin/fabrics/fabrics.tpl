<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}fabrics/fabricslist">Fabrics</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Fabrics{else}Edit Fabrics{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Fabrics
			{else}
			Edit Fabrics
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iFabricsId" id="iFabricsId" value="{if $operation neq 'add'}{$data->iFabricsId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Fabrics Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vTitle}{/if}" lang="*" title="Fabrics Name" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						
						<option value="{$initOrder}" >{$initOrder++}</option>
						
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}
							
						<option value="{$initOrder}"  {if $data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						
							{/while}
						{/if}
					
					</select>
				</div>
			
				<div class="inputboxes">
					<label for="textfield"><span class="red_star" ></span>Description</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tDescription" name="Data[tDescription]" class="inputbox" title="Description">{if $operation neq 'add'}{$data->tDescription}{/if}</textarea></div>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="InActive" {if $operation neq 'add'} {if $data->eStatus eq InActive}selected{/if}{/if} >InActive</option>
					</select>
				</div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Fabrics" class="submit_btn" title="Add Fabrics" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Fabrics" class="submit_btn" title="Edit Fabrics" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}fabrics/fabricslist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
