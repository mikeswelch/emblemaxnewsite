<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}size/sizelist">Size</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Size{else}Edit Size{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Size
			{else}
                        Edit Size
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iSizeId" id="iSizeId" value="{if $operation neq 'add'}{$data->iSizeId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Size</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vSize" name="Data[vSize]" class="inputbox" value="{if $operation neq 'add'}{$data->vSize}{/if}" lang="*" title="Size" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vTitle}{/if}" lang="*" title="Size Title" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrder" name="Data[iOrder]"  title="Order" lang="*" style="width:250px" >
						<option value=''>-- Select Order --</option>
						{for $ord=1 to $Order}
							<option value='{$ord}'{if $operation neq 'add'}{if $ord eq $data->iOrder}selected{/if}{/if} >{$ord}</option>
						{/for}

					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Size" class="submit_btn" title="Add Size" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Size" class="submit_btn" title="Edit Size" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}size/sizelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

{/literal} 
