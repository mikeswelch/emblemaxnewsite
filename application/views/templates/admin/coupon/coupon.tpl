<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}garment/garmentlist">Coupon</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Coupon{else}Edit Coupon{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Coupon
			{else}
                        Edit Coupon
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iCouponId" id="iCouponId" value="{if $operation neq 'add'}{$data->iCouponId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Coupon Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vCouponCode" name="Data[vCouponCode]" class="inputbox" value="{if $operation neq 'add'}{$data->vCouponCode}{/if}" lang="*" title="Coupon Code" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Discount</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fDiscount" name="Data[fDiscount]" class="inputbox" value="{if $operation neq 'add'}{$data->fDiscount}{/if}" lang="*" title="Dicsount" onkeypress="return checkprise(event)"/>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Validity Type</label>
					<span class="collan_dot">:</span>
					<select id="eValidityType" name="Data[eValidityType]" lang="*" title="Validity Type">
						<option value=''>-- Select Validity Type--</option>
						<option value="Permanent" {if $operation neq 'add'} {if $data->eValidityType eq Permanent}selected{/if}{/if}>Permanent</option>
						<option value="Defined" {if $operation neq 'add'} {if $data->eValidityType eq Defined}selected{/if}{/if} >Defined</option>
					</select>
				 </div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Discount Type</label>
					<span class="collan_dot">:</span>
					<select id="eType" name="Data[eType]" lang="*" title="Dicount Type">
						<option value=''>-- Select Discount Type--</option>
						<option value="%" {if $operation neq 'add'} {if $data->eType eq '%'}selected{/if}{/if}>&#37;</option>
						<option value="�" {if $operation neq 'add'} {if $data->eType eq '�'}selected{/if}{/if} >&#163;</option>
						<option value="$" {if $operation neq 'add'} {if $data->eType eq '$'}selected{/if}{/if} >&#36;</option>
					</select>
				 </div>
				
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Start Date</label>
					<span class="collan_dot">:</span>
					<input type="text" id="dActiveDate" name="Data[dActiveDate]" class="inputbox" value="{if $operation neq 'add'}{$data->dActiveDate}{/if}"  title="Start Date" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> End Date</label>
					<span class="collan_dot">:</span>
					<input type="text" id="dExpiryDate" name="Data[dExpiryDate]" class="inputbox" value="{if $operation neq 'add'}{$data->dExpiryDate}{/if}"  title="End Date" />
				</div>
				
				<div class="inputboxes">
				        <label for="textarea"><span class="red_star"></span> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tDescriptions" class="inputbox" title="Description" name="Data[tDescriptions]" id="tDescriptions">{if $operation neq 'add'}{$data->tDescriptions}{/if}</textarea>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Coupon" class="submit_btn" title="Add Coupon" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Coupon" class="submit_btn" title="Edit Coupon" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}coupon/couponlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
$('#dActiveDate').Zebra_DatePicker();		
$('#dExpiryDate').Zebra_DatePicker();
</script>
{/literal}
