<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="{$admin_css_path}style.css" rel="stylesheet" type="text/css" />

<link href="{$admin_js_path}datepicker/css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$admin_js_path}datepicker/js/datepicker.js"></script>
<script type="text/javascript" src="{$admin_js_path}jquery.min.js"></script>
<script type="text/javascript" src="{$admin_js_path}functions.js"></script>
<script type="text/javascript" src="{$admin_js_path}validate.js"></script>
<script type="text/javascript" src="{$admin_js_path}jlist.js"></script>

<!--Chart -->
<script type="text/javascript" src="{$admin_js_path}FusionCharts.js"></script>
<script src="{$admin_js_path}highcharts.js"></script>
<script src="{$admin_js_path}exporting.js"></script>
<!-- Gallary -->
<script type="text/javascript" src="{$admin_js_path}gallary/js/jcarousellite_0.4.0.pack.js"></script>

<!-- Define Common function  12/1/2012-->
<script type="text/javascript" src="{$admin_js_path}common.js"></script>
<!-- Define Custom validataion function  12/1/2012-->
<script type="text/javascript" src="{$admin_js_path}admin_custom_validation.js"></script>
<!-- Fancybox-->
<script type="text/javascript" src="{$fancybox_path}lib/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="{$fancybox_path}source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="{$fancybox_path}source/jquery.fancybox.css?v=2.1.0" media="screen" />
<script type="text/javascript" src="{$admin_js_path}jquery.form.js"></script>

<script type="text/javascript">
    var admin_url ='{$admin_url}';
</script>


</head>
<body>
<div class="sitecontainer">
	<div class="header">
		<div class="adminmsg">
            Welcome {$smarty.session.sess_Name}<br />            
        </div>
        <div class="userpanel"><a href="{$admin_url}authentication/logout">Logout</a>&nbsp;&nbsp;</div>
		<div class="logo"><img src="{$admin_image_path}logo.png" alt="" title="" /></div>
	</div>
	<div class="container">
		<div class="containerBg">