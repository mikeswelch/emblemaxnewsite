{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Report</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Analysis</div>
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />{$var_msg}</p>
		</div>
		<div></div>
		{/if}
		<div class="admin_top_part">
			<form name="frmsearchdate" id="frmsearchdate" method="post" action="{$admin_url}report/reportlist">
				<div class="chapter_ad_date">
					<div class="inputboxes inputboxesst_date">
						<label for="textfield">Start Date</label>
						<span class="collan_dot">:</span>
						<input type="text" readonly="readonly" id="start_date" name="start_date" class="inputbox" value="" title="Start Date" lang="*"/>
					</div>
					<div class="inputboxes inputboxesst_date">
						<label for="textfield">End Date</label>
						<span class="collan_dot">:</span>
						<input type="text" readonly="readonly" id="end_date" name="end_date" class="inputbox" value="" title="End Date" lang="*"/>
					</div>
					<select name="option" id="option" style="width:200px">
						<option value="">--Select--</option>
						<option value="Paid" {if {$data[0]->estatus} eq 'Paid'}Selected{/if}>Paid</option>
						<option value="Cancel" {if {$data[0]->estatus} eq 'Cancel'}Selected{/if}>Cancel</option>
						<option value="Decliend" {if {$data[0]->estatus} eq 'Decliend'}Selected{/if}>Decliend</option>
						<option value="Inprocess" {if {$data[0]->estatus} eq 'Inprocess'}Selected{/if}>Inprocess</option>
						<option value="Refund" {if {$data[0]->estatus} eq 'Refund'}Selected{/if}>Refund</option>

					</select>
					<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchbetween();"/>
				        </div>
			</form>
		</div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}event/search_action" method="post">
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="event_id" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><span style="color:white;">Invoice Number</span></th>
							<th><span style="color:white;">SubTotal</span></th>
							<th><span style="color:white;">Vat Price</span></th>
							<th><span style="color:white;">Grand Total</span></th>
							<th><span style="color:white;">Date</span></th>
							<th width="60px"><span style="color:white;">Status</span></th>
							<th width="90px"><span style="color:white;">Action</span></th>
						</tr>
					</thead>
					{if $data|@count gt 0}
					{section name=i loop=$data}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}">
						<tr>
							<td><input name="iTransactionId[]" type="checkbox" id="iId" value="{$data[i]->iTransactionId}"></td>
							<td><a href="#">{$data[i]->vInvoiceNumber}</a></td>
							<td>{$data[i]->fSubTotal}</td>
							<td>{$data[i]->fVatPrice}</td>
							<td>{$data[i]->fGrandTotal}</td>
							<td>{$data[i]->dTransactionDate}</a></td>
							<td>{$data[i]->eStatus}</td>
							<td><a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon_tab({$data[i]->iTransactionId},{$iBusinessUserId},'transaction/delete')"></a></td>
							
						</tr>
					</tbody>
					{/section}
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					{/if}
				</table>
			</form>
		</div>
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found">{$recmsg}</td>
					<td><div class="bottom_admin_paging"> {$page_link} </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}

{literal}
<script>
var opts1 = {formElements:{"start_date":"Y-ds-m-ds-d"}};
datePickerController.createDatePicker(opts1);
var opts2 = {formElements:{"end_date":"Y-ds-m-ds-d"}};
datePickerController.createDatePicker(opts2);

function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function Searchbetween(){
    document.getElementById('frmsearchdate').submit();   
}
</script>
{/literal} 