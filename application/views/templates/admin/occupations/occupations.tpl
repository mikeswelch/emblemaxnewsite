{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}occupations/occupationslist"> Occupations</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Occupation{else}Edit Occupation{/if}</li>
		</ul>
	</div>

	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg"> {if $operation eq 'add'}
		<div class="pagetitle">Add Occupation</div>
		{else}
		<div class="pagetitle">Edit Occupation</div>
		{/if}
		
		<div class="add_ad_contentbox">
			<input type="hidden" name="Data[iOccupationsId]" id="iOccupationsId" value="{if $operation neq 'add'}{$data->iOccupationsId}{/if}" />
			<input type="hidden" name="action" id="action" value="add" />
						
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star">*</span>Occupation Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" lang="*" title="Occupation Name" value="{if $operation neq 'add'}{$data->vTitle}{/if}" maxlength="30" class="inputbox" name="Data[vTitle]" >
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						
						<option value="{$initOrder}" >{$initOrder++}</option>
						
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}
							
						<option value="{$initOrder}"  {if $data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						
							{/while}
						{/if}
					
					</select>
				</div>
			
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star" ></span>Description</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tDescription" name="Data[tDescription]" class="inputbox" title="Description">{if $operation neq 'add'}{$data->tDescription}{/if}</textarea></div>
				</div>				
			
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="InActive" {if $operation neq 'add'} {if $data->eStatus eq InActive}selected{/if}{/if} >InActive</option>
					</select>
				</div>
				<div class="add_can_btn"> {if $operation eq 'add'}
					<input type="submit" value="Add Occupation" class="submit_btn" title="Add Occupation" onclick="return validate(document.frmadd);"/>
					{else}
					<input type="submit" value="Edit Occupation" class="submit_btn" title="Edit Occupation" onclick="return validate(document.frmadd);"/>
					{/if}
					<a href="{$admin_url}occupations/occupationslist" class="cancel_btn">Cancel </a>
				</div>
				<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
	</form>
</div>
{include file="admin/footer.tpl"}

{literal}
<script type="text/javascript">

function checkparent(id){

	if(id == '0'){
		$('#menuboxid').css({display: "block"});
		$('#submenuboxid').css({display: "none"});
		
		$('#iconbox').css({display: "block"});
		$('#urlbox').css({display: "none"});
	}else{
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
var operation = '{/literal}{$operation}{literal}';

if( operation == 'edit'){
	var iParentMenuId = '{/literal}{$data->iDesignParentCategoryId}{literal}';
	if( iParentMenuId != 0){
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
</script>
{/literal}
