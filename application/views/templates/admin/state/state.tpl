<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}state/statelist">State</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  State{else}Edit State{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add State
			{else}
			Edit State
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iStateId" id="iStateId" value="{if $operation neq 'add'}{$data->iStateId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					       <label for="textfield"><span class="red_star">*</span> Country</label>
						  <span class="collan_dot">:</span>
					       <select id="iCountryId" name="Data[iCountryId]" title="Country" lang="*" >
						     <option value=''>--Select Country--</option>
						    {section name=i loop=$db_country}
						     <option value='{$db_country[i]->iCountryId}'{if $operation neq 'add'}{if $data->iCountryId eq $db_country[i]->iCountryId}selected{/if}{/if}>{$db_country[i]->vCountry}</option>
						    {/section}
					      </select>
                                </div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> State</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vState" name="Data[vState]" class="inputbox" value="{if $operation neq 'add'}{$data->vState}{/if}" lang="*" title="State" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> State Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vstatecode" name="Data[vstatecode]" maxlength="2" class="inputbox" value="{if $operation neq 'add'}{$data->vstatecode}{/if}" lang="*" title="State Code" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="button" value="Add State" class="submit_btn" title="Add State" onclick="checkStateCode('{$admin_url}');"/>
						{else}
							<input type="button" value="Edit State" class="submit_btn" title="Edit State" onclick="checkStateCode('{$admin_url}');"/>
						{/if}
						<a href="{$admin_url}state/statelist" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
	
	</script>
{/literal} 
