<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}banner/bannerlist">Banner</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Banner{else}Edit Banner{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Banner
			{else}
			Edit Banner
			{/if}
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBannerId" id="iBannerId" value="{if $operation neq 'add'}{$data->iBannerId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />

			        <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Banner Group Name</label>
					 <span class="collan_dot">:</span>
					
					<select id="iBannerGroupId" name="Data[iBannerGroupId]" lang="*" title="Banner Group Name" >
						<option value=''>--Select Banner Group--</option>
						{section name=i loop=$bannergroup}
						<option value='{$bannergroup[i]['iBannerGroupId']}' {if $operation neq 'add'}{if $data->iBannerGroupId eq $bannergroup[i]['iBannerGroupId']}selected{/if}{/if}>{$bannergroup[i]['vTitle']}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Banner Url </label>
					<span class="collan_dot">:</span>
					<input type="text" id="vUrl" name="Data[vUrl]" class="inputbox" value="{if $operation neq 'add'}{$data->vUrl}{/if}"  title="URL" />
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					
					{else}
					<input type="file" id="vImage"  name="vImage" title="Image" value="{if $operation neq 'add'}{$data->vImage}{/if}"{if $data->vImage eq ''} lang="*" {/if} onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
					
				</div>
				{if $operation neq 'add'}{if $data->vImage neq ''}
				<div class="view_del_user">						
					<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg">View</a></div>
					<div class="delete_user"><a href="#" onclick="ImageDelete('{$data->iBannerId}','iBannerId');" class="delete_btnimg">Delete</a></div>
						<div style="display:none;">
							<div id="popfancy"><img src="{$upload_path}/banner/{$data->iBannerId}/195x200_{$data->vImage}"></div>
						</div>
				</div>
				<!--<div class="msg_alert1">
					Recommended Size : 341 X 418
				</div>-->
				{/if}{/if}

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						
						<option value="{$initOrder}" >{$initOrder++}</option>
						
						{/while}
						{else}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}
								
						<option value="{$initOrder}"  {if $data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						
							{/while}
						{/if}
					
					</select>
				</div> 		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Banner" class="submit_btn" title="Add Banner" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Banner" class="submit_btn" title="Edit Banner" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}banner/bannerlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

<script type="text/javascript">

$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});


   function ImageDelete(id,file1){
	
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}banner/deleteimage?id={/literal}{$data->iBannerId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
{/literal} 
