<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}banner/bannerlist">Banner</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Banner{else}Edit Banner{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Banner
			{else}
			<ul>
				<li class="active"><a href="#">Edit Banner</a></li>
				
			</ul>
			
			
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBannerId" id="iBannerId" value="{if $operation neq 'add'}{$data->iBannerId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Banner Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vTitle}{/if}" lang="*" title="Banner"/>
				</div>				
                                 <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Page</label>
					 <span class="collan_dot">:</span>
					<select id="iPageId" name="Data[iPageId]" title="Static Page" lang="*" >
					    <option value=''>--Select Page--</option>
					   {section name=i loop=$db_staticpages}
					    <option value='{$db_staticpages[i]->iSPageId}'{if $operation neq 'add'}{if $data->iPageId eq $db_staticpages[i]->iSPageId}selected{/if}{/if}>{$db_staticpages[i]->vpagename}</option>
					   {/section}
				     </select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Position</label>
					<span class="collan_dot">:</span>
					<select id="ePosition" name="Data[ePosition]" title="Position" lang="*">
						<option value=''>--Select Position--</option>
						<option value="Home" {if $operation neq 'add'} {if $data->ePosition eq Home}selected{/if}{/if}>Home</option>
						<option value="Left" {if $operation neq 'add'} {if $data->ePosition eq Left}selected{/if}{/if} >Left</option>
					</select>
				 </div>
				<div class="inputboxes" >
                                        <label  for="textarea"><span class="red_star">*</span> Description</label>
					<span class="collan_dot">:</span>
                                        <textarea id="vText"  name="Data[vText]" class="inputbox" title="Description" value="" lang="*">{if $operation neq 'add'}{$data->vText}{/if}</textarea>
                                </div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Banner" class="submit_btn" title="Add Banner" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Banner" class="submit_btn" title="Edit Banner" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}banner/bannerlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

{/literal} 
