<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ideas that promote</title>
<link href="{$admin_css_path}style.css" rel="stylesheet" type="text/css" />
<link href="{$admin_css_path}boxes.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="{$admin_js_path}jquery.min.js"></script>
<script type="text/javascript" src="{$admin_js_path}functions.js"></script>
<script type="text/javascript" src="{$admin_js_path}validate.js"></script>
<script type="text/javascript" src="{$admin_js_path}jlist.js"></script>


<!--Chart -->
<script type="text/javascript" src="{$admin_js_path}FusionCharts.js"></script>
<script src="{$admin_js_path}highcharts.js"></script>
<script src="{$admin_js_path}exporting.js"></script>

<script type="text/javascript" src="{$admin_js_path}common.js"></script>
<script type="text/javascript" src="{$admin_js_path}admin_custom_validation.js"></script>

<script type="text/javascript" src="{$fancybox_path}lib/jquery-1.8.0.min.js"></script>
<script  type="text/javascript" src="{$admin_js_path}bootstrap-modal.js"></script>
<script type="text/javascript" src="{$fancybox_path}source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="{$fancybox_path}source/jquery.fancybox.css?v=2.1.0" media="screen" />
<script type="text/javascript" src="{$admin_js_path}jquery.form.js"></script>

<link rel="stylesheet" href="{$admin_js_path}zebra-datepicker/css/zebra_datepicker.css" type="text/css">
<script type="text/javascript" src="{$admin_js_path}zebra-datepicker/javascript/zebra_datepicker.js"></script>

<script type="text/javascript">
    var admin_url ='{$admin_url}';
    var admin_image_path='{$admin_image_path}';
</script>


</head>
<body>
<div class="sitecontainer">
	<div class="header">
		<div class="adminmsg">
            <label style="color:#9E053B">Welcome {$smarty.session.sess_Name}</label><br />            
        </div>
        <div class="userpanel"><a href="{$admin_url}authentication/logout">Logout</a>&nbsp;&nbsp;</div>
		<div class="logo"><img src="{$admin_image_path}logo.png" alt="" title="" /></div>
		<!--<div class="logo"><h1 style="color: #FFFFFF">Emblemax</h1></div>-->
	</div>
	<div class="container">
		<div class="containerBg">