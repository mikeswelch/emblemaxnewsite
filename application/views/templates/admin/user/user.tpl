<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}user/userlist">User</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  User{else}Edit User{/if}</li>
		</ul>
	</div>
	<div style="clear:both:"></div>
	<div class="centerpartbg">
		
			
			{if $operation eq 'add'}
			<div class="pagetitle">Add User</div>
			{else}<div class="pagetitle_tab">
			<ul>
				<li class="active"><a href="#">Edit User</a></li>
				<!--<li><a href="{$admin_url}projectbrief/projectbrieflist?iUserId={$data->iUserId}">Project Brief</a></li>-->
				<li><a href="{$admin_url}transaction/transactionlist?iUserId={$data->iUserId}">Transaction</a></li>
				<li><a href="{$admin_url}changepassword/passworduser?iUserId={$data->iUserId}">Change Password</a></li>
				
			</ul>
			
			</div>
			{/if} 
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iUserId" id="iUserId" value="{if $operation neq 'add'}{$data->iUserId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> First Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFirstName" name="Data[vFirstName]" class="inputbox" value="{if $operation neq 'add'}{$data->vFirstName}{/if}" lang="*" title="First Name" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Last Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" title="Last Name"  lang="*" value="{if $operation neq 'add'}{$data->vLastName}{/if}" >
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> User Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vUserName" name="Data[vUserName]" class="inputbox" value="{if $operation neq 'add'}{$data->vUserName}{/if}" lang="*" title="User Name" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Image</label>
					<span class="collan_dot">:</span>
					{if $operation eq 'add'}
					<input type="file" id="vPhoto"  name="vPhoto" title="Image" value="{if $operation neq 'add'}{$data->vPhoto}{/if}" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					{else}
					<input type="file" id="vPhoto"  name="vPhoto" title="Image" value="{if $operation neq 'add'}{$data->vPhoto}{/if}"{if $data->vPhoto eq ''} lang="*" {/if} onchange="CheckValidFile(this.value,this.name)"/>
					{/if}
				</div>
				{if $operation neq 'add'}{if $data->vPhoto neq ''}
					<div class="view_del_user">
						<!--<div class="view_btn_user"><a href="#popfancy" id="fancyhref"><img src="{$admin_image_path}view.png"/></a></div>
						<div class="delete_user"><img src="{$admin_image_path}delete.png" onclick="ImageDelete({$data->iUserId});"/></div>-->
						
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('{$data->iUserId}','user');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="popfancy"><img src="{$upload_path}/user/{$data->iUserId}/200x200_{$data->vPhoto}"></div>
						</div>
				</div>
					{/if}{/if}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Email</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vEmail"  name="Data[vEmail]" lang="{literal}*{E}{/literal}" class="inputbox" title="Email" value="{if $operation neq 'add'}{$data->vEmail}{/if}"/>
				</div>
				{if $operation eq 'add'}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vPassword" lang="*" title="Password" value="{if $operation neq 'add'}{$data->vPassword}{/if}" maxlength="10" class="inputbox" name="Data[vPassword]" >
				</div>
				{/if}
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Contact Number</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vPhone" name="Data[vPhone]" lang="*" class="inputbox" title="Contact Number"  value="{if $operation neq 'add'}{$data->vPhone}{/if}" maxlength="15" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Country</label>
					<span class="collan_dot">:</span>
					<select id="iCountryId" name="Data[iCountryId]"  title="Country" lang="*" style="width:250px" onchange="getStates(this.value);">
						<option value=''>-- Select Country --</option>
						
						{section name=i loop=$db_country}
						
							<option value='{$db_country[i]->iCountryId}'{if $operation neq 'add'}{if $data->iCountryId eq $db_country[i]->iCountryId}selected{/if}{/if} >{$db_country[i]->vCountry}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> State</label>
					<span class="collan_dot">:</span>
					<span id="statelist">
						<select id="iStateId" name="Data[iStateId]" title="State" style="width:250px">
							<option value=''>--Select State--</option>	
							{section name=i loop=$db_state}					    
							<option value='{$db_state[i]->iStateId}'{if $operation neq 'add'} {if $data->iStateId eq $db_state[i]->iStateId}selected{/if}{/if}>{$db_state[i]->vState}
							</option>
					    {/section}
						</select>
					</span>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Zipcode</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vZipCode" name="Data[vZipCode]" class="inputbox" title="Zipcode"  lang="*" value="{if $operation neq 'add'}{$data->vZipCode}{/if}" onkeypress="return checkprise(event)">
				</div>								
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Gender</label>
					<span class="collan_dot">:</span>
					<select id="eGender" name="Data[eGender]">
						<option value="Male" {if $operation neq 'add'} {if $data->eGender eq Male}selected{/if}{/if}>Male</option>
						<option value="Female" {if $operation neq 'add'} {if $data->eGender eq Female}selected{/if}{/if} >Female</option>
					</select>
				 </div>
				<div class="inputboxes">
				        <label for="textarea"> Address</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tAddress" class="inputbox" title="Address" name="Data[tAddress]" id="tIntroduction">{if $operation neq 'add'}{$data->tAddress}{/if}</textarea>
				</div>
				<!--<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Activation Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vActivationcode" name="Data[vActivationcode]" class="inputbox" title="Activation Code"  lang="*" value="{if $operation neq 'add'}{$data->vActivationcode}{/if}" >
				</div>-->
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add User" class="submit_btn" title="Add User" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit User" class="submit_btn" title="Edit User" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}user/userlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
         
function getStates(icountryid)
{
    
    var adm_url = admin_url + 'site/ajax_call';
    $.ajax({   
	url: adm_url,
	async: false,
	type: "POST",
	data: "icountryid="+icountryid,
	dataType: "html",
	success: function(data) {
	   $('#statelist').html(data);
	}
    });
}
	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});



function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="{/literal}{$admin_url}{literal}user/deleteimage?id={/literal}{$data->iUserId}{literal}">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	
	
</script>
{/literal} 
