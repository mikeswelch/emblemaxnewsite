{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}user/userlist">User</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Freelancer{else}Change Password{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle_tab">
			{if $operation eq 'add'}

			{else}
			<ul>
				<li><a href="{$admin_url}user/edit?iUserId={$iUserId}">Edit User</a></li>
				<!--<li><a href="{$admin_url}projectbrief/projectbrieflist?iUserId={$iUserId}">Project Brief</a></li>-->
				<li><a href="{$admin_url}transaction/transactionlist?iUserId={$iUserId}">Transaction</a></li>
				<li class="active"><a href="{$admin_url}changepassword/passworduser?iUserId={$iUserId}">Change Password</a></li>
			</ul>
			{/if}
		</div>
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data" action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iUserId" id="iUserId" value="{if $operation neq 'add'}{$iUserId}{/if}" />

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> New Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vPassword" name="Data[vPassword]"  class="inputbox" value="{if $operation neq 'add'}{$data->vFirstName}{/if}" lang="*" title="New Password" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Confirm Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vConformPass" lang="*" value="{if $operation neq 'add'}{$data->vPassword}{/if}" title="Conform Password" class="inputbox" name="Data[vConformPass]" >
				</div>
	
				<div class="add_can_btn"> {if $operation eq 'add'}
					<input type="submit" value="Add Password" class="submit_btn" title="Add Password"/>
					{else}
					<input type="submit" value="Save Password" class="submit_btn" title="Edit Password" onclick="return checkpass();"/>
					{/if} <a href="{$admin_url}user/userlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
				<div class="clear"></div>
			</div>
		</form>
	</div>
</div>
<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
function checkpass(){
	var pass = $('#vPassword').val();
	var confirmpass = $('#vConformPass').val();

	if($('#vPassword').val() ==''){
                $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please enter new password</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
		return false;
	}else if($('#vConformPass').val() ==''){
		$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please enter conform password</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
		return false;
	}else if($('#vPassword').val() != $('#vConformPass').val()){
		$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please check conform password</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
		return false;
	}else{
		//confirm("Sure you want to change your old password");
		$('<div  id="imgdelete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Change Password</div></div><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure , You wanted to Change password ?</h3></div><div class="view_del_user" style="margin-left:86px;"><div class="delete_user"  ><a  class="delete_btnimg" data-dismiss="modal" onclick="deleteimage();" >Change Password</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg" data-dismiss="modal">Cancel</a></div></div></div>').modal();
		return false;
	}
}
function deleteimage(){		
	document.frmadd.submit();
}
</script>
{/literal}
