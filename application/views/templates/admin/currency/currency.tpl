{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
    <div id="breadcrumb">
	    <ul>
		    <li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		    <li><a href="{$admin_url}dashboard">Dashboard</a></li>
		    <li>/</li>
		    <li><a href="{$admin_url}currency/currencylist">Currency</a></li>
		    <li>/</li>
		    <li class="current">{if $operation eq 'add'}Add Currency{else}Edit Currency{/if}</li>
	    </ul>
    </div>
    <div class="centerpartbg">
           {if $operation eq 'add'}
            <div class="pagetitle">Add Currency</div>
            {else}
            <div class="pagetitle">Edit Currency</div>
            {/if}
            <div class="add_ad_contentbox">
                    <form id="frmadd" name="frmadd" method="post" action="">
                            <input type="hidden" name="iCurrencyId" id="iCurrencyId" value="{if $operation neq 'add'}{$data->iCurrencyId}{/if}" />
                            <input type="hidden" name="action" id="action" value="add" />
                            
                            
                            <div class="inputboxes">
					         <label for="textfield"><span class="red_star">*</span> Currency code </label>
						    <span class="collan_dot">:</span>
					         <input type="text" id="vCurrency" name="Data[vCurrency]" class="inputbox" value="{if $operation neq 'add'}{$data->vCurrency}{/if}" lang="*" title="Currency" style="width:240px"/>
			    </div>
			    <div class="inputboxes">
					         <label for="textfield"><span class="red_star">*</span> Currency Icon </label>
						    <span class="collan_dot">:</span>
					         <input type="text" id="iCurrencyIcon" name="Data[iCurrencyIcon]" class="inputbox" value="{if $operation neq 'add'}{$data->iCurrencyIcon}{/if}" lang="*" title="Currency" style="width:240px"/>
			    </div>
			   
                            <div class="inputboxes">
                                    <label for="textfield"><span class="red_star"></span> Status</label>
							 <span class="collan_dot">:</span>
                                    <select id="eStatus" name="Data[eStatus]">
                                            <option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
                                            <option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
                                    </select>
                            </div>
                            <div class="add_can_btn">
                                    {if $operation eq 'add'}
                                    <input type="button" value="Add Currency" class="submit_btn" title="Add Currency" onclick="checkCurrency('{$admin_url}');"/>
                                    {else}
                                    <input type="button" value="Edit Currency" class="submit_btn" title="Edit Currency" onclick="checkCurrency('{$admin_url}');"/>
                                    {/if}
                                    <a href="{$admin_url}currency/currencylist" class="cancel_btn">Cancel</a>
                            </div>
                    </form>
            </div>
    </div>
    <div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
