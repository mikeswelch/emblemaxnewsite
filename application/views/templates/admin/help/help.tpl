<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}help/helplist">Help</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Help{else}Edit Help{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Help
			{else}
			Edit Help
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iHelpId" id="iHelpId" value="{if $operation neq 'add'}{$data->iHelpId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Help Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vHelpTitle" name="Data[vHelpTitle]" class="inputbox" value="{if $operation neq 'add'}{$data->vHelpTitle}{/if}" lang="*" title="Help Title" style="width:300px"/>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<input id="iOrderNo" name="Data[iOrderNo]" class="inputbox" title="Order No" maxlength="3" lang="{literal}*{N}{/literal}" value="{if $operation neq 'add'}{$data->iOrderNo}{/if}" onkeypress="return checkprise(event)">
				</div>
				<div class="inputboxes">
					<label for="textfield">Help Description</label>
					<span class="collan_dot">:</span>
					<textarea id="tHelpDescription" name="Data[tHelpDescription]" class="inputbox inputbox_big" title="Help Description">{if $operation neq 'add'}{$data->tHelpDescription}{/if}</textarea>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Help" class="submit_btn" title="Add Help" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Help" class="submit_btn" title="Edit Help" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}help/helplist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
	</script>
{/literal} 
