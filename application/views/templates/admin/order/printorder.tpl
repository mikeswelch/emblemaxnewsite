<link href="{$admin_css_path}style.css" rel="stylesheet" type="text/css" />
	<div class="centerpartbg">
		<div class="invoice_page">
			<div class="invoice_de_box">
				<div class="invo_de_title">Order Information</div>
				<div class="invoice_de_cont">
					<table cellpadding="0" cellspacing="1" width="100%">
						<tr>
							<td class="in_de_title">Transaction Number</td>
							<td><input type="text" value="{$orderdata[0]->vPaypalTransactionId}" /></td>
						</tr>
						<tr>
							<td class="in_de_title">Order Date</td>
							<td><input type="text" value="{date("dS M Y",strtotime($orderdata[0]->dAddedDate))}" /></td>
						</tr>
						<tr>
							<td class="in_de_title">Shipping Charge</td>
							<td><input type="text" value="{$orderdata[0]->vShippingCharge}" /></td>
						</tr>
						<tr>
							<td class="in_de_title">Sub Total</td>
							<td><input type="text" value="{$orderdata[0]->fSubTotal}" /></td>
						</tr>
						<tr>
							<td class="in_de_title">Amount</td>
							<td><input type="text" value="{$orderdata[0]->fGrandTotal}" /></td>
						</tr>
					</table>
				</div>
			</div>
			
			<div class="client_info_box">
				<div class="invo_de_title">User Information</div>
			<div class="in_ad_box">
				<div class="logo_de_sub" style="height:87px; width:147px; float:left;">
				{if $orderdata[0]->vPhoto neq ''}
				<img height="90px" width="140px" src="{$upload_path}user/{$orderdata[0]->iUserId}/{$orderdata[0]->vPhoto}"/>
				{else}
				<img height="120px" width="150px" src="{$admin_image_path}/noimage.jpg"/>
				{/if}
				</div>
				<div style="margin-left:160px;">
				<div class="in_ad_box_title">{$orderdata[0]->vFirstName} {$orderdata[0]->vLastName}</div>
				{$orderdata[0]->tAddress} , <br />
				 {$state[0]->vState},{$country[0]->vCountry}<br />
				<strong>Phone No :</strong> {$orderdata[0]->vShippPhone}<br />
				<strong>Email :</strong> <a href="mailto:{$orderdata[0]->vShippEmail}">{$orderdata[0]->vShippEmail}</a><br />
				<!--<strong>Skype Name :</strong> <a href="#">{$invoicedata[0]->vSkypeName}</a>--> </div>
			</div>
			</div>	
			
			<div class="clear"></div>
			<div style="height:10px;"></div>
				{if $orderdata|@count gt 0}
				{section name=i loop=$orderdata}
				        <table style="border:1px solid black;height: 30px;">
					    <tr>
						<td>					
						    <div class="logo_de_sub" style="height:100px; width:170px; float:left; margin-top:5px; ">
							{if $orderdata[0]->vFrontImage eq ''}
							 <img src="{$admin_image}/noimage.jpg}" width="70px" height="98px">&nbsp;&nbsp;<img src="{$upload_path}product/{$orderdata[0]->iProductId}/{$orderdata[0]->iProductColorId}/{$orderdata[0]->vBackImage}" width="70px" height="98px">
						{else}
						  <img src="{$upload_path}product/{$orderdata[0]->iProductId}/{$orderdata[0]->iProductColorId}/{$orderdata[0]->vFrontImage}" width="70px" height="98px">&nbsp;&nbsp;<img src="{$upload_path}product/{$orderdata[0]->iProductId}/{$orderdata[0]->iProductColorId}/{$orderdata[0]->vBackImage}" width="70px" height="98px">
						{/if}	
						    </div>
						</td>
						<td width="20%">
						    <div>
						        <div><strong>Design :</strong> {$orderdata[i]->vTitle}<br/>
							{$orderdata[i]->vProductName}</div><br />
					   	        <strong>Color :</strong> {$orderdata[i]->vColor}<br />
						    </div>
						</td>
						<td width="10%"></td>
						<td>
						    <select id="size" name="Data[iDesignParentCategoryId]" onchange="checkparent(this.value)">
							{section name=j loop=$size}
							<option value='{$size[j]['iSizeId']}' {if $orderdata[i]->iSizeId eq $size[j]['iSizeId']}selected{/if}>{$size[j]['vSize']}</option>
							{/section}
						    </select>
						</td>
						<td>
							<input type="text" id="iQty" name="Data[iQty]" class="inputbox" value="{$orderdata[i]->iQty}" title="Quantity" size="4"/>
						</td>
						<td width="10px"></td>
						<td width="200px">
						    <strong>${$orderdata[i]->fPrice}</strong>	
						</td>
					    </tr>
				        </table>
					<div style="height:10px;"></div>
				{/section}
				{/if}
			
			<div class="his_total" style="margin-top: -256px; width:200px; ">
				<table cellpadding="0" cellspacing="1" width="100%" >  
					<tbody>
					<!--<tr>	
						<td class="right_totaltitle">Shipping Charge</td>
						<td width="100">${$orderdata[0]->vShippingCharge}</td>
					</tr>-->
					<tr>	
							<td class="right_totaltitle">Sub Total</td>
						<td>${$total}</td>
					</tr>
					<!--<tr>	
						<td class="right_totaltitle"><strong>Grand Total</strong></td>
						<td><strong>${$orderdata[0]->fGrandTotal}</strong></td>
					</tr>-->
				</tbody></table>
			</div>
			<div class="clear"></div>
			<div class="print_send_cl">
				<input type="submit" value="Print" class="submit_btn" title="" onClick="javascript:window.print();"/>
				<a href="{$admin_url}order" style="text-decoration:none;" class="cancel_btn">Cancel</a>
		        </div>
		</div>
	</div>
