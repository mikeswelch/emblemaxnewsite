{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<link rel="stylesheet" href="{$admin_css_path}boxes.css" type="text/css">

<div class="centerpart">
<div id="breadcrumb">
    <ul>
		  <li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
		  <li><a href="{$admin_url}dashboard">Dashboard</a></li>
		  <li>/</li>
		  <li class="current"><a href="{$admin_url}order">Order Details</a></li>
    </ul>
	
</div>
   <div class="centerpartbg">
   <div class="pagetitle">Order Details</div>
	<div id="main-container" class="contrightpart">
		
		<ul id="sales_order_view_tabs" class="tabs">
			<li >
				<div class="box-left"> 
					<!--Order Information-->
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-account">Order {$orderData[0]->vInvoiceNum} (the order confirmation email was sent)</h4>
						</div>
						<div class="fieldset">
							<table cellspacing="0" class="form-list">
								<tr>
									<td class="label"><label>Order Date</label></td>
									<td class="value"><strong>{$orderDate}</strong></td>
								</tr>
								<tr>
									<td class="label"><label>Order Status</label></td>
									<td class="value"><strong><span id="order_status">{$orderData[0]->eStatus}</span></strong></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="box-right"> 
					<!--Account Information-->
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-account">Account Information</h4>
							<div class="tools"></div>
						</div>
						<div class="fieldset">
							<div class="hor-scroll">
								<table cellspacing="0" class="form-list">
									<tr>
										<td class="label"><label>Customer Name</label></td>
										<td class="value"><a href="#"><strong>{$orderData[0]->vShipFirstname|ucFirst}</strong></a></td>
									</tr>
									<tr>
										<td class="label"><label>Email</label></td>
										<td class="value"><a href="mailto:jkimilu@gmail.com"><strong>{$orderData[0]->vShipEmail}</strong></a></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="box-left"> 
					<!--Billing Address-->
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-billing-address">Billing Address</h4>
						</div>
						<fieldset>
							<address>
								{$orderData[0]->vBillFirstname|ucFirst} {$orderData[0]->vBillLastname} <br/>
								{$orderData[0]->vBillAddress}	,<br />
								{$orderData[0]->vBillCity},<br />								
								{section name=i loop=$state}
								<div style="color:#A9A9A9; ">{if $state[i]->iStateId eq $orderData[0]->iBillStateId} {$state[i]->vState} {/if}</div>
								{/section}
								
								{section name=j loop=$country}
								<div style="color:#A9A9A9; ">{if $country[j]->iCountryId eq $orderData[0]->iBillCountryId} {$country[j]->vCountry} {/if}</div>
								{/section}
								<br/>
								{$state[0]->vState}<br>
								{$country[0]->vCountry}<br>
								Zipcode :{$orderData[0]->vBillPostcode}<br/>
								Phone :{$orderData[0]->vBillPhone}
							</address>
						</fieldset>
					</div>
				</div>
				<div class="box-right"> 
					<!--Shipping Address-->
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-shipping-address">Shipping Address</h4>
						</div>
						<fieldset>
							<address>
								{$orderData[0]->vShipFirstname|ucFirst} {$orderData[0]->vShipLastname} <br/>
								{$orderData[0]->vShipAddress}	,<br />
								{$orderData[0]->vShipCity}	,<br />
								{section name=i loop=$state}
								<div style="color:#A9A9A9; ">{if $state[i]->iStateId eq $orderData[0]->iShippStateId} {$state[i]->vState} {/if}</div>
								{/section}
								
								{section name=j loop=$country}
								<div style="color:#A9A9A9; ">{if $country[j]->iCountryId eq $orderData[0]->iShippCountryId} {$country[j]->vCountry} {/if}</div>
								{/section}
								<br/>
								{$shipping_state[0]->vState}<br>
								{$shipping_country[0]->vCountry}<br>
								Zipcode :{$orderData[0]->vShipPostcode}<br/>
								Phone :{$orderData[0]->vShipPhone}
							</address>
						</fieldset>
					</div>
				</div>
				<div class="clear"></div>
				<input type="hidden" name="order_id" value="684"/>
				<div class="box-left"> 
					<!--Payment Method-->
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-payment-method">Payment Information</h4>
						</div>
						<fieldset>
							Paypal
							<div>Transaction Id : {$orderData[0]->vPaypalTransactionId}</div>
						</fieldset>
					</div>
				</div>
				<div class="box-right"> 
					<!--Shipping Method-->
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-shipping-method">Shipping &amp; Handling Information</h4>
						</div>
						<fieldset>
						  <fieldset>							
							<div>Shipping Method: {$orderData[0]->iShippingMethodId}</div>
						  </fieldset>
							
						</fieldset>
					</div>
				</div>
				<div class="clear"></div>
				<div class="clear"></div>
				<div class="entry-edit">
					<div class="entry-edit-head">
						<h4 class="icon-head head-products">Items Ordered</h4>
						
					</div>
				</div>
				<div class="grid np">
					<div class="hor-scroll">
						<table cellspacing="0" class="data order-tables">
							<col />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<col width="1" />
							<thead>
								<tr class="headings">
									<th>Product</th>
									<!--<th><span class="nobr">Item Status</span></th>-->
									<!--<th><span class="nobr">Original Price</span></th>-->
									<th>Product Name</th>
									<th>Select Print Location</th>
									<th>Color Sizes</th>
									
									<th class="a-center">Total Qty</th>
									<th>Grand Total</th>
								</tr>
							</thead>
							{if $orderData|@count gt 0}
							{section name=i loop=$orderData}							
							<tbody class="even">
								<tr class="border">
									<td><div id="order_item_1117" class="item-container">
											<div class="item-text" style="width: 209px;float:left;padding-top:20px;">
												<h5 class="title">
												<a href="#Fancyhrefmain{$smarty.section.i.index}" id="fancyhrefmain" class="view_btnimg">												    
												    <img src="{$upload_path}product/{$orderData[i]->iProductId}/193X198_{$orderData[i]->vImage}" height="150px" width="150px">
													
												    <div style="display:none;">
												    <div id="Fancyhrefmain{$smarty.section.i.index}"><img src="{$upload_path}product/{$orderData[i]->iProductId}/{$orderData[i]->vImage}"><!--<input type="submit" style="margin-left: 00px;" class="cancel_btn" value="Download">--></div>
												    </div>
												</a>
												<a href="#Fancyhrefback{$smarty.section.i.index}" id="fancyhrefback" class="view_btnimg">											
												<img src="{$upload_path}product/{$orderData[i]->iProductId}/{$orderData[i]->iProductColorId}/{$orderData[i]->vBackImage}" height="150px" width="150px">
											        <div style="display:none;">
												    <div id="Fancyhrefback{$smarty.section.i.index}" ><img src="{$upload_path}product/{$orderData[i]->iProductId}/{$orderData[i]->iProductColorId}/{$orderData[i]->vBackImage}"></div>
												</div>
											    </div>
											<div id="order_item_1117_title" style="float: left; width: 150px;padding-top:70px;">{$orderData[i]->vProductName}</div></h5>
										</div>
									</td>
									
									<td class="a-right" style="text-align: center;"><span class="price-excl-tax"> <span class="price">{$productName}</span> </span> <br /></td>
									
									<td class="a-right" width="250" style="text-align: center;"><span class="price-excl-tax"> <span class="price">
									   {section name=j loop=$quoteSummary['vPrintLocationsDescription']}
									    {$quoteSummary['vPrintLocationsDescription'][j]}<br>
									   {/section}															  
									</td>								
									<td class="a-right" style="text-align: center;" width="250"><span class="price-excl-tax"> <span class="price">
										  {section name=k loop=$quoteSummary['vColorSizeDescription']}
											{$quoteSummary['vColorSizeDescription'][k]}
											<br>
										 {/section}
									</td>
									
									<td style="text-align: center;"><table cellspacing="0" class="qty-table">
											<tr>
												<td><strong>{$quoteSummary['totalQty']}</strong></td>
											</tr>
										</table></td>
									<td class="a-right" style="text-align: center;">
										<span class="price-excl-tax"> <span class="price">$ {$orderData[i]->finalBillAmount}</span> </span> <br />
									</td>
								</tr>
							</tbody>
							{/section}
							{/if}
						</table>
					</div>
				</div>
				<br />
				<div class="clear"></div>
				<div class="box-left">
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4>Comments History</h4>
						</div>
						<fieldset>
							<div id="order_history_block">
							    <form method="POST" action="{$admin_url}order/saveComment" name="comment">
								<div id="history_form" class="order-history-form">
								    {if $smarty.get.msg}<div style="background-color: #38414B; border: 1px solid rgb(238, 211, 215); border-radius: 5px 5px 5px 5px; color: #FFFFFF; font-size: 13px; font-weight: bold; margin-left: 10px; margin-top: 0px; padding-top: 15px; padding-left: 5px; width: 433px; height: 30px;">{$smarty.get.msg}</div>{/if}
								    <div style="color: red;">{if $msg neq ''}{$msg}{/if}</div>
									<div>Add Order Comments</div>
									<input type="hidden" name="iOrderId" id="iOrderId" value="{$orderData[0]->iOrderId}" />
									<span class="field-row">
									<label class="normal" for="history_status">Status</label>
									<br/>
									<select name="history[status]" class="select" id="history_status">
									{section name=k loop=$orderStatus}	
										<option value="{$orderStatus[k]}" {if $orderStatus[k] eq $orderData[0]->eStatus} selected {/if}>{$orderStatus[k]}</option>
								        {/section}
									</select>
									</span> <span class="field-row">
									<label class="normal" for="history_comment">Comment</label>
									<textarea name="history[comment]" rows="3" cols="5" style="height:6em; width:99%;" id="history_comment"></textarea>
									</span>
									<div class="f-right">
										<button  id="id_f3896e37ceeafe25e3a573df26147241" title="Submit Comment" type="submit" <span>Submit Comment</span></span></span></button>
									</div>
									<div class="clear"></div>
								</div>
								</form>
								<div class="divider"></div>
								<ul class="note-list">
								    {if $tComments[0]->tComments neq ''}
								    {section name=k loop=$tComments}
									<li> <strong>{$date_comment}</strong> {$time_comment}<span class="separator">|</span><strong>{$tComments[k]->tComments}</strong><br/>
										<small>Customer {if $tComments[k]->vShippName eq ''}<strong class="subdue"> Not Notified </strong>{else}
										<strong class="subdue"> {$tComments[k]->vShippName} </strong>
										{/if}
										</small>
									</li>
									{/section}
								    {/if}
								</ul>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="box-right entry-edit">
					<div class="entry-edit-head">
						<h4>Order Totals</h4>
					</div>
					<div class="order-totals">
						<table cellspacing="0" width="100%">
							<col />
							<col width="1" />
							<tfoot>
								<tr class="0">
									<td  class="label"><strong>Grand Total:</strong></td>
									<td  class="emph"><strong><span class="price">${$orderData[0]->fGrandTotal}</span></strong></td>
								</tr>
							</tfoot>
							<tbody>
								<tr class="0">
									<td  class="label"> Subtotal: </td>
									<td ><span class="price">${$orderData[0]->fSubTotal}</span></td>
								</tr>
								<tr class="1">
									<td  class="label"> Discount[Coupon Code: {$orderData[0]->vCouponCode}]: </td>
									<td ><span class="price">${$orderData[0]->fDiscountAmount}</span></td>
								</tr>
								<tr class="1">
									<td  class="label"> Shipping Charges[{$orderData[0]->vShippingMethodTitle}]: </td>
									<td ><span class="price">$ {if $orderData[0]->fShippmentCharge eq '' }0.00 {else}{$orderData[0]->fShippmentCharge}{/if}</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="clear"></div>
			
			<div id="gift_options_window_mask" class="popup-window-mask" style="display: none;"></div>
			<div id="gift_options_configure_new" class="gift_options-popup product-configure-popup" style="display: none;">
				<div id="gift_options_form_contents">
					<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head fieldset-legend">Gift Options for <span id="gift_options_configure_title"></span></h4>
						</div>
						<div class="content"> </div>
						<div class="buttons-set a-right">
							<button type="button" class="scalable" id="gift_options_cancel_button"><span><span><span>Cancel</span></span></span></button>
							<button type="button" class="scalable" id="gift_options_ok_button"><span><span><span>OK</span></span></span></button>
						</div>
					</div>
				</div>
			</div>
			<div id="giftoptions_tooltip_window" class="gift-options-tooltip" style="display:none;">
				<div id="giftoptions_tooltip_window_content">&nbsp;</div>
			</div>
		
			</li>
			<li >
				<div id="sales_order_view_tabs_order_creditmemos_content" style="display:none;">
					<div id="order_creditmemos">
						<table cellspacing="0" class="actions">
							<tr>
								<td class="pager"> Page <img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow"/>
									<input type="text" name="page" value="1" class="input-text page" onkeypress="order_creditmemosJsObject.inputPage(event, '1')"/>
									<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/pager_arrow_right_off.gif" alt="Go to Previous page" class="arrow"/> of 1 pages <span class="separator">|</span> View
									<select name="limit" onchange="order_creditmemosJsObject.loadByElement(this)">
										<option value="20" selected="selected">20</option>
										<option value="30">30</option>
										<option value="50">50</option>
										<option value="100">100</option>
										<option value="200">200</option>
									</select>
									per page<span class="separator">|</span> Total 0 records found <span id="order_creditmemos-total-count" class="no-display">0</span></td>
								<td class="filter-actions a-right"><button  id="id_684146756b6375f43842376df1e07f35" title="Reset Filter" type="button" class="scalable " onclick="order_creditmemosJsObject.resetFilter()" style=""><span><span><span>Reset Filter</span></span></span></button>
									<button  id="id_13fce2231145966ef834f09556ccdfb8" title="Search" type="button" class="scalable task" onclick="order_creditmemosJsObject.doFilter()" style=""><span><span><span>Search</span></span></span></button></td>
							</tr>
						</table>
						<div class="grid">
							<div class="hor-scroll">
								<table cellspacing="0" class="data" id="order_creditmemos_table">
									<col  width="120" />
									<col  />
									<col  />
									<col  />
									<col  width="100" />
									<thead>
										<tr class="headings">
											<th><span class="nobr"><a href="#" name="increment_id" title="asc" class="not-sort"><span class="sort-title">Credit Memo #</span></a></span></th>
											<th><span class="nobr"><a href="#" name="billing_name" title="asc" class="not-sort"><span class="sort-title">Bill to Name</span></a></span></th>
											<th><span class="nobr"><a href="#" name="created_at" title="asc" class="not-sort"><span class="sort-title">Created At</span></a></span></th>
											<th><span class="nobr"><a href="#" name="state" title="asc" class="not-sort"><span class="sort-title">Status</span></a></span></th>
											<th class=" last"><span class="nobr"><a href="#" name="base_grand_total" title="asc" class="not-sort"><span class="sort-title">Refunded</span></a></span></th>
										</tr>
										<tr class="filter">
											<th><div class="field-100">
													<input type="text" name="increment_id" id="order_creditmemos_filter_increment_id" value="" class="input-text no-changes"/>
												</div></th>
											<th><div class="field-100">
													<input type="text" name="billing_name" id="order_creditmemos_filter_billing_name" value="" class="input-text no-changes"/>
												</div></th>
											<th><div class="range">
													<div class="range-line date"> <span class="label">From:</span>
														<input type="text" name="created_at[from]" id="order_creditmemos_filter_created_at1368728325.3431_from" value="" class="input-text no-changes"/>
														<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="order_creditmemos_filter_created_at1368728325.3431_from_trig" title="Date selector"/> </div>
													<div class="range-line date"> <span class="label">To :</span>
														<input type="text" name="created_at[to]" id="order_creditmemos_filter_created_at1368728325.3431_to" value="" class="input-text no-changes"/>
														<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="order_creditmemos_filter_created_at1368728325.3431_to_trig" title="Date selector"/> </div>
												</div>
												<input type="hidden" name="created_at[locale]" value="en_US"/></th>
											<th><select name="state" id="order_creditmemos_filter_state" class="no-changes">
													<option value=""></option>
													<option value="1">Pending</option>
													<option value="2">Refunded</option>
													<option value="3">Canceled</option>
												</select></th>
											<th class=" last"><div class="range">
													<div class="range-line"><span class="label">From:</span>
														<input type="text" name="base_grand_total[from]" id="order_creditmemos_filter_base_grand_total_from" value="" class="input-text no-changes"/>
													</div>
													<div class="range-line"><span class="label">To : </span>
														<input type="text" name="base_grand_total[to]" id="order_creditmemos_filter_base_grand_total_to" value="" class="input-text no-changes"/>
													</div>
												</div></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="empty-text a-center" colspan="5">No records found.</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li >
				<div id="sales_order_view_tabs_order_shipments_content" style="display:none;">
					<div id="order_shipments">
						<table cellspacing="0" class="actions">
							<tr>
								<td class="pager"> Page <img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow"/>
									<input type="text" name="page" value="1" class="input-text page" onkeypress="order_shipmentsJsObject.inputPage(event, '1')"/>
									<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/pager_arrow_right_off.gif" alt="Go to Previous page" class="arrow"/> of 1 pages <span class="separator">|</span> View
									<select name="limit" onchange="order_shipmentsJsObject.loadByElement(this)">
										<option value="20" selected="selected">20</option>
										<option value="30">30</option>
										<option value="50">50</option>
										<option value="100">100</option>
										<option value="200">200</option>
									</select>
									per page<span class="separator">|</span> Total 1 records found <span id="order_shipments-total-count" class="no-display">1</span></td>
								<td class="filter-actions a-right"><button  id="id_753abe89f59d2a06f2efde524165eb6f" title="Reset Filter" type="button" class="scalable " onclick="order_shipmentsJsObject.resetFilter()" style=""><span><span><span>Reset Filter</span></span></span></button>
									<button  id="id_8a9189333ddd20809841c4ecdee7dcb5" title="Search" type="button" class="scalable task" onclick="order_shipmentsJsObject.doFilter()" style=""><span><span><span>Search</span></span></span></button></td>
							</tr>
						</table>
						<div class="grid">
							<div class="hor-scroll">
								<table cellspacing="0" class="data" id="order_shipments_table">
									<col  />
									<col  />
									<col  />
									<col  width="100" />
									<thead>
										<tr class="headings">
											<th><span class="nobr"><a href="#" name="increment_id" title="asc" class="not-sort"><span class="sort-title">Shipment #</span></a></span></th>
											<th><span class="nobr"><a href="#" name="shipping_name" title="asc" class="not-sort"><span class="sort-title">Ship to Name</span></a></span></th>
											<th><span class="nobr"><a href="#" name="created_at" title="asc" class="not-sort"><span class="sort-title">Date Shipped</span></a></span></th>
											<th class=" last"><span class="nobr"><a href="#" name="total_qty" title="asc" class="not-sort"><span class="sort-title">Total Qty</span></a></span></th>
										</tr>
										<tr class="filter">
											<th><div class="field-100">
													<input type="text" name="increment_id" id="order_shipments_filter_increment_id" value="" class="input-text no-changes"/>
												</div></th>
											<th><div class="field-100">
													<input type="text" name="shipping_name" id="order_shipments_filter_shipping_name" value="" class="input-text no-changes"/>
												</div></th>
											<th><div class="range">
													<div class="range-line date"> <span class="label">From:</span>
														<input type="text" name="created_at[from]" id="order_shipments_filter_created_at1368728325.3539_from" value="" class="input-text no-changes"/>
														<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="order_shipments_filter_created_at1368728325.3539_from_trig" title="Date selector"/> </div>
													<div class="range-line date"> <span class="label">To :</span>
														<input type="text" name="created_at[to]" id="order_shipments_filter_created_at1368728325.3539_to" value="" class="input-text no-changes"/>
														<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="order_shipments_filter_created_at1368728325.3539_to_trig" title="Date selector"/> </div>
												</div>
												<input type="hidden" name="created_at[locale]" value="en_US"/></th>
											<th class=" last"><div class="range">
													<div class="range-line"><span class="label">From:</span>
														<input type="text" name="total_qty[from]" id="order_shipments_filter_total_qty_from" value="" class="input-text no-changes"/>
													</div>
													<div class="range-line"><span class="label">To : </span>
														<input type="text" name="total_qty[to]" id="order_shipments_filter_total_qty_to" value="" class="input-text no-changes"/>
													</div>
												</div></th>
										</tr>
									</thead>
									<tbody>
										<tr title="http://demo-admin.magentocommerce.com/index.php/admin/sales_order_shipment/view/shipment_id/497/key/5c3a310a87f5108e689cebaa54b14773/" >
											<td class=" "> 300000022 </td>
											<td class=" "> sergio hs </td>
											<td class=" "> May 16, 2013 8:18:53 AM </td>
											<td class=" a-right last"> 1 </td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li >
				<div id="sales_order_view_tabs_order_history_content" style="display:none;">
					<div class="entry-edit">
						<fieldset>
							<ul class="note-list">
								<li> <strong>May 16, 2013</strong> 8:17:21 AM<span class="separator">|</span> <strong>NEW  - Pending</strong> <span class="separator">|</span> <small> Customer <strong class="subdue"> Not Notified </strong> </small> </li>
								<li> <strong>May 16, 2013</strong> 8:18:53 AM<span class="separator">|</span> <strong>Tracking number 1222315 for Sarment delivery assigned</strong> <span class="separator">|</span> <small> Customer <strong class="subdue"> Not Notified </strong> </small> </li>
								<li> <strong>May 16, 2013</strong> 8:18:53 AM<span class="separator">|</span> <strong>Shipment #300000022 created</strong> <span class="separator">|</span> <small> Customer <strong class="subdue"> Not Notified </strong> </small> </li>
								<li> <strong>May 16, 2013</strong> 8:18:53 AM<span class="separator">|</span> <strong>aaabbbccc</strong> <span class="separator">|</span> <small> Customer <strong class="subdue"> Not Notified </strong> </small> </li>
							</ul>
						</fieldset>
					</div>
				</div>
			</li>
			<li >
				<div id="sales_order_view_tabs_order_transactions_content" style="display:none;">
					<div id="order_transactions">
						<table cellspacing="0" class="actions">
							<tr>
								<td class="pager"> Page <img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/pager_arrow_left_off.gif" alt="Go to Previous page" class="arrow"/>
									<input type="text" name="page" value="1" class="input-text page" onkeypress="order_transactionsJsObject.inputPage(event, '1')"/>
									<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/pager_arrow_right_off.gif" alt="Go to Previous page" class="arrow"/> of 1 pages <span class="separator">|</span> View
									<select name="limit" onchange="order_transactionsJsObject.loadByElement(this)">
										<option value="20" selected="selected">20</option>
										<option value="30">30</option>
										<option value="50">50</option>
										<option value="100">100</option>
										<option value="200">200</option>
									</select>
									per page<span class="separator">|</span> Total 0 records found <span id="order_transactions-total-count" class="no-display">0</span></td>
								<td class="filter-actions a-right"><button  id="id_64881cabf547a9772b16edd20b7affab" title="Reset Filter" type="button" class="scalable " onclick="order_transactionsJsObject.resetFilter()" style=""><span><span><span>Reset Filter</span></span></span></button>
									<button  id="id_86a6e3f12cf81bee4a1f0a3df4b6f5e6" title="Search" type="button" class="scalable task" onclick="order_transactionsJsObject.doFilter()" style=""><span><span><span>Search</span></span></span></button></td>
							</tr>
						</table>
						<div class="grid">
							<div class="hor-scroll">
								<table cellspacing="0" class="data" id="order_transactions_table">
									<col  width="100" />
									<col  />
									<col  />
									<col  />
									
									<col  />
									<col  />
									<col  width="1" />
									<col  width="1" />
									<thead>
										<tr class="headings">
											<th><span class="nobr"><a href="#" name="transaction_id" title="asc" class="not-sort"><span class="sort-title">ID #</span></a></span></th>
											<th><span class="nobr"><a href="#" name="increment_id" title="asc" class="not-sort"><span class="sort-title">Order ID</span></a></span></th>
											<th><span class="nobr"><a href="#" name="txn_id" title="asc" class="not-sort"><span class="sort-title">Transaction ID</span></a></span></th>
											<th><span class="nobr"><a href="#" name="parent_txn_id" title="asc" class="not-sort"><span class="sort-title">Parent Transaction ID</span></a></span></th>
											<th><span class="nobr"><a href="#" name="method" title="asc" class="not-sort"><span class="sort-title">Payment Method Name</span></a></span></th>
											<th><span class="nobr"><a href="#" name="txn_type" title="asc" class="not-sort"><span class="sort-title">Transaction Type</span></a></span></th>
											<th><span class="nobr"><a href="#" name="is_closed" title="asc" class="not-sort"><span class="sort-title">Is Closed</span></a></span></th>
											<th class=" last"><span class="nobr"><a href="#" name="created_at" title="asc" class="sort-arrow-desc"><span class="sort-title">Created At</span></a></span></th>
										</tr>
										<tr class="filter">
											<th><div class="range">
													<div class="range-line"><span class="label">From:</span>
														<input type="text" name="transaction_id[from]" id="order_transactions_filter_transaction_id_from" value="" class="input-text no-changes"/>
													</div>
													<div class="range-line"><span class="label">To : </span>
														<input type="text" name="transaction_id[to]" id="order_transactions_filter_transaction_id_to" value="" class="input-text no-changes"/>
													</div>
												</div></th>
											<th><div class="field-100">
													<input type="text" name="increment_id" id="order_transactions_filter_increment_id" value="" class="input-text no-changes"/>
												</div></th>
											<th><div class="field-100">
													<input type="text" name="txn_id" id="order_transactions_filter_txn_id" value="" class="input-text no-changes"/>
												</div></th>
											<th><div class="field-100">
													<input type="text" name="parent_txn_id" id="order_transactions_filter_parent_txn_id" value="" class="input-text no-changes"/>
												</div></th>
											<th><select name="method" id="order_transactions_filter_method" class="no-changes">
													<option value=""></option>
													<option value="authorizenet">Credit Card (Authorize.net)</option>
													<option value="authorizenet_directpost">Credit Card Direct Post (Authorize.net)</option>
													<option value="googlecheckout">Google Checkout</option>
													<optgroup label="Moneybookers">
													<option value="moneybookers_pwy">All Polish Banks</option>
													<option value="moneybookers_csi">CartaSi</option>
													<option value="moneybookers_gcb">Carte Bleue</option>
													<option value="moneybookers_acc">Credit Card / Visa, Mastercard, AMEX, JCB, Diners</option>
													<option value="moneybookers_dnk">Dankort</option>
													<option value="moneybookers_npy">EPS Online-Überweisung</option>
													<option value="moneybookers_gir">Giropay</option>
													<option value="moneybookers_lsr">Laser</option>
													<option value="moneybookers_did">Lastschrift (ELV)</option>
													<option value="moneybookers_mae">Maestro</option>
													<option value="moneybookers_wlt">Moneybookers eWallet</option>
													<option value="moneybookers_so2">Nordea Solo</option>
													<option value="moneybookers_ebt">Nordea Solo</option>
													<option value="moneybookers_obt">Onlineüberweisung (empfohlen)</option>
													<option value="moneybookers_pli">POLi</option>
													<option value="moneybookers_psp">Postepay</option>
													<option value="moneybookers_sft">Sofortüberweisung</option>
													<option value="moneybookers_ent">eNETS</option>
													<option value="moneybookers_idl">iDeal</option>
													</optgroup>
													<optgroup label="Offline Payment Methods">
													<option value="banktransfer">Bank Transfer Payment</option>
													<option value="cashondelivery">Cash On Delivery</option>
													<option value="checkmo">Check / Money order</option>
													<option value="ccsave">Credit Card (saved)</option>
													<option value="free">No Payment Information Required</option>
													<option value="purchaseorder">Purchase Order</option>
													</optgroup>
													<optgroup label="PayPal">
													<option value="payflow_link">Credit Card</option>
													<option value="payflow_advanced">Credit Card</option>
													<option value="paypal_billing_agreement">PayPal Billing Agreement</option>
													<option value="paypal_express">PayPal Express Checkout</option>
													<option value="paypaluk_express">PayPal Express Checkout Payflow Edition</option>
													<option value="paypal_mecl">PayPal Mobile Express Checkout Library</option>
													<option value="paypal_mep">PayPal Mobile Payments Library</option>
													<option value="paypal_direct">PayPal Payments Pro</option>
													<option value="paypaluk_direct">PayPal Payments Pro Payflow Edition</option>
													<option value="paypal_standard">PayPal Website Payments Standard</option>
													<option value="verisign">Payflow Pro</option>
													<option value="hosted_pro">Payment by cards or by PayPal account</option>
													</optgroup>
												</select></th>
											<th><select name="txn_type" id="order_transactions_filter_txn_type" class="no-changes">
													<option value=""></option>
													<option value="order">Order</option>
													<option value="authorization">Authorization</option>
													<option value="capture">Capture</option>
													<option value="void">Void</option>
													<option value="refund">Refund</option>
												</select></th>
											<th><select name="is_closed" id="order_transactions_filter_is_closed" class="no-changes">
													<option value=""></option>
													<option value="1">Yes</option>
													<option value="0">No</option>
												</select></th>
											<th class=" last"><div class="range">
													<div class="range-line date"> <span class="label">From:</span>
														<input type="text" name="created_at[from]" id="order_transactions_filter_created_at1368728325.3877_from" value="" class="input-text no-changes"/>
														<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="order_transactions_filter_created_at1368728325.3877_from_trig" title="Date selector"/> </div>
													<div class="range-line date"> <span class="label">To :</span>
														<input type="text" name="created_at[to]" id="order_transactions_filter_created_at1368728325.3877_to" value="" class="input-text no-changes"/>
														<img src="http://demo-admin.magentocommerce.com/skin/adminhtml/default/default/images/grid-cal.gif" alt="" class="v-middle" id="order_transactions_filter_created_at1368728325.3877_to_trig" title="Date selector"/> </div>
												</div>
												<input type="hidden" name="created_at[locale]" value="en_US"/></th>
										</tr>
									</thead>

								</table>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div>
	<a href="{$admin_url}order" style=" margin-left: 25px;text-align: center;text-decoration: none;width: 64px;" class="cancel_btn" >Back</a> </div>

	<div class="clear"></div>
	</div>
     </div>
   
</div>

{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
$("#fancyhrefmain").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,
 
    helpers : {
	
	    overlay : null
    }
});
$("#fancyhrefback").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

</script>
{/literal}