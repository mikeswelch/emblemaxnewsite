{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<!--<li><a href="{$admin_url}client/clientlist">Client</a></li>
			<li>/</li>-->
			<li class="current">Orders</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Orders</div>
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />{$var_msg}</p>
		</div>
		<div></div>
		{/if}
		<div class="export_btn" style="margin-top: 10px;">
			<!--<a href="{$admin_url}all_transaction/export">Export</a>-->
			<form  action="{$admin_url}order/export" >
					<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
					<!--<input type="submit" value="Export" class="search_btn" /> -->
			</form>
		
		</div>
		<form name="frmsearchdate" id="frmsearchdate" method="post" action="{$admin_url}order">
			<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
			<div class="all_trans_box">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top"><div class="search_top_admini">
								<label>Search:</label>
								<select name="option" id="option" onchange="advanceSearch(this.value);">
									<option value="">Select</option>
									<option value="searchByPaymentStatus"{if $filterBy == 'searchByPaymentStatus'}selected="selected"{/if}>Search By Payment Status</option>
									<option value="searchByTransactionId"{if $filterBy == 'searchByTransactionId'}selected="selected"{/if}>Search By First Name</option>
									<option value="searchByGrandTotal"{if $filterBy == 'searchByGrandTotal'}selected="selected"{/if}>Search By Sub Total</option>
								</select>
							</div></td>
						<td><div class="">
								<input type="hidden" value="" name="filterBy" id="filterBy">
								<div id="searchByDateDiv" style="display: none;" class="searchboxes">
									<div class="inputboxes inputboxesst_date">
										<label for="textfield">Start Date</label>
										<span class="collan_dot">:</span>
										<input type="text" readonly="readonly" id="start_date" name="start_date" class="inputbox" value="{if $start_date != ''}{$start_date}{/if}" title="Start Date" lang="*"/>
									</div>
									<div class="inputboxes inputboxesst_date">
										<label for="textfield">End Date</label>
										<span class="collan_dot">:</span>
										<input type="text" readonly="readonly" id="end_date" name="end_date" class="inputbox" value="{if $end_date != ''}{$end_date}{/if}" title="End Date" lang="*"/>
									</div>
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('date_range');"/>
								</div>
								<div id="searchByDayDiv" style="display: none;" class="searchboxes">
									<div class="inputboxes inputboxesst_date">
										<label for="textfield">Select Day</label>
										<span class="collan_dot">:</span>
										<input type="text" readonly="readonly" id="select_day" name="select_day" class="inputbox" value="{if $day != ''}{$day}{/if}" title="Select Day" lang="*"/>
									</div>
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('day');"/>
								</div>
								<div id="searchByPaymentStatusDiv" style="display: none;" class="searchboxes">
									<select name="PaymentStatus" id="PaymentStatus" class="adved">
										<option value="">--Select--</option>
										<option value="Paid" {if $eStatus eq 'Paid'}Selected{/if}>Paid</option>
										<option value="Cancelled" {if $eStatus eq 'Cancelled'}Selected{/if}>Cancelled</option>
										<option value="Processing" {if $eStatus eq 'Processing'}Selected{/if}>Processing</option>
										<option value="Decline" {if $eStatus eq 'Decline'}Selected{/if}>Decline</option>
									</select>
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('PaymentStatus');"/>
								</div>
								<div id="searchByTransactionId" style="display: none;" class="searchboxes">
									<input type="text" id="firstName" name="firstName" class="inputbox" value="{if $vPaypalTransactionId != ''}{$vPaypalTransactionId}{/if}" title="Transaction Id" lang="*">
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('firstName');"/>
								</div>
								<div id="searchByGrandTotal" style="display: none;" class="searchboxes">
									<input type="text" id="total" name="total" class="inputbox" value="{if $fGrandTotal != ''}{$fGrandTotal}{/if}" title="Total Amount" lang="*">
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('total_amount');"/>
								</div>
							</div></td>
					</tr>
				</table>
			</div>
		</form>
		<div class="action_apply_btn_tra">
			<select class="act_sl_box" name="newaction" id="newaction">
				<option value="">Select Action</option>
				<option value="Deletes"{if $action == 'Deletes'}selected="selected"{/if}>Make Delete</option>
				<option value="Show All"{if $action == 'Show All'}selected="selected"{/if}>Show All</option>
			</select>
			<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'order',document.frmlist,'order');"/>
		</div>
		<div class="total_top" style="margin-left: 325px !important;">Total Transaction:$ {$total_price}</div>
		<div class="clear"></div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}order/search_action" method="post">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="iTransactionId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><a href={$admin_url}order/index?field=o.vBillName&order={$order}><span style="color:white;">User Name</span>{if $field eq 'o.vBillName'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th><a href={$admin_url}order/index?field=o.fGrandTotal&order={$order}><span style="color:white;">Grand Total</span>{if $field eq 'o.fGrandTotal'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<!--<th><a href={$admin_url}order/index?field=fGrandTotal><span style="color:white;">Grand Total</span>{if $field eq 'fGrandTotal'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>-->
							<th width="77px"><a href={$admin_url}order/index?field=o.eStatus&order={$order}><span style="color:white;">Status</span> {if $field eq 'o.eStatus'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="90px"><span>Action</span></th>
						</tr>
					</thead>
					
					{if $data|@count gt 0}
					{section name=i loop=$data}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}">
						<tr>
							<td><input name="iOrderId[]" type="checkbox" id="iId" value="{$data[i]->iOrderId}"></td>
							<td><a href="{$admin_url}order/showorder?iOrderId={$data[i]->iOrderId}&iUserId={$data[i]->iUserId}">{$data[i]->vBillFirstname} {$data[i]->vBillLastname}</a></td>
							<td>$&nbsp;{$data[i]->finalBillAmount}</td>
							<td>{$data[i]->eStatus}</td>
							<td><a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon({$data[i]->iOrderId},'order/delete','{$ssql}')"></a></td>
						</tr>
					</tbody>
					{/section}
					<tbody><tr><td></td><td class="admin_antry_table total_bottom_tot">Total Amount:</td><td class="admin_antry_table total_bottom">$&nbsp;{$total}</td></tr></tbody>
					{else}
					<tr>
						<td height="70px;" colspan="6" style="text-align:center; color:#C44C22; font-size:18px; font-weight:bold;">No Record Found.</td>
					</tr>
					{/if}
					
				</table>
				<div></div>
				
			</form>
		</div>
               
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					{if $data|@count gt 0}<td class="reclord_found" width="33%" align="left">{$recmsg}</td>{/if}
					<td width="33%"><div class="bottom_admin_paging">{$page_link} </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script>
	var opts1 = {formElements:{"start_date":"Y-ds-m-ds-d"}};
		datePickerController.createDatePicker(opts1);
	var opts2 = {formElements:{"end_date":"Y-ds-m-ds-d"}};
		datePickerController.createDatePicker(opts2);
	var opts3 = {formElements:{"select_day":"Y-ds-m-ds-d"}};
		datePickerController.createDatePicker(opts3);
		
	function filterTransactions(filterBy){
		var site_url='{/literal}{$site_url}{literal}';
		if(filterBy =='date_range'){
			if($('#start_date').val() == ''){
				//alert("Please Enter Start Date");
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Start Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				return false;
			}else if($('#end_date').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter End Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter End Date");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'date_range';	
			}
		}else if(filterBy =='day'){
			if($('#select_day').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Date");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'day';
			}	
		}else if(filterBy == 'PaymentStatus'){			
			if($('#PaymentStatus').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please select any option</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please select any option");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'PaymentStatus';
			}
		}else if(filterBy == 'firstName'){
			if($('#firstName').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter First Name</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Order Number");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'firstName	';
			}
		}else if(filterBy == 'total_amount'){			
			if($('#total').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Sub Total</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Transation Amount");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'total_amount';
			}
		}
		document.getElementById('frmsearchdate').submit();
	}
	
	function advanceSearch(key){
		//alert(key);
		$(".searchboxes").css("display","none");
		if(key == 'searchByDate'){
			document.getElementById('searchByDateDiv').style.display='block';
		}else if(key == 'searchByDay'){
			document.getElementById('searchByDayDiv').style.display='block';
		}else if(key == 'searchByPaymentStatus'){
			document.getElementById('searchByPaymentStatusDiv').style.display='block';
		}else if(key == 'searchByTransactionId'){
			document.getElementById('searchByTransactionId').style.display='block';
		}else if(key == 'searchByGrandTotal'){
			document.getElementById('searchByGrandTotal').style.display='block';
		}		
	}
	var filterBy1 = '{/literal}{$filterBy}{literal}';
	if(filterBy1 !=''){		
	if(filterBy1 == 'searchByDate'){
		document.getElementById('searchByDateDiv').style.display='block';
	}else if(filterBy1 == 'searchByDay'){
		document.getElementById('searchByDayDiv').style.display='block';
	}else if(filterBy1 == 'searchByPaymentStatus'){
		document.getElementById('searchByPaymentStatusDiv').style.display='block';
	}else if(filterBy1 == 'searchByTransactionId'){
		document.getElementById('searchByTransactionId').style.display='block';
	}else if(filterBy1 == 'searchByGrandTotal'){
		document.getElementById('searchByGrandTotal').style.display='block';
	}
	}
</script>
{/literal} 