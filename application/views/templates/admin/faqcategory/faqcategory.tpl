<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}faqcategory/faqcategorylist">Faq Category</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Faq Category{else}Edit Faq Category{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Faq Category
			{else}
                        Edit Faq Category
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iFaqcategoryid" id="iFaqcategoryid" value="{if $operation neq 'add'}{$data->iFaqcategoryid}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Category Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vCategoryName" name="Data[vCategoryName]" class="inputbox" value="{if $operation neq 'add'}{$data->vCategoryName}{/if}" lang="*" title="Category Name" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Faq Category" class="submit_btn" title="Add Faq Category" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Faq Category" class="submit_btn" title="Edit Faq Category" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}faqcategory/faqcategorylist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

{/literal} 
