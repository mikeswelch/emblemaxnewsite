{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}designcategory/designcategorylist"> Clipart Category</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Clipart Category{else}Edit Clipart Category{/if}</li>
		</ul>
	</div>
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg" style="width:100%;float:left;"> {if $operation eq 'add'}
		<div class="pagetitle">Add Clipart Category</div>
		{else}
		<div class="pagetitle">Edit Clipart Category</div>
		{/if}
		
		<div class="add_ad_contentbox">
			<input type="hidden" name="Data[iDesignCategoryId]" id="iDesignCategoryId" value="{if $operation neq 'add'}{$Data->iDesignCategoryId}{/if}" />
			<input type="hidden" name="action" id="action" value="add" />
			{if $operation eq 'add'}
			<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="en" />
			{else}
			<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="{$lang}" />
			{/if}
			
				{if $operation eq 'add'}	
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star"></span>Category Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iDesignParentCategoryId]" onchange="checkparent(this.value)">
	
						<option value=''>--Select Category Name--</option>
						{section name=i loop=$parentarray}
						<option value='{$parentarray[i]['iDesignCategoryId']}' {if $operation neq 'add'}{if $Data->iDesignParentCategoryId eq $parentarray[i]['iDesignCategoryId']}selected{/if}{/if}>{$parentarray[i]['vTitle']}</option>
						{/section}
					</select>
				</div>
				{/if}
				{if $operation neq 'add'}
				<div class="inputboxes" id="">
					
					<label for="textfield"><span class="red_star">*</span>Category Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iDesignParentCategoryId]" onchange="checkparent(this.value)">
	
						<option value=''>--Select Category Design Name--</option>
						{section name=i loop=$parentarray}
						<option value='{$parentarray[i]['iDesignCategoryId']}' {if $operation neq 'add'}{if $Data->iDesignParentCategoryId eq $parentarray[i]['iDesignCategoryId']}selected{/if}{/if}>{$parentarray[i]['vTitle']}</option>
						{/section}
					</select>
				</div>
				{/if}
				
				<div class="inputboxes" >
					<label for="textfield" id="menuboxid" style="display:block;"><span class="red_star">*</span>Parent Category Name</label>
					<label for="textfield" id="submenuboxid" style="display:none;"><span class="red_star">*</span>Sub-Category Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" lang="*" title="Parent Design Category" value="{if $operation neq 'add'}{$data->vTitle}{/if}" maxlength="30" class="inputbox" name="data[vTitle]" >
				</div>
				
					<div class="inputboxes">
				        <label for="textarea"><span class="red_star">*</span></span> Order</label>
						<span class="collan_dot">:</span>
						<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						{if $operation eq 'add'}
						<option value=''>--Select Order--</option>
						{while ($totalRec+1) >= $initOrder}
						
						<option value="{$initOrder}" >{$initOrder++}</option>
						
						{/while}
						{else}
						{$initOrder|@print_r}
						<option value=''>--Select Order--</option>
						{while ($totalRec) >= $initOrder}	
						<option value="{$initOrder}"  {if $Data->iOrderNo eq $initOrder}selected{/if}>{$initOrder++}</option>
						{/while}
						{/if}
					</select>
				        
				</div>

				<div class="inputboxes" id="">
				<label for="textfield">Description</label>
				<span class="collan_dot">:</span>
				<textarea id="tDescription" class="inputbox" name="data[tDescription]" title="tDescription" value="">{if $operation neq 'add'}{$data->tDescription}{/if}</textarea>
				</div>					
			
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $Data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $Data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn"> {if $operation eq 'add'}
					<input type="submit" value="Add Clipart Category" class="submit_btn" title="Add Clipart Category" onclick="return validate(document.frmadd);"/>
					{else}
					<input type="submit" value="Edit Clipart Category" class="submit_btn" title="Edit Clipart Category" onclick="return validate(document.frmadd);"/>
					{/if}
					<a href="{$admin_url}designcategory/designcategorylist" class="cancel_btn">Cancel </a>
				</div>
				<div class="clear"></div>
		</div>
	</div>

	<div class="clear"></div>
	</form>
</div>
{include file="admin/footer.tpl"}

{literal}
<script type="text/javascript">

function checkparent(id){

	if(id == '0'){
		$('#menuboxid').css({display: "block"});
		$('#submenuboxid').css({display: "none"});
		
		$('#iconbox').css({display: "block"});
		$('#urlbox').css({display: "none"});
	}else{
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
var operation = '{/literal}{$operation}{literal}';

if( operation == 'edit'){
	var iParentMenuId = '{/literal}{$data->iDesignParentCategoryId}{literal}';
	if( iParentMenuId != 0){
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
</script>
{/literal}
