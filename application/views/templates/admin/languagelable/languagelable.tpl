
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}languagelable/langlablelist">Language Lable</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Language Lable{else}Edit Language Lable{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Language Lable
			{else}
			Edit Language Lable
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iLabelId" id="iLabelId" value="{if $operation neq 'add'}{$data->iLabelId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				

				<div class="inputboxes">
					<label for="textfield" style="width:250px"><span class="red_star">*</span> Lable Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vName" name="Data[vName]" class="inputbox" value="{if $operation neq 'add'}{$data->vName}{/if}" lang="*" title="Lable Name" style="width:250px"/>
				</div>
				{if $language_data|@count gt 0}
				{section name=i loop=$language_data}
				<div class="inputboxes">
					<label for="textarea" style="width:250px"><span class="red_star">*</span> Value [<span style="color:red;">{$language_data[i]->vLanguage}</span>]</label>
					<span class="collan_dot">:</span>
					<!--<input type="text" id="vValue_{$language_data[i]->vLangCode}" name="Data[vValue_{$language_data[i]->vLangCode}]" class="inputbox" value="{if $operation neq 'add'}{$data->vValue_{$language_data[i]->vLangCode}}{/if}"  title="{$language_data[i]->vLanguage}" style="width:250px" lang="*"/>-->
				        <textarea value="" class="inputbox" title="Address" name="Data[vValue_{$language_data[i]->vLangCode}]" id="vValue_{$language_data[i]->vLangCode}" style="width:292px;height:83px;" lang="*">{if $operation neq 'add'}{$data->vValue_{$language_data[i]->vLangCode}}{/if}</textarea>
				</div>
				{/section}
				{/if}
				<!--<div class="inputboxes">
				        <label for="textarea"> Address</label>
						<span class="collan_dot">:</span>
				        <textarea value="" class="inputbox" title="Address" name="Data[vValue_{$language_data[i]->vLangCode}]" id="vValue_{$language_data[i]->vLangCode}" style="width:250px" lang="*">{if $operation neq 'add'}{$data->vValue_{$language_data[i]->vLangCode}}{/if}</textarea>
				</div>-->					
				<!--<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Lable Value_en</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vValue_en" name="Data[vValue_en]" class="inputbox" maxlength="2" value="{if $operation neq 'add'}{$data->vValue_en}{/if}"  title="Lable Value_en" style="width:250px"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Lable Value_da</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vValue_da" name="Data[vValue_da]" class="inputbox" maxlength="2" value="{if $operation neq 'add'}{$data->vValue_da}{/if}"  title="Lable Value_da" style="width:250px"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Lable Value_pa</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vValue_pa" name="Data[vValue_pa]" class="inputbox" maxlength="2" value="{if $operation neq 'add'}{$data->vValue_pa}{/if}"  title="Lable Value_da" style="width:250px"/>
				</div>-->
				<!--<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>-->
				<div class="add_can_btn" style="margin-left: 286px;">
						{if $operation eq 'add'}
							<input type="submit" value="Add Language Lable" class="submit_btn" title="Add Language Lable" onclick="return validate(document.frmadd);" style="width:170px"/>
						{else}
							<input type="submit" value="Edit Language Lable" class="submit_btn" title="Edit Language Lable" onclick="return validate(document.frmadd);" style="width:170px"/>
						{/if}
						<a href="{$admin_url}languagelable/languagelablelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
	</script>
{/literal} 
