<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}size/sizelist">Quantity Range</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Quantity Range{else}Edit Quantity Range{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Quantity Range
			{else}
                        Edit Quantity Range
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iQuantityRangeId" id="iQuantityRangeId" value="{if $operation neq 'add'}{$data->iQuantityRangeId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Quantity Range Label</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vQuantityRange" name="Data[vQuantityRange]" class="inputbox" value="{if $operation neq 'add'}{$data->vQuantityRange}{/if}" lang="*" title="Quantity Range Label" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Start Range</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iStartRange" name="Data[iStartRange]" class="inputbox" value="{if $operation neq 'add'}{$data->iStartRange}{/if}" lang="*" title="Start Range" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>End Range</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iEndRange" name="Data[iEndRange]" class="inputbox" value="{if $operation neq 'add'}{$data->iEndRange}{/if}" lang="*" title="End Range" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Show Field</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iShowField" name="Data[iShowField]" class="inputbox" value="{if $operation neq 'add'}{$data->iShowField}{/if}" lang="*" title="Show Field" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Size" class="submit_btn" title="Add Size" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Size" class="submit_btn" title="Edit Size" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}quantityrange/quantityrangelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

{/literal} 
