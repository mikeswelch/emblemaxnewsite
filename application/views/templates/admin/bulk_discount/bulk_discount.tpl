<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}garment/garmentlist">Size</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Size{else}Edit Size{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Bulk Discount
			{else}
                        Edit Bulk Discount
			{/if} </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBulkDiscountId" id="iBulkDiscountId" value="{if $operation neq 'add'}{$data->iBulkDiscountId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Quantity</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iQuantity" name="Data[iQuantity]" class="inputbox" value="{if $operation neq 'add'}{$data->iQuantity}{/if}" lang="*" title="Quantity" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Percentage</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fPercentage" name="Data[fPercentage]" class="inputbox" value="{if $operation neq 'add'}{$data->fPercentage}{/if}" lang="*" title="Percentage" onkeypress="return checkprise(event)"/>
				</div>	
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Bulk Discount" class="submit_btn" title="Add Bulk Discount" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Bulk Discount" class="submit_btn" title="Edit Bulk Discount" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}bulk_discount/bulk_discountlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}

{/literal} 
