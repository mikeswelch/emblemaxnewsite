<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}zipcode/zipcodelist">Delivery ZipCode</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add Delivery ZipCode{else}Edit Delivery ZipCode{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Delivery Zipcode
			{else}
			Edit Delivery Zipcode			
			{/if}
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iZipcodeId" id="iZipcodeId" value="{if $operation neq 'add'}{$data->iZipcodeId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Delivery ZipCode</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iZipcode" name="Data[iZipcode]" class="inputbox" value="{if $operation neq 'add'}{$data->iZipcode}{/if}" lang="*" title="Delivery Zipcode" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Price</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fPrice" name="Data[fPrice]" class="inputbox" value="{if $operation neq 'add'}{$data->fPrice}{/if}" lang="*" title="Price" onkeypress="return checkprise(event)" />
				</div>				
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active"  {if $data->eStatus eq Active}selected{/if}>Active</option>
						<option value="Inactive"{if $data->eStatus eq Inactive}selected{/if} >Inactive</option>
					</select>
				</div>

				<div class="add_can_btn">
						{if $operation eq 'add'}
							<input type="submit" value="Add Delivery Zipcode" class="submit_btn" title="Add Delivery Zipcode" onclick="return validate(document.frmadd);"/>
						{else}
							<input type="submit" value="Edit Delivery Zipcode" class="submit_btn" title="Edit Delivery Zipcode" onclick="return validate(document.frmadd);"/>
						{/if}
						<a href="{$admin_url}zipcode/zipcodelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}