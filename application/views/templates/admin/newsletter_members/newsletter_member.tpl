<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}newslettermembers/newslettermemberslist">Newsletter Members</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Send Newsletter{else}Send Newsletter{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Send Newsletter
			{else}
			Send Newsletter		
			{/if}
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Email</label>
					<span class="collan_dot">:</span>
					<select id="iNewsletterId" name="Data[iNewsletterId]"  title="Email Address" lang="*" style="width:250px; margin-left: 3px;" onchange="getStates(this.value);">
						<option value=''>-- Select Email Address --</option>
						
						{section name=i loop=$newsletter_email}
						<option value='{$newsletter_email[i]->iNewsletterId}' >{$newsletter_email[i]->vEmail}</option>
						{/section}
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Message</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tMessage" name="Data[tMessage]" class="inputbox" title="Message"></textarea></div>
				</div>			

				<div class="add_can_btn">
						<input type="submit" value="Send Mail" class="submit_btn" title="Send Mail" onclick="return validate(document.frmadd);"/>
						<a href="{$admin_url}newslettermembers/newslettermemberslist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"} 
