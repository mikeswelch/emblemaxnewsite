{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Request Catalog</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Request Catalog</div>
		{if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />{$var_msg}</p>
		</div>
		<div></div>
		{/if}
		<div class="admin_top_part">
			<!--<div class="addnew_btn"> <a href="{$admin_url}requestcatalog/add" style="text-decoration:none;">Add New				
				</a> </div>-->
			<form name="frmsearch" id="frmsearch" method="post" action="{$admin_url}requestcatalog/requestcataloglist">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<div class="search_top_admini">
					<label>Search:</label>
					<div id="newstatus" class="statusleft">
						{if $keyword == 'Read' || $keyword == 'Unread'}
							<select name="keyword" id="keyword">
								<option value="Read"{if $keyword != ''}{if $keyword eq 'Read'}selected="selected"{/if}{/if}>Read</option>
								<option value="Unread"{if $keyword != ''}{if $keyword eq 'Unread'}selected="selected"{/if}{/if}>Unread</option>
							</select>
						{else}
							<input type="Text" id="keyword" name="keyword" value="{if $keyword != ''}{$keyword}{/if}"  class="search_input" />
						{/if}
					</div>
					<select name="option" id="option" onchange="estatusdd(this.value);">
						<option value="vName"{if $option eq 'vName'}selected{/if}>Name</option>
						<option value="vEmail"{if $option eq 'vEmail'}selected{/if}>Email</option>
						<option value="eStatus"{if $option eq 'eStatus'}selected{/if}>Status</option>
					</select>
					<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
				</div>
				</form>
			<div class="clear"></div>
			<div class="action_apply_btn">
				<select class="act_sl_box" name="newaction" id="newaction">
					<option value="">Select Action</option>
					<!--option value="Read"{if $action == 'Read'}selected="selected"{/if}>Make Read</option>
					<option value="Unread"{if $action == 'Unread'}selected="selected"{/if}>Make Unread</option>
					<option value="Deletes"{if $action == 'Deletes'}selected="selected"{/if}>Make Delete</option-->
					<option value="Show All"{if $action == 'Show All'}selected="selected"{/if}>Show All</option>
				</select>
				<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'requestcataloglist',document.frmlist,'requestcatalog');"/>
			</div>
			<div class="pagination"> {$AlphaBox} </div>
			<div class="clear"></div>
		</div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}requestcatalog/search_action" method="post">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="commonId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><a href={$admin_url}requestcatalog/requestcataloglist?field=vName&order={$order}><span style="color:white;">Name</span>{if $field eq 'vName'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th><a href={$admin_url}requestcatalog/requestcataloglist?field=vEmail&order={$order}><span style="color:white;">Email</span> {if $field eq 'vEmail'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="60px"><a href={$admin_url}requestcatalog/requestcataloglist?field=vCompany&order={$order}><span style="color:white;">Company</span> {if $field eq 'vCompany'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="60px"><a href={$admin_url}requestcatalog/requestcataloglist?field=vPhone&order={$order}><span style="color:white;">Phone</span> {if $field eq 'vPhone'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="60px"><a href={$admin_url}requestcatalog/requestcataloglist?field=vFax&order={$order}><span style="color:white;">Fax</span> {if $field eq 'vFax'} {if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							
							<!--th width="90px">Action</th-->
							
						</tr>
					</thead>
					{if $data|@count gt 0}
					{section name=i loop=$data}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}">
						<tr>
							<td><input name="iRequestCatalogId[]" type="checkbox" id="iId" value="{$data[i]->iRequestCatalogId}"></td>
							<td><a href="{$admin_url}requestcatalog/edit?iRequestCatalogId={$data[i]->iRequestCatalogId}">{$data[i]->vName}</a></td>
							<td>{$data[i]->vEmail}</td>
							<td>{$data[i]->vCompany}</td>
							<td>{$data[i]->vPhone}</td>
							<td>{$data[i]->vFax}</td>
							
							<!--td><a href="{$admin_url}requestcatalog/edit?iRequestCatalogId={$data[i]->iRequestCatalogId}"><img src="{$admin_image_path}icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iRequestCatalogId}','Read','iRequestCatalogId');"><img src="{$admin_image_path}icon_approve.png" alt="Read" title="Read"></a>
							<a href="javascript:void(0);" onclick="MakeAction('{$data[i]->iRequestCatalogId}','Unread','iRequestCatalogId');"><img src="{$admin_image_path}icon_unapprove.png" alt="Unread" title="Unread"></a>
							<a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon({$data[i]->iRequestCatalogId},'requestcatalog/delete','{$ssql}')"></a> </td-->	
						</tr>
					</tbody>
					{/section}
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					{/if}
				</table>
			</form>
		</div>
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found" width="33%" align="left">{$recmsg}</td>
					<td width="33%"><div class="bottom_admin_paging"> {$page_link} </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script >
function estatusdd(val1){
	var keyword=$('#keyword').val();
	if(keyword =='Read' || keyword =='Unread')
	{
		keyword='';
	}
	if(val1 == 'eStatus')
	{
		$('#newstatus').html('<select name="keyword" id="keyword"><option value="Read"{if $keyword != ""}{if $keyword eq "Read"}selected="selected"{/if}{/if}>Read</option><option value="Unread"{if $keyword != ""}{if $keyword eq "Unread"}selected="selected"{/if}{/if}>Unread</option></select>');
	}
	else{
		$('#newstatus').html('<input type="Text" id="keyword" name="keyword" value="'+keyword+'"  class="search_input" />');
	}
}
</script>
{/literal}