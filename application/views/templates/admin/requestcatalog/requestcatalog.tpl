<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}requestcatalog/requestcataloglist">Request Catalog</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Request Catalog{else}{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Request Catalog
			{else}
			Request Catalog		
			{/if}
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
            <div class="pagetitle">Personal Information</div>
                <div class="add_ad_contentbox">
                    <div class="inputboxes">
                            <label for="textarea"><span class="red_star"></span> Name</label>
                                    <span class="collan_dot">:</span>
                                    <label>{$data[0]->vName|@ucfirst}</label>
                            <label for="textarea"><span class="red_star"></span> Email Address </label>
                                    <span class="collan_dot">:</span>
                                    <label>{$data[0]->vEmail}</label>
                            <label for="textarea"><span class="red_star"></span> Contact No. </label>
                                    <span class="collan_dot">:</span>
                                    <label>{$data[0]->vPhone}</label>
                            
                            {if $data[0]->vFax neq ''}
                            <label for="textarea"><span class="red_star"></span> Fax </label>
                                    <span class="collan_dot">:</span>
                                    <label>{$data[0]->vFax}</label>
                            {/if}
                    </div>
                </div>
	</div>
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
            <div class="pagetitle">Address</div>
                <div class="add_ad_contentbox">
                    <div class="inputboxes">
                            <label for="textarea"><span class="red_star"></span>Company Name</label>
                                    <span class="collan_dot">:</span>
                                    <label>{$data[0]->vCompany|@ucfirst}</label>
                            <label for="textarea"><span class="red_star"></span> Address </label>
                                    <span class="collan_dot">:</span>
                                    <table>
                                        <tr>
                                            <td>
                                                <label style="font-size: 14px;">{$data[0]->vAddress|@ucfirst}</label>
                                            </td>
                                        </tr>
                                        {if $data[0]->vAddress1 neq ''}
                                        <tr>
                                            <td>
                                                    <label style="font-size: 14px;">{$data[0]->vAddress1|@ucfirst}</label>
                                            </td>
                                        </tr>
                                        {/if}
                                        <tr>
                                            <td>
                                            <label style="font-size: 14px;">{$data[0]->vCity|@ucfirst}</label>
                                                {section name=i loop=$state}
                                                        {if $state[i]->iStateId eq $data[0]->iStateId} <label style="font-size: 14px;">{$state[i]->vState}</label> {/if}
                                                {/section}
                                            </td>
                                        </tr>
                                       <tr>
                                        <td>
                                            {section name=j loop=$country}
                                                         {if $country[j]->iCountryId eq $data[0]->iCountryId} <label style="font-size: 14px;">{$country[j]->vCountry}</label> {/if}
                                            {/section}
                                            <label style="font-size: 14px;">{$data[0]->vZip}</label>
                                        </td>
                                        </tr>
                                    </table>
                    </div>
                </div>
	</div>

	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
            <div class="pagetitle">Comment</div>
                <div class="add_ad_contentbox">
                    <div class="inputboxes">
                            <label for="textarea"><span class="red_star"></span> Comment</label>
                                    <span class="collan_dot">:</span>
                                    <label >{$data[0]->tNote|@ucfirst}</label>
                    </div>
                </div>
	</div>
	</div>
	<div class="clear"></div>

        <div style="margin-left: 52px;margin-top: 20px;">
          <a href="{$admin_url}requestcatalog/requestcataloglist"><input type="button" value="Back" class="submit_btn" title="Back" onclick="return validate(document.frmadd);" /></a>     
        </div>       

</div>