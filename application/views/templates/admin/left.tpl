{$ssql = ''}
<div class="leftpart">
<div class="left_navbar">
	<ul id="nav">
         
		<li>
			<a href="{$admin_url}dashboard" class="collapsed heading leftnone">Dashboard</a>
			
			
    		</li>
        
		<li> <a class="collapsed heading">Master Module</a>
			{if $filename eq 'administrator' || $filename eq 'country' || $filename eq 'state' || $filename eq 'language' || $filename eq 'languagelable' || $filename eq 'loginhistory' || $filename eq 'zipcode'}
			<ul class="navigation" style="display:block !important;">
				<li class="heading"><a href="{$admin_url}administrator/adminlist" {if $filename eq 'administrator'}class="active_tab"{/if}>Administrator</a></li>
				<li class="heading"><a href="{$admin_url}country/countrylist" {if $filename eq 'country'}class="active_tab"{/if}>Country</a></li>
				<li class="heading"><a href="{$admin_url}state/statelist" {if $filename eq 'state'}class="active_tab"{/if}>State</a></li>
				<!--<li class="heading"><a href="{$admin_url}language/languagelist" {if $filename eq 'language'}class="active_tab"{/if}>Language</a></li>
				<li class="heading"><a href="{$admin_url}languagelable/languagelablelist" {if $filename eq 'languagelable'}class="active_tab"{/if}>Language Lable</a></li>-->
				<li class="heading"><a href="{$admin_url}loginhistory/loginhistorylist" {if $filename eq 'loginhistory'}class="active_tab"{/if}>Login History</a></li>
				<li class="heading"><a href="{$admin_url}zipcode/zipcodelist" {if $filename eq 'zipcode'}class="active_tab"{/if}>Delivery Zipcode</a></li>
			</ul>
			{else}
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}administrator/adminlist">Administrator</a></li>
				<li class="heading"><a href="{$admin_url}country/countrylist">Country</a></li>
				<li class="heading"><a href="{$admin_url}state/statelist">State</a></li>
				<!--<li class="heading"><a href="{$admin_url}language/languagelist">Language</a></li>-->
				<!--<li class="heading"><a href="{$admin_url}languagelable/languagelablelist">Language Lable</a></li>-->
				<li class="heading"><a href="{$admin_url}loginhistory/loginhistorylist">Login History</a></li>
				<li class="heading"><a href="{$admin_url}zipcode/zipcodelist">Delivery ZipCode</a></li>
			</ul>
			{/if}
		</li>
		
		<li> <a class="collapsed heading">Users</a>
			{if $filename eq 'user' || $filename eq 'coupon'}
			<ul class="navigation">
				<li class="heading"><a href="{$admin_url}user/userlist" {if $filename eq 'user'}class="active_tab"{/if}>User</a></li>
				<li class="heading"><a href="{$admin_url}coupon/couponlist" {if $filename eq 'coupon'}class="active_tab"{/if}>Coupon</a></li>
			</ul>
			{else}
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}user/userlist">User</a></li>
				<li class="heading"><a href="{$admin_url}coupon/couponlist">Coupon</a></li>
			</ul>
			{/if}
		</li>
		
		<li> <a class="collapsed heading">Product template</a>
			{if $filename eq 'productcategory' || $filename eq 'product' || $filename eq 'fabrics' || $filename eq 'brand' || $filename eq 'occupations'}
			<ul class="navigation">
				<li class="heading"><a href="{$admin_url}productcategory/productcategorylist" {if $filename eq 'productcategory'}class="active_tab"{/if}>Product Category</a></li>
				<li class="heading"><a href="{$admin_url}product/productlist" {if $filename eq 'product'}class="active_tab"{/if}>Product Templates</a></li>
				<li class="heading"><a href="{$admin_url}fabrics/fabricslist" {if $filename eq 'fabrics'}class="active_tab"{/if}>Fabrics</a></li>
				<li class="heading"><a href="{$admin_url}brand/brandlist" {if $filename eq 'brand'}class="active_tab"{/if}>Brand</a></li>
				<li class="heading"><a href="{$admin_url}occupations/occupationslist" {if $filename eq 'occupations'}class="active_tab"{/if}>Occupations</a></li>
				
			</ul>
			{else}
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}productcategory/productcategorylist">Product Category</a></li>
				<li class="heading"><a href="{$admin_url}product/productlist">Product Templates</a></li>
				<li class="heading"><a href="{$admin_url}fabrics/fabricslist">Fabrics</a></li>
				<li class="heading"><a href="{$admin_url}brand/brandlist">Brand</a></li>
				<li class="heading"><a href="{$admin_url}occupations/occupationslist">Occupations</a></li>
			</ul>
			{/if}
		</li>
		
		<li> <a class="collapsed heading">Orders</a>
			{if $filename eq 'order'}
			<ul class="navigation">
				<li class="heading"><a href="{$admin_url}order" {if $filename eq 'order'}class="active_tab"{/if}>Orders</a></li>				
			</ul>
			{else}
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}order">Orders</a></li>			
			</ul>
			{/if}
		</li>
		<li> <a class="collapsed heading">Design Manager</a>
			{if $filename eq 'designcategory' || $filename eq 'design_images' || $filename eq 'size' || $filename eq 'color' || $filename eq 'printcolor' || $filename eq 'printlocation' || $filename eq 'quantityrange'}
		        <ul class="navigation">
				<li class="heading"><a href="{$admin_url}designcategory/designcategorylist" {if $filename eq 'designcategory'}class="active_tab"{/if}>Clipart Category</a></li>
				<li class="heading"><a href="{$admin_url}design_images/design_imageslist" {if $filename eq 'design_images'}class="active_tab"{/if}>Clipart</a></li>
				<li class="heading"><a href="{$admin_url}printlocation/printlocationlist" {if $filename eq 'printlocation'}class="active_tab"{/if}>Print Location</a></li>
				<li class="heading"><a href="{$admin_url}printcolor/printcolorlist" {if $filename eq 'printcolor'}class="active_tab"{/if}>Print Color</a></li>
				<li class="heading"><a href="{$admin_url}size/sizelist" {if $filename eq 'size'}class="active_tab"{/if}>Size</a></li>
				<li class="heading"><a href="{$admin_url}color/colorlist" {if $filename eq 'color'}class="active_tab"{/if}>Color</a></li>
				<li class="heading"><a href="{$admin_url}quantityrange/quantityrangelist" {if $filename eq 'quantityrange'}class="active_tab"{/if}>Quantity Range</a></li>
			</ul>
			{else}
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}designcategory/designcategorylist">Clipart Category</a></li>
				<li class="heading"><a href="{$admin_url}design_images/design_imageslist">Clipart</a></li>
				<li class="heading"><a href="{$admin_url}printlocation/printlocationlist">Print Location</a></li>
				<li class="heading"><a href="{$admin_url}printcolor/printcolorlist">Print Color</a></li>
				<li class="heading"><a href="{$admin_url}size/sizelist">Size</a></li>
				<li class="heading"><a href="{$admin_url}color/colorlist">Color</a></li>
				<li class="heading"><a href="{$admin_url}quantityrange/quantityrangelist">Quantity Range</a></li>
				
			</ul>
			{/if}
		</li>
		
		<li> <a class="collapsed heading">Shipping</a>
		{if $filename eq 'shipping' || $filename eq 'turnaroundtime'}
			<ul class="navigation">
				<li class="heading"><a href="{$admin_url}shipping/shippinglist" {if $filename eq 'shipping'}class="active_tab"{/if}>Shipping Methods</a></li>
				<li class="heading"><a href="{$admin_url}turnaroundtime/turnaroundtimelist" {if $filename eq 'turnaroundtime'}class="active_tab"{/if}>Turn Around Time</a></li>
			</ul>
		{else}
		        <ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}shipping/shippinglist">Shipping Methods</a></li>
				<li class="heading"><a href="{$admin_url}turnaroundtime/turnaroundtimelist" {if $filename eq 'turnaroundtime'}class="active_tab"{/if}>Turn Around Time</a></li>
			</ul>
		{/if}
		</li>
		<li> <a class="collapsed heading">Settings</a>
			{if $filename eq 'configuration' || $filename eq 'newslettermembers' || $filename eq 'newsletter_format' || $filename eq 'bannergroup' || $filename eq 'customerreview'|| $filename eq 'bannergroupimage' || $filename eq 'systememail' || $filename eq 'staticpages' || $filename eq 'banner' || $filename eq 'faq' || $filename eq 'help' || $filename eq 'contactus' || $filename eq 'homeslider' || $filename eq 'faq' || $filename eq 'faqcategory' || $filename eq 'requestcatalog'}
			<ul class="navigation">
				<li class="heading"><a href="{$admin_url}configuration/loadconfiguration" {if $filename eq 'configuration'}class="active_tab"{/if}>General Configration</a></li>
				<li class="heading"><a href="{$admin_url}systememail/systememaillist" {if $filename eq 'systememail'}class="active_tab"{/if}>System Emails</a></li>
				<li class="heading"><a href="{$admin_url}staticpages/staticpageslist" {if $filename eq 'staticpages'}class="active_tab"{/if}>Static Pages</a></li>
				<li class="heading"><a href="{$admin_url}bannergroup/bannergrouplist" {if $filename eq 'bannergroup'}class="active_tab"{/if}>Banner Groups</a></li>
				<li class="heading"><a href="{$admin_url}banner/bannerlist" {if $filename eq 'banner'}class="active_tab"{/if}>Banner</a></li>
				<li class="heading"><a href="{$admin_url}homeslider/homesliderlist" {if $filename eq 'homeslider'}class="active_tab"{/if}>Home Slider</a></li>
				<li class="heading"><a href="{$admin_url}customer_review/customerreviewlist" {if $filename eq 'customerreview'}class="active_tab"{/if}>Customer Review</a></li>
				<li class="heading"><a href="{$admin_url}faqcategory/faqcategorylist" {if $filename eq 'faqcategory'}class="active_tab"{/if}>Faq Categories</a></li>
				<li class="heading"><a href="{$admin_url}faq/faqlist" {if $filename eq 'faq'}class="active_tab"{/if}>Faqs</a></li>
				<li class="heading"><a href="{$admin_url}newslettermembers/newslettermemberslist" {if $filename eq 'newslettermembers'}class="active_tab"{/if}>Newsletter Members</a></li>
				<li class="heading"><a href="{$admin_url}newsletter_format/newsletter_formatlist"{if $filename eq 'newsletter_format'}class="active_tab"{/if}>NewsLetter Formate</a></li>
				<!--<li class="heading"><a href="{$admin_url}help/helplist" {if $filename eq 'help'}class="active_tab"{/if}>Help</a></li>-->
				<li class="heading"><a href="{$admin_url}homepage_blocks/homepage_blockslist" {if $filename eq 'homepage_blocks'}class="active_tab"{/if}>Homepage Blocks</a></li>
				<li class="heading"><a href="{$admin_url}contactus/contactuslist" {if $filename eq 'contactus'}class="active_tab"{/if}>Contact Us</a></li>
				<li class="heading"><a href="{$admin_url}requestcatalog/requestcataloglist" {if $filename eq 'requestcatalog'}class="active_tab"{/if}>Request Catalog</a></li>
			</ul>
			{else}
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="{$admin_url}configuration/loadconfiguration">General Configration</a></li>
				<li class="heading"><a href="{$admin_url}systememail/systememaillist">System Emails</a></li>
				<li class="heading"><a href="{$admin_url}staticpages/staticpageslist">Static Pages</a></li>
				<li class="heading"><a href="{$admin_url}bannergroup/bannergrouplist">Banner Groups</a></li>
				<li class="heading"><a href="{$admin_url}banner/bannerlist">Banner</a></li>
				<li class="heading"><a href="{$admin_url}homeslider/homesliderlist">Home Slider</a></li>
				<li class="heading"><a href="{$admin_url}customer_review/customerreviewlist">Customer Review</a></li>
				<li class="heading"><a href="{$admin_url}faqcategory/faqcategorylist">Faq Categories</a></li>
				<li class="heading"><a href="{$admin_url}faq/faqlist">Faqs</a></li>
				<li class="heading"><a href="{$admin_url}newslettermembers/newslettermemberslist">Newsletter Members</a></li>
				<li class="heading"><a href="{$admin_url}newsletter_format/newsletter_formatlist">NewsLetter Formate</a></li>
				<!--<li class="heading"><a href="{$admin_url}help/helplist">Help</a></li>-->
				<li class="heading"><a href="{$admin_url}homepage_blocks/homepage_blockslist">Homepage Blocks</a></li>
				<li class="heading"><a href="{$admin_url}contactus/contactuslist">Contact Us</a></li>
				<li class="heading"><a href="{$admin_url}requestcatalog/requestcataloglist" {if $filename eq 'requestcatalog'}class="active_tab"{/if}>Request Catalog</a></li>
			</ul>
			{/if}
		</li>
        
	</ul>
    
</div>
</div>
