<link href="{$admin_css_path}style.css" rel="stylesheet" type="text/css" /> 

	
	<div class="centerpartbg">
		<div class="pagetitle_tab">
			<ul>
				<li ><a href="{$admin_url}client/edit?iBusinessUserId={$iBusinessUserId}">Edit Client</a></li>
				<li ><a href="{$admin_url}projectbrief/projectbrieflist?iBusinessUserId={$iBusinessUserId}">Project Brief</a></li>
				<li ><a href="#">Transaction</a></li>
			</ul>
		</div>
		<div class="invoice_page">
			<div class="invoice_de_box">
				<div class="invo_de_title">Invoice Information</div>
				<div class="invoice_de_cont">
					<table cellpadding="0" cellspacing="1" width="100%">
						<tr>
							<td class="in_de_title">Invoice Number</td>
							<td><input type="text" value="{$invoicedata[0]->vInvoiceNumber}" /></td>
						</tr>
						<tr>
							<td class="in_de_title">Invoice Date</td>
							<td><input type="text" value="{$invoicedata[0]->dTransactionDate}" /></td>
						</tr>
						
						<tr>
							<td class="in_de_title">Amount</td>
							<td><input type="text" value="{$invoicedata[0]->fGrandTotal}" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="client_info_box">
				<div class="invo_de_title">Client Information</div>
			<div class="in_ad_box">
				<!-- http://192.168.1.12/talentsrent/uploads/event/4/susanzevonart.jpg-->
				<div class="logo_de_sub">
				{if $invoicedata[0]->vImage neq ''}
				<img height="90px" width="140px" src="{$upload_path}client/{$invoicedata[0]->iBusinessUserId}/{$invoicedata[0]->vImage}"/>
				{else}
				<img height="120px" width="150px" src="{$admin_image_path}/noimage.jpg"/>
				{/if}
				</div>
				<div class="in_ad_box_title">{$invoicedata[0]->vFirstName} {$invoicedata[0]->vLastName}</div>
				{$invoicedata[0]->vLocation} , <br />
				 Gujarat , India<br />
				<strong>Phone No :</strong> {$invoicedata[0]->vContactNumber}<br />
				<strong>Email :</strong> <a href="mailto:divyesh.gadhetharia@php2india.com">{$invoicedata[0]->vEmail}</a><br />
				<strong>Skype Name :</strong> <a href="#">{$invoicedata[0]->vSkypeName}</a> </div>
			</div>	
				
				
			<div class="clear"></div>
			<div class="mem_info">
				<div class="chap_title">Freelancer Information</div>
				<table width="100%" cellpadding="0" cellspacing="1">
					<tr>
						<th>Freelancer Name</th>
						<th>Member Date</th>
						
						<th width="100px">Amount</th>
					</tr>
					{if $freelancerdata|@count gt 0}
					{section name=i loop=$freelancerdata}
					<tbody class="admin_antry_table_sec">
						<tr>
							<td>{$freelancerdata[i]->vFirstName} {$freelancerdata[i]->vLastName}</td>
							<td>{$freelancerdata[i]->dAddedDate}</td>
							
							<td>{$freelancerdata[i]->fAmount}</td>
						</tr>
					</tbody>
					{/section}
					{/if}
					
				</table>
			</div>
			<div class="his_total">
				<table cellpadding="0" cellspacing="1" width="100%">  
					<tbody><tr>	
						<td class="right_totaltitle">SubTotal</td>
						<td width="100">${$invoicedata[0]->fSubTotal}</td>
					</tr>
					<tr>	
						<td class="right_totaltitle">Vat Price</td>
						<td>${$invoicedata[0]->fVatPrice}</td>
					</tr>
					<tr>	
						<td class="right_totaltitle"><strong>Grand Total</strong></td>
						<td><strong>${$invoicedata[0]->fGrandTotal}</strong></td>
					</tr>
				</tbody></table>
			</div>
			<div class="clear"></div>
			<div class="print_send_cl">
				<input type="button" value="Print" class="submit_btn" title="" onClick="window.print();"/>
				<a href="{$admin_url}transaction/transactionlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
		</div>
	</div>
</div>

{literal}
<script>

</script>
{/literal}