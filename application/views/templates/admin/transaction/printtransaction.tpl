<link href="{$admin_css_path}style.css" rel="stylesheet" type="text/css" />
<div class="invoice_page invoice_page_prient">
	<div class="invoice_de_box">
		<div class="invo_de_title">Order Information</div>
		<div class="invoice_de_cont">
			<table cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="in_de_title">Order Number</td>
					<td><input type="text" value="{$invoicedata[0]->vOrderNum}" /></td>
				</tr>
				<tr>
					<td class="in_de_title">Order Date</td>
					<td><input type="text" value="{$invoicedata[0]->dPaymentDate}" /></td>
				</tr>
				<tr>
					<td class="in_de_title">Shipping Charge</td>
					<td><input type="text" value="{$invoicedata[0]->vShippingCharge}" /></td>
				</tr>
				<tr>
					<td class="in_de_title">Express Charge</td>
					<td><input type="text" value="{$invoicedata[0]->vExpressCharge}" /></td>
				</tr>
				<tr>
					<td class="in_de_title">Amount</td>
					<td><input type="text" value="{$invoicedata[0]->fTotalAmt}" /></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="client_info_box">
		<div class="invo_de_title">User Information</div>
		<div class="in_ad_box">
				<!-- http://192.168.1.12/talentsrent/uploads/event/4/susanzevonart.jpg-->
				<div class="logo_sub">
				{if $invoicedata[0]->vPhoto neq ''}
				<img height="90px" width="140px" src="{$upload_path}user/{$invoicedata[0]->iUserId}/{$invoicedata[0]->vPhoto}"/>
				{else}
				<img height="120px" width="150px" src="{$admin_image_path}/noimage.jpg"/>
				{/if}
				</div>
				<div class="invice_clienttxt">
				<div class="in_ad_box_title">{$invoicedata[0]->vFirstName} {$invoicedata[0]->vLastName}</div>
				{$invoicedata[0]->tAddress} , <br />
				{$invoicedata[0]->vState},{$invoicedata[0]->vCountry}<br />
				<strong>Phone No :</strong> {$invoicedata[0]->vPhone}<br />
				<strong>Email :</strong> <a href="mailto:{$invoicedata[0]->vEmail}">{$invoicedata[0]->vEmail}</a><br />
				</div></div>
	</div>
	<div class="clear"></div>
	<div class="mem_info">
		<div class="chap_title">Order Info</div>
		<table width="100%" cellpadding="0" cellspacing="1">
			<tr>
				<th >Product Name</th>
				<th >Size</th>
				<th >Color</th>
				<th width="100px">Amount</th>
			</tr>
			{if $invoicedata|@count gt 0}
			{section name=i loop=$invoicedata}
			<tbody class="admin_antry_table_sec">
				<tr>
					<td>{$invoicedata[i]->vProductName}</td>
					<td>{$invoicedata[i]->vSizeName}</td>
					<td>{$invoicedata[i]->vColorCode}</td>							
					<td>${$invoicedata[i]->vPrice}</td>
				</tr>
			</tbody>
			{/section}
			{/if}
		</table>
	</div>
	<div class="his_total">
		<table cellpadding="0" cellspacing="1" width="100%">
			<tbody>
			<tr>	
				<td class="right_totaltitle">Shipping Charge</td>
				<td width="100">${$invoicedata[0]->vShippingCharge}</td>
			</tr>
			<tr>	
				<td class="right_totaltitle">Express Charge</td>
				<td>${$invoicedata[0]->vExpressCharge}</td>
			</tr>
			<tr>	
				<td class="right_totaltitle"><strong>Grand Total</strong></td>
				<td><strong>${$invoicedata[0]->fTotalAmt}</strong></td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="clear"></div>
	<div class="print_send_cl">
		<input type="button" value="Print" class="submit_btn" title="" onClick="window.print();"/>
		<a href="{$admin_url}transaction/showinvoice?iOrderId={$invoicedata[0]->iOrderId}&iUserId={$invoicedata[0]->iUserId}" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
</div>
{literal}
<script>

</script>
{/literal}