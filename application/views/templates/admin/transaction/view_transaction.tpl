{include file="admin/header.tpl"  title="{$Name}"}
{include file="admin/left.tpl"}

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}user/userlist">User</a></li>
			<li>/</li>
			<li class="current">Transaction</li>
		</ul>
	</div>
	<div class="centerpartbg">
		
		<div class="pagetitle_tab">
			<ul>
				<li ><a href="{$admin_url}user/edit?iUserId={$iUserId}">Edit User</a></li>
				<!--<li ><a href="{$admin_url}projectbrief/projectbrieflist?iUserId={$iUserId}">Project Brief</a></li>-->
				<li class="active"><a href="#">Transaction</a></li>
				<li><a href="{$admin_url}changepassword/passworduser?iUserId={$iUserId}">Change Password</a></li>
				
			</ul>	
		</div>

                {if $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" /> {$var_msg}</p>
		</div>
		<div></div>
		{elseif $var_msg neq ''}
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="{$admin_image_path}icons/icon_success.png" title="Success" />{$var_msg}</p>
		</div>
		<div></div>
		{/if}
		<!--<div class="export_btn" style="margin-top: 10px;">
			
			<form  action="{$admin_url}transaction/export" >
					
					<input type="submit" value="Export" class="search_btn" />
			</form>
		
		</div>-->
		<div class="admin_top_part">
			<form name="frmsearchdate" id="frmsearchdate" method="post" action="{$admin_url}transaction/transactionlist">				
				<input type="hidden" name="iUserId" id="iUserId" value="{$iUserId}" />
				<input  type="hidden" name="iOrderId" value=""/>
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
			<div class="all_trans_box">
				<table cellpadding="0" cellspacing="0">
				  <tr>
					<td valign="top"><div class="search_top_admini">
					<label>Search:</label>
					<select name="option" id="option" onchange="advanceSearch(this.value);">
							<option value="">Select</option>
							<option value="searchByPaymentStatus"{if $filterBy == 'searchByPaymentStatus'}selected="selected"{/if}>Search By Payment Status</option>
							<option value="searchByOrderNumber"{if $filterBy == 'searchByOrderNumber'}selected="selected"{/if}>Search By Order Number</option>
							<option value="searchByTransactionAmount"{if $filterBy == 'searchByTransactionAmount'}selected="selected"{/if}>Search By Order Amount</option>
					</select></td>
					<td><div>
							<input type="hidden" value="" name="filterBy" id="filterBy">
							<div id="searchByDateDiv" style="display: none;" class="searchboxes">
								<div class="inputboxes inputboxesst_date">
									<label for="textfield">Start Date</label>
									<span class="collan_dot">:</span>
									<input type="text" readonly="readonly" id="start_date" name="start_date" class="inputbox" value="{if $start_date != ''}{$start_date}{/if}" title="Start Date" lang="*"/>
								</div>
								<div class="inputboxes inputboxesst_date">
									<label for="textfield">End Date</label>
									<span class="collan_dot">:</span>
									<input type="text" readonly="readonly" id="end_date" name="end_date" class="inputbox" value="{if $end_date != ''}{$end_date}{/if}" title="End Date" lang="*"/>
								</div>
								<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('date_range');"/>
							</div>
							<div id="searchByDayDiv" style="display: none;" class="searchboxes">
								<div class="inputboxes inputboxesst_date">
									<label for="textfield">Select Day</label>
									<span class="collan_dot">:</span>
									<input type="text" readonly="readonly" id="select_day" name="select_day" class="inputbox" value="{if $day != ''}{$day}{/if}" title="Select Day" lang="*"/>
								</div>
								<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('day');"/>
							</div>
							<div id="searchByPaymentStatusDiv" style="display: none;" class="searchboxes">
								<select name="PaymentStatus" id="PaymentStatus" class="adved">
									<option value="">--Select--</option>
									<option value="Completed" {if $eStatus eq 'Completed'}Selected{/if}>Completed</option>
									<option value="Cancelled" {if $eStatus eq 'Cancelled'}Selected{/if}>Cancelled</option>
									<option value="InProcess" {if $eStatus eq 'InProcess'}Selected{/if}>InProcess</option>
								</select>
								<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('PaymentStatus');"/>
							</div>
							<div id="searchByOrderNumber" style="display: none;" class="searchboxes">
								<input type="text" id="order_number" name="order_number" class="inputbox" value="{if $vOrderNum != ''}{$vOrderNum}{/if}" title="Order Number" lang="*">
								<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('order_number');"/>
							</div>
							<div id="searchByTransactionAmount" style="display: none;" class="searchboxes">
								<input type="text" id="tansaction" name="tansaction" class="inputbox" value="{if $fGrandTotal != ''}{$fGrandTotal}{/if}" title="Transation Amount" lang="*">
								<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('transaction_amount');"/>
							</div>
						</div></td>
					</tr>
				</table>
				</div>
			</form>			
			<div class="action_apply_btn_tra">
				<select class="act_sl_box" name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Deletes"{if $action == 'Deletes'}selected="selected"{/if}>Make Delete</option>
					<option value="Show All"{if $action == 'Show All'}selected="selected"{/if}>Show All</option>
				</select>
				<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction_tab(document.getElementById('newaction').value,'transactionlist',document.frmlist,'transaction');"/>
			</div>
			
			<div class="total_top" style="margin-left: 525px !important;">Total Transaction:$ {$total}</div>
			<div class="clear"></div>
			
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="{$admin_url}transaction/search_action" method="post">
				<input type="hidden" name="ssql" id="ssql" value="{$ssql}" />
				<input type="hidden" name="action" id="action" value="" />
				<input type="hidden" name="iUserId" id="iUserId" value="{$iUserId}" />
				<input  type="hidden" name="iOrderId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><a href={$admin_url}transaction/transactionlist?iUserId={$iUserId}&field=vOrderNum&order={$order}><span style="color:white;">Order Number</span>{if $field eq 'vOrderNum'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th><a href={$admin_url}transaction/transactionlist?iUserId={$iUserId}&field=vShippingCharge&order={$order}><span style="color:white;">Shipping Charge</span>{if $field eq 'vShippingCharge'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th><a href={$admin_url}transaction/transactionlist?iUserId={$iUserId}&field=vExpressCharge&order={$order}><span style="color:white;">Express Charge</span>{if $field eq 'vExpressCharge'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th><a href={$admin_url}transaction/transactionlist?iUserId={$iUserId}&field=fTotalAmt&order={$order}><span style="color:white;">Grand Total</span>{if $field eq 'fTotalAmt'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="60px"><a href={$admin_url}transaction/transactionlist?iUserId={$iUserId}&field=eStatus&order={$order}><span style="color:white;">Status</span>{if $field eq 'dPaymentDate'}{if $order eq 'ASC'}<img src="{$admin_image_path}up-arrow.png"/>{else}<img src="{$admin_image_path}down-arrow.png"/>{/if}{/if}</a></th>
							<th width="90px"><span style="color:white;">Action</span></th>
						</tr>
					</thead>
					{if $data|@count gt 0}
					{section name=i loop=$data}
					{if $smarty.section.i.index % 2 eq 0}
					{assign var='class' value='admin_antry_table_sec'}
					{else}
					{assign var='class' value='admin_antry_table'}
					{/if}
					<tbody class="{$class}">
						<tr>
							<td><input name="iOrderId[]" type="checkbox" id="iId" value="{$data[i]->iOrderId}"></td>
							<td><a href="{$admin_url}transaction/showinvoice?iOrderId={$data[i]->iOrderId}&iUserId={$iUserId}">{$data[i]->vOrderNum}</a></td>
							<td>{$data[i]->iCurrencyIcon}{$data[i]->vShippingCharge}</td>
							<td>{$data[i]->iCurrencyIcon}{$data[i]->vExpressCharge}</td>
							<td>{$data[i]->iCurrencyIcon}{$data[i]->fTotalAmt}</td>
							
							<td>{$data[i]->eStatus}</td>
							<td><a href="#"><img src="{$admin_image_path}icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon_tab({$data[i]->iOrderId},{$iUserId},'transaction/delete','{$ssql}')"></a></td>
							
						</tr>
					</tbody>
					{/section}
					<tbody><tr><td></td><td></td><td></td><td class="admin_antry_table total_bottom_tot">Total Amount:</td><td class="admin_antry_table total_bottom">$&nbsp;{$total}</td><td></td><td></td><td></td></tr></tbody>
					{else}
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					{/if}
				</table>
			</form>
		</div>
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found" width="33%" align="left">{$recmsg}</td>
					<td width="33%"><div class="bottom_admin_paging">{$page_link} </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script >
function filterTransactions(filterBy){
	var site_url='{/literal}{$site_url}{literal}';
		
		if(filterBy =='date_range'){
			if($('#start_date').val() == ''){
				//alert("Please Enter Start Date");
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Start Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				return false;
			}else if($('#end_date').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter End Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter End Date");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'date_range';	
			}
		}else if(filterBy =='day'){
			if($('#select_day').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Date");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'day';
			}	
		}else if(filterBy == 'PaymentStatus'){			
			if($('#PaymentStatus').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please select  any option</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please select any option");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'PaymentStatus';
			}
		}else if(filterBy == 'order_number'){
			if($('#order_number').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Order Number</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Order Number");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'order_number';
			}
		}else if(filterBy == 'transaction_amount'){			
			if($('#tansaction').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Transation Amount</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Transation Amount");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'transaction_amount';
			}
		}
		document.getElementById('frmsearchdate').submit();
	}
	
	function advanceSearch(key){
		//alert(key);
		$(".searchboxes").css("display","none");
		if(key == 'searchByDate'){
			document.getElementById('searchByDateDiv').style.display='block';
		}else if(key == 'searchByDay'){
			document.getElementById('searchByDayDiv').style.display='block';
		}else if(key == 'searchByPaymentStatus'){
			document.getElementById('searchByPaymentStatusDiv').style.display='block';
		}else if(key == 'searchByOrderNumber'){
			document.getElementById('searchByOrderNumber').style.display='block';
		}else if(key == 'searchByTransactionAmount'){
			document.getElementById('searchByTransactionAmount').style.display='block';
		}		
	}
	var filterBy1 = '{/literal}{$filterBy}{literal}';
	if(filterBy1 !=''){		
	if(filterBy1 == 'searchByDate'){
		document.getElementById('searchByDateDiv').style.display='block';
	}else if(filterBy1 == 'searchByDay'){
		document.getElementById('searchByDayDiv').style.display='block';
	}else if(filterBy1 == 'searchByPaymentStatus'){
		document.getElementById('searchByPaymentStatusDiv').style.display='block';
	}else if(filterBy1 == 'searchByOrderNumber'){
		document.getElementById('searchByOrderNumber').style.display='block';
	}else if(filterBy1 == 'searchByTransactionAmount'){
		document.getElementById('searchByTransactionAmount').style.display='block';
	}
	}
</script>
{/literal}