<script type="text/javascript" src="{$ckeditor_path}ckeditor.js"></script>
{include file="admin/header.tpl" title="{$Name}"}
{include file="admin/left.tpl"}
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="{$admin_image_path}icon_breadcrumb.png"></li>
			<li><a href="{$admin_url}dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="{$admin_url}skill/skilllist">Skill</a></li>
			<li>/</li>
			<li class="current">{if $operation eq 'add'}Add  Skill{else}Edit Skill{/if}</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			{if $operation eq 'add'}
			Add Skill
			{else}
			Edit Skill
			{/if} </div>
		
		<form id="frmaddSkill" name="frmaddSkill" method="post" action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iSkillId" id="iSkillId" value="{if $operation neq 'add'}{$data->iSkillId}{/if}" />
				<input type="hidden" name="operation" id="operation" value="{$operation}" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Skill</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vSkill" name="Data[vSkill]" class="inputbox" value="{if $operation neq 'add'}{$data->vSkill}{/if}" lang="*" title="Skill" style="width:250px"/>
				</div>
				
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" {if $operation neq 'add'} {if $data->eStatus eq Active}selected{/if}{/if}>Active</option>
						<option value="Inactive" {if $operation neq 'add'} {if $data->eStatus eq Inactive}selected{/if}{/if} >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
					{if $operation eq 'add'}
						<input type="button" value="Add Skill" class="submit_btn" title="Add Skill" onclick="checkSkill('{$admin_url}');"/>
					{else}
						<input type="button" value="Edit Skill" class="submit_btn" title="Edit Skill" onclick="checkSkill('{$admin_url}');"/>
					{/if}
					<a href="{$admin_url}skill/skilllist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
				<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
{include file="admin/footer.tpl"}
{literal}
<script type="text/javascript">
	</script>
{/literal} 
