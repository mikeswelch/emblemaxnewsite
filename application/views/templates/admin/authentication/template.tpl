{include file="admin/authentication/header.tpl" title="{$Name}"}


<div class="frient_admin_page">
	{if $var_msg neq ''}
    <div class="error_msg">
        {$var_msg}
    </div>
{/if}
<div id="showloginid"><form name="frmlogin" id="frmlogin" action="{$admin_url}authentication/check_login" method="post"  >
<input type="hidden" name="data[mode]" value="authenticate">
    
    <div class="userbox_input">
		<img src="{$admin_image_path}icon_username.png"/>
		<label>Username : </label>
		<input type="text"  name="vUserName" id="vUserName" title="User Name"/>
	</div>
	<div class="userbox_input">
		<img src="{$admin_image_path}icon_locked.png"/>
		<label>Password : </label>
		<input type="password"  id="vPassword" name="vPassword" title="Password"/>
	</div>
	<div class="submit_btn">
        <input type="submit"  value="Submit"  title="Submit" onclick="return CheckLogin();"/>
	</div>
	<div class="frient_forgotten">
		<a href="#" onclick="showforgot();">Forgotten Password?</a>
	</div>
</form>    
</div>

<div id="forgotpasswordid"  style="display:none;">
<form name="frmlogin" id="frmlogin" action="{$admin_url}authentication/check_login" method="post"  >
<input type="hidden" name="data[mode]" value="forgotpassword">
   
    <div class="userbox_input">
		<img src="{$admin_image_path}icon_username.png"/>
		<label>Email ID : </label>
		<input type="text"  name="vEmail" id="vEmail" title="User Email"/>
	</div>
	<div class="submit_btn">
        <input type="submit"  value="Submit"  title="Submit" onclick="return CheckLogin();"/>
	</div>
	<div class="frient_forgotten">
		<a href="#" onclick="showlogin();">Back to Login?</a>
	</div>
</form>
</div>
</div>
{literal}
<script type="text/javascript">
function CheckLogin()
{
	
    
    if (document.frmlogin.vUserName.value == "")
	{
		alert("Enter your Username");
		document.frmlogin.vUserName.focus();
		return false;
	}
	if (document.frmlogin.vPassword.value == "")
	{
		alert("Enter your password");
		document.frmlogin.vPassword.focus();
		return false;
	}
	document.frmlogin.submit()
}

</script>
<script>
function showforgot(){
    jQuery("#showloginid").hide("slow");
    jQuery("#forgotpasswordid").show("slow");
}
function showlogin(){
    jQuery("#forgotpasswordid").hide("slow");
    jQuery("#showloginid").show("slow");
}  
</script>
{/literal}


{include file="admin/authentication/footer.tpl"}
