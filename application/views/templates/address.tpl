{include file="header.tpl"}
<div class="main-container col2-right-layout">
<div class="main">
<div class="main-inner clearfix">
<div id="category-image" class="category-image-top"></div>
<div class="container">
<div class="row-fluid show-grid">
<div class="col-main-wap span9">
<div class="col-main">
<div class="col-main-inner">
<div class="my-account">
<div class="page-title title-buttons row-fluid show-grid">
<h1 class="pull-left">Address Book</h1>
<button class="button btn-book pull-right" onclick="window.location='http://192.168.1.12/magentowebapp/mt_robel/customer/address/new/';" title="Add New Address" type="button">
<span>
<span>Add New Address</span>
</span>
</button>
</div>
{if $msg}
<ul class="messages">
<li class="success-msg">
<ul>
<li>
<span>{$msg}</span>
</li>
</ul>
</li>
</ul>
{/if}
<div class="col2-set addresses-list row-fluid show-grid">
{if $UserDetails|@count gt 0}
<div class="col-1 addresses-primary span6">
<h2>Default Addresses</h2>
<ol>

<li class="item">
<h3>Default Billing Address</h3>
                    {if $BillShipDetails|@count gt '0'}
		    <address>
			{$BillShipDetails[0]->vBillingFirstName}&nbsp;{$BillShipDetails[0]->vBillingLastName}<br/>
			{$BillShipDetails[0]->tBillingStreetAddress1}<br />
			{$BillShipDetails[0]->tBillingStreetAddress2}<br />
			{$BillShipDetails[0]->vBillingCity}<br />
			{section name=i loop=$state}
			<div>{if $state[i]->iStateId eq $BillShipDetails[0]->iBillingStateId} {$state[i]->vState} {/if}</div>
			{/section}
			
			{section name=j loop=$country}
			<div>{if $country[j]->iCountryId eq $BillShipDetails[0]->iBillingCountryId} {$country[j]->vCountry} {/if}</div>
			{/section}
			{$BillShipDetails[0]->vBillingZipCode}<br/>
			T:{$BillShipDetails[0]->vBillingTelephone}
			{if $BillShipDetails[0]->vBillingFax neq ''}<br/>F:{$BillShipDetails[0]->vBillingFax}{/if}
		    </address>
		    {else}
		    </address>
		    You have not set a default billing address.<br/>
		    <!--<a href="{$site_url}myAccount/edit_address">Edit Address</a>-->
		    {/if}
                </address><p>
			<a href="{$site_url}myAccount/edit_address">Change Billing Address</a>
			</p>
			</li>
			<li class="item">
			<h3>Default Shipping Address</h3>
                {if $ShippingDetails|@count gt '0'}
		<address>
			{$ShippingDetails[0]->vShippingFirstName}&nbsp;{$ShippingDetails[0]->vShippingLastName}<br/>
			{$ShippingDetails[0]->tShippingStreetAddress1}<br />
			{$ShippingDetails[0]->tShippingStreetAddress2}<br />
			{$ShippingDetails[0]->vShippingCity}<br />
			{section name=i loop=$state}
			<div>{if $state[i]->iStateId eq $ShippingDetails[0]->iShippingStateId} {$state[i]->vState} {/if}</div>
			{/section}
			
			{section name=j loop=$country}
			<div>{if $country[j]->iCountryId eq $ShippingDetails[0]->iShippingCountryId} {$country[j]->vCountry} {/if}</div>
			{/section}
			{$ShippingDetails[0]->vShippingZipCode}<br/>
			T:{$ShippingDetails[0]->vShippingTelephone}
			{if $ShippingDetails[0]->vShippingFax neq ''}<br/>F:{$ShippingDetails[0]->vShippingFax}{/if}
			<address>
		       {else}
		       </address>
		       You have not set a default Shipping address.<br/>
		       <!--<a href="{$site_url}myAccount/edit_address">Edit Address</a>-->
                {/if}
                </address>
<p>
<a href="{$site_url}myAccount/edit_address">Change Shipping Address</a>
</p>
</li>
</ol>
</div>
<div class="col-2 addresses-additional span6">
<h2>Additional Address Entries</h2>
<ol>
<li class="item empty">
<p>You have no additional address entries in your address book.</p>
</li>
</ol>
</div>
</div>
<div class="buttons-set">
<p class="back-link pull-left">
{else}
You Have Not Mention Any Address Yet   
{/if}
</p>
<a href="{$site_url}myAccount/myDashboard">
<small style="float:left;">« </small>
<label style="float:left;">Back</label>
</a>
</p>
</div>
</div>
</div>
</div>
</div>
<div style="margin-top:104px;">{include file="right_myAccount.tpl"}</div>
</div>
</div>
</div>
</div></div>

{include file="footer.tpl"}