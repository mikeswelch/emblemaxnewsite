{include file="header.tpl" title="order details"}
<div class="main-container col2-right-layout">
<div class="container">
    <div class="main-inner">
        <div class="col-main">
	    <div class="account-login row-fluid show-grid">

  <div class="box-account box-recent" style="width: 850px;float: left;">
    <div class="box-head">
        <h2>My Orders</h2>
        <!--<a href="#">View All</a>-->
    </div>
	<table id="shopping-cart-table" class="data-table cart-table" style="border-bottom:2px solid #EAEAEA">
                <col width="1" />
                <col />
                <col width="1" />
		<col width="1" />
		<col width="1" />
		<col width="1" />
		<col width="1" />
		<thead class="hidden-phone">
			<tr class="">
			    <th class="" rowspan="1"><span class="nobr">Order Id</span></th>
			    <th width="70" rowspan="1" class=""><span class="nobr">Date Added</span></th>
			    <th width="200" rowspan="1" class=""><span class="nobr">Product</span></th>
			    <!--<th colspan="1"><span class="nobr">Model</span></th>-->
			    <th rowspan="1"><span class="nobr">Status</span></th>
			    <th colspan="1"><span class="nobr">Price</span></th>
			    <th rowspan="1"><span class="nobr">Details</span></th>
			</tr>
		</thead>
		<tbody>
                {if $orders|@count gt 0}
		{section name=i loop=$orders}
			<tr>
			   <td style="padding-bottom: 25px;">{$orders[i]['vInvoiceNum']}</td>
			   <td width="7">{$orders[i]['dAddedDate']}</td>
			   <td width="200">{$orders[i]['vProductName']}</td>
			   <!--<td>PR - 3</td>-->
			   <td>{$orders[i]['status']}</td>
			   <td>${$orders[i]['fGrandTotal']}</td>
			   <td><a href="{$site_url}myAccount/showOrderDetail?iOrderId={$orders[i]['iOrderId']}" class="viewdet">View Details</a></td>
		       </tr>
                {/section}
                {else}
                <tr>
                        <td height="20px;" colspan="8" style="text-align:center; color:#38414B; font-size:14px; font-weight:bold;">No Records Found.</td>
                </tr>
                {/if}
                </tbody>
            </table>
    <script type="text/javascript">decorateTable('my-orders-table')</script>
</div>
  <div style="margin-top: 21px;">{include file="right_myAccount.tpl"}</div>
	    </div>
	</div>
    </div>
 
</div>

</div>
</div>

<!--<div class="main-container col2-right-layout">
    <div class="grid_11 singleleft">
        <div class="billing edtptof">
            <div class="shiztitle">
            <h2 class="title h2bg">My Orders</h2>
        </div>

        <div class="orderlist">
            <table cellpadding="3" cellspacing="3">
                <tr class="blacky">
                        <td align="center" valign="middle" class="table80">Order Id</td>
                        <td align="center" valign="middle" class="table100">Added Date</td>
                        <td class="table95">Status</td>
                        <td class="table100">Grand Total</td>
                        <td class="table100">Order Details</td>
                        </tr>
                {if $orders|@count gt 0}
		{section name=i loop=$orders}
                <tr>
                        <td>{$orders[i]['vInvoiceNum']}</td>
                        <td>{$orders[i]['dAddedDate']}</td>
                        <td align="center">{$orders[i]['status']}</td>
                        <td>${$orders[i]['fGrandTotal']}</td>
                        <td><a href="{$site_url}myAccount/showOrderDetail?iOrderId={$orders[i]['iOrderId']}" class="eye">View Order</a></td>
                        </tr>
                {/section}
                {else}
                <tr>
                        <td height="20px;" colspan="8" style="text-align:center; color:#38414B; font-size:14px; font-weight:bold;">No Records Found</td>
                </tr>
                {/if}
            </table>
        </div>
    </div>
</div>
</div>
</div>
<div class="grid_4 righthome">

</div>

</div>-->
<div class="clear"></div>
{include file="footer.tpl"}