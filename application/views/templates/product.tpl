{include file="header.tpl"}

<script type="text/javascript">
$(window).load(function() {
    setTimeout(function() {
        $("#main").show('fadeIn', {}, 900)
    }, 7000);
});
</script>



<div class="main-container col2-right-layout">
    <div class="main">
	  <div class="main-inner clearfix">				
		<div class="breadcrumbs row-fluid">
		    <div class="container">
		    <ul class="span12" style="text-align:left;">
			  <li class="home">
				<a href="#" title="Go to Home Page">Home</a>
				<samp></samp>
			  </li> 
			  <li class="category5">
				<strong>Categories</strong>
			  </li> 
		    </ul> 
		    <!--		<div class ="mt-page-title hidden-phone span4"> 
		    <h2>Categories</h2> 
		    </div> 
		    -->
		    </div>	
		</div>
		<div class="container">
		    <div class="row-fluid show-grid">
			  <div class="col-main span9">
				<div class="col-main-inner">
				    <p class="category-image" style="display:block;"></p>
				    <div class="category-products">
					  <div class="toolbar"></div>
					   <div id="pageData">
						  {if $products|@count gt 0}
						  {section name=i loop=$products}
						  <div class="products-grid row-fluid show-grid">
							<div class="item first span4">
							    <div class="item-inner content">
								  <div class="product-image">
									<div class="product-thumb">
									   <div class="product_img_box">
									    <a href="{$site_url}instant-quote/{$products[i]['iProductId']}" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
									    <img  src="{$upload_path}product/{$products[i]['iProductId']}/1X160_{$products[i]['vImage']}" alt="Cras in risus et risus" />
									    </a>
									   </div>
									</div>
								  </div>
								  <div class="mask quotelinkmak" onclick="setLocation('#')">							
									<h3 class="product-name">
									<a href="{$site_url}instant-quote/{$products[i]['iProductId']}" title="Cras in risus et risus">{$products[i]['vProductName']}</a>
									</h3>
								  </div>
								  <div class="top-actions-inner">
									<div class="mt-actions clearfix">
									</div>					
								  </div>		
							    </div>
							</div>
						  </div>
						  {/section}
						  {else}
						  <span>No Item Found</span>
						  {/if}
						  <div class="mt-row-page">
							 <div id="ajax_paging">
							 <div class="Pagingbox">
								  {$pages}
							  </div>
							  <!--{$recmsg}-->
							 </div>
						  </div>
					   </div>
					  
					  <div class="toolbar-bottom">
						<div class="toolbar"></div>
					  </div>
				    </div>
				</div>
			  </div>
			  <div class="col-right sidebar span3 visible-desktop"> 
				<div class="block mt-smartmenu">
				    <div class="block-title">
					  <strong><span>Category</span></strong>
				    </div>
				    <div class="title-divider">
					  <span>&nbsp;</span>
				    </div>
				    <div class="block-content">
					   {literal}					   
						  <script type="text/javascript">
							 $mtkb(document).ready(function(){	
							 // applying the settings
							 $mtkb("#mt-accordion li.active span.head").addClass("selected");
							 $mtkb('#mt-accordion').Accordion({
								active: 'span.selected',
								header: 'span.head',
								alwaysOpen: false,
								animated: true,
								showSpeed: 400,
								hideSpeed: 800,
								event: 'click'
								});
							 });	
						  </script>
					   {/literal}
					   <ul id="mt-accordion" class="clearfix">
						 {if $categories|@count gt 0}
						 {section name=i loop=$categories}
						 {if $categories[i]->iParentId eq '0'}
						 <li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
							<a href="{$site_url}category/{$categories[i]->vUrlText}"><span>{$categories[i]->vCategory}</span></a><span class="head"><a href="#" style="float:right;"></a></span>
							<ul class="level0">
							   {section name=j loop=$categories}
							   {if $categories[j]->iParentId neq '0' AND $categories[j]->iParentId eq $categories[i]->iCategoryId}
							   <li class="level1 nav-categories-footwear-man">
								 <a href="{$site_url}category/{$categories[j]->vUrlText}"><span>{$categories[j]->vCategory}</span></a>
							   </li>
							   {/if}
							   {/section}
							</ul>
						 </li>
						 {/if}
						 {/section}
						 {/if}
					   </ul>
				    </div> 
				</div>
				<input id="max-price" type="hidden" name="max-price" value="{$max_pPrice}">
				<input id="filter_url" type="hidden" name="filter_url" value="{$site_url}category/product_filter/">
				   
				<input id="category" type="hidden"  value="{$alp}">
				 
				<div class="block block-layered-nav">
				    <div class="block-title">
					  <strong><span>Price</span></strong>
				    </div>
				    <div class="title-divider"><span>&nbsp;</span></div>
				    <div class="block-content"> 
					  <p class="block-subtitle" style="display: none;">Shopping Options</p>
					  <dl id="narrow-by-list">  
						<dd style="height:11px; margin: 20px 0 20px 0;" id="slider" class="odd ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header" style="left: 0%; width: 100%;"></div><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></a></dd>
						<dd class="last even"> 
						    <span class="filter_min" id="filter_min">$0.00</span> 
						    <span class="filter_max" id="filter_max">{$max_pPrice}.00</span>
						 </dd> 
					  </dl>
					  
				    </div>
				    {literal}
				    <script type="text/javascript">
					  var Magen_filterslider = {
						params : '',
						currency : '$',
						category_id: 5,
						currency_code : 'USD',
						filter_min: 0,
						filter_max: 427
					  }
				    </script>
				    {/literal}
				</div>
			  </div>
		    </div>
		</div>
	  </div>
	  <div class="mt-productcroller-container clearfix"></div>
    </div>
</div>
<script type='text/javascript'>
/*    
function changePaginationProduct(start){
    var iCategoryId = {$iCategoryId};
    var url = site_url+'category/ajaxpage';
    var dataString = 'start='+ start+'&iCategoryId='+ iCategoryId;
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		$("#pageData").html(result);
	  }
    });
}
*/
</script>
{include file="footer.tpl"}