 {if $homeBanner|@count gt 0}
                {section name=i loop=$homeBanner} 
		<div class="panel">
                        <div class="panel-wrapper">							
                    <h2 class="title" style="display:none; float:none; margin:0;">{$homeBanner[i]['vBottomTitle']}</h2>							
				<div class="slidcont">
					<div class="prodimgpart"><img src="{$upload_path}/home_slider/{$homeBanner[i]['iHomeSliderId']}/1170X468_{$homeBanner[i]['vImage']}" alt="" /></div>
					    {if $homeBanner[i]['eShowDeliveryDate'] eq 'Yes'}
					    <div class="datapart">
						    <h3>{$homeBanner[i]['vTitle']}</h3>
						    <p>{$homeBanner[i]['tShortDescription']}</p>
						    <div class="slidbtn"><a href="{$homeBanner[i]['vGetStarted']}">Get Started</a></div>
						    <div class="calendorpt">
							    <div class="estdate">Estimated delivery date<span style="color: red;">*</span></div>
							    <div class="calone">
								    <p class="standerdtx">Standard</p>
								    <p class="monthcal">{$standardDeliveryMonth}</p>
								    <p class="datecal"><strong>{$standardDeliveryDay}</strong></p>
							    </div> 
							    <div class="calone">
								    <p class="standerdtx">Rush</p>
								    <p class="monthcal">{$rushDeliveryMonth}</p>
								    <p class="datecal"><strong>{$rushDeliveryDay}</strong></p>
							    </div>
							    <p style="color: red;">*Need it faster? Contact us for options.</p>
						    </div>
						    
					    </div>
					    {else}
					    <div class="datapart_without_deliverydate">
						    <h3>{$homeBanner[i]['vTitle']}</h3>
						    <p>{$homeBanner[i]['tShortDescription']}</p>
						    <!--{if $homeBanner[i]['vGetStarted'] neq ''} -->
						    <div class="slidbtn"><a href="{$homeBanner[i]['vGetStarted']}">Get Started</a></div>
						    <!--{/if}-->
					    </div>
					    {/if}
				</div>
			</div>
		</div>
                {/section}
                {/if}
			 
			 
			 
{literal}
<script type="text/javascript">
	 /*$(window).load(function() {
		$('#coda-slider-1').codaSlider({
		    //autoSlide: true,
		    //autoSlideInterval: 3000,
		}
		);
		
	});
    */
	  function load(){
		$('#coda-slider-1').codaSlider({
		    //autoSlide: true,
		    //autoSlideInterval: 3000,
		}
		);
		
	  }
	  load();
</script>
{/literal}