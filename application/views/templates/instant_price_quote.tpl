{include file="header.tpl" title="Example Smarty Page"}
<!-- Add jQuery library -->
<script type="text/javascript" src="{$site_url}public/front-end/fancybox/jquery-1.9.0.min.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="{$site_url}public/front-end/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="{$site_url}public/front-end/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="{$front_js_path}jquery.form.js"></script>

<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF;">
	<div class="breadcrumbs row-fluid">
   <div class="container">
     <ul class="span12" style="text-align:left;">
	<li class="home">
	     <a href="{$site_url}" title="Go to Home Page">Home</a>
	     <samp></samp>
	</li> 
	<li class="category5">
	     <strong>Get a Quote</strong>
	</li> 
     </ul> 

   </div>	
   </div>
	<div class="main container">
	<h2 class="pagetital subtit" style="width:150px !important;" id="getAQuoteID">Get a Quote </h2>
         <div class="product_quto_main">
	    
	    	<div class="product_detail_left">
		<div class="images">
		  {if $product[0].vImage neq '' }
		    <div class="main_image multiple storePadHeight" style="text-align: center;" id="mainImage">
			<img height="400px" width="300px" src="{$upload_path}product/{$product[0].iProductId}/{$product[0].vImage}">			
		    </div>
		  {else}
		  <div class="main_image multiple storePadHeight" style="text-align: center;" id="mainImage">
			 <img height="400px" width="300px" src="{$front_image_path}No_image.jpg">
		    </div>	  
		  {/if}		  
		    {if count($productColorImages)>0}
		    {section name=i loop=$productColorImages }	    
		    <div class="main_image multiple storePadHeight" style="text-align: center;display: none" id="colorimage_{$iProductId}{$productColorImages[i]->iColorId}" >
			 {if $productColorImages[i]->vFrontImage neq ''}
			 <img height="400px" width="300px" src="{$upload_path}product/{$productColorImages[i]->iProductId}/{$productColorImages[i]->iProductColorId}/{$productColorImages[i]->vFrontImage}">
			 {else}
			 <img height="400px" width="300px" src="{$front_image_path}No_image.jpg">
			 {/if}
		    </div>		  
		    {/section}
		    {/if}
		    
		    
		    
		</div>
	    </div>
	    
	    
	    <div class="product_detail_right">
		<div id="Module_ApparelQuote_container">
		    <h4 class="quote_steps_head">Step 1 - Select Shirt</h4>
		    <div class="otherImageLinks" style="font-size: 16px;"><p>Style: {$product[0].vProductName} <a style="text-align: center; color: #9E053B; font-size: 14px;" href="#popfancy" id="fancyhref">(Switch Product)</a></p>
			<div style="display:none;" height="600px" width="100%">
			    <div id="popfancy">
				
<div class="main-container col2-right-layout">
    <div class="main">
	  <div class="main-inner clearfix">				
		<div class="breadcrumbs row-fluid">
		</div>
		<div class="container">
		    <h2 class="pagetital subtit" style="margin-left: 20px !important;">Switch Products </h2>
		    <div class="row-fluid show-grid" >
			  <div class="col-main span9" id="update_div">		
				<div class="col-main-inner" id="maindivHide">
				    <p class="category-image" style="display:block;"></p>
				    <div class="category-products">
					  <div class="toolbar"></div>
					  <div id="pageData">
						{if $all_category_data|@count gt 0}
						{section name=i loop=$all_category_data}
						<div class="products-grid row-fluid show-grid">
						    <div class="item first span4">
							  <div class="item-inner content">
								<div class="product-image">
								    <div class="product-thumb">
									  <a href="{$site_url}instant-quote/{$all_category_data[i]['iProductId']}" title="{$all_category_data[i]['vProductName']}" class="product-image visible-desktop" id="product_31">
									  <img src="{$upload_path}product/{$all_category_data[i]['iProductId']}/1X160_{$all_category_data[i]['vImage']}" alt="Cras in risus et risus" />
									  </a>
								    </div>
								</div>
								<div class="mask" onclick="setLocation('#')">							
								    <h3 class="product-name">
								    <a href="{$site_url}instant-quote/{$all_category_data[i]['iProductId']}" title="{$all_category_data[i]['vProductName']}">{$all_category_data[i]['vProductName']}</a>
								    </h3>
								</div>
								<div class="top-actions-inner">
								    <div class="mt-actions clearfix">
								    </div>					
								</div>
							  </div>
						    </div>
						</div>
						{/section}
						{else}
						<span style="margin-left: 50%; font-size: medium;">No Item Found</span>
						{/if}
						<div class="mt-row-page">
						  <div id="ajax_paging">
						  <div class="Pagingbox">
							   {$pages}
						   </div>
						   {$recmsg}
						  </div>
					   </div>
					  </div>
					  <!-- <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script> -->
					  <div class="toolbar-bottom">
						<div class="toolbar"></div>
					  </div>
				    </div>
				</div>
			  </div>
			  
			  <div class="col-right sidebar span3 visible-desktop"> 
				<div class="block mt-smartmenu">
				    <div class="block-title">
					  <strong><span>Category</span></strong>
				    </div>
				    <div class="title-divider">
					  <span>&nbsp;</span>
				    </div>
				    <div class="block-content">
					  {literal}	
					  <!-- <script type="text/javascript" src="js/mt.accordion.js"></script> -->
					  <script type="text/javascript">
						$mtkb(document).ready(function(){	
						// applying the settings
						$mtkb("#mt-accordion li.active span.head").addClass("selected");
						$mtkb('#mt-accordion').Accordion({
						active: 'span.selected',
						header: 'span.head',
						alwaysOpen: false,
						animated: true,
						showSpeed: 400,
						hideSpeed: 800,
						event: 'click'
						});
						});	
					  </script>
					  {/literal}
					  <ul id="mt-accordion" class="clearfix">
						{if $categories|@count gt 0}
						{section name=i loop=$categories}
						{if $categories[i]->iParentId eq '0'}
						<li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
						    <a href="javascript:void(0);" onclick = "getProduct('{$categories[i]->iCategoryId}');"><span>{$categories[i]->vCategory}</span></a><span class="head"><a href="#" style="float:right;"></a></span>
						    <ul class="level0">
							  {section name=j loop=$categories}
							  {if $categories[j]->iParentId neq '0' AND $categories[j]->iParentId eq $categories[i]->iCategoryId}
							  <li class="level1 nav-categories-footwear-man">
								<a href="javascript:void(0);" onclick = "getProduct('{$categories[i]->iCategoryId}');"><span>{$categories[j]->vCategory}</span></a>
							  </li>
							  {/if}
							  {/section}
						    </ul>
						</li>
						{/if}
						{/section}
						{/if}
					  </ul>
				    </div> 
				</div>
			  </div>
		    </div>
		</div>
	  </div>
	  <div class="mt-productcroller-container clearfix"></div>
    </div>
</div>

{literal}
<script type='text/javascript'>
function changePagination(start){
    //alert(pageId);
    //alert(liId);
    var q = "{/literal}{$alp}{literal}";
    //alert(q);
    if(q){
	   var dataString = 'start='+ start+'&q='+q;
    }else{
	   var dataString = 'start='+ start;
    }
    var url = site_url+'category/ajaxpage';
    //alert(url);
    //var dataString = 'start='+ start;
    //alert(dataString);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		$("#pageData").html(result);
	  }
    });
}
</script>

{/literal}				
				
				
				
			    </div>
			</div>
		    </div>
		    
		    
		    <form id="frmGetQuote" method="post" action="{$site_url}instant_quote/calculate_quote" enctype="multipart/form-data">
		    <input type="hidden" name="iProductId" value="{$product[0].iProductId}">			 
		    <h4 class="quote_steps_head">Step 2 - Select Color</h4>
		    <div class="inner" style="background-color: #EDEDED; padding: 2%; width: 98%; float: left; margin-bottom: 15px;">
			<div class="colors">
			    {section name=i loop=$colors}
			    <div style="float: left; padding:2px;">
				    <div title="{$colors[i].vColor}" id="Module_ApparelQuote_colorSwatch_{$colors[i].iColorId}" style="background-color: {$colors[i].vColorCode};" class="color" onclick="checkboxcheck('{$colors[i].iColorId}');" onmouseover="return showColorImage('{{$colors[i].iColorId}}');"  onmouseout="return hideColorImage('{{$colors[i].iColorId}}');"></div>
			    </div>
			    {/section}
			</div>
		    </div>
		    <h4 class="quote_steps_head">Step 3 - Enter Sizes and quantity</h4>
		    <div class="selectSizeBox" style="background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;">
			<div id="noneSizeSelected">Please select colors from step 2 before proceeding to step 3.</div>
			<table class="colors">
			    <tbody id="colorsizebox">
				<tr id="colorsizebox_root" style="display: none;">
				    <td><span>Colors</span></td>
				    {section name=i loop=$sizes}
				    <td style="padding-left: 10px;">
					<span>{$sizes[i].vSize}</span>
				    </td>
				    {/section}
				</tr>
				{section name=i loop=$colors}
				<tr id="colorsizebox_{$colors[i].iColorId}" style="display: none;">
				    <td>{$colors[i].vColor}</td>
				    {section name=j loop=$sizes}
				    <td class="color_size">
					<span>
					    {if $colors[i].iColorId|in_array:$productColorArray AND $sizes[j].iSizeId|in_array:$productSizeArray[$colors[i].iColorId]}
					    <input type="text" onchange="countQtyTotal({$colors[i].iColorId},this.value);" onkeyup="changeTextboxQty(this.value);" name="color_size[orderqty-size-{$sizes[j].iSizeId}-color-{$colors[i].iColorId}]" style="width: 40px;" id="color_size_orderqty_size_{$sizes[j].iSizeId}_color_{$colors[i].iColorId}">
					    {else}
					    <input type="text" value="N/A" style="width: 40px;" disabled="disabled">
					    {/if}
					</span>
				    </td>
				    {/section}
				</tr>
				{/section}
				
			    </tbody>
			</table>
		    </div>
		    
		    <h4 class="quote_steps_head">Step 4 - Select Imprint colors and locations</h4>
		    <div id="noneDecorationSelection" style="background-color: #EDEDED; padding: 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div>Please enter quantities in step 3 before proceeding to step 4.</div>
		    </div>
		    <div id="decorationService" class="outer" style="display: none; background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div class="inner" style="">
			    <p>Select a decoration for one or more of the areas below.</p>
			    <div class="areaSelection">
				{section name=i loop=$printLocations}
				<div class="area_66">
				    <input type="checkbox" class="print_location_text" id="print_location_checks_{$printLocations[i].iPrintLocationId}" name="print_location[printlocation_{$printLocations[i].iPrintLocationId}]" style="" onchange="selectPrintLocation('{$printLocations[i].iPrintLocationId}');">
				    <label for="print_location_checks_{$printLocations[i].iPrintLocationId}">{$printLocations[i].vPrintLocation}</label>
				</div>
				{/section}
			    </div>
			  <!--   <table class="areas" style="">
				<tbody>
				{section name=i loop=$printLocations}
				    <tr style="display: none;" id="screenprint1_{$printLocations[i].iPrintLocationId}">
					<td style="width: 15%; padding-left: 10px;"><span class="areaName" style="color:#002F5F">{$printLocations[i].vPrintLocation}</span></td>
					<td style="padding: 10px; width: 30%;">
					    <select name="decoration_type[{$printLocations[i].iPrintLocationId}]" style="" onchange="selectDecorationType(this.value,'{$printLocations[i].iPrintLocationId}')">
						<option value="screenprinting" selected="selected">Screenprinting</option>
						<option value="embroidery">Embroidery</option>
						<!--<option value="DTG">DTG</option>
					    </select>
					</td>
					<td>
					    <div id="screenprint2_{$printLocations[i].iPrintLocationId}" style="display: none; padding: 5px 0px 0px 10px;" >
						<span style="float: left; margin: 0px 5px 0px 0px;">Number of colors: </span>
						<select name="number_of_colors[numberofcolors_{$printLocations[i].iPrintLocationId}]" class="screenprinting_colors" style="">
						    {section name=j loop=$numberColors} 
						    <option value="printlocation-{$printLocations[i].iPrintLocationId}-numbercolor-{$numberColors[j].iNumberColorsId}">{$numberColors[j].vNumberColor}</option>
						    {/section}
						</select>
					    </div>
					</td>
				    </tr>
				    {/section}
				    
				</tbody>
			    </table>
			    -->
			    <div id="ajaxdiv">
				
				
				
			    </div>
			    
			    
			    
			</div>
		    </div>
		    
		    <h4 class="quote_steps_head">Step 5 - Turn Around Time</h4>
		    <div id="shipmentTypes" style="background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div class="rush">
			    {section name=i loop=$shipments}
			    <input style="float: left; width: 4%;" type="radio" value="{$shipments[i].iTurnAroundTimeId}"  id="shipmentId{$shipments[i].iTurnAroundTimeId}" name="shipment" {if $smarty.section.i.index eq 0}checked="checked"{/if} style="">
			    <label style="width: 90%; float: left;" for="shipmentId{$shipments[i].iTurnAroundTimeId}">{$shipments[i].vTitle} ({$shipments[i].tNote})</label>
			    <br><br>
			    {/section}
			</div>
		    </div>
		    
		    <h4 class="quote_steps_head">Step 6 - Coupon Code (optional)</h4>
		    <div id="shipmentTypes" style="background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div class="rush">
			    <label style="width:15%; float: left;"> Coupon Code</label>
			    <input type="text" name="coupon_code" id="coupon_code">
			</div>
		    </div>
		    
		    <div style="border-top: 1px solid #CCCCCC; width:100%; float: left; padding-top:15px;">
			<div id="quotePrice"></div>
		    </div>
		    
		    
		    <div style="float: left; padding-top:15px;">
			<input type="submit" value="Quote Now" class="instnt_btn" style="float: left; background: #9e0546;border: 2px solid #d17a99; line-height: 25px;padding: 5px 15px;">			
			 <div id="uploadArtworkButton" style="float: left;">
				<a href="#upload-artwork" id="clickUploadArtwork"></a>
				<input type="button" onclick="uploadArtworkAndProceedCart();" value="Upload Artwork and Proceed to Cart" class="instnt_btn" id="upload_artwork_cart" style="float: left; margin-left: 20px;background: #9e0546; border: 2px solid #d17a99;line-height: 25px; padding: 5px 15px;display: none;" >
				<div id="ajax_loader" style="color:#c03b37;margin-top: -3px;padding: 10px; font-size:16px;text-align:center;display: none; "><img src="{$front_image_path}ajax-loader.gif">  Loading...</div>
			 </div>
		    </div>    
		    </form>
		
		    <div style="display: none;" id="save_quote">
			<div>
			    <form id="frmSaveQuote" name="frmSaveQuote" action="{$site_url}instant_quote/saveQuote" method="post">
				<h4>Email a quote</h4>
				Enter Your Email Address: 
				<input type="text" name="emailAddress" id="emailAddress">
				<input type="button" onclick="saveQuote();" value="Email a quote" id="savebutton" style="float:right;margin-right: 110px;width: 26%;"> 
			    </form>
			</div>
		    </div>
		
		
		    <div style="display: none" id="sadfadsf">
			<div id="upload-artwork" style="width:1200px;">
			 
			    <form id="frmUploadArtwork" method="post" name="frmUploadArtwork" action="{$site_url}instant_quote/uploadArtwork" enctype="multipart/form-data">
				
				<div style="width:50%; float: left; margin-right:20px;">
				    <h4>Upload Artwork</h4>
				    <lable>Please upload the gif,jpg,jpeg,png,bmp,eps,psd,ps,ai,tif,tiff,pdf,cdr extension files</lable>
				    <br>
				    <lable style="font-weight: bold;color: red;" id="errorMsgg">
					   
				    </lable>
				    
				    <div>
					<table>
					    {section name=i loop=$printLocations}
					    
					    <tr style="display: none;" id="artwork_location_{$printLocations[i]['iPrintLocationId']}">
						<td>{$printLocations[i]['vPrintLocation']}</td>
						<td>Upload artwork:
						    <br>
						    <input type="file" id="{$printLocations[i]['iPrintLocationId']}" name="artwork_img-{$printLocations[i]['iPrintLocationId']}"  onchange="return imagename(this.value,'first',{$printLocations[i]['iPrintLocationId']});">
						</td>
						<td><textarea name="artwork_comment[{$printLocations[i]['iPrintLocationId']}]" placeholder="write your comments here..."></textarea></td>
					    </tr>
					    {/section}
					</table>
				    </div>
				</div>				
				<div style="width:40%; float: left;" id="printLocationNumbers">
				    <h4>Add Names & Numbers</h4>
				    <div>
					<table id="nameAndNumberTABLE">
					    <tr>
						  <td id="NumberFront">
							 Number Front
						  </td>
						  <td id="NumberBack">
							 Number Back
						  </td>
						  <td id="CustomNameBack">
							 Custom Name Back
						  </td>
						  <td>
							 Size
						  </td>
					   </tr>
					    {for $i=0 to 9}
						<tr id="nameAndNumberTR_{$i}">
						    <td id="NumberFrontTD_{$i}">
							<input type="text" style="width:70px;">
						    </td>
						    <td id="NumberBackTD_{$i}">
							<input type="text" style="width:70px;">
						    </td>
						    <td id="CustomNameBackTD_{$i}">
							<input type="text" style="width:120px;">
						    </td>
						    <td>
							<select style="width:70px;" >
							    {section name=j loop=$sizes}
							    <option>{$sizes[j].vSize}</option>
							    {/section}
							</select>
						    </td>
						</tr>
					    {/for}								
					</table>
				    </div>
				</div>

				<div>
				    <input onclick="submitArtwork()"  type="button" value="Proceed to Cart" class="instnt_btn" id="proceed_addcartsdsad22" style="float: right;" >
				</div>
				
			    </form>
			    
			    
			</div>
		    </div>
		    {literal}
			<script>
			    var numDecorationService = 0;
			    function changeTextboxQty(val) {
				//alert(val);
				$('.color_size').find('span').each(function() {
					var textTag = $(this).html();
					//alert($(textTag).attr('value'));
				});
				
				if (val != 0) {
				    $('#decorationService').show();
				    $('#noneDecorationSelection').hide();
				}
			    }
			    
			    function countQtyTotal(iColorId, val) {
				if ($('#Module_ApparelQuote_colorSwatch_'+iColorId).html() != '') {
				    totalQty += parseInt(val);
				}
				fianlTotalQty=totalQty;
			    }
			    
			    var numSizeBoxs = 0;
			    function checkboxcheck(colorId) {
				if ($('#Module_ApparelQuote_colorSwatch_'+colorId).html() == '') {
				    $('#Module_ApparelQuote_colorSwatch_'+colorId).html('<img src="http://www.fairfaxscreenprinting.com/module_support/ApparelQuote/check.png" class="transpng">');
				    if (numSizeBoxs == 0) {
					$('#noneSizeSelected').hide();
					$('#colorsizebox').append('<tr id="finalcolorsizebox_root" style="color:#002F5F;">'+$('#colorsizebox_root').html()+'</tr>');
				    }
				    $('#colorsizebox').append('<tr id="finalcolorsizebox_'+colorId+'">'+$('#colorsizebox_'+colorId).html()+'</tr>');
				    numSizeBoxs++;
				}else{
				    $('#Module_ApparelQuote_colorSwatch_'+colorId).html('');
				    $('#finalcolorsizebox_'+colorId).remove();
				    numSizeBoxs--;
				    if (numSizeBoxs == 0) {
					$('#finalcolorsizebox_root').remove();
					$('#noneSizeSelected').show();
				    }
				}
			    }
			</script>
		    {/literal}
		</div>
    	    </div>
	    
	    </div>
	    
	    
	    
	    {literal}
	    
	    <script>
		var totalQty = 0;
		
		var ele_numberFront=[];
		  $('td[id^="NumberFrontTD_"]').each(function(){
			var $this=  $(this);
			ele_numberFront.push($this.attr("id"));	    
		  });
		  var numberFrontTds=ele_numberFront;
		  var numberFrontTds_length=numberFrontTds.length;		
		
		var ele_numberBack=[];
		  $('td[id^="NumberBackTD_"]').each(function(){
			var $this=  $(this);
			ele_numberBack.push($this.attr("id"));	    
		  });
		  var numberbacktds=ele_numberBack;
		  var numberbacktds_length=numberbacktds.length;
		  
		  
		  var ele_customBack=[];
		  $('td[id^="CustomNameBackTD_"]').each(function(){
			var $this=  $(this);
			ele_customBack.push($this.attr("id"));	    
		  });
		  var cumtomNameBackTds=ele_customBack;
		  var cumtomNameBackTds_length=cumtomNameBackTds.length;
		  
		  
		   
		
		
		$("#upload_artwork_cart").click(function(){		  
		   var someObj={};
		  someObj.checked=[];
		  someObj.unchecked=[];		  
		  $(".print_location_text:input:checkbox").each(function(){
			 var $this = $(this);	
			 if($this.is(":checked")){
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.checked.push(finalid);
			 }else{
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.unchecked.push(finalid);
			 }
		  });		  
	       var all_cheked_checkbox=someObj.checked;
		  var len=all_cheked_checkbox.length;
		  var json = JSON.stringify(all_cheked_checkbox);	  
		  var site_url='{/literal}{$site_url}{literal}';
		  var extra='';				
		  extra+='?json='+json;		  		  
		  var url=site_url+'instant_quote/checkPrintLocation';			 
		  var pars=extra;
		  
		  $('#clickUploadArtwork').hide();
		  $('#upload_artwork_cart').hide();
		  $('#ajax_loader').show();		  
		  $.post(url+pars,			    
			 function name(data){				
			  $('#ajax_loader').hide();			
			   if (data.trim()=='Number Front,Number Back,Custom Name Back'){
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").show();
				$("#CustomNameBack").show();
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).show();
				    $("#"+cumtomNameBackTds[i]).show();	    
				}				
			   }else if (data.trim()=='Number Front,Number Back') {
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").show();
				$("#CustomNameBack").hide();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).show();
				    $("#"+cumtomNameBackTds[i]).hide();	    
				}		
			   }else if (data.trim()=='Number Front,Custom Name Back') {
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").hide();
				$("#CustomNameBack").show();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).hide();
				    $("#"+cumtomNameBackTds[i]).show();	    
				}			
			   }else if (data.trim()=='Number Back,Custom Name Back') {
				$("#printLocationNumbers").show();
				$("#NumberFront").hide();
				$("#NumberBack").show();
				$("#CustomNameBack").show();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).hide();
				    $("#"+numberbacktds[i]).show();
				    $("#"+cumtomNameBackTds[i]).show();	    
				}		
			   }
			   
			   else if (data.trim()=='NumberFront'){
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").hide();
				$("#CustomNameBack").hide();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).hide();
				    $("#"+cumtomNameBackTds[i]).hide();	    
				}					
			  }else if (data.trim()=='NumberBack') {
				$("#printLocationNumbers").show();				
				$("#NumberBack").show();
				$("#NumberFront").hide();
				$("#CustomNameBack").hide();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberbacktds[i]).show();
				    $("#"+numberFrontTds[i]).hide();
				    $("#"+cumtomNameBackTds[i]).hide();	    
				}			
			  }else if (data.trim()=='CustomNameBack') {
				$("#printLocationNumbers").show();				
				$("#NumberBack").hide();
				$("#NumberFront").hide();
				$("#CustomNameBack").show();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+cumtomNameBackTds[i]).show();
				    $("#"+numberFrontTds[i]).hide();
				    $("#"+numberbacktds[i]).hide();
				    //$("#"+cumtomNameBackTds[i]).hide();	    
				}			
			  }		  
			  else{
				$("#printLocationNumbers").hide();
			  }
			  
			  $('#clickUploadArtwork').click();		  
			  $('#clickUploadArtwork').show();
			  $('#upload_artwork_cart').show();
			  
			 });	 
		});
		
		function proceedCartFun() {
		   window.location = site_url + 'instant_quote/addtocart';
		}
		         
		function uploadArtworkAndProceedCart(){		  
		    $('#frmGetQuote').submit();
		}
	
	
		$(document).ready(function(){ 
			$('#frmGetQuote').ajaxForm(function(data) {			      
			      $('#quotePrice').html(data);
			      //alert(data);
				 $("#upload_artwork_cart").show();		 
			      return false;
			});
		});
		
		function submitArtwork() {
			$('#frmUploadArtwork').ajaxSubmit({
				success: function(data){
				    //alert(data);
				    proceedCartFun();
				}
			});
		}
		function saveQuote() {		    
		   hidefile();
		   var email=$("#emailAddress").val();
		   var site_url='{/literal}{$site_url}{literal}';
		   var extra="";
		   extra+='?email='+email;
		   var pars=extra;
		   var url=site_url+'instant_quote/saveQuote';
		   //alert(url+pars);return false;
		   $('#Quote').html('<div style="color:#c03b37; font-size:15px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Saving...</div>');
		   $.post(url+pars,
			function(data){			  		  			 
			  $('#Quote').html('<a style="color: #9E053B; font-size: 16px; text-align: center;" href="#save_quote" id="saveQuote">Save Quote</a>');				  
			}
		    );
		   
		  }
			
		function hidefile(){
			 $.fancybox.close(); 
		}
		
		$('#clickUploadArtwork').fancybox({
		    'overlayShow'	: true,
		    'openEffect'	: 'elastic',
		    'openSpeed'	: 250,
		    'closeEffect'	: 'elastic',
		    'closeSpeed'	: 250,
		});
			
		$('#saveQuote').fancybox({
		    'overlayShow'	: true,
		    'openEffect'	: 'elastic',
		    'openSpeed'	: 250,
		    'closeEffect'	: 'elastic',
		    'closeSpeed'	: 250,
		});
		function hidefile(){
		    $.fancybox.close(); 
		}
			
		 var fianlTotalQty='';	
		function selectPrintLocation(iPrintLocationId){		  
		  if ($("#print_location_checks_"+iPrintLocationId).is(':checked')){
			 $('#artwork_location_'+iPrintLocationId).show();
		    }else{			
			$('#artwork_location_'+iPrintLocationId).hide();
		  }
		  $(".print_location_text:input:checkbox").each(function(){
			 $(this).attr('disabled','disabled');
		  });	  
		  var someObj={};
		  someObj.checked=[];
		  someObj.unchecked=[];		  
		  $(".print_location_text:input:checkbox").each(function(){
			 var $this = $(this);	
			 if($this.is(":checked")){
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.checked.push(finalid);
			 }else{
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.unchecked.push(finalid);
			 }
		  });		  
	       var all_cheked_checkbox=someObj.checked;
		  var len=all_cheked_checkbox.length;		  
		  if (len>0){					  
				var json = JSON.stringify(all_cheked_checkbox);	  
				var site_url='{/literal}{$site_url}{literal}';
				var iProductId='{/literal}{$iProductId}{literal}';		  
				var extra='';
				extra+='?iProductId='+iProductId;
				extra+='&json='+json;		  
				extra+='&fianlTotalQty='+fianlTotalQty;		  
				var url=site_url+'instant_quote/ajax_printlocation';			 
				var pars=extra;
				//alert(url+pars);return false;
				$('#ajaxdiv').html('<div style="color:#c03b37;float: center;margin-top: 58px;padding: 10px; font-size:16px;text-align:center "><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');		  
				$.post(url+pars,			    
					  function name(data){					   
					  $("#ajaxdiv").html(data);
					  selectDecorationType('screenprinting',iPrintLocationId)
					   $(".print_location_text:input:checkbox").each(function(){
						   $(this).removeAttr('disabled','disabled');
					   });
				    });
			 }else{
				$(".print_location_text:input:checkbox").each(function(){
				    $(this).removeAttr('disabled','disabled');
			    });
				$("#ajaxdiv").html('');
			 }
		  }
		  function selectDecorationType(decorationType, iPrintLocationId) {		    
		    if (decorationType == 'screenprinting') {
			$('#screenprint2_'+iPrintLocationId).show();
		    }else{
			$('#screenprint2_'+iPrintLocationId).hide();
		    }	    
		  }
		
    $("#fancyhref").fancybox({
	padding: 0,
	openEffect : 'elastic',
	openSpeed  : 250,
	closeEffect : 'elastic',
	closeSpeed  : 250,
	hideOnOverlayClick: false,
	hideOnContentClick:false,
	autoScale : false,
	frameWidth  : 100,
	frameHeight : 600,
	helpers :{
	    closeClick: false
	}
    });
    
    
    function getProduct(id) {
	var extra = '';
	extra+='?iCategoryId='+id;
	var url = site_url + 'instant_quote/ajax_categaorydata';
	$('#maindivHide').html('<div style="color:#c03b37; font-size:16px;text-align:center "><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');
	var pars = extra;
	$.post(url+pars,
	    function(data) {
	      $('#update_div').html(data);
	    }
	);
    }
    function imagename(val,name,id){
	   //alert(id);
		$("#"+name).text(val);
		$("#firstimage").val(val);		
		var a = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
		if(a == 'gif' || a == 'GIF' || a == 'jpg'  ||a == 'JPG' ||a == 'jpeg' ||a == 'JPEG' ||a == 'png' ||a == 'bmp' ||a == 'BMP' ||a == 'eps' ||a == 'EPS' ||a == 'psd' ||a == 'PSD' ||a == 'ps' ||a == 'PS' ||a == 'ai' ||a == 'AI' ||a == 'tiff' ||a == 'TIFF' ||a == 'tif' ||a == 'TIF' ||a == 'pdf' ||a == 'PDF' ||a == 'cdr' ||a == 'CDR'      ){	   
		return true;	
		}
		else{
			//alert('extension not valid');			
			$("#errorMsgg").html('The extension is not valid , Please use valid extension');			
			$("#"+id).val("");
			return false;
		}		
	}
	
	function showColorImage(iColorId){	   
	  var iProductId='{/literal}{$iProductId}{literal}';
	  var colorImage=iProductId+iColorId;	  
	  $("#mainImage").hide();  
	  $("#colorimage_"+colorImage).show();	  
	}
	
	function hideColorImage(iColorId){
	   var iProductId='{/literal}{$iProductId}{literal}';
	  var colorImage=iProductId+iColorId;
	  $("#colorimage_"+colorImage).hide();	  
	  $("#mainImage").show();  
	  
	}
	

</script>
{/literal}
	    
        </div>
    </div>
</div>
{include file="footer.tpl"}