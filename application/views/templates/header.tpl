<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 8 ]><html class="ie8"<![endif]-->
<!--[if IE 9 ]><html class="ie9"<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$pageTitle}</title>
<meta name="description" content="Default Description" />
<meta name="keywords" content="Magento, Varien, E-commerce" />
<meta name="robots" content="INDEX,FOLLOW" />
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="{$front_css_path}widgets.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}general.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}productslist.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}productsscroller.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}cooslider.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}animate.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}print.css" media="print" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}custom-bootstrap.css" media="print" />

<script type="text/javascript" src="{$front_js_path}prototype.js"></script>
<script type="text/javascript" src="{$front_js_path}slider.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="{$front_js_path}bootstrap.min.js"></script>
<script type="text/javascript" src="{$front_js_path}bootstrap-tooltip.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery.mtlib.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery.selectbox-0.2.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery.filter.js"></script>
<script type="text/javascript" src="{$front_js_path}jquery.blockUI.js"></script>
<script type="text/javascript" src="{$front_js_path}noConflict.js"></script>
<script src="{$front_js_path}jquery-1.2.6.pack.js" type="text/javascript"></script>
<script src="{$front_js_path}jquery.flow.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{$front_js_path}mt.accordion.js"></script>

<script type="text/javascript">
    var site_url = '{$site_url}';
</script>

<link href="{$front_css_path}default.css" rel="stylesheet" type="text/css" />
<script src="{$front_js_path}/mColorPicker.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{$front_css_path}styles-violet.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}custom-bootstrap.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}bootstrap-responsive.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="{$front_css_path}styles-responsive.css" media="all" />
<link href='{$front_css_path}fonts.css' rel='stylesheet' type='text/css'/>
<link href='{$front_css_path}fontslat.css' rel='stylesheet' type='text/css'/>
<link href='{$front_css_path}main.css' rel='stylesheet' type='text/css'/>

</head>
		
		<div class="header-container">
			<div class="header">
				<div class="container">
					<div class="row-fluid show-grid">
						<div class="span4"> <a href="{$site_url}" title="Emblemax" class="logo pull-left"><strong>Magento Commerce</strong><img src="{$front_image_path}logo.png" alt="Magento Commerce" /></a> </div>
							<div class="span6">
							<div class="conno">{$config_tollfree}</div>
							<div style="clear:both;"></div>
						<div class="row-fluid show-grid">
							<div class="grid-col">
								<div class="mt-show-right pull-right">
									<div class="top-search pull-left"> <span class="search-icon" style="font-size:11px; color:#002f5f; padding:0 0 0 13px; text-transform:uppercase; font-weight:bold;">Search</span>
										<div class="mt-search-form">
											<form id="search_mini_form" action="{$site_url}search" method="get">
												<div class="search-form-border"></div>
												<div class="form-search">
													<input id="search" type="text" name="q" value="" class="input-text" maxlength="128" />
													<button type="submit" title="Search" class="button"> <span style="background:#9e0546;"><span style="background:#9e0546; font-family:Arial, Helvetica, sans-serif;">Search</span></span> </button>
													<div id="search_autocomplete" class="search-autocomplete"></div>
												</div>
											</form>
										</div>
									</div>
									<div class="top-cart pull-left"><span class="cart-loading">Loading...</span>
										<div class="cart"> <a href="{$site_url}cart" class="mt-icon-ajaxcart" style="font-size:11px; color:#002f5f; padding:0 0 0 18px; text-transform:uppercase; font-weight:bold;">cart</a> <span class="mt-cart-label"> Bag:(0) </span>
											<div class="mtajaxcart">
												<div class="search-form-border"></div>
												<div class="ajax-container">
													<p class="no-items-in-cart">You have no items in your shopping cart.</p>
												</div>
											</div>
										</div>
									</div>
									{if $smarty.session.sess_UserName neq ''}	
									<div class="top-link pull-left" style="margin:0 0 0 0;"> <span class="link-icon hidden-phone" style="font-size:11px; color:#002f5f; padding:0 16px 0 18px;display: inline; text-transform:uppercase; font-weight:bold;">Welcome, {$smarty.session.sess_UserName} </span>
									{else}
									<div class="top-link pull-left" style="margin:0 0 0 0;"> <a href="{$site_url}login"><span class="link-icon hidden-phone" style="font-size:11px; color:#002f5f; text-transform:uppercase; font-weight:bold;">Login</span></a>
									{/if}
										<div class="mt-top-link">
											<div class="search-form-border"></div>
											{if $smarty.session.sess_UserName}
											<div class="mt-top-link-inner">
											    <ul class="links">
												<li class="first" ><a href="{$site_url}myAccount/myAccount" title="My Account" >My Account</a></li>
                                                                                                <li ><a href="#" title="My Wishlist" >My Wishlist</a></li>
                                                                                                <li ><a href="#" title="My Cart" class="top-link-cart">My Cart</a></li>
                                                                                                <li ><a href="#" title="Checkout" class="top-link-checkout">Checkout</a></li>
                                                                                                <li class=" last" ><a href="{$site_url}authentication/logout" title="Log In" >Log Out</a></li>
                                                                                            </ul>
											</div>
											{/if}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						
						<div class="mt-drill-menu clearfix">
							<div class="mt-drillmenu hidden-desktop">
								<div class="navbar">
									<div class="navbar-inner">
										<div class="mt-nav-container">
											<div class="block-title clearfix"> <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <span class="brand">Navigation</span> </div>
											<div class="nav-collapse collapse"> 
												
												<ul id="mt_accordionmenu" class="nav-accordion">
													<li><a href="{$site_url}">Home</a></li>
													<li class="item nav-categories hasChild"> <a class="item" href="{$site_url}category">Categories</a>
													</li>
													<li class="item nav-kids"> <a class="item" href="#">KIDS</a> </li>
													<li class="item nav-men"> <a class="item" href="#">Men</a> </li>
													<li class="item nav-women hasChild"> <a class="item" href="#">Women</a>
														<ul class="detail-parent">
															<li class="item nav-women-dresses"> <a class="item" href="#">Dresses</a> </li>
															<li class="item nav-women-day"> <a class="item" href="#">Day</a> </li>
															<li class="item nav-women-evening"> <a class="item" href="#">Evening</a> </li>
															<li class="item nav-women-sundresses"> <a class="item" href="#">Sundresses</a> </li>
															<li class="item nav-women-sweater"> <a class="item" href="#">Sweater</a> </li>
															<li class="item nav-women-belts"> <a class="item" href="#">Belts</a> </li>
															<li class="item nav-women-blouses-and-shirts"> <a class="item" href="#">Blouses and Shirts</a> </li>
															<li class="item nav-women-t-shirts"> <a class="item" href="#">T-Shirts</a> </li>
															<li class="item nav-women-cocktail"> <a class="item" href="#">Cocktail</a> </li>
															<li class="item nav-women-hair-accessories"> <a class="item" href="#">Hair Accessories</a> </li>
															<li class="item nav-women-hats-and-gloves"> <a class="item" href="#">Hats and Gloves</a> </li>
															<li class="item nav-women-lifestyle"> <a class="item" href="#">Lifestyle</a> </li>
															<li class="item nav-women-bras"> <a class="item" href="#">Bras</a> </li>
															<li class="item nav-women-scarves"> <a class="item" href="#">Scarves</a> </li>
															<li class="item nav-women-small-leathers"> <a class="item" href="#">Small Leathers</a> </li>
															<li class="item nav-women-accessories hasChild"> <a class="item" href="#">Accessories</a>
																<ul class="detail-parent">
																	<li class="item nav-women-accessories-sunglasses last"> <a class="item" href="#">Sunglasses</a> </li>
																</ul>
															</li>
															<li class="item nav-women-evening-23"> <a class="item" href="#">Evening</a> </li>
															<li class="item nav-women-long-sleeved"> <a class="item" href="#">Long Sleeved</a> </li>
															<li class="item nav-women-short-sleeved"> <a class="item" href="#">Short Sleeved</a> </li>
															<li class="item nav-women-sleeveless"> <a class="item" href="#">Sleeveless</a> </li>
															<li class="item nav-women-tanks-and-camis"> <a class="item" href="#">Tanks and Camis</a> </li>
															<li class="item nav-women-tops hasChild"> <a class="item" href="#">Tops</a>
																<ul class="detail-parent">
																	<li class="item nav-women-tops-tunics-and-kaftans last"> <a class="item" href="#">Tunics and Kaftans</a> </li>
																</ul>
															</li>
															<li class="item nav-women-totes"> <a class="item" href="#">Totes</a> </li>
															<li class="item nav-women-clutches"> <a class="item" href="#">Clutches</a> </li>
															<li class="item nav-women-cross-body"> <a class="item" href="#">Cross Body</a> </li>
															<li class="item nav-women-satchels"> <a class="item" href="#">Satchels</a> </li>
															<li class="item nav-women-shoulder"> <a class="item" href="#">Shoulder</a> </li>
															<li class="item nav-women-briefs"> <a class="item" href="#">Briefs</a> </li>
															<li class="item nav-women-handbags"> <a class="item" href="#">Handbags</a> </li>
															<li class="item nav-women-camis"> <a class="item" href="#">Camis</a> </li>
															<li class="item nav-women-nightwear"> <a class="item" href="#">Nightwear</a> </li>
															<li class="item nav-women-shapewear"> <a class="item" href="#">Shapewear</a> </li>
															<li class="item nav-women-lingerie last"> <a class="item" href="#">Lingerie</a> </li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				
				<div class="mainnav">
					<div class="container">
						<div class="naviga span8">
							<div class="row-fluid show-grid mt-nav">
								<div class="mt-main-menu clearfix"> 
									<!-- navigation BOF -->
									<div class="mt-navigation visible-desktop">
										<div class="mt-main-menu">
											<ul id="nav" class="megamenu pull-right">
												<li class="level0 home level-top"> <a href="{$site_url}" {if $isActiveMenu eq 'home'} class="level-top actnav" {/if}><span>Home</span></a> </li>
												<li class="level0 nav-2 level-top"> <a href="http://www.companycasuals.com/emblemax/start.jsp" target="_blank" class="level-top"> <span>Apparel</span> </a> </li>
												<li class="level0 nav-3 level-top"> <a href="http://www.emblemax.net/" target="_blank" class="level-top"> <span>Promotional Products</span> </a> </li>
												<li class="level0 nav-4 level-top last parent"> <a href="{$site_url}instant-quote" {if $isActiveMenu eq 'instant-quote'} class="level-top actnav" {/if}><span>Get a Quote</span> </a>
                                                                                                <!--<li class="level0 nav-4 level-top last parent"> <a href="{$site_url}design-studio" class="level-top"> <span>Design Studio</span> </a>-->
												<li class="level0 nav-4 level-top last parent"> <a href="#" class="level-top"> <span>Preferred Supplier</span> </a>
													<div class="sub-wrapper">
														<ul class="level0">
															<li class="first">
																<ol>
																	<li class="level1 nav-4-1 first last"> <a href="http://www.emblemax.com/leeds.php" target="_blank"> <span>Leeds</span> </a> </li>
																	<li class="level1 nav-4-2 last"> <a href="http://www.emblemax.com/awardcraft.php" target="_blank"> <span>Award Craft</span> </a> </li>
																	<li class="level1 nav-4-3 last"> <a href="http://www.emblemax.com/highline.php" target="_blank"> <span>High Caliber</span> </a> </li>
																	<li class="level1 nav-4-4 last"> <a href="http://www.emblemax.com/lanco.php" target="_blank"> <span>Lanco</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/gemline.php" target="_blank"> <span>Gemline</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/primeline.php" target="_blank"> <span>Prime Line</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/bicgraphic.php" target="_blank"> <span>Bic Graphics USA</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/cutterbuck.php" target="_blank"> <span>Cutter & Buck</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/bulletline.php" target="_blank"> <span>Bullet Line</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/logomark.php" target="_blank"> <span>Logomark</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/wearablescatalog.php" target="_blank"> <span>Wearables Catalog</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/alternative.php" target="_blank"> <span>Alternative</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/snugz.php" target="_blank"> <span>Snugz Lanyards</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/hitpromo.php" target="_blank"> <span>Hit Promo</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/crystal.php" target="_blank"> <span>Crystal Images</span> </a> </li>																
																	
						    										</ol>
															</li>
															<li class="menu-static-blocks"></li>
														</ul>												
                                                                                                <li class="level0 nav-1 level-top first parent"> <a href="{$site_url}aboutus" {if $isActiveMenu eq 'aboutus'} class="level-top actnav" {/if}> <span>About Us</span> </a>
                                                                                                <li class="level0 level-top parent custom-block "> <a href="{$site_url}faq" {if $isActiveMenu eq 'faq'} class="level-top actnav" {/if}> <span>FAQ</span> </a> </li>
													<div class="sub-wrapper">
														<ul class="level0">
															<li class="first">
																<ol>
																	<li class="level1 nav-4-1 first last"> <a href="#"> <span>01</span> </a> </li>
																	<li class="level1 nav-4-2 last"> <a href="#"> <span>02</span> </a> </li>
																	<li class="level1 nav-4-3 last"> <a href="#"> <span>Evening</span> </a> </li>
																	<li class="level1 nav-4-4 last"> <a href="#"> <span>Sundresses</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="#"> <span>Sweater</span> </a> </li>											
																	
																	<li class="level1 nav-4-16 last parent"> <a href="#"> <span>Accessories</span> </a>
																		<div class="sub-wrapper">
																			<ul class="level1">
																				<li class="level2 nav-4-16-1 first last"> <a href="#"> <span>Sunglasses</span> </a> </li>
																				<li class="menu-static-blocks"></li>
																			</ul>
																		</div>
																	</li>
																	<li class="level1 nav-4-17 last"> <a href="#"> <span>Evening</span> </a> </li>
																	
																	<li class="level1 nav-4-22 last parent"> <a href="#"> <span>Tops</span> </a>
																		<div class="sub-wrapper">
																			<ul class="level1">
																				<li class="level2 nav-4-22-2 first last"> <a href="#"> <span>Tunics and Kaftans</span> </a> </li>
																				<li class="menu-static-blocks"></li>
																			</ul>
																		</div>
																	</li>
																	<li class="level1 nav-4-23 last"> <a href="#"> <span>Totes</span> </a> </li>
																	<li class="level1 nav-4-24 last"> <a href="#"> <span>Clutches</span> </a> </li>
																</ol>
															</li>
															<li class="menu-static-blocks"></li>
														</ul>
													</div>
												</li>
												<li class="level0 nav-4 level-top last parent"> <a href="#" > <span>Contact</span> </a>
													<div class="sub-wrapper">
														<ul class="level0">
															<li class="first">
																<ol>
																	<li class="level1 nav-4-1 first last"> <a href="{$site_url}contactus" > <span>Contact Us</span> </a> </li>
																	<li class="level1 nav-4-2 last"> <a href="{$site_url}request_catalog" > <span>Request Catalog</span> </a> </li>
						    										</ol>
															</li>
															<li class="menu-static-blocks"></li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</div>
									{literal}
									<script type="text/javascript"> 
									$mtkb(function(){ 
										$mtkb(".megamenu").megamenu({
											'animation':'slide', 
											'mm_timeout': 150
										}); 
									});  
									</script>
									{/literal}
									<!-- navigation EOF --> </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
{if $name eq 'home' OR $name eq 'category'}
<script type='text/javascript' src='{$front_js_path}jquery.easing.1.3.js'></script>
<script type='text/javascript' src='{$front_js_path}jquery.coda-slider-2.0.js'></script>
{literal}
<script type="text/javascript">
    /*$(window).load(function() {
        //$('#coda-slider-1').html('<div style="color:#c03b37; font-size:15px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');
	});
    
    */
    
	  $(window).load(function() {        
		$('#coda-slider-1').codaSlider({
		    //autoSlide: true,
		    //autoSlideInterval: 3000,
		}
		);
		
	});
	
</script>
	 
<script type="text/javascript">
	$mtkb(document).ready(function(){
		$mtkb("#select-language").selectbox();
    	$mtkb("#select-currency").selectbox();
    	//$mtkb("#select_9").selectbox();
    	$mtkb("#mt_sort_by").selectbox();
    	$mtkb("#mt_limiter").selectbox();
	});
</script>
{/literal}
<div class="coda-slider-wrapper">
	<div class="coda-slider preload" id="coda-slider-1">            
                {if $homeBanner|@count gt 0}
                {section name=i loop=$homeBanner} 
		<div class="panel">
                        <div class="panel-wrapper">							
                    <h2 class="title" style="display:none; float:none; margin:0;">{$homeBanner[i]['vBottomTitle']}</h2>							
				<div class="slidcont">
					<div class="prodimgpart"><img src="{$upload_path}/home_slider/{$homeBanner[i]['iHomeSliderId']}/1170X468_{$homeBanner[i]['vImage']}" alt="" /></div>
					    {if $homeBanner[i]['eShowDeliveryDate'] eq 'Yes'}
					    <div class="datapart">
						    <h3>{$homeBanner[i]['vTitle']}</h3>
						    <p>{$homeBanner[i]['tShortDescription']}</p>
						    <div class="slidbtn"><a href="{$homeBanner[i]['vGetStarted']}">Get Started</a></div>
						    <div class="calendorpt">
							    <div class="estdate">Estimated delivery date<span style="color: red;">*</span></div>
							    <div class="calone">
								    <p class="standerdtx">Standard</p>
								    <p class="monthcal">{$standardDeliveryMonth}</p>
								    <p class="datecal"><strong>{$standardDeliveryDay}</strong></p>
							    </div> 
							    <div class="calone">
								    <p class="standerdtx">Rush</p>
								    <p class="monthcal">{$rushDeliveryMonth}</p>
								    <p class="datecal"><strong>{$rushDeliveryDay}</strong></p>
							    </div>
							    <p style="color: red;">*Need it faster? Contact us for options.</p>
						    </div>
						    
					    </div>
					    {else}
					    <div class="datapart_without_deliverydate">
						    <h3>{$homeBanner[i]['vTitle']}</h3>
						    <p>{$homeBanner[i]['tShortDescription']}</p>
						    <!--{if $homeBanner[i]['vGetStarted'] neq ''} -->
						    <div class="slidbtn"><a href="{$homeBanner[i]['vGetStarted']}">Get Started</a></div>
						    <!--{/if}-->
					    </div>
					    {/if}
				</div>
			</div>
		</div>
                {/section}
                {/if}
	</div><!-- .coda-slider -->
</div>
{/if}


