{include file="header.tpl"}
  
<div class="main-container col2-right-layout">
<form action="#" method="post" id="form-validate" >
			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
						            <div class="main container">
			<div class="slideshow_static"></div>
                       <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>			
                <div class="main-inner">
                    
                    <div class="col-main">
                                                <div class="account-create">
    			<h2 class="pagetital">Create an Account</h2>
			<div style="clear:both;"></div>
           
        <div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Personal Information</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>First Name</label>
    <div class="input-box">
        <input type="text" id="vFirstName" name="Data[vFirstName]" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"/>
    </div>
    <div class="validation-advice" id="firstNameDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Last Name</label>
    <div class="input-box">
        <input type="text" id="vLastName" name="Data[vLastName]" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
     <div class="validation-advice" id="lastNameDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Telephone</label>
    <div class="input-box">
        <input type="text" id="vTelePhone" name="Data[vPhone]" value="" title="Telephone" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="phoneDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em></em>Fax</label>
    <div class="input-box">
        <input type="text" id="vFax" name="Data[vFax]" value="" title="Fax" maxlength="255" class="input-text span12 required-entry"  />
    </div>
</div>
</div>
                </li>
                <li>
                    <label for="email_address" class="required"><em>*</em>Email Address</label>
                    <div class="input-box">
                        <input type="text" name="Data[vEmail]" id="vEmail" value="" title="Email Address" class="input-text validate-email required-entry span12" />
                    </div>
		    <div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
                </li>
                                <li class="control">
                    <div class="input-box">
                        <input type="checkbox" name="Data[vNewsletter]" title="Sign Up for Newsletter" value="chek" id="vNews" class="checkbox" />
                    </div>
                    <label for="is_subscribed">Sign Up for Newsletter</label>
                </li>
                                                            </ul>
</div>

<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Your Address</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
  <div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Address</label>
    <div class="input-box">
        <input type="text" id="tAddress" name="Data[tAddress]" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="addressDiv" style="display:none; float: left;"></div>
</div>  
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>City</label>
    <div class="input-box">
        <input type="text" id="vCity" name="Data[vCity]" value="" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="cityDiv" style="display:none; float: left;"></div>
</div>

</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Post Code</label>
    <div class="input-box">
        <input type="text" id="vZipCode" name="Data[vZipCode]" value="" title="Zipcode"  class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="zipcodeDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
                <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Select Country</label>
    <div class="input-box">
    		<select  class="input-text span12 required-entry" id="country" name="Data[iCountryId]" onchange="getStates(this.value);" title="Country">
		    <option value="">--Select Country--</option>
		{section name=i loop=$db_country}
    			<option value='{$db_country[i]->iCountryId}'>{$db_country[i]->vCountry}</option>
                {/section}
    		</select>
        
    </div>
    <div class="validation-advice" id="countryDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Select State</label>
    <div class="input-box">
        <select  class="input-text span12 required-entry" title="State" id="states" name="Data[iStateId]">
	    <option value=''>--Select State--</option>
    	</select>
    </div>
    <div class="validation-advice" id="statesDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
        </ul>
</div>

    <div class="fieldset">
        <h2 class="legend">Login Information</h2>
        <ul class="form-list row-fluid show-grid">
            <li>
                    <label for="email_address" class="required"><em>*</em>Username</label>
                    <div class="input-box">
                        <input type="text" name="Data[vUserName]" id="vUserName" value="" title="Username" class="input-text validate-email required-entry span12" />
                    </div>
		    <div class="validation-advice" id="usernameDiv" style="display:none; float: left;"></div>
                </li>
			 <li>
                    <label for="email_address" class="required"><em>*</em>Password</label>
                    <div class="input-box">
                        <input type="password" name="Data[vPassword]" id="vPassword" value="" title="Password" class="input-text validate-email required-entry span12" />
                    </div>
		    <div class="validation-advice" id="passwordDiv" style="display:none; float: left;"></div>
                </li>
			 <li>
                    <label for="email_address" class="required"><em>*</em>Confirm Password</label>
                    <div class="input-box">
                        <input type="password" name="Data[vPasswordAgain]" id="vPasswordAgain" value="" title="Password Again" class="input-text validate-email required-entry span12" />
                    </div>
		    <div class="validation-advice" id="passwordagainDiv" style="display:none; float: left;"></div>
                </li>
                                </ul>
	    
        <div id="window-overlay" class="window-overlay" style="display:none;"></div>
<div id="remember-me-popup" class="remember-me-popup" style="display:none;">
    <div class="remember-me-popup-head">
        <h3>What's this?</h3>
        <a href="#" class="remember-me-popup-close" title="Close">Close</a>
    </div>
    <div class="remember-me-popup-body">
        <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
        <div class="remember-me-popup-close-button a-right">
            <a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a>
        </div>
    </div>
</div>

    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="{$site_url}login" class="back-link"><small>&laquo; </small>Back</a></p>
       <button type="button" title="Submit" class="button" onclick="return CheckRegister();"><span><span>Submit</span></span></button>
    </div>
    </form>
</form>
<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
    </script>
</div>
                    </div>
					
                </div>				
				
									
								
            </div>
        </div>
			 
			 
        </div>
{include file="footer.tpl"}
{literal}
<script type="text/javascript">
         
function getStates(icountryid)
{
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}

function CheckRegister()
{
    var extra = '';
    var firstname =  $('#vFirstName').val();   
    var lastname =  $('#vLastName').val();
    var email = $('#vEmail').val();
    var phone = $('#vTelePhone').val();
    var fax = $('#vFax').val();
    var address = $('#tAddress').val();
    var city = $('#vCity').val();
    var country = $('#country').val();
    var state = $('#states').val();
    var zip = $('#vZipCode').val();
    var username = $('#vUserName').val();
    var password = $('#vPassword').val();
    var resetpass = $('#vPasswordAgain').val();    
    var newsletter  = $('#vNews').val();
    if ($('#vNews').is(":checked"))
    {
      newsletter = 'Yes';
    }else{
        newsletter = 'No';
    }
    
   
    if (document.getElementById('vFirstName').value == "" || document.getElementById('vLastName').value == "" ||
	document.getElementById('vEmail').value == "" || document.getElementById('vTelePhone').value == "" ||
	document.getElementById('tAddress').value == "" || document.getElementById('vCity').value == "" ||
	document.getElementById('country').value == "" || document.getElementById('states').value == "" ||
	document.getElementById('vZipCode').value == "" || document.getElementById('vUserName').value == "" ||
	document.getElementById('vPassword').value == "" || document.getElementById('vPasswordAgain').value == "") {
	$("#vFirstName").addClass("borderValidations");
	$('#firstNameDiv').html('This is a required field.');
	$('#firstNameDiv').show();
	$("#vLastName").addClass("borderValidations");
	$('#lastNameDiv').html('This is a required field.');
	$('#lastNameDiv').show();
	$("#vTelePhone").addClass("borderValidations");
	$('#phoneDiv').html('This is a required field.');
	$('#phoneDiv').show();
	$("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('This is a required field.');
	$('#emailDiv').show();
        $("#tAddress").addClass("borderValidations");
	$('#addressDiv').html('This is a required field.');
	$('#addressDiv').show();
	$("#vCity").addClass("borderValidations");
	$('#cityDiv').html('This is a required field.');
	$('#cityDiv').show();
	$("#vZipCode").addClass("borderValidations");
	$('#zipcodeDiv').html('This is a required field.');
	$('#zipcodeDiv').show();
	$("#country").addClass("borderValidations");
	$('#countryDiv').html('This is a required field.');
	$('#countryDiv').show();
	$("#states").addClass("borderValidations");
	$('#statesDiv').html('This is a required field.');
	$('#statesDiv').show();
	$("#vUserName").addClass("borderValidations");
	$('#usernameDiv').html('This is a required field.');
	$('#usernameDiv').show();
	$("#vPassword").addClass("borderValidations");
	$('#passwordDiv').html('This is a required field.');
	$('#passwordDiv').show();
	$("#vPasswordAgain").addClass("borderValidations");
	$('#passwordagainDiv').html('This is a required field.');
	$('#passwordagainDiv').show();
	
	//return false;		 
    }
 
  var validate = true;
    if( $('#vFirstName').val() == ''){
	$("#vFirstName").addClass("borderValidations");
	$('#firstNameDiv').html('This is a required field.');
	$('#firstNameDiv').show();
	validate = false;
    }
    else{
      $('#firstNameDiv').hide();
      $("#vFirstName").removeClass("borderValidations");
      //return false;
     }
     
  
    if( $('#vLastName').val() == ''){
        $("#vLastName").addClass("borderValidations");
	$('#lastNameDiv').html('This is a required field.');
	$('#lastNameDiv').show();
	validate = false;
    }
    
    else{
      $('#lastNameDiv').hide();
      $("#vLastName").removeClass("borderValidations");
      
    }
    
    if($('#vTelePhone').val() == ''){
        $("#vTelePhone").addClass("borderValidations");
	$('#phoneDiv').html('This is a required field.');
	$('#phoneDiv').show();
	validate = false;
	
    }   
    else{
      $('#phoneDiv').hide();
      $("#vTelePhone").removeClass("borderValidations");
       
    }
    
   var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var isvalid = emailRegexStr.test(email);
    if( $('#vEmail').val() == ''){
        $("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('This is a required field.');
	$('#emailDiv').show();
	validate = false;
    }
    else if (!isvalid) {
        $("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('Enter Your Email Address like demo@doman.com');
	$('#emailDiv').show();
	validate = false;
        }
   else{
      $('#emailDiv').hide();
      $("#vEmail").removeClass("borderValidations");
    }
    
    
    if( $('#tAddress').val() == ''){
        $("#tAddress").addClass("borderValidations");
	$('#addressDiv').html('This is a required field.');
	$('#addressDiv').show();
	validate = false;
       // $('#addressDiv').html('Enter Your Address'); $('#addressDiv').show(); return false;
    }
  else{
      $('#addressDiv').hide();
      $("#tAddress").removeClass("borderValidations");
    }
    
    if( $('#vCity').val() == ''){
        $("#vCity").addClass("borderValidations");
	$('#cityDiv').html('This is a required field.');
	$('#cityDiv').show();
	validate = false;
       //$('#cityDiv').html('Enter Your City'); $('#cityDiv').show(); return false;
    }
    else{
      $('#cityDiv').hide();
      $("#vCity").removeClass("borderValidations");
    }
    
    
    if( $('#vZipCode').val() == ''){
       $("#vZipCode").addClass("borderValidations");
	$('#zipcodeDiv').html('This is a required field.');
	$('#zipcodeDiv').show();
	validate = false;
       //$('#zipcodeDiv').html('Enter Your Zipcode'); $('#zipcodeDiv').show(); return false;
    }
    else{
      $('#zipcodeDiv').hide();
      $("#vZipCode").removeClass("borderValidations");
    }
    
    if( $('#country').val() == ''){
         $("#country").addClass("borderValidations");
	$('#countryDiv').html('This is a required field.');
	$('#countryDiv').show();
	validate = false;
       //$('#countryDiv').html('Select Country'); $('#countryDiv').show(); return false;
    }
    else{
      $('#countryDiv').hide();
      $("#country").removeClass("borderValidations");
    }
    
    if( $('#states').val() == ''){
        $("#states").addClass("borderValidations");
	$('#statesDiv').html('This is a required field.');
	$('#statesDiv').show();
	validate = false;
       //$('#statesDiv').html('Select State'); $('#statesDiv').show(); return false;
    }
    else{
      $('#statesDiv').hide();
      $("#states").removeClass("borderValidations");
    }
    
    if( $('#vUserName').val() == ''){
        $("#vUserName").addClass("borderValidations");
	$('#usernameDiv').html('This is a required field.');
	$('#usernameDiv').show();
	validate = false;
       //$('#usernameDiv').html('Enter Your Username'); $('#usernameDiv').show(); return false;
    }
    else{
      $('#usernameDiv').hide();
      $("#vUserName").removeClass("borderValidations");
    }
    
    if( $('#vPassword').val() == ''){
       $("#vPassword").addClass("borderValidations");
	$('#passwordDiv').html('This is a required field.');
	$('#passwordDiv').show();
	validate = false;
       //$('#passwordDiv').html('Enter Your Password'); $('#passwordDiv').show(); return false;
    }
    else if(password.length < 6 ){
     $("#vPassword").addClass("borderValidations");
	$('#passwordDiv').html("Your password must be 6 chacters. The one you entered had "+password.length+" characters");
	$('#passwordDiv').show();
	validate = false;
    }
    
    else{
      $('#passwordDiv').hide();
      $("#vPassword").removeClass("borderValidations");
    }
    
    if( $('#vPasswordAgain').val() == ''){
       $("#vPasswordAgain").addClass("borderValidations");
	$('#passwordagainDiv').html('This is a required field.');
	$('#passwordagainDiv').show();
	validate = false;
       //$('#passwordagainDiv').html('Enter Your Password again'); $('#passwordagainDiv').show(); return false;
    }
    else if( $('#vPasswordAgain').val() !=  $('#vPassword').val()){
        $("#vPasswordAgain").addClass("borderValidations");
	$('#passwordagainDiv').html('Password Is Not Match Please Enter again');
	$('#passwordagainDiv').show();
	validate = false;
       //$('#passwordagainDiv').html('Password Is Not Match Please Enter again'); $('#passwordagainDiv').show(); return false;
    }
    else{
      $('#passwordagainDiv').hide();
      $("#vPasswordAgain").removeClass("borderValidations");
    }
   
  if(validate){
  
  
   var firstname =  $('#vFirstName').val();   
    var lastname =  $('#vLastName').val();
    var email = $('#vEmail').val();
    var phone = $('#vTelePhone').val();
    var fax = $('#vFax').val();
    var address = $('#tAddress').val();
    var city = $('#vCity').val();
    var country = $('#country').val();
    var state = $('#states').val();
    var zip = $('#vZipCode').val();
    var username = $('#vUserName').val();
    var password = $('#vPassword').val();
    var resetpass = $('#vPasswordAgain').val();    
    var newsletter  = $('#vNews').val();
    var extra = '';
    
	  extra+='?vFirstName='+firstname;
	  extra+='&vLastName='+lastname;
          extra+='&vEmail='+email;
	  extra+='&vTelePhone='+phone;
	  extra+='&vFax='+fax;
          extra+='&tAddress='+address;
	  extra+='&vCity='+city;
          extra+='&vZipCode='+zip;
          extra+='&iCountryId='+country;
          extra+='&iStateId='+state;
          extra+='&vUserName='+username;
          extra+='&vPassword='+password;
	 extra+='&vNews='+newsletter;	
	var url = site_url + 'registration/check_register';
        var pars = extra;
         $.post(url+pars,
	      function(data) {

		if(data == 'allredy exists'){
			  $("#vUserName").addClass("borderValidations");
			 $('#usernameDiv').html('Username already exists.');
			 $('#usernameDiv').show();
			 validate = false;			 
		}
		if(data == 'email allredy exists'){
			  $("#vEmail").addClass("borderValidations");
			 $('#emailDiv').html('Email Address already exists.');
			 $('#emailDiv').show();
			 validate = false;			 
		}		
		if(data == 'success'){
			 window.location = site_url+'registration/registration_success';
		}
	      }
	 );
  }
  
    
}


</script>
{/literal}
