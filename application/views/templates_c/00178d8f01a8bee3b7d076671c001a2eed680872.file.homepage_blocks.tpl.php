<?php /* Smarty version Smarty-3.1.11, created on 2014-01-07 14:40:54
         compiled from "application/views/templates/admin/homepage_blocks/homepage_blocks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:49733132452025d89693394-15230423%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00178d8f01a8bee3b7d076671c001a2eed680872' => 
    array (
      0 => 'application/views/templates/admin/homepage_blocks/homepage_blocks.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49733132452025d89693394-15230423',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_52025d898f8c20_70358701',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'lang' => 0,
    'upload_path' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52025d898f8c20_70358701')) {function content_52025d898f8c20_70358701($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homepage_blocks/homepage_blockslist">Homepage Blocks</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Homepage Blocks<?php }else{ ?>Edit Homepage Blocks<?php }?></li>
		</ul>
	</div>
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg">
		<div class="pagetitle">
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Homepage Blocks
			<?php }else{ ?>
			Edit Homepage Blocks
			<?php }?> </div>
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBlockslistId" id="iBlockslistId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iBlockslistId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				<!--<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="en" />
				<?php }else{ ?>
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" />
				<?php }?>-->
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Blocks Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Title" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Blocks Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>"<?php if ($_smarty_tpl->tpl_vars['data']->value->vImage==''){?> lang="*" <?php }?> onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->vImage!=''){?>
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg" >View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('<?php echo $_smarty_tpl->tpl_vars['data']->value->iBlockslistId;?>
','product');" class="delete_btnimg">Delete</a></div>
						<div style="display:none;" >
							<div id="popfancy""><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/homepage_blocks/<?php echo $_smarty_tpl->tpl_vars['data']->value->iBlockslistId;?>
/282X320_<?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
"></div>
						</div>
						</div>
					<?php }?><?php }?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Blocks Url</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLinkUrl" name="data[vLinkUrl]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vLinkUrl;?>
<?php }?>" lang="*" title="vLinkUrl" />
				</div>
				
				<div class="inputboxes">
				     <label for="textarea"><span class="red_star">*</span></span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="data[iOrder]" lang="*" title="Order Number">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						<?php }?>
						<?php }else{ ?>
						<?php echo print_r($_smarty_tpl->tpl_vars['initOrder']->value);?>

						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>	
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['data']->value->iOrder==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						<?php }?>
						<?php }?>
					</select>
				</div>
				<div class="inputboxes">
				     <label for="textarea"><span class="red_star"></span> Description</label>
					<span class="collan_dot">:</span>
				     <textarea value="" title="tDescription" class="inputbox" title="Description" name="data[tDescription]" id="tDescription"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tDescription;?>
<?php }?></textarea>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<input type="submit" value="Add Homepage Blocks" class="submit_btn" title="Add Homepage Blocks" onclick="return validate(document.frmadd);"/>
					<?php }else{ ?>
						<input type="submit" value="Edit Homepage Blocks" class="submit_btn" title="Edit Homepage Blocks" onclick="return validate(document.frmadd);"/>
					<?php }?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homepage_blocks/homepage_blockslist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>	
	</form>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
         	
         	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});
function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homepage_blocks/deleteimage?iBlockslistId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iBlockslistId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
 
<?php }} ?>