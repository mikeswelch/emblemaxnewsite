<?php /* Smarty version Smarty-3.1.11, created on 2013-03-25 10:31:48
         compiled from "application/views/templates/admin/languagelable/languagelable.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1849475373514fda3cbd0488-98634697%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '370dabc39100c5a45bf21e622ab1f9a15f3daf7e' => 
    array (
      0 => 'application/views/templates/admin/languagelable/languagelable.tpl',
      1 => 1363845750,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1849475373514fda3cbd0488-98634697',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'language_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514fda3cd7e790_18107062',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514fda3cd7e790_18107062')) {function content_514fda3cd7e790_18107062($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/langlablelist">Language Lable</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add  Language Lable<?php }else{ ?>Edit Language Lable<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Language Lable
			<?php }else{ ?>
			Edit Language Lable
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iLabelId" id="iLabelId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iLabelId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				

				<div class="inputboxes">
					<label for="textfield" style="width:250px"><span class="red_star">*</span> Lable Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vName" name="Data[vName]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vName;?>
<?php }?>" lang="*" title="Lable Name" style="width:250px"/>
				</div>
				<?php if (count($_smarty_tpl->tpl_vars['language_data']->value)>0){?>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['language_data']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<div class="inputboxes">
					<label for="textarea" style="width:250px"><span class="red_star">*</span> Value [<span style="color:red;"><?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLanguage;?>
</span>]</label>
					<span class="collan_dot">:</span>
					<!--<input type="text" id="vValue_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
" name="Data[vValue_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->{'vValue_'.$_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode};?>
<?php }?>"  title="<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLanguage;?>
" style="width:250px" lang="*"/>-->
				        <textarea value="" class="inputbox" title="Address" name="Data[vValue_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
]" id="vValue_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
" style="width:292px;height:83px;" lang="*"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->{'vValue_'.$_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode};?>
<?php }?></textarea>
				</div>
				<?php endfor; endif; ?>
				<?php }?>
				<!--<div class="inputboxes">
				        <label for="textarea"> Address</label>
						<span class="collan_dot">:</span>
				        <textarea value="" class="inputbox" title="Address" name="Data[vValue_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
]" id="vValue_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
" style="width:250px" lang="*"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->{'vValue_'.$_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode};?>
<?php }?></textarea>
				</div>-->					
				<!--<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Lable Value_en</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vValue_en" name="Data[vValue_en]" class="inputbox" maxlength="2" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vValue_en;?>
<?php }?>"  title="Lable Value_en" style="width:250px"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Lable Value_da</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vValue_da" name="Data[vValue_da]" class="inputbox" maxlength="2" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vValue_da;?>
<?php }?>"  title="Lable Value_da" style="width:250px"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Lable Value_pa</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vValue_pa" name="Data[vValue_pa]" class="inputbox" maxlength="2" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vValue_pa;?>
<?php }?>"  title="Lable Value_da" style="width:250px"/>
				</div>-->
				<!--<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				</div>-->
				<div class="add_can_btn" style="margin-left: 286px;">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Language Lable" class="submit_btn" title="Add Language Lable" onclick="return validate(document.frmadd);" style="width:170px"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Language Lable" class="submit_btn" title="Edit Language Lable" onclick="return validate(document.frmadd);" style="width:170px"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
	</script>
 
<?php }} ?>