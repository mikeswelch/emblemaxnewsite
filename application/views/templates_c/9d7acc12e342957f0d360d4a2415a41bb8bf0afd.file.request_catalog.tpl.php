<?php /* Smarty version Smarty-3.1.11, created on 2014-01-02 07:01:18
         compiled from "application/views/templates/request_catalog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:84606674451e5234824a807-91646960%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d7acc12e342957f0d360d4a2415a41bb8bf0afd' => 
    array (
      0 => 'application/views/templates/request_catalog.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '84606674451e5234824a807-91646960',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51e523482f68f0_37964428',
  'variables' => 
  array (
    'msg' => 0,
    'site_ul' => 0,
    'db_country' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51e523482f68f0_37964428')) {function content_51e523482f68f0_37964428($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="main container">
	    <div class="slideshow_static">
		<?php if ($_smarty_tpl->tpl_vars['msg']->value){?>
                <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>			
                   <div class="main-inner">    
                    <div class="col-main">
                       <div class="account-create">
    			<h2 class="pagetital">Request Catalog</h2>
			
			<div style="clear:both;">
			    <ul class="messages">
				<li class="success-msg" style="margin: 20px 0px 0px 0px !important;">
				  <ul>
				     <li>
				     <span style="margin-left: 31%;"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</span>
				     </li>
				  </ul>
				</li>
			    </ul>
			   
			</div>
		       </div>		
                    </div>					
                  </div>
		<?php }?>
	    </div>
	    <div class="main-inner">
		<div class="col-main">
		    <div class="account-create">
			<h2 class="pagetital">Request a Catalog</h2>
			<div style="clear:both;"></div>
			<form action="<?php echo $_smarty_tpl->tpl_vars['site_ul']->value;?>
request_catalog/save" method="post" id="form-validate" id="rform" onsubmit="return CheckForm();">
			<div class="fieldset">
			    <input type="hidden" name="success_url" value="" />
			    <input type="hidden" name="error_url" value="" />
			    <h2 class="legend">Personal Information</h2>
			    <ul class="form-list row-fluid show-grid">
				<li class="fields">
				    <div class="customer-name">
					<div class="field name-firstname span6">
					<label for="firstname" class="required"><em>*</em>Name</label>
					    <div class="input-box">
						<input type="text" id="name" name="Data[vName]" value="" title="Name" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					    <div class="validation-advice" id="divname" style="display:none; float: left;"></div>
					</div>
					<!--div class="field name-lastname span6">
					    <label for="lastname" class="required"><em>*</em>Last Name</label>
					    <div class="input-box">
						<input type="text" id="lastname" name="lastname" value="" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					</div-->
				    </div>
				</li>
				<li class="fields">
				    <div class="customer-name">
					<div class="field name-firstname span6">
					    <label for="telephone" class="required"><em>*</em>Telephone</label>
					    <div class="input-box">
						<input type="text" id="telephone" name="Data[vPhone]" value="" title="Telephone" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					    <div class="validation-advice" id="divtelephone" style="display:none; float: left;"></div>
					</div>
					<div class="field name-lastname span6">
					    <!--
					    <label for="fax" class="required"><em>*</em>Fax</label>
					    <div class="input-box">
						<input type="text" id="fax" name="Data[vFax]" value="" title="Fax" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					    <div class="validation-advice" id="divfax" style="display:none; float: left;"></div>
					    -->
					    <label for="email_address" class="required"><em>*</em>Email Address</label>
					    <div class="input-box">
						<input type="text" name="Data[vEmail]" id="email" value="" title="Email Address" class="input-text validate-email required-entry span12" />
					    </div>
					    <div class="validation-advice" id="divemail" style="display:none; float: left;"></div>
					</div>
				    </div>
				</li>
			    </ul>
			</div>
			<div class="fieldset">
			    <input type="hidden" name="success_url" value="" />
			    <input type="hidden" name="error_url" value="" />
			    <h2 class="legend">Your Address</h2>
			    <ul class="form-list row-fluid show-grid">
				<li class="fields">
				    <div class="customer-name">
					<div class="field name-firstname span6">
					    <label for="cname" class="required"><em>*</em>Company Name</label>
					    <div class="input-box">
						<input type="text" id="cname" name="Data[vCompany]" value="" title="Company Name" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					    <div class="validation-advice" id="divcname" style="display:none; float: left;"></div>
					</div>
					<div class="field name-lastname span6">
					    <label for="address" class="required"><em>*</em>Address</label>
					    <div class="input-box">
						<input type="text" id="address" name="Data[vAddress]" value="" title="Address" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					    <div class="validation-advice" id="divaddress" style="display:none; float: left;"></div>
					</div>
				    </div>
				</li>
				<li class="fields">
				    <div class="customer-name">
					<div class="field name-firstname span6">
					    <label for="city" class="required"><em>*</em>City</label>
					    <div class="input-box">
						<input type="text" id="city" name="Data[vCity]" value="" title="City" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					     <div class="validation-advice" id="divcity" style="display:none; float: left;"></div>
					</div>
					<div class="field name-lastname span6">
					    <label for="postcode" class="required"><em>*</em>Post Code</label>
					    <div class="input-box">
						<input type="text" id="postcode" name="Data[vZip]" value="" title="Post Code" maxlength="255" class="input-text span12 required-entry"  />
					    </div>
					     <div class="validation-advice" id="divpostcode" style="display:none; float: left;"></div>
					</div>
				    </div>
				</li>
				<li class="fields">
				    <div class="customer-name">
					<div class="field name-firstname span6">
					    <label for="firstname" class="required"><em>*</em>Select Country</label>
					    <div class="input-box">
						<select  class="input-text span12 required-entry" id="country" name="Data[iCountryId]" onchange="getStates(this.value);" title="Country">
						    <option value="">--Select Country--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
							<option value='<?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId;?>
'><?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vCountry;?>
</option>
						<?php endfor; endif; ?>
						</select>
					    </div>
					    <div class="validation-advice" id="divcountry" style="display:none; float: left;"></div>
					</div>
					<div class="field name-lastname span6">
					    <label for="lastname" class="required"><em>*</em>Select State</label>
					    <div class="input-box">
						<select  class="input-text span12 required-entry" title="State" id="states" name="Data[iStateId]">
						    <option value=''>--Select State--</option>
						</select>
					    </div>
					    <div class="validation-advice" id="divstate" style="display:none; float: left;"></div>
					</div>
				    </div>
				</li>
			    </ul>
			</div>

			<div class="fieldset">
			    <h2 class="legend">Note any other special requests</h2>
			    <ul class="form-list row-fluid show-grid">
				<li>
				    <label for="email_address" class="required"><em>*</em>Comment</label>
				    <div class="txtboxwrp">
					<textarea class="bigewizz ordnote" name="Data[tNote]" id="note"></textarea>
				    </div>
				    <div class="validation-advice" id="divnote" style="display:none; float: left;"></div>
				</li>
			    </ul>
			    <div id="window-overlay" class="window-overlay" style="display:none;"></div>
			    <div id="remember-me-popup" class="remember-me-popup" style="display:none;">
				<div class="remember-me-popup-head">
				    <h3>What's this?</h3>
				    <a href="#" class="remember-me-popup-close" title="Close">Close</a>
				</div>
				<div class="remember-me-popup-body">
				    <p>Checking &quot;Remember Me&quot; will let you access your shopping cart on this computer when you are logged out</p>
				    <div class="remember-me-popup-close-button a-right">
					<a href="#" class="remember-me-popup-close button" title="Close"><span>Close</span></a>
				    </div>
				</div>
			    </div>
			</div>
			<div class="buttons-set">
			    <p class="required"><em>*</em> Required Fields</p>
			    <p class="back-link"><a href="#" class="back-link"><small>&laquo; </small>Back</a></p>
			    <button type="submit" title="Submit" class="button" ><span><span>Submit</span></span></button>
			</div>
		    </form>
		</div>
	    </div>
	</div>				
    </div>
</div>
</div>


<script type='text/javascript'>
function getStates(icountryid)
{
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}

function CheckForm()
{
    
    var extra = '';
    var name =  $('#name').val();   
    var telephone =  $('#telephone').val();
    var fax =  $('#fax').val();
    var email =  $('#email').val();
    var cname =  $('#cname').val();
    var address =  $('#address').val();
    var city =  $('#city').val();
    var postcode =  $('#postcode').val();
    var country =  $('#country').val();
    var state =  $('#state').val();
    var note =  $('#note').val();
    
    
    if( name == ''){
	$("#name").addClass("borderValidations");
	$('#divname').html('This is a required field.');
	$('#divname').show();
	return false;
    }
    else{
      $('#divname').hide();
      $("#name").removeClass("borderValidations");
      //return false;
     }
     
     if( telephone == ''){
	$("#telephone").addClass("borderValidations");
	$('#divtelephone').html('This is a required field.');
	$('#divtelephone').show();
	return false;
    }
    else{
      $('#divtelephone').hide();
      $("#telephone").removeClass("borderValidations");
      //return false;
     }  

     if( fax == ''){
	$("#fax").addClass("borderValidations");
	$('#divfax').html('This is a required field.');
	$('#divfax').show();
	return false;
    }
    else{
      $('#divfax').hide();
      $("#fax").removeClass("borderValidations");
      //return false;
     }
    var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var isvalid = emailRegexStr.test(email);
     if( email == ''){
	$("#email").addClass("borderValidations");
	$('#divemail').html('This is a required field.');
	$('#divemail').show();
	return false;
    
    }else if (!isvalid) {
        $("#email").addClass("borderValidations");
	$('#divemail').html('Enter Your Email Address like demo@doman.com');
	$('#divemail').show();
	return false;
    }  
    else{
      $('#divemail').hide();
      $("#email").removeClass("borderValidations");
      //return false;
     }  

    if( cname == ''){
	$("#cname").addClass("borderValidations");
	$('#divcname').html('This is a required field.');
	$('#divcname').show();
	return false;
    }
    else{
      $('#divcname').hide();
      $("#cname").removeClass("borderValidations");
      //return false;
     }

    if( address == ''){
	$("#address").addClass("borderValidations");
	$('#divaddress').html('This is a required field.');
	$('#divaddress').show();
	return false;
    }
    else{
      $('#divaddress').hide();
      $("#address").removeClass("borderValidations");
      //return false;
     }

    if( city == ''){
	$("#city").addClass("borderValidations");
	$('#divcity').html('This is a required field.');
	$('#divcity').show();
	return false;
    }
    else{
      $('#divcity').hide();
      $("#city").removeClass("borderValidations");
      //return false;
     }

    if( postcode == ''){
	$("#postcode").addClass("borderValidations");
	$('#divpostcode').html('This is a required field.');
	$('#divpostcode').show();
	return false;
    }
    else{
      $('#divpostcode').hide();
      $("#postcode").removeClass("borderValidations");
      //return false;
     }

    if( country == ''){
	$("#country").addClass("borderValidations");
	$('#divcountry').html('This is a required field.');
	$('#divcountry').show();
	return false;
    }
    else{
      $('#divcountry').hide();
      $("#country").removeClass("borderValidations");
      //return false;
     }
    if( state == ''){
	$("#state").addClass("borderValidations");
	$('#divstate').html('This is a required field.');
	$('#divstate').show();
	return false;
    }
    else{
      $('#divstate').hide();
      $("#state").removeClass("borderValidations");
      //return false;
     }

    if( note == ''){
	$("#note").addClass("borderValidations");
	$('#divnote').html('This is a required field.');
	$('#divnote').show();
	return false;
    }
    else{
      $('#divnote').hide();
      $("#note").removeClass("borderValidations");
       $("#rform").submit();
      //return false;
     }     
   
      
}
</script>


<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>