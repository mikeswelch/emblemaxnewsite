<?php /* Smarty version Smarty-3.1.11, created on 2013-05-01 09:43:20
         compiled from "application/views/templates/admin/administrator/administrator.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1640642410515417f2468eb5-36556979%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'db7fce33b488e58dfc181ae06f2e3e310c824de3' => 
    array (
      0 => 'application/views/templates/admin/administrator/administrator.tpl',
      1 => 1367422327,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1640642410515417f2468eb5-36556979',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515417f25c2c37_55024354',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'action' => 0,
    'data' => 0,
    'iAdminId' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515417f25c2c37_55024354')) {function content_515417f25c2c37_55024354($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
administrator/adminlist">Administrator</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Administrator<?php }else{ ?>Edit Administrator<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Add Administrators</div>
		<div class="add_ad_contentbox">
			<form id="frmadd" name="frmadd" method="post" action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
				<input type="hidden" name="iAdminId" id="iAdminId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iAdminId;?>
<?php }?>" />
				<input type="hidden" name="action" id="action" value="add" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> First Name</label>
					<span class="collan_dot">:</span>
					<input type="text" lang="*" title="First Name" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vFirstName;?>
<?php }?>" class="inputbox" name="Data[vFirstName]" id="vFirstName">
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Last Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vLastName;?>
<?php }?>" title="Last Name" lang="*"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> E-mail</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vEmail"  name="Data[vEmail]" lang="*{E}" class="inputbox" title="E-mail" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vEmail;?>
<?php }?>"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> User Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vUserName" name="Data[vUserName]" class="inputbox" title="User Name" lang="*{P}6:0" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vUsername;?>
<?php }?>"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vPassword"  name="Data[vPassword]" class="inputbox" title="Password" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPassword;?>
<?php }?>" lang="*{P}6:0"/>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['iAdminId']->value!=$_smarty_tpl->tpl_vars['data']->value->iAdminId){?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				</div>
				<?php }?>
				<div class="add_can_btn"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="button" value="Add Administrator" class="submit_btn" title="Add Administrator" onclick="checkUsername('<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
');"/>
					<?php }else{ ?>
					<input type="button" value="Edit Administrator" class="submit_btn" title="Edit Administrator" onclick="checkUsername('<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
');"/>
					<?php }?> 
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
administrator/adminlist" class="cancel_btn">
						Cancel
					</a> 
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>