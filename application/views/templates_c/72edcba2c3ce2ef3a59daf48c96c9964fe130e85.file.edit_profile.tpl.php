<?php /* Smarty version Smarty-3.1.11, created on 2013-09-03 07:19:39
         compiled from "application/views/templates/edit_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:209294290951850f2e245447-93982542%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '72edcba2c3ce2ef3a59daf48c96c9964fe130e85' => 
    array (
      0 => 'application/views/templates/edit_profile.tpl',
      1 => 1377257059,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '209294290951850f2e245447-93982542',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51850f2e46d477_24341013',
  'variables' => 
  array (
    'operation' => 0,
    'user_data' => 0,
    'newsletter' => 0,
    'db_country' => 0,
    'db_state' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51850f2e46d477_24341013')) {function content_51850f2e46d477_24341013($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
		
			<div class="main-container col2-right-layout">			 
			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
						            <div class="main container">

                <div class="main-inner">
                    
                    <div class="col-main">
                                                <div class="account-create">
    			<h2 class="pagetital subtit">Edit Profile</h2>
			<div style="clear:both;"></div>
            <div class="ordleftpart">
		  		<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Personal Information</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>First Name</label>
    <div class="input-box">
        <input type="text" id="vFirstName" name="Data[vFirstName]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vFirstName;?>
<?php }?>" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="firstNameDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Last Name</label>
    <div class="input-box">
        <input type="text" id="vLastName" name="Data[vLastName]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vLastName;?>
<?php }?>" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="lastNameDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Telephone</label>
    <div class="input-box">
        <input type="text" id="vPhone" name="Data[vPhone]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vPhone;?>
<?php }?>" title="Telephone" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="phoneDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em></em>Fax</label>
    <div class="input-box">
        <input type="text" id="vFax" name="Data[vFax]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vFax;?>
<?php }?>" title="Fax" maxlength="255" class="input-text span12 required-entry"  />
    </div>
</div>
</div>
                </li>
                <li>
                    <label for="email_address" class="required"><em>*</em>Email Address</label>
                    <div class="input-box">
                        <input type="text" name="Data[vEmail]" id="vEmail" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vEmail;?>
<?php }?>" title="Email Address" class="input-text validate-email required-entry span12" />
                    </div>
		    <div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
                </li>
                                 <li class="control">
                    <div class="input-box">
                        <input type="checkbox" name="Data['vNewsleter']" title="Sign Up for Newsletter" value="" <?php if ($_smarty_tpl->tpl_vars['newsletter']->value[0]['iUserId']!=''&&$_smarty_tpl->tpl_vars['newsletter']->value[0]['eStatus']=='Active'){?>checked<?php }?>  class="checkbox" id="vNews"/>
                    </div>
                    <label for="is_subscribed">Sign Up for Newsletter</label>
                </li>
                                
                                                            </ul>
</div>
<div class="fieldset">
            <input type="hidden" name="success_url" value="" />
            <input type="hidden" name="error_url" value="" />
            <h2 class="legend">Your Address</h2>
            <ul class="form-list row-fluid show-grid">
                <li class="fields">
                       <div class="customer-name">
   
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Address</label>
    <div class="input-box">
        <input type="text" id="tAddress" name="Data[tAddress]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->tAddress;?>
<?php }?>" title="Address" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="addressDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
			 <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>City</label>
    <div class="input-box">
        <input type="text" id="vCity" name="Data[vCity]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vCity;?>
<?php }?>" title="City" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="cityDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Post Code</label>
    <div class="input-box">
        <input type="text" id="vZipCode" name="Data[vZipCode]" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->vZipCode;?>
<?php }?>" title="Post Code" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="zipcodeDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
                <li class="fields">
                       <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>Select Country</label>
    <div class="input-box">
    		<select name="" class="input-text span12 required-entry" id="iCountryId" name"Data[iCountryId]" onchange="getStates(this.value);" title="Country">
		 <option value="">--Select Country--</option>
		 <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    			<option value='<?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId;?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'){?><?php if ($_smarty_tpl->tpl_vars['user_data']->value[0]->iCountryId==$_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vCountry;?>
</option>
    		 <?php endfor; endif; ?>
    		</select>
        
    </div>
     <div class="validation-advice" id="countryDiv" style="display:none; float: left;"></div>
<input type="hidden" name="iUserId" id="iUserId" value="<?php echo $_smarty_tpl->tpl_vars['user_data']->value[0]->iUserId;?>
"> 
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Select State</label>
    <div class="input-box">
        <select id="states" name="Data[iStateId]"  title="State" class="input-text span12 required-entry"  >
	 <option>--Select State--</option>
    	     <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		 <option value='<?php echo $_smarty_tpl->tpl_vars['db_state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'){?> <?php if ($_smarty_tpl->tpl_vars['user_data']->value[0]->iStateId==$_smarty_tpl->tpl_vars['db_state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['db_state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>

	     <?php endfor; endif; ?>
		 </option>
    	</select>
    </div>
    <div class="validation-advice" id="statesDiv" style="display:none; float: left;"></div>
</div>
</div>
                </li>
   
                                                            </ul>
</div>
<div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
" class="back-link"><small>&laquo; </small>Back</a></p>
        <button type="button" title="Submit" class="button" onclick=" return EditProfile();"><span><span>Submit</span></span></button>
    </div>
		  </div>
<?php echo $_smarty_tpl->getSubTemplate ("right_myAccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
    </script>
</div>

		    </div>
					
                </div>				
				
									
								
            </div>
        </div>
			 
			 
        </div>
		
		<!--************mid-part-end************-->
<div class="botline">
		<div class="linebg">&nbsp;</div>
		</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>




<script type="text/javascript">
         
function getStates(icountryid)
{
  
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}
</script>

<script type="text/javascript">
  
function EditProfile()
{
    var iUserId = $('#iUserId').val();
    var extra = '';
    var firstname =  $('#vFirstName').val();   
    var lastname =  $('#vLastName').val();
    var email = $('#vEmail').val();
    var phone = $('#vPhone').val();
    var fax = $('#vFax').val();
    var address = $('#tAddress').val();
    var city = $('#vCity').val();
    var country = $('#iCountryId').val();
    var state = $('#states').val();
    var zip = $('#vZipCode').val();
    var password = $('#vPassword').val();
    var resetpass = $('#vPasswordAgain').val();    
    var newsletter  = $('#vNews').val();
 
    if ($('#vNews').is(":checked"))
    {
      newsletter = 'Active';
    }else{
        newsletter = 'Inactive';
    }
if (document.getElementById('vFirstName').value == "" || document.getElementById('vLastName').value == "" ||
	document.getElementById('vEmail').value == "" || document.getElementById('vPhone').value == "" ||
	document.getElementById('tAddress').value == "" || document.getElementById('vCity').value == "" ||
	document.getElementById('iCountryId').value == "" || document.getElementById('states').value == "" ||
	document.getElementById('vZipCode').value == "") {
	$("#vFirstName").addClass("borderValidations");
	$('#firstNameDiv').html('This is a required field.');
	$('#firstNameDiv').show();
	$("#vLastName").addClass("borderValidations");
	$('#lastNameDiv').html('This is a required field.');
	$('#lastNameDiv').show();
	$("#vPhone").addClass("borderValidations");
	$('#phoneDiv').html('This is a required field.');
	$('#phoneDiv').show();
	$("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('This is a required field.');
	$('#emailDiv').show();
        $("#tAddress").addClass("borderValidations");
	$('#addressDiv').html('This is a required field.');
	$('#addressDiv').show();
	$("#vCity").addClass("borderValidations");
	$('#cityDiv').html('This is a required field.');
	$('#cityDiv').show();
	$("#vZipCode").addClass("borderValidations");
	$('#zipcodeDiv').html('This is a required field.');
	$('#zipcodeDiv').show();
	$("#iCountryId").addClass("borderValidations");
	$('#countryDiv').html('This is a required field.');
	$('#countryDiv').show();
	$("#states").addClass("borderValidations");
	$('#statesDiv').html('This is a required field.');
	$('#statesDiv').show();	
	//return false;		 
    }    
    var validate = true;
    if( $('#vFirstName').val() == ''){
	$("#vFirstName").addClass("borderValidations");
	$('#firstNameDiv').html('This is a required field.');
	$('#firstNameDiv').show();
	validate = false;
    }
    else{
      $('#firstNameDiv').hide();
      $("#vFirstName").removeClass("borderValidations");
     }
        
    if( $('#vLastName').val() == ''){
        $("#vLastName").addClass("borderValidations");
	$('#lastNameDiv').html('This is a required field.');
	$('#lastNameDiv').show();
	validate = false;
    }
    
    else{
      $('#lastNameDiv').hide();
      $("#vLastName").removeClass("borderValidations");
    }
    
    if( $('#vPhone').val() == ''){
        $("#vPhone").addClass("borderValidations");
	$('#phoneDiv').html('This is a required field.');
	$('#phoneDiv').show();
	validate = false;
    }   
    else{
      $('#phoneDiv').hide();
      $("#vPhone").removeClass("borderValidations");
    }    
    var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    var isvalid = emailRegexStr.test(email);
    if( $('#vEmail').val() == ''){
        $("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('This is a required field.');
	$('#emailDiv').show();
	validate = false;
    }
    else if (!isvalid) {
        $("#vEmail").addClass("borderValidations");
	$('#emailDiv').html('Enter Your Email Address like demo@doman.com');
	$('#emailDiv').show();
	validate = false;
        }
   else{
      $('#emailDiv').hide();
      $("#vEmail").removeClass("borderValidations");
    }
    
    
    if( $('#tAddress').val() == ''){
        $("#tAddress").addClass("borderValidations");
	$('#addressDiv').html('This is a required field.');
	$('#addressDiv').show();
	validate = false;
       // $('#addressDiv').html('Enter Your Address'); $('#addressDiv').show(); return false;
    }
  else{
      $('#addressDiv').hide();
      $("#tAddress").removeClass("borderValidations");
    }
    
    if( $('#vCity').val() == ''){
        $("#vCity").addClass("borderValidations");
	$('#cityDiv').html('This is a required field.');
	$('#cityDiv').show();
	validate = false;
       //$('#cityDiv').html('Enter Your City'); $('#cityDiv').show(); return false;
    }
    else{
      $('#cityDiv').hide();
      $("#vCity").removeClass("borderValidations");
    }
    
    
    if( $('#vZipCode').val() == ''){
       $("#vZipCode").addClass("borderValidations");
	$('#zipcodeDiv').html('This is a required field.');
	$('#zipcodeDiv').show();
	validate = false;
       //$('#zipcodeDiv').html('Enter Your Zipcode'); $('#zipcodeDiv').show(); return false;
    }
    else{
      $('#zipcodeDiv').hide();
      $("#vZipCode").removeClass("borderValidations");
    }
    
    if( $('#iCountryId').val() == ''){
         $("#iCountryId").addClass("borderValidations");
	$('#countryDiv').html('This is a required field.');
	$('#countryDiv').show();
	validate = false;
       //$('#countryDiv').html('Select Country'); $('#countryDiv').show(); return false;
    }
    else{
      $('#countryDiv').hide();
      $("#iCountryId").removeClass("borderValidations");
    }
    
    if( $('#states').val() == ''){
        $("#states").addClass("borderValidations");
	$('#statesDiv').html('This is a required field.');
	$('#statesDiv').show();
	validate = false;
       //$('#statesDiv').html('Select State'); $('#statesDiv').show(); return false;
    }
    else{
      $('#statesDiv').hide();
      $("#states").removeClass("borderValidations");
    }
    if (validate)
    {

          extra+='?vFirstName='+firstname;
	  extra+='&iUserId='+iUserId;
	  extra+='&vLastName='+lastname;
          extra+='&vEmail='+email;
	  extra+='&vPhone='+phone;
	  extra+='&fax='+fax;
          extra+='&tAddress='+address;
	  extra+='&vCity='+city;
          extra+='&zip='+zip;
          extra+='&iCountryId='+country;
          extra+='&iStateId='+state;
	  extra+='&vNewsletter='+newsletter; 
          
	  var url = site_url + 'myAccount/edit_profile';
	  var pars = extra;
	 
	  $.post(url+pars,
          function(data) {
	  if(data == 'success'){
              window.location = site_url+'myAccount/myDashboard';
            }
	});
	  return true;
    }
    else
    {
	return false;
    }
}
</script>
<?php }} ?>