<?php /* Smarty version Smarty-3.1.11, created on 2013-06-01 05:29:27
         compiled from "application/views/templates/my_orders.tpl" */ ?>
<?php /*%%SmartyHeaderCode:104021594851a9db1741b959-68087267%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2df7128caf1764530c766b1cf631879e1f07e594' => 
    array (
      0 => 'application/views/templates/my_orders.tpl',
      1 => 1370086134,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '104021594851a9db1741b959-68087267',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'orders' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51a9db1757fd67_75361228',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51a9db1757fd67_75361228')) {function content_51a9db1757fd67_75361228($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"order details"), 0);?>

<div class="main-container col2-right-layout">
<div class="container">
    <div class="main-inner">
        <div class="col-main">
	    <div class="account-login row-fluid show-grid">

  <div class="box-account box-recent" style="width: 850px;float: left;">
    <div class="box-head">
        <h2>My Orders</h2>
        <!--<a href="#">View All</a>-->
    </div>
	<table id="shopping-cart-table" class="data-table cart-table" style="border-bottom:2px solid #EAEAEA">
                <col width="1" />
                <col />
                <col width="1" />
		<col width="1" />
		<col width="1" />
		<col width="1" />
		<col width="1" />
		<thead class="hidden-phone">
			<tr class="">
			    <th class="" rowspan="1"><span class="nobr">Order Id</span></th>
			    <th width="70" rowspan="1" class=""><span class="nobr">Date Added</span></th>
			    <th width="200" rowspan="1" class=""><span class="nobr">Product</span></th>
			    <!--<th colspan="1"><span class="nobr">Model</span></th>-->
			    <th rowspan="1"><span class="nobr">Status</span></th>
			    <th colspan="1"><span class="nobr">Price</span></th>
			    <th rowspan="1"><span class="nobr">Details</span></th>
			</tr>
		</thead>
		<tbody>
                <?php if (count($_smarty_tpl->tpl_vars['orders']->value)>0){?>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['orders']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<tr>
			   <td style="padding-bottom: 25px;"><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vInvoiceNum'];?>
</td>
			   <td width="7"><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'];?>
</td>
			   <td width="200"><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
</td>
			   <!--<td>PR - 3</td>-->
			   <td><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status'];?>
</td>
			   <td>$<?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fGrandTotal'];?>
</td>
			   <td><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/showOrderDetail?iOrderId=<?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iOrderId'];?>
" class="viewdet">View Details</a></td>
		       </tr>
                <?php endfor; endif; ?>
                <?php }else{ ?>
                <tr>
                        <td height="20px;" colspan="8" style="text-align:center; color:#38414B; font-size:14px; font-weight:bold;">No Records Found.</td>
                </tr>
                <?php }?>
                </tbody>
            </table>
    <script type="text/javascript">decorateTable('my-orders-table')</script>
</div>
  <div style="margin-top: 21px;"><?php echo $_smarty_tpl->getSubTemplate ("right_myAccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
</div>
	    </div>
	</div>
    </div>
 
</div>

</div>
</div>

<!--<div class="main-container col2-right-layout">
    <div class="grid_11 singleleft">
        <div class="billing edtptof">
            <div class="shiztitle">
            <h2 class="title h2bg">My Orders</h2>
        </div>

        <div class="orderlist">
            <table cellpadding="3" cellspacing="3">
                <tr class="blacky">
                        <td align="center" valign="middle" class="table80">Order Id</td>
                        <td align="center" valign="middle" class="table100">Added Date</td>
                        <td class="table95">Status</td>
                        <td class="table100">Grand Total</td>
                        <td class="table100">Order Details</td>
                        </tr>
                <?php if (count($_smarty_tpl->tpl_vars['orders']->value)>0){?>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['orders']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vInvoiceNum'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate'];?>
</td>
                        <td align="center"><?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status'];?>
</td>
                        <td>$<?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fGrandTotal'];?>
</td>
                        <td><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/showOrderDetail?iOrderId=<?php echo $_smarty_tpl->tpl_vars['orders']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iOrderId'];?>
" class="eye">View Order</a></td>
                        </tr>
                <?php endfor; endif; ?>
                <?php }else{ ?>
                <tr>
                        <td height="20px;" colspan="8" style="text-align:center; color:#38414B; font-size:14px; font-weight:bold;">No Records Found</td>
                </tr>
                <?php }?>
            </table>
        </div>
    </div>
</div>
</div>
</div>
<div class="grid_4 righthome">

</div>

</div>-->
<div class="clear"></div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>