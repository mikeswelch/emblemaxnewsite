<?php /* Smarty version Smarty-3.1.11, created on 2013-09-26 03:26:25
         compiled from "application/views/templates/admin/systememail/systememail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10514944514d85feea8518-35317511%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '348be1e596fbe6be7f2f9fc4909766901f50f95f' => 
    array (
      0 => 'application/views/templates/admin/systememail/systememail.tpl',
      1 => 1377257307,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10514944514d85feea8518-35317511',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514d85ff030300_23027285',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'action' => 0,
    'data' => 0,
    'language_data' => 0,
    'operation' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514d85ff030300_23027285')) {function content_514d85ff030300_23027285($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
    <div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
		<li>/</li>
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
systememail/systememaillist">System Email</a></li>
		<li>/</li>
		<li class="current">Edit System Email</li>
	</ul>
    </div>
    <div class="centerpartbg">
           
            <div class="pagetitle">Edit System Email</div>
            
            <div class="add_ad_contentbox">
                    <form id="frmadd" name="frmadd" method="post" action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                            <input type="hidden" name="iEmailTemplateId" id="iEmailTemplateId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->iEmailTemplateId;?>
" />
                            <input type="hidden" name="action" id="action" value="add" />
			    
                            <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span> Email Title</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vEmailTitle" name="Data[vEmailTitle]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->vEmailTitle;?>
" lang="*" title="Email Title" style="width:252px" />
			    </div>
			    
			    <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span>From Email</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vFromEmail" name="Data[vFromEmail]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->vFromEmail;?>
" lang="*" title="From Email" style="width:252px" />
			    </div>
			    
			    <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span> Email Subject</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vEmailSubject" name="Data[vEmailSubject]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->vEmailSubject;?>
" lang="*" title="Email Subject" style="width:252px" />
			    </div>
			    
			    <div class="inputboxes worddocument_input">
			             <label  for="textarea" style="width:220px;"><span class="red_star"></span> Email Message </label>
				     <span >:</span>
				     <div class="worddocument" style="margin-left:250px; margin-top:-15px;"><textarea id="tEmailMessage_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[0]->vLangCode;?>
"  name="Data[tEmailMessage_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[0]->vLangCode;?>
]" class="inputbox" title="Email Message" ><?php echo $_smarty_tpl->tpl_vars['data']->value->{'tEmailMessage_'.$_smarty_tpl->tpl_vars['language_data']->value[0]->vLangCode};?>
</textarea></div>
			    </div>

<script type="text/javascript">
var id = '<?php echo $_smarty_tpl->tpl_vars['language_data']->value[0]->vLangCode;?>
';
	CKEDITOR.replace('tEmailMessage_'+id);
	
</script>

			    
			   <div style="clear:both;"></div><br>
			    <div class="inputboxes">
				    <label for="textfield" style="width:220px;"><span class="red_star">*</span> Email Footer</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vEmailFooter" name="Data[vEmailFooter]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->vEmailFooter;?>
" lang="*" title="Email Footer" style="width:252px" />
			    </div>
					       
                            <div class="inputboxes">
                                    <label for="textfield" style="width:220px;"><span class="red_star"></span> Status</label>
				    <span class="collan_dot">:</span>
                                    <select id="eStatus" name="Data[eStatus]">
                                            <option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
                                            <option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
                                    </select>
                            </div>
                          
                            <div class="add_can_btn" >                                    
                                    <input type="submit" value="Edit System Email" class="submit_btn" title="Edit System Email" onclick="return validate(document.frmadd);" style="margin-left: 45px;"/>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
systememail/systememaillist" class="cancel_btn">Cancel</a>
                            </div>
                    </form>
            </div>
    </div>
    <div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php }} ?>