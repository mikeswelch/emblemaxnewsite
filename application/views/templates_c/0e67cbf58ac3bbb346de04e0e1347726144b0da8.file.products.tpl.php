<?php /* Smarty version Smarty-3.1.11, created on 2014-02-17 12:48:44
         compiled from "application/views/templates/admin/product/products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:89179410851a61ae3cc2c07-19545369%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e67cbf58ac3bbb346de04e0e1347726144b0da8' => 
    array (
      0 => 'application/views/templates/admin/product/products.tpl',
      1 => 1392208062,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89179410851a61ae3cc2c07-19545369',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51a61ae42ace48_64781962',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'isActive' => 0,
    'data' => 0,
    'sec' => 0,
    'db_producttype' => 0,
    'Data' => 0,
    'upload_path' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
    'fabrics' => 0,
    'iFabricsId' => 0,
    'brand' => 0,
    'iBrandId' => 0,
    'occupations' => 0,
    'iOccupationsId' => 0,
    'sizelist' => 0,
    'all_color' => 0,
    'ProductsColors' => 0,
    'lang' => 0,
    'color' => 0,
    'var_msg' => 0,
    'data_color' => 0,
    'class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51a61ae42ace48_64781962')) {function content_51a61ae42ace48_64781962($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist">Product Template</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Product<?php }else{ ?>Edit Product Template<?php }?></li>
		</ul>
	</div>
		<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?>
		<div class="pagetitle_tab">
			<ul>
				<li <?php if ($_smarty_tpl->tpl_vars['isActive']->value=='main'){?> class="active" <?php }elseif($_smarty_tpl->tpl_vars['isActive']->value!='qd'&&$_smarty_tpl->tpl_vars['isActive']->value!='ds'){?>class="active" <?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/edit?iProductId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
&sec=main">Product Detail</a></li>
				<!--<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
projectbrief/projectbrieflist?iUserId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
">Project Brief</a></li>-->
				<li <?php if ($_smarty_tpl->tpl_vars['isActive']->value=='qd'){?> class="active" <?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/edit?iProductId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
&sec=qd">Quote Detail</a></li>
				<li <?php if ($_smarty_tpl->tpl_vars['isActive']->value=='ds'){?> class="active" <?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/edit?iProductId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
&sec=ds">Design Studio</a></li>
			</ul>
		</div>
		<?php }else{ ?>
		<div class="pagetitle">Add Product Detail
			<!--<ul>
				<li class="active"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/add?sec=main">Product Detail</a></li>
				
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/add?sec=qd"> Quote Detail</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/add?sec=ds">Design Studio</a></li>
			</ul> -->
		</div>
		<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['sec']->value=='main'){?>
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg" style="width:49%;float:left;">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Product Template

			<?php }else{ ?>
			Edit Product Template
			<?php }?> </div>
		
			<div class="add_ad_contentbox">
				<input type="hidden" name="iProductId" id="iProductId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				<input type="hidden" name="vImage" id="vImage_old" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
" />
                                 <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Category</label>
					 <span class="collan_dot">:</span>
					<select name="Data[iCategoryId]" onchange="makesizediv(this.value)" lang="*" title="Parent Category">						
						<option value=''>--Select Product Category--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_producttype']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['db_producttype']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId'];?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->iCategoryId==$_smarty_tpl->tpl_vars['db_producttype']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId']){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['db_producttype']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCategory'];?>
</option>
						<?php endfor; endif; ?>
					</select>
					
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vProductName" name="data[vProductName]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->vProductName;?>
<?php }?>" lang="*" title="Product Name" />
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>"<?php if ($_smarty_tpl->tpl_vars['data']->value->vImage==''){?> lang="*" <?php }?> onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
				<div class="inputboxes" >
					<div class="msg_alert1" style="margin-left: 38% !important;">
						Recommended Size : 193 X 198
					</div>
				</div>			
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->vImage!=''){?>
					<div class="view_del_user">						
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg" >View</a></div>						
						<div class="delete_user"><a href="#" onclick="ImageDelete('<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
','product');" class="delete_btnimg">Delete</a></div>
						<div style="display:none;" >
							<div id="popfancy" style="padding-bottom: 50px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
/193X198_<?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
"></div>
						</div>
				         </div>
					<?php }?><?php }?>
 				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?>
				<div class="inputboxes" >
					<div class="msg_alert1" style="margin-left: 39% !important;">
						Recommended Size : 193 X 198
					</div>
				</div>			
				<?php }?>
                                <div class="inputboxes">
				        <label for="textarea"><span class="red_star"></span> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tDescription" class="inputbox" title="Description" name="data[tDescription]" id="tDescription"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->tDescription;?>
<?php }?></textarea>
				</div>        
				<!--div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Price</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fPrice" name="Data[fPrice]" class="inputbox" title="fPrice"  lang="*" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->fPrice;?>
<?php }?>" onkeypress="return checkprise(event)">
				</div-->								
				<div class="inputboxes">
				        <label for="textarea"><span class="red_star">*</span></span> Order</label>
						<span class="collan_dot">:</span>
						<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
						<?php }?>
						<?php }else{ ?>
						<?php echo print_r($_smarty_tpl->tpl_vars['initOrder']->value);?>

						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>	
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['data']->value->iOrderNo==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						<?php }?>
						<?php }?>
					</select>
				        
				</div> 
                    <div class="inputboxes">
				<label><span class="red_star"></span> Is Featured ?</label>
					<span class="collan_dot">:</span>
						<input type="checkbox" value="Yes" name="Data[ePromotion]" onclick="return showBannerText();" id="eFeatured" <?php if ($_smarty_tpl->tpl_vars['Data']->value->iProductId!=''&&$_smarty_tpl->tpl_vars['data']->value->ePromotion=='Yes'){?>checked<?php }?>/>							
				</div>
				
				<div class="inputboxes" id="vBannerText" style="display: none;">
				<label>Banner Text </label>
				 <span class="collan_dot">:</span>
				 
						<input type="text" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vBannerText;?>
<?php }?>" class="inputbox" name="Data[vBannerText]" id="vBannerText"/>							
				</div>				
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Size Chart</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vSizeChart"  name="vSizeChart" title="Size Chart" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vSizeChart;?>
<?php }?>"  onchange="CheckPdfFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vSizeChart"  name="vSizeChart" title="Size Chart" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vSizeChart;?>
<?php }?>"<?php if ($_smarty_tpl->tpl_vars['data']->value->vSizeChart==''){?>  <?php }?> onchange="CheckPdfFile(this.value,this.name)"/>
					<?php }?>
				</div>
				
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->vSizeChart!=''){?>
					<div class="view_del_user">
						
						<div class="view_btn_user"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
/product/download?iProductId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
" id="#" class="view_btnimg" >Download</a></div>
						<div class="delete_user"><a href="#" onclick="PdfDelete('<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
','product');" class="delete_btnimg">Delete</a></div>
						<div style="display:none;" >
							<div id="popfancy" style="padding-bottom: 50px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value->vSizeChart;?>
"></div>
						</div>
				         </div>
					<?php }?><?php }?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Product Template" class="submit_btn" title="Add Product" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Product Template" class="submit_btn" title="Edit Product" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
	<div class="pagetitle">Fabrics Section</div>
	<div class="add_ad_contentbox">
		<div class="inputboxes">
			<label for="textarea"><span class="red_star"></span> Fabrics</label>
				<span class="collan_dot">:</span>
				<div class="event_skill" style="width: 55%">
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['fabrics']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
						<div class="event_slikk_inner">
							<input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['fabrics']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iFabricsId'];?>
" id="iFabricsId" name="iFabricsId[]" <?php if ($_smarty_tpl->tpl_vars['fabrics']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iFabricsId']==$_smarty_tpl->tpl_vars['iFabricsId']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]){?>checked<?php }?>/>
							<?php echo $_smarty_tpl->tpl_vars['fabrics']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['vTitle'];?>

						</div>
					<?php endfor; endif; ?>
				</div>
		</div>
	</div>
	<!--<div class="add_ad_contentbox">
                               <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Fabrics</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iFabricsId]" onchange="makesizediv(this.value)">
						
						<option value=''>--Select Fabrics--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['fabrics']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['fabrics']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iFabricsId'];?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->iFabricsId==$_smarty_tpl->tpl_vars['fabrics']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iFabricsId']){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['fabrics']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</option>
						<?php endfor; endif; ?>
					</select>
					
				</div>
	</div>-->	
	</div>
	
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
	<div class="pagetitle">Brands Section</div>
			<div class="add_ad_contentbox">
                               <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Brands</label>
					<span class="collan_dot">:</span>
					<div class="event_skill" style="width: 55%">
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['brand']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
						<div class="event_slikk_inner">
							<input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['brand']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iBrandId'];?>
" id="iBrandId" name="iBrandId[]" <?php if ($_smarty_tpl->tpl_vars['brand']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iBrandId']==$_smarty_tpl->tpl_vars['iBrandId']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]){?>checked<?php }?>/>
							<?php echo $_smarty_tpl->tpl_vars['brand']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['vTitle'];?>

						</div>
						<?php endfor; endif; ?>
					</div>
				</div>
			</div>
	</div>

	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px; margin-top: 20px;">
	<div class="pagetitle">Occupations Section</div>
			<div class="add_ad_contentbox">
                               <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Occupations</label>
					<span class="collan_dot">:</span>
					<div class="event_skill" style="width: 55%">
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['occupations']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
						<div class="event_slikk_inner">
							<input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['occupations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iOccupationsId'];?>
" id="iOccupationsId" name="iOccupationsId[]" <?php if ($_smarty_tpl->tpl_vars['occupations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iOccupationsId']==$_smarty_tpl->tpl_vars['iOccupationsId']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]){?>checked<?php }?>/>
							<?php echo $_smarty_tpl->tpl_vars['occupations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['vTitle'];?>

						</div>
						<?php endfor; endif; ?>
					</div>
				</div>
			</div>
		
	</div>
	
	<!--div class="centerpartbg" style="float:right;width:49%;margin-right:6px;min-height:235px;margin-top:20px;">
	<div class="pagetitle">Size Selection</div>
	
		<div class="add_ad_contentbox" id="sizedivid">
				
				<?php if (count($_smarty_tpl->tpl_vars['sizelist']->value)>0&&$_smarty_tpl->tpl_vars['operation']->value!='add'){?>
				<div class="inputboxes" style="padding-bottom:0px;">
				<label for="textfield"></label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Width</label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Length</label>
				</div>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sizelist']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<div class="inputboxes">
				<label for="textfield"><?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vTitle;?>
&nbsp;(<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vSize;?>
)</label>
				<span class="collan_dot">:</span>
				<input type="hidden" id="" name="SIZE[<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
][iSizeId]" value="<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iSizeId;?>
"/>
				<input type="text" id="Width<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" name="SIZE[<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
][iWidth]" class="inputbox" title="Width" style="width:100px;" value="<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iWidth;?>
" onkeypress="return checkprise(event)"/>
				<input type="text" id="Length<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" name="SIZE[<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
][iLength]" class="inputbox" title="Length" style="width:100px;margin-left:10px;" value="<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLength;?>
" onkeypress="return checkprise(event)"/>
				</div>
				<?php endfor; endif; ?>
				<?php }else{ ?>
				<div style="text-align:center;color:#C44C22; font-size:14px; font-weight:bold;">No Size available for this category</div>
				<?php }?>
		</div>
	</div-->
	</form>
	<?php }?>	
	<?php if ($_smarty_tpl->tpl_vars['sec']->value=='qd'){?>
	<div class="centerpartbg" style="width:100%;float:left;min-height:250px;">
		<div id="relaceformcolor">
			<div class="pagetitle" style="text-align:center;">
				Color Selection
			</div>			
			<form id="frmaddscolor" name="frmaddscolor" method="post" enctype="multipart/form-data"  action="">
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_color']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
					<div class="event_slikk_inner">
						<input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['all_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iColorId'];?>
" id="iColorId" name="iColorId[]" class="aka" <?php if (in_array($_smarty_tpl->tpl_vars['all_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iColorId'],$_smarty_tpl->tpl_vars['ProductsColors']->value)){?>checked <?php }else{ ?> disabled<?php }?>/>
						<?php echo $_smarty_tpl->tpl_vars['all_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['vColor'];?>

					</div>
				<?php endfor; endif; ?>
			</form>			
		</div>
	</div>
	<div class="centerpartbg" style="width:100%;float:left;margin-top:20px;min-height:250px;">
		<div id="relaceformcolor">
			<div class="pagetitle" style="text-align:center;">
				 Price by Size and Color				
			</div>
			 <div id="inn" style="text-align: center"><h2>Please select colors</h2></div>
			 
		</div>
	</div>

	<?php }?>
<?php if ($_smarty_tpl->tpl_vars['sec']->value=='ds'){?>
	<?php if ($_smarty_tpl->tpl_vars['isActive']->value!='ds'){?>
	
	<?php }?>
	<div class="centerpartbg" style="width:100%;float:left;min-height:250px;">
		<div id="relaceformcolor">
		<div class="pagetitle" style="text-align:center;">
			Add Color Image
		</div>
		<form id="frmaddcolor" name="frmaddcolor" method="post" enctype="multipart/form-data"  action="">
			<input type="hidden" name="iProductId" id="iProductId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
" />
			<input type="hidden" name="type" id="type" value="color" />
			<input type="hidden" name="mode" id="mode" value="add" />
			<input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" />
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Color :</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[iColorId]" lang="*" title="color">
						<option value=''>--Select Color--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['color']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iColorId;?>
'><?php echo $_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColor;?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Front Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Back Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Left Sleeve</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vLeftSleeveImage"  name="vLeftSleeveImage" title="Left Sleeve Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Right Sleeve</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vRightSleeveImage"  name="vRightSleeveImage" title="Right Sleeve Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[eStatus]">
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
					<input type="submit" value="Add Color Image" class="submit_btn" title="Add Color Image" onclick="return validate(document.frmaddcolor);"/>
				</div>
			</div>
		</form>
		</div>
	</div>
	
	<div class="centerpartbg" style="width:100%;float:left;margin-top:20px;" id="colorformadd">
		<div class="pagetitle">
			Color Image list
		</div>
		<?php if ($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" /> <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p>
		</div>
		<div></div>
		<?php }?>
		
			<div class="add_ad_contentbox">
				<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/search_action" method="post">
				<input  type="hidden" name="iProductId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th>Color Name</th>
							<th>Color code</th>
							<th>Front Image</th>
							<th>Back Image</th>
							<th>Left Sleeve</th>
							<th>Right Sleeve</th>
							
							<th width="77px">Status</th>
							<th width="90px">Action</th>
						</tr>
					</thead>
					<?php if (count($_smarty_tpl->tpl_vars['data_color']->value)>0){?>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data_color']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2==0){?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table_sec', null, 0);?>
					<?php }else{ ?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table', null, 0);?>
					<?php }?>
					<tbody class="<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
" style="font-size:13px;">
						<tr>
							
							<td><a href="javascript:void(0);" onclick="editform(<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
);"><?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColor;?>
</a></td>
							<td><?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColorCode;?>
</td>
							<td><?php if ($_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vFrontImage!=''){?><div class="view_btn_user"><a href="#Fancyhrefmain<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" id="fancyhrefmain" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="Fancyhrefmain<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" style="padding-bottom: 800px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vFrontImage;?>
"></div>
																												    
							</div><?php }?>
							</td>
							
							<td><?php if ($_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vBackImage!=''){?><div class="view_btn_user"><a href="#Fancyhrefback<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" id="fancyhrefback" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="Fancyhrefback<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" style="padding-bottom: 800px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vBackImage;?>
"></div>
							</div><?php }?>
							</td>
							<td><?php if ($_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLeftSleeveImage!=''){?><div class="view_btn_user"><a href="#fancyhrefleftSleeeve<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" id="fancyhrefleftSleeeve" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="fancyhrefleftSleeeve<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" style="padding-bottom: 800px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLeftSleeveImage;?>
"></div>
							</div><?php }?>
							</td>
							<td><?php if ($_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vRightSleeveImage!=''){?><div class="view_btn_user"><a href="#fancyhrerightSleeeve<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" id="fancyhrerightSleeeve" class="view_btnimg">View</a></div>
							    <div style="display:none;"><div id="fancyhrerightSleeeve<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" style="padding-bottom: 800px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vRightSleeveImage;?>
"></div>
							</div><?php }?>
							</td>
							
							<td><?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->eStatus;?>
</td>
							<td><a href="javascript:void(0);" onclick="editform(<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
);"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommoncolor(<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
,'<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
','product/deletecolor',<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
)"></a> </td>
						</tr>
					</tbody>
					<?php endfor; endif; ?>
					<?php }else{ ?>
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					<?php }?>
				</table>
			</form>
		</div>
			</div>
		<?php }?>
	</div>
	
	<div class="clear"></div>
	
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
         	
         	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrefmain").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrefleftSleeeve").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});


$("#fancyhrefback").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrerightSleeeve").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});


function PdfDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this file ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deletepdf?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}

function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deleteimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

function makesizediv(val)
{
	var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	if(val == '<?php echo $_smarty_tpl->tpl_vars['data']->value->iCategoryId;?>
')
	{
	  var url = site_url+"product/makesizedefault?iCategoryId="+val+"&iProductId="+'<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
';	
	}
	else{
	  var url = site_url+"product/makesize?iCategoryId="+val;
	}
	var pars = '';
	$.post(url+pars,
	    function(data) {
		$('#sizedivid').html(data);
	    });
}


function editform(id){	
	var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	var url = site_url+"product/makeeditcolor?iProductColorId="+id+"&lang="+'<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
';	
	var pars = '';	
	$.post(url+pars,
	    function(data) {		
		$('#relaceformcolor').html(data);
	    });
}
$('.aka').change(function() {	
	load_color_size();
});
load_color_size();
function load_color_size(){	
	var str = $("#frmaddscolor").serialize();	
	var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	var url = site_url+"product/scdetail";	
	var pid = '&iProductId='+'<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
';	
	var pars = str;
	//alert(site_url+"product/scdetail?"+pars+pid);return false;
	$.ajax({
		type: "POST",
		url: site_url+"product/scdetail?"+pars+pid,
		success: function(data){
			//alert(data);return false;
		    $('#inn').html(data);
		}
	});
}

showBannerText();
function showBannerText() {
	var checisFeautred=document.getElementById('eFeatured').checked;
	if (checisFeautred) {
		$("#vBannerText").show();
	}else{
		$("#vBannerText").hide();
	}
}







$('.simple').keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || 
            (event.keyCode == 65 && event.ctrlKey === true) || 
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
 
</script>
 

<?php }} ?>