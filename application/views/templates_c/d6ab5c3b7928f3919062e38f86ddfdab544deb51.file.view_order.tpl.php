<?php /* Smarty version Smarty-3.1.11, created on 2014-03-07 17:04:40
         compiled from "application/views/templates/admin/order/view_order.tpl" */ ?>
<?php /*%%SmartyHeaderCode:199256801951c85998096292-76881020%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd6ab5c3b7928f3919062e38f86ddfdab544deb51' => 
    array (
      0 => 'application/views/templates/admin/order/view_order.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '199256801951c85998096292-76881020',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c859984b76e1_68810247',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'var_msg' => 0,
    'ssql' => 0,
    'filterBy' => 0,
    'start_date' => 0,
    'end_date' => 0,
    'day' => 0,
    'eStatus' => 0,
    'vPaypalTransactionId' => 0,
    'fGrandTotal' => 0,
    'action' => 0,
    'total_price' => 0,
    'order' => 0,
    'field' => 0,
    'data' => 0,
    'class' => 0,
    'total' => 0,
    'recmsg' => 0,
    'page_link' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c859984b76e1_68810247')) {function content_51c859984b76e1_68810247($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<!--<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
client/clientlist">Client</a></li>
			<li>/</li>-->
			<li class="current">Orders</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Orders</div>
		<?php if ($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" /> <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p>
		</div>
		<div></div>
		<?php }elseif($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" /><?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p>
		</div>
		<div></div>
		<?php }?>
		<div class="export_btn" style="margin-top: 10px;">
			<!--<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
all_transaction/export">Export</a>-->
			<form  action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/export" >
					<input type="hidden" name="ssql" id="ssql" value="<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
" />
					<!--<input type="submit" value="Export" class="search_btn" /> -->
			</form>
		
		</div>
		<form name="frmsearchdate" id="frmsearchdate" method="post" action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order">
			<input type="hidden" name="ssql" id="ssql" value="<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
" />
			<div class="all_trans_box">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top"><div class="search_top_admini">
								<label>Search:</label>
								<select name="option" id="option" onchange="advanceSearch(this.value);">
									<option value="">Select</option>
									<option value="searchByPaymentStatus"<?php if ($_smarty_tpl->tpl_vars['filterBy']->value=='searchByPaymentStatus'){?>selected="selected"<?php }?>>Search By Payment Status</option>
									<option value="searchByTransactionId"<?php if ($_smarty_tpl->tpl_vars['filterBy']->value=='searchByTransactionId'){?>selected="selected"<?php }?>>Search By First Name</option>
									<option value="searchByGrandTotal"<?php if ($_smarty_tpl->tpl_vars['filterBy']->value=='searchByGrandTotal'){?>selected="selected"<?php }?>>Search By Sub Total</option>
								</select>
							</div></td>
						<td><div class="">
								<input type="hidden" value="" name="filterBy" id="filterBy">
								<div id="searchByDateDiv" style="display: none;" class="searchboxes">
									<div class="inputboxes inputboxesst_date">
										<label for="textfield">Start Date</label>
										<span class="collan_dot">:</span>
										<input type="text" readonly="readonly" id="start_date" name="start_date" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['start_date']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['start_date']->value;?>
<?php }?>" title="Start Date" lang="*"/>
									</div>
									<div class="inputboxes inputboxesst_date">
										<label for="textfield">End Date</label>
										<span class="collan_dot">:</span>
										<input type="text" readonly="readonly" id="end_date" name="end_date" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['end_date']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['end_date']->value;?>
<?php }?>" title="End Date" lang="*"/>
									</div>
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('date_range');"/>
								</div>
								<div id="searchByDayDiv" style="display: none;" class="searchboxes">
									<div class="inputboxes inputboxesst_date">
										<label for="textfield">Select Day</label>
										<span class="collan_dot">:</span>
										<input type="text" readonly="readonly" id="select_day" name="select_day" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['day']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['day']->value;?>
<?php }?>" title="Select Day" lang="*"/>
									</div>
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('day');"/>
								</div>
								<div id="searchByPaymentStatusDiv" style="display: none;" class="searchboxes">
									<select name="PaymentStatus" id="PaymentStatus" class="adved">
										<option value="">--Select--</option>
										<option value="Paid" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value=='Paid'){?>Selected<?php }?>>Paid</option>
										<option value="Cancelled" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value=='Cancelled'){?>Selected<?php }?>>Cancelled</option>
										<option value="Processing" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value=='Processing'){?>Selected<?php }?>>Processing</option>
										<option value="Decline" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value=='Decline'){?>Selected<?php }?>>Decline</option>
									</select>
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('PaymentStatus');"/>
								</div>
								<div id="searchByTransactionId" style="display: none;" class="searchboxes">
									<input type="text" id="firstName" name="firstName" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['vPaypalTransactionId']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['vPaypalTransactionId']->value;?>
<?php }?>" title="Transaction Id" lang="*">
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('firstName');"/>
								</div>
								<div id="searchByGrandTotal" style="display: none;" class="searchboxes">
									<input type="text" id="total" name="total" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['fGrandTotal']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['fGrandTotal']->value;?>
<?php }?>" title="Total Amount" lang="*">
									<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="filterTransactions('total_amount');"/>
								</div>
							</div></td>
					</tr>
				</table>
			</div>
		</form>
		<div class="action_apply_btn_tra">
			<select class="act_sl_box" name="newaction" id="newaction">
				<option value="">Select Action</option>
				<option value="Deletes"<?php if ($_smarty_tpl->tpl_vars['action']->value=='Deletes'){?>selected="selected"<?php }?>>Make Delete</option>
				<option value="Show All"<?php if ($_smarty_tpl->tpl_vars['action']->value=='Show All'){?>selected="selected"<?php }?>>Show All</option>
			</select>
			<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'order',document.frmlist,'order');"/>
		</div>
		<div class="total_top" style="margin-left: 325px !important;">Total Transaction:$ <?php echo $_smarty_tpl->tpl_vars['total_price']->value;?>
</div>
		<div class="clear"></div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/search_action" method="post">
				<input type="hidden" name="ssql" id="ssql" value="<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
" />
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="iTransactionId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/index?field=o.vBillName&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">User Name</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='o.vBillName'){?> <?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/index?field=o.fGrandTotal&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Grand Total</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='o.fGrandTotal'){?> <?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
							<!--<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/index?field=fGrandTotal><span style="color:white;">Grand Total</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='fGrandTotal'){?> <?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>-->
							<th width="77px"><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/index?field=o.eStatus&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Status</span> <?php if ($_smarty_tpl->tpl_vars['field']->value=='o.eStatus'){?> <?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
							<th width="90px"><span>Action</span></th>
						</tr>
					</thead>
					
					<?php if (count($_smarty_tpl->tpl_vars['data']->value)>0){?>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2==0){?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table_sec', null, 0);?>
					<?php }else{ ?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table', null, 0);?>
					<?php }?>
					<tbody class="<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
">
						<tr>
							<td><input name="iOrderId[]" type="checkbox" id="iId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iOrderId;?>
"></td>
							<td><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order/showorder?iOrderId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iOrderId;?>
&iUserId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iUserId;?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vBillFirstname;?>
 <?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vBillLastname;?>
</a></td>
							<td>$&nbsp;<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->finalBillAmount;?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->eStatus;?>
</td>
							<td><a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon(<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iOrderId;?>
,'order/delete','<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
')"></a></td>
						</tr>
					</tbody>
					<?php endfor; endif; ?>
					<tbody><tr><td></td><td class="admin_antry_table total_bottom_tot">Total Amount:</td><td class="admin_antry_table total_bottom">$&nbsp;<?php echo $_smarty_tpl->tpl_vars['total']->value;?>
</td></tr></tbody>
					<?php }else{ ?>
					<tr>
						<td height="70px;" colspan="6" style="text-align:center; color:#C44C22; font-size:18px; font-weight:bold;">No Record Found.</td>
					</tr>
					<?php }?>
					
				</table>
				<div></div>
				
			</form>
		</div>
               
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<?php if (count($_smarty_tpl->tpl_vars['data']->value)>0){?><td class="reclord_found" width="33%" align="left"><?php echo $_smarty_tpl->tpl_vars['recmsg']->value;?>
</td><?php }?>
					<td width="33%"><div class="bottom_admin_paging"><?php echo $_smarty_tpl->tpl_vars['page_link']->value;?>
 </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script>
	var opts1 = {formElements:{"start_date":"Y-ds-m-ds-d"}};
		datePickerController.createDatePicker(opts1);
	var opts2 = {formElements:{"end_date":"Y-ds-m-ds-d"}};
		datePickerController.createDatePicker(opts2);
	var opts3 = {formElements:{"select_day":"Y-ds-m-ds-d"}};
		datePickerController.createDatePicker(opts3);
		
	function filterTransactions(filterBy){
		var site_url='<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
';
		if(filterBy =='date_range'){
			if($('#start_date').val() == ''){
				//alert("Please Enter Start Date");
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Start Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				return false;
			}else if($('#end_date').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter End Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter End Date");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'date_range';	
			}
		}else if(filterBy =='day'){
			if($('#select_day').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Date</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Date");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'day';
			}	
		}else if(filterBy == 'PaymentStatus'){			
			if($('#PaymentStatus').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please select any option</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please select any option");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'PaymentStatus';
			}
		}else if(filterBy == 'firstName'){
			if($('#firstName').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter First Name</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Order Number");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'firstName	';
			}
		}else if(filterBy == 'total_amount'){			
			if($('#total').val() == ''){
				$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="'+site_url+'/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please Enter Sub Total</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
				//alert("Please Enter Transation Amount");
				return false;
			}else{
				document.frmsearchdate.filterBy.value = 'total_amount';
			}
		}
		document.getElementById('frmsearchdate').submit();
	}
	
	function advanceSearch(key){
		//alert(key);
		$(".searchboxes").css("display","none");
		if(key == 'searchByDate'){
			document.getElementById('searchByDateDiv').style.display='block';
		}else if(key == 'searchByDay'){
			document.getElementById('searchByDayDiv').style.display='block';
		}else if(key == 'searchByPaymentStatus'){
			document.getElementById('searchByPaymentStatusDiv').style.display='block';
		}else if(key == 'searchByTransactionId'){
			document.getElementById('searchByTransactionId').style.display='block';
		}else if(key == 'searchByGrandTotal'){
			document.getElementById('searchByGrandTotal').style.display='block';
		}		
	}
	var filterBy1 = '<?php echo $_smarty_tpl->tpl_vars['filterBy']->value;?>
';
	if(filterBy1 !=''){		
	if(filterBy1 == 'searchByDate'){
		document.getElementById('searchByDateDiv').style.display='block';
	}else if(filterBy1 == 'searchByDay'){
		document.getElementById('searchByDayDiv').style.display='block';
	}else if(filterBy1 == 'searchByPaymentStatus'){
		document.getElementById('searchByPaymentStatusDiv').style.display='block';
	}else if(filterBy1 == 'searchByTransactionId'){
		document.getElementById('searchByTransactionId').style.display='block';
	}else if(filterBy1 == 'searchByGrandTotal'){
		document.getElementById('searchByGrandTotal').style.display='block';
	}
	}
</script>
 <?php }} ?>