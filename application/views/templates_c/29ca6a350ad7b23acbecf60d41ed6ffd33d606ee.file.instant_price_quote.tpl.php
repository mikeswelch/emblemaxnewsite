<?php /* Smarty version Smarty-3.1.11, created on 2014-02-25 04:34:44
         compiled from "application/views/templates/instant_price_quote.tpl" */ ?>
<?php /*%%SmartyHeaderCode:64129225951bc0d8884f036-88647649%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29ca6a350ad7b23acbecf60d41ed6ffd33d606ee' => 
    array (
      0 => 'application/views/templates/instant_price_quote.tpl',
      1 => 1393320879,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '64129225951bc0d8884f036-88647649',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51bc0d88adeb18_34418365',
  'variables' => 
  array (
    'site_url' => 0,
    'front_js_path' => 0,
    'product' => 0,
    'upload_path' => 0,
    'front_image_path' => 0,
    'productColorImages' => 0,
    'iProductId' => 0,
    'all_category_data' => 0,
    'pages' => 0,
    'recmsg' => 0,
    'categories' => 0,
    'alp' => 0,
    'colors' => 0,
    'sizes' => 0,
    'productColorArray' => 0,
    'productSizeArray' => 0,
    'printLocations' => 0,
    'numberColors' => 0,
    'shipments' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51bc0d88adeb18_34418365')) {function content_51bc0d88adeb18_34418365($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"Example Smarty Page"), 0);?>

<!-- Add jQuery library -->
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
public/front-end/fancybox/jquery-1.9.0.min.js"></script>
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
public/front-end/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
public/front-end/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.form.js"></script>

<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF;">
	<div class="breadcrumbs row-fluid">
   <div class="container">
     <ul class="span12" style="text-align:left;">
	<li class="home">
	     <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
" title="Go to Home Page">Home</a>
	     <samp></samp>
	</li> 
	<li class="category5">
	     <strong>Get a Quote</strong>
	</li> 
     </ul> 

   </div>	
   </div>
	<div class="main container">
	<h2 class="pagetital subtit" style="width:150px !important;" id="getAQuoteID">Get a Quote </h2>
         <div class="product_quto_main">
	    
	    	<div class="product_detail_left">
		<div class="images">
		  <?php if ($_smarty_tpl->tpl_vars['product']->value[0]['vImage']!=''){?>
		    <div class="main_image multiple storePadHeight" style="text-align: center;" id="mainImage">
			<img height="400px" width="300px" src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['product']->value[0]['iProductId'];?>
/<?php echo $_smarty_tpl->tpl_vars['product']->value[0]['vImage'];?>
">			
		    </div>
		  <?php }else{ ?>
		  <div class="main_image multiple storePadHeight" style="text-align: center;" id="mainImage">
			 <img height="400px" width="300px" src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
No_image.jpg">
		    </div>	  
		  <?php }?>		  
		    <?php if (count($_smarty_tpl->tpl_vars['productColorImages']->value)>0){?>
		    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['productColorImages']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>	    
		    <div class="main_image multiple storePadHeight" style="text-align: center;display: none" id="colorimage_<?php echo $_smarty_tpl->tpl_vars['iProductId']->value;?>
<?php echo $_smarty_tpl->tpl_vars['productColorImages']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iColorId;?>
" >
			 <?php if ($_smarty_tpl->tpl_vars['productColorImages']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vFrontImage!=''){?>
			 <img height="400px" width="300px" src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['productColorImages']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['productColorImages']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
/<?php echo $_smarty_tpl->tpl_vars['productColorImages']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vFrontImage;?>
">
			 <?php }else{ ?>
			 <img height="400px" width="300px" src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
No_image.jpg">
			 <?php }?>
		    </div>		  
		    <?php endfor; endif; ?>
		    <?php }?>
		    
		    
		    
		</div>
	    </div>
	    
	    
	    <div class="product_detail_right">
		<div id="Module_ApparelQuote_container">
		    <h4 class="quote_steps_head">Step 1 - Select Shirt</h4>
		    <div class="otherImageLinks" style="font-size: 16px;"><p>Style: <?php echo $_smarty_tpl->tpl_vars['product']->value[0]['vProductName'];?>
 <a style="text-align: center; color: #9E053B; font-size: 14px;" href="#popfancy" id="fancyhref">(Switch Product)</a></p>
			<div style="display:none;" height="600px" width="100%">
			    <div id="popfancy">
				
<div class="main-container col2-right-layout">
    <div class="main">
	  <div class="main-inner clearfix">				
		<div class="breadcrumbs row-fluid">
		</div>
		<div class="container">
		    <h2 class="pagetital subtit" style="margin-left: 20px !important;">Switch Products </h2>
		    <div class="row-fluid show-grid" >
			  <div class="col-main span9" id="update_div">		
				<div class="col-main-inner" id="maindivHide">
				    <p class="category-image" style="display:block;"></p>
				    <div class="category-products">
					  <div class="toolbar"></div>
					  <div id="pageData">
						<?php if (count($_smarty_tpl->tpl_vars['all_category_data']->value)>0){?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_category_data']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<div class="products-grid row-fluid show-grid">
						    <div class="item first span4">
							  <div class="item-inner content">
								<div class="product-image">
								    <div class="product-thumb">
									  <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant-quote/<?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
" class="product-image visible-desktop" id="product_31">
									  <img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
/1X160_<?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="Cras in risus et risus" />
									  </a>
								    </div>
								</div>
								<div class="mask" onclick="setLocation('#')">							
								    <h3 class="product-name">
								    <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant-quote/<?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
"><?php echo $_smarty_tpl->tpl_vars['all_category_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
</a>
								    </h3>
								</div>
								<div class="top-actions-inner">
								    <div class="mt-actions clearfix">
								    </div>					
								</div>
							  </div>
						    </div>
						</div>
						<?php endfor; endif; ?>
						<?php }else{ ?>
						<span style="margin-left: 50%; font-size: medium;">No Item Found</span>
						<?php }?>
						<div class="mt-row-page">
						  <div id="ajax_paging">
						  <div class="Pagingbox">
							   <?php echo $_smarty_tpl->tpl_vars['pages']->value;?>

						   </div>
						   <?php echo $_smarty_tpl->tpl_vars['recmsg']->value;?>

						  </div>
					   </div>
					  </div>
					  <!-- <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script> -->
					  <div class="toolbar-bottom">
						<div class="toolbar"></div>
					  </div>
				    </div>
				</div>
			  </div>
			  
			  <div class="col-right sidebar span3 visible-desktop"> 
				<div class="block mt-smartmenu">
				    <div class="block-title">
					  <strong><span>Category</span></strong>
				    </div>
				    <div class="title-divider">
					  <span>&nbsp;</span>
				    </div>
				    <div class="block-content">
					  	
					  <!-- <script type="text/javascript" src="js/mt.accordion.js"></script> -->
					  <script type="text/javascript">
						$mtkb(document).ready(function(){	
						// applying the settings
						$mtkb("#mt-accordion li.active span.head").addClass("selected");
						$mtkb('#mt-accordion').Accordion({
						active: 'span.selected',
						header: 'span.head',
						alwaysOpen: false,
						animated: true,
						showSpeed: 400,
						hideSpeed: 800,
						event: 'click'
						});
						});	
					  </script>
					  
					  <ul id="mt-accordion" class="clearfix">
						<?php if (count($_smarty_tpl->tpl_vars['categories']->value)>0){?>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['categories']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<?php if ($_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iParentId=='0'){?>
						<li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
						    <a href="javascript:void(0);" onclick = "getProduct('<?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCategoryId;?>
');"><span><?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vCategory;?>
</span></a><span class="head"><a href="#" style="float:right;"></a></span>
						    <ul class="level0">
							  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['categories']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
							  <?php if ($_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iParentId!='0'&&$_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iParentId==$_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCategoryId){?>
							  <li class="level1 nav-categories-footwear-man">
								<a href="javascript:void(0);" onclick = "getProduct('<?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCategoryId;?>
');"><span><?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCategory;?>
</span></a>
							  </li>
							  <?php }?>
							  <?php endfor; endif; ?>
						    </ul>
						</li>
						<?php }?>
						<?php endfor; endif; ?>
						<?php }?>
					  </ul>
				    </div> 
				</div>
			  </div>
		    </div>
		</div>
	  </div>
	  <div class="mt-productcroller-container clearfix"></div>
    </div>
</div>


<script type='text/javascript'>
function changePagination(start){
    //alert(pageId);
    //alert(liId);
    var q = "<?php echo $_smarty_tpl->tpl_vars['alp']->value;?>
";
    //alert(q);
    if(q){
	   var dataString = 'start='+ start+'&q='+q;
    }else{
	   var dataString = 'start='+ start;
    }
    var url = site_url+'category/ajaxpage';
    //alert(url);
    //var dataString = 'start='+ start;
    //alert(dataString);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		$("#pageData").html(result);
	  }
    });
}
</script>

				
				
				
				
			    </div>
			</div>
		    </div>
		    
		    
		    <form id="frmGetQuote" method="post" action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant_quote/calculate_quote" enctype="multipart/form-data">
		    <input type="hidden" name="iProductId" value="<?php echo $_smarty_tpl->tpl_vars['product']->value[0]['iProductId'];?>
">			 
		    <h4 class="quote_steps_head">Step 2 - Select Color</h4>
		    <div class="inner" style="background-color: #EDEDED; padding: 2%; width: 98%; float: left; margin-bottom: 15px;">
			<div class="colors">
			    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['colors']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			    <div style="float: left; padding:2px;">
				    <div title="<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColor'];?>
" id="Module_ApparelQuote_colorSwatch_<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
" style="background-color: <?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColorCode'];?>
;" class="color" onclick="checkboxcheck('<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
');" onmouseover="return showColorImage('<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
');"  onmouseout="return hideColorImage('<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
<?php $_tmp2=ob_get_clean();?><?php echo $_tmp2;?>
');"></div>
			    </div>
			    <?php endfor; endif; ?>
			</div>
		    </div>
		    <h4 class="quote_steps_head">Step 3 - Enter Sizes and quantity</h4>
		    <div class="selectSizeBox" style="background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;">
			<div id="noneSizeSelected">Please select colors from step 2 before proceeding to step 3.</div>
			<table class="colors">
			    <tbody id="colorsizebox">
				<tr id="colorsizebox_root" style="display: none;">
				    <td><span>Colors</span></td>
				    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sizes']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				    <td style="padding-left: 10px;">
					<span><?php echo $_smarty_tpl->tpl_vars['sizes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSize'];?>
</span>
				    </td>
				    <?php endfor; endif; ?>
				</tr>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['colors']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<tr id="colorsizebox_<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
" style="display: none;">
				    <td><?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColor'];?>
</td>
				    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sizes']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
				    <td class="color_size">
					<span>
					    <?php if (in_array($_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'],$_smarty_tpl->tpl_vars['productColorArray']->value)&&in_array($_smarty_tpl->tpl_vars['sizes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'],$_smarty_tpl->tpl_vars['productSizeArray']->value[$_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId']])){?>
					    <input type="text" onchange="countQtyTotal(<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
,this.value);" onkeyup="changeTextboxQty(this.value);" name="color_size[orderqty-size-<?php echo $_smarty_tpl->tpl_vars['sizes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
-color-<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
]" style="width: 40px;" id="color_size_orderqty_size_<?php echo $_smarty_tpl->tpl_vars['sizes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
_color_<?php echo $_smarty_tpl->tpl_vars['colors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
">
					    <?php }else{ ?>
					    <input type="text" value="N/A" style="width: 40px;" disabled="disabled">
					    <?php }?>
					</span>
				    </td>
				    <?php endfor; endif; ?>
				</tr>
				<?php endfor; endif; ?>
				
			    </tbody>
			</table>
		    </div>
		    
		    <h4 class="quote_steps_head">Step 4 - Select Imprint colors and locations</h4>
		    <div id="noneDecorationSelection" style="background-color: #EDEDED; padding: 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div>Please enter quantities in step 3 before proceeding to step 4.</div>
		    </div>
		    <div id="decorationService" class="outer" style="display: none; background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div class="inner" style="">
			    <p>Select a decoration for one or more of the areas below.</p>
			    <div class="areaSelection">
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['printLocations']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<div class="area_66">
				    <input type="checkbox" class="print_location_text" id="print_location_checks_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
" name="print_location[printlocation_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
]" style="" onchange="selectPrintLocation('<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
');">
				    <label for="print_location_checks_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
"><?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocation'];?>
</label>
				</div>
				<?php endfor; endif; ?>
			    </div>
			  <!--   <table class="areas" style="">
				<tbody>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['printLocations']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				    <tr style="display: none;" id="screenprint1_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
">
					<td style="width: 15%; padding-left: 10px;"><span class="areaName" style="color:#002F5F"><?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocation'];?>
</span></td>
					<td style="padding: 10px; width: 30%;">
					    <select name="decoration_type[<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
]" style="" onchange="selectDecorationType(this.value,'<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
')">
						<option value="screenprinting" selected="selected">Screenprinting</option>
						<option value="embroidery">Embroidery</option>
						<!--<option value="DTG">DTG</option>
					    </select>
					</td>
					<td>
					    <div id="screenprint2_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
" style="display: none; padding: 5px 0px 0px 10px;" >
						<span style="float: left; margin: 0px 5px 0px 0px;">Number of colors: </span>
						<select name="number_of_colors[numberofcolors_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
]" class="screenprinting_colors" style="">
						    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['numberColors']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?> 
						    <option value="printlocation-<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
-numbercolor-<?php echo $_smarty_tpl->tpl_vars['numberColors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iNumberColorsId'];?>
"><?php echo $_smarty_tpl->tpl_vars['numberColors']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['vNumberColor'];?>
</option>
						    <?php endfor; endif; ?>
						</select>
					    </div>
					</td>
				    </tr>
				    <?php endfor; endif; ?>
				    
				</tbody>
			    </table>
			    -->
			    <div id="ajaxdiv">
				
				
				
			    </div>
			    
			    
			    
			</div>
		    </div>
		    
		    <h4 class="quote_steps_head">Step 5 - Turn Around Time</h4>
		    <div id="shipmentTypes" style="background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div class="rush">
			    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['shipments']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			    <input style="float: left; width: 4%;" type="radio" value="<?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iTurnAroundTimeId'];?>
"  id="shipmentId<?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iTurnAroundTimeId'];?>
" name="shipment" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']==0){?>checked="checked"<?php }?> style="">
			    <label style="width: 90%; float: left;" for="shipmentId<?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iTurnAroundTimeId'];?>
"><?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
 (<?php echo $_smarty_tpl->tpl_vars['shipments']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tNote'];?>
)</label>
			    <br><br>
			    <?php endfor; endif; ?>
			</div>
		    </div>
		    
		    <h4 class="quote_steps_head">Step 6 - Coupon Code (optional)</h4>
		    <div id="shipmentTypes" style="background-color: #EDEDED; padding: 2% 2% 0 2%; width: 98%; float: left; margin-bottom: 15px;" >
			<div class="rush">
			    <label style="width:15%; float: left;"> Coupon Code</label>
			    <input type="text" name="coupon_code" id="coupon_code">
			</div>
		    </div>
		    
		    <div style="border-top: 1px solid #CCCCCC; width:100%; float: left; padding-top:15px;">
			<div id="quotePrice"></div>
		    </div>
		    
		    
		    <div style="float: left; padding-top:15px;">
			<input type="submit" value="Quote Now" class="instnt_btn" style="float: left; background: #9e0546;border: 2px solid #d17a99; line-height: 25px;padding: 5px 15px;">			
			 <div id="uploadArtworkButton" style="float: left;">
				<a href="#upload-artwork" id="clickUploadArtwork"></a>
				<input type="button" onclick="uploadArtworkAndProceedCart();" value="Upload Artwork and Proceed to Cart" class="instnt_btn" id="upload_artwork_cart" style="float: left; margin-left: 20px;background: #9e0546; border: 2px solid #d17a99;line-height: 25px; padding: 5px 15px;display: none;" >
				<div id="ajax_loader" style="color:#c03b37;margin-top: -3px;padding: 10px; font-size:16px;text-align:center;display: none; "><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
ajax-loader.gif">  Loading...</div>
			 </div>
		    </div>    
		    </form>
		
		    <div style="display: none;" id="save_quote">
			<div>
			    <form id="frmSaveQuote" name="frmSaveQuote" action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant_quote/saveQuote" method="post">
				<h4>Email a quote</h4>
				Enter Your Email Address: 
				<input type="text" name="emailAddress" id="emailAddress">
				<input type="button" onclick="saveQuote();" value="Email a quote" id="savebutton" style="float:right;margin-right: 110px;width: 26%;"> 
			    </form>
			</div>
		    </div>
		
		
		    <div style="display: none" id="sadfadsf">
			<div id="upload-artwork" style="width:1200px;">
			 
			    <form id="frmUploadArtwork" method="post" name="frmUploadArtwork" action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant_quote/uploadArtwork" enctype="multipart/form-data">
				
				<div style="width:50%; float: left; margin-right:20px;">
				    <h4>Upload Artwork</h4>
				    <lable>Please upload the gif,jpg,jpeg,png,bmp,eps,psd,ps,ai,tif,tiff,pdf,cdr extension files</lable>
				    <br>
				    <lable style="font-weight: bold;color: red;" id="errorMsgg">
					   
				    </lable>
				    
				    <div>
					<table>
					    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['printLocations']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					    
					    <tr style="display: none;" id="artwork_location_<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
">
						<td><?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocation'];?>
</td>
						<td>Upload artwork:
						    <br>
						    <input type="file" id="<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
" name="artwork_img-<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
"  onchange="return imagename(this.value,'first',<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
);">
						</td>
						<td><textarea name="artwork_comment[<?php echo $_smarty_tpl->tpl_vars['printLocations']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPrintLocationId'];?>
]" placeholder="write your comments here..."></textarea></td>
					    </tr>
					    <?php endfor; endif; ?>
					</table>
				    </div>
				</div>				
				<div style="width:40%; float: left;" id="printLocationNumbers">
				    <h4>Add Names & Numbers</h4>
				    <div>
					<table id="nameAndNumberTABLE">
					    <tr>
						  <td id="NumberFront">
							 Number Front
						  </td>
						  <td id="NumberBack">
							 Number Back
						  </td>
						  <td id="CustomNameBack">
							 Custom Name Back
						  </td>
						  <td>
							 Size
						  </td>
					   </tr>
					    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 9+1 - (0) : 0-(9)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
						<tr id="nameAndNumberTR_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
						    <td id="NumberFrontTD_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
							<input type="text" style="width:70px;">
						    </td>
						    <td id="NumberBackTD_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
							<input type="text" style="width:70px;">
						    </td>
						    <td id="CustomNameBackTD_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
							<input type="text" style="width:120px;">
						    </td>
						    <td>
							<select style="width:70px;" >
							    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sizes']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
							    <option><?php echo $_smarty_tpl->tpl_vars['sizes']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['vSize'];?>
</option>
							    <?php endfor; endif; ?>
							</select>
						    </td>
						</tr>
					    <?php }} ?>								
					</table>
				    </div>
				</div>

				<div>
				    <input onclick="submitArtwork()"  type="button" value="Proceed to Cart" class="instnt_btn" id="proceed_addcartsdsad22" style="float: right;" >
				</div>
				
			    </form>
			    
			    
			</div>
		    </div>
		    
			<script>
			    var numDecorationService = 0;
			    function changeTextboxQty(val) {
				//alert(val);
				$('.color_size').find('span').each(function() {
					var textTag = $(this).html();
					//alert($(textTag).attr('value'));
				});
				
				if (val != 0) {
				    $('#decorationService').show();
				    $('#noneDecorationSelection').hide();
				}
			    }
			    
			    function countQtyTotal(iColorId, val) {
				if ($('#Module_ApparelQuote_colorSwatch_'+iColorId).html() != '') {
				    totalQty += parseInt(val);
				}
				fianlTotalQty=totalQty;
			    }
			    
			    var numSizeBoxs = 0;
			    function checkboxcheck(colorId) {
				if ($('#Module_ApparelQuote_colorSwatch_'+colorId).html() == '') {
				    $('#Module_ApparelQuote_colorSwatch_'+colorId).html('<img src="http://www.fairfaxscreenprinting.com/module_support/ApparelQuote/check.png" class="transpng">');
				    if (numSizeBoxs == 0) {
					$('#noneSizeSelected').hide();
					$('#colorsizebox').append('<tr id="finalcolorsizebox_root" style="color:#002F5F;">'+$('#colorsizebox_root').html()+'</tr>');
				    }
				    $('#colorsizebox').append('<tr id="finalcolorsizebox_'+colorId+'">'+$('#colorsizebox_'+colorId).html()+'</tr>');
				    numSizeBoxs++;
				}else{
				    $('#Module_ApparelQuote_colorSwatch_'+colorId).html('');
				    $('#finalcolorsizebox_'+colorId).remove();
				    numSizeBoxs--;
				    if (numSizeBoxs == 0) {
					$('#finalcolorsizebox_root').remove();
					$('#noneSizeSelected').show();
				    }
				}
			    }
			</script>
		    
		</div>
    	    </div>
	    
	    </div>
	    
	    
	    
	    
	    
	    <script>
		var totalQty = 0;
		
		var ele_numberFront=[];
		  $('td[id^="NumberFrontTD_"]').each(function(){
			var $this=  $(this);
			ele_numberFront.push($this.attr("id"));	    
		  });
		  var numberFrontTds=ele_numberFront;
		  var numberFrontTds_length=numberFrontTds.length;		
		
		var ele_numberBack=[];
		  $('td[id^="NumberBackTD_"]').each(function(){
			var $this=  $(this);
			ele_numberBack.push($this.attr("id"));	    
		  });
		  var numberbacktds=ele_numberBack;
		  var numberbacktds_length=numberbacktds.length;
		  
		  
		  var ele_customBack=[];
		  $('td[id^="CustomNameBackTD_"]').each(function(){
			var $this=  $(this);
			ele_customBack.push($this.attr("id"));	    
		  });
		  var cumtomNameBackTds=ele_customBack;
		  var cumtomNameBackTds_length=cumtomNameBackTds.length;
		  
		  
		   
		
		
		$("#upload_artwork_cart").click(function(){		  
		   var someObj={};
		  someObj.checked=[];
		  someObj.unchecked=[];		  
		  $(".print_location_text:input:checkbox").each(function(){
			 var $this = $(this);	
			 if($this.is(":checked")){
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.checked.push(finalid);
			 }else{
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.unchecked.push(finalid);
			 }
		  });		  
	       var all_cheked_checkbox=someObj.checked;
		  var len=all_cheked_checkbox.length;
		  var json = JSON.stringify(all_cheked_checkbox);	  
		  var site_url='<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
';
		  var extra='';				
		  extra+='?json='+json;		  		  
		  var url=site_url+'instant_quote/checkPrintLocation';			 
		  var pars=extra;
		  
		  $('#clickUploadArtwork').hide();
		  $('#upload_artwork_cart').hide();
		  $('#ajax_loader').show();		  
		  $.post(url+pars,			    
			 function name(data){				
			  $('#ajax_loader').hide();			
			   if (data.trim()=='Number Front,Number Back,Custom Name Back'){
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").show();
				$("#CustomNameBack").show();
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).show();
				    $("#"+cumtomNameBackTds[i]).show();	    
				}				
			   }else if (data.trim()=='Number Front,Number Back') {
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").show();
				$("#CustomNameBack").hide();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).show();
				    $("#"+cumtomNameBackTds[i]).hide();	    
				}		
			   }else if (data.trim()=='Number Front,Custom Name Back') {
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").hide();
				$("#CustomNameBack").show();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).hide();
				    $("#"+cumtomNameBackTds[i]).show();	    
				}			
			   }else if (data.trim()=='Number Back,Custom Name Back') {
				$("#printLocationNumbers").show();
				$("#NumberFront").hide();
				$("#NumberBack").show();
				$("#CustomNameBack").show();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).hide();
				    $("#"+numberbacktds[i]).show();
				    $("#"+cumtomNameBackTds[i]).show();	    
				}		
			   }
			   
			   else if (data.trim()=='NumberFront'){
				$("#printLocationNumbers").show();
				$("#NumberFront").show();
				$("#NumberBack").hide();
				$("#CustomNameBack").hide();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberFrontTds[i]).show();
				    $("#"+numberbacktds[i]).hide();
				    $("#"+cumtomNameBackTds[i]).hide();	    
				}					
			  }else if (data.trim()=='NumberBack') {
				$("#printLocationNumbers").show();				
				$("#NumberBack").show();
				$("#NumberFront").hide();
				$("#CustomNameBack").hide();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+numberbacktds[i]).show();
				    $("#"+numberFrontTds[i]).hide();
				    $("#"+cumtomNameBackTds[i]).hide();	    
				}			
			  }else if (data.trim()=='CustomNameBack') {
				$("#printLocationNumbers").show();				
				$("#NumberBack").hide();
				$("#NumberFront").hide();
				$("#CustomNameBack").show();				
				for(i=0;i<numberbacktds_length;i++){
				    $("#"+cumtomNameBackTds[i]).show();
				    $("#"+numberFrontTds[i]).hide();
				    $("#"+numberbacktds[i]).hide();
				    //$("#"+cumtomNameBackTds[i]).hide();	    
				}			
			  }		  
			  else{
				$("#printLocationNumbers").hide();
			  }
			  
			  $('#clickUploadArtwork').click();		  
			  $('#clickUploadArtwork').show();
			  $('#upload_artwork_cart').show();
			  
			 });	 
		});
		
		function proceedCartFun() {
		   window.location = site_url + 'instant_quote/addtocart';
		}
		         
		function uploadArtworkAndProceedCart(){		  
		    $('#frmGetQuote').submit();
		}
	
	
		$(document).ready(function(){ 
			$('#frmGetQuote').ajaxForm(function(data) {			      
			      $('#quotePrice').html(data);
			      //alert(data);
				 $("#upload_artwork_cart").show();		 
			      return false;
			});
		});
		
		function submitArtwork() {
			$('#frmUploadArtwork').ajaxSubmit({
				success: function(data){
				    //alert(data);
				    proceedCartFun();
				}
			});
		}
		function saveQuote() {		    
		   hidefile();
		   var email=$("#emailAddress").val();
		   var site_url='<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
';
		   var extra="";
		   extra+='?email='+email;
		   var pars=extra;
		   var url=site_url+'instant_quote/saveQuote';
		   //alert(url+pars);return false;
		   $('#Quote').html('<div style="color:#c03b37; font-size:15px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Saving...</div>');
		   $.post(url+pars,
			function(data){			  		  			 
			  $('#Quote').html('<a style="color: #9E053B; font-size: 16px; text-align: center;" href="#save_quote" id="saveQuote">Save Quote</a>');				  
			}
		    );
		   
		  }
			
		function hidefile(){
			 $.fancybox.close(); 
		}
		
		$('#clickUploadArtwork').fancybox({
		    'overlayShow'	: true,
		    'openEffect'	: 'elastic',
		    'openSpeed'	: 250,
		    'closeEffect'	: 'elastic',
		    'closeSpeed'	: 250,
		});
			
		$('#saveQuote').fancybox({
		    'overlayShow'	: true,
		    'openEffect'	: 'elastic',
		    'openSpeed'	: 250,
		    'closeEffect'	: 'elastic',
		    'closeSpeed'	: 250,
		});
		function hidefile(){
		    $.fancybox.close(); 
		}
			
		 var fianlTotalQty='';	
		function selectPrintLocation(iPrintLocationId){		  
		  if ($("#print_location_checks_"+iPrintLocationId).is(':checked')){
			 $('#artwork_location_'+iPrintLocationId).show();
		    }else{			
			$('#artwork_location_'+iPrintLocationId).hide();
		  }
		  $(".print_location_text:input:checkbox").each(function(){
			 $(this).attr('disabled','disabled');
		  });	  
		  var someObj={};
		  someObj.checked=[];
		  someObj.unchecked=[];		  
		  $(".print_location_text:input:checkbox").each(function(){
			 var $this = $(this);	
			 if($this.is(":checked")){
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.checked.push(finalid);
			 }else{
				var checkboxid=$this.attr("id");
				var finalid=checkboxid.substring(22);
				someObj.unchecked.push(finalid);
			 }
		  });		  
	       var all_cheked_checkbox=someObj.checked;
		  var len=all_cheked_checkbox.length;		  
		  if (len>0){					  
				var json = JSON.stringify(all_cheked_checkbox);	  
				var site_url='<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
';
				var iProductId='<?php echo $_smarty_tpl->tpl_vars['iProductId']->value;?>
';		  
				var extra='';
				extra+='?iProductId='+iProductId;
				extra+='&json='+json;		  
				extra+='&fianlTotalQty='+fianlTotalQty;		  
				var url=site_url+'instant_quote/ajax_printlocation';			 
				var pars=extra;
				//alert(url+pars);return false;
				$('#ajaxdiv').html('<div style="color:#c03b37;float: center;margin-top: 58px;padding: 10px; font-size:16px;text-align:center "><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');		  
				$.post(url+pars,			    
					  function name(data){					   
					  $("#ajaxdiv").html(data);
					  selectDecorationType('screenprinting',iPrintLocationId)
					   $(".print_location_text:input:checkbox").each(function(){
						   $(this).removeAttr('disabled','disabled');
					   });
				    });
			 }else{
				$(".print_location_text:input:checkbox").each(function(){
				    $(this).removeAttr('disabled','disabled');
			    });
				$("#ajaxdiv").html('');
			 }
		  }
		  function selectDecorationType(decorationType, iPrintLocationId) {		    
		    if (decorationType == 'screenprinting') {
			$('#screenprint2_'+iPrintLocationId).show();
		    }else{
			$('#screenprint2_'+iPrintLocationId).hide();
		    }	    
		  }
		
    $("#fancyhref").fancybox({
	padding: 0,
	openEffect : 'elastic',
	openSpeed  : 250,
	closeEffect : 'elastic',
	closeSpeed  : 250,
	hideOnOverlayClick: false,
	hideOnContentClick:false,
	autoScale : false,
	frameWidth  : 100,
	frameHeight : 600,
	helpers :{
	    closeClick: false
	}
    });
    
    
    function getProduct(id) {
	var extra = '';
	extra+='?iCategoryId='+id;
	var url = site_url + 'instant_quote/ajax_categaorydata';
	$('#maindivHide').html('<div style="color:#c03b37; font-size:16px;text-align:center "><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');
	var pars = extra;
	$.post(url+pars,
	    function(data) {
	      $('#update_div').html(data);
	    }
	);
    }
    function imagename(val,name,id){
	   //alert(id);
		$("#"+name).text(val);
		$("#firstimage").val(val);		
		var a = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
		if(a == 'gif' || a == 'GIF' || a == 'jpg'  ||a == 'JPG' ||a == 'jpeg' ||a == 'JPEG' ||a == 'png' ||a == 'bmp' ||a == 'BMP' ||a == 'eps' ||a == 'EPS' ||a == 'psd' ||a == 'PSD' ||a == 'ps' ||a == 'PS' ||a == 'ai' ||a == 'AI' ||a == 'tiff' ||a == 'TIFF' ||a == 'tif' ||a == 'TIF' ||a == 'pdf' ||a == 'PDF' ||a == 'cdr' ||a == 'CDR'      ){	   
		return true;	
		}
		else{
			//alert('extension not valid');			
			$("#errorMsgg").html('The extension is not valid , Please use valid extension');			
			$("#"+id).val("");
			return false;
		}		
	}
	
	function showColorImage(iColorId){	   
	  var iProductId='<?php echo $_smarty_tpl->tpl_vars['iProductId']->value;?>
';
	  var colorImage=iProductId+iColorId;	  
	  $("#mainImage").hide();  
	  $("#colorimage_"+colorImage).show();	  
	}
	
	function hideColorImage(iColorId){
	   var iProductId='<?php echo $_smarty_tpl->tpl_vars['iProductId']->value;?>
';
	  var colorImage=iProductId+iColorId;
	  $("#colorimage_"+colorImage).hide();	  
	  $("#mainImage").show();  
	  
	}
	

</script>

	    
        </div>
    </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>