<?php /* Smarty version Smarty-3.1.11, created on 2013-09-04 10:55:42
         compiled from "application/views/templates/admin/newsletter_format/view-newsletter_format.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32254618151bf18cc4104b1-99631526%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '140ebe1066691bcdd0b05c4f9905f1fd8bc41dbe' => 
    array (
      0 => 'application/views/templates/admin/newsletter_format/view-newsletter_format.tpl',
      1 => 1377257164,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32254618151bf18cc4104b1-99631526',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51bf18cc724435_83587566',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'var_msg' => 0,
    'keyword' => 0,
    'option' => 0,
    'AlphaBox' => 0,
    'order' => 0,
    'field' => 0,
    'data' => 0,
    'class' => 0,
    'ssql' => 0,
    'recmsg' => 0,
    'page_link' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51bf18cc724435_83587566')) {function content_51bf18cc724435_83587566($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li class="current"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Newsletter Format</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Newsletter Format</div>
		<?php if ($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		 <div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" />
			  <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p> 
		 </div>     
		<div></div>
		<?php }elseif($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv"> 
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p> 
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" />
			  <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p> 
		</div>
		<div></div>
		<?php }?>
		<div class="admin_top_part">
			<div class="addnew_btn"> <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/add" style="text-decoration:none;">Add New </a> </div>
			
			<div class="search_top_admini">
				<form name="frmsearch" id="frmsearch" action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist" method="post">
					<label>Search:</label>
					<div id="newstatus" class="statusleft">
						<?php if ($_smarty_tpl->tpl_vars['keyword']->value=='Active'||$_smarty_tpl->tpl_vars['keyword']->value=='Inactive'){?>
							<select name="keyword" id="keyword">
								<option value="Active"<?php if ($_smarty_tpl->tpl_vars['keyword']->value!=''){?><?php if ($_smarty_tpl->tpl_vars['keyword']->value=='Active'){?>selected="selected"<?php }?><?php }?>>Active</option>
								<option value="Inactive"<?php if ($_smarty_tpl->tpl_vars['keyword']->value!=''){?><?php if ($_smarty_tpl->tpl_vars['keyword']->value=='Inactive'){?>selected="selected"<?php }?><?php }?>>Inactive</option>
							</select>
						<?php }else{ ?>
							<input type="Text" id="keyword" name="keyword" value="<?php if ($_smarty_tpl->tpl_vars['keyword']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>"  class="search_input" />
						<?php }?>
					</div>
					
					<select name="option" id="option" onchange="estatusdd(this.value);">
						<option  value='vTitle'<?php if ($_smarty_tpl->tpl_vars['option']->value=='vTitle'){?>selected<?php }?>>Title</option>
						<option  value="dAddedDate"<?php if ($_smarty_tpl->tpl_vars['option']->value=='dAddedDate'){?>selected<?php }?>>Added Date</option>
						<option value="dLastSentDate"<?php if ($_smarty_tpl->tpl_vars['option']->value=='dLastSentDate'){?>selected<?php }?>>Last Sent Date</option>
						<option value="eSendStatus"<?php if ($_smarty_tpl->tpl_vars['option']->value=='eSendStatus'){?>selected<?php }?>>Send Status</option>
						<option value="eStatus"<?php if ($_smarty_tpl->tpl_vars['option']->value=='eStatus'){?>selected<?php }?>>Status</option>
					</select>
					<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
				</form>
			
			<div class="clear"></div>
			<div class="action_apply_btn">
							<select class="act_sl_box" name="newaction" id="newaction">
								<option value="">Select Action</option>
								<option value="Active">Make Active</option>
								<option value="Inactive">Make Inactive</option>
								<option value="Deletes">Make Delete</option>
								<option value="Show All">Show All</option>
							</select>
							<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'newsletter_formatlist',document.frmlist,'Newsletter Format');"/>
						</div>
			</div>
			<div class="pagination">
				<?php echo $_smarty_tpl->tpl_vars['AlphaBox']->value;?>

			</div>
			<div class="clear"></div>
			
		</div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/search_action" method="post">
			<input type="hidden" name="action" id="action" value="" />
			<input  type="hidden" name="iNformatId" value=""/>
			<table cellpadding="0" cellspacing="1" width="100%">
				<thead>
					<tr>
						<th><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
						<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist?field=vTitle&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Title</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='vTitle'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
						<th>Send Email</th>
						<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist?field=dAddedDate&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Added Date</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='dAddedDate'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
						<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist?field=dLastSentDate&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Last Sent Date</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='dLastSentDate'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
						<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist?field=eSendStatus&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Send Status</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='eSendStatus'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
						<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist?field=eStatus&order=<?php echo $_smarty_tpl->tpl_vars['order']->value;?>
><span style="color:white;">Status</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='eStatus'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
						<th>Action</th>
					</tr>
				</thead>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2==0){?>
				<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table_sec', null, 0);?>
				<?php }else{ ?>
				<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table', null, 0);?>
				<?php }?>
				<tbody class="<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
">
					<tr>
						<td><input name="iNformatId[]" type="checkbox" id="iId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
"></td>
						<td><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/edit?iNformatId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vTitle;?>
</a></td>
						<td><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/sendemail?iNformatId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
">Send Email</a></td>
						<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->dAddedDate;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->dLastSentDate;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->eSendStatus;?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->eStatus;?>
</td>
						<td>
							<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/edit?iNformatId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);" onclick="MakeAction('<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
','Active');"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_approve.png" alt="Active" title="Active"></a>
							<a href="javascript:void(0);" onclick="MakeAction('<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
','Inactive');"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_unapprove.png" alt="Inactive" title="Inactive"></a>
							<a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon('<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iNformatId;?>
','newsletter_format/delete','<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
')"></a> </td>
						
					</tr>
				</tbody>
				<?php endfor; endif; ?>
			</table>
			</form>
		</div>
		
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found"><?php echo $_smarty_tpl->tpl_vars['recmsg']->value;?>
</td>
					<td>
						
					</td>
					<td>
						<div class="bottom_admin_paging">
							<?php echo $_smarty_tpl->tpl_vars['page_link']->value;?>
 
						</div>
					</td>
				</tr>
			</table>
			
			<div>
				
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script>
function Searchoption(){
    document.getElementById('frmsearch').submit();
}
function AlphaSearch(val){
	var alphavalue = val;
	window.location= admin_url+"newsletter_format/newsletter_formatlist?alp="+alphavalue;
	return false;
}
function MakeAction(loopid,type){
	document.frmlist.iNformatId.value = loopid;
	document.frmlist.action.value = type;
	document.frmlist.submit();	
}
function deletenewsletter(a)
{
	
if(confirm("Are you sure you really want to delete this Newsletter Format?"))
{
window.open(admin_url+"/newsletter_format/delete/"+a,'_self',false);
}
}
function hidemessage(){
    jQuery("#errormsgdiv").slideUp();
}

function estatusdd(val1){
	
	var keyword=$('#keyword').val();
	if(val1 == 'dAddedDate')
	{
		$('#newstatus').html('<input name="keyword" type="text" class="search_input" id="dDate" value="" >');
		$('#dDate').Zebra_DatePicker();	
	}
        if(keyword =='Active' || keyword =='Inactive')
	{
		keyword='';
	}
	if(val1 == 'eStatus')
	{
		$('#newstatus').html('<select name="keyword" id="keyword"><option value="Active"{if $keyword != ""}{if $keyword eq "Active"}selected="selected"{/if}{/if}>Active</option><option value="Inactive"{if $keyword != ""}{if $keyword eq "Inactive"}selected="selected"{/if}{/if}>Inactive</option></select>');
	}
	else{
		$('#newstatus').html('<input type="Text" id="keyword" name="keyword" value="'+keyword+'"  class="search_input" />');
	}
}
</script>
<?php }} ?>