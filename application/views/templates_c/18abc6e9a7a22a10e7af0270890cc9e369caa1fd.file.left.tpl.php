<?php /* Smarty version Smarty-3.1.11, created on 2013-12-12 10:39:12
         compiled from "application/views/templates/admin/left.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1811732829514d3521d523e3-60792426%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18abc6e9a7a22a10e7af0270890cc9e369caa1fd' => 
    array (
      0 => 'application/views/templates/admin/left.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1811732829514d3521d523e3-60792426',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514d3521da32a9_20096876',
  'variables' => 
  array (
    'admin_url' => 0,
    'filename' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514d3521da32a9_20096876')) {function content_514d3521da32a9_20096876($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['ssql'] = new Smarty_variable('', null, 0);?>
<div class="leftpart">
<div class="left_navbar">
	<ul id="nav">
         
		<li>
			<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard" class="collapsed heading leftnone">Dashboard</a>
			
			
    		</li>
        
		<li> <a class="collapsed heading">Master Module</a>
			<?php if ($_smarty_tpl->tpl_vars['filename']->value=='administrator'||$_smarty_tpl->tpl_vars['filename']->value=='country'||$_smarty_tpl->tpl_vars['filename']->value=='state'||$_smarty_tpl->tpl_vars['filename']->value=='language'||$_smarty_tpl->tpl_vars['filename']->value=='languagelable'||$_smarty_tpl->tpl_vars['filename']->value=='loginhistory'||$_smarty_tpl->tpl_vars['filename']->value=='zipcode'){?>
			<ul class="navigation" style="display:block !important;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
administrator/adminlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='administrator'){?>class="active_tab"<?php }?>>Administrator</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
country/countrylist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='country'){?>class="active_tab"<?php }?>>Country</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
state/statelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='state'){?>class="active_tab"<?php }?>>State</a></li>
				<!--<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
language/languagelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='language'){?>class="active_tab"<?php }?>>Language</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='languagelable'){?>class="active_tab"<?php }?>>Language Lable</a></li>-->
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
loginhistory/loginhistorylist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='loginhistory'){?>class="active_tab"<?php }?>>Login History</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
zipcode/zipcodelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='zipcode'){?>class="active_tab"<?php }?>>Delivery Zipcode</a></li>
			</ul>
			<?php }else{ ?>
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
administrator/adminlist">Administrator</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
country/countrylist">Country</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
state/statelist">State</a></li>
				<!--<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
language/languagelist">Language</a></li>-->
				<!--<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist">Language Lable</a></li>-->
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
loginhistory/loginhistorylist">Login History</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
zipcode/zipcodelist">Delivery ZipCode</a></li>
			</ul>
			<?php }?>
		</li>
		
		<li> <a class="collapsed heading">Users</a>
			<?php if ($_smarty_tpl->tpl_vars['filename']->value=='user'||$_smarty_tpl->tpl_vars['filename']->value=='coupon'){?>
			<ul class="navigation">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/userlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='user'){?>class="active_tab"<?php }?>>User</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
coupon/couponlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='coupon'){?>class="active_tab"<?php }?>>Coupon</a></li>
			</ul>
			<?php }else{ ?>
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/userlist">User</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
coupon/couponlist">Coupon</a></li>
			</ul>
			<?php }?>
		</li>
		
		<li> <a class="collapsed heading">Product template</a>
			<?php if ($_smarty_tpl->tpl_vars['filename']->value=='productcategory'||$_smarty_tpl->tpl_vars['filename']->value=='product'||$_smarty_tpl->tpl_vars['filename']->value=='fabrics'||$_smarty_tpl->tpl_vars['filename']->value=='brand'||$_smarty_tpl->tpl_vars['filename']->value=='occupations'){?>
			<ul class="navigation">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
productcategory/productcategorylist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='productcategory'){?>class="active_tab"<?php }?>>Product Category</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='product'){?>class="active_tab"<?php }?>>Product Templates</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
fabrics/fabricslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='fabrics'){?>class="active_tab"<?php }?>>Fabrics</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
brand/brandlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='brand'){?>class="active_tab"<?php }?>>Brand</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
occupations/occupationslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='occupations'){?>class="active_tab"<?php }?>>Occupations</a></li>
				
			</ul>
			<?php }else{ ?>
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
productcategory/productcategorylist">Product Category</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist">Product Templates</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
fabrics/fabricslist">Fabrics</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
brand/brandlist">Brand</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
occupations/occupationslist">Occupations</a></li>
			</ul>
			<?php }?>
		</li>
		
		<li> <a class="collapsed heading">Orders</a>
			<?php if ($_smarty_tpl->tpl_vars['filename']->value=='order'){?>
			<ul class="navigation">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='order'){?>class="active_tab"<?php }?>>Orders</a></li>				
			</ul>
			<?php }else{ ?>
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
order">Orders</a></li>			
			</ul>
			<?php }?>
		</li>
		<li> <a class="collapsed heading">Design Manager</a>
			<?php if ($_smarty_tpl->tpl_vars['filename']->value=='designcategory'||$_smarty_tpl->tpl_vars['filename']->value=='design_images'||$_smarty_tpl->tpl_vars['filename']->value=='size'||$_smarty_tpl->tpl_vars['filename']->value=='color'||$_smarty_tpl->tpl_vars['filename']->value=='printcolor'||$_smarty_tpl->tpl_vars['filename']->value=='printlocation'||$_smarty_tpl->tpl_vars['filename']->value=='quantityrange'){?>
		        <ul class="navigation">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
designcategory/designcategorylist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='designcategory'){?>class="active_tab"<?php }?>>Clipart Category</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
design_images/design_imageslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='design_images'){?>class="active_tab"<?php }?>>Clipart</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
printlocation/printlocationlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='printlocation'){?>class="active_tab"<?php }?>>Print Location</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
printcolor/printcolorlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='printcolor'){?>class="active_tab"<?php }?>>Print Color</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
size/sizelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='size'){?>class="active_tab"<?php }?>>Size</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
color/colorlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='color'){?>class="active_tab"<?php }?>>Color</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
quantityrange/quantityrangelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='quantityrange'){?>class="active_tab"<?php }?>>Quantity Range</a></li>
			</ul>
			<?php }else{ ?>
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
designcategory/designcategorylist">Clipart Category</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
design_images/design_imageslist">Clipart</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
printlocation/printlocationlist">Print Location</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
printcolor/printcolorlist">Print Color</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
size/sizelist">Size</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
color/colorlist">Color</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
quantityrange/quantityrangelist">Quantity Range</a></li>
				
			</ul>
			<?php }?>
		</li>
		
		<li> <a class="collapsed heading">Shipping</a>
		<?php if ($_smarty_tpl->tpl_vars['filename']->value=='shipping'||$_smarty_tpl->tpl_vars['filename']->value=='turnaroundtime'){?>
			<ul class="navigation">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
shipping/shippinglist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='shipping'){?>class="active_tab"<?php }?>>Shipping Methods</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
turnaroundtime/turnaroundtimelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='turnaroundtime'){?>class="active_tab"<?php }?>>Turn Around Time</a></li>
			</ul>
		<?php }else{ ?>
		        <ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
shipping/shippinglist">Shipping Methods</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
turnaroundtime/turnaroundtimelist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='turnaroundtime'){?>class="active_tab"<?php }?>>Turn Around Time</a></li>
			</ul>
		<?php }?>
		</li>
		<li> <a class="collapsed heading">Settings</a>
			<?php if ($_smarty_tpl->tpl_vars['filename']->value=='configuration'||$_smarty_tpl->tpl_vars['filename']->value=='newslettermembers'||$_smarty_tpl->tpl_vars['filename']->value=='newsletter_format'||$_smarty_tpl->tpl_vars['filename']->value=='bannergroup'||$_smarty_tpl->tpl_vars['filename']->value=='customerreview'||$_smarty_tpl->tpl_vars['filename']->value=='bannergroupimage'||$_smarty_tpl->tpl_vars['filename']->value=='systememail'||$_smarty_tpl->tpl_vars['filename']->value=='staticpages'||$_smarty_tpl->tpl_vars['filename']->value=='banner'||$_smarty_tpl->tpl_vars['filename']->value=='faq'||$_smarty_tpl->tpl_vars['filename']->value=='help'||$_smarty_tpl->tpl_vars['filename']->value=='contactus'||$_smarty_tpl->tpl_vars['filename']->value=='homeslider'||$_smarty_tpl->tpl_vars['filename']->value=='faq'||$_smarty_tpl->tpl_vars['filename']->value=='faqcategory'||$_smarty_tpl->tpl_vars['filename']->value=='requestcatalog'){?>
			<ul class="navigation">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
configuration/loadconfiguration" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='configuration'){?>class="active_tab"<?php }?>>General Configration</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
systememail/systememaillist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='systememail'){?>class="active_tab"<?php }?>>System Emails</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
staticpages/staticpageslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='staticpages'){?>class="active_tab"<?php }?>>Static Pages</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
bannergroup/bannergrouplist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='bannergroup'){?>class="active_tab"<?php }?>>Banner Groups</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
banner/bannerlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='banner'){?>class="active_tab"<?php }?>>Banner</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homeslider/homesliderlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='homeslider'){?>class="active_tab"<?php }?>>Home Slider</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
customer_review/customerreviewlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='customerreview'){?>class="active_tab"<?php }?>>Customer Review</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
faqcategory/faqcategorylist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='faqcategory'){?>class="active_tab"<?php }?>>Faq Categories</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
faq/faqlist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='faq'){?>class="active_tab"<?php }?>>Faqs</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newslettermembers/newslettermemberslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='newslettermembers'){?>class="active_tab"<?php }?>>Newsletter Members</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist"<?php if ($_smarty_tpl->tpl_vars['filename']->value=='newsletter_format'){?>class="active_tab"<?php }?>>NewsLetter Formate</a></li>
				<!--<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
help/helplist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='help'){?>class="active_tab"<?php }?>>Help</a></li>-->
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homepage_blocks/homepage_blockslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='homepage_blocks'){?>class="active_tab"<?php }?>>Homepage Blocks</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
contactus/contactuslist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='contactus'){?>class="active_tab"<?php }?>>Contact Us</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
requestcatalog/requestcataloglist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='requestcatalog'){?>class="active_tab"<?php }?>>Request Catalog</a></li>
			</ul>
			<?php }else{ ?>
			<ul class="navigation inactivehide" style="display:none;">
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
configuration/loadconfiguration">General Configration</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
systememail/systememaillist">System Emails</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
staticpages/staticpageslist">Static Pages</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
bannergroup/bannergrouplist">Banner Groups</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
banner/bannerlist">Banner</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homeslider/homesliderlist">Home Slider</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
customer_review/customerreviewlist">Customer Review</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
faqcategory/faqcategorylist">Faq Categories</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
faq/faqlist">Faqs</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newslettermembers/newslettermemberslist">Newsletter Members</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist">NewsLetter Formate</a></li>
				<!--<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
help/helplist">Help</a></li>-->
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homepage_blocks/homepage_blockslist">Homepage Blocks</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
contactus/contactuslist">Contact Us</a></li>
				<li class="heading"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
requestcatalog/requestcataloglist" <?php if ($_smarty_tpl->tpl_vars['filename']->value=='requestcatalog'){?>class="active_tab"<?php }?>>Request Catalog</a></li>
			</ul>
			<?php }?>
		</li>
        
	</ul>
    
</div>
</div>
<?php }} ?>