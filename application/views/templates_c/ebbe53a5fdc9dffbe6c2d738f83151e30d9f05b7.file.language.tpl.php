<?php /* Smarty version Smarty-3.1.11, created on 2013-04-09 10:06:10
         compiled from "application/views/templates/admin/language/language.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9698903451639aba350069-05056935%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebbe53a5fdc9dffbe6c2d738f83151e30d9f05b7' => 
    array (
      0 => 'application/views/templates/admin/language/language.tpl',
      1 => 1362646327,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9698903451639aba350069-05056935',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51639aba46aea3_18820860',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51639aba46aea3_18820860')) {function content_51639aba46aea3_18820860($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
language/languagelist">Language</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add  Language<?php }else{ ?>Edit Language<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Language
			<?php }else{ ?>
			Edit Language
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iLanguageId" id="iLanguageId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iLanguageId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Language</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLanguage" name="Data[vLanguage]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vLanguage;?>
<?php }?>" lang="*" title="Language" style="width:250px"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Language Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLangCode" name="Data[vLangCode]" class="inputbox" maxlength="2" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vLangCode;?>
<?php }?>"  title="Language Code" style="width:250px"/>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="button" value="Add Language" class="submit_btn" title="Add Language" onclick="checkLanguageCode('<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
');"/>
						<?php }else{ ?>
							<input type="button" value="Edit Language" class="submit_btn" title="Edit Language" onclick="checkLanguageCode('<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
');"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
language/languagelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
	</script>
 
<?php }} ?>