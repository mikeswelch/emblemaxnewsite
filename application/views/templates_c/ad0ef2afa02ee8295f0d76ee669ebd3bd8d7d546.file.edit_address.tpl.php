<?php /* Smarty version Smarty-3.1.11, created on 2013-06-06 02:16:04
         compiled from "application/views/templates/edit_address.tpl" */ ?>
<?php /*%%SmartyHeaderCode:122324502851aeec7a82f990-69146266%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad0ef2afa02ee8295f0d76ee669ebd3bd8d7d546' => 
    array (
      0 => 'application/views/templates/edit_address.tpl',
      1 => 1370506179,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '122324502851aeec7a82f990-69146266',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51aeec7ab42802_59290020',
  'variables' => 
  array (
    'site_url' => 0,
    'UserDetail' => 0,
    'country' => 0,
    'operation' => 0,
    'state' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51aeec7ab42802_59290020')) {function content_51aeec7ab42802_59290020($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container col2-right-layout">
<div class="main-inner clearfix">
<div class="container">
<div class="row-fluid show-grid">
<div class="col-main-wap span9">
    
<div class="col-main">
<div class="col-main-inner">
<div class="my-account"><div class="page-title">
    <h1 style="float:left;">Edit Address</h1>
</div>
<!--<form action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address" method="post" id="form-validate" onsubmit="return checkAdd();">-->
<form action="#" method="post" id="form-validate" >
<input type="hidden" name="iUserId" id="iUserId" value="<?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->iUserId;?>
">
<input type="hidden" name="iAddressId" id="iAddressId" value="<?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->iUserAddressId;?>
"> 
    <div class="fieldset">
    <h2 class="legend">Contact Information</h2>
        <ul class="form-list row-fluid show-grid">
            <li class="fields">
                   <div class="customer-name">
    <div class="field name-firstname span6">
    <label for="firstname" class="required"><em>*</em>First Name</label>
    <div class="input-box">
        <input type="text" id="vFirstName" name="Data[vFirstName]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingFirstName!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingFirstName;?>
<?php }?>" title="First Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
     <div class="validation-advice" id="vFirstNameDiv" style="display:none; float: left;"></div>
</div>
<div class="field name-lastname span6">
    <label for="lastname" class="required"><em>*</em>Last Name</label>
    <div class="input-box">
        <input type="text" id="vLastName" name="Data[vLastName]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingLastName!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingLastName;?>
<?php }?>" title="Last Name" maxlength="255" class="input-text span12 required-entry"  />
    </div>
    <div class="validation-advice" id="vLastNameDiv" style="display:none; float: left;"></div>
</div>
</div>
            </li>
            <li class="fields">
                <div class="field span6">
                    <label for="telephone" class="required"><em>*</em>Telephone</label>
                    <div class="input-box">
                        <input type="text" name="Data[vTelephone]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingTelephone!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingTelephone;?>
 <?php }?>" title="Telephone" class="input-text   required-entry" id="vTelephone" />
                    </div>
		    <div class="validation-advice" id="vTelephoneDiv" style="display:none; float: left;"></div>
                </div>
                <div class="field span6">
                    <label for="fax">Fax</label>
                    <div class="input-box">
                        <input type="text" name="Data[vFax]" id="vFax" title="Fax" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingFax!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingFax;?>
<?php }?>" class="input-text " />
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="fieldset">
        <h2 class="legend">Address</h2>
        <ul class="form-list row-fluid show-grid">
                    <li class="wide">
                <label for="street_1" class="required"><em>*</em>Street Address</label>
                <div class="input-box">
                    <input type="text" name="Data[tStreetAddress1]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->tBillingStreetAddress1!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->tBillingStreetAddress1;?>
<?php }?>" title="Street Address" id="tStreetAddress1" class="span12 input-text  required-entry" />
                </div>
		<div class="validation-advice" id="tStreetAddress1Div" style="display:none; float: left;"></div>
            </li>
                            <li class="wide">
                <div class="input-box">
                    <input type="text" name="Data[tStreetAddress2]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->tBillingStreetAddress2!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->tBillingStreetAddress2;?>
<?php }else{ ?><?php }?>" title="Street Address 2" id="tStreetAddress2" class="span12 input-text " />
                </div>
            </li>
                                <li class="fields">
                <div class="field span6">
                    <label for="city" class="required"><em>*</em>City</label>
                    <div class="input-box">
                        <input type="text" name="Data[vCity]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingCity!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingCity;?>
<?php }else{ ?><?php }?>" title="vCity" id="vCity" class="span12 input-text " />
                    </div>
		    <div class="validation-advice" id="vCityDiv" style="display:none; float: left;"></div>
                </div>
            <div class="input-box" style="width:410px;">
                <label for="firstname" class="required"><em>*</em>Select Country</label>
    		<select name="" class="input-text span12 required-entry" id="iCountryId" name="Data[iCountryId]" onchange="getStates(this.value);" title="Country">
		 <option>--Select Country--</option>
		 <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    			<option value='<?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId;?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'){?><?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->iBillingCountryId==$_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vCountry;?>
</option>
    		 <?php endfor; endif; ?>
    		</select>
            </div>
	    <div class="validation-advice" id="iCountryIdDiv" style="display:none; float: left;"></div>
            </li>
            <li>
            <div class="field name-lastname span6">
                <label for="lastname" class="required"><em>*</em>Select State</label>
                <div class="input-box">
                    <select id="states" name="Data[iStateId]"  title="State" class="input-text span12 required-entry"  >
                     <option value="">--Select State--</option>
                         <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                             <option value='<?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'){?> <?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->iBillingStateId==$_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>

                         <?php endfor; endif; ?>
                             </option>
                    </select>
                </div>
		<div class="validation-advice" id="statesDiv" style="display:none; float: left;"></div>
            </div>       
            </li>
            <li class="fields">
                <div class="field span6">
                    <label for="zip" class="required"><em>*</em>Zip/Postal Code</label>
                    <div class="input-box">
                        <input type="text" name="Data[vZipCode]" value="<?php if ($_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingZipCode!=''){?><?php echo $_smarty_tpl->tpl_vars['UserDetail']->value[0]->vBillingZipCode;?>
<?php }?>" title="Zip/Postal Code" id="vZipCode" class="input-text validate-zip-international  required-entry" />
                    </div>
		    <div class="validation-advice" id="vZipCodeDiv" style="display:none; float: left;"></div>
                </div>
            <div class="field name-firstname span6">
            
            </li>
            <li>
                                    <input type="hidden" name="default_billing" value="1" />
                            </li>
            <li>
                                    <input type="hidden" name="default_shipping" value="1" />
                            </li>
<label style="float:left;">
Default Billing Address<br/>
Default Shipping Address
</label></br>
</ul>
    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/myDashboard"><small>&laquo; </small>Back</a></p>
        <button type='button' title="Save Address" class="button" ondblclick="return checkAdd();"><span><span>Save Address</span></span></button>
    </div>
</form>
</div>									</div>
								</div>
							</div>
 <div style="margin-top:98px;"><?php echo $_smarty_tpl->getSubTemplate ("right_myAccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
</div>

	<div class="block mt-smartmenu">
		<div class="title-divider">
			<span>&nbsp;</span>
                </div>

</div>
						</div>
					</div>
                </div>
				
				<div class="mt-productcroller-container clearfix"></div>
				<!--Call logo scroller-->
									<div class="mt-footer-static-container-logoscroller clearfix">
						<div class="mt-footer-static-logo container">
							<div class="mt-logoscroller">								
								
								<div class="list_logocarousel">
								</div>								
							</div>
						</div>
     
					</div>
								
            </div>
        </div>
                        <p id="back-top" class="hidden-phone">
				<a href="#top"><span title="Back to Top"></span></a>
			</p>
			        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
	var tooltip = $mtkb('#back-top span');
	$mtkb(tooltip).tooltip(true);
	$mtkb(".block-layered-nav .block-content p.block-subtitle").hide();
	$mtkb("#product-options-wrapper p.required").hide();	
	$mtkb('.toolbar.row-pager, .toolbar-bottom .sort-by, .toolbar-bottom .limiter, .toolbar-bottom .view-mode').hide();
	$mtkb('.toolbar-bottom .toolbar .row-pager').show();
	$mtkb('table#my-orders-table thead').addClass('hidden-phone');	
	
	$mtkb("table").removeClass('data-table');
	$mtkb("table").addClass('table table-bordered');	
	$mtkb("table#shopping-cart-totals-table").removeClass('table table-bordered');	
	$mtkb('#sitemap_top_links').addClass('row-fluid show-grid');
	$mtkb('#sitemap_top_links .links, .page-sitemap ul.links').addClass('clearfix');
	
	$mtkb("#review-form .form-list input.input-text,#review-form .form-list textarea").addClass('span4');
	$mtkb("#shipping-zip-form .form-list select, #billing-new-address-form select.validate-select, ul li.fields .input-box select.validate-select, ul li.fields div.field .input-box input.input-text, #wishlist-table textarea").addClass('span12');
	$mtkb(".buttons-set p.required").css("width","100%");
	$mtkb(".buttons-set p.back-link").addClass("pull-left");
	$mtkb(".buttons-set button.button").addClass("pull-right");
	$mtkb(".checkout-cart-index .col-main .cart-empty li.error-msg").addClass('span4 offset4');
	$mtkb("#multiship-addresses-table select").addClass('span4');	
	$mtkb("#multiship-addresses-table .qty").addClass('span2');	
	var msize = false;
	var msize1 = false;
	var checksize = function(){
        w = $mtkb(".container").width();  
        msize = (w > 730) ? false : true; 
        msize1 = (w >= 704) ? false : true; 
    }   
    
    $mtkb('.mtajaxcart').hide(); 
	$mtkb(".top-cart").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(300).slideDown(500, "easeOutCubic");
	}, function() {
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	});  
	$mtkb('.mt-search-form').hide(); 
	$mtkb(".top-search").hover(function() {
		if(!msize) {
			$mtkb(this).addClass('hover');
			$mtkb(".top-search .mt-search-form").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
		}
	}, function() {
		if(!msize) {
		$mtkb(".top-search .mt-search-form").stop(true, true).delay(100).slideUp(200, "easeInCubic");
		}
	}); 
	$mtkb('.mt-top-link').hide(); 
	$mtkb(".top-link").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
	}, function() {
		if(msize1){
			$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideDown(200, "easeInCubic");
		}
		else
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	}); 
    $mtkb(document).ready(function(){
        checksize();
	}); 
    $mtkb(window).resize(function(){
        checksize();
    });	
	
	$mtkb("#back-top").hide();  
</script>

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">    
function getStates(icountryid)
{
          var extra ='';
	  extra+='?icountryid='+icountryid;
	          
	  var url = site_url + 'registration/getstates';
	  var pars = extra;
	  $.post(url+pars,
          function(data) {
            if(data != '')
            {
                $('#states').html(data);
            }
            else{
            }
    
	});
}
</script>
<script type="text/javascript">
function checkAdd()
{
    var extra = '';
    var firstname =  $('#vFirstName').val();
    var lastname =  $('#vLastName').val();    
    var phone = $('#vTelephone').val();    
    var streetAddress1 = $('#tStreetAddress1').val();
    var streetAddress2 = $('#tStreetAddress2').val();
    var fax = $('#vFax').val();
    var city = $('#vCity').val();    
    var country = $('#iCountryId').val();    
    var state = $('#states').val();
    var zip = $('#vZipCode').val();   
    
    if( $('#vFirstName').val() == '' && $('#vLastName').val() ==''&& $('#vTelephone').val() ==''&& $('#tStreetAddress1').val() ==''&& $('#vCity').val() ==''&& $('#iCountryId').val() =='--Select Country--'&& $('#vZipCode').val() =='')
   {
			  $("#vFirstName").addClass("borderValidations");
			  $('#vFirstNameDiv').html('This is requried field');
			  $('#vFirstNameDiv').show();
			  
			  $("#vLastName").addClass("borderValidations");
			  $('#vLastNameDiv').html('This is requried field');
			  $('#vLastNameDiv').show();
			  
			  $("#vTelephone").addClass("borderValidations");
			  $('#vTelephoneDiv').html('This is requried field');
			  $('#vTelephoneDiv').show();
			  
			  $("#tStreetAddress1").addClass("borderValidations");
			  $('#tStreetAddress1Div').html('This is requried field');
			  $('#tStreetAddress1Div').show();
			  
			  $("#vCity").addClass("borderValidations");
			  $('#vCityDiv').html('This is requried field');
			  $('#vCityDiv').show();
			  
			  $("#iCountryId").addClass("borderValidations");
			  $('#iCountryIdDiv').html('This is requried field');
			  $('#iCountryIdDiv').show();
			  
			  $("#states").addClass("borderValidations");
			  $('#statesDiv').html('This is requried field');
			  $('#statesDiv').show();
			  
			  $("#vZipCode").addClass("borderValidations");
			  $('#vZipCodeDiv').html('This is requried field');
			  $('#vZipCodeDiv').show();
			//  return false;
			  
   }
   var validate = true;
    if( $('#vFirstName').val() == '')
    {
			  $("#vFirstName").addClass("borderValidations");
			  $('#vFirstNameDiv').html('This is requried field');
			  $('#vFirstNameDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vFirstNameDiv').hide();
			 $("#vFirstName").removeClass("borderValidations");
    }
    if( $('#vLastName').val() == '')
    {
			  $("#vLastName").addClass("borderValidations");
			  $('#vLastNameDiv').html('This is requried field');
			  $('#vLastNameDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vLastNameDiv').hide();
			 $("#vLastName").removeClass("borderValidations");
    }
    if( $('#vTelephone').val() == '')
    {
			  $("#vTelephone").addClass("borderValidations");
			  $('#vTelephoneDiv').html('This is requried field');
			  $('#vTelephoneDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vTelephoneDiv').hide();
			 $("#vTelephone").removeClass("borderValidations");
    }
    if( $('#tStreetAddress1').val() == '')
    {
			  $("#tStreetAddress1").addClass("borderValidations");
			  $('#tStreetAddress1Div').html('This is requried field');
			  $('#tStreetAddress1Div').show();
			  validate=false;
    }
    else
    {
			 $('#tStreetAddress1Div').hide();
			 $("#tStreetAddress1").removeClass("borderValidations");
    }
    if( $('#vCity').val() == '')
    {
			  $("#vCity").addClass("borderValidations");
			  $('#vCityDiv').html('This is requried field');
			  $('#vCityDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vCityDiv').hide();
			 $("#vCity").removeClass("borderValidations");
    }
    if( $('#iCountryId').val() == '--Select Country--')
    {
			  $("#iCountryId").addClass("borderValidations");
			  $('#iCountryIdDiv').html('This is requried field');
			  $('#iCountryIdDiv').show();
			  validate=false;
    }
    else
    {
			 $('#iCountryIdDiv').hide();
			 $("#iCountryId").removeClass("borderValidations");
    }
    if( $('#states').val() == '')
    {
			  $("#states").addClass("borderValidations");
			  $('#statesDiv').html('This is requried field');
			  $('#statesDiv').show();
			  validate=false;
    }
    else
    {
			 $('#statesDiv').hide();
			 $("#states").removeClass("borderValidations");
    }
    if( $('#vZipCode').val() == '')
    {
			  $("#vZipCode").addClass("borderValidations");
			  $('#vZipCodeDiv').html('This is requried field');
			  $('#vZipCodeDiv').show();
			  validate=false;
    }
    else
    {
			 $('#vZipCodeDiv').hide();
			 $("#vZipCode").removeClass("borderValidations");
    }
    if (validate)
    {
	  extra+='?vFirstName='+firstname;
	  extra+='&vLastName='+lastname;
          extra+='&tStreetAddress1='+streetAddress1;
          extra+='&tStreetAddress2='+streetAddress2;
	  extra+='&vTelePhone='+phone;
	  extra+='&vZipCode='+zip;
	  extra+='&vCity='+city;
          extra+='&vFax='+fax;
          extra+='&iCountryId='+country;
	  extra+='&iStateId='+state;
	  
	var url = site_url + 'myAccount/edit_address';
        var pars = extra;
         $.post(url+pars,
	      function(data) {
		if(data == 'updated'){
                    window.location = site_url+'myAccount/myDashboard';
		}
	      });
    }
    else
    {
	 return false;
    }
}

</script>
<?php }} ?>