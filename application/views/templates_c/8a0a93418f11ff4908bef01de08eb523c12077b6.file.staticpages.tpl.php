<?php /* Smarty version Smarty-3.1.11, created on 2014-01-07 14:23:24
         compiled from "application/views/templates/admin/staticpages/staticpages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:682688735515c1f80c7fe72-74467080%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a0a93418f11ff4908bef01de08eb523c12077b6' => 
    array (
      0 => 'application/views/templates/admin/staticpages/staticpages.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '682688735515c1f80c7fe72-74467080',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515c1f80dae148_83774128',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'action' => 0,
    'data' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
    'language_data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515c1f80dae148_83774128')) {function content_515c1f80dae148_83774128($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
    <div id="breadcrumb">
	<ul>
		<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
		<li>/</li>
		<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
staticpages/staticpageslist">Static Pages</a></li>
		<li>/</li>
		<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Static Page<?php }else{ ?>Edit Static Page<?php }?></li>
	</ul>
    </div>
    <div class="centerpartbg">
           <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
            <div class="pagetitle">Add Static Page</div>
            <?php }else{ ?>
            <div class="pagetitle">Edit Static Page</div>
            <?php }?>
            <div class="add_ad_contentbox">
		  	
                    <form id="frmadd" name="frmadd" method="post" action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
">
                            <input type="hidden" name="iSPageId" id="iSPageId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iSPageId;?>
<?php }?>" />
							<input type="hidden" name="action" id="action" value="add" />
			    
			   <!-- <div class="inputboxes">
				       	<label for="textfield" style="width:180px;"><span class="red_star">*</span> Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFile" readonly="readonly" name="Data[vFile]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vFile;?>
<?php }?>" lang="*" title="Name" style="width:252px"/>
			    </div>-->
			    <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vpagename" name="Data[vpagename]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vpagename;?>
<?php }?>"  lang="*"  title="Page Name" />
				</div>
			    
			    <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						<?php }?>
						<?php }else{ ?>
						<?php echo print_r($_smarty_tpl->tpl_vars['initOrder']->value);?>

						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['data']->value->iOrderNo==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						<?php }?>
						<?php }?>
					
					</select>
				</div>

			    <div id="content">
					    <div class="inputboxes worddocument_input">
			    		<label  for="textarea" style="width:180px;"><span class="red_star"></span> Content</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tContent_<?php echo $_smarty_tpl->tpl_vars['language_data']->value[0]->vLangCode;?>
" class="content_lang" name="Data[tContent]" class="inputbox" title="Content_en" value=""><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tContent;?>
<?php }?></textarea></div>
					
			    </div>

<script type="text/javascript">
var id = '<?php echo $_smarty_tpl->tpl_vars['language_data']->value[0]->vLangCode;?>
';	
	CKEDITOR.replace('tContent_'+id);	
	
</script>

			    </div>
			    
			    <div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
			    </div>       
                           <div class="clear"></div><br>
			    
			    <div class="add_can_btn">
				    <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
				    <input type="submit" value="Add Page" class="submit_btn" title="Add Page" onclick="return validate(document.frmadd);"/>
				    <?php }else{ ?>
				    <input type="submit" value="Edit Page" class="submit_btn" title="Edit Page" onclick="return validate(document.frmadd);"/>
				    <?php }?>
				    <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
staticpages/staticpageslist" class="cancel_btn">Cancel</a>
			    </div>
                    </form>
                    
            </div>
    </div>
    <div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>