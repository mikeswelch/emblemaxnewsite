<?php /* Smarty version Smarty-3.1.11, created on 2013-04-03 15:50:52
         compiled from "application/views/templates/admin/categorydesignimages/categorydesignimages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1680934109515c02845656f5-06320278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6c536134a1aeea790656a27907435c86dc4b4cb6' => 
    array (
      0 => 'application/views/templates/admin/categorydesignimages/categorydesignimages.tpl',
      1 => 1363617607,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1680934109515c02845656f5-06320278',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'action' => 0,
    'data' => 0,
    'parentarray' => 0,
    'pre_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515c028467ae45_84370411',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515c028467ae45_84370411')) {function content_515c028467ae45_84370411($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
categorydesignimages/categorydesignimageslist">Category Design Images</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Category Design Images<?php }else{ ?>Edit Category Design Images<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
		<div class="pagetitle">Add Category Design Images</div>
		<?php }else{ ?>
		<div class="pagetitle">Edit Category Design Images</div>
		<?php }?>
		<div class="add_ad_contentbox">
			
			<form id="frmadd" name="frmadd" method="post" action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
" enctype="multipart/form-data">
			<input type="hidden" name="Data[iCategoryDesignImageId]" id="iCategoryDesignImageId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iCategoryDesignImageId;?>
<?php }?>" />
			<input type="hidden" name="action" id="action" value="add" />
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>	
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star">*</span>Category Design Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iCategoryParentImageId]" onchange="checkparent(this.value)">
						
						<option value=''>--Select Parent Category--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['parentarray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryDesignImageId'];?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['parentarray']->value->iCategoryParentImageId==$_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCategoryDesignImageId){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'&&$_smarty_tpl->tpl_vars['pre_data']->value[0]->iCategoryParentImageId==0){?>		
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star">*</span>Category Design Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iCategoryParentImageId]" onchange="checkparent(this.value)">
					
						<option value=''>--Select Category Design Name--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['parentarray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryDesignImageId'];?>
' ><?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<?php }?>
				
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'&&$_smarty_tpl->tpl_vars['pre_data']->value[0]->iCategoryParentImageId!='0'){?>		
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star">*</span>Category Design Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iCategoryParentImageId]" onchange="checkparent(this.value)">
					
						<option value=''>--Select Category Design Name--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['parentarray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryDesignImageId'];?>
' <?php if ($_smarty_tpl->tpl_vars['pre_data']->value[0]->iCategoryParentImageId==$_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryDesignImageId']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<?php }?>
				<div class="inputboxes" >
					<label for="textfield" id="menuboxid" style="display:block;"><span class="red_star">*</span>Parent Category Name</label>
					<label for="textfield" id="submenuboxid" style="display:none;"><span class="red_star">*</span>Sub-Category Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" lang="*" title="Title" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" maxlength="30" class="inputbox" name="Data[vTitle]" >
				</div>
											
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="submit" value="Add Category Images" class="submit_btn" title="Add Category Images" onclick="return validate(document.frmadd);"/>
					<?php }else{ ?>
					<input type="submit" value="Edit Category Images" class="submit_btn" title="Edit Category Images" onclick="return validate(document.frmadd);"/>
					<?php }?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
categorydesignimages/categorydesignimageslist" class="cancel_btn">Cancel </a>
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script type="text/javascript">

function checkparent(id){

	if(id == '0'){
		$('#menuboxid').css({display: "block"});
		$('#submenuboxid').css({display: "none"});
		
		$('#iconbox').css({display: "block"});
		$('#urlbox').css({display: "none"});
	}else{
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
var operation = '<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
';

if( operation == 'edit'){
	var iParentMenuId = '<?php echo $_smarty_tpl->tpl_vars['data']->value->iCategoryParentImageId;?>
';
	if( iParentMenuId != 0){
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
</script>

<?php }} ?>