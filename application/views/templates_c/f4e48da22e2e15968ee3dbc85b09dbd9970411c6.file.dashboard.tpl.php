<?php /* Smarty version Smarty-3.1.11, created on 2013-12-12 10:39:12
         compiled from "application/views/templates/admin/dashboard/dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1476379706514d35242f5ab6-92047243%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f4e48da22e2e15968ee3dbc85b09dbd9970411c6' => 
    array (
      0 => 'application/views/templates/admin/dashboard/dashboard.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1476379706514d35242f5ab6-92047243',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514d35244367a0_63025581',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'tot_administator' => 0,
    'users' => 0,
    'products' => 0,
    'category_types' => 0,
    'faq' => 0,
    'trasaction' => 0,
    'contact_us' => 0,
    'help' => 0,
    'lastlogin' => 0,
    'admin_js_path' => 0,
    'arr_week' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514d35244367a0_63025581')) {function content_514d35244367a0_63025581($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li class="current">Dashboard</li>
		</ul>
	</div>
	<!--pie chart-->
	<div class="centerpartbg">
		<div class="quick_link">
			<ul>
				<!--
				<li> <a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_lrg_calendar.png" alt="Calendar" /><br />
					Calendar</a> </li>
				-->
				<li> <a href="administrator/adminlist"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
admin.png" alt="Admin" /><br />
					Admin</a> </li>
				<!--<li> <a href="user/userlist"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon8.png" alt="Users" /><br />
					Users</a> </li>-->
				<li> <a href="user/userlist"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon1.png" alt="Users" /><br />
					Manage<br />
					Users</a> </li>				
				<!--<li> <a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon3.png" alt="Users" /><br />
					Manage<br />
					Analysis</a> </li>-->				
				<li> <a href="configuration/loadconfiguration"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon5.png" alt="Users" /><br />
					General<br />
					Settings</a> </li>
				<!--<li> <a href="brief/brieflist"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon6.png" alt="Messages" /><br />
					Messages </a> </li>-->
				<li> <a href="contactus/contactuslist"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon7.png" alt="Contact Us" /><br />
					Contact us</a> </li>
				<li> <a href="all_transaction"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
trans-icon.png" alt="Transaction" /><br />
					Transaction</a> </li>
				<!--<li> <a href="event/eventlist"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
event.png" alt="Events" /><br />
					Events</a> </li> -->
			</ul>
		</div>
		<div id="dashbord_admin">
			<div id="content_admin">
				<!-- Charts box start -->
				<div class="left_welcome_text">
					<div class="chart_box">
						<div class="conthead">
							<h2>Revenue of Year 2012</h2>
						</div>
						<div class="table" align="center">
							<div id="chart4Div"></div>
						</div>
					</div>
					<div class="welcome_text">
						<div class="conthead">
							<h2>Pai Chart</h2>
						</div>
						<!-- Tabbed navigation start -->
						<div class="contentbox">
							<div id="container" class="paichart"></div>
						</div>
					</div>
				</div>
				<!-- Website stats start -->
				<div class=" sml left">
					<div class="container_admin">
						<div class="conthead">
							<h2>Statstics</h2>
						</div>
						<div class="contentbox">
							<ul class="summarystats">
								<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['tot_administator']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['tot_administator']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Administator</p>
									<p class="statview"><a href="administrator/adminlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['users']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['users']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Users</p>
									<p class="statview"><a href="user/userlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['products']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['products']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Products</p>
									<p class="statview"><a href="product/productlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['category_types']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['category_types']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Category</p>
									<p class="statview"><a href="category/categorylist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['faq']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['faq']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Faq</p>
									<p class="statview"><a href="faq/faqlist" title="View">View</a></p>
								</li>
								<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['trasaction']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['trasaction']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Transaction</p>
									<p class="statview"><a href="all_transaction" title="View">View</a></p>
								</li>
								<li class="last">
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['contact_us']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['contact_us']->value;?>
<?php }else{ ?>0<?php }?> </p>
									<p>Contact Us</p>
									<p class="statview"><a href="contactus/contactuslist" title="View">View</a></p>
								</li>
								<!--<li>
									<p class="statcount"><?php if ($_smarty_tpl->tpl_vars['help']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['help']->value;?>
<?php }else{ ?>0<?php }?></p>
									<p>Help</p>
									<p class="statview"><a href="help/helplist" title="View">View</a></p>
								</li>-->
								
							</ul>
						</div>
					</div>
					<div class="container_admin">
						<div class="conthead">
							<h2>Last Login Information</h2>
						</div>
						<div class="contentbox_login">
							<div class="login_his_table">
								<table width="80%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td width="30%">Last Login</td>
											<td width="5%">:</td>
											<td><?php echo $_smarty_tpl->tpl_vars['lastlogin']->value->dLoginDate;?>
</td>
										</tr>
										<tr>
											<td>From IP</td>
											<td width="5%">:</td>
											<td><?php echo $_smarty_tpl->tpl_vars['lastlogin']->value->vFromIP;?>
</td>
										</tr>
										<tr>
											<td>User Name</td>
											<td width="5%">:</td>
											<td><?php echo $_smarty_tpl->tpl_vars['lastlogin']->value->vUserName;?>
</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- Website stats end -->
				<!-- Clear finsih for all floated content boxes -->
				<div style="clear: both;"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">
	var chart2 = new FusionCharts("<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
FCF_StackedColumn2D.swf", "ChId1", "580", "300");
	chart2.setDataURL("<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
ProductSales.xml");
	chart2.render("chart4Div");
</script>
<script type="text/javascript">
	
	var data_week = '<?php echo $_smarty_tpl->tpl_vars['arr_week']->value;?>
';
	data_week = eval(data_week);
	
	var data_y = new Array();
	for(var i=0; i<7; i++)
	{
	    data_y[i] = parseInt(data_week[1][i], 10);
	} 
	
	$(document).ready(function () {    
	
        RenderPieChart('container', [
		[data_week[0][0],data_week[1][0]],
		[data_week[0][1],data_week[1][1]],
		[data_week[0][2],data_week[1][2]],                         
		[data_week[0][3],data_week[1][3]],
		[data_week[0][4],data_week[1][4]],
		[data_week[0][5],data_week[1][5]],
		[data_week[0][6],data_week[1][6]]
        ]);     

	/*$('#btnPieChart').live('click', function(){
		var data = [
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]],
			{
			    name: data_week[0][6],
			    y: data_week[0][6],
			    sliced: true,
			    selected: true
			},
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]],
			[data_week[0][6], data_week[0][6]]
		];
	
	       RenderPieChart('container', data);
	});*/

	function RenderPieChart(elementId, dataList) {
		new Highcharts.Chart({
		chart: {
			renderTo: elementId,
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
			}, title: {
			    text: ''
			},
		tooltip: {
		    formatter: function () {
			return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
		    }
		},
		plotOptions: {
		    pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
			    enabled: true,
			    color: '#000000',
			    connectorColor: '#000000',
			    formatter: function () {
				return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
			    }
			}
		    }
		},
		series: [{
		    type: 'pie',
		    name: 'Browser share',
		    data: dataList
		}]
	    });
	};
});
</script>

<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
 <?php }} ?>