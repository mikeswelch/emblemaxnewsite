<?php /* Smarty version Smarty-3.1.11, created on 2013-04-03 17:33:51
         compiled from "application/views/templates/admin/garment/garment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:881412883515c1aa7dbec76-75443173%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '658a9933aa1b8ddd91419a4520df7030e9fd860b' => 
    array (
      0 => 'application/views/templates/admin/garment/garment.tpl',
      1 => 1363082888,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '881412883515c1aa7dbec76-75443173',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515c1aa7e8bc10_03911631',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515c1aa7e8bc10_03911631')) {function content_515c1aa7e8bc10_03911631($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
garment/garmentlist">Garment</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Garment Type<?php }else{ ?>Edit Garment Type<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Garment Type
			<?php }else{ ?>
			<ul>
				<li class="active"><a href="#">Edit Garment Type</a></li>
				
			</ul>
			
			
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iGarmentTypeId" id="iGarmentTypeId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iGarmentTypeId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Garment Type</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vGarmentType" name="Data[vGarmentType]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vGarmentType;?>
<?php }?>" lang="*" title="Garment Type" />
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Garment Type" class="submit_btn" title="Add Garment Type" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Garment Type" class="submit_btn" title="Edit Garment Type" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
garment/garmentlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



 
<?php }} ?>