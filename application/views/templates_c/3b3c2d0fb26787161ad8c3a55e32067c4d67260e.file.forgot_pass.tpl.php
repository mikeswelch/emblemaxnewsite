<?php /* Smarty version Smarty-3.1.11, created on 2013-09-03 07:04:36
         compiled from "application/views/templates/forgot_pass.tpl" */ ?>
<?php /*%%SmartyHeaderCode:186529003851adab04ef5bd3-42474252%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b3c2d0fb26787161ad8c3a55e32067c4d67260e' => 
    array (
      0 => 'application/views/templates/forgot_pass.tpl',
      1 => 1377257053,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '186529003851adab04ef5bd3-42474252',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51adab0503c898_56246830',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51adab0503c898_56246830')) {function content_51adab0503c898_56246830($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container col1-layout">
						            <div class="main container">
			<div class="slideshow_static">
                                            </div>
                <div class="main-inner row-fluid">
                    <div class="span12">
												<div class="col-main">
							<div class="col-main-inner">
																<div class="page-title">
    <h1>Forgot Your Password?</h1>
</div>
<form action="#" method="post" id="form-validate">
    <div class="fieldset">
        <h2 class="legend">Retrieve your password here</h2>
        <p>Please enter your email address below. You will receive a link to reset your password.</p>
        <ul class="form-list row-fluid show-grid">
            <li>
                <label for="email_address" class="required"><em>*</em>Email Address</label>
                <div class="input-box">
                    <input type="text" id="vEmail" name="Data[vEmail]" alt="email" id="email_address" class="input-text required-entry validate-email span3" value="" />
                </div>
		<div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
            </li>
                    </ul>
    </div>
    <div class="buttons-set">
        <p class="required"><em>*</em> Required Fields</p>
        <p class="back-link"><a href="#"><small>&laquo; </small>Back to Login</a></p>
        <button type="button" title="Submit" class="button" onclick="return CheckUser();"><span><span>Submit</span></span></button>
    </div>
</form>
<script type="text/javascript">
//<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
//]]>
</script>
							</div>
						</div>
					</div>
                </div>
							    </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
function CheckUser()
{
	var email = $('#vEmail').val();
	var validate = true;
	var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	var isvalid = emailRegexStr.test(email);
    
	if( $('#vEmail').val() == ''){
	$("#vEmail").addClass("borderValidations");
	    $("#emailDiv").html('This is a required field.');
	    $("#emailDiv").show();
	    validate = false;
	}
      else if (!isvalid) {
	    $("#vEmail").addClass("borderValidations");
	    $("#emailDiv").html('Enter Your Email Address like demo@domain.com');
	    $("#emailDiv").show();
	    validate = false;
	}
       else{
	  $("#emailDiv").hide();
	  $("#vEmail").removeClass("borderValidations");
	  }
       if (validate)
	{
	    var extra = '';
	    extra+='?vEmail='+email;
	    extra+='&mode=forgotpassword';     
	    var url = site_url + 'authentication/check_login';
	    var pars = extra;
	    $.post(url+pars,
	    function(data) {
		if(data == 'invalid'){
			window.location = site_url+'login';
		}
		if(data == 'success'){
			window.location = site_url+'login';
		}
	});
		return true;
	}
	else
	{
		return false;
	} 
}
</script>
<?php }} ?>