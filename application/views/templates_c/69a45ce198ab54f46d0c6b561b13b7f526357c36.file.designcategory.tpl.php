<?php /* Smarty version Smarty-3.1.11, created on 2013-06-29 07:37:58
         compiled from "application/views/templates/admin/designcategory/designcategory.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1280329854516272aa834a51-34428262%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '69a45ce198ab54f46d0c6b561b13b7f526357c36' => 
    array (
      0 => 'application/views/templates/admin/designcategory/designcategory.tpl',
      1 => 1372505915,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1280329854516272aa834a51-34428262',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_516272aa988d27_39389413',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'Data' => 0,
    'lang' => 0,
    'parentarray' => 0,
    'data' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_516272aa988d27_39389413')) {function content_516272aa988d27_39389413($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
designcategory/designcategorylist"> Design Category</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Design Category<?php }else{ ?>Edit Design Category<?php }?></li>
		</ul>
	</div>
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg" style="width:100%;float:left;"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
		<div class="pagetitle">Add Design Category</div>
		<?php }else{ ?>
		<div class="pagetitle">Edit Design Category</div>
		<?php }?>
		
		<div class="add_ad_contentbox">
			<input type="hidden" name="Data[iDesignCategoryId]" id="iDesignCategoryId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->iDesignCategoryId;?>
<?php }?>" />
			<input type="hidden" name="action" id="action" value="add" />
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="en" />
			<?php }else{ ?>
			<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" />
			<?php }?>
			
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>	
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star"></span>Category Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iDesignParentCategoryId]" onchange="checkparent(this.value)">
	
						<option value=''>--Select Category Name--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['parentarray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDesignCategoryId'];?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['Data']->value->iDesignParentCategoryId==$_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDesignCategoryId']){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?>
				<div class="inputboxes" id="">
					
					<label for="textfield"><span class="red_star">*</span>Category Name</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iDesignParentCategoryId]" onchange="checkparent(this.value)">
	
						<option value=''>--Select Category Design Name--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['parentarray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDesignCategoryId'];?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['Data']->value->iDesignParentCategoryId==$_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iDesignCategoryId']){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['parentarray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<?php }?>
				
				<div class="inputboxes" >
					<label for="textfield" id="menuboxid" style="display:block;"><span class="red_star">*</span>Parent Category Name</label>
					<label for="textfield" id="submenuboxid" style="display:none;"><span class="red_star">*</span>Sub-Category Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" lang="*" title="Parent Design Category" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" maxlength="30" class="inputbox" name="data[vTitle]" >
				</div>
				
					<div class="inputboxes">
				        <label for="textarea"><span class="red_star">*</span></span> Order</label>
						<span class="collan_dot">:</span>
						<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
						<?php }?>
						<?php }else{ ?>
						<?php echo print_r($_smarty_tpl->tpl_vars['initOrder']->value);?>

						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>	
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['Data']->value->iOrderNo==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						<?php }?>
						<?php }?>
					</select>
				        
				</div>

				<div class="inputboxes" id="">
				<label for="textfield">Description</label>
				<span class="collan_dot">:</span>
				<textarea id="tDescription" class="inputbox" name="data[tDescription]" title="tDescription" value=""><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tDescription;?>
<?php }?></textarea>
				</div>					
			
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				</div>
				<div class="add_can_btn"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="submit" value="Add Design Category" class="submit_btn" title="Add Design Category" onclick="return validate(document.frmadd);"/>
					<?php }else{ ?>
					<input type="submit" value="Edit Design Category" class="submit_btn" title="Edit Design Category" onclick="return validate(document.frmadd);"/>
					<?php }?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
designcategory/designcategorylist" class="cancel_btn">Cancel </a>
				</div>
				<div class="clear"></div>
		</div>
	</div>

	<div class="clear"></div>
	</form>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script type="text/javascript">

function checkparent(id){

	if(id == '0'){
		$('#menuboxid').css({display: "block"});
		$('#submenuboxid').css({display: "none"});
		
		$('#iconbox').css({display: "block"});
		$('#urlbox').css({display: "none"});
	}else{
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
var operation = '<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
';

if( operation == 'edit'){
	var iParentMenuId = '<?php echo $_smarty_tpl->tpl_vars['data']->value->iDesignParentCategoryId;?>
';
	if( iParentMenuId != 0){
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
</script>

<?php }} ?>