<?php /* Smarty version Smarty-3.1.11, created on 2013-11-13 04:26:29
         compiled from "application/views/templates/admin/size/size.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1850755317515c02918a1da3-37270605%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91fa04651b7b9ecc0ce8d0814b65746fd2e9d5d7' => 
    array (
      0 => 'application/views/templates/admin/size/size.tpl',
      1 => 1377257117,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1850755317515c02918a1da3-37270605',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515c0291940710_18393288',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'Order' => 0,
    'ord' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515c0291940710_18393288')) {function content_515c0291940710_18393288($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
size/sizelist">Size</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Size<?php }else{ ?>Edit Size<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Size
			<?php }else{ ?>
                        Edit Size
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iSizeId" id="iSizeId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iSizeId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Size</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vSize" name="Data[vSize]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vSize;?>
<?php }?>" lang="*" title="Size" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Size Title" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrder" name="Data[iOrder]"  title="Order" lang="*" style="width:250px" >
						<option value=''>-- Select Order --</option>
						<?php $_smarty_tpl->tpl_vars['ord'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['ord']->step = 1;$_smarty_tpl->tpl_vars['ord']->total = (int)ceil(($_smarty_tpl->tpl_vars['ord']->step > 0 ? $_smarty_tpl->tpl_vars['Order']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['Order']->value)+1)/abs($_smarty_tpl->tpl_vars['ord']->step));
if ($_smarty_tpl->tpl_vars['ord']->total > 0){
for ($_smarty_tpl->tpl_vars['ord']->value = 1, $_smarty_tpl->tpl_vars['ord']->iteration = 1;$_smarty_tpl->tpl_vars['ord']->iteration <= $_smarty_tpl->tpl_vars['ord']->total;$_smarty_tpl->tpl_vars['ord']->value += $_smarty_tpl->tpl_vars['ord']->step, $_smarty_tpl->tpl_vars['ord']->iteration++){
$_smarty_tpl->tpl_vars['ord']->first = $_smarty_tpl->tpl_vars['ord']->iteration == 1;$_smarty_tpl->tpl_vars['ord']->last = $_smarty_tpl->tpl_vars['ord']->iteration == $_smarty_tpl->tpl_vars['ord']->total;?>
							<option value='<?php echo $_smarty_tpl->tpl_vars['ord']->value;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['ord']->value==$_smarty_tpl->tpl_vars['data']->value->iOrder){?>selected<?php }?><?php }?> ><?php echo $_smarty_tpl->tpl_vars['ord']->value;?>
</option>
						<?php }} ?>

					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Size" class="submit_btn" title="Add Size" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Size" class="submit_btn" title="Edit Size" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
size/sizelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



 
<?php }} ?>