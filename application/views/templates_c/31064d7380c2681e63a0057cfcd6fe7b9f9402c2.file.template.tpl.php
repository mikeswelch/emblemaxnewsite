<?php /* Smarty version Smarty-3.1.11, created on 2013-12-12 10:31:24
         compiled from "application/views/templates/admin/authentication/template.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1116538537514d820f643810-82462403%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '31064d7380c2681e63a0057cfcd6fe7b9f9402c2' => 
    array (
      0 => 'application/views/templates/admin/authentication/template.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1116538537514d820f643810-82462403',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514d820f6af0e7_63568832',
  'variables' => 
  array (
    'Name' => 0,
    'var_msg' => 0,
    'admin_url' => 0,
    'admin_image_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514d820f6af0e7_63568832')) {function content_514d820f6af0e7_63568832($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/authentication/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>



<div class="frient_admin_page">
	<?php if ($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
    <div class="error_msg">
        <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>

    </div>
<?php }?>
<div id="showloginid"><form name="frmlogin" id="frmlogin" action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
authentication/check_login" method="post"  >
<input type="hidden" name="data[mode]" value="authenticate">
    
    <div class="userbox_input">
		<img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_username.png"/>
		<label>Username : </label>
		<input type="text"  name="vUserName" id="vUserName" title="User Name"/>
	</div>
	<div class="userbox_input">
		<img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_locked.png"/>
		<label>Password : </label>
		<input type="password"  id="vPassword" name="vPassword" title="Password"/>
	</div>
	<div class="submit_btn">
        <input type="submit"  value="Submit"  title="Submit" onclick="return CheckLogin();"/>
	</div>
	<div class="frient_forgotten">
		<a href="#" onclick="showforgot();">Forgotten Password?</a>
	</div>
</form>    
</div>

<div id="forgotpasswordid"  style="display:none;">
<form name="frmlogin" id="frmlogin" action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
authentication/check_login" method="post"  >
<input type="hidden" name="data[mode]" value="forgotpassword">
   
    <div class="userbox_input">
		<img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_username.png"/>
		<label>Email ID : </label>
		<input type="text"  name="vEmail" id="vEmail" title="User Email"/>
	</div>
	<div class="submit_btn">
        <input type="submit"  value="Submit"  title="Submit" onclick="return CheckLogin();"/>
	</div>
	<div class="frient_forgotten">
		<a href="#" onclick="showlogin();">Back to Login?</a>
	</div>
</form>
</div>
</div>

<script type="text/javascript">
function CheckLogin()
{
	
    
    if (document.frmlogin.vUserName.value == "")
	{
		alert("Enter your Username");
		document.frmlogin.vUserName.focus();
		return false;
	}
	if (document.frmlogin.vPassword.value == "")
	{
		alert("Enter your password");
		document.frmlogin.vPassword.focus();
		return false;
	}
	document.frmlogin.submit()
}

</script>
<script>
function showforgot(){
    jQuery("#showloginid").hide("slow");
    jQuery("#forgotpasswordid").show("slow");
}
function showlogin(){
    jQuery("#forgotpasswordid").hide("slow");
    jQuery("#showloginid").show("slow");
}  
</script>



<?php echo $_smarty_tpl->getSubTemplate ("admin/authentication/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>