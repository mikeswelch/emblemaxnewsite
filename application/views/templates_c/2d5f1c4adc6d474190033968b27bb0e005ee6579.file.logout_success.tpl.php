<?php /* Smarty version Smarty-3.1.11, created on 2013-09-03 07:18:32
         compiled from "application/views/templates/logout_success.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8396034751b1663492e833-95725710%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2d5f1c4adc6d474190033968b27bb0e005ee6579' => 
    array (
      0 => 'application/views/templates/logout_success.tpl',
      1 => 1377257040,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8396034751b1663492e833-95725710',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51b166349d9487_61048521',
  'variables' => 
  array (
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51b166349d9487_61048521')) {function content_51b166349d9487_61048521($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

  
<div class="main-container col2-right-layout">
<form action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
registration/check_register" method="post" id="form-validate" onsubmit="return CheckRegister();">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="main container">
 	    <div class="slideshow_static"></div>
                <div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>			
                   <div class="main-inner">    
                    <div class="col-main">
                       <div class="account-create">
    			<h2 class="pagetital">You are now logged out</h2>
			<div style="clear:both;">
			    <p>You have logged out and will be redirected to our homepage in <span id="countdown">5</span> seconds.</p>
			</div>
		       </div>		
                    </div>					
                  </div>
 	        </div>	
 	    </div>
	</div>		 		 
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script language="javascript">

var time_left = 5;
var cinterval;
function time_dec(){
  time_left--;
  document.getElementById('countdown').innerHTML = time_left;
  if(time_left == 0){
    window.location = site_url+'home';
    clearInterval(cinterval);
  }
}
cinterval = setInterval('time_dec()', 1000);
</script>
<?php }} ?>