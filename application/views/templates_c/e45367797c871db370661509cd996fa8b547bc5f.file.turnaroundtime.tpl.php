<?php /* Smarty version Smarty-3.1.11, created on 2013-07-10 08:03:13
         compiled from "application/views/templates/admin/turnaroundtime/turnaroundtime.tpl" */ ?>
<?php /*%%SmartyHeaderCode:85555297651dc230b3cb656-08596381%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e45367797c871db370661509cd996fa8b547bc5f' => 
    array (
      0 => 'application/views/templates/admin/turnaroundtime/turnaroundtime.tpl',
      1 => 1373464793,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '85555297651dc230b3cb656-08596381',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51dc230b5b0293_50731100',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'Order' => 0,
    'ord' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51dc230b5b0293_50731100')) {function content_51dc230b5b0293_50731100($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
turnaroundtime/turnaroundtimelist">Turn Aound Time</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Turn Aound Time<?php }else{ ?>Edit Turn Aound Time<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Tun Around Time
			<?php }else{ ?>
			Edit Tun Around Time		
			<?php }?>
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iTurnAroundTimeId" id="iTurnAroundTimeId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iTurnAroundTimeId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Title" />
				</div>
		
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Note</label>
					<span class="collan_dot">:</span>
					<textarea id="tNote" class="inputbox" name="Data[tNote]" title="Note" value=""><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tNote;?>
<?php }?></textarea>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>Charge</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fCharge" name="Data[fCharge]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->fCharge;?>
<?php }?>" lang="*" title="Charge" onkeypress="return checkprise(event)"/>
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Charge Fee</label>
					<span class="collan_dot">:</span>
					<select id="eType" name="Data[eFeeCharge]" lang="*" title="Charge Fee">
						<option value=''>-- Select Discount Type--</option>
						<option value="%" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eFeeCharge=='%'){?>selected<?php }?><?php }?>>&#37;</option>
						<option value="$" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eFeeCharge=='$'){?>selected<?php }?><?php }?> >&#36;</option>
					</select>
				 </div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrder" name="Data[iOrder]"  title="Order" lang="*" style="width:250px" >
						<option value=''>-- Select Order --</option>
						<?php $_smarty_tpl->tpl_vars['ord'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['ord']->step = 1;$_smarty_tpl->tpl_vars['ord']->total = (int)ceil(($_smarty_tpl->tpl_vars['ord']->step > 0 ? $_smarty_tpl->tpl_vars['Order']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['Order']->value)+1)/abs($_smarty_tpl->tpl_vars['ord']->step));
if ($_smarty_tpl->tpl_vars['ord']->total > 0){
for ($_smarty_tpl->tpl_vars['ord']->value = 1, $_smarty_tpl->tpl_vars['ord']->iteration = 1;$_smarty_tpl->tpl_vars['ord']->iteration <= $_smarty_tpl->tpl_vars['ord']->total;$_smarty_tpl->tpl_vars['ord']->value += $_smarty_tpl->tpl_vars['ord']->step, $_smarty_tpl->tpl_vars['ord']->iteration++){
$_smarty_tpl->tpl_vars['ord']->first = $_smarty_tpl->tpl_vars['ord']->iteration == 1;$_smarty_tpl->tpl_vars['ord']->last = $_smarty_tpl->tpl_vars['ord']->iteration == $_smarty_tpl->tpl_vars['ord']->total;?>
							<option value='<?php echo $_smarty_tpl->tpl_vars['ord']->value;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['ord']->value==$_smarty_tpl->tpl_vars['data']->value->iOrder){?>selected<?php }?><?php }?> ><?php echo $_smarty_tpl->tpl_vars['ord']->value;?>
</option>
						<?php }} ?>

					</select>
				</div>				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='InActive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Turn Around Time" class="submit_btn" title="Add Turn Around Time" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Turn Around Time" class="submit_btn" title="Edit Turn Around Time" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
turnaroundtime/turnaroundtimelist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script type="text/javascript">

   function ImageDelete(id,file1){
	
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
color/deleteimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iColorId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
 
<?php }} ?>