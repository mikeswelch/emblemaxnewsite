<?php /* Smarty version Smarty-3.1.11, created on 2014-03-14 04:54:48
         compiled from "application/views/templates/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9784773715183dcba471e70-80387818%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '624aee4ddd80f6a0390166f17f94c29ba4eb3119' => 
    array (
      0 => 'application/views/templates/home.tpl',
      1 => 1394778773,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9784773715183dcba471e70-80387818',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5183dcba5e5b21_47995610',
  'variables' => 
  array (
    'msg' => 0,
    'getHomeBlocks' => 0,
    'upload_path' => 0,
    'front_image_path' => 0,
    'isPromotional' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5183dcba5e5b21_47995610')) {function content_5183dcba5e5b21_47995610($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="container">
    <div class="main">
       <div class="main-inner clearfix">
          <div class="container">
			<div class="row-fluid show-grid">
				<div class="col-main span9">
					<div class="col-main-inner">
						<?php if ($_smarty_tpl->tpl_vars['msg']->value){?>
						<ul class="messages" style="margin-top: 15px;">
						    <li class="success-msg">
						      <ul>
						         <li>
						         <span><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</span>
						         </li>
						      </ul>
						    </li>
						</ul>
						<?php }?>
						  <div class="mt-product-list">
							 <div id="options" class="row-fluid show-grid clearfix">
							 <div class="fillter clearfix"> </div>
							 </div>
							 <div class="conleftpart">
								<div class="toptenpart"></div>
								<div class="servicespart">
								    <?php if (count($_smarty_tpl->tpl_vars['getHomeBlocks']->value)>'0'){?>
								    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['getHomeBlocks']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								    <div class="sevicesbox">
									   <div class="sevicon"><a href="<?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLinkUrl;?>
" title="<?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLinkUrl;?>
" target="_blank"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/homepage_blocks/<?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iBlockslistId;?>
/<?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vImage;?>
" alt="" title="<?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vTitle;?>
"/></a></div>
									   <div class="boxarrow"><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
box-arrow.png" alt="" /></div>
									   <div class="textboxsev">
										  <h2><?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vTitle;?>
</h2>
										  <p><?php echo $_smarty_tpl->tpl_vars['getHomeBlocks']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription;?>
</p>
									   </div>
								    </div>
								    <?php endfor; endif; ?>
								    <?php }?>
								</div>
								<div class="prodpart">
								    <?php if (count($_smarty_tpl->tpl_vars['isPromotional']->value)>0){?>
									   <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['isPromotional']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
									   	<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
productdetail/<?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
">
										  	<div class="prodone">
										  		<div style="height:198px;"> 
										  		<img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
/193X198_<?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vImage;?>
" alt="" title="<?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vProductName;?>
"/>
										  		 </div>
										<div class="img_box"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
productdetail/<?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
"><?php if ($_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vBannerText!=''){?><?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vBannerText;?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['isPromotional']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vProductName;?>
<?php }?></a></div> </div>

										</a>
									   <?php endfor; endif; ?>
								    <?php }?>
								</div>
							 </div>
							 <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--> 
						  </div>
							</div>
							</div>
							<div class="col-right sidebar span3 visible-desktop">
							<?php echo $_smarty_tpl->getSubTemplate ("right.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

							</div>
						</div>
					</div>
				</div>
				<!--Call logo scroller-->
			</div>
		</div>
		<div class="botline">
		    <div class="container">
			    <div class="linebg">
			    &nbsp;
			    </div>
		    </div>
	    </div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>