<?php /* Smarty version Smarty-3.1.11, created on 2013-12-02 19:51:31
         compiled from "application/views/templates/right.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18015747585183dcba80f279-12701956%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '84de055b27e73af22aa1da9807ca01da9e7d6775' => 
    array (
      0 => 'application/views/templates/right.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18015747585183dcba80f279-12701956',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5183dcba819df6_41655439',
  'variables' => 
  array (
    'front_js_path' => 0,
    'review' => 0,
    'db_country' => 0,
    'site_url' => 0,
    'twitter_updates' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5183dcba819df6_41655439')) {function content_5183dcba819df6_41655439($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/emblemax/public_html/newsite/system/libs/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/emblemax/public_html/newsite/system/libs/smarty/plugins/modifier.truncate.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.ui.widget.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.fiji.ticker.js"></script>


	<script type="text/javascript">
		jq180 = jQuery.noConflict(true);
		
		jq180(function() {
			var i = 0;
			jq180("#ticker").ticker({
				initialTimeout: 1000,
				mouseOnTimeout: 4000,
				mouseOffTimeout: 3000,
				scrollTime: 1200,
				fadeTime: 1000,
				fixContainer: true,
				nextItem: function(lastItem) {
					return lastItem;
				}
			});
		})
	</script>

<div class="mt-col-right">
        <div class="mt-right-static" style="border:1px solid #e1e2e2;">
			<?php if (count($_smarty_tpl->tpl_vars['review']->value)>0){?>
	   		<h3 class="customerhd">Customer Reviews</h3>
			<div class="reviewpart">
				<div class="datepart"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['review']->value[0]['dAddedDate'],"%D");?>
</div>
				<div class="revtxt"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['review']->value[0]['tDescription'],100);?>
...</div>
				<div class="addname"><?php echo $_smarty_tpl->tpl_vars['review']->value[0]['vCustomerName'];?>
<br />
				<?php echo $_smarty_tpl->tpl_vars['review']->value[0]['vCity'];?>
,<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?><?php if ($_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCountryId']==$_smarty_tpl->tpl_vars['review']->value[0]['iCountryId']){?><?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCountry'];?>
<?php }?><?php endfor; endif; ?>
				</div>
				<div class="readmorelk"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
review">Read More...</a></div>
				<div class="readmorelk"><a class="subrev" href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
review#reviewsubmission">Submit Your Review</a></div>
			</div>
			<?php }?>
			<div style="clear:both;"></div>
	   </div>
	   
	  <!-- //Twitter Part-->
	   
	   
	   <div class="span12">
	<div class="twitter">
	<div class="footer-static-title">
		<h3>twitter</h3>
	</div>
	        
		<!--<div id="ticker">
	<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['twitter_updates']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>        
			<div id="mt-twitter"><p><?php echo $_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->user->screen_name;?>
<br /><a href="http://twitter.com/zootemplates" target="_blank"></a>
			<?php echo $_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->text;?>
<br /><a href="#" target="_blank"><?php echo $_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->source;?>
</a></p>
			    <div class="icontwitter clearfix"><span class="date pull-left"><?php echo $_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->user->created_at;?>
</span></div>
			</div>
			<div class=""></div>			 
	<?php endfor; endif; ?>
		</div>-->
		<ul class="tweets" id="ticker" style="height: 200px; width: 297px !important; padding-bottom: 20px;">
                        <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['twitter_updates']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<li><?php echo $_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->text;?>
<a href="#"><?php echo $_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->source;?>
</a>
                            <span><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['twitter_updates']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->created_at,"%A, %B %e, %Y");?>
</span>
                        </li>
                        <?php endfor; endif; ?>
		</ul>

	<!--<a href="#" class="prev_text" style="width:81px; float:left; text-align:right; margin-top:-10px; color:#484848; font-weight:bold; background:url(images/Previous_img.png) no-repeat 15px 7px;">Previous</a><a href="#" class="prev_text" style=" float:right; width:41px; margin:-10px 10px 10px 0; color:#484848; font-weight:bold; background:url(images/Previous_right.png) no-repeat right 8px;">Next</a>-->				</div>
</div>
</div>
<!--<div class="revbtn"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
review" class="revlk">Submit Reviews</a></div>--><?php }} ?>