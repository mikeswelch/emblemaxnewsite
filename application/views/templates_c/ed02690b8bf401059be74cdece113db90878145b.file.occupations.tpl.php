<?php /* Smarty version Smarty-3.1.11, created on 2014-02-21 05:45:14
         compiled from "application/views/templates/admin/occupations/occupations.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17220337951a5c8b6b443f7-28798600%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ed02690b8bf401059be74cdece113db90878145b' => 
    array (
      0 => 'application/views/templates/admin/occupations/occupations.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17220337951a5c8b6b443f7-28798600',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51a5c8b6cf2304_49360340',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51a5c8b6cf2304_49360340')) {function content_51a5c8b6cf2304_49360340($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
occupations/occupationslist"> Occupations</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Occupation<?php }else{ ?>Edit Occupation<?php }?></li>
		</ul>
	</div>

	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
		<div class="pagetitle">Add Occupation</div>
		<?php }else{ ?>
		<div class="pagetitle">Edit Occupation</div>
		<?php }?>
		
		<div class="add_ad_contentbox">
			<input type="hidden" name="Data[iOccupationsId]" id="iOccupationsId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iOccupationsId;?>
<?php }?>" />
			<input type="hidden" name="action" id="action" value="add" />
						
				<div class="inputboxes" id="">
					<label for="textfield"><span class="red_star">*</span>Occupation Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" lang="*" title="Occupation Name" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" maxlength="30" class="inputbox" name="Data[vTitle]" >
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
						<?php }?>
						<?php }else{ ?>
						<?php echo print_r($_smarty_tpl->tpl_vars['initOrder']->value);?>

						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
							
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['data']->value->iOrderNo==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
							<?php }?>
						<?php }?>
					
					</select>
				</div>
			
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star" ></span>Description</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tDescription" name="Data[tDescription]" class="inputbox" title="Description"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tDescription;?>
<?php }?></textarea></div>
				</div>				
			
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="InActive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='InActive'){?>selected<?php }?><?php }?> >InActive</option>
					</select>
				</div>
				<div class="add_can_btn"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="submit" value="Add Occupation" class="submit_btn" title="Add Occupation" onclick="return validate(document.frmadd);"/>
					<?php }else{ ?>
					<input type="submit" value="Edit Occupation" class="submit_btn" title="Edit Occupation" onclick="return validate(document.frmadd);"/>
					<?php }?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
occupations/occupationslist" class="cancel_btn">Cancel </a>
				</div>
				<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
	</form>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script type="text/javascript">

function checkparent(id){

	if(id == '0'){
		$('#menuboxid').css({display: "block"});
		$('#submenuboxid').css({display: "none"});
		
		$('#iconbox').css({display: "block"});
		$('#urlbox').css({display: "none"});
	}else{
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
var operation = '<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
';

if( operation == 'edit'){
	var iParentMenuId = '<?php echo $_smarty_tpl->tpl_vars['data']->value->iDesignParentCategoryId;?>
';
	if( iParentMenuId != 0){
		$('#submenuboxid').css({display: "block"});
		$('#menuboxid').css({display: "none"});
		
		$('#urlbox').css({display: "block"});
		$('#iconbox').css({display: "none"});
	}
}
</script>

<?php }} ?>