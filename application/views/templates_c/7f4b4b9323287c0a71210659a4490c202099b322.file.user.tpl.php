<?php /* Smarty version Smarty-3.1.11, created on 2014-02-21 05:48:57
         compiled from "application/views/templates/admin/user/user.tpl" */ ?>
<?php /*%%SmartyHeaderCode:879435301514fd52dbc1960-12887236%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7f4b4b9323287c0a71210659a4490c202099b322' => 
    array (
      0 => 'application/views/templates/admin/user/user.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '879435301514fd52dbc1960-12887236',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514fd52ddb1759_13054607',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'upload_path' => 0,
    'db_country' => 0,
    'db_state' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514fd52ddb1759_13054607')) {function content_514fd52ddb1759_13054607($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/userlist">User</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add  User<?php }else{ ?>Edit User<?php }?></li>
		</ul>
	</div>
	<div style="clear:both:"></div>
	<div class="centerpartbg">
		
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			<div class="pagetitle">Add User</div>
			<?php }else{ ?><div class="pagetitle_tab">
			<ul>
				<li class="active"><a href="#">Edit User</a></li>
				<!--<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
projectbrief/projectbrieflist?iUserId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
">Project Brief</a></li>-->
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
transaction/transactionlist?iUserId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
">Transaction</a></li>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
changepassword/passworduser?iUserId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
">Change Password</a></li>
				
			</ul>
			
			</div>
			<?php }?> 
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iUserId" id="iUserId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> First Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vFirstName" name="Data[vFirstName]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vFirstName;?>
<?php }?>" lang="*" title="First Name" />
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Last Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vLastName" name="Data[vLastName]" class="inputbox" title="Last Name"  lang="*" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vLastName;?>
<?php }?>" >
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> User Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vUserName" name="Data[vUserName]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vUserName;?>
<?php }?>" lang="*" title="User Name" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vPhoto"  name="vPhoto" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPhoto;?>
<?php }?>" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vPhoto"  name="vPhoto" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPhoto;?>
<?php }?>"<?php if ($_smarty_tpl->tpl_vars['data']->value->vPhoto==''){?> lang="*" <?php }?> onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->vPhoto!=''){?>
					<div class="view_del_user">
						<!--<div class="view_btn_user"><a href="#popfancy" id="fancyhref"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
view.png"/></a></div>
						<div class="delete_user"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
delete.png" onclick="ImageDelete(<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
);"/></div>-->
						
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
','user');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="popfancy"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/user/<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
/200x200_<?php echo $_smarty_tpl->tpl_vars['data']->value->vPhoto;?>
"></div>
						</div>
				</div>
					<?php }?><?php }?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Email</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vEmail"  name="Data[vEmail]" lang="*{E}" class="inputbox" title="Email" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vEmail;?>
<?php }?>"/>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vPassword" lang="*" title="Password" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPassword;?>
<?php }?>" maxlength="10" class="inputbox" name="Data[vPassword]" >
				</div>
				<?php }?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Contact Number</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vPhone" name="Data[vPhone]" lang="*" class="inputbox" title="Contact Number"  value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPhone;?>
<?php }?>" maxlength="15" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Country</label>
					<span class="collan_dot">:</span>
					<select id="iCountryId" name="Data[iCountryId]"  title="Country" lang="*" style="width:250px" onchange="getStates(this.value);">
						<option value=''>-- Select Country --</option>
						
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						
							<option value='<?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->iCountryId==$_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCountryId){?>selected<?php }?><?php }?> ><?php echo $_smarty_tpl->tpl_vars['db_country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vCountry;?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> State</label>
					<span class="collan_dot">:</span>
					<span id="statelist">
						<select id="iStateId" name="Data[iStateId]" title="State" style="width:250px">
							<option value=''>--Select State--</option>	
							<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>					    
							<option value='<?php echo $_smarty_tpl->tpl_vars['db_state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->iStateId==$_smarty_tpl->tpl_vars['db_state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['db_state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>

							</option>
					    <?php endfor; endif; ?>
						</select>
					</span>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Zipcode</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vZipCode" name="Data[vZipCode]" class="inputbox" title="Zipcode"  lang="*" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vZipCode;?>
<?php }?>" onkeypress="return checkprise(event)">
				</div>								
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Gender</label>
					<span class="collan_dot">:</span>
					<select id="eGender" name="Data[eGender]">
						<option value="Male" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eGender=='Male'){?>selected<?php }?><?php }?>>Male</option>
						<option value="Female" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eGender=='Female'){?>selected<?php }?><?php }?> >Female</option>
					</select>
				 </div>
				<div class="inputboxes">
				        <label for="textarea"> Address</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tAddress" class="inputbox" title="Address" name="Data[tAddress]" id="tIntroduction"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tAddress;?>
<?php }?></textarea>
				</div>
				<!--<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Activation Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vActivationcode" name="Data[vActivationcode]" class="inputbox" title="Activation Code"  lang="*" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vActivationcode;?>
<?php }?>" >
				</div>-->
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add User" class="submit_btn" title="Add User" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit User" class="submit_btn" title="Edit User" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/userlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
         
function getStates(icountryid)
{
    
    var adm_url = admin_url + 'site/ajax_call';
    $.ajax({   
	url: adm_url,
	async: false,
	type: "POST",
	data: "icountryid="+icountryid,
	dataType: "html",
	success: function(data) {
	   $('#statelist').html(data);
	}
    });
}
	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});



function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/deleteimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iUserId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	
	
</script>
 
<?php }} ?>