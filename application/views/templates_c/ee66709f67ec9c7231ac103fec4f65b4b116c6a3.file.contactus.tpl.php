<?php /* Smarty version Smarty-3.1.11, created on 2013-06-29 08:05:38
         compiled from "application/views/templates/admin/contactus/contactus.tpl" */ ?>
<?php /*%%SmartyHeaderCode:945702584515c2bf0cbee99-92311494%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee66709f67ec9c7231ac103fec4f65b4b116c6a3' => 
    array (
      0 => 'application/views/templates/admin/contactus/contactus.tpl',
      1 => 1372505991,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '945702584515c2bf0cbee99-92311494',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515c2bf0db3083_89422392',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515c2bf0db3083_89422392')) {function content_515c2bf0db3083_89422392($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
contactus/contactuslist">Contact Us</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add  Contact Us<?php }else{ ?>Edit Contact Us<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			 Contact Us Page
			 </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iContactId" id="iContactId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iContactId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					<label for="textfield"> Name</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><div style="color:#A9A9A9"><?php echo $_smarty_tpl->tpl_vars['data']->value->vName;?>
</div><?php }?>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"> Email</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><div style="color:#A9A9A9"><?php echo $_smarty_tpl->tpl_vars['data']->value->vEmail;?>
</div><?php }?>
				</div>
				<div class="inputboxes">
					<label for="textfield">Question</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><div style="color:#A9A9A9"><?php echo $_smarty_tpl->tpl_vars['data']->value->tQuestion;?>
</div><?php }?>
				</div>
				<div class="inputboxes">
				      <label for="textfield"> Added Date</label>
				      <span class="collan_dot">:</span>
				      <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><div style="color:#A9A9A9"><?php echo $_smarty_tpl->tpl_vars['data']->value->dAddedDate;?>
</div><?php }?>
			        
				</div>
				<div class="inputboxes">
					<label for="textfield"></span> Status</label>
					<span class="collan_dot">:</span>
					 <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><div style="color:#A9A9A9"><?php echo $_smarty_tpl->tpl_vars['data']->value->eStatus;?>
</div><?php }?>
					<!--<select id="estatus" name="Data[eStatus]">
						<option value="Read" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Read'){?>selected<?php }?><?php }?>>Read</option>
						<option value="Unread" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Unread'){?>selected<?php }?><?php }?> >Unread</option>
					</select>-->
				</div>
				<div >
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
contactus/contactuslist" style=" margin-left: 90px;text-align: center;text-decoration: none;width: 64px;" class="cancel_btn" >Back</a> </div>

					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<!--
<script type="text/javascript">
	
	var opts = {     
	formElements:{"dAddedDate":"d-ds-M-ds-Y"}                  
	};
	datePickerController.createDatePicker(opts);
	</script>
 -->
<?php }} ?>