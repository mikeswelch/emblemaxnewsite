<?php /* Smarty version Smarty-3.1.11, created on 2013-03-28 20:27:14
         compiled from "application/views/templates/admin/languagelable/view-langlable.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1112380508515459c1463d04-09480750%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d50c5f353dcce647632d4bd8a96ffa066ed9fab' => 
    array (
      0 => 'application/views/templates/admin/languagelable/view-langlable.tpl',
      1 => 1364482596,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1112380508515459c1463d04-09480750',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515459c1679e29_03742714',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'var_msg' => 0,
    'ssql' => 0,
    'keyword' => 0,
    'option' => 0,
    'action' => 0,
    'AlphaBox' => 0,
    'field' => 0,
    'order' => 0,
    'data' => 0,
    'class' => 0,
    'language_data' => 0,
    'recmsg' => 0,
    'page_link' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515459c1679e29_03742714')) {function content_515459c1679e29_03742714($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Language Lable</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Language Lable</div>
		<?php if ($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" /> <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p>
		</div>
		<div></div>
		<?php }elseif($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" /><?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p>
		</div>
		<div></div>
		<?php }?>
		<div class="admin_top_part">
			<!--<div class="addnew_btn"> <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/add" style="text-decoration:none;">Add New </a> </div>-->
			<form name="frmsearch" id="frmsearch" method="post" action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist">
				<input type="hidden" name="ssql" id="ssql" value="<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
" />
				<div class="search_top_admini">
					<label>Search:</label>
					<div id="newstatus" class="statusleft">
						<?php if ($_smarty_tpl->tpl_vars['keyword']->value=='Active'||$_smarty_tpl->tpl_vars['keyword']->value=='Inactive'){?>
							<select name="keyword" id="keyword">
								<option value="Active"<?php if ($_smarty_tpl->tpl_vars['keyword']->value!=''){?><?php if ($_smarty_tpl->tpl_vars['keyword']->value=='Active'){?>selected="selected"<?php }?><?php }?>>Active</option>
								<option value="Inactive"<?php if ($_smarty_tpl->tpl_vars['keyword']->value!=''){?><?php if ($_smarty_tpl->tpl_vars['keyword']->value=='Inactive'){?>selected="selected"<?php }?><?php }?>>Inactive</option>
							</select>
						<?php }else{ ?>
							<input type="Text" id="keyword" name="keyword" value="<?php if ($_smarty_tpl->tpl_vars['keyword']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['keyword']->value;?>
<?php }?>"  class="search_input" />
						<?php }?>
					</div>
					<select name="option" id="option" onchange="estatusdd(this.value);">
						<option value="vName"<?php if ($_smarty_tpl->tpl_vars['option']->value=='vName'){?>selected<?php }?>>Label Name</option>
						<option value="vValue_en"<?php if ($_smarty_tpl->tpl_vars['option']->value=='vValue_en'){?>selected<?php }?>>USA English</option>
						<option value="vValue_fr"<?php if ($_smarty_tpl->tpl_vars['option']->value=='vValue_fr'){?>selected<?php }?>>France</option>
						<option value="vValue_da"<?php if ($_smarty_tpl->tpl_vars['option']->value=='vValue_da'){?>selected<?php }?>>Canada Français</option>
						<option value="vValue_cn"<?php if ($_smarty_tpl->tpl_vars['option']->value=='vValue_cn'){?>selected<?php }?>>Canada English</option>
						
					</select>
					<input type="button" value="Search" class="search_btn" id="Search" name="Search" title="Search" onclick="Searchoption();"/>
				</div>
			</form>
			<div class="clear"></div>
			<!--<div class="action_apply_btn">
				<select class="act_sl_box" name="newaction" id="newaction">
					<option value="">Select Action</option>
					<option value="Active"<?php if ($_smarty_tpl->tpl_vars['action']->value=='Active'){?>selected="selected"<?php }?>>Make Active</option>
					<option value="Inactive"<?php if ($_smarty_tpl->tpl_vars['action']->value=='Inactive'){?>selected="selected"<?php }?>>Make Inactive</option>
					<option value="Deletes"<?php if ($_smarty_tpl->tpl_vars['action']->value=='Deletes'){?>selected="selected"<?php }?>>Make Delete</option>
					<option value="Show All"<?php if ($_smarty_tpl->tpl_vars['action']->value=='Show All'){?>selected="selected"<?php }?>>Show All</option>
				</select>
				<input type="button" value="Apply" class="apply_btn"  onclick="return Doaction(document.getElementById('newaction').value,'languagelablelist',document.frmlist,'languagelable');"/>
			</div>-->
			<div class="pagination"> <?php echo $_smarty_tpl->tpl_vars['AlphaBox']->value;?>
 </div>
			<div class="clear"></div>
		</div>
		<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/search_action" method="post">
				<input type="hidden" name="ssql" id="ssql" value="<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
" />
				<input type="hidden" name="action" id="action" value="" />
				<input  type="hidden" name="commonId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th width="40px"><input type="checkbox" id="check_all" name="check_all" onclick="checkAll(document.frmlist);"></th>
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist?field=vName><span style="color:white;">Lable Name</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='vName'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>						
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist?field=vValue_en><span style="color:white;">USA English</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='vValue_en'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist?field=vValue_fr><span style="color:white;">France</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='vValue_fr'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist?field=vValue_da><span style="color:white;">Canada Français</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='vValue_da'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>
							<th><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/languagelablelist?field=vValue_cn><span style="color:white;">Canada English</span><?php if ($_smarty_tpl->tpl_vars['field']->value=='vValue_cn'){?><?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>			
							<!--<th width="60px"><a href=<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelablelist/languagelablelist?field=estatus><span style="color:white;">Status</span> <?php if ($_smarty_tpl->tpl_vars['field']->value=='estatus'){?> <?php if ($_smarty_tpl->tpl_vars['order']->value=='ASC'){?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
up-arrow.png"/><?php }else{ ?><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
down-arrow.png"/><?php }?><?php }?></a></th>-->
							<th width="90px">Action</th>
						</tr>
					</thead>
					<?php if (count($_smarty_tpl->tpl_vars['data']->value)>0){?>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2==0){?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table_sec', null, 0);?>
					<?php }else{ ?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table', null, 0);?>
					<?php }?>
					<tbody class="<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
">
						<tr>
							<td><input name="iLabelId[]" type="checkbox" id="iId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLabelId;?>
"></td>
							<td><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/edit?iLabelId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLabelId;?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
</a></td>
							<?php if (count($_smarty_tpl->tpl_vars['language_data']->value)>0){?>
				                        <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['language_data']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
							<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->{'vValue_'.$_smarty_tpl->tpl_vars['language_data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vLangCode};?>
</td>
							<?php endfor; endif; ?>
				                        <?php }?>
							<!--<td><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->eStatus;?>
</td>-->
							<td><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
languagelable/edit?iLabelId=<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLabelId;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_edit.png" alt="Edit" title="Edit"><!--</a> <a href="javascript:void(0);" onclick="MakeAction('<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLabelId;?>
','Active','iLabelId');"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_approve.png" alt="Active" title="Active"></a> <a href="javascript:void(0);" onclick="MakeAction('<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLabelId;?>
','Inactive','iLabelId');"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_unapprove.png" alt="Inactive" title="Inactive"></a> <a href="#"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommon(<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLabelId;?>
,'languagelable/delete','<?php echo $_smarty_tpl->tpl_vars['ssql']->value;?>
')"></a>--> </td>
						</tr>
					</tbody>
					<?php endfor; endif; ?>
					<?php }else{ ?>
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					<?php }?>
				</table>
			</form>
		</div>
		<div class="bottomBox_admini">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td class="reclord_found" width="33%" align="left"><?php echo $_smarty_tpl->tpl_vars['recmsg']->value;?>
</td>
					<td width="33%"><div class="bottom_admin_paging"> <?php echo $_smarty_tpl->tpl_vars['page_link']->value;?>
 </div></td>
				</tr>
			</table>
			<div> </div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script >
function estatusdd(val1){
	var keyword=$('#keyword').val();
	if(keyword =='Active' || keyword =='Inactive')
	{
		keyword='';
	}
	if(val1 == 'eStatus')
	{
		$('#newstatus').html('<select name="keyword" id="keyword"><option value="Active"{if $keyword != ""}{if $keyword eq "Active"}selected="selected"{/if}{/if}>Active</option><option value="Inactive"{if $keyword != ""}{if $keyword eq "Inactive"}selected="selected"{/if}{/if}>Inactive</option></select>');
	}
	else{
		$('#newstatus').html('<input type="Text" id="keyword" name="keyword" value="'+keyword+'"  class="search_input" />');
	}
}
</script>
<?php }} ?>