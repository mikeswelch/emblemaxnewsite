<?php /* Smarty version Smarty-3.1.11, created on 2014-01-07 10:49:06
         compiled from "application/views/templates/admin/homeslider/homeslider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18181436915183e06bd7ff88-93791161%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bcabdab303110ec894869ec3181b97c78495c70a' => 
    array (
      0 => 'application/views/templates/admin/homeslider/homeslider.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18181436915183e06bd7ff88-93791161',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5183e06c0ba402_07127846',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'upload_path' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5183e06c0ba402_07127846')) {function content_5183e06c0ba402_07127846($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
banner/bannerlist">Home Slider Banner</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Banner<?php }else{ ?>Edit Banner<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Banner
			<?php }else{ ?>
			<ul>
				<li class="active"><a href="#">Edit Banner</a></li>
				
			</ul>
			
			
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBannerId" id="iBannerId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iBannerId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Banner Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Banner"/>
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Image</label>
					<span class="collan_dot">:</span>					
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>"<?php if ($_smarty_tpl->tpl_vars['data']->value->vImage==''){?> lang="*" <?php }?> onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
				<div class="inputboxes" >
					<div class="msg_alert1">
						Recommended Size : 1170 X 468
					</div>
				</div>			
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->vImage!=''){?>
					<div class="view_del_user">
						
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('<?php echo $_smarty_tpl->tpl_vars['data']->value->iHomeSliderId;?>
','homeslider');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="popfancy"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
home_slider/<?php echo $_smarty_tpl->tpl_vars['data']->value->iHomeSliderId;?>
/200X272_<?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
"></div>
						</div>
				</div>
				<?php }?><?php }?>

				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?>
				<div class="inputboxes" >
					<div class="msg_alert1">
						Recommended Size : 1170 X 468
					</div>
				</div>			
				<?php }?>
				<div class="inputboxes" >
                                        <label  for="textfield"><span class="red_star">*</span> Bottom Title</label>
					<span class="collan_dot">:</span>
                                        <input type="text" id="vBottomTitle"  name="Data[vBottomTitle]" class="inputbox" title="Bottom Title" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vBottomTitle;?>
<?php }?>" lang="*"/>
                                </div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
						<?php }?>
						<?php }else{ ?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
								
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['data']->value->iOrderNo==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
							<?php }?>
						<?php }?>
					
					</select>
				</div> 							

				<div class="inputboxes" >
                                        <label  for="textarea"><span class="red_star">*</span> Short Description</label>
					<span class="collan_dot">:</span>
                                        <textarea id="tShortDescription"  name="Data[tShortDescription]" class="inputbox" title="Short Description" value="" lang="*"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tShortDescription;?>
<?php }?></textarea>
                                </div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Show Delivery Date</label>
					<span class="collan_dot">:</span>
					<select id="eShowDeliveryDate" name="Data[eShowDeliveryDate]">
						<option value="Yes" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eShowDeliveryDate=='Yes'){?>selected<?php }?><?php }?>>Yes</option>
						<option value="No" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eShowDeliveryDate=='No'){?>selected<?php }?><?php }?> >No</option>
					</select>
				 </div>
				<div class="inputboxes" >
                                        <label  for="textfield"> Get Started Link</label>
					<span class="collan_dot">:</span>
                                        <input type="text" id="vGetStarted"  name="Data[vGetStarted]" class="inputbox" title="Get Started Link" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vGetStarted;?>
<?php }?>"/>
                                </div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='InActive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Banner" class="submit_btn" title="Add Banner" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Banner" class="submit_btn" title="Edit Banner" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homeslider/homesliderlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
         

$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});



function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
homeslider/deleteimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iHomeSliderId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	
	
</script>
 
<?php }} ?>