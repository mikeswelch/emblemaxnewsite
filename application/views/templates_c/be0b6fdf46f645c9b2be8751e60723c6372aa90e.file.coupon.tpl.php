<?php /* Smarty version Smarty-3.1.11, created on 2014-02-21 05:49:13
         compiled from "application/views/templates/admin/coupon/coupon.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17848556225167e92a233c96-85338619%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be0b6fdf46f645c9b2be8751e60723c6372aa90e' => 
    array (
      0 => 'application/views/templates/admin/coupon/coupon.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17848556225167e92a233c96-85338619',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5167e92a2f87b5_57020510',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5167e92a2f87b5_57020510')) {function content_5167e92a2f87b5_57020510($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
garment/garmentlist">Coupon</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Coupon<?php }else{ ?>Edit Coupon<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Coupon
			<?php }else{ ?>
                        Edit Coupon
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iCouponId" id="iCouponId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iCouponId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Coupon Code</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vCouponCode" name="Data[vCouponCode]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vCouponCode;?>
<?php }?>" lang="*" title="Coupon Code" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Discount</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fDiscount" name="Data[fDiscount]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->fDiscount;?>
<?php }?>" lang="*" title="Dicsount" onkeypress="return checkprise(event)"/>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Validity Type</label>
					<span class="collan_dot">:</span>
					<select id="eValidityType" name="Data[eValidityType]" lang="*" title="Validity Type">
						<option value=''>-- Select Validity Type--</option>
						<option value="Permanent" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eValidityType=='Permanent'){?>selected<?php }?><?php }?>>Permanent</option>
						<option value="Defined" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eValidityType=='Defined'){?>selected<?php }?><?php }?> >Defined</option>
					</select>
				 </div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Discount Type</label>
					<span class="collan_dot">:</span>
					<select id="eType" name="Data[eType]" lang="*" title="Dicount Type">
						<option value=''>-- Select Discount Type--</option>
						<option value="%" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eType=='%'){?>selected<?php }?><?php }?>>&#37;</option>
						<option value="�" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eType=='�'){?>selected<?php }?><?php }?> >&#163;</option>
						<option value="$" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eType=='$'){?>selected<?php }?><?php }?> >&#36;</option>
					</select>
				 </div>
				
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Start Date</label>
					<span class="collan_dot">:</span>
					<input type="text" id="dActiveDate" name="Data[dActiveDate]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->dActiveDate;?>
<?php }?>"  title="Start Date" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> End Date</label>
					<span class="collan_dot">:</span>
					<input type="text" id="dExpiryDate" name="Data[dExpiryDate]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->dExpiryDate;?>
<?php }?>"  title="End Date" />
				</div>
				
				<div class="inputboxes">
				        <label for="textarea"><span class="red_star"></span> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tDescriptions" class="inputbox" title="Description" name="Data[tDescriptions]" id="tDescriptions"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tDescriptions;?>
<?php }?></textarea>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Coupon" class="submit_btn" title="Add Coupon" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Coupon" class="submit_btn" title="Edit Coupon" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
coupon/couponlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
$('#dActiveDate').Zebra_DatePicker();		
$('#dExpiryDate').Zebra_DatePicker();
</script>

<?php }} ?>