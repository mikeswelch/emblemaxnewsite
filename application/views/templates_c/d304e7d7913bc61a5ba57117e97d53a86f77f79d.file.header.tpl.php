<?php /* Smarty version Smarty-3.1.11, created on 2014-02-12 00:55:45
         compiled from "application/views/templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2496860155183dcba60e289-19060752%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd304e7d7913bc61a5ba57117e97d53a86f77f79d' => 
    array (
      0 => 'application/views/templates/header.tpl',
      1 => 1392184447,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2496860155183dcba60e289-19060752',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5183dcba7fe682_09160869',
  'variables' => 
  array (
    'pageTitle' => 0,
    'front_css_path' => 0,
    'front_js_path' => 0,
    'site_url' => 0,
    'front_image_path' => 0,
    'config_tollfree' => 0,
    'isActiveMenu' => 0,
    'name' => 0,
    'homeBanner' => 0,
    'upload_path' => 0,
    'standardDeliveryMonth' => 0,
    'standardDeliveryDay' => 0,
    'rushDeliveryMonth' => 0,
    'rushDeliveryDay' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5183dcba7fe682_09160869')) {function content_5183dcba7fe682_09160869($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 8 ]><html class="ie8"<![endif]-->
<!--[if IE 9 ]><html class="ie9"<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $_smarty_tpl->tpl_vars['pageTitle']->value;?>
</title>
<meta name="description" content="Default Description" />
<meta name="keywords" content="Magento, Varien, E-commerce" />
<meta name="robots" content="INDEX,FOLLOW" />
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
widgets.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
general.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
productslist.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
productsscroller.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
cooslider.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
animate.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
print.css" media="print" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
custom-bootstrap.css" media="print" />

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
prototype.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
slider.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.mtlib.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.selectbox-0.2.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.filter.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
noConflict.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery-1.2.6.pack.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.flow.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
mt.accordion.js"></script>

<script type="text/javascript">
    var site_url = '<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
';
</script>

<link href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
default.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
/mColorPicker.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
styles-violet.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
custom-bootstrap.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
bootstrap-responsive.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
styles-responsive.css" media="all" />
<link href='<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
fonts.css' rel='stylesheet' type='text/css'/>
<link href='<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
fontslat.css' rel='stylesheet' type='text/css'/>
<link href='<?php echo $_smarty_tpl->tpl_vars['front_css_path']->value;?>
main.css' rel='stylesheet' type='text/css'/>

</head>
		
		<div class="header-container">
			<div class="header">
				<div class="container">
					<div class="row-fluid show-grid">
						<div class="span4"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
" title="Emblemax" class="logo pull-left"><strong>Magento Commerce</strong><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
logo.png" alt="Magento Commerce" /></a> </div>
							<div class="span6">
							<div class="conno"><?php echo $_smarty_tpl->tpl_vars['config_tollfree']->value;?>
</div>
							<div style="clear:both;"></div>
						<div class="row-fluid show-grid">
							<div class="grid-col">
								<div class="mt-show-right pull-right">
									<div class="top-search pull-left"> <span class="search-icon" style="font-size:11px; color:#002f5f; padding:0 0 0 13px; text-transform:uppercase; font-weight:bold;">Search</span>
										<div class="mt-search-form">
											<form id="search_mini_form" action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
search" method="get">
												<div class="search-form-border"></div>
												<div class="form-search">
													<input id="search" type="text" name="q" value="" class="input-text" maxlength="128" />
													<button type="submit" title="Search" class="button"> <span style="background:#9e0546;"><span style="background:#9e0546; font-family:Arial, Helvetica, sans-serif;">Search</span></span> </button>
													<div id="search_autocomplete" class="search-autocomplete"></div>
												</div>
											</form>
										</div>
									</div>
									<div class="top-cart pull-left"><span class="cart-loading">Loading...</span>
										<div class="cart"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
cart" class="mt-icon-ajaxcart" style="font-size:11px; color:#002f5f; padding:0 0 0 18px; text-transform:uppercase; font-weight:bold;">cart</a> <span class="mt-cart-label"> Bag:(0) </span>
											<div class="mtajaxcart">
												<div class="search-form-border"></div>
												<div class="ajax-container">
													<p class="no-items-in-cart">You have no items in your shopping cart.</p>
												</div>
											</div>
										</div>
									</div>
									<?php if ($_SESSION['sess_UserName']!=''){?>	
									<div class="top-link pull-left" style="margin:0 0 0 0;"> <span class="link-icon hidden-phone" style="font-size:11px; color:#002f5f; padding:0 16px 0 18px;display: inline; text-transform:uppercase; font-weight:bold;">Welcome, <?php echo $_SESSION['sess_UserName'];?>
 </span>
									<?php }else{ ?>
									<div class="top-link pull-left" style="margin:0 0 0 0;"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
login"><span class="link-icon hidden-phone" style="font-size:11px; color:#002f5f; text-transform:uppercase; font-weight:bold;">Login</span></a>
									<?php }?>
										<div class="mt-top-link">
											<div class="search-form-border"></div>
											<?php if ($_SESSION['sess_UserName']){?>
											<div class="mt-top-link-inner">
											    <ul class="links">
												<li class="first" ><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/myAccount" title="My Account" >My Account</a></li>
                                                                                                <li ><a href="#" title="My Wishlist" >My Wishlist</a></li>
                                                                                                <li ><a href="#" title="My Cart" class="top-link-cart">My Cart</a></li>
                                                                                                <li ><a href="#" title="Checkout" class="top-link-checkout">Checkout</a></li>
                                                                                                <li class=" last" ><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
authentication/logout" title="Log In" >Log Out</a></li>
                                                                                            </ul>
											</div>
											<?php }?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						
						<div class="mt-drill-menu clearfix">
							<div class="mt-drillmenu hidden-desktop">
								<div class="navbar">
									<div class="navbar-inner">
										<div class="mt-nav-container">
											<div class="block-title clearfix"> <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <span class="brand">Navigation</span> </div>
											<div class="nav-collapse collapse"> 
												
												<ul id="mt_accordionmenu" class="nav-accordion">
													<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
">Home</a></li>
													<li class="item nav-categories hasChild"> <a class="item" href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category">Categories</a>
													</li>
													<li class="item nav-kids"> <a class="item" href="#">KIDS</a> </li>
													<li class="item nav-men"> <a class="item" href="#">Men</a> </li>
													<li class="item nav-women hasChild"> <a class="item" href="#">Women</a>
														<ul class="detail-parent">
															<li class="item nav-women-dresses"> <a class="item" href="#">Dresses</a> </li>
															<li class="item nav-women-day"> <a class="item" href="#">Day</a> </li>
															<li class="item nav-women-evening"> <a class="item" href="#">Evening</a> </li>
															<li class="item nav-women-sundresses"> <a class="item" href="#">Sundresses</a> </li>
															<li class="item nav-women-sweater"> <a class="item" href="#">Sweater</a> </li>
															<li class="item nav-women-belts"> <a class="item" href="#">Belts</a> </li>
															<li class="item nav-women-blouses-and-shirts"> <a class="item" href="#">Blouses and Shirts</a> </li>
															<li class="item nav-women-t-shirts"> <a class="item" href="#">T-Shirts</a> </li>
															<li class="item nav-women-cocktail"> <a class="item" href="#">Cocktail</a> </li>
															<li class="item nav-women-hair-accessories"> <a class="item" href="#">Hair Accessories</a> </li>
															<li class="item nav-women-hats-and-gloves"> <a class="item" href="#">Hats and Gloves</a> </li>
															<li class="item nav-women-lifestyle"> <a class="item" href="#">Lifestyle</a> </li>
															<li class="item nav-women-bras"> <a class="item" href="#">Bras</a> </li>
															<li class="item nav-women-scarves"> <a class="item" href="#">Scarves</a> </li>
															<li class="item nav-women-small-leathers"> <a class="item" href="#">Small Leathers</a> </li>
															<li class="item nav-women-accessories hasChild"> <a class="item" href="#">Accessories</a>
																<ul class="detail-parent">
																	<li class="item nav-women-accessories-sunglasses last"> <a class="item" href="#">Sunglasses</a> </li>
																</ul>
															</li>
															<li class="item nav-women-evening-23"> <a class="item" href="#">Evening</a> </li>
															<li class="item nav-women-long-sleeved"> <a class="item" href="#">Long Sleeved</a> </li>
															<li class="item nav-women-short-sleeved"> <a class="item" href="#">Short Sleeved</a> </li>
															<li class="item nav-women-sleeveless"> <a class="item" href="#">Sleeveless</a> </li>
															<li class="item nav-women-tanks-and-camis"> <a class="item" href="#">Tanks and Camis</a> </li>
															<li class="item nav-women-tops hasChild"> <a class="item" href="#">Tops</a>
																<ul class="detail-parent">
																	<li class="item nav-women-tops-tunics-and-kaftans last"> <a class="item" href="#">Tunics and Kaftans</a> </li>
																</ul>
															</li>
															<li class="item nav-women-totes"> <a class="item" href="#">Totes</a> </li>
															<li class="item nav-women-clutches"> <a class="item" href="#">Clutches</a> </li>
															<li class="item nav-women-cross-body"> <a class="item" href="#">Cross Body</a> </li>
															<li class="item nav-women-satchels"> <a class="item" href="#">Satchels</a> </li>
															<li class="item nav-women-shoulder"> <a class="item" href="#">Shoulder</a> </li>
															<li class="item nav-women-briefs"> <a class="item" href="#">Briefs</a> </li>
															<li class="item nav-women-handbags"> <a class="item" href="#">Handbags</a> </li>
															<li class="item nav-women-camis"> <a class="item" href="#">Camis</a> </li>
															<li class="item nav-women-nightwear"> <a class="item" href="#">Nightwear</a> </li>
															<li class="item nav-women-shapewear"> <a class="item" href="#">Shapewear</a> </li>
															<li class="item nav-women-lingerie last"> <a class="item" href="#">Lingerie</a> </li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				
				<div class="mainnav">
					<div class="container">
						<div class="naviga span8">
							<div class="row-fluid show-grid mt-nav">
								<div class="mt-main-menu clearfix"> 
									<!-- navigation BOF -->
									<div class="mt-navigation visible-desktop">
										<div class="mt-main-menu">
											<ul id="nav" class="megamenu pull-right">
												<li class="level0 home level-top"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['isActiveMenu']->value=='home'){?> class="level-top actnav" <?php }?>><span>Home</span></a> </li>
												<li class="level0 nav-2 level-top"> <a href="http://www.companycasuals.com/emblemax/start.jsp" target="_blank" class="level-top"> <span>Apparel</span> </a> </li>
												<li class="level0 nav-3 level-top"> <a href="http://www.emblemax.net/" target="_blank" class="level-top"> <span>Promotional Products</span> </a> </li>
												<li class="level0 nav-4 level-top last parent"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant-quote" <?php if ($_smarty_tpl->tpl_vars['isActiveMenu']->value=='instant-quote'){?> class="level-top actnav" <?php }?>><span>Get a Quote</span> </a>
                                                                                                <!--<li class="level0 nav-4 level-top last parent"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
design-studio" class="level-top"> <span>Design Studio</span> </a>-->
												<li class="level0 nav-4 level-top last parent"> <a href="#" class="level-top"> <span>Preferred Supplier</span> </a>
													<div class="sub-wrapper">
														<ul class="level0">
															<li class="first">
																<ol>
																	<li class="level1 nav-4-1 first last"> <a href="http://www.emblemax.com/leeds.php" target="_blank"> <span>Leeds</span> </a> </li>
																	<li class="level1 nav-4-2 last"> <a href="http://www.emblemax.com/awardcraft.php" target="_blank"> <span>Award Craft</span> </a> </li>
																	<li class="level1 nav-4-3 last"> <a href="http://www.emblemax.com/highline.php" target="_blank"> <span>High Caliber</span> </a> </li>
																	<li class="level1 nav-4-4 last"> <a href="http://www.emblemax.com/lanco.php" target="_blank"> <span>Lanco</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/gemline.php" target="_blank"> <span>Gemline</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/primeline.php" target="_blank"> <span>Prime Line</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/bicgraphic.php" target="_blank"> <span>Bic Graphics USA</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/cutterbuck.php" target="_blank"> <span>Cutter & Buck</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/bulletline.php" target="_blank"> <span>Bullet Line</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/logomark.php" target="_blank"> <span>Logomark</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/wearablescatalog.php" target="_blank"> <span>Wearables Catalog</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/alternative.php" target="_blank"> <span>Alternative</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/snugz.php" target="_blank"> <span>Snugz Lanyards</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/hitpromo.php" target="_blank"> <span>Hit Promo</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/crystal.php" target="_blank"> <span>Crystal Images</span> </a> </li>																
																	
						    										</ol>
															</li>
															<li class="menu-static-blocks"></li>
														</ul>												
                                                                                                <li class="level0 nav-1 level-top first parent"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
aboutus" <?php if ($_smarty_tpl->tpl_vars['isActiveMenu']->value=='aboutus'){?> class="level-top actnav" <?php }?>> <span>About Us</span> </a>
                                                                                                <li class="level0 level-top parent custom-block "> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
faq" <?php if ($_smarty_tpl->tpl_vars['isActiveMenu']->value=='faq'){?> class="level-top actnav" <?php }?>> <span>FAQ</span> </a> </li>
													<div class="sub-wrapper">
														<ul class="level0">
															<li class="first">
																<ol>
																	<li class="level1 nav-4-1 first last"> <a href="#"> <span>01</span> </a> </li>
																	<li class="level1 nav-4-2 last"> <a href="#"> <span>02</span> </a> </li>
																	<li class="level1 nav-4-3 last"> <a href="#"> <span>Evening</span> </a> </li>
																	<li class="level1 nav-4-4 last"> <a href="#"> <span>Sundresses</span> </a> </li>
																	<li class="level1 nav-4-5 last"> <a href="#"> <span>Sweater</span> </a> </li>											
																	
																	<li class="level1 nav-4-16 last parent"> <a href="#"> <span>Accessories</span> </a>
																		<div class="sub-wrapper">
																			<ul class="level1">
																				<li class="level2 nav-4-16-1 first last"> <a href="#"> <span>Sunglasses</span> </a> </li>
																				<li class="menu-static-blocks"></li>
																			</ul>
																		</div>
																	</li>
																	<li class="level1 nav-4-17 last"> <a href="#"> <span>Evening</span> </a> </li>
																	
																	<li class="level1 nav-4-22 last parent"> <a href="#"> <span>Tops</span> </a>
																		<div class="sub-wrapper">
																			<ul class="level1">
																				<li class="level2 nav-4-22-2 first last"> <a href="#"> <span>Tunics and Kaftans</span> </a> </li>
																				<li class="menu-static-blocks"></li>
																			</ul>
																		</div>
																	</li>
																	<li class="level1 nav-4-23 last"> <a href="#"> <span>Totes</span> </a> </li>
																	<li class="level1 nav-4-24 last"> <a href="#"> <span>Clutches</span> </a> </li>
																</ol>
															</li>
															<li class="menu-static-blocks"></li>
														</ul>
													</div>
												</li>
												<li class="level0 nav-4 level-top last parent"> <a href="#" > <span>Contact</span> </a>
													<div class="sub-wrapper">
														<ul class="level0">
															<li class="first">
																<ol>
																	<li class="level1 nav-4-1 first last"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
contactus" > <span>Contact Us</span> </a> </li>
																	<li class="level1 nav-4-2 last"> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
request_catalog" > <span>Request Catalog</span> </a> </li>
						    										</ol>
															</li>
															<li class="menu-static-blocks"></li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
									</div>
									
									<script type="text/javascript"> 
									$mtkb(function(){ 
										$mtkb(".megamenu").megamenu({
											'animation':'slide', 
											'mm_timeout': 150
										}); 
									});  
									</script>
									
									<!-- navigation EOF --> </div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
<?php if ($_smarty_tpl->tpl_vars['name']->value=='home'||$_smarty_tpl->tpl_vars['name']->value=='category'){?>
<script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.easing.1.3.js'></script>
<script type='text/javascript' src='<?php echo $_smarty_tpl->tpl_vars['front_js_path']->value;?>
jquery.coda-slider-2.0.js'></script>

<script type="text/javascript">
    /*$(window).load(function() {
        //$('#coda-slider-1').html('<div style="color:#c03b37; font-size:15px;"><img src="'+site_url+'public/front-end/images/ajax-loader.gif">  Loading...</div>');
	});
    
    */
    
	  $(window).load(function() {        
		$('#coda-slider-1').codaSlider({
		    //autoSlide: true,
		    //autoSlideInterval: 3000,
		}
		);
		
	});
	
</script>
	 
<script type="text/javascript">
	$mtkb(document).ready(function(){
		$mtkb("#select-language").selectbox();
    	$mtkb("#select-currency").selectbox();
    	//$mtkb("#select_9").selectbox();
    	$mtkb("#mt_sort_by").selectbox();
    	$mtkb("#mt_limiter").selectbox();
	});
</script>

<div class="coda-slider-wrapper">
	<div class="coda-slider preload" id="coda-slider-1">            
                <?php if (count($_smarty_tpl->tpl_vars['homeBanner']->value)>0){?>
                <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['homeBanner']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?> 
		<div class="panel">
                        <div class="panel-wrapper">							
                    <h2 class="title" style="display:none; float:none; margin:0;"><?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vBottomTitle'];?>
</h2>							
				<div class="slidcont">
					<div class="prodimgpart"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/home_slider/<?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iHomeSliderId'];?>
/1170X468_<?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="" /></div>
					    <?php if ($_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eShowDeliveryDate']=='Yes'){?>
					    <div class="datapart">
						    <h3><?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</h3>
						    <p><?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tShortDescription'];?>
</p>
						    <div class="slidbtn"><a href="<?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vGetStarted'];?>
">Get Started</a></div>
						    <div class="calendorpt">
							    <div class="estdate">Estimated delivery date<span style="color: red;">*</span></div>
							    <div class="calone">
								    <p class="standerdtx">Standard</p>
								    <p class="monthcal"><?php echo $_smarty_tpl->tpl_vars['standardDeliveryMonth']->value;?>
</p>
								    <p class="datecal"><strong><?php echo $_smarty_tpl->tpl_vars['standardDeliveryDay']->value;?>
</strong></p>
							    </div> 
							    <div class="calone">
								    <p class="standerdtx">Rush</p>
								    <p class="monthcal"><?php echo $_smarty_tpl->tpl_vars['rushDeliveryMonth']->value;?>
</p>
								    <p class="datecal"><strong><?php echo $_smarty_tpl->tpl_vars['rushDeliveryDay']->value;?>
</strong></p>
							    </div>
							    <p style="color: red;">*Need it faster? Contact us for options.</p>
						    </div>
						    
					    </div>
					    <?php }else{ ?>
					    <div class="datapart_without_deliverydate">
						    <h3><?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTitle'];?>
</h3>
						    <p><?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['tShortDescription'];?>
</p>
						    <!--<?php if ($_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vGetStarted']!=''){?> -->
						    <div class="slidbtn"><a href="<?php echo $_smarty_tpl->tpl_vars['homeBanner']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vGetStarted'];?>
">Get Started</a></div>
						    <!--<?php }?>-->
					    </div>
					    <?php }?>
				</div>
			</div>
		</div>
                <?php endfor; endif; ?>
                <?php }?>
	</div><!-- .coda-slider -->
</div>
<?php }?>


<?php }} ?>