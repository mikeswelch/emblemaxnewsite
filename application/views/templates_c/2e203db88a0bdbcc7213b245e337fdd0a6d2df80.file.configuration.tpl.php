<?php /* Smarty version Smarty-3.1.11, created on 2014-01-16 13:21:01
         compiled from "application/views/templates/admin/configuration/configuration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:613977912514d3684eb4692-62704616%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e203db88a0bdbcc7213b245e337fdd0a6d2df80' => 
    array (
      0 => 'application/views/templates/admin/configuration/configuration.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '613977912514d3684eb4692-62704616',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514d368508c4b9_05899548',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'db_config' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514d368508c4b9_05899548')) {function content_514d368508c4b9_05899548($_smarty_tpl) {?> <?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li class="current">Configuration</li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">Edit Configuration</div>
		<div class="add_ad_contentbox configuration_input">
			<form id="frmadd" name="frmadd" method="post" action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
configuration/edit">
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_config']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"><?php if ($_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Gmail Plus Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='FaceBook Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Twitter Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='LinkedIn Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Vimeo Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Wordpress Blog Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Flickr Link'){?><?php echo '';?>
<?php }else{ ?>*<?php }?></span> <?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription;?>
</label>
					<span class="collan_dot">:</span> <?php if ($_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Admin Email Id'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Mail Footer'){?>
					<input type="text"  id="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
" name="Data[<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vValue;?>
" lang="*{E}" title="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription;?>
"/>
					<br />
					<?php }elseif($_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='FaceBook Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Twitter Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='LinkedIn Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Vimeo Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Flickr Link'||$_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Wordpress Blog'){?>
					<input type="text"  id="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
" name="Data[<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vValue;?>
"  title="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription;?>
"/>
					<br />
					<?php }elseif($_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription=='Date time set'){?>
					<input type="text"  id="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
" name="Data[<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vValue;?>
" lang="*" title="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription;?>
"/>
					<br />
					<br>
					<div class="dated_conf_txt">[ Date must be enter in   yyyy-mm-dd  format]</div>
					<?php }else{ ?>
					<input type="text"  id="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
" name="Data[<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vName;?>
]" class="inputbox" value="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vValue;?>
" lang="*" title="<?php echo $_smarty_tpl->tpl_vars['db_config']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->tDescription;?>
"/>
					<br />
					<?php }?> </div>
				<?php endfor; endif; ?>
				<div class="add_can_btn">
					<input type="submit" value="Edit Configuration" class="submit_btn" title="Edit Configuration" onclick="return validate(document.frmadd);"/>
					<!--<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
configuration/configurationlist"><input type="button" value="Cancel" class="cancel_btn" title="Cancel"/></a>-->
				</div>
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
$('#DELIVERY_DATE').Zebra_DatePicker();		
</script>
<?php }} ?>