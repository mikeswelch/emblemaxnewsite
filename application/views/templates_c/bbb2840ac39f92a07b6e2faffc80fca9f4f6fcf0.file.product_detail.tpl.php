<?php /* Smarty version Smarty-3.1.11, created on 2013-12-04 21:10:39
         compiled from "application/views/templates/product_detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1121929611522727529f1749-13893119%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbb2840ac39f92a07b6e2faffc80fca9f4f6fcf0' => 
    array (
      0 => 'application/views/templates/product_detail.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1121929611522727529f1749-13893119',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_52272752aef922_53238719',
  'variables' => 
  array (
    'site_url' => 0,
    'productData' => 0,
    'upload_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52272752aef922_53238719')) {function content_52272752aef922_53238719($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"Example Smarty Page"), 0);?>

<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
	<div class="breadcrumbs row-fluid">
   <div class="container">
     <ul class="span12" style="text-align:left;">
	<li class="home">
	     <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
" title="Go to Home Page">Home</a>
	     <samp></samp>
	</li> 
	<li class="category5">
	     <strong>T-Shirt</strong>
	</li> 
     </ul> 

   </div>	
   </div>
	<div class="main container">
	<h2 class="pagetital subtit" style="width:150px !important; width:28% !important;"><?php echo ucfirst($_smarty_tpl->tpl_vars['productData']->value[0]->vProductName);?>
 </h2>
         <div class="product_quto_main">
	    
	    	<div class="product_detail_left">
		<div class="images">
		    <div class="main_image multiple storePadHeight" style="text-align: center;">
			<img height="400px" width="300px" src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['productData']->value[0]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['productData']->value[0]->vImage;?>
">			
		    </div>
		    
		</div>
	    </div>
	    
	    
	    <div class="product_detail_right">
		<div id="Module_ApparelQuote_container">
		    <h4 class="quote_steps_head"><?php echo ucfirst($_smarty_tpl->tpl_vars['productData']->value[0]->vProductName);?>
</h4>
		    <h5 class="noneSizeSelected" style="margin-top: 20px;"><?php echo ucfirst($_smarty_tpl->tpl_vars['productData']->value[0]->tDescription);?>
</h5>
		    <div style="float: left; margin-top:35%;">
			<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant-quote/<?php echo $_smarty_tpl->tpl_vars['productData']->value[0]->iProductId;?>
"><input type="button" value="Get A Quote" class="instnt_btn" style="float: left; background: #9e0546;border: 2px solid #d17a99; line-height: 25px;padding: 5px 15px;" title="GET A QUOTE"></a>
			<a href="#upload-artwork" id="clickUploadArtwork"></a>
			<?php if ($_smarty_tpl->tpl_vars['productData']->value[0]->vSizeChart!=''){?><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
productdetail/download?id=<?php echo $_smarty_tpl->tpl_vars['productData']->value[0]->iProductId;?>
"><input type="button" value="Size Chart" class="instnt_btn" id="upload_artwork_cart" style="float: left; margin-left: 20px;background: #9e0546; border: 2px solid #d17a99;line-height: 25px; padding: 5px 15px; " title="SIZE CHART"></a><?php }?>
		    </div>
		</div>
	    </div>
	 </div>        </div>
    </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>