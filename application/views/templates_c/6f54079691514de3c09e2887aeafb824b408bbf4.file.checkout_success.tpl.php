<?php /* Smarty version Smarty-3.1.11, created on 2013-09-03 07:37:04
         compiled from "application/views/templates/checkout_success.tpl" */ ?>
<?php /*%%SmartyHeaderCode:107486692651ea9296997126-56831020%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6f54079691514de3c09e2887aeafb824b408bbf4' => 
    array (
      0 => 'application/views/templates/checkout_success.tpl',
      1 => 1377257072,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '107486692651ea9296997126-56831020',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51ea92969f91c8_32713192',
  'variables' => 
  array (
    'invoiceNumber' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51ea92969f91c8_32713192')) {function content_51ea92969f91c8_32713192($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container col2-right-layout" style="min-height: 210px;">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0; min-height: 210px;">
        <div class="main container">
            <div class="slideshow_static"></div>
            <div class="main-inner">
                <div class="col-main">
                    <div class="account-create">
                        <h2 class="pagetital subtit">Order Placed Successfully</h2>
                        <div style="clear:both;"></div>
                        <div class="ordleftpart" style="width:100%;">
                            <div class="emptyCart">
                                <p style="font-size: 20px;">Thank you for your purchase!</p>
                                <p style="font-size: 15px;">Your order number is: <?php echo $_smarty_tpl->tpl_vars['invoiceNumber']->value;?>
</p>
                                <p style="font-size: 15px;">You will receive an order confirmation email with details of your order.</p>
                                <p style="font-size: 15px;">Click <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category">here</a> to continue shopping.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>