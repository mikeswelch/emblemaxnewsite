<?php /* Smarty version Smarty-3.1.11, created on 2014-02-12 01:27:21
         compiled from "application/views/templates/admin/product/editcolor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14602969395166c198e17d27-20727707%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b14c0049fdb3643c4eb084528f04894cea134de' => 
    array (
      0 => 'application/views/templates/admin/product/editcolor.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14602969395166c198e17d27-20727707',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5166c198e7e535_14055451',
  'variables' => 
  array (
    'data' => 0,
    'operation' => 0,
    'lang' => 0,
    'color' => 0,
    'upload_path' => 0,
    'admin_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5166c198e7e535_14055451')) {function content_5166c198e7e535_14055451($_smarty_tpl) {?><div class="pagetitle" style="text-align:center;">
			Edit Color Image
		</div>
			<form id="frmaddcolor" name="frmaddcolor" method="post" enctype="multipart/form-data"  action="">
			<input type="hidden" name="iProductColorId" id="iProductColorId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
" />
                        <input type="hidden" name="iProductId" id="iProductId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
" />
			<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
			
                        <input type="hidden" name="type" id="type" value="color" />
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='edit'){?>
			<input type="hidden" name="mode" id="mode" value="edit" />
			<?php }else{ ?>
			<input type="hidden" name="mode" id="mode" value="edit" />
			<?php }?>
			<input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" />
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Color :</label>
					 <span class="collan_dot">:</span>					 
					<select id="eStatus" name="Datacolor[iColorId]" lang="*" title="color">
			                        <option value=''>--Select Color--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['color']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iColorId;?>
' <?php if ($_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iColorId==$_smarty_tpl->tpl_vars['data']->value[0]->iColorId){?>selected<?php }?>><?php echo ucfirst($_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColor);?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Front Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
			                <?php }else{ ?>
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image"  onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
                                <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value[0]->vFrontImage!=''){?>
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#Fancyhrefmain" id="fancyhrefmain" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteFrontImage('<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
','product','vFrontImage');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="Fancyhrefmain" style="padding-bottom: 900px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vFrontImage;?>
"></div>
						</div>
				         </div>
				<?php }?><?php }?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Back Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image"   onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
                                <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value[0]->vBackImage!=''){?>
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#Fancyhrefback" id="fancyhrefback" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteBackImage('<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="Fancyhrefback" style="padding-bottom: 900px;"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vBackImage;?>
"></div>
						</div>
				         </div>
				<?php }?><?php }?>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Left Sleeve Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vLeftSleeveImage"  name="vLeftSleeveImage" title="Left Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vLeftSleeveImage"  name="vLeftSleeveImage" title="Left Image"  onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
                                <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value[0]->vLeftSleeveImage!=''){?>
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#FancyhrefleftSleeeve" id="fancyhrefleftSleeeve" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteLeftImage('<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="FancyhrefleftSleeeve"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vLeftSleeveImage;?>
"></div>
						</div>
				         </div>
				<?php }?><?php }?>	
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Right Sleeve Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vRightSleeveImage"  name="vRightSleeveImage" title="Right Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vRightSleeveImage"  name="vRightSleeveImage" title="Right Image"   onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
                                <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value[0]->vRightSleeveImage!=''){?>
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#FancyhrerightSleeeve" id="fancyhrerightSleeeve" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="DeleteRightImage('<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="FancyhrerightSleeeve"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
/200X200_<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vRightSleeveImage;?>
"></div>
						</div>
				         </div>
				<?php }?><?php }?>
						
                                
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['data']->value[0]->eStatus=='Active'){?>selected<?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['data']->value[0]->eStatus=='Inactive'){?>selected<?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<input type="submit" value="Add Color Image" class="submit_btn" title="Add Color Image" onclick="return validate(document.frmaddcolor);"/>
						<?php }else{ ?>
						<input type="submit" value="Edit Color Image" class="submit_btn" title="Edit Color Image" onclick="return validate(document.frmaddcolor);"/>
						<?php }?>
				</div>
			</div>
</form>


<script type="text/javascript">
         	
function DeleteFrontImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deleteColorimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
&productId=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
&img=vFrontImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}

function DeleteBackImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deleteColorimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
&productId=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
&img=vBackImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}
function DeleteRightImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deleteColorimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
&productId=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
&img=vRightSleeveImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}
function DeleteLeftImage(id,file1,imageName){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deleteColorimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductColorId;?>
&productId=<?php echo $_smarty_tpl->tpl_vars['data']->value[0]->iProductId;?>
&img=vLeftSleeveImage">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}
<?php }} ?>