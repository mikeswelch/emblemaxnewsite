<?php /* Smarty version Smarty-3.1.11, created on 2013-12-12 10:39:12
         compiled from "application/views/templates/admin/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:254648822514d3521cfb794-22721498%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c5f3220bc959dcdb0f8227dba3175dd7f8fdd89' => 
    array (
      0 => 'application/views/templates/admin/header.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '254648822514d3521cfb794-22721498',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_514d3521d4fca2_03045862',
  'variables' => 
  array (
    'admin_css_path' => 0,
    'admin_js_path' => 0,
    'fancybox_path' => 0,
    'admin_url' => 0,
    'admin_image_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_514d3521d4fca2_03045862')) {function content_514d3521d4fca2_03045862($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ideas that promote</title>
<link href="<?php echo $_smarty_tpl->tpl_vars['admin_css_path']->value;?>
style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $_smarty_tpl->tpl_vars['admin_css_path']->value;?>
boxes.css" rel="stylesheet" type="text/css" />


<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
functions.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
validate.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
jlist.js"></script>


<!--Chart -->
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
FusionCharts.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
highcharts.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
exporting.js"></script>

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
common.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
admin_custom_validation.js"></script>

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['fancybox_path']->value;?>
lib/jquery-1.8.0.min.js"></script>
<script  type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
bootstrap-modal.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['fancybox_path']->value;?>
source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['fancybox_path']->value;?>
source/jquery.fancybox.css?v=2.1.0" media="screen" />
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
jquery.form.js"></script>

<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
zebra-datepicker/css/zebra_datepicker.css" type="text/css">
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['admin_js_path']->value;?>
zebra-datepicker/javascript/zebra_datepicker.js"></script>

<script type="text/javascript">
    var admin_url ='<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
    var admin_image_path='<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
';
</script>


</head>
<body>
<div class="sitecontainer">
	<div class="header">
		<div class="adminmsg">
            <label style="color:#9E053B">Welcome <?php echo $_SESSION['sess_Name'];?>
</label><br />            
        </div>
        <div class="userpanel"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
authentication/logout">Logout</a>&nbsp;&nbsp;</div>
		<div class="logo"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
logo.png" alt="" title="" /></div>
		<!--<div class="logo"><h1 style="color: #FFFFFF">Emblemax</h1></div>-->
	</div>
	<div class="container">
		<div class="containerBg"><?php }} ?>