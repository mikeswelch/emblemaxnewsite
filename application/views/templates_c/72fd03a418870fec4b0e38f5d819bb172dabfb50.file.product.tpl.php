<?php /* Smarty version Smarty-3.1.11, created on 2013-05-01 09:53:37
         compiled from "application/views/templates/admin/product/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1836428349515c1a997af203-49385685%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '72fd03a418870fec4b0e38f5d819bb172dabfb50' => 
    array (
      0 => 'application/views/templates/admin/product/product.tpl',
      1 => 1367422342,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1836428349515c1a997af203-49385685',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_515c1a999e9626_92003475',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'language' => 0,
    'lang' => 0,
    'data' => 0,
    'db_producttype' => 0,
    'Data' => 0,
    'upload_path' => 0,
    'sizelist' => 0,
    'color' => 0,
    'var_msg' => 0,
    'data_color' => 0,
    'class' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_515c1a999e9626_92003475')) {function content_515c1a999e9626_92003475($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist">Product Template</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Product<?php }else{ ?>Edit Product Template<?php }?></li>
		</ul>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?>
			<div class="pagetitle_tab">
				<ul>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['language']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<?php if ($_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode==$_smarty_tpl->tpl_vars['lang']->value){?>
				<li class="active"><a href="#"><?php echo $_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLanguage;?>
</a></li>
				<?php }else{ ?>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/edit?iProductId=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
&lang=<?php echo $_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode;?>
"><?php echo $_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLanguage;?>
</a></li>
				<?php }?>
				<?php endfor; endif; ?>
				</ul>
			</div>
	<?php }else{ ?>
			<div class="pagetitle_tab">
				<ul>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['language']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<?php if ($_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLangCode==$_smarty_tpl->tpl_vars['lang']->value){?>
				<li class="active"><a href="#"><?php echo $_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLanguage;?>
</a></li>
				<?php }else{ ?>
				<li class="inactive"><a href="#"><?php echo $_smarty_tpl->tpl_vars['language']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vLanguage;?>
</a></li>
				<?php }?>
				<?php endfor; endif; ?>
				</ul>
			</div>
	<?php }?>
	<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
	<div class="centerpartbg" style="width:49%;float:left;">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Product Template
			<?php }else{ ?>
			Edit Product Template
			
			
			<?php }?> </div>
		
		
			<div class="add_ad_contentbox">
				<input type="hidden" name="iProductId" id="iProductId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				<input type="hidden" name="vImage" id="vImage_old" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
" />
				<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="en" />
				<?php }else{ ?>
				<input type="hidden" name="data[vLanguageCode]" id="vLanguageCode" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" />
				<?php }?>
                                 <div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Category</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Data[iCategoryId]" onchange="makesizediv(this.value)">
						
						<option value=''>--Select Product Category--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['db_producttype']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['db_producttype']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId'];?>
' <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->iCategoryId==$_smarty_tpl->tpl_vars['db_producttype']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId']){?>selected<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['db_producttype']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCategory'];?>
</option>
						<?php endfor; endif; ?>
					</select>
					
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vProductName" name="data[vProductName]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->vProductName;?>
<?php }?>" lang="*" title="Product Name" />
				</div>				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Product Image</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
					<?php }else{ ?>
					<input type="file" id="vImage"  name="vImage" title="Image" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
<?php }?>"<?php if ($_smarty_tpl->tpl_vars['data']->value->vImage==''){?> lang="*" <?php }?> onchange="CheckValidFile(this.value,this.name)"/>
					<?php }?>
				</div>
				<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['data']->value->vImage!=''){?>
					<div class="view_del_user">
						<div class="view_btn_user"><a href="#popfancy" id="fancyhref" class="view_btnimg">View</a></div>
						<div class="delete_user"><a href="#" onclick="ImageDelete('<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
','product');" class="delete_btnimg">Delete</a></div>
						
						<div style="display:none;">
							<div id="popfancy"><img src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
/product/<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
/1_<?php echo $_smarty_tpl->tpl_vars['data']->value->vImage;?>
"></div>
						</div>
				         </div>
					<?php }?><?php }?>
                                <div class="inputboxes">
				        <label for="textarea"><span class="red_star"></span> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tDescription" class="inputbox" title="Description" name="data[tDescription]" id="tDescription"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->tDescription;?>
<?php }?></textarea>
				</div>        
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Price</label>
					<span class="collan_dot">:</span>
					<input type="text" id="fPrice" name="Data[fPrice]" class="inputbox" title="fPrice"  lang="*" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->fPrice;?>
<?php }?>" onkeypress="return checkprise(event)">
				</div>								
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Product Template" class="submit_btn" title="Add Product" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Product Template" class="submit_btn" title="Edit Product" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
	<div class="centerpartbg" style="float:left;width:49%;margin-left:20px;min-height:100px;">
	<div class="pagetitle">SEO Search</div>
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					<label for="textfield"> Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vSeoTitle" name="data[vSeoTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->vSeoTitle;?>
<?php }?>" title="Seo Title" />
				</div>				
		                <div class="inputboxes">
				        <label for="textarea"> Keyword</label>
						<span class="collan_dot">:</span>
				        <input type="text" id="vSeoKeyword" name="data[vSeoKeyword]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->vSeoKeyword;?>
<?php }?>" title="Seo Keyword" />
				</div>
				<div class="inputboxes">
				        <label for="textarea"> Description</label>
						<span class="collan_dot">:</span>
				        <textarea value="" title="tSeoDescription" class="inputbox" title="Seo Description" name="data[tSeoDescription]" id="tSeoDescription"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['Data']->value->tSeoDescription;?>
<?php }?></textarea>
				</div>
			</div>
		
	</div>
	<div class="centerpartbg" style="float:right;width:49%;margin-right:6px;min-height:235px;margin-top:20px;">
	<div class="pagetitle">Size Selection</div>
	
		<div class="add_ad_contentbox" id="sizedivid">
				
				<?php if (count($_smarty_tpl->tpl_vars['sizelist']->value)>0&&$_smarty_tpl->tpl_vars['operation']->value!='add'){?>
				<div class="inputboxes" style="padding-bottom:0px;">
				<label for="textfield"></label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Width</label>
				<label for="textfield" style="width:100px;text-align:center;color:#2883B7;font-weight:bold;">Length</label>
				</div>
				<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sizelist']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
				<div class="inputboxes">
				<label for="textfield"><?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vTitle;?>
&nbsp;(<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vSize;?>
)</label>
				<span class="collan_dot">:</span>
				<input type="hidden" id="" name="SIZE[<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
][iSizeId]" value="<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iSizeId;?>
"/>
				<input type="text" id="Width<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" name="SIZE[<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
][iWidth]" class="inputbox" title="Width" style="width:100px;" value="<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iWidth;?>
" onkeypress="return checkprise(event)"/>
				<input type="text" id="Length<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
" name="SIZE[<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index'];?>
][iLength]" class="inputbox" title="Length" style="width:100px;margin-left:10px;" value="<?php echo $_smarty_tpl->tpl_vars['sizelist']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iLength;?>
" onkeypress="return checkprise(event)"/>
				</div>
				<?php endfor; endif; ?>
				<?php }else{ ?>
				<div style="text-align:center;color:#C44C22; font-size:14px; font-weight:bold;">No Size available for this category</div>
				<?php }?>

			</div>
	</div>
	</form>
	<div class="centerpartbg" style="width:50%;float:left;margin-top:20px;min-height:250px;margin-left:25%;">
		<div id="relaceformcolor">
		<div class="pagetitle" style="text-align:center;">
			Add Color Image
		</div>
		<form id="frmaddcolor" name="frmaddcolor" method="post" enctype="multipart/form-data"  action="">
			<input type="hidden" name="iProductId" id="iProductId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
" />
			<input type="hidden" name="type" id="type" value="color" />
			<input type="hidden" name="mode" id="mode" value="add" />
			<input type="hidden" name="lang" id="lang" value="<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
" />
			<div class="add_ad_contentbox">
				<div class="inputboxes">
					
					<label for="textfield"><span class="red_star">*</span> Color :</label>
					 <span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[iColorId]" lang="*" title="color">
						
						<option value=''>--Select Color--</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['color']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value='<?php echo $_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iColorId;?>
'><?php echo $_smarty_tpl->tpl_vars['color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColor;?>
</option>
						<?php endfor; endif; ?>
					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Front Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vFrontImage"  name="vFrontImage" title="Front Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Back Image</label>
					<span class="collan_dot">:</span>
					<input type="file" id="vBackImage"  name="vBackImage" title="Back Image" lang="*" onchange="CheckValidFile(this.value,this.name)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Datacolor[eStatus]">
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<input type="submit" value="Add Color Image" class="submit_btn" title="Add Color Image" onclick="return validate(document.frmaddcolor);"/>
				</div>
			</div>
		</form>
		</div>
	</div>
	
	<div class="centerpartbg" style="width:100%;float:left;margin-top:20px;" id="colorformadd">
		<div class="pagetitle">
			Color Image list
		</div>
		<?php if ($_smarty_tpl->tpl_vars['var_msg']->value!=''){?>
		<div class="status success" id="errormsgdiv">
			<p class="closestatus"><a href="javascript:void(0);" title="Close" onclick="hidemessage();">x</a></p>
			<p><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icons/icon_success.png" title="Success" /> <?php echo $_smarty_tpl->tpl_vars['var_msg']->value;?>
</p>
		</div>
		<div></div>
		<?php }?>
		
			<div class="add_ad_contentbox">
				<div class="administator_table">
			<form name="frmlist" id="frmlist"  action="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/search_action" method="post">
				<input  type="hidden" name="iProductId" value=""/>
				<table cellpadding="0" cellspacing="1" width="100%">
					<thead>
						<tr>
							<th>Color Name</th>
							<th>Color code</th>
							<th width="77px">Status</th>
							<th width="90px">Action</th>
						</tr>
					</thead>
					<?php if (count($_smarty_tpl->tpl_vars['data_color']->value)>0){?>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data_color']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']%2==0){?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table_sec', null, 0);?>
					<?php }else{ ?>
					<?php $_smarty_tpl->tpl_vars['class'] = new Smarty_variable('admin_antry_table', null, 0);?>
					<?php }?>
					<tbody class="<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
" style="font-size:13px;">
						<tr>
							<td><a href="javascript:void(0);" onclick="editform(<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
);"><?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColor;?>
</a></td>
							<td><?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vColorCode;?>
</td>											
							<td><?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->eStatus;?>
</td>
							<td><a href="javascript:void(0);" onclick="editform(<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
);"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_edit.png" alt="Edit" title="Edit"></a>
							<a href="javascript:void(0);"><img src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_delete.png" alt="Delete" title="Delete"  onclick="deletecommoncolor(<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductColorId;?>
,'<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
','product/deletecolor',<?php echo $_smarty_tpl->tpl_vars['data_color']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iProductId;?>
)"></a> </td>
						</tr>
					</tbody>
					<?php endfor; endif; ?>
					<?php }else{ ?>
					<tr>
						<td height="70px;" colspan="8" style="text-align:center; color:#C44C22; font-size:14px; font-weight:bold;">No record found.</td>
					</tr>
					<?php }?>
				</table>
			</form>
		</div>
			</div>
	</div>
	
	<div class="clear"></div>
	
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
         	
         	
$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});

$("#fancyhrefmain").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});
$("#fancyhrefback").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});
function ImageDelete(id,file1){
   
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wanted to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/deleteimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

function makesizediv(val)
{
	var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	if(val == '<?php echo $_smarty_tpl->tpl_vars['data']->value->iCategoryId;?>
')
	{
	var url = site_url+"product/makesizedefault?iCategoryId="+val+"&iProductId="+'<?php echo $_smarty_tpl->tpl_vars['data']->value->iProductId;?>
';	
	}
	else{
	var url = site_url+"product/makesize?iCategoryId="+val;
	}
	var pars = '';
	$.post(url+pars,
	    function(data) {
		$('#sizedivid').html(data);
	    });
}


function editform(id)
{
	//alert(id);
	var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	var url = site_url+"product/makeeditcolor?iProductColorId="+id+"&lang="+'<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
';	
	var pars = '';
	//alert(url+pars);
	$.post(url+pars,
	    function(data) {
		//alert(data);
		$('#relaceformcolor').html(data);
	    });
}
</script>
 
<?php }} ?>