<?php /* Smarty version Smarty-3.1.11, created on 2013-09-03 07:16:50
         compiled from "application/views/templates/account-dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:105410049051a9f39fba4a19-30879747%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b3a9c4ae8a349039759424c33b6ed692f2cebddb' => 
    array (
      0 => 'application/views/templates/account-dashboard.tpl',
      1 => 1377257095,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '105410049051a9f39fba4a19-30879747',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51a9f39feb1898_62086133',
  'variables' => 
  array (
    'msg' => 0,
    'UserDetails' => 0,
    'orderData' => 0,
    'site_url' => 0,
    'BillShipDetails' => 0,
    'state' => 0,
    'country' => 0,
    'ShippingDetails' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51a9f39feb1898_62086133')) {function content_51a9f39feb1898_62086133($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home2/zerninph/public_html/php/emblemax/system/libs/smarty/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			<div class="main-container col2-right-layout">
                
			 
			 <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
						            <div class="main container">
			<div class="slideshow_static">
                                            </div>
                <div class="main-inner">
                    
                    <div class="col-main">
                                                <div class="account-create">
    			<h2 class="pagetital subtit">My Dashboard</h2>		
			<div style="clear:both;"></div>
			<?php if ($_smarty_tpl->tpl_vars['msg']->value){?>
			<ul class="messages" style="margin-top: 15px;">
			    <li class="success-msg">
			      <ul>
				 <li>
				 <span><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</span>
				 </li>
			      </ul>
			    </li>
			</ul>
			<?php }?>	
            <div class="ordleftpart">
		  
		  <div class="col-main-wap span9">
		<div class="col-main">
		<div class="col-main-inner">
		<div class="my-account"><div class="dashboard">
    
        <div class="welcome-msg">
    <p class="hello"><strong>Hello, <?php echo $_smarty_tpl->tpl_vars['UserDetails']->value[0]->vFirstName;?>
&nbsp; <?php echo $_smarty_tpl->tpl_vars['UserDetails']->value[0]->vLastName;?>
</strong></p>
    <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information. Select a link below to view or edit information.</p>
</div>
    <div class="box-account box-recent">
    <div class="box-head">
        <h2>Recent Orders</h2>
        <a href="#">View All</a>    </div>
    <table id="shopping-cart-table" class="data-table cart-table" style="border-bottom:2px solid #EAEAEA">
                <col width="1" />
                <col />
                <col width="1" />
                                        <col width="1" />
                                        <col width="1" />
                            <col width="1" />
                                        <col width="1" />

                            <thead class="hidden-phone">
                    <tr class="">
                        <th class="" rowspan="1"><span class="nobr">Order Id</span></th>
                        <th width="70" rowspan="1" class=""><span class="nobr">Date Added</span></th>
                        <th width="200" rowspan="1" class=""><span class="nobr">Product</span></th>
                        <!--<th colspan="1"><span class="nobr">Model</span></th>-->
                        <th rowspan="1"><span class="nobr">Status</span></th>
                        <th colspan="1"><span class="nobr">Price</span></th>
                        <th rowspan="1"><span class="nobr">Details</span></th>
                    </tr>
                                    </thead>
                
                <tbody>
               <?php if (count($_smarty_tpl->tpl_vars['orderData']->value)>0){?>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['orderData']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<tr>
			   <td style="padding-bottom: 25px;"><?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vInvoiceNum'];?>
</td>
			   <td width="7"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['dAddedDate']);?>
</td>
			   <td width="200"><?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
</td>
			   <!--<td>PR - 3</td>-->
			   <td><?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['status'];?>
</td>
			   <td>$<?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fGrandTotal'];?>
</td>
			   <td><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/showOrderDetail?iOrderId=<?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iOrderId'];?>
" class="viewdet">View Details</a></td>
		       </tr>
                <?php endfor; endif; ?>
                <?php }else{ ?>
                <tr>
                        <td height="20px;" colspan="8" style="text-align:center; color:#38414B; font-size:14px; font-weight:bold;">No Records Found.</td>
                </tr>
                <?php }?>
                                </tbody>
            </table>
    <script type="text/javascript">decorateTable('my-orders-table')</script>
</div>
    <div class="box-account box-info">
        <div class="box-head">
            <h2>Account Information</h2>
        </div>
                        <div class="col2-set row-fluid show-grid">
    <div class="col-1 span6">
        <div class="box">
            <div class="box-title">
                <h3>Contact Information</h3>
                <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/myAccount">Edit</a>
            </div>
            <div class="box-content">
                <p>
                    <?php echo $_smarty_tpl->tpl_vars['UserDetails']->value[0]->vFirstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['UserDetails']->value[0]->vLastName;?>
<br />
                    <?php echo $_smarty_tpl->tpl_vars['UserDetails']->value[0]->vEmail;?>
<br />
                    <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/changepass">Change Password</a>
                </p>
            </div>
        </div>
    </div>
        <div class="col-2 span6">
        <!--<div class="box">
            <div class="box-title">
                <h3>Newsletters</h3>
                <a href="#">Edit</a>
            </div>
            <div class="box-content">
                <p>You are currently not subscribed to any newsletter.</p>
            </div>
        </div>-->
                    </div>
    </div>
        <div class="col2-set row-fluid show-grid">
    <div class="box">
        <div class="box-title">
            <h3>Address Book</h3>
            <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/address">Manage Addresses</a>
        </div>
        <div class="box-content">
            <div class="col-1 span6">
                <h4>Default Billing Address</h4>
		<?php if (count($_smarty_tpl->tpl_vars['BillShipDetails']->value)>'0'){?>
		    <address>
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingFirstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingLastName;?>
<br/>
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->tBillingStreetAddress1;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->tBillingStreetAddress2;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingCity;?>
<br />
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->iBillingStateId){?> <?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->iBillingCountryId){?> <?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingZipCode;?>
<br/>
			T:<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingTelephone;?>

			<?php if ($_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingFax!=''){?><br/>F:<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingFax;?>
<?php }?><br/>
			<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Edit Address</a>
		    <address>                    
		    <?php }else{ ?>
		    </address>
		    You have not set a default billing address.<br/>
		    <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Edit Address</a>
		    <?php }?>
                </address>
            </div>
            <div class="col-2 span6">
                <h4>Default Shipping Address</h4>
		<?php if (count($_smarty_tpl->tpl_vars['ShippingDetails']->value)>'0'){?>
		<address>
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingFirstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingLastName;?>
<br/>
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->tShippingStreetAddress1;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->tShippingStreetAddress2;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingCity;?>
<br />
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->iShippingStateId){?> <?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->iShippingCountryId){?> <?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingZipCode;?>
<br/>
			T:<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingTelephone;?>

			<?php if ($_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingFax!=''){?><br/>F:<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingFax;?>
<?php }?>
			<address>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Edit Address</a>
		       <?php }else{ ?>
		       </address>
		       You have not set a default Shipping address.<br/>
		       <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Edit Address</a>
		       <?php }?>
                </address>
            </div>
        </div>
    </div>
</div>
    </div>
        </div></div></div>
								</div>
							</div>
</div>
<div style="margin-top: 90px;"><?php echo $_smarty_tpl->getSubTemplate ("right_myAccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
</div>
</div>
                    </div>
					
                </div>				
				
									
								
            </div>
        </div>
			 
			 
        </div>
		
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
    //<![CDATA[
    var dataForm = new VarienForm('form-validate', true);
        //]]>
</script>
<script type="text/javascript">decorateDataList('narrow-by-list')</script>
<?php }} ?>