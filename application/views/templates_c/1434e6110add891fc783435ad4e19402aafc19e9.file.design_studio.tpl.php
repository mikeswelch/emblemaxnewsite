<?php /* Smarty version Smarty-3.1.11, created on 2013-11-21 08:59:22
         compiled from "application/views/templates/design_studio.tpl" */ ?>
<?php /*%%SmartyHeaderCode:71045004251d2ee017087b7-94861607%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1434e6110add891fc783435ad4e19402aafc19e9' => 
    array (
      0 => 'application/views/templates/design_studio.tpl',
      1 => 1385049556,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '71045004251d2ee017087b7-94861607',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51d2ee01b72a70_44260496',
  'variables' => 
  array (
    'custom_tshirt_path' => 0,
    'fancybox_path' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51d2ee01b72a70_44260496')) {function content_51d2ee01b72a70_44260496($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"Example Smarty Page"), 0);?>


<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
css/plugins.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
css/jquery.fancyProductDesigner.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
css/jquery.fancyProductDesigner-fonts.css" />

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['fancybox_path']->value;?>
lib/jquery-1.8.0.min.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
js/fabric.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
js/plugins.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		var designerBaseUrl = '<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
design_studio/';
	</script>

<script src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
js/jquery.fancyProductDesigner.min.js" type="text/javascript"></script>


<script type="text/javascript">
	    jQuery(document).ready(function(){
	    
	    	var yourDesigner = $('#clothing-designer').fancyProductDesigner({
	    		editorMode: false, 
	    		fonts: ['Arial', 'Fearless', 'Helvetica', 'Times New Roman', 'Verdana', 'Geneva'], 
	    		customTextParamters: {x: 210, y: 250, colors: false, removable: true, resizable: true, draggable: true, rotatable: true}
	    	}).data('fancy-product-designer');
	    	
	    	//print button
			$('#print-button').click(function(){
				yourDesigner.print();
				return false;
			});
			
			//create an image
			$('#image-button').click(function(){
				var image = yourDesigner.createImage();
				return false;
			});
			
			//create a pdf with jsPDF
			$('#pdf-button').click(function(){
				var image = new Image();
				image.src = yourDesigner.getProductDataURL('jpeg');
				image.onload = function() {
					var doc = new jsPDF();
					doc.addImage(this.src, 'JPEG', 0, 0, this.width * 0.2, this.height * 0.2);
					doc.save('Product.pdf');
				}		
				return false;
			});
			
			//checkout button with getProduct()
			$('#checkout-button').click(function(){
				var product = yourDesigner.getProduct();
				console.log(product);
				return false;
			});
			
			//event handler when the price is changing
			$('#clothing-designer')
			.bind('priceChange', function(evt, price, currentPrice) {
				$('#thsirt-price').text(currentPrice);
			});
		
			//recreate button
			$('#recreation-button').click(function(){
				var fabricJSON = JSON.stringify(yourDesigner.getFabricJSON());
				$('#recreation-form input:first').val(fabricJSON).parent().submit();
				return false;
			});
			
			//click handler for input upload
			$('#upload-button').click(function(){
				$('#design-upload').click();
				return false;
			});
			
			//save image on webserver
			$('#save-image-php').click(function() {
				$.post( "php/save_image.php", { base64_image: yourDesigner.getProductDataURL()} );
			});
			
			//send image via mail
			$('#send-image-mail-php').click(function() {
				$.post( "php/send_image_via_mail.php", { base64_image: yourDesigner.getProductDataURL()} );
			});
			
			//upload image
			/*document.getElementById('design-upload').onchange = function (e) {
				if(window.FileReader) {
					var reader = new FileReader();
			    	reader.readAsDataURL(e.target.files[0]); 
			    	reader.onload = function (e) {
			    		var image = new Image;
			    		image.src = e.target.result;
			    		image.onload = function() {
				    		yourDesigner.addElement('image', e.target.result, 'my custom design', {colors: $('#colorizable').is(':checked') ? '#000000' : false, zChangeable: true, removable: true, draggable: true, resizable: true, rotatable: true, x: 50, y: 50});  	
			    		};	    		               
					};
				}
				else {
					alert('FileReader API is not supported in your browser, please use Firefox, Safari, Chrome or IE10!')
				}
			};*/
	    });
    </script>	
    





<section id="midcontentpart">
	

	<div class="mainpart">
		
		<div id="main-container" class="container">
          	<div id="clothing-designer">
          		<div class="fpd-product" title="Shirt Front" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/front/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/front/base.png" title="Base" data-parameters='{"x": 325, "y": 329, "colors": "#990000", "price": 20}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/front/shadows.png" title="Shadow" data-parameters='{"x": 325, "y": 329}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/front/body.png" title="Hightlights" data-parameters='{"x": 322, "y": 137}' />
			  		<span title="Any Text" data-parameters='{"boundingBox": "Base", "x": 326, "y": 232, "zChangeable": true, "removable": true, "draggable": true, "rotatable": true, "resizable": true, "colors": "#000000"}' >Default Text</span>
			  		<div class="fpd-product" title="Shirt Back" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/back/preview.png">
		    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/back/base.png" title="Base" data-parameters='{"x": 317, "y": 329, "colors": "Base", "price": 20}' />
		    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/back/body.png" title="Hightlights" data-parameters='{"x": 333, "y": 119}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/yellow_shirt/back/shadows.png" title="Shadow" data-parameters='{"x": 318, "y": 329}' />
					</div>
				</div>
          		<div class="fpd-product" title="Sweater" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/sweater/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/sweater/basic.png" title="Base" data-parameters='{"x": 332, "y": 311, "colors": "#D5D5D5,#990000,#cccccc", "price": 20}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/sweater/highlights.png" title="Hightlights" data-parameters='{"x": 332, "y": 311}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/sweater/shadow.png" title="Shadow" data-parameters='{"x": 332, "y": 309}' />
				</div>
				<div class="fpd-product" title="Scoop Tee" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/scoop_tee/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/scoop_tee/basic.png" title="Base" data-parameters='{"x": 314, "y": 323, "colors": "#98937f, #000000, #ffffff", "price": 15}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/scoop_tee/highlights.png" title="Hightlights" data-parameters='{"x":308, "y": 316}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/scoop_tee/shadows.png" title="Shadow" data-parameters='{"x": 308, "y": 316}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/scoop_tee/label.png" title="Label" data-parameters='{"x": 314, "y": 140}' />
				</div>
				<div class="fpd-product" title="Hoodie" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/hoodie/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/hoodie/basic.png" title="Base" data-parameters='{"x": 313, "y": 322, "colors": "#850b0b", "price": 40}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/hoodie/highlights.png" title="Hightlights" data-parameters='{"x": 311, "y": 318}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/hoodie/shadows.png" title="Shadow" data-parameters='{"x": 311, "y": 321}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/hoodie/zip.png" title="Zip" data-parameters='{"x": 303, "y": 216}' />
				</div>
				<div class="fpd-product" title="Shirt" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shirt/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shirt/basic.png" title="Base" data-parameters='{"x": 327, "y": 313, "colors": "#6ebed5", "price": 10}' />
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shirt/collar_arms.png" title="Collars & Arms" data-parameters='{"x": 326, "y": 217, "colors": "#000000"}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shirt/highlights.png" title="Hightlights" data-parameters='{"x": 330, "y": 313}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shirt/shadow.png" title="Shadow" data-parameters='{"x": 327, "y": 312}' />
			  		<span title="Any Text" data-parameters='{"boundingBox": "Base", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "colors": "#000000"}' >Default Text</span>
				</div>
				<div class="fpd-product" title="Short" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shorts/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shorts/basic.png" title="Base" data-parameters='{"x": 317, "y": 332, "colors": "#81b5eb", "price": 15}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shorts/highlights.png" title="Hightlights" data-parameters='{"x": 318, "y": 331}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shorts/pullstrings.png" title="Pullstrings" data-parameters='{"x": 307, "y": 195, "colors": "#ffffff"}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shorts/midtones.png" title="Midtones" data-parameters='{"x": 317, "y": 332}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/shorts/shadows.png" title="Shadow" data-parameters='{"x": 320, "y": 331}' />
				</div>
				<div class="fpd-product" title="Basecap" data-thumbnail="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/cap/preview.png">
	    			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/cap/basic.png" title="Base" data-parameters='{"x": 318, "y": 311, "colors": "#ededed", "price": 5}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/cap/highlights.png" title="Hightlights" data-parameters='{"x": 309, "y": 300}' />
			  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/cap/shadows.png" title="Shadows" data-parameters='{"x": 306, "y": 299}' />
				</div>
		  		<div class="fpd-design">
		  			<div class="fpd-category" title="Swirls">
			  			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/swirl.png" title="Swirl" data-parameters='{"zChangeable": true, "x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10, "boundingBox": "Base", "scale": 1}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/swirl2.png" title="Swirl 2" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/swirl3.png" title="Swirl 3" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/heart_blur.png" title="Heart Blur" data-parameters='{"x": 215, "y": 200, "colors": "#bf0200", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/converse.png" title="Converse" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/crown.png" title="Crown" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/men_women.png" title="Men hits Women" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "boundingBox": "Base"}' />
		  			</div>
		  			<div class="fpd-category" title="Retro">
			  			<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/retro_1.png" title="Retro One" data-parameters='{"x": 210, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.25, "price": 7, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/retro_2.png" title="Retro Two" data-parameters='{"x": 193, "y": 180, "colors": "#ffffff", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.46, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/retro_3.png" title="Retro Three" data-parameters='{"x": 240, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.25, "price": 8, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/heart_circle.png" title="Heart Circle" data-parameters='{"x": 240, "y": 200, "colors": "#007D41", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "scale": 0.4, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/swirl.png" title="Swirl" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 10, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/swirl2.png" title="Swirl 2" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true, "price": 5, "boundingBox": "Base"}' />
				  		<img src="<?php echo $_smarty_tpl->tpl_vars['custom_tshirt_path']->value;?>
images/designs/swirl3.png" title="Swirl 3" data-parameters='{"x": 215, "y": 200, "colors": "#000000", "removable": true, "draggable": true, "rotatable": true, "resizable": true}' />
				  	</div>					  		
		  		</div>
		  	</div>

    	</div>

	</div>
</section>





<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>