<?php /* Smarty version Smarty-3.1.11, created on 2014-02-27 09:13:04
         compiled from "application/views/templates/admin/product/scdetail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:46611241451b1ed2ef36c05-82954628%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8bf78b09db475ea88cc96a6b0f25736acfafb393' => 
    array (
      0 => 'application/views/templates/admin/product/scdetail.tpl',
      1 => 1393510344,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '46611241451b1ed2ef36c05-82954628',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51b1ed2f18b7f2_73292347',
  'variables' => 
  array (
    'all_size_details' => 0,
    'color_details' => 0,
    'final_all_colorsizes' => 0,
    'final_all_colorsizes_price' => 0,
    'all_quantity_range' => 0,
    'selected_quantity_range_id' => 0,
    'decoration' => 0,
    'range_color' => 0,
    'foo' => 0,
    'editDecorationNew' => 0,
    'embroideryPriceNew' => 0,
    'all_product' => 0,
    'product_id' => 0,
    'admin_url' => 0,
    'all_sizeid' => 0,
    'admin_image_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51b1ed2f18b7f2_73292347')) {function content_51b1ed2f18b7f2_73292347($_smarty_tpl) {?><form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="" >
  <input type="hidden" name="type" value="sizecolordetail"/>
<table width="95%" style="margin: 15px 0 0 0;">
  <tr>
    <th></th>
    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_size_details']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"><?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSize'];?>
</th>
    <?php endfor; endif; ?>
  </tr>
  
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['color_details']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
    <tr id="col_<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
">
      <td style="padding:0 15px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;"><?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColor'];?>
</td>
          <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_size_details']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
            <?php if (in_array($_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'],$_smarty_tpl->tpl_vars['final_all_colorsizes']->value[$_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId']])){?>
              <td><input type="checkbox" name="sizedetail" value="sizedetail" id="color-<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
-size-<?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
" class="size_detail" checked><br/>
		  <input type="text" class="simple" name="Data[<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
][<?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
]" id="color-<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
-size-<?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
-o" style="width: 60px;background-color:#fff;" value="<?php echo $_smarty_tpl->tpl_vars['final_all_colorsizes_price']->value[$_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId']][$_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId']]['price'];?>
"/></td>
            <?php }else{ ?>
              <td><input type="checkbox" name="sizedetail" value="sizedetail" id="color-<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
-size-<?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
" class="size_detail"><br/>
		  <input type="text" class="simple" name="Data[<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
][<?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
]" id="color-<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
-size-<?php echo $_smarty_tpl->tpl_vars['all_size_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iSizeId'];?>
-o" style="width: 60px;background-color:#D1D1D1;" disabled="disabled"/></td>
            <?php }?>
          <?php endfor; endif; ?>
    </tr>
    <tr >
      <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_size_details']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
      <td></td>
      <?php endfor; endif; ?>
      <td><?php if ($_smarty_tpl->getVariable('smarty')->value['section']['i']['index']!=0){?><input type="checkbox" name="copy[<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
]" value="<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']-1]['iColorId'];?>
" onclick="duplicate(<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']-1]['iColorId'];?>
,<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iColorId'];?>
);" id="dup_<?php echo $_smarty_tpl->tpl_vars['color_details']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']-1]['iColorId'];?>
">Copy Previous<?php }?></td></tr>
  <?php endfor; endif; ?>
</table>
<br/>

<div class="" style="width:100%;">
        <div id="relaceformcolor">
	  <div class="pagetitle" style="text-align:center;">
	  <select id="iQuantityRangeId" name="iQuantityRangeId" style="margin: 10px;float: left" title="Quantity Range" lang="*" onchange="load_decoration(this.value);">
	    <option value="">-------Select Quantity Range-----</option>
	    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_quantity_range']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		<option value="<?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iQuantityRangeId'];?>
" <?php if ($_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iQuantityRangeId']==$_smarty_tpl->tpl_vars['selected_quantity_range_id']->value){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vQuantityRange'];?>
</option>
	    <?php endfor; endif; ?>
	  </select>
	  <span style="margin-right: 300px;">Screen Printing Services </span>                       
	  </div>
	   <div id="ajax-load-deco" >
	   <table width="95%" id="Decore" disabled="disabled" class="divtext">		
	    <tr>
	      <th></th>
	      <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		  <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"><?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocation'];?>
</th>
	      <?php endfor; endif; ?>
		 <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th>
	    </tr>
	    <!-- <?php echo count($_smarty_tpl->tpl_vars['range_color']->value);?>
 -->
	    
	    <?php $_smarty_tpl->tpl_vars['foo'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['foo']->step = 1;$_smarty_tpl->tpl_vars['foo']->total = (int)ceil(($_smarty_tpl->tpl_vars['foo']->step > 0 ? count($_smarty_tpl->tpl_vars['range_color']->value)-1+1 - (0) : 0-(count($_smarty_tpl->tpl_vars['range_color']->value)-1)+1)/abs($_smarty_tpl->tpl_vars['foo']->step));
if ($_smarty_tpl->tpl_vars['foo']->total > 0){
for ($_smarty_tpl->tpl_vars['foo']->value = 0, $_smarty_tpl->tpl_vars['foo']->iteration = 1;$_smarty_tpl->tpl_vars['foo']->iteration <= $_smarty_tpl->tpl_vars['foo']->total;$_smarty_tpl->tpl_vars['foo']->value += $_smarty_tpl->tpl_vars['foo']->step, $_smarty_tpl->tpl_vars['foo']->iteration++){
$_smarty_tpl->tpl_vars['foo']->first = $_smarty_tpl->tpl_vars['foo']->iteration == 1;$_smarty_tpl->tpl_vars['foo']->last = $_smarty_tpl->tpl_vars['foo']->iteration == $_smarty_tpl->tpl_vars['foo']->total;?>
	     <tr id="row-<?php echo $_smarty_tpl->tpl_vars['foo']->value;?>
">
		  <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;"><?php echo $_smarty_tpl->tpl_vars['range_color']->value[$_smarty_tpl->tpl_vars['foo']->value][0]->vNumberColor;?>
</td>
		  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
		  <td><input type="text" class="simple" name="Decoration[<?php echo $_smarty_tpl->tpl_vars['range_color']->value[$_smarty_tpl->tpl_vars['foo']->value][0]->iNumberColorsId;?>
][<?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId'];?>
]" id="vartical_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['j']['index']+1;?>
<?php echo $_smarty_tpl->tpl_vars['foo']->value+1;?>
" style="width: 60px;" value="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['range_color']->value[$_smarty_tpl->tpl_vars['foo']->value][0]->iNumberColorsId;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['editDecorationNew']->value[$_tmp1][$_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId']];?>
" id="size"/></td>
		  <?php endfor; endif; ?>
		  <td><input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['foo']->value+1;?>
" class="allow_color" checked/></td>
		</tr>
	    <?php }} ?>
	    <!-- 
	    <tr id="row-emb">
		 <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">Embroidery</td>
		  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
		  <td><input type="text" class="simple" name="embroidery[<?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId'];?>
]" id="emb_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['j']['index']+1;?>
" style="width: 60px;" value="<?php if ($_smarty_tpl->tpl_vars['embroideryPriceNew']->value[$_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId']]!=0.00){?><?php echo $_smarty_tpl->tpl_vars['embroideryPriceNew']->value[$_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId']];?>
<?php }?>"  /></td>
		  <input type="hidden" class="simple" name="iPrintLocationId[]" style="width: 60px;" value="<?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId'];?>
" />
		  <?php endfor; endif; ?>
		  <td><input type="checkbox" id="embroidery" class="embroidery" checked/></td>
	     </tr>
	    <tr>
	      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"></td>
	    </tr>
	    <tr>
		
		<!-- <th style="float:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th> -->
	      <td class="allow_text">Allow Location</td>
	      <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>		  
	      <td><div class="check_box"><input type="checkbox" id="vartical_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['j']['index']+1;?>
" class="allow_colors" onclick="testignofcheckbox(this.id)"/ ></div></td>
	      <?php endfor; endif; ?>
	      <td></td>
	    </tr>
	  </table>
	  </div>
	  <div style="clear: both;"></div>
	  <br><br>	  
	 <input type="checkbox" name="CopyDecoration" value="yes"  id="CopyDecoration">Want to copy form Another Template
	  <select id="pro_temp" name="copy_pro_temp" style="display: none;">
	    <option value="">-------Select Product Template-----</option>
	    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_product']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		    <?php if ($_smarty_tpl->tpl_vars['all_product']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId']!=$_smarty_tpl->tpl_vars['product_id']->value){?>
		      <option value="<?php echo $_smarty_tpl->tpl_vars['all_product']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
"><?php echo $_smarty_tpl->tpl_vars['all_product']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vUrlText'];?>
</option>
		    <?php }?>
	    <?php endfor; endif; ?>
	  </select>
	  <br><br>
	  <input type="checkbox" name="CopyQuantityRange" value="yes"  id="CopyQuantityRange" style="margin-left: 35px;">Want to copy form Another Quantity Range	  
	  <select id="qty_temp" name="copy_qty_temp" style="display: none;">
	    <option value="">-------Select Quantity Range-----</option>	    
	    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_quantity_range']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>	   
		<option value="<?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iQuantityRangeId'];?>
" ><?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vQuantityRange'];?>
</option>		
	    <?php endfor; endif; ?>    
	  </select>  
        </div>
	   <div class="btnmaincheck">
	 <input type="submit" name="color_deatil" value="save" class="submit_btn" onclick="return validate(document.frmadd);"/>
	 <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a></div>
	   <br><br><br><br>
	   
	   <!-- All Are working perect-->
	   <!-- Embrodary codign start -->
	   
	   
	   
	  <div class="pagetitle" style="text-align:center;">
		<select id="iQuantityRangeId_Embrodary" name="Embrodary_iQuantityRangeId" style="margin: 10px;float: left" title="Quantity Range" lang="*" onchange="loadEmrodary(this.value);">
		<option value="">----Embrodary Quantity Range-----</option>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_quantity_range']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		 <option value="<?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iQuantityRangeId'];?>
" <?php if ($_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iQuantityRangeId']==$_smarty_tpl->tpl_vars['selected_quantity_range_id']->value){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vQuantityRange'];?>
</option>
		<?php endfor; endif; ?>
	   </select>
	   <span style="margin-right: 300px;">Embroidery Services </span>
	   
	    <div id="ajax-load-embrodary" >
		<table width="95%" id="Embrodary_decoration" disabled="disabled" class="divtext">		
	    <tr>
	      <th></th>
	      <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		  <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"><?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocation'];?>
</th>
	      <?php endfor; endif; ?>
		 <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th>
	    </tr>
	    <!-- <?php echo count($_smarty_tpl->tpl_vars['range_color']->value);?>
 -->
	 
	    
	    <tr id="row-emb">
		 <td width="80" style="padding:0 25px 0 0; text-align:right; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#002f5f;">Embroidery</td>
		  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
		  <td><input type="text" class="simple" name="embroidery[<?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId'];?>
]" id="emb_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['j']['index']+1;?>
" style="width: 60px;" value="<?php if ($_smarty_tpl->tpl_vars['embroideryPriceNew']->value[$_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId']]!=0.00){?><?php echo $_smarty_tpl->tpl_vars['embroideryPriceNew']->value[$_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId']];?>
<?php }?>"  /></td>
		  <input type="hidden" class="simple" name="iPrintLocationId[]" style="width: 60px;" value="<?php echo $_smarty_tpl->tpl_vars['decoration']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]['iPrintLocationId'];?>
" />
		  <?php endfor; endif; ?>
		  <td><input type="checkbox" id="embroidery" class="embroidery" checked/></td>
	     </tr>
	    <tr>
	      <th style="text-align:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;"></td>
	    </tr>
	    
	    <tr>		
		<!-- <th style="float:left; font-weight:normal; font-size:14px; font-family: Arial,Helvetica,sans-serif; color:#9e053b;">Allow Color</th> -->
	      <td class="allow_text">Allow Location</td>
	      <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['decoration']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>		  
	      <td><div class="check_box"><input type="checkbox" id="embrodary_<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['j']['index']+1;?>
" class="allow_colors1" onclick="embrodaryCheckBox(this.id)"/ ></div></td>
	      <?php endfor; endif; ?>
	      <td></td>
	    </tr>
	  </table>	
	  </div>
	 	 <div style="clear: both;"></div>
	  <br><br>	  
	 <input type="checkbox" name="CopyEmbrodary" value="yes"  id="CopyEmbrodary">
	   <span style="font-family: 'latoblack';font-size: 13px;line-height: 16px;color: #000;">Want to copy form Another Template</span>
	  <select id="embr_pro_temp" name="copy_embr_pro_temp" style="display: none;margin-top: 11px;">
	    <option value="">-------Select Product Template-----</option>
	    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_product']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		    <?php if ($_smarty_tpl->tpl_vars['all_product']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId']!=$_smarty_tpl->tpl_vars['product_id']->value){?>
		      <option value="<?php echo $_smarty_tpl->tpl_vars['all_product']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
"><?php echo $_smarty_tpl->tpl_vars['all_product']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vUrlText'];?>
</option>
		    <?php }?>
	    <?php endfor; endif; ?>
	  </select>  
	 <br>
	  
	  <input type="checkbox" name="CopyEmrodaryQuantityRange" value="yes"  id="CopyEmrodaryQuantityRange" style="margin-left: 18%;">
	    <span style="font-family: 'latoblack';font-size: 13px;line-height: 16px;color: #000;">Want to copy form Another Quantity Range</span>
	  <select id="Embr_qty_temp" name="copy_embr_qty_temp" style="display: none;margin-top: 10px;">
	    <option value="">-------Select Quantity Range-----</option>	    
	    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['all_quantity_range']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>	   
		<option value="<?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iQuantityRangeId'];?>
" ><?php echo $_smarty_tpl->tpl_vars['all_quantity_range']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vQuantityRange'];?>
</option>		
	    <?php endfor; endif; ?>    
	  </select>  

	  


	    

	   

	    
	    

	    <div class="btnmaincheck">
		<input type="submit" name="color_deatil" value="save" class="submit_btn" onclick="return validate(document.frmadd);"/>
		<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
product/productlist" style="text-decoration:none;" class="cancel_btn">Cancel</a>
	    </div>   
	  </div>
	  
</form>
</div>



<script>
   /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: function is used to show text box when user click on  vertical checkbox
     */   
   function embrodaryCheckBox(id){    
	var str=id;
	var text=str.substring(10);	
     var id_check=(document.getElementById(id).checked);	
	var someObj={};	
	  var embroidery_checkbox=(document.getElementById('embroidery').checked);
	   if (id_check && embroidery_checkbox){
		 $("#emb_"+text).removeAttr("disabled");
		 $("#emb_"+text).css({'background-color': '#fff'})		
	   }else{
		 $("#emb_"+text).attr("disabled");
		 $("#emb_"+text).attr('value','');
		 $("#emb_"+text).css({'background-color': '#D1D1D1'})
	   }
	 }  
  function testignofcheckbox(id){    
	var str=id;	
	var text=str.substring(9);	
     var id_check=(document.getElementById(id).checked);
	
	var someObj={};
	someObj.fruitsGranted=[];
	someObj.fruitsDenied=[];	
	$(".allow_color:input:checkbox").each(function(){
	    var $this = $(this);	
	    if($this.is(":checked")){
		   someObj.fruitsGranted.push($this.attr("id"));
	    }else{
		   someObj.fruitsDenied.push($this.attr("id"));
	    }
	});	
	var all_cheked_checkbox=someObj.fruitsGranted;	
	var length=all_cheked_checkbox.length;
	
	 for(i=0;i<length;i++){	
	  var another_cheked=(document.getElementById(all_cheked_checkbox[i]).checked); //1	 
	  var create_id=all_cheked_checkbox[i];// 1
	  var current_checkbox=id; //vartical_1
	  var check_current=(document.getElementById(current_checkbox).checked);	 
	    if (check_current && another_cheked) {		
		var str=id;	
		var current_check=str.substring(9);		   //1
		var another=all_cheked_checkbox[i];	  
		 $("#vartical_"+current_check+another).removeAttr('disabled');		 
		 $("#vartical_"+current_check+another).css({'background-color': '#fff'})    
		 $("input[class=allow_color]").attr('disabled',false);
	    }else{
		  var str=id;	
		  var current_check=str.substring(9);		   //1
		  var another=all_cheked_checkbox[i];		  
		 $("#vartical_"+current_check+another).attr("disabled", true);
		 $("#vartical_"+current_check+another).attr('value','');
		 $("#vartical_"+current_check+another).css({'background-color': '#D1D1D1'})    
		 $("input[class=allow_color]").attr('disabled',false);
	    }  
	 }
	 /*
	  var embroidery_checkbox=(document.getElementById('embroidery').checked);
	   if (id_check && embroidery_checkbox){
		 $("#emb_"+text).removeAttr("disabled");
		 $("#emb_"+text).css({'background-color': '#fff'})		
	   }else{
		 $("#emb_"+text).attr("disabled");
		 $("#emb_"+text).attr('value','');
		 $("#emb_"+text).css({'background-color': '#D1D1D1'})
	   }
	   */
  }
  
</script>
  <script> 
	   var count = 0;
	   var emptyLenght = $("#row-emb").find('input[type=text]:empty').length;	   
	   $("#row-emb input:text").each(function(){
	    
		if (this.value == '') {
		  count++;
		}
	   });
	  console.log(count);
   	   if (count == emptyLenght) {
		$("#row-emb input:checkbox").attr('checked', false);
		$("#row-emb").find("input").attr("disabled", true);
		$("#row-emb").find("input").css({'background-color':'#D1D1D1'})
		$("input[class=embroidery]").attr('disabled',false);		
		//$("input[class=embroidery]").attr('disabled',false);	
	   }   
    var myStringArray = '<?php echo json_encode($_smarty_tpl->tpl_vars['all_sizeid']->value);?>
';
        
     // alert(myStringArray+'===='+myStringArray.length);
    $('.size_detail').change(function(){
      var id = $(this).attr('id');	 
      if($(this).is(':checked')){
	   $("input[id="+id+"-o]").attr('disabled',false);
	   $("input[id="+id+"-o]").css({'background-color':'#fff'});
	   }else{
         $("input[id="+id+"-o]").val('');
         $("input[id="+id+"-o]").attr('disabled',true);
          $("input[id="+id+"-o]").css({'background-color': '#D1D1D1'})
      }
    });
    $( document ).ready(function() {	 
	 var ele=[];
	 $('input:text[id^="color-"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	 var all_textboxid=ele;
	 var length=all_textboxid.length; 
	 //var test=$("#"+all_textboxid[3]).val();
	 for(i=0;i<length;i++){
		var textboxid=(all_textboxid[i]); //color-45-size-10-o
		var makecheckboxid=textboxid.substr(0,16);   // color-45-size-10
		if ($("#"+all_textboxid[i]).val()!="0.00" && $("#"+all_textboxid[i]).val() != ''){
		  $("#"+makecheckboxid).attr('checked', true);
		  $("#"+textboxid).removeAttr("disabled",false);
		  $("#"+textboxid).css({'background-color': '#fff'}); 
		}else{	   
		  $("#"+makecheckboxid).attr('checked',false);
		  $("#"+textboxid).attr('disabled', true);
		  $("#"+textboxid).val('');
		  $("#"+textboxid).css({'background-color': '#D1D1D1'}); 
		}
	 } 
    });   
    
    $( document ).ready(function() {
	 var rowCount = $('#Decore tr').length;	 
	 for (var i = 1; i < rowCount; i++) {
	   $emptyLenght = $("#row-"+i).find('input[type=text]:empty').length;
	   
	   var count = 0;
	   $("#row-"+i+" input:text").each(function(){
		if (this.value == '') {
		  count++;
		}
	   });
	   if (count == $emptyLenght){
		$("#row-"+i+" input:checkbox").attr('checked', false);
		$("#row-"+i).find("input").attr("disabled", true);
		$("#row-"+i).find("input").css({'background-color': '#D1D1D1'})
		$("input[class=allow_color]").attr('disabled',false);
	   }
	 }
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: get all the vertical checkbox's id
     */   
    $( document ).ready(function() {	 
	 var someObj={};
	 someObj.fruitsGranted=[];
	 someObj.fruitsDenied=[];	
	   $(".allow_color:input:checkbox").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.fruitsGranted.push($this.attr("id"));
		  }else{
			 someObj.fruitsDenied.push($this.attr("id"));
		  }
	   });	
	
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : check and unchekck the horizontal checkbox
     */    
    $(document ).ready(function(){
	   var ele=[];
	   $('input:checkbox[id^="vartical_"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_checkboxid=ele;
	   var length=all_checkboxid.length;
	   //alert(all_checkboxid);	   
	   for(i=0;i<length;i++){
		var text=[];
	       $('input:text[id^="vartical_"]').each(function(){
		    var $this=  $(this);
		    text.push($this.attr("id"));
		    
		   });
	     }
		text.sort();		  
		  var all_text=text;
		  var textlength=all_text.length;		  
		  for(j=0;j<textlength;j++){		    
		    if ($("#"+all_text[j]).val()!=""){			
			 var current_textbox=all_text[j].substr(9,1);   // vartical_21 return 2			
			 $("#vartical_"+current_textbox).attr('checked', true);
		    }
		  }
		  
		  var emb=[];
	   $('input:checkbox[id^="embrodary_"]').each(function(){
	    var $this=  $(this);
	    emb.push($this.attr("id"));	    
	   });
	   emb.sort();
	   var all_embrodaryCheckbox=emb;
	   var embrodaryLength=all_embrodaryCheckbox.length;
	  
	   for(i=0;i<embrodaryLength;i++){
		var embtext=[];
	       $('input:text[id^="emb_"]').each(function(){
		    var $this=  $(this);
		    embtext.push($this.attr("id"));
		    
		   });
	     }
		embtext.sort();		  
		  var all_embText=embtext;
		  var emblength=all_embText.length;		  
		  for(j=0;j<emblength;j++){		    
		    if ($("#"+all_embText[j]).val()!=""){			
			 var current_embbox=all_embText[j].substr(4,1);  
			 $("#embrodary_"+current_embbox).attr('checked', true);			 
		    }
		  } 
		  
		  
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : check and unchekck the horizontal checkbox for embroiedary 
     */       
    $(document ).ready(function(){
	  var ele=[];
	   $('input:checkbox[id^="embrodary_"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_checkboxid=ele;
	   var length=all_checkboxid.length;	   
	   for(i=0;i<length;i++){
		var text=[];
	       $('input:text[id^="emb_"]').each(function(){
		    var $this=  $(this);
		    text.push($this.attr("id"));		    
		  });
	   }
	   var all_text=text;	   
	   var textlength=all_text.length;
	   for(j=0;j<textlength;j++){		    
		if ($("#"+all_text[j]).val()!=""){
		  var current_textbox=all_text[j].substr(4,1);   // vartical_21 return 2		  
		  $("#embrodary_"+current_textbox).attr('checked', true);
		}
	   }	   
   });   
    
    $( document ).ready(function() {
	 var someObj={};
	 someObj.fruitsGranted=[];
	 someObj.fruitsDenied=[];	
	   $(".simple:input:text").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.fruitsGranted.push($this.attr("id"));
		  }else{
			 someObj.fruitsDenied.push($this.attr("id"));
		  }
	   });	   
	   var all_cheked_checkbox=someObj.fruitsGranted;	   
	   var length=all_cheked_checkbox.length;
	   
	   for(i=0;i<length;i++){
		$checked_id=all_cheked_checkbox[i];
		$("#vartical_"+$checked_id).attr('checked', true);		
	   } 
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : disabled or enabled text box for all colors
     */    
    $( document ).ready(function(){ 
	 var ele=[];
	   $('input:text[id^="varti"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_textboxid=ele;
	   var length=all_textboxid.length;
	   
	   //alert(length);
	   for(i=0;i<length;i++){
	   var str=all_textboxid[i];//'vartical_31'
	   var horizontal_textbox=str.substring(10); //vartical_21 return 1
	   var vertical_textbox=str.substr(9,1);   // vartical_21 return 2
	   //alert(str);	   
	   var check_horizontal_textbox=(document.getElementById(horizontal_textbox).checked);	   
	   var vertical="vartical_"+vertical_textbox;
	   var check_vertical_textbox=(document.getElementById(vertical).checked);	   
		if (check_horizontal_textbox && check_vertical_textbox){		
		  $("#vartical_"+vertical_textbox+horizontal_textbox).removeAttr("disabled",false);
		  $("#vartical_"+str).css({'background-color': '#fff'})    		
		}else{		
		  $("#vartical_"+vertical_textbox+horizontal_textbox).attr("disabled",true);
		  $("#vartical_"+vertical_textbox+horizontal_textbox).attr('value','');
		  $("#vartical_"+vertical_textbox+horizontal_textbox).css({'background-color': '#D1D1D1'})    
		  
		}	   
	  }  
    });
    /*
      Created  Date:-
      Created By:
      Modified By: Nikhil Detroja
      Date:10-01-2013
      Purpose: on page load : disabled or enabled text box for all colors
     */
    $( document ).ready(function(){
	  var ele=[];
	  $('input:text[id^="emb_"]').each(function(){
	    var $this=  $(this);
	    ele.push($this.attr("id"));	    
	   });
	   ele.sort();
	   var all_textboxid=ele;	   
	   var length=all_textboxid.length;
	   for(i=0;i<length;i++){
		var embroidery_checkbox=(document.getElementById('embroidery').checked); 
		var textboxid=all_textboxid[i].substr(4,1); //vartical_21 return 2		
		var vertical="embrodary_"+textboxid;	  
		var check_vertical_textbox=(document.getElementById(vertical).checked);
		
		if (embroidery_checkbox && check_vertical_textbox){   
		  $("#emb_"+textboxid).removeAttr("disabled",false);
		  $("#emb_"+textboxid).css({'background-color': '#fff'})  
		}else{
		   $("#emb_"+textboxid).attr("disabled",true);
		   $("#emb_"+textboxid).attr('value','');
		   $("#emb_"+textboxid).css({'background-color': '#D1D1D1'})   
		}
	   } 
    });   
    
    $('.allow_color').change(function() {	 
	 var id = $(this).attr('id');	 
	 var check_currentid=(document.getElementById(id).checked);
	 var someObj={};
	 someObj.fruitsGranted=[];
	 someObj.fruitsDenied=[];	
	   $(".allow_colors:input:checkbox").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.fruitsGranted.push($this.attr("id"));
		  }else{
			 someObj.fruitsDenied.push($this.attr("id"));
		  }
	  });	
	 var all_cheked_checkbox=someObj.fruitsGranted; //all bottom of the checkbox 
	 var length=all_cheked_checkbox.length;
		for(i=0;i<length;i++){	
		 var another_cheked=(document.getElementById(all_cheked_checkbox[i]).checked); //1		 
		 var create_id=all_cheked_checkbox[i];// vartical_1		 
		 var current_checkbox=id; //1
		 
		 var check_current=(document.getElementById(current_checkbox).checked);		 
		   if (check_current && another_cheked){		    
		    var another=all_cheked_checkbox[i];
		    var str=another;
		    var another_check=str.substring(9);		   //1  another+current	    
			$("#vartical_"+another_check+current_checkbox).removeAttr("disabled");
			$("#vartical_"+another_check+current_checkbox).css({'background-color': '#fff'})    
			$("input[class=allow_color]").attr('disabled',false);
		   }else{
		      var another=all_cheked_checkbox[i];
			 var str=another;
			 var another_check=str.substring(9);	
			$("#vartical_"+another_check+current_checkbox).attr("disabled");
			$("#vartical_"+another_check+current_checkbox).attr('value','');
			$("#vartical_"+another_check+current_checkbox).css({'background-color': '#D1D1D1'})    
			$("input[class=allow_color]").attr('disabled',false);
		   }	 	 
	    }	 
    });    
    $('.simple').keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 110 || 
            (event.keyCode == 65 && event.ctrlKey === true) || 
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
 
   $('.embroidery').change(function(){	  
      var id = $(this).attr('id');
	 var check_currentid=(document.getElementById(id).checked);
	 var someObj={};
	 someObj.checked=[];
	 someObj.unchecked=[];	
	   $(".allow_colors1:input:checkbox").each(function(){
		  var $this = $(this);	
		  if($this.is(":checked")){
			 someObj.checked.push($this.attr("id"));
		  }else{
			 someObj.unchecked.push($this.attr("id"));
		  }
	  });
	    var all_cheked_checkbox=someObj.checked; //all bottom of the checkbox
	    var length=all_cheked_checkbox.length;
	    
	    for(i=0;i<length;i++){			
		var another_cheked=(document.getElementById(all_cheked_checkbox[i]).checked); //1		
		var current_checkbox=id; //1		
		var check_current=(document.getElementById(current_checkbox).checked);
		  if (check_current && another_cheked){
		    var another=all_cheked_checkbox[i];
		    var str=another;		    
		    var another_check=str.substring(10);	    
		    $("#emb_"+another_check).removeAttr("disabled");
		    $("#emb_"+another_check).css({'background-color': '#fff'})    
		    $("input[class=simple]").attr('disabled',false);
		  }else{
		    var another=all_cheked_checkbox[i];
		    var str=another;
		    var another_check=str.substring(10);
		    $("#emb_"+another_check).attr("disabled");
		    $("#emb_"+another_check).attr('value','');
		    $("#emb_"+another_check).css({'background-color': '#D1D1D1'})    
		    $("input[class=simple]").attr('disabled',false);	  
		  }	
	   }   
  });
   
 function duplicate_old(fid,id) {
  var act_id = '#col_'+id;
  $(act_id).toggle();
 }
  function duplicate(fid,tid) {
    
    var data=myStringArray;
    var jsn = JSON.parse(data);    
    var dupid = '#dup_'+fid;    
    for (var i = 0; i < jsn .length; i++) { 
	 //alert(myStringArray[1]);return false;
      var fromid = '#color-'+fid+'-size-'+jsn[i]+'-o';	 
      var toid = '#color-'+tid+'-size-'+jsn[i]+'-o';	 
      var chekid = '#color-'+tid+'-size-'+jsn[i];
	 if($(dupid).is(':checked')){
	var fval = $(fromid).val();
        $(toid).val(fval);
	if (fval != '') {
	  $(toid).attr('disabled',false);
	  $(chekid).attr('checked',true);
	  $(toid).css({'background-color':'#fff'});	  //code
  	}

      }else{
        $(toid).val('');
	$(toid).attr('disabled',true);
	$(chekid).attr('checked',false);	
      }
    //alert(myStringArray[i]);
    //Do something
    }
    //alert(this.value);
    //var act_id = '#col_'+id;
    //$(act_id).toggle();
  }
  
  function disable(table_id)
    {
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    for(var i=0; i<inputs.length; ++i)
        inputs[i].disabled=true;
    }
    
  function enable(table_id){
    var inputs=document.getElementById(table_id).getElementsByTagName('input');
    for(var i=0; i<inputs.length; ++i)
      inputs[i].disabled=false;
  }
  $(function() {
    $('#CopyDecoration').click(function(){
      if(this.checked){
	disable('Decore');
	$("#CopyQuantityRange").attr('disabled',true);
	 $("#pro_temp").show();
      }else{
	enable('Decore');
	 $("#pro_temp").hide();
	 $("#CopyQuantityRange").removeAttr('disabled',true);
      }
    });
  });
  $(function() {
    $('#CopyQuantityRange').click(function(){
      if(this.checked){
	disable('Decore');
	$("#CopyDecoration").attr('disabled',true);
	 $("#qty_temp").show();
      }else{
	enable('Decore');
	 $("#qty_temp").hide();
	 $("#CopyDecoration").removeAttr('disabled',true);
      }
    });
  });
  
  $(function() {
    $('#CopyEmbrodary').click(function(){
      if(this.checked){
	disable('Embrodary_decoration');
	$("#CopyEmrodaryQuantityRange").attr('disabled',true);
	 $("#embr_pro_temp").show();
      }else{
	enable('Embrodary_decoration');	
	 $("#embr_pro_temp").hide();
	 $("#CopyEmrodaryQuantityRange").removeAttr('disabled',true);
      }
    });
  });
  
  $(function() {
    $('#CopyEmrodaryQuantityRange').click(function(){
      if(this.checked){
	disable('Embrodary_decoration');
	 $("#Embr_qty_temp").show();
	 $("#CopyEmbrodary").attr('disabled',true);
      }else{
	enable('Embrodary_decoration');
	 $("#Embr_qty_temp").hide();
	  $("#CopyEmbrodary").removeAttr('disabled',true);
      }
    });
  });
  
  
  
  var loaderimage='<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
ajax-loader.gif';
  //alert(loaderimage);
  
  function load_decoration(id){
    if (id!='') {
	var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	var pid = '&iProductId='+'<?php echo $_smarty_tpl->tpl_vars['product_id']->value;?>
'+'&qid='+id;
	$('#ajax-load-deco').html('<img src="'+loaderimage+'">');
	$.ajax({
		type: "POST",
		url: site_url+"product/load_decoration?"+pid,	    
		success: function(data){
		    $('#ajax-load-deco').html(data);
	    }
	});
    }else{
	 return false;
    }
    
  }
  var id=$("#iQuantityRangeId").val();
  
  function loadEmrodary(id){    
    if (id!=''){	 
	 var site_url = '<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
';
	 var pid = '&iProductId='+'<?php echo $_smarty_tpl->tpl_vars['product_id']->value;?>
'+'&qid='+id;
	 //alert(site_url+"product/loadEmbrodaryDecoration?"+pid);return false;
	 $('#ajax-load-embrodary').html('<img src="'+loaderimage+'">');
	 $.ajax({
		 type: "POST",
		 url: site_url+"product/loadEmbrodaryDecoration?"+pid,	    
		 success: function(data){
			$('#ajax-load-embrodary').html(data);
		}
	 });
    }
  }
  
  
  
  
  
  
  
  </script>

<?php }} ?>