<?php /* Smarty version Smarty-3.1.11, created on 2013-11-11 22:16:48
         compiled from "application/views/templates/admin/fabrics/fabrics.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53052112851a5c8f175e082-72603718%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0212b24ac51113ff765edb33a10eb07c55cfff78' => 
    array (
      0 => 'application/views/templates/admin/fabrics/fabrics.tpl',
      1 => 1377257317,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53052112851a5c8f175e082-72603718',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51a5c8f198e2f6_80439660',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'totalRec' => 0,
    'initOrder' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51a5c8f198e2f6_80439660')) {function content_51a5c8f198e2f6_80439660($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
fabrics/fabricslist">Fabrics</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add  Fabrics<?php }else{ ?>Edit Fabrics<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Fabrics
			<?php }else{ ?>
			Edit Fabrics
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iFabricsId" id="iFabricsId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iFabricsId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				<input type="hidden" name="preview" id="preview" value="1" />

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Fabrics Name</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Fabrics Name" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order No</label>
					<span class="collan_dot">:</span>
					<select id="iOrderNo" name="Data[iOrderNo]" lang="*" title="Order Number">
						
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value+1)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
						
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
						<?php }?>
						<?php }else{ ?>
						<?php echo print_r($_smarty_tpl->tpl_vars['initOrder']->value);?>

						<option value=''>--Select Order--</option>
						<?php while (($_smarty_tpl->tpl_vars['totalRec']->value)>=$_smarty_tpl->tpl_vars['initOrder']->value){?>
							
						<option value="<?php echo $_smarty_tpl->tpl_vars['initOrder']->value;?>
"  <?php if ($_smarty_tpl->tpl_vars['data']->value->iOrderNo==$_smarty_tpl->tpl_vars['initOrder']->value){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['initOrder']->value++;?>
</option>
						
							<?php }?>
						<?php }?>
					
					</select>
				</div>
			
				<div class="inputboxes">
					<label for="textfield"><span class="red_star" ></span>Description</label>
					<span class="collan_dot">:</span>
					<div class="worddocument"><textarea id="tDescription" name="Data[tDescription]" class="inputbox" title="Description"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tDescription;?>
<?php }?></textarea></div>
				</div>
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="estatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="InActive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='InActive'){?>selected<?php }?><?php }?> >InActive</option>
					</select>
				</div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Fabrics" class="submit_btn" title="Add Fabrics" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Fabrics" class="submit_btn" title="Edit Fabrics" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
fabrics/fabricslist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>