<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 07:51:36
         compiled from "application/views/templates/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11351916751c9a068d79bc3-42916280%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8590e4da4dfe98e3c0a264b029dc45087aae9c97' => 
    array (
      0 => 'application/views/templates/search.tpl',
      1 => 1372168259,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11351916751c9a068d79bc3-42916280',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'allProduct' => 0,
    'upload_path' => 0,
    'site_url' => 0,
    'pages' => 0,
    'recmsg' => 0,
    'iCategoryId' => 0,
    'categories' => 0,
    'alp' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9a06904f9e2_58229120',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9a06904f9e2_58229120')) {function content_51c9a06904f9e2_58229120($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container col2-right-layout">
    <div class="main">
	  <div class="main-inner clearfix">				
		<div class="breadcrumbs row-fluid">
		    <div class="container">
		    <ul class="span12" style="text-align:left;">
			  <li class="home">
				<a href="#" title="Go to Home Page">Home</a>
				<samp></samp>
			  </li> 
			  <li class="category5">
				<strong>Categories</strong>
			  </li> 
		    </ul> 
		    <!--		<div class ="mt-page-title hidden-phone span4"> 
		    <h2>Categories</h2> 
		    </div> 
		    -->
		    </div>	
		</div>
		<div class="container">
		    <div class="row-fluid show-grid">
			  <div class="col-main span9">
				<div class="col-main-inner">
				    <p class="category-image" style="display:block;"></p>
				    <div class="category-products">
					  <div class="toolbar">
						<div class="pager row-fluid show-grid">		
						    <div class="sort-by span4">
							  <div class="mt-sort-by clearfix">			
							  <p class="pull-left mt-toolbar-label hidden-tablet"><label>Sort By</label></p>
							  <select id="mt_sort_by" onchange="setLocation(this.value)">
							  <option value="posi" selected="selected">Position</option>
							  <option value="name">Name</option>
							  <option value="price">Price</option>
							  <option value="tier">Tier Price</option>
							  </select>
							  <p class="mt-sort-arrows pull-left">
								<a href="#" title="Set Descending Direction"><img src="images/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
							  </p>
							  </div>
						    </div>
						    <div class="limiter span4">
							  <div class="mt-limiter">
								<p class="pull-left mt-toolbar-label mt-slabel-1 hidden-tablet"><label>Show</label></p>
								<select id="mt_limiter" onchange="setLocation(this.value)">
								    <option value="df" selected="selected">9</option>
								    <option value="dfasf">15</option>
								    <option value="df">30</option>
								</select> 
								<p class="pull-left mt-toolbar-label mt-slabel-2 hidden-tablet">per page</p>
							  </div>
						    </div>
						    <div class="row-pager clearfix" style="display:none;">
							  <div class="mt-row-page">
							  </div>			
						    </div>
						    
						    <div class="view-mode span4">
							  <div class="mt-view pull-right clearfix">			
							  <p class="pull-left mt-toolbar-label hidden-tablet"><label>View as:</label></p>
							  <span class="grid">
								<strong title="Grid"></strong>
							  </span>
							  <span class="list">
								<a href="#" title="List"></a>
							  </span>
							  </div>			
						    </div>
						</div>
					   </div>
					   <div id="pageData">
						  <?php if (count($_smarty_tpl->tpl_vars['allProduct']->value)>0){?>
						  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['allProduct']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						  <div class="products-grid row-fluid show-grid">
							<div class="item first span4">
							    <div class="item-inner content">
								  <div class="product-image">
									<div class="product-thumb">
									    <a href="#" title="Cras in risus et risus" class="product-image visible-desktop" id="product_31">
									    <img style="width:270px; height:306px;" src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['allProduct']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
/<?php echo $_smarty_tpl->tpl_vars['allProduct']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="Cras in risus et risus" />
									    </a>
									</div>
									<div class="quotelink"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant-quote/<?php echo $_smarty_tpl->tpl_vars['allProduct']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
">Get a Quote</a></div>
								  </div>
								  <div class="mask quotelinkmak" onclick="setLocation('#')">							
									<h3 class="product-name">
									<a href="#" title="Cras in risus et risus"><?php echo $_smarty_tpl->tpl_vars['allProduct']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
</a>
									</h3>
								  </div>
								  <div class="top-actions-inner">
									<div class="mt-actions clearfix">
									</div>					
								  </div>		
							    </div>
							</div>
						  </div>
						  <?php endfor; endif; ?>
						  <?php }else{ ?>
						  <span>No Item Found</span>
						  <?php }?>
						  <div class="mt-row-page">
							 <div id="ajax_paging">
							 <div class="Pagingbox">
								  <?php echo $_smarty_tpl->tpl_vars['pages']->value;?>

							  </div>
							  <!--<?php echo $_smarty_tpl->tpl_vars['recmsg']->value;?>
-->
							 </div>
						  </div>
					   </div>
					  <script type="text/javascript">decorateGeneric($$('ul.products-grid'), ['odd','even','first','last'])</script>
					  <div class="toolbar-bottom">
						<div class="toolbar">
						    <div class="pager row-fluid show-grid">		
							  <div class="sort-by span4">
								<div class="mt-sort-by clearfix">			
								    <p class="pull-left mt-toolbar-label hidden-tablet"><label>Sort By</label></p>
								    <select id="mt_sort_by" onchange="setLocation(this.value)">
									  <option value="position" selected="selected">Position</option>
									  <option value="name">Name</option>
									  <option value="price">Price</option>
									  <option value="tier">Tier Price</option>
								    </select>
								    <p class="mt-sort-arrows pull-left">
								    <a href="#" title="Set Descending Direction"><img src="images/i_asc_arrow.gif" alt="Set Descending Direction" class="v-middle" /></a>
								    </p>
								</div>			
							  </div>
							  <div class="limiter span4">
								<div class="mt-limiter">
								    <p class="pull-left mt-toolbar-label mt-slabel-1 hidden-tablet"><label>Show</label></p>
								    <select id="mt_limiter" onchange="setLocation(this.value)">
									  <option value="http://robel.joomvision.com/categories.html?limit=9" selected="selected">9</option>
									  <option value="http://robel.joomvision.com/categories.html?limit=15">15</option>
									  <option value="http://robel.joomvision.com/categories.html?limit=30">30</option>
								    </select> 
								    <p class="pull-left mt-toolbar-label mt-slabel-2 hidden-tablet">per page</p>
								</div>
							  </div>
							  <div class="row-pager clearfix" style="display:none;">
								<!--<div class="mt-row-page">
								    <div class="pages">
									  <ol class="paging">
									  <li class="current">1</li>
									  <li><a href="#">2</a></li>
									  <li><a class="next i-next" href="#" title="Next"></a></li>
									  </ol>
								    </div>
								</div>-->
								<!--<div class="mt-row-page">
								    <div class="pages">
									  <?php if (count($_smarty_tpl->tpl_vars['pages']->value)>0){?>
									  <ol class="paging">
										<li><a class="back i-back" title="Back" href="javascript:void(0)" onclick="changePagination('1',<?php echo $_smarty_tpl->tpl_vars['iCategoryId']->value;?>
)"></a></li>
										<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['pages']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
										<li id="no" class='link'><a href="javascript:void(0)" onclick="changePagination('<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index']+1;?>
',<?php echo $_smarty_tpl->tpl_vars['iCategoryId']->value;?>
)"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['i']['index']+1;?>
</a></li>
										<?php endfor; endif; ?>
										<li><a class="next i-next" title="Next" href="javascript:void(0)" onclick="changePagination('<?php echo $_smarty_tpl->tpl_vars['pages']->value;?>
',<?php echo $_smarty_tpl->tpl_vars['iCategoryId']->value;?>
)"></a></li>
									  </ol>
									  <?php }?>
								    </div>
								</div>-->
							  </div>
							  <div class="view-mode span4">
								<div class="mt-view pull-right clearfix">			
								    <p class="pull-left mt-toolbar-label hidden-tablet"><label>View as:</label></p>
								    <span class="grid">
									  <strong title="Grid"></strong>
								    </span>
								    <span class="list">
									  <a href="#" title="List"> </a>
								    </span>
								</div>			
							  </div>
						    </div>
						</div>
					  </div>
				    </div>
				</div>
			  </div>
			  <div class="col-right sidebar span3 visible-desktop"> 
				<div class="block mt-smartmenu">
				    <div class="block-title">
					  <strong><span>Category</span></strong>
				    </div>
				    <div class="title-divider">
					  <span>&nbsp;</span>
				    </div>
				    <div class="block-content">
					   	
					   <script type="text/javascript" src="js/mt.accordion.js"></script>
						  <script type="text/javascript">
							 $mtkb(document).ready(function(){	
							 // applying the settings
							 $mtkb("#mt-accordion li.active span.head").addClass("selected");
							 $mtkb('#mt-accordion').Accordion({
								active: 'span.selected',
								header: 'span.head',
								alwaysOpen: false,
								animated: true,
								showSpeed: 400,
								hideSpeed: 800,
								event: 'click'
								});
							 });	
						  </script>
					   
					   <ul id="mt-accordion" class="clearfix">
						 <?php if (count($_smarty_tpl->tpl_vars['categories']->value)>0){?>
						 <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['categories']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						 <?php if ($_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iParentId=='0'){?>
						 <li onmouseover="Element.addClassName(this, 'over') " onmouseout="Element.removeClassName(this, 'over') " class="level0 nav-categories parent">
							<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category/<?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vUrlText;?>
"><span><?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vCategory;?>
</span></a><span class="head"><a href="#" style="float:right;"></a></span>
							<ul class="level0">
							   <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['categories']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
							   <?php if ($_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iParentId!='0'&&$_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iParentId==$_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iCategoryId){?>
							   <li class="level1 nav-categories-footwear-man">
								 <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category/<?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vUrlText;?>
"><span><?php echo $_smarty_tpl->tpl_vars['categories']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCategory;?>
</span></a>
							   </li>
							   <?php }?>
							   <?php endfor; endif; ?>
							</ul>
						 </li>
						 <?php }?>
						 <?php endfor; endif; ?>
						 <?php }?>
					   </ul>
				    </div> 
				</div>
				<input id="max-price" type="hidden" name="max-price" value="151">
				<input id="filter_url" type="hidden" name="filter_url" value="http://robel.joomvision.com/">  
				<input id="category" type="hidden"  value="5">
				<div class="block block-layered-nav">
				    <div class="block-title">
					  <strong><span>Price</span></strong>
				    </div>
				    <div class="title-divider"><span>&nbsp;</span></div>
				    <div class="block-content"> 
					  <p class="block-subtitle" style="display: none;">Shopping Options</p>
					  <dl id="narrow-by-list">  
						<dd style="height:11px; margin: 20px 0 20px 0;" id="slider" class="odd ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header" style="left: 0%; width: 100%;"></div><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></a><a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></a></dd>
						<dd class="last even"> 
						    <span class="filter_min" id="filter_min">$0.00</span> 
						    <span class="filter_max" id="filter_max">$427.00</span>
						</dd> 
					  </dl>
					  <script type="text/javascript">decorateDataList('narrow-by-list')</script>
				    </div>
				    
				    <script type="text/javascript">
					  var Magen_filterslider = {
						params : '',
						currency : '$',
						category_id: 5,
						currency_code : 'USD',
						filter_min: 0,
						filter_max: 427
					  }
				    </script>
				    
				</div>
			  </div>
		    </div>
		</div>
	  </div>
	  <div class="mt-productcroller-container clearfix"></div>
    </div>
</div>

<script type='text/javascript'>
function searchPaginationProduct(start){
   var q = '<?php echo $_smarty_tpl->tpl_vars['alp']->value;?>
';
   //alert(q);
    var url = site_url+'search';
    var dataString = 'start='+ start+'&q='+q+'&ajax=yes';
    //alert(url);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    cache: false,
    success: function(result){
		//alert(result);
		//return false;
		$("#pageData").html(result);
	  }
    });
}
</script>

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>