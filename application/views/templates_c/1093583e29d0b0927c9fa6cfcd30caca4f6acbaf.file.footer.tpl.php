<?php /* Smarty version Smarty-3.1.11, created on 2013-12-02 19:51:31
         compiled from "application/views/templates/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16277303155183dcba81e101-10799900%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1093583e29d0b0927c9fa6cfcd30caca4f6acbaf' => 
    array (
      0 => 'application/views/templates/footer.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16277303155183dcba81e101-10799900',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5183dcba85d7c3_41948660',
  'variables' => 
  array (
    'site_url' => 0,
    'product_category' => 0,
    'front_image_path' => 0,
    'config_facebook' => 0,
    'config_twitter' => 0,
    'config_google' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5183dcba85d7c3_41948660')) {function content_5183dcba85d7c3_41948660($_smarty_tpl) {?>	<div class="mt-footer-static-container">
			<div class="mt-footer-static-container-top hidden-phone">
				<div class="mt-footer-static container">
					<div class="row-fluid show-grid">
						<div class="mt-footer-static-inner">
							<div class="span6">
								<div class="shipping">
									<div class="row-fluid">
										<p class="span2"><span class="icon">icon</span></p>
										<p class="span10" style="padding:21px 0 0 0;">Free shipping for online t-shirt orders </p>
										<p class="stand">standard service</p>
									</div>
								</div>
							</div>
							<div class="span6">
								<div class="block subscribe">
									<form action="#" method="post" id="form-validate" ">
										<div class="block-content">
											<div class="row-fluid show-grid">
												<div class="form-subscribe-header span6">
													<div class="row-fluid">
														<p class="span4" style="margin-left: 190px;"><span class="icon">icon</span></p>
														<label class="span8" for="newsletter" style="margin-left: 165px;"> Specials Newsletter</label>
													</div>
												</div>
												<div class="input-box span6">
													<div class="mt-subscribe row-fluid show-grid">
														<div class="span7">
															<div class="input">
																<input id="vNewsletter" placeholder="Enter Your Email" class="input-text required-entry validate-email pull-left" title="Newsletter" name="Data[news]" />
															</div>
															<div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
															<div class="validation-advice" id="emailsuccess" style="display:none; float: left; color: #5EA948;"></div>
														</div>
														<div class="span5">
															<div class="clear-margin clearfix">
																<button class="button pull-left" title="Subscribe" type="button" data-placement="top" onclick="return Check();"> <span> <span>Submit</span> </span> </button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="mt-footer-static-container-bottom">
				<div class="mt-footer-static container">
					<div class="mt-footer-container">
						<div class="row-fluid show-grid">
							<div class="span3">
								<div class="about-us">
									<h3>navigation</h3>
									<div class="footer-static-content">
										<ul>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
home">Home </a></li>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
aboutus">About Us</a></li>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
request_catalog">Request catalog</a></li>
											<li><a href="http://emblemax.com/apparel">Apparel</a></li>
											<li><a href="http://www.emblemax.net">Promotional Products</a></li>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
instant-quote">Get a Quote</a></li>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
contactus">Contact</a></li>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
faq">FAQ</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="our-account">
									<h3><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category" class="level-top"> <span>Categories</span></h3>
									<div class="footer-static-content">
										<ul>
											<?php if (count($_smarty_tpl->tpl_vars['product_category']->value)>0){?>
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['product_category']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category/<?php echo $_smarty_tpl->tpl_vars['product_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vUrlText'];?>
"><?php echo $_smarty_tpl->tpl_vars['product_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCategory'];?>
</a></li>
											<?php endfor; endif; ?>
											<?php }?>
										</ul>
									</div>
								</div>
							</div>
							<div class="span3">
								<div class="block_support">
									<div class="contact">
										<h3>follow us On</h3>
										
										
										<div class="followicon">
											<span><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
f-icon.jpg" alt="" /></span> <a href="<?php echo $_smarty_tpl->tpl_vars['config_facebook']->value;?>
" title="Facebook" target="_blank">Facebook</a>
										</div>
										<div class="followicon">
											<span><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
t-icon.jpg" alt="" /></span> <a href="<?php echo $_smarty_tpl->tpl_vars['config_twitter']->value;?>
" title="Twitter" target="_blank">Twitter</a>
										</div>
										<div class="followicon">
											<span><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
g-icon.jpg" alt="" /></span> <a href="<?php echo $_smarty_tpl->tpl_vars['config_google']->value;?>
" title="Google+" target="_blank">Google+</a>
										</div>
										<div class="followicon">
											<span><img src="<?php echo $_smarty_tpl->tpl_vars['front_image_path']->value;?>
blog-icon.png" alt="" /></span> <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
blog" title="Wordpress Blog" target="_blank">Wordpress Blog</a>
										</div>
									</div>
									
								</div>
							</div>
							<div class="span3">
								<div class="facebook">
									<div class="footer-static-title">
										<h4>Blog</h4>
									</div>
									
									<div class="blogwrap">
										
										<div class="blogcomt">
											<p>9 in 10 shoppers say they would consider buying editor- reccomended products.</p>
											<a href="#" class="moretx">More</a>
										</div>
										<div class="blogcomt">
											<p>9 in 10 shoppers say they would consider buying editor- reccomended products.</p>
											<a href="#" class="moretx">More</a>
										</div>
									</div>
								</div>
							</div>
							 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="error_msg" style="color:red;display: none;background-color: #F2DEDE; border: 1px solid #EED3D7;border-radius: 5px 5px 5px 5px; color: #B94A48;font-size: 13px;font-weight: bold; margin-top: 20px;padding-top: 8px;padding-left: 10px;text-shadow: 0 1px 1px #FFFFFF; width: 940px; height: 30px;"></div>			
<div class="footer-container mt-copyright">
	<div class="container">
		<div class="footer row-fluid show-grid">
			<address class="address span6">
			Copyright &#169; 2012 <a href="#">Emblemax ideas that promote</a> by. All rights reserved.
			</address>
			<div class="span6 hidden-phone">
				<ul>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
aboutus">About Us</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
termscondition">Terms-Condition</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
customerservices">Customer Service</a></li>
					<li class="last privacy"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
privacy">Privacy Policy</a></li>
				</ul>
				<ul class="links">
					<li class="first" ><a href="#" title="Site Map" >Site Map</a></li>
					<li ><a href="#" title="Search Terms" >Search Terms</a></li>
					<li ><a href="#" title="Advanced Search" >Advanced Search</a></li>
					<li ><a href="#" title="Orders and Returns" >Orders and Returns</a></li>
					<li class=" last" ><a href="#" title="Contact Us" >Contact Us</a></li>
				</ul>
			</div>
			<p id="back-top" class="hidden-phone"> <a href="#top"><span title="Back to Top"></span></a> </p>
		</div>
	</div>
</div>

<script type="text/javascript">
function Check()
{
	var email = $('#vNewsletter').val();
	var validate = true;
	var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var isvalid = emailRegexStr.test(email);
    
    if( $('#vNewsletter').val() == ''){
    $("#vNewsletter").addClass("borderValidations");
	$("#emailDiv").html('This is a required field.');
	$("#emailDiv").show();
	$("#emailsuccess").hide();
	validate = false;
    }
  else if (!isvalid) {
	$("#vNewsletter").addClass("borderValidations");
	$("#emailDiv").html('Enter Your Email Address like demo@domain.com');
	$("#emailDiv").show();
	$("#emailsuccess").hide();
	validate = false;
    }
   else{
      $("#emailDiv").hide();
      $("#vNewsletter").removeClass("borderValidations");
      }
   if (validate)
    {
	var extra = '';
	extra+='?vNewsletter='+email;
	var url = site_url + '/footer/newsletter';
	var pars = extra;
         $.post(url+pars,
          function(data) {
		if(data == 'exists'){
		$("#vNewsletter").addClass("borderValidations");
		$("#emailDiv").html('You Are Already Subscibed.');
		$("#emailsuccess").hide();
		$("#emailDiv").show();
		validate = false;			
		}
		if(data == 'success'){
		$("#emailsuccess").html('Thank you for your subscription.');
		$("#emailsuccess").show();
		 $("#vNewsletter").removeClass("borderValidations");
		validate = true;
		}
	  });
	
	
		return true;
    }
    else
    {
		return false;
    } 
}
</script>
<script type="text/javascript">
	var tooltip = $mtkb('#back-top span');
	$mtkb(tooltip).tooltip(true);
	$mtkb(".block-layered-nav .block-content p.block-subtitle, .addtolink .no-rating").hide();
	$mtkb("#product-options-wrapper p.required").hide();	
	$mtkb('.toolbar.row-pager, .toolbar-bottom .sort-by, .toolbar-bottom .limiter, .toolbar-bottom .view-mode').hide();
	$mtkb('.toolbar-bottom .toolbar .row-pager').show();
	$mtkb('table#my-orders-table thead').addClass('hidden-phone');	
	
	$mtkb("table").removeClass('data-table');
	$mtkb("table").addClass('table table-bordered');	
	$mtkb("table#shopping-cart-totals-table").removeClass('table table-bordered');	
	$mtkb('#sitemap_top_links').addClass('row-fluid show-grid');
	$mtkb('#sitemap_top_links .links, .page-sitemap ul.links').addClass('clearfix');
	
	$mtkb("#review-form .form-list input.input-text,#review-form .form-list textarea").addClass('span4');
	$mtkb("#shipping-zip-form .form-list select, #billing-new-address-form select.validate-select, ul li.fields .input-box select.validate-select, ul li.fields div.field .input-box input.input-text, #wishlist-table textarea").addClass('span12');
	$mtkb(".buttons-set p.required").css("width","100%");
	$mtkb(".buttons-set p.back-link").addClass("pull-left");
	$mtkb(".buttons-set button.button").addClass("pull-right");
	$mtkb(".checkout-cart-index .col-main .cart-empty li.error-msg").addClass('span4 offset4');
	$mtkb("#multiship-addresses-table select").addClass('span4');	
	$mtkb("#multiship-addresses-table .qty").addClass('span2');	
	var msize = false;
	var msize1 = false;
	var checksize = function(){
        w = $mtkb(".container").width();  
        msize = (w > 730) ? false : true; 
        msize1 = (w >= 704) ? false : true; 
    }   
    
    $mtkb('.mtajaxcart').hide(); 
	$mtkb(".top-cart").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(300).slideDown(500, "easeOutCubic");
	}, function() {
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	});  
	$mtkb('.mt-search-form').hide(); 
	$mtkb(".top-search").hover(function() {
		if(!msize) {
			$mtkb(this).addClass('hover');
			$mtkb(".top-search .mt-search-form").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
		}
	}, function() {
		if(!msize) {
		$mtkb(".top-search .mt-search-form").stop(true, true).delay(100).slideUp(200, "easeInCubic");
		}
	}); 
	$mtkb('.mt-top-link').hide(); 
	$mtkb(".top-link").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
	}, function() {
		if(msize1){
			$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideDown(200, "easeInCubic");
		}
		else
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	}); 
    $mtkb(document).ready(function(){
        checksize();
	}); 
    $mtkb(window).resize(function(){
        checksize();
    });	
	
	$mtkb("#back-top").hide();  
</script>


</div>
</div>
</body>
</html>
<?php }} ?>