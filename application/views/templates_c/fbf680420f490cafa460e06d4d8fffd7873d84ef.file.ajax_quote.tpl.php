<?php /* Smarty version Smarty-3.1.11, created on 2014-02-24 02:25:01
         compiled from "application/views/templates/ajax_quote.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57653354051ea8fd3a13fc4-01083332%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fbf680420f490cafa460e06d4d8fffd7873d84ef' => 
    array (
      0 => 'application/views/templates/ajax_quote.tpl',
      1 => 1393226493,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57653354051ea8fd3a13fc4-01083332',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51ea8fd3a9c747_87118451',
  'variables' => 
  array (
    'quoteSummary' => 0,
    'costPerPiece' => 0,
    'subTotal' => 0,
    'shipmentCharge' => 0,
    'couponDiscount' => 0,
    'totalQuote' => 0,
    'sortQtyRange' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51ea8fd3a9c747_87118451')) {function content_51ea8fd3a9c747_87118451($_smarty_tpl) {?><div class="product_tab_left">
    <table>	   
	<tr>
	    <td>
		Product name :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		<?php echo $_smarty_tpl->tpl_vars['quoteSummary']->value['vProductName'];?>

	    </td>
	</tr>
	<tr>
	    <td>
		Size & Color :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['quoteSummary']->value['vColorSizeDescription']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		    <?php echo $_smarty_tpl->tpl_vars['quoteSummary']->value['vColorSizeDescription'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		    <br>
		<?php endfor; endif; ?>
	    </td>
	</tr>
	<tr>
	    <td>
		Print Locations :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['quoteSummary']->value['vPrintLocationsDescription']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
		    <?php echo $_smarty_tpl->tpl_vars['quoteSummary']->value['vPrintLocationsDescription'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>

		    <br>
		<?php endfor; endif; ?>
	    </td>
	</tr>
	<tr>
	    <td>
		Total Qty :
	    </td>
	    <td>&nbsp;
		
	    </td>
	    <td>
		<?php echo $_smarty_tpl->tpl_vars['quoteSummary']->value['totalQty'];?>

	    </td>
	</tr>
    </table>
    <a class="link_quot margin_align" href="#getAQuoteID">Revise Quote</a><br>
    <!--<a class="link_quot">Need better deal? Order more to lower cost per peice</a>-->
    
    <br><br>    
    
    
</div>
<div class="prod_tab_right">
    <table class="product_detail_quote_table">
	<tr>
	    <td class="quote_subtotal">Cost Per Piece: </td><td class="quote_subtotal">$ <?php echo $_smarty_tpl->tpl_vars['costPerPiece']->value;?>
</td>
	</tr>
	<tr>
	    <td class="quote_subtotal">Sub Total : </td><td class="quote_subtotal">$ <?php echo $_smarty_tpl->tpl_vars['subTotal']->value;?>
</td>
	</tr>
	<!--<tr>
	    <td class="quote_subtotal">Shipment Charge : </td><td class="quote_subtotal">$ <?php echo $_smarty_tpl->tpl_vars['shipmentCharge']->value;?>
</td>
	</tr>-->
	<tr>
    	    <td class="quote_subtotal">Coupon Discount : </td><td class="quote_subtotal">$ <?php echo $_smarty_tpl->tpl_vars['couponDiscount']->value;?>
</td>
	</tr>
	<tr>
	    <td class="quote_subtotal">Total Cost : </td><td class="quote_subtotal">$ <?php echo $_smarty_tpl->tpl_vars['totalQuote']->value;?>
</td>
	</tr>
	<tr>
	    <td class="quote_subtotal"></td>
	    <td class="quote_subtotal" id="Quote"></td>
	</tr>
	
	
    </table>
	<a class="link_quot margin_align_right"  href="#save_quote" id="saveQuote">Save Quote</a>
    <div>
  <!--   <a style="color: #9E053B; font-size: 16px; text-align: center;" href="#save_quote" id="saveQuote">Save Quote</a>  -->
    </div>
    
</div>
<div class="note_quote_price">
	<p>Note: Pricing includes standard shipping, all set up fees, and professional art review.</p>
    </div>
	<div class="dont-forger_guot"><p >Don't forget to order more and save!</p>
		<table class="table_order_quote">    
			<tr>
			   <th>Quantity</th>
			   <th>Each</th>
			   <th>Total</th>	   
			</tr>
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sortQtyRange']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<tr>
			   <td><?php echo $_smarty_tpl->tpl_vars['sortQtyRange']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iStartRange'];?>
</td>
			   <td>$<?php echo $_smarty_tpl->tpl_vars['sortQtyRange']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['Each'];?>
</td>
			   <td>$<?php echo $_smarty_tpl->tpl_vars['sortQtyRange']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['Total'];?>
</td>	   
			</tr>
			<?php endfor; endif; ?>   
		</table>  
	</div><?php }} ?>