<?php /* Smarty version Smarty-3.1.11, created on 2013-06-16 23:58:42
         compiled from "application/views/templates/admin/user/changepass.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1836678285162507a436189-88544007%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad90b74cca782c354dd88f7a64e30a277d715e42' => 
    array (
      0 => 'application/views/templates/admin/user/changepass.tpl',
      1 => 1367422227,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1836678285162507a436189-88544007',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5162507a506285_49475594',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'iUserId' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5162507a506285_49475594')) {function content_5162507a506285_49475594($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/userlist">User</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add  Freelancer<?php }else{ ?>Change Password<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle_tab">
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>

			<?php }else{ ?>
			<ul>
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/edit?iUserId=<?php echo $_smarty_tpl->tpl_vars['iUserId']->value;?>
">Edit User</a></li>
				<!--<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
projectbrief/projectbrieflist?iUserId=<?php echo $_smarty_tpl->tpl_vars['iUserId']->value;?>
">Project Brief</a></li>-->
				<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
transaction/transactionlist?iUserId=<?php echo $_smarty_tpl->tpl_vars['iUserId']->value;?>
">Transaction</a></li>
				<li class="active"><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
changepassword/passworduser?iUserId=<?php echo $_smarty_tpl->tpl_vars['iUserId']->value;?>
">Change Password</a></li>
			</ul>
			<?php }?>
		</div>
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data" action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iUserId" id="iUserId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['iUserId']->value;?>
<?php }?>" />

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> New Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vPassword" name="Data[vPassword]"  class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vFirstName;?>
<?php }?>" lang="*" title="New Password" />
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Confirm Password</label>
					<span class="collan_dot">:</span>
					<input type="password" id="vConformPass" lang="*" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPassword;?>
<?php }?>" title="Conform Password" class="inputbox" name="Data[vConformPass]" >
				</div>
	
				<div class="add_can_btn"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="submit" value="Add Password" class="submit_btn" title="Add Password"/>
					<?php }else{ ?>
					<input type="submit" value="Save Password" class="submit_btn" title="Edit Password" onclick="return checkpass();"/>
					<?php }?> <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
user/userlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
				<div class="clear"></div>
			</div>
		</form>
	</div>
</div>
<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
function checkpass(){
	var pass = $('#vPassword').val();
	var confirmpass = $('#vConformPass').val();

	if($('#vPassword').val() ==''){
                $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please enter new password</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
		return false;
	}else if($('#vConformPass').val() ==''){
		$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please enter conform password</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
		return false;
	}else if($('#vPassword').val() != $('#vConformPass').val()){
		$('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Error</div></div><div class="modal-body"><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Please check conform password</h3></div><input type="button"  data-dismiss="modal" aria-hidden="true" value="ok" /></div></div>').modal();
		return false;
	}else{
		//confirm("Sure you want to change your old password");
		$('<div  id="imgdelete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Change Password</div></div><div class="eor_poptxt"><img src="http://09php.com/demo/Onwardz/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure , You wanted to Change password ?</h3></div><div class="view_del_user" style="margin-left:86px;"><div class="delete_user"  ><a  class="delete_btnimg" data-dismiss="modal" onclick="deleteimage();" >Change Password</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg" data-dismiss="modal">Cancel</a></div></div></div>').modal();
		return false;
	}
}
function deleteimage(){		
	document.frmadd.submit();
}
</script>

<?php }} ?>