<?php /* Smarty version Smarty-3.1.11, created on 2014-02-21 06:58:03
         compiled from "application/views/templates/shoping_cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17054269051ea9140469223-81997996%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '37fa9cea4b5c09ee45f174e350bbf9e78834f42a' => 
    array (
      0 => 'application/views/templates/shoping_cart.tpl',
      1 => 1386038767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17054269051ea9140469223-81997996',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51ea914056d447_14064385',
  'variables' => 
  array (
    'msg' => 0,
    'cart' => 0,
    'site_url' => 0,
    'upload_path' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51ea914056d447_14064385')) {function content_51ea914056d447_14064385($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"Example Smarty Page"), 0);?>

<div class="main-container col2-right-layout">
   <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
      <div class="main container">
	  <div class="slideshow_static"></div>
                <div class="main-inner">
                    <div class="col-main">
			<div class="account-create">
    			<h2 class="pagetital subtit">Shopping Cart</h2>
			<div style="clear:both;"></div>
			<div class="success_msg"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</div>
            <div class="ordleftpart" style="width:100%;">
<fieldset>
<?php if (count($_smarty_tpl->tpl_vars['cart']->value['item'])>0){?>
<form id="frmShoppingCart" name="frmShoppingCart" method="post" action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
cart/update_cart">
<table id="shopping-cart-table" class="data-table cart-table" style="border-bottom:2px solid #EAEAEA">
    <col width="1" />
    <col />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <col width="1" />

    <thead class="hidden-phone">
      <tr class="">
	  <th class="" rowspan="1"><span class="nobr">Remove</span></th>
	  <th width="100" rowspan="1" class=""><span class="nobr">Image</span></th>
	  <th width="200" rowspan="1" class=""><span class="nobr">Product Name</span></th>
	  <th width="200" rowspan="1" class=""><span class="nobr">Selected Print locations</span></th>
	  <th width="200" rowspan="1" class=""><span class="nobr">Selected Color - Size</span></th>
	  <th width="100" colspan="1"><span class="nobr">Total Qty</span></th>
	  <th rowspan="1"><span class="nobr">Total</span></th>
      </tr>
    </thead>

    <tbody>
       <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['cart']->value['item']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
       <tr>
	   <td><input name="remove[]" type="checkbox" value="" /></td>
	   <td width="100"><img width="100px" src="<?php echo $_smarty_tpl->tpl_vars['upload_path']->value;?>
product/<?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductId'];?>
/<?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="" /></td>
	   <td width="200"><?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductName'];?>
</td>
	   <td width="200">
	     <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocationsDescription']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
	       <?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vPrintLocationsDescription'][$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']];?>
<br>
	     <?php endfor; endif; ?>
	   </td>
	   <td width="200">
	     <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColorSizeDescription']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
	       <?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColorSizeDescription'][$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']];?>
<br>
	     <?php endfor; endif; ?>
	   </td>
	   <td width="100"><?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vTotalQty'];?>
</td>
	   <td width="100">$<?php echo $_smarty_tpl->tpl_vars['cart']->value['item'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['fQuoteTotal'];?>
</td>
       </tr>
       <?php endfor; endif; ?>
    </tbody>
</table>
</form>
<div class="grid_7 coupons">
  <form id="frmCouponDiscount" name="frmCouponDiscount" method="post" action="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
cart/apply_coupon">
     <label for="couponcode">Coupon code</label>
     <input type="text" name="couponcode" id="couponcode" value="">
     <input type="submit" name="submitcoupon" class="sub" id="submitcoupon" value="Apply">
     <div class="clear"></div>
   </form>
</div>
<div class="grid_9">
<span class="subtotal"><span class="subtotal_lbl">Sub Total:</span>&nbsp;<span class="subtotal_amt">$<?php echo $_smarty_tpl->tpl_vars['cart']->value['SUBTOTAL'];?>
</span></span>
<span class="subtotal"><span class="subtotal_lbl">Turn Around Time Changes (<?php echo $_smarty_tpl->tpl_vars['cart']->value['vTurnAroundTime'];?>
):</span>&nbsp;<span class="subtotal_amt">$<?php echo $_smarty_tpl->tpl_vars['cart']->value['fTurnAroundTimeCharge'];?>
</span></span>
<span class="subtotal"><span class="subtotal_lbl">Coupon Discount (Code : <?php echo $_smarty_tpl->tpl_vars['cart']->value['vCouponCode'];?>
):</span>&nbsp;<span class="subtotal_amt">$<?php echo $_smarty_tpl->tpl_vars['cart']->value['fCouponDiscount'];?>
</span></span>
<div class="total">Total : <span class="bigprice">$<?php echo $_smarty_tpl->tpl_vars['cart']->value['GRANDTOTAL'];?>
</span></div>
<div class="updaters">
<input type="button" name="updater" id="updater" value="Update" class="mailsubmit" onclick="updateCart();">
<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
checkout"><input type="button" name="checkouter" id="checkouter" value="Checkout" class="mailsubmit"></a>
</div>
<a class="yellow" href="#">Continue Shopping</a>
</div>
<?php }else{ ?>
     <div class="emptyCart">
        <p style="font-size: 20px;">Shopping Cart is Empty.</p>
        <p style="font-size: 15px;">Click <a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
category">here</a> to continue shopping.</p>
    </div>
<?php }?>
</fieldset>
</div>   
</div>
</div>
</div>
</div>
</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script>
 function updateCart() {
  $("#frmShoppingCart").submit();
 }
</script>
<?php }} ?>