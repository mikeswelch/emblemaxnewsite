<?php /* Smarty version Smarty-3.1.11, created on 2013-09-02 08:36:24
         compiled from "application/views/templates/admin/requestcatalog/requestcatalog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11491968015224a2686d91b2-65225142%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9b0df98a2d2d3b42d342dd273dee3e5ed5cf79ff' => 
    array (
      0 => 'application/views/templates/admin/requestcatalog/requestcatalog.tpl',
      1 => 1378132551,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11491968015224a2686d91b2-65225142',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'state' => 0,
    'country' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5224a2688e3694_87167698',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5224a2688e3694_87167698')) {function content_5224a2688e3694_87167698($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
requestcatalog/requestcataloglist">Request Catalog</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Request Catalog<?php }else{ ?><?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Request Catalog
			<?php }else{ ?>
			Request Catalog		
			<?php }?>
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
            <div class="pagetitle">Personal Information</div>
                <div class="add_ad_contentbox">
                    <div class="inputboxes">
                            <label for="textarea"><span class="red_star"></span> Name</label>
                                    <span class="collan_dot">:</span>
                                    <label><?php echo ucfirst($_smarty_tpl->tpl_vars['data']->value[0]->vName);?>
</label>
                            <label for="textarea"><span class="red_star"></span> Email Address </label>
                                    <span class="collan_dot">:</span>
                                    <label><?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vEmail;?>
</label>
                            <label for="textarea"><span class="red_star"></span> Contact No. </label>
                                    <span class="collan_dot">:</span>
                                    <label><?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vPhone;?>
</label>
                            
                            <?php if ($_smarty_tpl->tpl_vars['data']->value[0]->vFax!=''){?>
                            <label for="textarea"><span class="red_star"></span> Fax </label>
                                    <span class="collan_dot">:</span>
                                    <label><?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vFax;?>
</label>
                            <?php }?>
                    </div>
                </div>
	</div>
	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
            <div class="pagetitle">Address</div>
                <div class="add_ad_contentbox">
                    <div class="inputboxes">
                            <label for="textarea"><span class="red_star"></span>Company Name</label>
                                    <span class="collan_dot">:</span>
                                    <label><?php echo ucfirst($_smarty_tpl->tpl_vars['data']->value[0]->vCompany);?>
</label>
                            <label for="textarea"><span class="red_star"></span> Address </label>
                                    <span class="collan_dot">:</span>
                                    <table>
                                        <tr>
                                            <td>
                                                <label style="font-size: 14px;"><?php echo ucfirst($_smarty_tpl->tpl_vars['data']->value[0]->vAddress);?>
</label>
                                            </td>
                                        </tr>
                                        <?php if ($_smarty_tpl->tpl_vars['data']->value[0]->vAddress1!=''){?>
                                        <tr>
                                            <td>
                                                    <label style="font-size: 14px;"><?php echo ucfirst($_smarty_tpl->tpl_vars['data']->value[0]->vAddress1);?>
</label>
                                            </td>
                                        </tr>
                                        <?php }?>
                                        <tr>
                                            <td>
                                            <label style="font-size: 14px;"><?php echo ucfirst($_smarty_tpl->tpl_vars['data']->value[0]->vCity);?>
</label>
                                                <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                                                        <?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['data']->value[0]->iStateId){?> <label style="font-size: 14px;"><?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
</label> <?php }?>
                                                <?php endfor; endif; ?>
                                            </td>
                                        </tr>
                                       <tr>
                                        <td>
                                            <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
                                                         <?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['data']->value[0]->iCountryId){?> <label style="font-size: 14px;"><?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
</label> <?php }?>
                                            <?php endfor; endif; ?>
                                            <label style="font-size: 14px;"><?php echo $_smarty_tpl->tpl_vars['data']->value[0]->vZip;?>
</label>
                                        </td>
                                        </tr>
                                    </table>
                    </div>
                </div>
	</div>

	<div class="centerpartbg" style="float:left;width:48%;margin-left:20px;min-height:100px;">
            <div class="pagetitle">Comment</div>
                <div class="add_ad_contentbox">
                    <div class="inputboxes">
                            <label for="textarea"><span class="red_star"></span> Comment</label>
                                    <span class="collan_dot">:</span>
                                    <label ><?php echo ucfirst($_smarty_tpl->tpl_vars['data']->value[0]->tNote);?>
</label>
                    </div>
                </div>
	</div>
	</div>
	<div class="clear"></div>

        <div style="margin-left: 52px;margin-top: 20px;">
          <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
requestcatalog/requestcataloglist"><input type="button" value="Back" class="submit_btn" title="Back" onclick="return validate(document.frmadd);" /></a>     
        </div>       

</div><?php }} ?>