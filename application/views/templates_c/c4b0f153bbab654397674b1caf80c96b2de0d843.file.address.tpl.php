<?php /* Smarty version Smarty-3.1.11, created on 2013-06-06 02:18:34
         compiled from "application/views/templates/address.tpl" */ ?>
<?php /*%%SmartyHeaderCode:143663877151af2d2ba8c820-62429338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c4b0f153bbab654397674b1caf80c96b2de0d843' => 
    array (
      0 => 'application/views/templates/address.tpl',
      1 => 1370506709,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '143663877151af2d2ba8c820-62429338',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51af2d2bd73fa8_61494225',
  'variables' => 
  array (
    'msg' => 0,
    'UserDetails' => 0,
    'BillShipDetails' => 0,
    'state' => 0,
    'country' => 0,
    'site_url' => 0,
    'ShippingDetails' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51af2d2bd73fa8_61494225')) {function content_51af2d2bd73fa8_61494225($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="main-container col2-right-layout">
<div class="main">
<div class="main-inner clearfix">
<div id="category-image" class="category-image-top"></div>
<div class="container">
<div class="row-fluid show-grid">
<div class="col-main-wap span9">
<div class="col-main">
<div class="col-main-inner">
<div class="my-account">
<div class="page-title title-buttons row-fluid show-grid">
<h1 class="pull-left">Address Book</h1>
<button class="button btn-book pull-right" onclick="window.location='http://192.168.1.12/magentowebapp/mt_robel/customer/address/new/';" title="Add New Address" type="button">
<span>
<span>Add New Address</span>
</span>
</button>
</div>
<?php if ($_smarty_tpl->tpl_vars['msg']->value){?>
<ul class="messages">
<li class="success-msg">
<ul>
<li>
<span><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</span>
</li>
</ul>
</li>
</ul>
<?php }?>
<div class="col2-set addresses-list row-fluid show-grid">
<?php if (count($_smarty_tpl->tpl_vars['UserDetails']->value)>0){?>
<div class="col-1 addresses-primary span6">
<h2>Default Addresses</h2>
<ol>

<li class="item">
<h3>Default Billing Address</h3>
                    <?php if (count($_smarty_tpl->tpl_vars['BillShipDetails']->value)>'0'){?>
		    <address>
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingFirstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingLastName;?>
<br/>
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->tBillingStreetAddress1;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->tBillingStreetAddress2;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingCity;?>
<br />
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->iBillingStateId){?> <?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->iBillingCountryId){?> <?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingZipCode;?>
<br/>
			T:<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingTelephone;?>

			<?php if ($_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingFax!=''){?><br/>F:<?php echo $_smarty_tpl->tpl_vars['BillShipDetails']->value[0]->vBillingFax;?>
<?php }?>
		    </address>
		    <?php }else{ ?>
		    </address>
		    You have not set a default billing address.<br/>
		    <!--<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Edit Address</a>-->
		    <?php }?>
                </address><p>
			<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Change Billing Address</a>
			</p>
			</li>
			<li class="item">
			<h3>Default Shipping Address</h3>
                <?php if (count($_smarty_tpl->tpl_vars['ShippingDetails']->value)>'0'){?>
		<address>
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingFirstName;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingLastName;?>
<br/>
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->tShippingStreetAddress1;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->tShippingStreetAddress2;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingCity;?>
<br />
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->iShippingStateId){?> <?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?>
			<div><?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->iShippingCountryId){?> <?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
 <?php }?></div>
			<?php endfor; endif; ?>
			<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingZipCode;?>
<br/>
			T:<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingTelephone;?>

			<?php if ($_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingFax!=''){?><br/>F:<?php echo $_smarty_tpl->tpl_vars['ShippingDetails']->value[0]->vShippingFax;?>
<?php }?>
			<address>
		       <?php }else{ ?>
		       </address>
		       You have not set a default Shipping address.<br/>
		       <!--<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Edit Address</a>-->
                <?php }?>
                </address>
<p>
<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/edit_address">Change Shipping Address</a>
</p>
</li>
</ol>
</div>
<div class="col-2 addresses-additional span6">
<h2>Additional Address Entries</h2>
<ol>
<li class="item empty">
<p>You have no additional address entries in your address book.</p>
</li>
</ol>
</div>
</div>
<div class="buttons-set">
<p class="back-link pull-left">
<?php }else{ ?>
You Have Not Mention Any Address Yet   
<?php }?>
</p>
<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/myDashboard">
<small style="float:left;">« </small>
<label style="float:left;">Back</label>
</a>
</p>
</div>
</div>
</div>
</div>
</div>
<div style="margin-top:104px;"><?php echo $_smarty_tpl->getSubTemplate ("right_myAccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
</div>
</div>
</div>
</div>
</div></div>

<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>