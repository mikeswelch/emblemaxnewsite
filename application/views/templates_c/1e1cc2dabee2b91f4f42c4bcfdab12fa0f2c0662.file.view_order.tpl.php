<?php /* Smarty version Smarty-3.1.11, created on 2013-06-01 05:30:57
         compiled from "application/views/templates/view_order.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44525393251a9db7124d3f6-49595454%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e1cc2dabee2b91f4f42c4bcfdab12fa0f2c0662' => 
    array (
      0 => 'application/views/templates/view_order.tpl',
      1 => 1370086123,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44525393251a9db7124d3f6-49595454',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'orderData' => 0,
    'state' => 0,
    'country' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51a9db714cd7e1_54040798',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51a9db714cd7e1_54040798')) {function content_51a9db714cd7e1_54040798($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home2/zerninph/public_html/php/emblemax/system/libs/smarty/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>"order details"), 0);?>

<div class="main-container col1-layout">
<div class="main container">
<div class="container">
<div class="row-fluid show-grid">
<div class="col-main-wap span9">
        <div class="col-main">
<div class="col-main-inner">
																				<div class="my-account"><div class="page-title title-buttons">
    <h1>Order <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vInvoiceNum;?>
 - <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->eStatus;?>
</h1>
    
   <!-- <a href="http://192.168.1.12/magentowebapp/mt_robel/sales/order/reorder/order_id/5/" class="link-reorder">Reorder</a>
    <span class="separator">|</span>
<a href="http://192.168.1.12/magentowebapp/mt_robel/sales/order/print/order_id/5/" class="link-print" onclick="this.target='_blank';">Print Order</a>-->
</div>
<dl class="order-info">
    <dt>About This Order:</dt>
    <dd>
                <ul id="order-info-tabs">
                                    <li class="current">Order Information</li>
                            </ul>
        
    </dd>
</dl>
<p class="order-date">Order Date: <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['orderData']->value[0]->dAddedDate);?>
</p>
<div class="col2-set order-info-box">
    <div class="col-1">
        <div class="box">
            <div class="box-title">
                <h2>Shipping Address</h2>
            </div>
            <div class="box-content">
                <address><?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vShippName;?>
<br/>
                    <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vShippCity;?>
<br />
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                    <?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['orderData']->value[0]->iShippStateId){?> <?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
 <?php }?>
                    <?php endfor; endif; ?><br />
                    , <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vShippPostalcode;?>
<br/>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?><?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['orderData']->value[0]->iShippCountryId){?> <?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
 <?php }?><?php endfor; endif; ?><br/>
                    <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vShippPhone;?>

                </address>
            </div>
        </div>
    </div>
    <div class="col-2">
        <div class="box">
            <div class="box-title">
                <h2>Shipping Method</h2>
            </div>
            <div class="box-content">
                                    Flat Rate - <?php if ($_smarty_tpl->tpl_vars['orderData']->value[0]->vShippingMethodTitle==''){?>N/A <?php }else{ ?> <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vShippingMethodTitle;?>
<?php }?>
            </div>
        </div>
    </div>
</div>
<div class="col2-set order-info-box">
    <div class="col-1">
        <div class="box">
            <div class="box-title">
                <h2>Billing Address</h2>
            </div>
            <div class="box-content">
                <address><?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vBillName;?>
<br/>
                  <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vBillCity;?>
<br />
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['state']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                    <?php if ($_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iStateId==$_smarty_tpl->tpl_vars['orderData']->value[0]->iBillStateId){?> <?php echo $_smarty_tpl->tpl_vars['state']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->vState;?>
 <?php }?>
                    <?php endfor; endif; ?><br />
                    , <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vBillPostalcode;?>
<br/>
                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['j'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['j']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['name'] = 'j';
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['country']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['j']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['j']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['j']['total']);
?><?php if ($_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->iCountryId==$_smarty_tpl->tpl_vars['orderData']->value[0]->iBillCountryId){?> <?php echo $_smarty_tpl->tpl_vars['country']->value[$_smarty_tpl->getVariable('smarty')->value['section']['j']['index']]->vCountry;?>
 <?php }?><?php endfor; endif; ?><br/>
                    <?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vBillPhone;?>

            </div>
        </div>
    </div>
    <div class="col-2">
        <div class="box box-payment">
            <div class="box-title">
                <h2>Payment Method</h2>
            </div>
            <div class="box-content">
                <p>Check / Money order</p>
            </div>
        </div>
    </div>
</div>
<div class="order-items order-details">
            <h2 class="table-caption">Items Ordered            </h2>

    <table class="data-table" id="my-orders-table" summary="Items Ordered">
    <col />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <col width="1" />
    <thead>
        <tr style="color:#FFFFFF">
            <th>Product Name</th>
            <th>SKU</th>
            <th class="a-right">Price</th>
            <th class="a-center">Qty</th>
            <th class="a-right">Subtotal</th>
        </tr>
    </thead>
 
    <tfoot>
                <tr class="subtotal">
        <td colspan="4" class="a-right">
                        Subtotal                    </td>
        <td class="last a-right">
                        <span class="price">$<?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->fSubTotal;?>
</span>                    </td>
    </tr>
            <tr class="shipping">
        <td colspan="4" class="a-right">
                        Shipping &amp; Handling                    </td>
        <td class="last a-right">
                        <span class="price"><?php if ($_smarty_tpl->tpl_vars['orderData']->value[0]->fShippmentCharge==''){?>N/A <?php }else{ ?>$<?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->fShippmentCharge;?>
<?php }?></span></td>
    </tr>
            <tr class="grand_total">
        <td colspan="4" class="a-right">
                        <strong>Grand Total</strong>
                    </td>
        <td class="last a-right">
                        <strong><span class="price">$<?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->total;?>
</span></strong>
                    </td>
    </tr>
        </tfoot>
   <?php if (count($_smarty_tpl->tpl_vars['orderData']->value)>0){?>
    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['orderData']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
        <tbody>
            <tr class="border" id="order-item-row-8">
    <td><h3 class="product-name"><?php echo $_smarty_tpl->tpl_vars['orderData']->value[0]->vProductName;?>
</h3>
                                                                </td>
    <td>023</td>
    <td class="a-right">
                    <span class="price-excl-tax">
                                                    <span class="cart-price">
                
                                            <span class="price">$<?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->fPrice;?>
</span>                    
                </span>


                            </span>
            <br />
                    </td>
    <td class="a-right">
        <span class="nobr" style="color: #002F5F;">
                            Ordered: <strong> <?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->iQty;?>
</strong><br />
                                        </span>
    </td>
    <td class="a-right">
                    <span class="price-excl-tax">
                                                    <span class="cart-price">
                                            <span class="price">$<?php echo $_smarty_tpl->tpl_vars['orderData']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]->fSubTotal;?>
</span>                    
                </span>


                            </span>
            <br />
                    </td>
    <!--
        <th class="a-right"><span class="price">$125.00</span></th>
            -->
</tr>
                    </tbody>
    <?php endfor; endif; ?>
    <?php }?>
        </table>
                <div class="buttons-set">
        <p class="back-link"><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
myAccount/myOrders"><small>&laquo; </small>Back to My Orders</a></p>
    </div>
</div></div>									</div>
								</div>
							</div>
 <div style="margin-top:68px;"><?php echo $_smarty_tpl->getSubTemplate ("right_myAccount.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
</div>
 
	</div> <!-- end class=mt-smartmenu --> 
<div class="block block-bestseller" id="block-bestseller">
	    <div class="title-divider">
        <span>&nbsp;</span>
    </div>
    <div class="block-content">
        <ol class="mini-products-list" id="bestseller-sidebar">
                    </ol>
  
    </div>
</div>
</div>
</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>