<?php /* Smarty version Smarty-3.1.11, created on 2013-11-21 08:59:28
         compiled from "application/views/templates/productstage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1460916728528e2de0a5ced2-54749803%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b3022ad0c2464f9db7e7f2e866ea20f0b08af0c' => 
    array (
      0 => 'application/views/templates/productstage.tpl',
      1 => 1385049557,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1460916728528e2de0a5ced2-54749803',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_528e2de0ab8925_15752411',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_528e2de0ab8925_15752411')) {function content_528e2de0ab8925_15752411($_smarty_tpl) {?><section class="fpd-product-container fpd-border-color">
	<div class="fpd-menu-bar fpd-clearfix fpd-main-color">
		<div class="fpd-menu">
			<ul class="fpd-clearfix">
				<li><span class="fpd-save-product icon-save fpd-main-color fpd-tooltip" title="Save product"></span></li>
				<li><span class="fpd-download-image icon-download fpd-main-color fpd-tooltip" title="Download Product Image"></span></li>
				<li><span class="fpd-print icon-print fpd-main-color fpd-tooltip" title="Print"></span></li>
			</ul>
		</div>
	</div>
	<!-- Kinetic Stage -->
	<div class="fpd-product-stage fpd-content-color">
		<canvas></canvas>
	</div>
</section><?php }} ?>