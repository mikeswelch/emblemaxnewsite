<?php /* Smarty version Smarty-3.1.11, created on 2013-06-29 07:52:49
         compiled from "application/views/templates/admin/bannergroups/bannergroup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:69549252651bf133e2720d2-76587567%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'efb3a35b32a227ab8781048f5a0afa3df2959c35' => 
    array (
      0 => 'application/views/templates/admin/bannergroups/bannergroup.tpl',
      1 => 1372505902,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '69549252651bf133e2720d2-76587567',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51bf133e3f8082_66796299',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51bf133e3f8082_66796299')) {function content_51bf133e3f8082_66796299($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
bannergroup/bannergrouplist">Banner Groups</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Banner Groups<?php }else{ ?>Edit Banner Groups<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add Banner Groups
			<?php }else{ ?>
			Edit Banner Groups			
			<?php }?>
		</div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iBannerGroupId" id="iBannerGroupId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iBannerGroupId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />
				
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Group Name</label>
					<span class="collan_dot">:</span>
					<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Group Name" readonly/>
					<?php }else{ ?>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Group Name" />
					<?php }?>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Height</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iHeight" name="Data[iHeight]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iHeight;?>
<?php }?>" lang="*" title="Height" onkeypress="return checkprise(event)"/>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Width</label>
					<span class="collan_dot">:</span>
					<input type="text" id="iWidth" name="Data[iWidth]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iWidth;?>
<?php }?>" lang="*" title="Width" onkeypress="return checkprise(event)"/>
				</div>	
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='InActive'){?>selected<?php }?><?php }?> >InActive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add Banner Group" class="submit_btn" title="Add Banner Group" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit Banner Group" class="submit_btn" title="Edit Banner Group" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
bannergroup/bannergrouplist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script type="text/javascript">

$("#fancyhref").fancybox({
    padding: 0,

    openEffect : 'elastic',
    openSpeed  : 250,

    closeEffect : 'elastic',
    closeSpeed  : 250,

    closeClick : true,

    helpers : {
	    overlay : null
    }
});


   function ImageDelete(id,file1){
	
    $('<div  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete image</div></div><div class="eor_poptxt"><img src="http://192.168.1.12/Izishirt/public/admin/images/eor-img.png" alt="" title="" /><h3 id="myModalLabel">Are you sure, You wnated to delete this image ?</h3></div><div class="view_del_user"><div class="delete_user"><a  class="delete_btnimg_large"  href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
color/deleteimage?id=<?php echo $_smarty_tpl->tpl_vars['data']->value->iColorId;?>
">Delete</a></div><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
}	

</script>
 
<?php }} ?>