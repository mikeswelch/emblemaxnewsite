<?php /* Smarty version Smarty-3.1.11, created on 2013-06-29 06:15:23
         compiled from "application/views/templates/admin/country/country.tpl" */ ?>
<?php /*%%SmartyHeaderCode:930483022515418052242f6-76366338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce30b71554eec67caa18fd8f485619405b544733' => 
    array (
      0 => 'application/views/templates/admin/country/country.tpl',
      1 => 1372505935,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '930483022515418052242f6-76366338',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51541805347ae4_21304975',
  'variables' => 
  array (
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51541805347ae4_21304975')) {function content_51541805347ae4_21304975($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<div class="centerpart">
    <div id="breadcrumb">
	  <ul>
		  <li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
		  <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
		  <li>/</li>
		  <li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
country/countrylist">Country</a></li>
		  <li>/</li>
		  <li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Country<?php }else{ ?>Edit Country<?php }?></li>
	  </ul>
    </div>
    <div class="centerpartbg">
           <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
            <div class="pagetitle">Add Country</div>
            <?php }else{ ?>
            <div class="pagetitle">Edit Country</div>
            <?php }?>
            <div class="add_ad_contentbox">
                    <form id="frmadd" name="frmadd" method="post" action="">
				<input type="hidden" name="iCountryId" id="iCountryId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iCountryId;?>
<?php }?>" />
				<input type="hidden" name="action" id="action" value="add" />
				
				<div class="inputboxes">
				    <label for="textfield"><span class="red_star">*</span> Country</label>
				    <span class="collan_dot">:</span>
				    <input type="text" id="vCountry" name="Data[vCountry]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vCountry;?>
<?php }?>" lang="*" title="Country" style="width:240px"/>
				</div>
                          <div class="inputboxes">
					         <label for="textfield"><span class="red_star">*</span> Country Code</label>
						    <span class="collan_dot">:</span>
					         <input type="text" id="vCountryCode" name="Data[vCountryCode]" maxlength="2" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vCountryCode;?>
<?php }?>" lang="*" title="Country Code" style="width:240px"/>
			              </div>					      				       
                            <div class="inputboxes">
                                    <label for="textfield"><span class="red_star"></span> Status</label>
							 <span class="collan_dot">:</span>
                                    <select id="eStatus" name="Data[eStatus]">
                                            <option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
                                            <option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
                                    </select>
                            </div>
                            <div class="add_can_btn">
                                    <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
                                    <input type="button" value="Add Country" class="submit_btn" title="Add Country" onclick="checkCountryCode('<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
');"/>
                                    <?php }else{ ?>
                                    <input type="button" value="Edit Country" class="submit_btn" title="Edit Country" onclick="checkCountryCode('<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
');"/>
                                    <?php }?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
country/countrylist" class="cancel_btn">Cancel</a>
                            </div>
                    </form>
            </div>
    </div>
    <div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>