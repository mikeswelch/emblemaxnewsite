<?php /* Smarty version Smarty-3.1.11, created on 2013-06-29 08:04:41
         compiled from "application/views/templates/admin/newsletter_format/newsletter_format.tpl" */ ?>
<?php /*%%SmartyHeaderCode:191703780651c038fb2d3a76-10869888%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7af0feb70c8324458f48150be96d81f227c1e487' => 
    array (
      0 => 'application/views/templates/admin/newsletter_format/newsletter_format.tpl',
      1 => 1372505949,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '191703780651c038fb2d3a76-10869888',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c038fb47d144_14909228',
  'variables' => 
  array (
    'Name' => 0,
    'ckeditor_path' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'action' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c038fb47d144_14909228')) {function content_51c038fb47d144_14909228($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist">Newsletter Format</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add Newsletter Format<?php }else{ ?>Update Newsletter Format<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg"> <?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
		<div class="pagetitle">Add Newsletter Format</div>
		<?php }else{ ?>
		<div class="pagetitle">Update Newsletter Format</div>
		<?php }?>
		<div class="edit_event_contentbox">
			<form id="frmadd" name="frmadd" method="post"  action="<?php echo $_smarty_tpl->tpl_vars['action']->value;?>
"  enctype="multipart/form-data">
				<input type="hidden" name="iNformatId" id="iNformatId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iNformatId;?>
<?php }?>" />
				<input type="hidden" name="action" id="action" value="add" />
				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span>&nbsp; Title</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vTitle" name="Data[vTitle]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vTitle;?>
<?php }?>" lang="*" title="Title" style="width:252px"/>
				</div>
				<div class="inputboxes worddocument_input">
					<label  for="textarea" ><span class="red_star">*</span>&nbsp; Content</label>
					<span class="collan_dot">:</span> 
					<div class="worddocument"><textarea id="tContent" name="Data[tContent]" class="inputbox" title="Content"><?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->tContent;?>
<?php }?></textarea></div>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span>&nbsp; Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				</div>
				<div class="clear"></div>
				<div class="add_can_btn">
					<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
					<input type="submit" value="Add Newsletter Format" class="submit_btn" title="Add Newsletter Format" onclick="return validate(document.frmadd);"/>
					<?php }else{ ?>
					<input type="submit" value="Update Newsletter Format" class="submit_btn" title="Update Newsletter Format" onclick="return validate(document.frmadd);"/>
					<?php }?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
newsletter_format/newsletter_formatlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
				</div>
			</form>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script type="text/javascript">
	CKEDITOR.replace('tContent');
</script>
<?php }} ?>