<?php /* Smarty version Smarty-3.1.11, created on 2013-09-10 10:55:06
         compiled from "application/views/templates/admin/printlocation/printlocation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:161924638251dc21212ace32-79382116%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '12809906a214ec27c0c51173f48ed07ceaa768e8' => 
    array (
      0 => 'application/views/templates/admin/printlocation/printlocation.tpl',
      1 => 1377257409,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '161924638251dc21212ace32-79382116',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51dc2121437c97_09303282',
  'variables' => 
  array (
    'ckeditor_path' => 0,
    'Name' => 0,
    'admin_image_path' => 0,
    'admin_url' => 0,
    'operation' => 0,
    'data' => 0,
    'Order' => 0,
    'ord' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51dc2121437c97_09303282')) {function content_51dc2121437c97_09303282($_smarty_tpl) {?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['ckeditor_path']->value;?>
ckeditor.js"></script>
<?php echo $_smarty_tpl->getSubTemplate ("admin/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('title'=>((string)$_smarty_tpl->tpl_vars['Name']->value)), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<div class="centerpart">
	<div id="breadcrumb">
		<ul>
			<li><img alt="Location" src="<?php echo $_smarty_tpl->tpl_vars['admin_image_path']->value;?>
icon_breadcrumb.png"></li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
dashboard">Dashboard</a></li>
			<li>/</li>
			<li><a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
printlocation/printlocationlist">Print Location</a></li>
			<li>/</li>
			<li class="current"><?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>Add PrintLocation<?php }else{ ?>Edit PrintLocation<?php }?></li>
		</ul>
	</div>
	<div class="centerpartbg">
		<div class="pagetitle">
			
			<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
			Add PrintLocation
			<?php }else{ ?>
                        Edit PrintLocation
			<?php }?> </div>
		
		<form id="frmadd" name="frmadd" method="post" enctype="multipart/form-data"  action="">
			<div class="add_ad_contentbox">
				<input type="hidden" name="iPrintLocationId" id="iPrintLocationId" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->iPrintLocationId;?>
<?php }?>" />
				<input type="hidden" name="operation" id="operation" value="<?php echo $_smarty_tpl->tpl_vars['operation']->value;?>
" />		

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> PrintLocation</label>
					<span class="collan_dot">:</span>
					<input type="text" id="vPrintLocation" name="Data[vPrintLocation]" class="inputbox" value="<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php echo $_smarty_tpl->tpl_vars['data']->value->vPrintLocation;?>
<?php }?>" lang="*" title="PrintLocation" />
				</div>

				<div class="inputboxes">
					<label for="textfield"><span class="red_star">*</span> Order</label>
					<span class="collan_dot">:</span>
					<select id="iOrder" name="Data[iOrder]"  title="Country" lang="*" style="width:250px" >
						<option value=''>-- Select Order --</option>
						<?php $_smarty_tpl->tpl_vars['ord'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['ord']->step = 1;$_smarty_tpl->tpl_vars['ord']->total = (int)ceil(($_smarty_tpl->tpl_vars['ord']->step > 0 ? $_smarty_tpl->tpl_vars['Order']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['Order']->value)+1)/abs($_smarty_tpl->tpl_vars['ord']->step));
if ($_smarty_tpl->tpl_vars['ord']->total > 0){
for ($_smarty_tpl->tpl_vars['ord']->value = 1, $_smarty_tpl->tpl_vars['ord']->iteration = 1;$_smarty_tpl->tpl_vars['ord']->iteration <= $_smarty_tpl->tpl_vars['ord']->total;$_smarty_tpl->tpl_vars['ord']->value += $_smarty_tpl->tpl_vars['ord']->step, $_smarty_tpl->tpl_vars['ord']->iteration++){
$_smarty_tpl->tpl_vars['ord']->first = $_smarty_tpl->tpl_vars['ord']->iteration == 1;$_smarty_tpl->tpl_vars['ord']->last = $_smarty_tpl->tpl_vars['ord']->iteration == $_smarty_tpl->tpl_vars['ord']->total;?>
							<option value='<?php echo $_smarty_tpl->tpl_vars['ord']->value;?>
'<?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?><?php if ($_smarty_tpl->tpl_vars['ord']->value==$_smarty_tpl->tpl_vars['data']->value->iOrder){?>selected<?php }?><?php }?> ><?php echo $_smarty_tpl->tpl_vars['ord']->value;?>
</option>
						<?php }} ?>

					</select>
				</div>
				<div class="inputboxes">
					<label for="textfield"><span class="red_star"></span> Status</label>
					<span class="collan_dot">:</span>
					<select id="eStatus" name="Data[eStatus]">
						<option value="Active" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Active'){?>selected<?php }?><?php }?>>Active</option>
						<option value="Inactive" <?php if ($_smarty_tpl->tpl_vars['operation']->value!='add'){?> <?php if ($_smarty_tpl->tpl_vars['data']->value->eStatus=='Inactive'){?>selected<?php }?><?php }?> >Inactive</option>
					</select>
				 </div>
				<div class="add_can_btn">
						<?php if ($_smarty_tpl->tpl_vars['operation']->value=='add'){?>
							<input type="submit" value="Add PrintLocation" class="submit_btn" title="Add PrintLocation" onclick="return validate(document.frmadd);"/>
						<?php }else{ ?>
							<input type="submit" value="Edit PrintLocation" class="submit_btn" title="Edit PrintLocation" onclick="return validate(document.frmadd);"/>
						<?php }?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['admin_url']->value;?>
printlocation/printlocationlist" style="text-decoration:none;" class="cancel_btn">Cancel</a> </div>
					<div class="clear"></div>
					
				</div>
			</div>
		</form>
	</div>
	<div class="clear"></div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("admin/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



 
<?php }} ?>