<?php
class MY_Controller extends CI_Controller{
    
    function __construct(){	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('upload');
        $this->load->library('image_lib');
        $filename = $this->config->item('filename');
        $this->smarty->assign("filename",$filename);
        $front_css_path = $this->config->item('front_css_path');
        $front_js_path = $this->config->item('front_js_path');
        $this->smarty->assign("front_css_path",$front_css_path);
        $this->smarty->assign("front_js_path",$front_js_path);		
        $fancybox_path = $this->config->item('fancybox_path');
        $this->smarty->assign("fancybox_path",$fancybox_path);
        $front_image_path = $this->config->item('front_image_path');
        $this->smarty->assign("front_image_path",$front_image_path);
        $site_url= $this->config->item('site_url');
        $this->smarty->assign("site_url",$site_url);
        $FB_PATH = $this->config->item('FB_PATH');
        $this->smarty->assign("FB_PATH",$FB_PATH);
        $upload_path = $this->config->item('upload_path');
        $this->smarty->assign("upload_path",$upload_path);
        $custom_tshirt_path = $this->config->item('custom_tshirt_path');
        $this->smarty->assign("custom_tshirt_path",$custom_tshirt_path);
	   $this->load->database();
	   $product_category = $this->getProduct_category()->result_array();
	   $this->smarty->assign("product_category",$product_category);

	   $promotional =  $this->getPromotional()->result();
	   $this->smarty->assign("promotional",$promotional);
	
	$config_twitter = $this->getConfigData('TWITTER_LINK');
        $config_facebook = $this->getConfigData('FACEBOOK_LINK');
        $config_flikr = $this->getConfigData('FLICKR_LINK');
        
        $config_contact = $this->getConfigData('CONTACT_NUMBER');
	$config_tollfree = $this->getConfigData('TOLL_FREE');
        $config_mobile = $this->getConfigData('MOBILE_NUMBER');
        $config_inquiry = $this->getConfigData('EMAIL_ADMIN');
	$config_google= $this->getConfigData('GMAIL_LINK');
	$config_wordpress= $this->getConfigData('WORDPRESS_LINK');
	
	$pageTitle= $this->getConfigData('PAGE_TITLE');
	$this->smarty->assign("pageTitle",$pageTitle);	

        $config_deliveryDate = $this->getConfigData('DELIVERY_DATE');
	$config_deliveryMonth = date("F", strtotime($config_deliveryDate));
	$config_deliveryMonth = substr($config_deliveryMonth,0,4);
	$config_deliveryDay = date("d", strtotime($config_deliveryDate));
	$this->smarty->assign("config_deliveryMonth",$config_deliveryMonth);
	$this->smarty->assign("config_deliveryDay",$config_deliveryDay);
	
	$categories = $this->getCategory()->result();
	$this->smarty->assign("categories",$categories);
	
	$footerContent = $this->getFooterAbout()->result();
	$this->smarty->assign("footerContent",$footerContent);
	
	$homeBanner = $this->getHomeBanner()->result_array();
		
	//echo "<pre>";print_r($homeBanner);exit;
	$this->smarty->assign("homeBanner",$homeBanner);	
	$static_aboutus = $this->getpageContent('about_us')->result_array();
	$this->smarty->assign("static_aboutus",$static_aboutus);
	$this->smarty->assign("config_tollfree",$config_tollfree);
	$this->smarty->assign("config_wordpress",$config_wordpress);
	$this->smarty->assign("config_twitter",$config_twitter);
        $this->smarty->assign("config_facebook",$config_facebook);
        $this->smarty->assign("config_flikr",$config_flikr);
	$this->smarty->assign("config_google",$config_google);
        $this->smarty->assign("config_contact",$config_contact);
        $this->smarty->assign("config_mobile",$config_mobile);
        $this->smarty->assign("config_inquiry",$config_inquiry);
	
        $recent_posts = $this->db->query("SELECT * FROM wp_posts where post_status='publish' AND post_type='post'   ORDER BY RAND()")->result_array();
        
	   
	   for($i=0;$i<count($recent_posts);$i++){
          $recent_posts[$i]['permalink'] =  $site_url."blog/".$recent_posts[$i]['post_name'] ;
	   }
       $this->smarty->assign("recent_posts",$recent_posts);
        
    
    
       //Twitter Updates
	function buildBaseString($baseURI, $method, $params) {
	    $r = array();
	    ksort($params);
	    foreach($params as $key=>$value){
		$r[] = "$key=" . rawurlencode($value);
	    }
	    return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
	}
	
	function buildAuthorizationHeader($oauth) {
	    $r = 'Authorization: OAuth ';
	    $values = array();
	    foreach($oauth as $key=>$value)
		$values[] = "$key=\"" . rawurlencode($value) . "\"";
	    $r .= implode(', ', $values);
	    return $r;
	}
	
	$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
	$oauth_access_token = $this->getConfigData('TWITTER_ACCESS_TOKEN');
	$oauth_access_token_secret = $this->getConfigData('TWITTER_ACCESS_TOKEN_SECRET');
	$consumer_key = $this->getConfigData('CONSUMER_KEY');
	$consumer_secret = $this->getConfigData('TWITTER_CONSUMER_SECTET');
	
	$standardDelivery = $this->getConfigData('STANDARD_DELIVERY_DAYS');
	$standardDeliveryDate = date("Y-m-d", strtotime("+ ".$standardDelivery." day"));
	$standardDeliveryMonth = date('F',strtotime($standardDeliveryDate));
	$standardDeliveryDay = date('d',strtotime($standardDeliveryDate));
	$this->smarty->assign("standardDeliveryMonth",$standardDeliveryMonth);
	$this->smarty->assign("standardDeliveryDay",$standardDeliveryDay);
	
	$rushDelivery = $this->getConfigData('RUSH_DELIVERY_DAYS');
	$rushDeliveryDate = date("Y-m-d", strtotime("+ ".$rushDelivery." day"));
	$rushDeliveryMonth = date('F',strtotime($rushDeliveryDate));
	$rushDeliveryDay = date('d',strtotime($rushDeliveryDate));
	$this->smarty->assign("rushDeliveryMonth",$rushDeliveryMonth);
	$this->smarty->assign("rushDeliveryDay",$rushDeliveryDay);
	
	
	
	$oauth = array( 'oauth_consumer_key' => $consumer_key,
			'oauth_nonce' => time(),
			'oauth_signature_method' => 'HMAC-SHA1',
			'oauth_token' => $oauth_access_token,
			'oauth_timestamp' => time(),
			'oauth_version' => '1.0');
	
	$base_info = buildBaseString($url, 'GET', $oauth);
	$composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
	$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
	$oauth['oauth_signature'] = $oauth_signature;
	
	// Make Requests
	$header = array(buildAuthorizationHeader($oauth), 'Expect:');
	$options = array( CURLOPT_HTTPHEADER => $header,
			  //CURLOPT_POSTFIELDS => $postfields,
			  CURLOPT_HEADER => false,
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_SSL_VERIFYPEER => false);
	
	   $feed = curl_init();
	   curl_setopt_array($feed, $options);
	   $json = curl_exec($feed);
	   curl_close($feed);	
	   $twitter_updates = json_decode($json);
        $this->smarty->assign("twitter_updates",$twitter_updates);
 
    }
    
    function getProduct_category(){
	$configData ="SELECT pc.*,pct.* FROM product_category pc LEFT JOIN product_category_translation pct ON (pc.iCategoryId = pct.iCategoryId) WHERE pc.eStatus='Active'";
	$query = $this->db->query($configData);
	return $query; 
    }
    
    function getConfigData($vName){
        $configData = $this->db->query("SELECT * FROM configurations where vName='".$vName."'")->result_array();
        return $configData[0]['vValue'];
    }
   
    function getPromotional(){
        $sql = "SELECT * FROM product WHERE ePromotion = 'Yes' AND eStatus = 'Active'";
	$query = $this->db->query($sql);
	return $query;
    }
   
    function getHomeBanner(){
	$sql = "SELECT * FROM home_slider WHERE eStatus = 'Active' ORDER BY iOrderNo ASC";
	$query = $this->db->query($sql);
	return $query;	
    }
    
    function getpageContent($vFileName){
	$sql = "SELECT * FROM static_pages WHERE vFile = '".$vFileName."'";
	$query = $this->db->query($sql);
	return $query;	
    }
    function getCategory(){
	$sql = "select pc.*, pct.* FROM product_category pc LEFT JOIN product_category_translation pct ON pc.iCategoryId = pct.iCategoryId WHERE pc.eStatus = 'Active' ORDER BY pc.iCategoryId ASC";
	return $this->db->query($sql);
    }
    function getFooterAbout(){
	$sql = "SELECT * FROM static_pages WHERE vFile = 'about_footer'";
	return $this->db->query($sql);
    }
}
?>
