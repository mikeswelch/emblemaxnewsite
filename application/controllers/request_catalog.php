<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_catalog extends CI_Controller {
     
     function __construct() {
		
	    parent::__construct();
	       
	       $this -> load -> model('requestcatalog_model', '', TRUE);
	       $this -> load -> model('registration_model', '', TRUE);
	       $this -> load -> helper('url');
	       $this->load->library('upload');
	       $this->load->library('image_lib');
	       /*$site_url = $this->config->item('site_url');
	       		if(isset($_SESSION["sess_iUserId"]) ==''){
			redirect(site_url);
			exit ;
		}
	       */
	       $front_css_path = $this->config->item('front_css_path');
	       $front_js_path = $this->config->item('front_js_path');
	       $this->smarty->assign("front_css_path",$front_css_path);
	       $this->smarty->assign("front_js_path",$front_js_path);		
	       $fancybox_path = $this->config->item('fancybox_path');
	       $this->smarty->assign("fancybox_path",$fancybox_path);
	       $front_image_path = $this->config->item('front_image_path');
	       $this->smarty->assign("front_image_path",$front_image_path);
	       $this->smarty->assign("site_url",$site_url);
	       $FB_PATH = $this->config->item('FB_PATH');
	       $this->smarty->assign("FB_PATH",$FB_PATH	);
	       $upload_path = $this->config->item('upload_path');
	       $this->smarty->assign("upload_path",$upload_path);
	       
	  }

	  function index()
	  {
	       //echo "<pre>";print_r($_SESSION);exit;
	       if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
		      $msg = $_SESSION['msg'];
		      unset($_SESSION['msg']);
	       }else{
		      $msg = '';
	       }
	       $this->smarty->assign("msg",$msg);
	       $site_url = $this->config->item('site_url');
	       $db_country = $this->registration_model->list_country()->result();
	       
	       $this->smarty->assign("db_country",$db_country);	  

               $this->smarty->assign("site_url",$site_url);
	       $this->smarty->view('request_catalog.tpl');
	  }
	  
	  function save(){
	       $data = $_REQUEST['Data'];
	       $id = $this->requestcatalog_model->save($data);
	             
	       //Send Mail
	       	    $vEmail=$data['vEmail'];
		    $vFax=$data['vFax'];
		    $vMessage=$data['tNote'];
		    $vAddress=$data['vAddress'];
		    $vPhone=$data['vPhone'];
		    $vCity=$data['vCity'];
		    $iStateId=$data['iStateId'];
		    $iCountryId=$data['iCountryId'];
		    $vCountryName=$this->requestcatalog_model->getCountry($iCountryId)->row();
		    $vCountry=$vCountryName->vCountry;

		    $vStateName=$this->requestcatalog_model->getState($iStateId)->row();
		    $vState=$vStateName->vState;
		    $vPincode=$data['vZip'];
		    $vCompanyName=$data['vCompany'];
		    $site_url = $this->config->item('site_url');
		    $site_nameuser=$this->requestcatalog_model->getsitename()->row();
		    $email_user=$this->requestcatalog_model->getemail()->row();
		    $emailuser=$email_user->vValue;
		    $sitenameuser=$site_nameuser->vValue;
		    $MAIL_FOOTER = 'support@emblemax.com';
		    $vName=$data['vName'];	
		    $bodyArr = array("#NAME#","#USERNAME#","#EMAIL#","#COMMENT#","#SITE_URL#","#MAIL_FOOTER#","#SITE_NAME#","#PHONE#","#FAX#","#COMPANYNAME#","#ADDRESS#","#CITY#","#STATE#","#COUNTRY#","#ZIPCODE#");
		    $postArr = array(ucfirst($vName),$vEmail,$vEmail,ucfirst($vMessage),$site_url,$MAIL_FOOTER,'emblemax',$vPhone,$vFax,ucfirst($vCompanyName),ucfirst($vAddress),ucfirst($vCity),ucfirst($vState),ucfirst($vCountry),$vPincode);
		    //$this->Send("CONTACT_US_ADMIN_INFORMATION","Administrator",'akankshita.satapathy@php2india.com',$bodyArr,$postArr,$sitenameuser,$emailuser);
		    $this->Send("CONTACT_US_ADMIN_INFORMATION","Administrator",$emailuser,$bodyArr,$postArr,$sitenameuser,$emailuser);
		    $this->Send("CONTACT_US_INFORMATION","Member",$vEmail,$bodyArr,$postArr,$sitenameuser,$emailuser);
		    if($id){
			 $_SESSION['msg'] = "Thank you for contacting us. We will get in touch with you shortly.";
		    }
		    else
		    {
			 $_SESSION['msg']="Error-in add.";
		    }
	       
	       redirect($site_url.'request_catalog');
	       exit;
	  }
	  
	  
	  
	  
	  public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr,$sitenameuser,$emailuser){
	     	
	       $site_url = $this->config->item('site_url');		
		$ssql = "AND vEmailCode='".$EmailCode."' AND eSendType ='".$SendType."'";
		//echo $ssql;exit;
		$email_info = $this->requestcatalog_model->list_sysemaildata($ssql)->result();
		//echo $ssql;exit;
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: emblemax.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
				
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		
		$this->body = str_replace($bodyArr,$postArr, $this->body);
	          
	  	$To = stripcslashes($ToEmail);
		
		$htmlMail = '<!DOCTYPE html>
			<html>
			<head>
			<title>Emblemax</title>
			</head>
			<body>
			<div style=" float:left; border:2px solid #46606D; width:700px; margin:0 auto; height:auto; background:url('.$site_url.'public/front-end/images/mid-conbg.jpg) repeat left top;">
				<div style="width:700px;  margin:auto;">
					<div style="width:700px; float:left; background:#b2c8d3; height:auto;">
					<h1 style="margin:10px;"><img src="'.$site_url.'public/front-end/images/email_logo.png" alt="" height="50px"/></h1>
					</div>
					<div style="float:left; width:640px; height:auto; padding:30px; background:url('.$site_url.'public/front-end/images/bg_template_header.png) no-repeat;">
					'.$this->body.'
					</div>
					<div style="width:700px; float:left; background:#d1e2ea; height:auto;">
					<div style="float:left;">
					<img style="width:64px; height:64px; float:left; padding:12px;" src="'.$site_url.'public/front-end/images/email.png" alt="">
					<span style="float:left; width:70%; margin:20px 0 0 0; font-family:droid_sansregular; font-size:1.063em;">Kind Regards,Emblemax</span>
					<a href='.$site_url.' style="float:left; width:70%; text-decoration:none; color:#46606d; font-family:droid_sansregular;font-size:14px; ">'.$sitenameuser.'</a>
					<a href="#" style="float:left; width:70%; text-decoration:none; color:#46606d; font-family:droid_sansregular;font-size:14px; ">'.$emailuser.'</a>
					</div>
				</div>
			</div>
			</body>
			</html>';
	      
		if($_SERVER['SERVER_ADDR'] == '192.168.1.41'){ // for localhost server
			require_once "Mail.php";
			require_once "Mail/mime.php";
			$from = "demo2.testing2@gmail.com";
			//$EMAIL_ADMIN = $this->getConfigData('EMAIL_ADMIN');
			//echo $EMAIL_ADMIN;exit;
			$to = 'parthiv.patel@php2india.com';
			$subject = "Welcome to emblemax!";
			$crlf = "\n";
			$html = "<h1> This is HTML </h1>";
			$headers = array('From' => $from,'To' => $to,'Subject' => $subject);
			
			$host = "smtp.gmail.com";
			$username = "demo2.testing2@gmail.com";
			$password = "demo1234";
			$mime =  new Mail_mime(array('eol' => $crlf));
			$mime->setHTMLBody($htmlMail);
			$body = $mime->getMessageBody();
			$headers = $mime->headers($headers);
			
			$smtp = Mail::factory("smtp",array("host" => $host,"auth" => true,"username" => $username,"password" => $password));
			$res = $smtp->send($to, $headers, $body);
					
		}else{ // for live server
			 $res = mail($To,$Subject,$htmlMail,$headers);
		}
	       
	       if($res){
		    return 'Thank you for contacting us. We will get in touch with you shortly.';
	       }else{
		    return 'You Can Not Contacting To Us.';
	       }
	}
}
	  
