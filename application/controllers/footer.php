<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Footer extends CI_Controller {
     
     function __construct() {
		
	    parent::__construct();
	       
	       $this -> load -> model('footer_model', '', TRUE);
	       $this -> load -> helper('url');
	       $this->load->library('upload');
	       $this->load->library('image_lib');
	       $site_url = $this->config->item('site_url');
	       $front_css_path = $this->config->item('front_css_path');
	       $front_js_path = $this->config->item('front_js_path');
	       $this->smarty->assign("front_css_path",$front_css_path);
	       $this->smarty->assign("front_js_path",$front_js_path);
	       $fancybox_path = $this->config->item('fancybox_path');
	       $this->smarty->assign("fancybox_path",$fancybox_path);
	       $front_image_path = $this->config->item('front_image_path');
	       $this->smarty->assign("front_image_path",$front_image_path);
	       $this->smarty->assign("site_url",$site_url);
	       $admin_js_path = $this->config->item('admin_js_path');
	       $this->smarty->assign("admin_js_path",$admin_js_path);
	       $FB_PATH = $this->config->item('FB_PATH');
	       $this->smarty->assign("FB_PATH",$FB_PATH	);
	       $upload_path = $this->config->item('upload_path');
	       $this->smarty->assign("upload_path",$upload_path);
	  
	  }

	  function index()
	  {
	       $this->smarty->assign("home_data",$home_data);
	       $site_url = $this->config->item('site_url');
	       $front_js_path = $this->config->item('front_js_path');
	       $this->smarty->assign("front_js_path",$front_js_path);
	       $front_css_path = $this->config->item('front_css_path');
	       $this->smarty->assign("front_css_path",$front_css_path);
	       $front_image_path = $this->config->item('front_image_path');
	       $this->smarty->assign("front_image_path",$front_image_path);
	       $this->smarty->assign("site_url",$site_url);
	       $this->smarty->assign("name","Find a freelancer");
	       
	       $this->smarty->view('footer.tpl');
	  }
	  function newsletter(){
	       $email = $_GET['vNewsletter'];
	       $newsData = $this->footer_model->checkNews($email)->result_array();
	       if(count($newsData) > 0){
		    echo "exists";exit;
	       }
	       else{
		    $Data['vEmail'] = $email;
		    $Data['dAddedDate'] = date('Y-m-d H:i:s');
		    $iNewsId = $this->footer_model->save($Data);
		    if($iNewsId != ''){
			 echo "success";exit;
		    }
	       }
	  }
}	  