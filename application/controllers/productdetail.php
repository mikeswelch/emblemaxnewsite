<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productdetail extends MY_Controller {
     
     function __construct() {
	       parent::__construct();
	       $this -> load -> model('productdetail_model', '', TRUE);
	  }
	  function index(){
               $this->download();
          }
          
          function download()
          {
              $iProductId= basename($_SERVER['REQUEST_URI']);
              $productData = $this->productdetail_model->getData($iProductId)->result();
              $upload_path = $this->config->item('upload_path');
              $this->smarty->assign("productData",$productData);
              $this->smarty->assign("upload_path",$upload_path);
              
              $this->smarty->view('product_detail.tpl');
        
              if($_REQUEST['id'])
              {
                    $id = $_REQUEST['id'];
                    $productData = $this->productdetail_model->getData($id)->result();
                    $vFile = $productData[0]->vSizeChart;
                    $dir = $this->config->item('upload_image_path');
                    header('Content-Type: application/pdf');
                    header('Content-Disposition: attachment; filename=sizechart.pdf');
                    header('Pragma: no-cache');
                    readfile($dir."/product/".$id."/".$vFile);
                    redirect(site_url.'productdetail/'.$id);
                    //exit;                  
                      
              }
          }
}