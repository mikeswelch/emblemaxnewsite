
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller{
	 function __construct(){
		  parent::__construct();
		  $this -> load -> model('search_model', '', TRUE);
		  if ($_REQUEST['per_page']){
		      $this->limit = $_REQUEST['per_page'];
		  }else{
		      $this->limit =3;
		  }
		  $name = 'category';
		  $this->smarty->assign("name",$name);
	 }
	 function index() {
		  /*
		  echo "<pre>";
		  print_r($_REQUEST);
		  exit;
		  */
		  if(isset($_REQUEST['start'])){
		      $start = $_REQUEST['start'];
		  }else{
		      $start = 0;
		  }
		  if(isset($_REQUEST['q']) != ''){
		     $alp = $_REQUEST['q'];
		     //echo $alp;exit;
		  }
		  
		  $totalData =  $this->search_model->getProductProduct($alp)->num_rows;
		  
		  $this->rec_limit = $this->limit;
		  $rec_limit = $this->rec_limit;
		  $var_limit = "";
		  
		  if($start != 0){
		      $num_limit = ($start-1)*$rec_limit;
		      $var_limit = " LIMIT $num_limit, $rec_limit";
		  }else{
		      $var_limit = " LIMIT 0, $rec_limit";
		      $start = 1;
		  }
		  //echo $rec_limit;exit;
		  
		  $site_path = $this->config->item('site_path');	     
		  include($site_path."system/libraries/class.paging-ajax.php");
		  $PagingObj = new Paging($totalData,$start,'searchPaginationProduct',$rec_limit);
		  
		  //$recmsg = $PagingObj->setMessage('Category');
		  $pages = $PagingObj->displayPaging();
		  
		  $allProduct = $this->search_model->getProductProduct($alp,$var_limit)->result_array();
		  
		  //$this->smarty->assign("recmsg",$recmsg);
		  $this->smarty->assign("pages",$pages);
		  
		  $alp = $_REQUEST['q'];
		  
		  $this->smarty->assign("alp",$alp);
		  $this->smarty->assign("allProduct",$allProduct);
		  if(isset($_REQUEST['ajax']) == 'yes'){
			   $this->smarty->view('aj-search.tpl');
		  }else{
			   $this->smarty->view('search.tpl'); 
		  }
	 }
}
