<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Design_tshirt extends MY_Controller {
     
     function __construct() {
	  parent::__construct();
	  $this->load->library('upload');
	  $this->load->library('image_lib');
	  $this->load->model('designtshirt_model', '', TRUE);
     }

     function index(){
	  $product_category = $this->designtshirt_model->getParentCatNew('0','','0','1','0');
	  $all_products = $this->designtshirt_model->getAllProducts()->result();
	  $this->smarty->assign("all_products",$all_products);
	  $this->smarty->assign("product_category",$product_category);
	  $this->smarty->view('custom_design.tpl');
     }
     function upload_art(){
	  $randomDesignId = 'CUSTOM'.rand();
	  $imageUploaded = $this->do_upload('vUploadImage',$randomDesignId);
	  $product_category = $this->designtshirt_model->getParentCatNew('0','','0','1','0');
	  $all_products = $this->designtshirt_model->getAllProducts()->result();
	  $this->smarty->assign("all_products",$all_products);
	  $this->smarty->assign("product_category",$product_category);
	  $this->smarty->view('ajax_custom_design.tpl');
	  
	  /*
	  echo $imageUploaded;exit;
	  
	  
	  if($imageUploaded != ''){
	       $data = array(
		    'iDesignImageId' => $randomDesignId,
		    'iDesignCategoryId' => 0,
		    'vImage' => $imageUploaded,
	       );
	       $_SESSION['design_images'][] = (object) $data;
	       $_SESSION['design_images_uploaded'] = 1;
	       echo "success"; exit;
	  }echo "error";exit;*/
     }
     function do_upload($field,$randomDesignId){
	  if(!is_dir('uploads/design_art/')){
	       @mkdir('uploads/design_art/', 0777);
	  }
	  if(!is_dir('uploads/design_art/'.$randomDesignId.'/')){
	       @mkdir('uploads/design_art/'.$randomDesignId.'/', 0777);
	  }
	  
	  $config = array(
	     'allowed_types' => "jpg|jpeg|gif|png",
	     'upload_path' => 'uploads/design_art/'.$randomDesignId,
	     'max_size'=>2000
	  );
	  
	  $this->upload->initialize($config);
	  $this->upload->do_upload($field); //do upload
	    
	  $image_data = $this->upload->data(); //get image data
	  
	  $config1 = array(
	     'source_image' => $image_data['full_path'], //get original image
	     'new_image' => 'uploads/design_art/'.$randomDesignId.'/195x131_'.$image_data['file_name'], //save as new image //need to create thumbs first
	     'maintain_ratio' => false,
	     'width' => 195,
	     'height' => 131
	  );
	  $this->image_lib->initialize($config1); //load library
	  $test1 = $this->image_lib->resize(); //do whatever specified in config
	  $this->image_lib->clear();
	  
	  $img_uploaded = $image_data['file_name'];
	  unset($config);
	  unset($image_data);
	  return $img_uploaded;
     }
}