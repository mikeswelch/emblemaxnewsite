<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Design_studio extends MY_Controller {     
	function __construct() {
		parent::__construct();
	}

	function index(){	 
		$this->smarty->view('design_studio.tpl');
	}

	function sidebar(){        
        $this->smarty->view('sidebar.tpl');
    }
    function productstage(){
        $this->smarty->view('productstage.tpl');   
    }
}