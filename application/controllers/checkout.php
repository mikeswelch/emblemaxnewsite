<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends MY_Controller{

	function __construct(){	
		parent::__construct();
		$this->load->model('checkout_model', '', TRUE);
		$name = 'checkout';
		$this->smarty->assign("name",$name);
	}
	function index() {
		if(!isset($_SESSION['cart']['item']) || !count($_SESSION['cart']['item'])){
			redirect(site_url.'cart');	
		}
		
		if(isset($_REQUEST['msg']) && $_REQUEST['msg'] != ''){
			$msg = $_REQUEST['msg'];
		}else{
			$msg = '';
		}
		$this->smarty->assign("msg",$msg);
		
		if(isset($_SESSION['sess_iUserId']) && $_SESSION['sess_iUserId'] != ''){
			$isLoggedIn = 1;
			$userAddress = $this->checkout_model->getUserAddress($_SESSION['sess_iUserId'])->row_array();
			$userAddress['vEmail'] = $_SESSION['sess_vEmail'];
			$iUserId = $_SESSION['sess_iUserId'];
			$this->smarty->assign("userAddress",$userAddress);
		}else{
			$iUserId = 0;
			$isLoggedIn = 0;
		}
		$this->smarty->assign("isLoggedIn",$isLoggedIn);
		$this->smarty->assign("iUserId",$iUserId);
		
		$cart = $_SESSION['cart'];
		$db_country = $this->checkout_model->getAllcountries()->result();
		$this->smarty->assign("cart",$cart);
		$this->smarty->assign("db_country",$db_country);
		$this->smarty->view('checkout.tpl');
	}
	function login(){
		$vUserName = $_POST['vUsername'];
		$vPassword = $this->encrypt($_POST['vPassword']);
		$userData = $this->checkout_model->getUserData($vUserName,$vPassword)->row_array($vUserName,$vPassword);
		if(count($userData)){
			$_SESSION['sess_iUserId'] = $userData['iUserId'];
			$_SESSION["sess_vUserName"]= $userData['vUsername'];
			$_SESSION['sess_vFirstName'] = $userData['vFirstName'];
			$_SESSION['sess_vLastName'] = $userData['vLastName'];
			$_SESSION['sess_vEmail'] = $userData['vEmail'];
			$_SESSION['sess_UserName'] = $_SESSION["sess_vFirstName"]." ".$_SESSION["sess_vLastName"];
			redirect(site_url.'checkout');	
		}else{
			redirect(site_url.'checkout?msg=login+failed');
		}
	}
	function encrypt($data){
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++){
			$c = 255 & ($key ^ ($c << 1));
			$table[$key] = $c;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++){
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return base64_encode($data);
	}
	
	function save_order(){
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$fSubTotal = $_SESSION['cart']['SUBTOTAL'];
		$fGrandTotal = $_SESSION['cart']['GRANDTOTAL'];
		$iUserId = $_POST['iUserId'];
		$paymentMethod = $_POST['payment_method'];
		
		$stateId = $_POST['iBillStateId'];
		$stateName = $this->checkout_model->getStateNameById($stateId)->row_array();
		$countryId = $_POST['iBillCountryId'];
		$countryCode = $this->checkout_model->getCountryCodeById($countryId)->row_array();
		
		//Processing Credit Card payment
		$result_array  = array();
		if($paymentMethod == 'CreditCard'){
			
			//Taking Credentials From General Configuration
			$sandboxMode = $this->getConfigData('SANDBOX');
			if($sandboxMode == 'n'){
			     $sandbox = FALSE;
			}else{
			     $sandbox = TRUE;
			}
			$api_version = '85.0';
			$api_endpoint = $sandbox ? 'https://api-3t.sandbox.paypal.com/nvp' : 'https://api-3t.paypal.com/nvp';
			$api_username = $this->getConfigData('PAYPAL_API_USERNAME');
			$api_password = $this->getConfigData('PAYPAL_API_PASSWORD');
			$api_signature = $this->getConfigData('PAYPAL_API_SIGNATURE');
			
			$api_test_cardtype = 'Visa';
			$api_test_card_number = '4662657870070982';
			$api_test_cvv = '000';
			
			// Store request params in an array
			$request_params = array
			(
			     'METHOD' => 'DoDirectPayment', 
			     'USER' => $api_username, 
			     'PWD' => $api_password, 
			     'SIGNATURE' => $api_signature, 
			     'VERSION' => $api_version, 
			     'PAYMENTACTION' => 'Sale', 					
			     'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
			     'CREDITCARDTYPE' => $_POST['cardType'], 
			     'ACCT' => $_POST['cardNumber'],
			     'EXPDATE' => $_POST['expiryDate'],
			     'CVV2' =>  $_POST['cvvNumber'],
			     'FIRSTNAME' => $_POST['firstnameOnCard'], 
			     'LASTNAME' => $_POST['lastnameOnCard'], 
			     'STREET' => $_POST['vBillAddress'], 
			     'CITY' => $_POST['vBillCity'], 
			     'STATE' => $stateName['vState'], // eg Gujarat, Ha Noi
			     'COUNTRYCODE' => $countryCode['vCountryCode'], // eg IN, CA, US
			     'ZIP' => $_POST['vBillPostcode'],
			     'AMT' => $fGrandTotal, 
			     'CURRENCYCODE' => 'USD',
			     'DESC' => 'Emblemax Order'
			);
			// Loop through $request_params array to generate the NVP string.
			$nvp_string = '';
			foreach($request_params as $var=>$val)
			{
				$nvp_string .= '&'.$var.'='.urlencode($val);	
			}
			
			// Send NVP string to PayPal and store response
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_VERBOSE, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl, CURLOPT_TIMEOUT, 30);
			curl_setopt($curl, CURLOPT_URL, $api_endpoint);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
			$result = curl_exec($curl);
			curl_close($curl);
			
			// Parse the API response
			$result_array = $this->NVPToArray($result);
			
			//Credit Card Payment Failure 
			if($result_array['ACK'] == 'Failure' || $result_array['ACK'] == 'FailureWithWarning'){
			     echo 'Failure|'.$result_array['L_LONGMESSAGE0'];
			     exit;
			}
		}
		
		
		
		// New User Registration During Checkout
		
		if(!$_POST['isGuest']){			
			$newUser = array();
			$newUser['vUserName'] = $_POST['vUsernameNew'];
			$newUser['vPassword'] = $this->encrypt($_POST['vPasswordNew']);
			$newUser['vFirstName'] = $_POST['vBillFirstname'];
			$newUser['vLastName'] = $_POST['vBillLastname'];
			$newUser['vEmail'] = $_POST['vBillEmail'];
			$newUser['vPhone'] = $_POST['vBillPhone'];
			$newUser['vZipCode'] = $_POST['vBillPostcode'];
			$newUser['vCity'] = $_POST['vBillCity'];
			$newUser['iCountryId'] = $_POST['iBillCountryId'];
			$newUser['iStateId'] = $_POST['iBillStateId'];
			$newUser['tAddress'] = $_POST['vBillAddress'];
			$newUser['dAddedDate'] = date('Y-m-d H:i:s');
			$newUser['eStatus'] = 'Active';
			
			
			$iUserId = $this->checkout_model->saveNewUser($newUser);
			
			$_SESSION['sess_iUserId'] = $iUserId;
			$_SESSION["sess_vUserName"]=$newUser['vUserName'];
			$_SESSION['sess_vFirstName'] = $newUser['vFirstName'];
			$_SESSION['sess_vLastName'] = $newUser['vLastName'];
			$_SESSION['sess_vEmail'] = $newUser['vEmail'];
			$_SESSION['sess_UserName'] = $_SESSION["sess_vFirstName"]." ".$_SESSION["sess_vLastName"];
			
		}
		
		$data = array();
		$data['iUserId'] = $iUserId;
		$data['iQuoteId']=$_SESSION['iQuoteId'];
		$data['vBillFirstname'] = $_POST['vBillFirstname'];
		$data['vBillLastname'] = $_POST['vBillLastname'];
		$data['vBillCompany'] = $_POST['vBillCompany'];
		$data['iBillCountryId'] = $_POST['iBillCountryId'];
		$data['iBillStateId'] = $_POST['iBillStateId'];
		$data['vBillCity'] = $_POST['vBillCity'];
		$data['vBillPostcode'] = $_POST['vBillPostcode'];
		$data['vBillAddress'] = $_POST['vBillAddress'];
		$data['vBillPhone'] = $_POST['vBillPhone'];
		$data['vBillEmail'] = $_POST['vBillEmail'];
		
		$data['vShipFirstname'] = $_POST['vShipFirstname'];
		$data['vShipLastName'] = $_POST['vShipLastname'];
		$data['vShipCompany'] = $_POST['vShipCompany'];
		$data['iShipCountryId'] = $_POST['iShipCountryId'];
		$data['iShipStateId'] = $_POST['iShipStateId'];
		$data['vShipCity'] = $_POST['vShipCity'];
		$data['vShipPostcode'] = $_POST['vShipPostcode'];
		$data['vShipAddress'] = $_POST['vShipAddress'];
		$data['vShipPhone'] = $_POST['vShipPhone'];
		$data['vShipEmail'] = $_POST['vShipEmail'];
		$data['iShippingMethodId'] = $_SESSION['cart']['iTurnAroundTimeId'];
		$data['fShippingCharge'] = $_SESSION['cart']['fTurnAroundTimeCharge'];

		$data['vPaymentMethod'] = $_POST['payment_method'];
		
		$data['fSubTotal'] = $fSubTotal;
		$data['fGrandTotal'] = $fGrandTotal;
		
		$data['dAddedDate'] = date('Y-m-d H:i:s');
		// Keeping Status Pending in case of Paypal
		$data['eStatus'] =  'Pending';	
		
		// Storing Paypal Pro (CC) response of SUCCESS
		if($paymentMethod == 'CreditCard'){
			if(isset($result_array['TRANSACTIONID'])){
				$data['vCreditCardTransactionId'] = $result_array['TRANSACTIONID'];
				$data['eStatus'] = 'Processing';
				$data['vNameOnCard'] = $_POST['firstnameOnCard'].' '.$_POST['lastnameOnCard'];
				$data['vCardNumber'] = $_POST['cardNumber'];
				$data['vCvv'] = $_POST['cvvNumber'];
				$data['vExpireDate'] = $_POST['expiryDate'];
				$data['vCardType'] = $_POST['cardType'];
			}
		}		
		$orderId = $this->checkout_model->saveOrder($data);		
		$data_update['vInvoiceNumber'] = '#EMB'.str_pad($orderId,5,"0",STR_PAD_LEFT);
		$updateInv = $this->checkout_model->updateOrder($data_update,$orderId);	
		
		//save order comment
		$comment_data = array();
		$comment_data['iOrderId'] = $orderId;
		$comment_data['tComments'] = $_POST['tComment'];
		$comment_data['dDate'] = date('Y-m-d H:i:s');
		$comment_data['eStatus'] = 'Active';
		$commentId = $this->checkout_model->saveOrderComment($comment_data);
		
		/*		 
		 user invoice email template coding start	 
		 */
		
		
		$totalColorSizes=array();
		$totalColorSizes=$_SESSION['quoteSummary']['vColorSizeDescription'];
		$totalQty=$_SESSION['quoteSummary']['totalQty'];  
	  
	  
	  $printlocationNumberofcolorsData=array();
	  $tPrintlocationNumberofcolorsData=$_SESSION['quote']['tPrintlocationNumberofcolorsData'];	  
	  for($i=0;$i<count($tPrintlocationNumberofcolorsData);$i++){
	       //screenprinting on Left chest
	       $printlocation = $this->checkout_model->getPrintLocationById($tPrintlocationNumberofcolorsData[$i]['print_location'])->row_array();
	       if($tPrintlocationNumberofcolorsData[$i]['decoration_type'] == 'screenprinting'){
		    $number_of_colors = $this->checkout_model->getNumberOfColorsById($tPrintlocationNumberofcolorsData[$i]['number_of_colors'])->row_array();
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'].' ( '.$number_of_colors['vNumberColor'].' )';    
	       }else{
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'];
	       }
	  }  
	  
	  //echo count($quoteSummary['vPrintLocationsDescription']);
	  //echo "<pre>";print_r($quoteSummary);//exit;
	  
	  
	  //$_SESSION['quoteSummary']['vPrintLocationsDescription']
	  
	  $getShippingMethods=$this->checkout_model->getShippingMethods($_SESSION['cart']['iTurnAroundTimeId'])->row();
	  $shippingMethod=$getShippingMethods->vTitle.'  '.$getShippingMethods->tNote; 
	  
	  
	  $subTotal=$_SESSION['cart']['SUBTOTAL'];
	  $shipmentCharge=$_SESSION['cart']['fTurnAroundTimeCharge'];	  
	  $couponDiscount=$_SESSION['cart']['fCouponDiscounte'];
	  $grandTotal=$_SESSION['cart']['GRANDTOTAL'];
	  $costPerPiece=round($subTotal/$totalQty);
	  
	  $table='<div style="float: left;width: 100%;padding: 10px 0 10px 0;">
			<table style="border-bottom:2px solid #EAEAEA" class="cart-table table table-bordered" id="shopping-cart-table">
			<colgroup>
				<col width="1">
				<col>
				<col width="1">
				<col width="1">
				<col width="1">
				<col width="1">
				<col width="1">
			</colgroup>
			<thead style="background-color: #002f5f;">
				<th width="100" rowspan="1" class="" style="color:white;"><span class="nobr">Image</span></th>
				<th width="200" rowspan="1" class="" style="color:white;"><span class="nobr">Product Name</span></th>
				<th width="200" rowspan="1" class="" style="color:white;"><span class="nobr">Selected Print locations</span></th>
				<th width="200" rowspan="1" class="" style="color:white;"><span class="nobr">Selected Color - Size</span></th>
				<th width="100" colspan="1" style="color:white;"><span class="nobr">Total Qty</span></th>
				<th rowspan="1" style="color:white;"><span class="nobr">Total</span></th>
			</thead>
			';
		$table.='<tbody>';
		$table.='<tr>';
		//echo "<pre>";print_r($_SESSION['cart']);exit;
		for($i=0;$i<count($_SESSION['cart']['item']);$i++){
			$imagePath=$upload_path.'product/'.$_SESSION['cart']['item'][$i]['iProductId'].'/'.$_SESSION['cart']['item'][$i]['vImage'];
			$table.='<td width="100"><img width="100px" src="'.$imagePath.'" alt="" /></td>'; //image 
			$table.='<td width="200">'.$_SESSION['cart']['item'][$i]['vProductName'].'</td>'; //product name
			$table.='<td width="200">';														
			for($j=0;$j<=count($_SESSION['cart']['item'][$i]['vPrintLocationsDescription']);$j++){  //print location				 
		       $table.=$_SESSION['cart']['item'][$i]['vPrintLocationsDescription'][$j].'<br>';		  
			}
			//$table.='<td/>';			
			$table.='<td width="200">';
			for($k=0;$k<count($totalColorSizes);$k++){
		       $table.=$totalColorSizes[$k].'<br>';	  
			}
			//$table.='<td/>';			
			$table.=' <td width="100">'.$_SESSION['cart']['item'][$i]['vTotalQty'].'</td>';
			$table.=' <td width="100">'.$_SESSION['cart']['item'][$i]['fQuoteTotal'].'</td>';
		}	
		$table.='</tr>';		
		$table.='</tbody>';
		$table.='</table>';
		$table.='</div>';
		$table.='<div style="clear:both;"></div>';
		
		$table.='<div style="width: 520px;float: right;">';
		$table.='<span style="border-bottom: 1px dashed #C2CBD3;color: #38414B;display: block;font-size: 14px;height: 30px;line-height: 28px;margin-left: 157px;padding-right: 0;text-align: right;width: 360px;">
					<span class="subtotal_lbl">Sub Total:</span>&nbsp;
					<span class="subtotal_amt">$ '.$_SESSION['cart']['SUBTOTAL'].'</span></span><br>';
		$table.='<span style="border-bottom: 1px dashed #C2CBD3;color: #38414B;display: block;font-size: 14px;height: 30px;line-height: 28px;margin-left: 157px;padding-right: 0;text-align: right;width: 360px;">
					<span class="subtotal_lbl">Turn Around Time Changes : </span>&nbsp;
					<span class="subtotal_amt">$ '.$_SESSION['cart']['fTurnAroundTimeCharge'].' </span></span><br>';
		$table.='<span style="border-bottom: 1px dashed #C2CBD3;color: #38414B;display: block;font-size: 14px;height: 30px;line-height: 28px;margin-left: 157px;padding-right: 0;text-align: right;width: 360px;">
					<span class="subtotal_lbl">Coupon Discount : </span>&nbsp;
					<span class="subtotal_amt">$ '.$_SESSION['cart']['fCouponDiscount'].'</span></span>';
		$table.='<div style="color: #38414B;float: right;font-size: 21px;margin-bottom: 10px;margin-top: 10px;">
			      Total : <span class="bigprice">$ '.$_SESSION['cart']['GRANDTOTAL'].'</span><br></div>';			
			
	  
		//echo $costDetails;exit;
		$fSubTotal = $_SESSION['cart']['SUBTOTAL'];
		$fGrandTotal = $_SESSION['cart']['GRANDTOTAL'];
		
		
			
			$site_url = $this->config->item('site_url');
			$this->smarty->assign("site_url",$site_url);
			$MailFooter = $this->getConfigData('MAIL_FOOTER');
			$siteName = $this->getConfigData('SITE_NAME');
			
			$Email=$data['vBillEmail'];			
			$name=ucfirst($data['vBillFirstname']).' '.$data['vBillLastname'];
			
			$invoiceNumber=$data_update['vInvoiceNumber'];
			
			$comment=$_POST['tComment'];
			
			$orderDate=$data['dAddedDate'];
			
			$dates=date('Y-m-d',strtotime($orderDate));
			$placedOn=date('jS F Y',strtotime($dates));
			
			//echo $placedOn;exit;
			
			$orderSatus= $data['eStatus'];
			$billAddress=$_POST['vBillAddress'];
			$shipAddress=$data['vShipAddress'];
			
			$bodyArr = array("#NAME#","#BILLING_INFORMATION#","#SHIPPING_INFORMATION#","#SHIPPING_METHOD#","#COMMENT#","#INVOICE_NUMBER#","#ORDER_DATE#","#ORDER_STATUS#","#EMAIL#","#COLORDETAIL#","#COSTDETAILS#","#PAYMENT_METHOD#","#SITE_URL#","#MAIL_FOOTER#","#SITE_NAME#");
			$postArr = array($name,$billAddress,$shipAddress,$shippingMethod,$comment,$invoiceNumber,$placedOn,$orderSatus,$Email,$table,$costDetails,$paymentMethod,$site_url,$MailFooter,$siteName);
			//echo "<pre>";print_r($bodyArr );
			//echo "<pre>";print_r($postArr );exit;
			$sendMail=$this->Send("USER_INVOICE","Member",$Email,$bodyArr,$postArr);
			//echo $sendMail;exit;
		
		if($paymentMethod == 'Paypal'){
			echo 'pending|paypal|'.$orderId;
			exit;
		}else if($paymentMethod == 'Authorizedotnet'){
			echo 'pending|Authorizedotnet|'.$orderId;
			exit;
		}else{
			echo "success|creditcard|".$orderId;
			exit;
		}
	}
	function NVPToArray($NVPString){
		$proArray = array();
		while(strlen($NVPString)){
			// name
			$keypos= strpos($NVPString,'=');
			$keyval = substr($NVPString,0,$keypos);
			// value
			$valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
			$valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
			// decoding the respose
			$proArray[$keyval] = urldecode($valval);
			$NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
		}
		return $proArray;
	}
	function paypal(){
		$orderId= $_REQUEST['orderId'];
		$orderDetail = $this->checkout_model->getOrderDetail($orderId)->result_array();
		
		$sandboxMode = $this->getConfigData('SANDBOX');
		
		if($sandboxMode == 'n'){
		     $sandbox = FALSE;
		}else{
		     $sandbox = TRUE;
		}
	      
		$PAYPAL_ACCOUNT = $this->getConfigData('PAYPAL_BUSINESS_ACCOUNT');
		$currency_code = 'USD';
		$this->load->library('paypal_class');
		if($sandbox){
		     $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url    
		}else{
		     $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';		 // paypal url
		}
		//$this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';		 // paypal url
		$this->paypal_class->add_field('currency_code', $currency_code);
		$this->paypal_class->add_field('business', $PAYPAL_ACCOUNT);
		//$this->paypal_class->add_field('business', $this->config->item('bussinessPayPalAccount'));
		$this->paypal_class->add_field('return', site_url.'checkout/success'); // return url
		$this->paypal_class->add_field('cancel_return', site_url.'checkout'); // cancel url
		$this->paypal_class->add_field('notify_url',site_url.'checkout/validatePaypal'); // notify url
		$this->paypal_class->add_field('item_name', 'Emblemax Product');
		$this->paypal_class->add_field('amount', $orderDetail[0]['fGrandTotal']);
		//$this->paypal_class->add_field('shipping', $orderDetail[0]['fShippingCharge']);
		$this->paypal_class->add_field('custom', $orderId);
		$this->paypal_class->submit_paypal_post(); // submit the fields to paypal
		//$p->dump_fields();	  // for debugging, output a table of all the fields
		exit;
		
	}
	function validatePaypal(){
		$this->load->library('paypal_class');	   
		$sandboxMode = $this->getConfigData('SANDBOX');
		if($sandboxMode == 'n'){
		     $sandbox = FALSE;
		}else{
		     $sandbox = TRUE;
		}
		if($sandbox){
		     $this->paypal_class->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url    
		}else{
		     $this->paypal_class->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';		 // paypal url
		}
		// error_log(print_r($_REQUEST, true), 3,dirname(__FILE__)."/error.log");
		if(isset($_REQUEST['txn_id'])){
		     $data_update['vPaypalTransactionId'] = $_REQUEST['txn_id'];
		     if($_REQUEST['payment_status'] == 'Completed'){
			  $data_update['eStatus'] = 'Processing';   
		     }else{
			  $data_update['eStatus'] = 'Pending';
		     }
		     $orderId = $_REQUEST['custom'];
		     $updateInv = $this->checkout_model->updateOrder($data_update,$orderId);
		}
	}
	
	function success(){
		if(isset($_REQUEST['custom']) && $_REQUEST['custom'] != ''){
			//if its paypal payment 
			$orderId = $_REQUEST['custom'];	
		}else{
			//if its cc payment
			$orderId = $_REQUEST['orderId'];
		}

		$orderDetail = $this->checkout_model->getOrderDetail($orderId)->result_array();
		$invoiceNumber = $orderDetail[0]['vInvoiceNumber'];
		
		$data_update = array();
		$data_update['eStatus'] = 'Processing';
		$updateOrder = $this->checkout_model->updateOrder($data_update,$orderId);
		
		unset($_SESSION['cart']); // unset session cart after success
		$this->smarty->assign("invoiceNumber",$invoiceNumber);
		$this->smarty->view('checkout_success.tpl');
	}
	/*
	function Authorizedotnet(){
		$this->load->library('Authorize_net');
		/*
		$_POST = array(
			'amount' => '10.00',
			'card_num' => '370000000000002',
			'exp_date' => '04/17',
			'first_name' => 'John',
			'last_name' => 'Doe',
			'address' => '123 Main Street',
			'city' => 'Boston',
			'state' => 'MA',
			'country' => 'USA',
			'zip' => '02142',
			'email' => 'some@email.com',
			'card_code' => '782',
		);
		*/
		/*
		$tran_key = '983cCv7wRT6av9Dc';
		$login_id = '546Gwp7fMZJJ';
		$_POST = array(	
			// the API Login ID and Transaction Key must be replaced with valid values
			"x_login"			=> $login_id,
			"x_tran_key"		=> $tran_key,
			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",
			"x_type"			=> "AUTH_CAPTURE",
			"x_method"			=> "CC",
			"x_card_num"		=> "4111111111111111",
			"x_exp_date"		=> "0115",
			"x_amount"			=> "19.99",
			"x_description"		=> "Sample Transaction",
			"x_first_name"		=> "John",
			"x_last_name"		=> "Doe",
			"x_address"			=> "1234 Street",
			"x_state"			=> "WA",
			"x_zip"				=> "98004"
			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);
		
		//echo "<pre>";print_r($_POST);exit;
		$this->authorize_net->set_params($_POST);
		//echo "<pre>";print_r($this->authorize_net);exit;
		$this->authorize_net->set_login($login_id, $tran_key);
		
		$this->authorize_net->process(true);
		$response = $this->authorize_net->response();
		
		print_r($response);
		exit;
		*/
		/*
		$this->authorizenet->setFields(
		array(
			'amount' => '10.00',
			'card_num' => '6011000000000012',
			'exp_date' => '04/17',
			'first_name' => 'John',
			'last_name' => 'Doe',
			'address' => '123 Main Street',
			'city' => 'Boston',
			'state' => 'MA',
			'country' => 'USA',
			'zip' => '02142',
			'email' => 'some@email.com',
			'card_code' => '782',
			)
		);
		$this->authorizenet->authorizeAndCapture();
		
		
	}
	*/
	function Authorizedotnet() {
		// Lets do a test transaction
		$this->load->library('authorizenet');
		$this->authorizenet->setCredential(
			array(
				'api_login' => '546Gwp7fMZJJ',
				'transaction_key' => '983cCv7wRT6av9Dc',
			)
		);
		$this->authorizenet->setFields(
			array(
			'amount' => '10.00',
			'card_num' => '6011000000000012',
			'exp_date' => '04/17',
			'first_name' => 'John',
			'last_name' => 'Doe',
			'address' => '123 Main Street',
			'city' => 'Boston',
			'state' => 'MA',
			'country' => 'USA',
			'zip' => '02142',
			'email' => 'some@email.com',
			'card_code' => '782',
			)
		);
		$response = $this->authorizenet->authorizeAndCapture();

		//$response = $transaction->authorizeAndCapture();
		echo "<pre>";print_r($response);exit;
		if ($response->approved) {
			return "APPROVED";
		} else {
			return "DENIED";
		}
		
	}
	public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr){
	  
	    //echo $EmailCode."<br>";
	    //echo $SendType."<br>";
	    //echo $ToEmail."<br>";exit;
	    //echo "<pre>";print_r($bodyArr);
		//echo "<pre>";print_r($postArr);exit;
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";		
		$email_info = $this->checkout_model->list_sysemaildata($ssql)->result();
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: emblemax.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
				
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		$this->body = str_replace($bodyArr,$postArr, $this->body);
		$To = stripcslashes($ToEmail);
		//echo$To ;exit; 
		$htmlMail = '	
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		    <html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>Emblemax</title>
		    </head>
		    
		    <body style="padding:0; margin:0; border:0;">
			 <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			      <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
				   <div style="background:#363F49; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt="" height="50px"/></div>
					'.$this->body.'
				   </div>
			      </div>
			 </div>
		    </body>
	       </html>';
	       
		//echo $htmlMail;exit;
	       if($_SERVER['SERVER_ADDR'] == '192.168.1.41'){ // for localhost server
			 
			 require_once "Mail.php";
			 require_once "Mail/mime.php";
			 $from = "demo3.testing3@gmail.com";			   
			 //echo $from;exit;
			 $to =$To;
			 $subject = "EmbleMax - Thank your for your order request";
			 $crlf = "\n";
			 $html = "<h1> This is HTML </h1>";
			 $headers = array('From' => $from,'To' => $to,'Subject' => $subject);
			 $host = "smtp.gmail.com";
			 $username = "demo2.testing2@gmail.com";
			 $password = "demo1234";
			 $mime =  new Mail_mime(array('eol' => $crlf));
			 $mime->setHTMLBody($htmlMail);			
			 $body = $mime->getMessageBody();			
			 $headers = $mime->headers($headers);
			 
			 $smtp = Mail::factory("smtp",array("host" => $host,"auth" => true,"username" => $username,"password" => $password));			 
			 $res = $smtp->send($to, $headers, $body);			 
		}else
		{ // for live server
			 //echo "else";exit;
			 $res = mail($To,$Subject,$htmlMail,$headers);
		}
	     return $res; 
		//redirect(site_url.'myAccount/myDashboard');
		//redirect(site_url.'registration/registration_success');
		//return $var_msg;
     }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}