<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $logged = '';
	var $data = '';
    
    
    
    function __construct() {
		
	    parent::__construct();
	    $this -> load -> model('admin/admin_model', '', TRUE);
	    //$this -> load -> library('session');
	    $this -> load -> helper('url');
	    $this -> data['post'] = FALSE;
	    $admin_url = $this->config->item('admin_url');
	    $admin_css_path = $this->config->item('admin_css_path');
	    $admin_js_path = $this->config->item('admin_js_path');
	    $this->smarty->assign("admin_js_path",$admin_js_path);
	    $this->smarty->assign("admin_css_path",$admin_css_path);
	    
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
	    $filename = $this->config->item('filename');
		$this->smarty->assign("filename",$filename);    

	    $admin_image_path = $this->config->item('admin_image_path');
	    $this->smarty->assign("admin_image_path",$admin_image_path);
	    $this->smarty->assign("admin_url",$admin_url);
	    $this->smarty->assign("Name","Welcome To Talents Rent Admin Panel");
	}
    
    
    function index() {
	
        $this -> login();
        
	}
    
    
    public function login()
    {
	
	if(isset($_SESSION))
	{
		
		if(isset($_REQUEST['admin_id']))
		{
		$ssql = "AND iAdminId = '".$_REQUEST['admin_id']."'";
		$var_limit = 'LIMIT 0,10';
		$data = $this->admin_model->list_all($var_limit,$ssql)->result();
		if($data)
		{
			
					$login_data = serialize($_SESSION);
					/*echo "<pre>";
					print_r($login_data); exit; */
					$_SESSION['tmp'] = unserialize($login_data);
					$_SESSION['sess_iAdminId'] = $data[0]->iAdminId;
					$_SESSION["sess_vUserName"]= $data[0]->vUsername;
					$_SESSION['sess_vFirstName'] = $data[0]->vFirstName;
					$_SESSION['sess_vLastName'] = $data[0]->vLastName;
					$_SESSION['sess_vEmail'] = $data[0]->vEmail;
		            $_SESSION["sess_lang"] = $_POST['vlanguage'];
					
					$_SESSION['sess_Name'] = $_SESSION["sess_vFirstName"]." ".$_SESSION["sess_vLastName"];
					$var_msg = 'Success';
					redirect(admin_url.'dashboard?msg='.$var_msg);
					exit;
				
			
		}
		}else{
			if(isset($_SESSION["sess_iAdminId"]) !=''){
				redirect(admin_url.'dashboard');
				exit ; 
				}
			else{
				$var_msg ='';
				if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];  
				}
				$this->smarty->assign("var_msg",$var_msg);
				$this->smarty->view( 'admin/authentication/template.tpl');
			}
		
		}
		
	}
    }
    
    public function logout(){
        $iAdminId ='';
	
        if(isset($_SESSION['tmp']))
	{
		if($_SESSION['tmp']['sess_iAdminId'] != '')
		{
		$_SESSION['sess_iAdminId'] = $_SESSION['tmp']['sess_iAdminId'];
		$_SESSION["sess_vUserName"] = $_SESSION['tmp']['sess_vUserName'];
		$_SESSION['sess_vFirstName'] = $_SESSION['tmp']['sess_vFirstName'];
		$_SESSION['sess_vLastName'] = $_SESSION['tmp']['sess_vLastName'];
		$_SESSION['sess_vEmail'] = $_SESSION['tmp']['sess_vEmail'];
		$_SESSION['sess_Name'] = $_SESSION['tmp']['sess_Name'];
		$_SESSION['tmp'] = '';
		}else{
			session_destroy();
		}
		redirect(admin_url.'administrator/adminlist');
		exit;	
		
		
	}else{
		if(isset($_SESSION['sess_iAdminId'])){
		$iAdminId = $_SESSION['sess_iAdminId'];
		$iLoginHistoryId = $_SESSION["sess_iLoninID"];
		}
		$date = date('Y-m-d h:i:s');
		$id = $this->admin_model->logoutentry($iAdminId,$date,$iLoginHistoryId);
		session_destroy();
		$var_msg = "You have logout successfully.";
		redirect(admin_url.'authentication/login?msg='.$var_msg);
		exit;
	}
    }
    
    public function check_login()
    {
     
     
        if(isset($_POST) && $_POST !="")
        {	
            $data = $_POST['data'];
	    
                    if($data['mode'] == "authenticate")
                    {
                           //$eType = $_POST['eType'];
                           $vUserName = $_POST['vUserName'];
                           $vPassword = $this->encrypt($_POST['vPassword']);
                           $dataArr = $this->admin_model->admin_login_check($vUserName,$vPassword)->result();
			   
                           $tot = count($data);
			   
                           if($dataArr){
				
				if($dataArr[0]->eStatus=='Inactive')
				   {
					$var_msg = "Sorry , You are inactive please contact to administrator";
					redirect(admin_url.'authentication/login?msg='.$var_msg);
					exit;
				   }
           
	                        $_SESSION["sess_iAdminId"]=$dataArr[0]->iAdminId;
                            	$_SESSION["sess_vUserName"]= $dataArr[0]->vUsername;
                            	$_SESSION["sess_vFirstName"]=$dataArr[0]->vFirstName;
                            	$_SESSION["sess_vLastName"]=$dataArr[0]->vLastName;
                                $_SESSION["sess_vEmail"]=$dataArr[0]->vEmail;
								$_SESSION["sess_Name"]= $_SESSION["sess_vFirstName"]." ".$_SESSION["sess_vLastName"];
                            	#------------------------------------------------------
                            	# FOR LAST LOGIN LOG.
                            	#------------------------------------------------------
                            	$tLastLogin=date("Y-m-d h:i:s");
                            	$vFromIP=$_SERVER["REMOTE_ADDR"];
                                
                                $editid = $this->admin_model->admin_edit_data($tLastLogin,$vFromIP,$_SESSION["sess_iAdminId"]);
                                $loginid = $this->admin_model->add_loginhistory($_SESSION["sess_iAdminId"],$_SESSION["sess_vFirstName"],$_SESSION["sess_vLastName"],$_SESSION["sess_vEmail"],$_SESSION["sess_vUserName"],$vFromIP);
				$_SESSION["sess_iLoninID"]= $loginid;
                                $var_msg = "You login successfully.";
				
                                redirect(admin_url.'dashboard?msg='.$var_msg);
                                exit;
                           }else{
                                $var_msg = "Invalid username or password";
                            	redirect(admin_url.'authentication/login?msg='.$var_msg);
                                exit;
                           }
                    }
		    elseif($data['mode'] == "forgotpassword"){
			$vEmail = $_POST['vEmail'];
			$ssql = " AND vEmail = '".$vEmail."'";
			//echo $ssql; exit;
			$var_limit = '';
			$dataArr = $this->admin_model->list_all($var_limit,$ssql,'','')->result();
                        $tot = count($dataArr);
			if($tot > 0){
				$MAIL_FOOTER = 'izishirt@izishirt.com';
				$site_url ='<a href="'.$this->config->item('site_url').'" >'.$this->config->item('site_url').'</a>';
				$vUserName = $dataArr[0]->vUsername;
				$name =  $dataArr[0]->vFirstName." ".$dataArr[0]->vLastName;
				$vPassword = $vPassword = $this->decrypt($dataArr[0]->vPassword);
				$bodyArr_Admin = array("#NAME#", "#USERNAME#", "#PASSWORD#", "#SITE_URL#", "#MAIL_FOOTER#");
				$postArr_Admin = array($name, $vUserName, $vPassword, $site_url, $MAIL_FOOTER);
				$vEmail = $dataArr[0]->vEmail;
				$this->Send('ADMIN_FORGOT_PASSWORD','Admin',$vEmail,$bodyArr_Admin,$postArr_Admin);
				$var_msg = "Forgot password mail sent successfully.";
				exit;
			}else{
				$var_msg = "Please enter valid email address.";
				
			}redirect(admin_url.'authentication/login?msg='.$var_msg);
				exit;
		}
        }
    }
    
	public function encrypt($data)
	{
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$key] = $c;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return base64_encode($data);
	}
     public function decrypt($data)
	{
		$data = base64_decode($data);
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$c] = $key;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return $data;
	}
	public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr)
	{		
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";
		
		$email_info = $this->admin_model->list_sysemail($ssql)->result();
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: izishirt.com <support@www.izishirt.com>' . "\r\n".
				'Reply-To: izishirt.com <support@www.izishirt.com>'. "\r\n".
				'Return-Path: izishirt.com <support@.izishirt.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
		
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;

		$this->body = str_replace($bodyArr,$postArr, $this->body);

		$To = stripcslashes($ToEmail);
		
		$htmlMail = '
		
		<!DOCTYPE html>
		<html>
		<head>
		<meta charset="utf-8">
		<title>Talents Rent</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
		</head>
		<body>
			<div style="border:10px solid #9BC55F; background:#fcfcfc; font-family:Arial, Helvetica, sans-serif; width:630px;">
		  <div style="background:#000; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/admin/images/logo.png" alt=""/></div>
		  <div style="padding: 10px 10px 10px 15px; font-size:14px; color:#000;">
			'.$this->body.'
		</body>
		</html>';
		//echo $htmlMail;exit;
		$res = @mail($To,$Subject,$htmlMail,$headers);
		if($res)
		{
			 $var_msg = 'Forgot password mail sent successfully';
		}else
		{
			$var_msg =  'Forgot password mail sent fail';
		}
		redirect(admin_url.'authentication/login?msg='.$var_msg);exit;
		//return $var_msg;
	}
    
}

