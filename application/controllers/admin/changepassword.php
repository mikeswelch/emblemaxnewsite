<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changepassword extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	private $limit = 10;
    
	function __construct() {
		    
		parent::__construct();
		$this->load->library('session');
		
		$this -> load -> model('admin/changepassword_model', '', TRUE);
		
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){ 
		redirect(admin_url.'authentication/login');
		exit ; 
		}
				$filename = $this->config->item('filename');
		$this->smarty->assign("filename",$filename);    

		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");		
	}
	/*      Function Name: index
		Developer Name: Deepak Khamari
		Purpose: to be called when the Skill module of admin page loads
		Created Date: 12-11-2012
	*/
	function index() {
		$this->passwordchange();
	}
	/*
	 Function Name: passwordchange
	 Developer Name: Parth Devmorari
	 Purpose: to be called when  save data
	 Created Date: 12-11-2012
	 */
	function passwordchange()
	{	$this->smarty->assign('operation','edit');
		$iFreelancerUserId = $_REQUEST['iFreelancerUserId'];
		$this->smarty->assign('iFreelancerUserId',$iFreelancerUserId);
	        
                $this->smarty->view('admin/freelancer/changepass.tpl');
		if($_POST){
			$Data = $_POST['Data'];
			$iFreelancerUserId = $_POST['iFreelancerUserId'];
			$data['vPassword'] = md5($Data['vPassword']);
			
			$id = $this->changepassword_model->update($iFreelancerUserId,$data);
			if($id)$var_msg = "Freelancer password is change successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'freelancer/freelancerlist?msg='.$var_msg);
			exit;
		}
	}
	function passworduser()
	{
		
		$this->smarty->assign('operation','edit');
		$iUserId = $_REQUEST['iUserId'];
		$this->smarty->assign('iUserId',$iUserId);
	        
                $this->smarty->view('admin/user/changepass.tpl');
		if($_POST){
			$Data = $_POST['Data'];
			$iUserId = $_POST['iUserId'];
			$data['vPassword'] = md5($Data['vPassword']);
			
			$id = $this->changepassword_model->update_client($iUserId,$data);
			if($id)$var_msg = "User password is changed successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'user/userlist?msg='.$var_msg);
			exit;
		}
	}
}
