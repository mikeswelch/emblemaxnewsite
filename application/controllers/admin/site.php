<?php
class site extends CI_Controller {
  
    function __construct() {
        parent::__construct();
			$filename = $this->config->item('filename');
		$this->smarty->assign("filename",$filename);    

        $this->load->model('admin/site_model');
    }
     
    function ajax_call() { 
        
	$icountryid = $_POST['icountryid']; 
        $db_state = $this->site_model->get_state_by_country($icountryid);
	
        $this->smarty->assign("db_state",$db_state);
	$this->smarty->view('admin/site/site.tpl');
    }
}
?>
