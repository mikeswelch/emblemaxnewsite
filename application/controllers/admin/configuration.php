<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends CI_Controller{
	
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	
	function __construct()
	{
		parent::__construct();
		
		$this -> load -> model('admin/configuration_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ;
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'configuration');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		
		$this->smarty->assign("Name","Welcome To Byu Admin Panel");
	}
	
	function index()
	{
		$this->loadconfiguration();
	}
	
	function loadconfiguration()
	{
		$db_config = $this->configuration_model->loadcofig()->result();
		$this->smarty->assign("db_config",$db_config);
		$this->smarty->view( 'admin/configuration/configuration.tpl');
	}
	
	
	function edit()
	{  
		
		
		if($_POST){
			$Data = $_POST['Data'];
			foreach($Data as $key=>$val) {
				$Value = array(
						'vValue'=>$val
						);
				$where = ' vName  = "'.$key.'"';
				$result = $this->configuration_model->update($Value, $key);
			}
		
			if($result)$var_msg = "Configuration is updated successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'configuration/loadconfiguration?msg='.$var_msg);
			exit;
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
