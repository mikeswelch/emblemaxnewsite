    <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{

    function __construct() {
		
	parent::__construct();
	    
	$this -> load -> model('admin/dashboard_model', '', TRUE);
	$this -> load -> helper('url');
	$admin_url = $this->config->item('admin_url');
	
	$filename = $this->config->item('filename');
	$this->smarty->assign("filename",$filename);    
	
	if(isset($_SESSION["sess_iAdminId"]) ==''){
	    redirect(admin_url.'authentication/login');
	    exit ;
	}
        $admin_css_path = $this->config->item('admin_css_path');
        $admin_js_path = $this->config->item('admin_js_path');
        $this->smarty->assign("admin_js_path",$admin_js_path);
        $this->smarty->assign("admin_css_path",$admin_css_path);
        		$filename = $this->config->item('filename');
		$this->smarty->assign("filename",$filename);    

	$fancybox_path = $this->config->item('fancybox_path');
	$this->smarty->assign("fancybox_path",$fancybox_path);
	
        $admin_image_path = $this->config->item('admin_image_path');
        $this->smarty->assign("admin_image_path",$admin_image_path);
        $this->smarty->assign("admin_url",$admin_url);
	
    }
    /*Function Name: index
    Developer Name: Parth Devmorari
    Purpose: to be called when the Dashboard page of admin loads
    Created Date: 12-01-2012
    */    
    function index() {
	
	$site_path = $this->config->item('site_path');
	
	/*$date = date("Y-01-01");
	$date = strtotime($date);
	$start_date = date('Y-m-d', $date);	
	$end_date = date('Y-m-t', $date);
	//$date = strtotime("+1 month", $date);

	for($i=0;$i<13;$i++){

	   $ssql =" AND dPaymentDate between '$start_date' AND '$end_date'";	   
	   $revanue_data = $this->dashboard_model->count_month_wise($ssql)->result();
	   $revanue_data1[$i] = $revanue_data[0]->month_wise_cnt;
	
	   $date = strtotime($start_date);
	   $date = strtotime("+1 month", $date);
	   $start_date = date('Y-m-d', $date);
	   $end_date = date('Y-m-t', $date);
	}
	
	$file= fopen($site_path."public/admin/js/ProductSales.xml", "w");
	$_xml = '';
	
	$_xml .="\t<graph bgColor='FCFCFC'  xaxisname='Month' yaxisname='Revenue' canvasBorderColor='333333'
	    numdivlines='4' divLineColor='333333' yAxisMaxValue='10' decimalPrecision='0	' numberPrefix=''>\r\n";
	    $_xml .="\t<categories font='Arial' fontSize='10' fontColor='000000'>\r\n";
		$_xml .="\t<category name='Jan' />\r\n";
		$_xml .="\t<category name='Feb' />\r\n";
		$_xml .="\t<category name='Mar' />\r\n";
		$_xml .="\t<category name='Apr' />\r\n";
		$_xml .="\t<category name='May' />\r\n";
		$_xml .="\t<category name='Jun' />\r\n";
		$_xml .="\t<category name='Jul' />\r\n";
		$_xml .="\t<category name='Aug' />\r\n";
		$_xml .="\t<category name='Sep' />\r\n";
		$_xml .="\t<category name='Oct' />\r\n";
		$_xml .="\t<category name='Nov' />\r\n";
		$_xml .="\t<category name='Dec' />\r\n";

	    $_xml .="\t</categories>\r\n";
		$_xml .="\t<dataset seriesname='Transactions' color='AFD8F8' showValues='0'>\r\n";
		
		    $_xml .="\t<set value='".$revanue_data1[0]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[1]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[2]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[3]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[4]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[5]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[6]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[7]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[8]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[9]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[10]."' />\r\n";
		    $_xml .="\t<set value='".$revanue_data1[11]."' />\r\n";
		
		$_xml .="\t</dataset>\r\n";
	
		$_xml .="\t<trendLines>\r\n";
		$_xml .="\t<line startValue='1250000' endValue='1450000' color='8BBA00' thickness='1' alpha='20' showOnTop='1' displayValue='Estimated' isTrendZone='1'/>\r\n";

	    $_xml .="\t</trendLines>\r\n";
	$_xml .="\t</graph>\r\n";
	
	fwrite($file, $_xml);
	
	fclose($file);
	*/
	#echo "<pre>";
	#print_r($revanue_data1);exit;
	
	
	
        $this->smarty->assign("Name","Welcome To Onwards Admin Panel");
        
        $administator = $this->dashboard_model->count_administator()->result();
	$tot_administator = $administator[0]->tot;
	
	/*$clients = $this->dashboard_model->count_user()->result();
	$users = $clients[0]->tot;
	
        $freelancers = $this->dashboard_model->count_product()->result();
	$products = $freelancers[0]->tot;
	
	$briefs = $this->dashboard_model->count_category_types()->result();
	$category_types = $briefs[0]->tot; 
	
	$quotes = $this->dashboard_model->count_faq()->result();
	$faq = $quotes[0]->tot;
	
	$onwardzbriefs = $this->dashboard_model->count_transaction()->result();
	$trasaction = $onwardzbriefs[0]->tot;
	
	$skills = $this->dashboard_model->count_contact_us()->result();
	$contact_us = $skills[0]->tot;
	
	$industries = $this->dashboard_model->count_help()->result();
	$help = $industries[0]->tot;*/
	
	$lastlogin = $this->dashboard_model->lastlogin()->row();
	
        $lastlogin->dLoginDate= date('dS M Y',strtotime($lastlogin->dLoginDate));
	
	
	//--------- Pai Chart Data Count of Transaction ---------
	
	/*$date = date('Y-m-d');
	$date = strtotime($date);
	$date = strtotime("-7 day", $date);
	$todate = date('Y-m-d', $date);
	$tot = 0;
	for($i=0;$i<7;$i++){
	   $start_day = $todate.' 00:00:00';
	   $end_day = $todate.' 23:59:59';
	   
	   $ssql =" AND dPaymentDate between '$start_day' AND '$end_day'";
	   $day_by_cnt1[$i] = $this->dashboard_model->paichat_data_cnt($ssql)->result();
	  
	   $tot = $tot + $day_by_cnt1[$i][0]->day_by_cnt;
	    
	   $week_dy[$i] = $todate;
	   $week_dy[$i] = strtotime($week_dy[$i]);
	   $week_dy[$i] = date('dS-M-Y', $week_dy[$i]);
	   
	   $date = strtotime($todate);
	   $date = strtotime("+1 day", $date);
	   $todate = date('Y-m-d', $date);
	}

	for($i=0;$i<7;$i++){
	    $day_by_cnt[$i] = ($day_by_cnt1[$i][0]->day_by_cnt/$tot)*100;
	}

	$cnt_week =array("0" => $day_by_cnt[0], "1" => $day_by_cnt[1], "2" => $day_by_cnt[2], "3" => $day_by_cnt[3], "4" => $day_by_cnt[4], "5" => $day_by_cnt[5], "6" => $day_by_cnt[6]);
	
	$arr_week = array(
			    0 => $week_dy,
			    1 => $cnt_week,
			);
	$arr_week = json_encode($arr_week);
	$this->smarty->assign("arr_week",$arr_week);
	
	*/
	#
	
        //--------- Pai Chart Data Count of Transaction End --------
    
	$this->smarty->assign("lastlogin",$lastlogin);
        
        $this->smarty->assign("users",$users);
	$this->smarty->assign("products",$products);
	$this->smarty->assign("category_types",$category_types);
	$this->smarty->assign("faq",$faq);
	$this->smarty->assign("trasaction",$trasaction);
	$this->smarty->assign("contact_us",$contact_us);
	$this->smarty->assign("help",$help);
        $this->smarty->assign("tot_administator",$tot_administator);
        
        $this->smarty->view( 'admin/dashboard/dashboard.tpl');
    }
    
}
/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */
