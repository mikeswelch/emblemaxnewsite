<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class homepage_blocks extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	var$limit = '';
    
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this -> load -> model('admin/homepage_blocks_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
			if(isset($_SESSION["sess_iAdminId"]) ==''){ 
			redirect(admin_url.'authentication/login');
			exit; 
		}
		$this->smarty->assign("filename",'homepage_blocks');    
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To EmbleMax Admin Panel");
	}
	function index() {
		$this->homepage_blockslist();
	}
	function homepage_blockslist(){
		$limit1 = $this->homepage_blocks_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){		    
		    $ssql.= " AND vTitle LIKE '".stripslashes($alp)."%'";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		if ($option != '' && $option=='eStatus' && $keyword != ''){
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}else if($option != '' && $keyword != ''){
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		if($_SESSION['module_name'] != 'homepage_blocks_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		$totrec = $this->homepage_blocks_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !=''){
			$var_msg=$num_totrec." Record matched for ".$alp;
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		if(!isset($_REQUEST['order'])){
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!=''){
			$field = $_REQUEST['field'];
			$order=$_REQUEST['order'];
			if($field =='vTitle'){
				  if($order == 'ASC'){
					   $sort='DESC';
				  }else{
					   $sort='ASC';
				  }
			}elseif($field =='vEmail'){
				  if($order == 'ASC'){
					   $sort='DESC';
				  }else{
					   $sort='ASC';
				  }
			}elseif($field =='estatus'){
				if($order == 'ASC'){
					 $sort='DESC';
				}else{
					 $sort='ASC';
				}
			}
		}
		$data = $this->homepage_blocks_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		if($start == '0'){
			$start = 1;
		}
		
          $num_limit = ($start-1)*$limit;
    	     $startrec = $num_limit;
    	     $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 ){
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}else{
				$recmsg="";
			}
               $db_alp = $this->homepage_blocks_model->displayalphasearch()->result();
		
		for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'homepage_blocks/homepage_blockslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			     }else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'homepage_blocks/homepage_blockslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'homepage_blocks/homepage_blockslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'homepage_blocks_model';
		$AlphaBox.='</ul>';
		//echo "<pre>";print_r($AlphaBox);exit;
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/homepage_blocks/view-homepage_blocks.tpl');
	}
	function add(){
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("admin_url",$admin_url);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$upload_path = $this->config->item('upload_path');
	     $this->smarty->assign("upload_path",$upload_path);
		
		$totalRec = $this->homepage_blocks_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->smarty->assign("totalRec",$totalRec);
		
		$action = site_url('homepage_blocks/add');
		$this->smarty->assign('operation','add');
	     $this->smarty->assign("action",$action);   
          $this->smarty->view('admin/homepage_blocks/homepage_blocks.tpl');
		if($_POST){
			$Data = $_POST['data'];
			
			$id = $this->homepage_blocks_model->save($Data);
			
			$img_uploaded = $this->do_upload($id);			
			$Data['vImage'] = $img_uploaded;
			//echo "<pre>";print_r($Data);exit;
               $id = $this->homepage_blocks_model->update($iPriductId,$Data);
			
			if($id)$var_msg = "Contact us is added successfully.";else $var_msg="Error-in add.";
			redirect(admin_url.'homepage_blocks/homepage_blockslist?msg='.$var_msg);
			exit;
		}
	}
     function edit(){
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		
		$id = $_REQUEST['iBlockslistId'];
		
		$admin_url = $this->config->item('admin_url');
		$data = $this->homepage_blocks_model->get_one_by_id($id)->row();
		
		$totalRec = $this->homepage_blocks_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->smarty->assign("totalRec",$totalRec);
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'homepage_blocks/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/homepage_blocks/homepage_blocks.tpl');
		
		if($_POST){
			$Data = $_POST['data'];
			$iBlockslistId = $_POST['iBlockslistId'];
			
			if($_FILES['vImage']['name']!=''){
				$data = $this->homepage_blocks_model->get_one_by_id($iBlockslistId)->result();
				$var = unlink($upload_path.'homepage_blocks/'.$iBlockslistId.'/282X320_'.$data[0]->vImage);
				unlink($upload_path.'homepage_blocks/'.$iBlockslistId.'/'.$data[0]->vImage);
				
				$Data['vImage'] = $_FILES['vImage']['name'];
			}
			$img_uploaded = $this->do_upload($id);
			$id = $this->homepage_blocks_model->update($iBlockslistId,$Data);
			if($id)$var_msg = "Contact us is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'homepage_blocks/homepage_blockslist?msg='.$var_msg);
			exit;
		}	
	}
	function delete(){
		$var = $this->homepage_blocks_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Contact us is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'homepage_blocks/homepage_blockslist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	function search_action(){
		$action = $_POST['action'];
		$iBlockslistId = $_POST['commonId'];
		//echo $iBlockslistId; exit; 
		if($iBlockslistId == '')
		$iBlockslistId = $_POST['iBlockslistId'];
		if(is_array($iBlockslistId)){
		    $iBlockslistId  = @implode("','",$iBlockslistId);
		}
		$iBlockslistId = $iBlockslistId;
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iBlockslistId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iBlockslistId);
		}else{
	    }
	}
	function make_active($action,$iBlockslistId){
		$id = $this->homepage_blocks_model->multiple_update_status($iBlockslistId,$action);
		
		$iBlockslistId  = @explode("','",$iBlockslistId);
		$cnt=count($iBlockslistId);
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Homepage blocks make active successfully.";
		      }else{
		          $var_msg = $cnt."  Homepage blocks  make inactive successfully.";
		      }
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'homepage_blocks/homepage_blockslist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	function make_delete($iBlockslistId){
		$id = $this->homepage_blocks_model->delete_data($iBlockslistId);
		
		$iBlockslistId  = @explode("','",$iBlockslistId);
		$cnt=count($iBlockslistId);
		
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'homepage_blocks/homepage_blockslist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	function do_upload($iBlockslistId,$vImageName){
		if(!is_dir('uploads/homepage_blocks/')){
			@mkdir('uploads/homepage_blocks/', 0777);
		}
		if(!is_dir('uploads/homepage_blocks/'.$iBlockslistId)){
			@mkdir('uploads/homepage_blocks/'.$iBlockslistId, 0777);
		}
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/homepage_blocks/'.$iBlockslistId,
		  'max_size'=>2000
		);
		$this->load->library('upload', $config); 
		$this->upload->do_upload('vImage'); //do upload
		//want to create thumbnail
		$image_data = $this->upload->data(); //get image data
                $config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/homepage_blocks/'.$iBlockslistId.'/282X320_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 282,
		  'height' => 320
		);
		$this->load->library('image_lib', $config1); //load library
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	function deleteimage(){
		$upload_path = $this->config->item('upload_image_path');
		$iBlockslistId = $_REQUEST['iBlockslistId'];
		//$lang = $_REQUEST['lang'];
		$data = $this->homepage_blocks_model->get_one_by_id($iBlockslistId)->result();
		//$Data = $this->homepage_blocks_model->get_data_by_id($iBlockslistId,$lang)->row();
		//echo "<pre>";print_r($data);exit;
		$var = unlink($upload_path.'homepage_blocks/'.$iBlockslistId.'/282X320_'.$data[0]->vImage);
		unlink($upload_path.'homepage_blocks/'.$iBlockslistId.'/'.$data[0]->vImage);
		if($var){
			$var = $this->homepage_blocks_model->delete_image($iBlockslistId);
		}
		if($var){
			$var_msg = "Photo deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'homepage_blocks/edit?iBlockslistId='.$iBlockslistId.'&msg='.$var_msg.'&lang=en');
		exit;
	}
}
