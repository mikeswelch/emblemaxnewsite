<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_format extends CI_Controller
{	
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	private $limit = 10;
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/newsletter_format_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ;
		}
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$admin_css_path = $this->config->item('admin_css_path');
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$ckeditor_path= $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("filename",'newsletter_format');
		/*load libraries for image upload and resize */
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->smarty->assign("Name","Welcome To Byu Admin Panel");
		
	}
	
	function index()
	{
		$this->newsletter_formatlist();
	}
	
	function newsletter_formatlist()
	{
		
		$AlphaBox ='';
		$ssql ='';
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
		$this->smarty->assign("var_msg",$var_msg);
	
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND vTitle LIKE '".stripslashes($alp)."%'";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option == 'dAddedDate')
		{
		   $keyword=date("Y-m-d", strtotime($keyword));
		}
       
		if($option != '' && $keyword != ''){
		    $ssql.= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		    //echo $ssql;exit;
		}
		
		$totrec = $this->newsletter_format_model->count_all($ssql)->result();
		
		$num_totrec = $totrec[0]->tot;
		
		include($site_path."system/libraries/paging.inc.php");
		$data = $this->newsletter_format_model->list_all($var_limit,$ssql,$field,$sort)->result();
				
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vTitle'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='dAddedDate'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='dLastSentDate'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eSendStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->newsletter_format_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		
		if($start == '0'){
			$start = 1;
		}
        
		$num_limit = ($start-1)*$this->limit;
    	
		$startrec = $num_limit;
    	
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." Records Of ".$num_totrec;
			}
			else
			{
				$recmsg="No Records Found.";
			}
			$db_alp = $this->newsletter_format_model->displayalphasearch()->result();
			for($i=0;$i<count($db_alp);$i++){
			$db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
		}

		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'user/userlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'user/userlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'user/userlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$AlphaBox.='</ul>';
		for($i=0;$i<count($data);$i++)
		 {
		    if($data[$i]->dAddedDate != ''){
			$date1 = $data[$i]->dAddedDate;
			$data[$i]->dAddedDate = date('m/d/Y',strtotime($date1));
		    }else{
			$data[$i]->dAddedDate = '';
		    }
		    if($data[$i]->dLastSentDate != ''){
			$date1 = $data[$i]->dLastSentDate;
			$data[$i]->dLastSentDate = date('m/d/Y',strtotime($date1));
		    }else{
			$data[$i]->dLastSentDate = '';
		    }
		}
		
		
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);		
		$this->smarty->assign("data",$data);
		$this->smarty->assign("order",$sort);
		$this->smarty->view( 'admin/newsletter_format/view-newsletter_format.tpl');
	}
	function add()
	{
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'newsletter_format/add';
		$totgal = 1;  
		$this->smarty->assign("totgal",$totgal);
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/newsletter_format/newsletter_format.tpl');
			
		if($_POST)
		{
			$Data = $_POST['Data'];
			$Data['dAddedDate'] = date("Y-m-d H:i:s");
			$id = $this->newsletter_format_model->save($Data);
			if($id)$var_msg = "Newsletter Format is Added Successfully.";else $var_msg="Eror-in Add.";
			redirect(admin_url.'newsletter_format/newsletter_formatlist?msg='.$var_msg);
			exit;
		}
	}
	function edit()
	{
		$id = $_REQUEST['iNformatId'];
		$admin_url = $this->config->item('admin_url');
		$data = $this->newsletter_format_model->get_one_by_id($id)->row();
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'newsletter_format/edit';
		
		$date = $data->dAddedDate;
		$data->dAddedDate= date('m-d-Y',strtotime($date));
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/newsletter_format/newsletter_format.tpl');

		if($_POST)
		{
			$Data = $_POST['Data'];
			$iNformatId = $_POST['iNformatId'];
			$id = $this->newsletter_format_model->update($iNformatId,$Data);
			if($id)$var_msg = "Newsletter Format is Edited Successfully.";else $var_msg="Eror-in Edit.";
			redirect(admin_url.'newsletter_format/newsletter_formatlist?msg='.$var_msg);
			exit;
		}
	}
	function delete($id)
	{
		$id = $_REQUEST['id'];
		$var = $this->newsletter_format_model->delete($id);
		if($var)$var_msg = "Newsletter Format is Deleted Successfully.";else $var_msg="Eror-in Delete.";
		redirect(admin_url.'newsletter_format/newsletter_formatlist?msg='.$var_msg);
		exit;
	}
	function make_delete($iNformatId)
	{
		$id = $this->newsletter_format_model->delete_data($iNformatId);
		if($id){
		      $var_msg = "Record Deleted Successfully";
		}else{
		  $var_msg = "Eror-in Delete";
		}
		redirect(admin_url.'newsletter_format/newsletter_formatlist?msg='.$var_msg);
		exit;
	}
	function search_action()
	{
		$action = $_POST['action'];
		$iNformatId= $_POST['iNformatId'];
		if(is_array($iNformatId)){
		    $iNformatId= @implode("','",$iNformatId);
		}
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iNformatId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iNformatId);
		}else{
		}
	}
	function search()
	{
		$vSearchKey = $_POST['vSearchKey'];
		$vSearchText = $_POST['vSearchText'];
		$data = $this->newsletter_format_model->search_data($vSearchKey,$vSearchText);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/newsletter_format/view-newsletter_format.tpl');
	}
	function make_active($action,$iNformatId)
	{
		$id = $this->newsletter_format_model->multiple_update_status($iNformatId,$action);
		if($id){
		      if($action == 'Active'){
		          $var_msg = "Newletter Format  is Activated Successfully.";
		      }else{
		          $var_msg = "Newletter Format is Inactivated Successfully.";
		      }
		}else{
		  if($action == 'Active'){
		          $var_msg = "Eror-in Active.";
		      }else{
		          $var_msg = "Eror-in Inactive.";
		      }
		}
		redirect(admin_url.'newsletter_format/newsletter_formatlist?msg='.$var_msg);
		exit;
	}
	function sendemail()
	{
		$iNformatId = $_REQUEST['iNformatId'];
		$data = $this->newsletter_format_model->get_one_by_id($iNformatId)->row();
		//$chapter_id= $_SESSION['chapter_id'];
		$userlist = $this->newsletter_format_model->chapter_user_list()->result();
		//$eventlist = $this->newsletter_format_model->upcoming_eventlist($chapter_id)->result();
		
		$flag = 1;
		$this->smarty->assign("flag",$flag);
		$this->smarty->assign("data",$data);
		$this->smarty->assign("userlist",$userlist);
		//$this->smarty->assign("eventlist",$eventlist);
		$this->smarty->view('admin/email/email.tpl');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */