<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designcategory extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/designcategory_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'designcategory');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		$this->designcategorylist();
	}
	
	function designcategorylist()
	{
		$ssql ='';						
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		
		$AlphaBox ='';
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
								
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vTitle LIKE '".stripslashes($alp)."%' OR vTitle LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
		    $ssql.= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		if($_SESSION['module_name'] != 'designcategory_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		
		$totrec = $this->designcategory_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		include($site_path."system/libraries/paging.inc.php");
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='b.iDesignCategoryId'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='b.eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}

		   //echo $ssql;
		$data = $this->designcategory_model->list_all($var_limit,$ssql,$field,$sort)->result();
		//echo "<pre>";
		//print_r($data);exit;
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->designcategory_model->displayalphasearch()->result();
		//echo "<pre>";
		//print_r($db_alp);exit;
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
                    
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'designcategory/designcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'designcategory/designcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'designcategory/designcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'designcategory_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/designcategory/view-designcategory.tpl');
	}	
	function add()
	{
		
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'designcategory/add';
		
		$parentarray = $this->designcategory_model->getParentCatNew('0','','0','1','0');
		$language = $this->designcategory_model->all_language()->result();
		//print_r($language);exit;
		$totalRec = $this->designcategory_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
	
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);

		$lang = 'en';
		$this->smarty->assign("parentarray",$parentarray);
		$data->iDesignParentCategoryId = 0;
		$this->smarty->assign("data",$data);
		$this->smarty->assign("language",$language);
		$this->smarty->assign("lang",$lang);
		$this->smarty->assign("action",$action);
		$this->smarty->assign('operation','add');
		//echo 'test';exit;
		$this->smarty->view('admin/designcategory/designcategory.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$data = $_POST['data'];
			
			if($data['vSeoTitle'] == ''){
			    $data['vSeoTitle'] = $data['vTitle'];	
			}
			if($data['vSeoKeyword'] == ''){
			    $data['vSeoKeyword'] = $data['vTitle'];	
			}

			if($data['tSeoDescription'] == ''){
			    $data['tSeoDescription'] = $data['tDescription'];	
			}
			
			if($Data['iDesignParentCategoryId'] == '')
			{
				$data['vPath'] = $data['vTitle'];
				
			}
			else
			{
				$path1 = $this->designcategory_model->path($Data['iDesignParentCategoryId'])->result();
				//echo '<pre>';print_r($path1);exit;
				$path = $path1[0]->vTitle;
				//echo $path;exit;
			
				$iPId = $path1[0]->iDesignParentCategoryId;	
				$data['vPath'] = ($path.' >> '.$data['vTitle']);
			
			}
			for($i=0;$i<count($category);$i++){
				$category[$i]->vUrlText = $this->seo_url($category[$i]->vTitle);
			}
			$id = $this->designcategory_model->save($Data);
			$data['iDesignCategoryId'] = $id;
			$Id = $this->designcategory_model->saveData($data);
			if($id)$var_msg = "Clipart Category is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'designcategory/designcategorylist?msg='.$var_msg);
				exit;
		}
	}
		
	function edit()
	{	
		$admin_url = $this->config->item('admin_url');
		$lang = $_REQUEST['lang'];
		$admin_url = $this->config->item('admin_url');
		$id = $_REQUEST['iDesignCategoryId'];
		
		$Data = $this->designcategory_model->get_one_by_id($id)->row();
		$data = $this->designcategory_model->get_data_by_id($id,$lang)->row();
		
		$language = $this->designcategory_model->all_language()->result();
		$parentarray = $this->designcategory_model->getParentCatNew('0','','0','1','0');

		$totalRec = $this->designcategory_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		
		$this->smarty->assign("parentarray",$parentarray);
		$this->smarty->assign("language",$language);
		$this->smarty->assign("lang",$lang);
		$action = $admin_url.'designcategory/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign('operation','edit');
		$this->smarty->assign("Data",$Data);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/designcategory/designcategory.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$data = $_POST['data'];
			$lang = $data['vLanguageCode'];
			$iDesignCategoryId = $Data['iDesignCategoryId'];
			$check_lang = $this->designcategory_model->get_data_by_id($iDesignCategoryId,$lang)->result();			
			if(isset($check_lang[0]->iDesignCategoryTranslationId) AND $check_lang[0]->iDesignCategoryTranslationId != '')
			{
				if($data['vSeoTitle'] == ''){
				    $data['vSeoTitle'] = $data['vTitle'];	
				}
				if($data['vSeoKeyword'] == ''){
				    $data['vSeoKeyword'] = $data['vTitle'];	
				}
				if($data['tDescription'] == ''){
				    $data['tSeoDescription'] = $data['tDescription'];	
				}
				if($data['tSeoDescription'] == ''){
				    $data['tSeoDescription'] = $data['tDescription'];	
				}
				if(isset($Data['iDesignParentCategoryId']) && ($Data['iDesignParentCategoryId']) != '')
				{			
				$path1 = $this->designcategory_model->path($Data['iDesignParentCategoryId'])->result();
				//echo "<pre>";print_r($path1);exit;
				$path = $path1[0]->vPath;
				if($path != '')
				$data['vPath'] = ($path.' >> '.$data['vTitle']);
			}
			else{				
				$data['vPath']=$data['vTitle'];
				
			}
			
				$id = $this->designcategory_model->updateDataCatTran($check_lang[0]->iDesignCategoryTranslationId,$data);
			}else{
				$data['iDesignCategoryId'] = $iDesignCategoryId;
								echo $data['tDescription'];exit;
				if($data['vSeoTitle'] == ''){
				    $data['vSeoTitle'] = $data['vTitle'];	
				}
				if($data['vSeoKeyword'] == ''){
				    $data['vSeoKeyword'] = $data['vTitle'];	
				}
				if($data['tDescription'] == ''){
				    $data['tSeoDescription'] = $data['tDescription'];	
				}
				if($data['tSeoDescription'] == ''){
				    $data['tSeoDescription'] = $data['tDescription'];	
				}				
				$Id = $this->designcategory_model->saveData($data);
			}
			$id = $this->designcategory_model->update($iDesignCategoryId,$Data);
			if($id)$var_msg = "Clipart Category is Edited Successfully.";else $var_msg="Eror-in Edit.";
			redirect(admin_url.'designcategory/designcategorylist?msg='.$var_msg);
			exit;
		}
	}
	
	function delete()
	{
		$var = $this->designcategory_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Clipart Category is Deleted Successfully.";else $var_msg="Eror-in Delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'designcategory/designcategorylist?msg='.$var_msg);
		exit;
	}
	
	function make_active($action,$iDesignCategoryId)
	{
		
		$id = $this->designcategory_model->multiple_update_status($iDesignCategoryId,$action);
		$Productidcnt  = @explode("','",$iDesignCategoryId);
		$cnt=count($Productidcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Clipart activated successfully.";
		      }else{
		          $var_msg = $cnt." Clipart is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'designcategory/designcategorylist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	function search_action(){
		$action = $_POST['action'];
		$iDesignCategoryId = $_POST['commonId'];
		if($iDesignCategoryId == '')
		$iDesignCategoryId = $_POST['iDesignCategoryId'];
		
		if(is_array($iDesignCategoryId)){
		    $iDesignCategoryId  = @implode("','",$iDesignCategoryId);
		}
		
		$iDesignCategoryId = $iDesignCategoryId;
                		
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iDesignCategoryId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iDesignCategoryId);
		}else{
	     }	
	}
	
	function make_delete($iDesignCategoryId){
		$id = $this->designcategory_model->delete_data($iDesignCategoryId);
		
		$useridcnt  = @explode("','",$iDesignCategoryId);
		$cnt=count($useridcnt);
		if($id){
		      $var_msg = $cnt." Record Deleted Successfully";
		}else{
		  $var_msg = "Eror-in Delete";
		}
		redirect(admin_url.'designcategory/designcategorylist?msg='.$var_msg);
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
