<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loginhistory extends CI_Controller{

	var $logged = '';
	var $data = '';
	var $var_msg ='';
	var $limit = '';
    
    
    
    function __construct() {
		
	    parent::__construct();
	    
	    
	    $this -> load -> model('admin/loginhistory_model', '', TRUE);
	    $this -> load -> helper('url');
	    $this -> data['post'] = FALSE;
	    
	    $admin_url = $this->config->item('admin_url');
	    if(isset($_SESSION["sess_iAdminId"]) ==''){
           redirect(admin_url.'authentication/login');
            exit ; 
        }
        $admin_css_path = $this->config->item('admin_css_path');
	    $admin_js_path = $this->config->item('admin_js_path');
	    $this->smarty->assign("admin_js_path",$admin_js_path);
	    $this->smarty->assign("admin_css_path",$admin_css_path);
	    
	    $fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
	    
	    $admin_image_path = $this->config->item('admin_image_path');
	    $this->smarty->assign("admin_image_path",$admin_image_path);
	    $this->smarty->assign("admin_url",$admin_url);
	    $this->smarty->assign("filename",'loginhistory');    

	    $this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
		
	}

	function index() {
	    $this->loginhistorylist();
	}

	function loginhistorylist()
	{
		
		$limit1 = $this->loginhistory_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		
		$AlphaBox ='';
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssqll.= " AND vFirstName LIKE '".stripslashes($alp)."%'";
		    
		}
		
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}		
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword'];
		   
		}else{
			$keyword = '';
		}
		if($option == 'dLoginDate'){
			
		   $keyword = $_REQUEST['keyword'];
		   $keyword1 = date("Y-m-d",strtotime($keyword));
		   $start_day = $keyword1.' 00:00:00';
		   $end_day = $keyword1.' 23:59:59';

		}
		
		if($option != '' && $keyword != '' && $option != 'dLoginDate'){
		    $ssqll.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}elseif($option == 'dLoginDate'){
			$ssqll.=" AND $option between '$start_day' AND '$end_day'";
		}
		
		if($_SESSION['module_name'] != 'loginhistory_model' || $_REQUEST['action'] == 'Show All'){
			$_SESSION['ssql_action'] = '';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		$totrec = $this->loginhistory_model->count_all($ssqll)->result();
		
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   if($field =='vFirstName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vEmail'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='dLoginDate'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='dLogoutDate'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vFromIP'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		   $data = $this->loginhistory_model->list_all($var_limit,$ssqll,$field,$sort)->result();
		
		
		 for($i=0;$i<count($data);$i++)
		 {
			$date = $data[$i]->dLoginDate;
			$data[$i]->dLoginDate= date('dS M Y h:i A',strtotime($date));
		    if($data[$i]->dLogoutDate != ''){
			$date1 = $data[$i]->dLogoutDate;
			$data[$i]->dLogoutDate = date('dS M Y h:i A',strtotime($date1));
		    }else{
			$data[$i]->dLogoutDate = '';
		    }
		 }
		
		
		if($start == '0'){
			$start = 1;
		}
        
		$num_limit = ($start-1)*$limit;
    	
		$startrec = $num_limit;
    	
		$lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	
		
		$db_alp = $this->loginhistory_model->displayalphasearch()->result();
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vFirstName, 0,1));
		}
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'loginhistory/loginhistorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'loginhistory/loginhistorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'loginhistory/loginhistorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'loginhistory_model';
		
		$AlphaBox.='</ul>';
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/administrator/view-loginhistory.tpl');
	}
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-01-2012
	 */
	function delete(){

		$var = $this->loginhistory_model->delete($_REQUEST['id']);
		
		if($var)$var_msg = "LoginHistory is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'loginhistory/loginhistorylist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: make_delete
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the multiple data delete
	 Created Date: 12-01-2012
	*/
	function make_delete($iLoginHistoryId){
		$id = $this->loginhistory_model->delete_data($iLoginHistoryId);
		
		$iLoginHistoryId  = @explode("','",$iLoginHistoryId);
		$cnt=count($iLoginHistoryId);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		      
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'loginhistory/loginhistorylist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: search_action
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the search status wise
	 Created Date: 12-01-2012
	 */
	function search_action(){
		$action = $_POST['action'];
		$iLoginHistoryId = $_POST['commonId'];
		if($iLoginHistoryId == '')
		$iLoginHistoryId = $_POST['iLoginHistoryId'];
		
		if(is_array($iLoginHistoryId)){
		    $iLoginHistoryId  = @implode("','",$iLoginHistoryId);
		}
		$iLoginHistoryId = $iLoginHistoryId;
		
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iLoginHistoryId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iLoginHistoryId);
		}
	}
}

/* End of file loginhistory.php */
/* Location: ./application/controllers/admin/loginhistory.php */
