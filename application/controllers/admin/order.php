<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller{

	private $limit = 1;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
	
	function __construct(){
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/order_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$this->smarty->assign("filename",'order');    
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
		
		
	}
	
	function index() {
		
		$limit1 = $this->order_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		
		//echo "<pre>";print_r($_POST);exit;
		/*Filteration start*/
		$showTime=0;	
		if(isset($_POST['filterBy'])){
			if($_POST['filterBy']=='date_range' && $_POST['start_date']!='' && $_POST['end_date']!=''){
				$ssql1 = '';
				$start_date = $_POST['start_date'].' 00:00:00';
				$end_date = $_POST['end_date'].' 23:59:59';
				$ssql1 =" AND dPaymentDate between '$start_date' AND '$end_date'";
				$ssql = $ssql1;
				$filterBy = 'searchByDate';
				$start_date = $_POST['start_date'];
				$end_date = $_POST['end_date'];
				$this->smarty->assign("start_date",$start_date);
				$this->smarty->assign("end_date",$end_date);
			}
			if($_POST['filterBy']=='day' && $_POST['select_day']!=''){
				$ssql1 = '';
				$start_day = $_POST['select_day'].' 00:00:00';
				$end_day= $_POST['select_day'].' 23:59:59';
				$ssql1 =" AND dTransactionDate between '$start_day' AND '$end_day'";
				$showTime=1;
				$ssql = $ssql1;
				$filterBy = 'searchByDay';
				$day = $_POST['select_day'];
				$this->smarty->assign("day",$day);
			}
			if($_POST['filterBy'] == 'PaymentStatus' && $_POST['PaymentStatus']){
				$ssql1 = '';
				$eStatus = $_POST['PaymentStatus'];
				$ssql1 =" AND o.eStatus ='$eStatus'";
				$ssql = $ssql1;
				$filterBy = 'searchByPaymentStatus';
				$this->smarty->assign("eStatus",$eStatus);
				
			}
			if($_POST['filterBy'] == 'paypal_transactionId' && $_POST['paypal_transactionId']){
				$ssql1 = '';
				$vPaypalTransactionId = $_POST['paypal_transactionId'];
				$ssql1 =" AND vFirstName ='$vPaypalTransactionId'";
				$ssql = $ssql1;
				
				$filterBy = 'searchByTransactionId';
				$this->smarty->assign("vPaypalTransactionId",$vPaypalTransactionId);
			}
			if($_POST['filterBy'] == 'total_amount' && $_POST['total']){
				$ssql1 = '';
				$fGrandTotal = $_POST['total'];
				$ssql1 =" AND o.fGrandTotal ='$fGrandTotal'";
				$ssql = $ssql1;
				$filterBy = 'searchByGrandTotal';
				$this->smarty->assign("fTotalAmt",$fGrandTotal);
			}			
			if($_POST['filterBy']= 'firstName' && $_POST['firstName']){				
				$ssql1 = '';
				$vBillFirstname = $_POST['firstName'];
				$ssql1 =" AND vBillFirstname LIKE '$vBillFirstname%'";				
				$ssql = $ssql1;
				$filterBy = 'firstName';
				$this->smarty->assign("vBillFirstname",$vBillFirstname);
			}
			//echo $ssql ;exit;
			//echo "<pre>";
			//print_r($_POST);exit;
		}
		$this->smarty->assign("showTime",$showTime);
		/*Filteration end*/
		if($_SESSION['module_name'] != 'order_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';			
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$AlphaBox ='';
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
		$totrec = $this->order_model->count_all($ssql)->result();
		//echo '<pre>';print_r($totrec);exit;
		$num_totrec = count($totrec);
		//echo $num_totrec;exit;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			#echo $var_msg;exit;
			
		}
		if($ssql != '' ){
		    $var_msg=$num_totrec." Record matched";		    
		}
		
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='o.vBillName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='o.fGrandTotal'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='o.eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}		   
		
		$data = $this->order_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		
		
		$totTranscation=$this->order_model->totTrancsation()->result();	
		$total_price="";
		for($k=0;$k<count($totTranscation);$k++){		 		 
		 $total_price=$total_price+$totTranscation[$k]->fGrandTotal;	
		}		
		
		for($j=0;$j<count($totTranscation);$j++){
		$total_transaction=$total_transaction+$data[$j]->fPrice;
		
		}
		$totaltrans = $this->order_model->transactionAmount($var_limit)->result();
		
		//echo $totaltrans[0]->finalBillAmount;
		//echo "<pre>";print_r($totaltrans );exit;
		for($k=0;$k<count($totaltrans);$k++){
		$total=$total+$totaltrans[$k]->finalBillAmount;
		}
		for($i=0;$i<count($data);$i++){
			
			$date= $data[$i]->dPaymentDate;
			$data[$i]->dPaymentDate= date('dS M Y',strtotime($date));
		}
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->order_model->displayalphasearch()->result();
		//echo '<pre>';print_r($db_alp);exit;
		
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->name, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faq/faqlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faq/faqlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faq/faqlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$AlphaBox.='</ul>';
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'order_model';
		if(!isset($page_link)) $page_link= '';
		
		if($showTime ==1){
			for($i=0;$i<count($data);$i++){
				$arrTime=explode(" ", $data[0]->dTransactionDate);
				$data[0]->dTransactionDate = $arrTime[1];
			}	
		}


		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$order);
		$this->smarty->assign("filterBy",$filterBy);
		$site_url = $this->config->item('site_url');		 
		$this->smarty->assign("site_url",$site_url);
		$this->smarty->assign("order",$sort);
		
		$this->smarty->assign("total_transaction",$total_transaction);
		$this->smarty->assign("total",$total);
		$this->smarty->assign("total_price",$total_price);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		
		//echo $data[0]->fGrandTotal;
		//echo "<pre>";print_r($data);exit;
		$this->smarty->view('admin/order/view_order.tpl');
	}
	// Add data function
	function add()
	{
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);

		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'common/add';
		
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/transaction/common.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$Data['date_added'] = date("Y-m-d");
			$Data['ip'] = $_SERVER["REMOTE_ADDR"];
			$id = $this->commonmodel->save($Data);
			$img_uploaded = $this->do_upload($id);
			$Data['vphoto'] = $img_uploaded;
			$id = $this->commonmodel->update($id,$Data);
			if($id)$var_msg = "User is added successfully.";else $var_msg="Error-in Add.";
				redirect(admin_url.'transaction/index?msg='.$var_msg);
				exit;
		}
	}
	//showinvoice
	function showorder()
	{
		//$upload_image_path = $this->config->item('upload_image_path');
		$iOrderId=$_REQUEST['iOrderId'];
		
		$tComments = $this->order_model->getComments($iOrderId)->result();
		
		$this->smarty->assign("tComments",$tComments);
		$date_comment = date('dS M Y',strtotime($tComments[0]->dDate));
		$time_comment = date('h:i:A',strtotime($tComments[0]->dDate));
		//echo "<pre>";print_r($time_comment);exit;
		$this->smarty->assign("time_comment",$time_comment);
		$this->smarty->assign("date_comment",$date_comment);
		$this->smarty->assign("upload_image_path",$upload_image_path);
		
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$admin_url = $this->config->item('admin_url');
		
		$orderData = $this->order_model->show_orderdata($iOrderId)->result();		
		$admin_image = $this->config->item('admin_image_path');				
		for($k=0;$k<count($orderData);$k++){
		$total=$total+$orderData[$k]->fPrice;
		}
		$size = $this->order_model->all_size()->result_array();		
		for($i=0;$i<count($orderData);$i++){
		       $orderData[$i]->total = $orderData[$i]->fPrice * $orderData[$i]->iQty;
		}		
		$type = $this->db->query( "SHOW COLUMNS FROM `order` WHERE Field = 'eStatus'" )->row( 0 )->Type;
		preg_match('/^enum\((.*)\)$/', $type, $matches);
		foreach( explode(',', $matches[1]) as $value )
		{
		     $enum[] = trim( $value, "'" );
		}
	     $orderDate = date('dS M Y h:i:A',strtotime($orderData[0]->dAddedDate));
		
		
		
		$iBillCountryId= $orderData[0]->iBillCountryId;
		$iBillStateId = $orderData[0]->iBillStateId;
		$country = $this->order_model->show_country($iBillCountryId)->result();
		$state = $this->order_model->show_state($iBillStateId)->result();
		
		$iShippStateId=$orderData[0]->iShippStateId;
		$iShippCountryId=$orderData[0]->iShippCountryId;
		$shipping_state = $this->order_model->shipping_state($iShippStateId)->result();
		$shipping_country = $this->order_model->shipping_country($iShippCountryId)->result();
		
		
		
		$iProductId=$orderData[0]->iProductId;		
		$getProductName=$this->order_model->getProductName($iProductId)->row();
		$productName=$getProductName->vProductName;
		
		$colorsizeData=$orderData[0]->tColorSizeData;		
		$tColorSizeData=json_decode($colorsizeData);		
		for($i=0;$i<count($tColorSizeData);$i++){	       
	       $color = $this->order_model->getColorById($tColorSizeData[$i]->iColorId)->row_array();		  
	       $size = $this->order_model->getSizeById($tColorSizeData[$i]->iSizeId)->row_array();		  
	       $quoteSummary['vColorSizeDescription'][] = "Qty ".$tColorSizeData[$i]->iQty." of size ".$size['vSize']." in ".$color['vColor'];		 
	       $quoteSummary['totalQty'] += $tColorSizeData[$i]->iQty;
		  
		}		
		$printLocation=$orderData[0]->tPrintlocationNumberofcolorsData;
		$tPrintlocationNumberofcolorsData=json_decode($printLocation);		
		for($i=0;$i<count($tPrintlocationNumberofcolorsData);$i++){
	       //screenprinting on Left chest
	       $printlocation = $this->order_model->getPrintLocationById($tPrintlocationNumberofcolorsData[$i]->print_location)->row_array();	  
	       if($tPrintlocationNumberofcolorsData[$i]->decoration_type == 'screenprinting'){
		    $number_of_colors = $this->order_model->getNumberOfColorsById($tPrintlocationNumberofcolorsData[$i]->number_of_colors)->row_array();		    
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]->decoration_type." on ".$printlocation['vPrintLocation'].' ( '.$number_of_colors['vNumberColor'].' )';    
	       }else{
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'];
	       }
		}	
		$this->smarty->assign("quoteSummary",$quoteSummary); // total color wise size wise total qty.
		$this->smarty->assign("productName",$productName);
		$this->smarty->assign("orderDate",$orderDate);
		$this->smarty->assign("orderStatus",$enum);
		$this->smarty->assign("admin_image",$admin_image);
		$this->smarty->assign("country",$country);
		$this->smarty->assign("shipping_state",$shipping_state);
		$this->smarty->assign("shipping_country",$shipping_country);
		
		$this->smarty->assign("total",$total);
		$this->smarty->assign("size",$size);
		$this->smarty->assign("state",$state);
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->assign("orderData",$orderData);
		$this->smarty->assign("productdata",$productdata);
		//echo 'order';exit;
		$this->smarty->view('admin/order/view-order.tpl');
	}
	

	
	// Edit data function 
	function edit()
	{

		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['commonid'];
		$admin_url = $this->config->item('admin_url');
		
		$data = $this->commonmodel->get_one_by_id($id)->row();
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'commonid/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/all_transaction/index.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			
                	$Data['date_added '] = date("Y-m-d");
			$iTransactionId = $_POST['iTransactionId'];
			
			$id = $this->commonmodel->update($iTransactionId,$Data);
			if($id)$var_msg = "Common is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'all_transaction/index?msg='.$var_msg);
			exit;
		}
	}
	
	function delete()
	{		
		
		$var = $this->order_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Transaction is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'order/index?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	
	function make_active($action,$iTransactionId)
	{
		
		$id = $this->order_model->multiple_update_status($iTransactionId,$action);
		$useridcnt  = @explode("','",$iTransactionId);
		$cnt=count($useridcnt);
		//echo  $cnt; exit;
		
		if($id){
			if($action == 'Active'){
			    $var_msg = $cnt." Record activated successfully.";
			}else{
			    $var_msg = $cnt." Record is inactivated successfully.";
			}
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'order/index?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	function search_action(){
		$iBusinessUserId = $_POST['iBusinessUserId'];
		
		$action = $_POST['action'];
		$iTransactionId = $_POST['commonId'];
		if($iTransactionId == '')
		$iTransactionId = $_POST['iOrderId'];
			
		if(is_array($iTransactionId)){
		    $iTransactionId  = @implode("','",$iTransactionId);
		}
		$iTransactionId = $iTransactionId;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iTransactionId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iTransactionId,$iBusinessUserId);
		}else{
			
	    }	
	}
	
	function make_delete($commonid){
		$id = $this->order_model->delete_data($commonid);
		
		
		$useridcnt  = @explode("','",$commonid);
		$cnt=count($useridcnt);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'order/index?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Created By: Nikhil Detroja
	 Created Date: 18-10-2013
	 Modified By:
	 Modified Date:	 
	 */
	function saveComment(){
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);		
		if($_POST['history']['comment'] == '')
		{
			$msg = "Please Enter Your Comments";
			$this->smarty->assign("msg",$msg);
			redirect(admin_url.'order/showorder?iOrderId='.$_POST['iOrderId'].'&msg='.$msg);
		}
		else{
			$Data['dDate'] = date("Y-m-d H:i:s");	
			$Data['iOrderId'] = $_POST['iOrderId'];
			$Data['tComments'] = $_POST['history']['comment'];			
			$orderStatus['eStatus']=$_POST['history']['status'];			
			
			$iCommentId = $this->order_model->saveComments($Data);		
			if($iCommentId!=''){
				$udpateOrderTable=$this->order_model->update($Data['iOrderId'],$orderStatus);
			}			
			$orderId = $_POST['iOrderId'];		
			$orderDetail = $this->order_model->getOrderDetail($orderId)->result_array();			
			//echo "<pre>";print_r($orderDetail);exit;
			
			$colorSizes=$orderDetail[0]['tColorSizeData'];
			$totalColorSizes=json_decode($colorSizes);			
			
			$colorsizeData=$orderData[0]->tColorSizeData;		
			$tColorSizeData=json_decode($colorSizes);			
			for($i=0;$i<count($tColorSizeData);$i++){	       
			  $color = $this->order_model->getColorById($tColorSizeData[$i]->iColorId)->row_array();		  
			  $size = $this->order_model->getSizeById($tColorSizeData[$i]->iSizeId)->row_array();		  
			  $quoteSummary['vColorSizeDescription'][] = "Qty ".$tColorSizeData[$i]->iQty." of size ".$size['vSize']." in ".$color['vColor'];		 
			  $quoteSummary['totalQty'] += $tColorSizeData[$i]->iQty;			  
			}					
			
			$printLocation=$orderDetail[0]['tPrintlocationNumberofcolorsData'];			
			$tPrintlocationNumberofcolorsData=json_decode($printLocation);	
			
			for($i=0;$i<count($tPrintlocationNumberofcolorsData);$i++){			  
			  $printlocation = $this->order_model->getPrintLocationById($tPrintlocationNumberofcolorsData[$i]->print_location)->row_array();	  
			  if($tPrintlocationNumberofcolorsData[$i]->decoration_type == 'screenprinting'){
			    $number_of_colors = $this->order_model->getNumberOfColorsById($tPrintlocationNumberofcolorsData[$i]->number_of_colors)->row_array();		    
			    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]->decoration_type." on ".$printlocation['vPrintLocation'].' ( '.$number_of_colors['vNumberColor'].' )';    
			  }else{
			    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'];
			  }
			}			
			
			$getShippingMethods=$this->order_model->getShippingMethods($orderDetail[0]['iTurnAroundTimeId'])->row();
			$shippingMethod=$getShippingMethods->vTitle.'  '.$getShippingMethods->tNote; 
			
			$subTotal=$orderDetail[0]['fSubTotal'];
			$shipmentCharge=$orderDetail[0]['fTurnAroundTimeCharge'];	  
			$couponDiscount=$orderDetail[0]['fCouponDiscounte'];
			$grandTotal=$orderDetail[0]['fGrandTotal'];
			
			$table='<div style="float: left;width: 100%;padding: 10px 0 10px 0;">
			<table style="border-bottom:2px solid #EAEAEA" class="cart-table table table-bordered" id="shopping-cart-table">
			<colgroup>
				<col width="1">
				<col>
				<col width="1">
				<col width="1">
				<col width="1">
				<col width="1">
				<col width="1">
			</colgroup>
			<thead style="background-color: #002f5f;">
				<th width="100" rowspan="1" class="" style="color:white;"><span class="nobr">Image</span></th>
				<th width="200" rowspan="1" class="" style="color:white;"><span class="nobr">Product Name</span></th>
				<th width="200" rowspan="1" class="" style="color:white;"><span class="nobr">Selected Print locations</span></th>
				<th width="200" rowspan="1" class="" style="color:white;"><span class="nobr">Selected Color - Size</span></th>
				<th width="100" colspan="1" style="color:white;"><span class="nobr">Total Qty</span></th>
				<th rowspan="1" style="color:white;"><span class="nobr">Total</span></th>
			</thead>
			';
			$table.='<tbody>';
			$table.='<tr>';
			
			$iProductId=$orderDetail[0]['iProductId'];		
			$getProductName=$this->order_model->getProductName($iProductId)->row();
			$productName=$getProductName->vProductName;		
			$imagePath=$upload_path.'product/'.$orderDetail[0]['iProductId'].'/'.$orderDetail[0]['vImage'];		
			
			
			$table.='<td width="100"><img width="100px" src="'.$imagePath.'" alt="" /></td>'; //image 
			$table.='<td width="200">'.$productName.'</td>'; //product name
			
			$table.='<td width="200">';														
			for($j=0;$j<=count($quoteSummary['vPrintLocationsDescription']);$j++){  //print location				 
		       $table.=$quoteSummary['vPrintLocationsDescription'][$j].'<br>';		  
			}
			//$table.='<td/>';			
			$table.='<td width="200">';
			for($k=0;$k<count($quoteSummary['vColorSizeDescription']);$k++){
		       $table.=$quoteSummary['vColorSizeDescription'][$k].'<br>';	  
			}
			//$table.='<td/>';		
			$table.=' <td width="100">'.$quoteSummary['totalQty'].'</td>';
			$table.=' <td width="100">'.$orderDetail[0]['finalBillAmount'].'</td>';
		$table.='</tr>';		
		$table.='</tbody>';
		$table.='</table>';
		$table.='</div>';
		$table.='<div style="clear:both;"></div>';
		
		$table.='<div style="width: 520px;float: right;">';
		$table.='<span style="border-bottom: 1px dashed #C2CBD3;color: #38414B;display: block;font-size: 14px;height: 30px;line-height: 28px;margin-left: 157px;padding-right: 0;text-align: right;width: 360px;">
					<span class="subtotal_lbl">Sub Total:</span>&nbsp;
					<span class="subtotal_amt">$ '.$orderDetail[0]['fSubTotal'].'</span></span><br>';
		$table.='<span style="border-bottom: 1px dashed #C2CBD3;color: #38414B;display: block;font-size: 14px;height: 30px;line-height: 28px;margin-left: 157px;padding-right: 0;text-align: right;width: 360px;">
					<span class="subtotal_lbl">Turn Around Time Changes : </span>&nbsp;
					<span class="subtotal_amt">$ '.$orderDetail[0]['fTurnAroundTimeCharge'].' </span></span><br>';
		$table.='<span style="border-bottom: 1px dashed #C2CBD3;color: #38414B;display: block;font-size: 14px;height: 30px;line-height: 28px;margin-left: 157px;padding-right: 0;text-align: right;width: 360px;">
					<span class="subtotal_lbl">Coupon Discount : </span>&nbsp;
					<span class="subtotal_amt">$ '.$orderDetail[0]['fCouponDiscount'].'</span></span>';
		$table.='<div style="color: #38414B;float: right;font-size: 21px;margin-bottom: 10px;margin-top: 10px;">
			      Total : <span class="bigprice">$ '.$orderDetail[0]['finalBillAmount'].'</span><br></div>';		
			
		$allComments=$this->order_model->getAllComments($_POST['iOrderId'])->result();
			
		for($n=0;$n<count($allComments);$n++){		
			$date_comment = date('dS M Y',strtotime($allComments[0]->dDate));			
			$time_comment = date('h:i:A',strtotime($allComments[0]->dDate));			
			$comments.='<table><tr><td>'.$allComments[$n]->tComments.' </td></tr>';
			$comments.='<tr><td>'.$date_comment .' '.$time_comment .' </td></tr>';
			$comments.='<tr><tr></tr></tr></table>';	
		}				
		
			$site_url = $this->config->item('site_url');
			$this->smarty->assign("site_url",$site_url);
			$MailFooter = $this->order_model->generalData('MAIL_FOOTER');		
			$siteName = $this->order_model->generalData('SITE_NAME');	
			
			$Email=$orderDetail[0]['vBillEmail'];			
			$name=ucfirst($orderDetail[0]['vBillFirstname']).' '.$orderDetail[0]['vBillLastname'];			
			$invoiceNumber=$orderDetail[0]['vInvoiceNumber'];						
			$orderDate=$orderDetail[0]['dAddedDate'];			
			$dates=date('Y-m-d',strtotime($orderDate));
			$placedOn=date('jS F Y',strtotime($dates));	
			
			$orderSatus= $orderDetail[0]['eStatus'];
			$billAddress=$orderDetail[0]['vBillAddress'];
			$shipAddress=$orderDetail[0]['vShipAddress'];
			$paymentMethod=$orderDetail[0]['vPaymentMethod'];		
			
			$bodyArr = array("#NAME#","#BILLING_INFORMATION#","#SHIPPING_INFORMATION#","#SHIPPING_METHOD#","#COMMENT#","#INVOICE_NUMBER#","#ORDER_DATE#","#ORDER_STATUS#","#EMAIL#","#COLORDETAIL#","#COSTDETAILS#","#PAYMENT_METHOD#","#SITE_URL#","#MAIL_FOOTER#","#SITE_NAME#");
			$postArr = array($name,$billAddress,$shipAddress,$shippingMethod,$comments,$invoiceNumber,$placedOn,$orderSatus,$Email,$table,$costDetails,$paymentMethod,$site_url,$MailFooter,$siteName);
			$sendMail=$this->Send("USER_INVOICE","Member",$Email,$bodyArr,$postArr);			
			redirect(admin_url.'order/showorder?iOrderId='.$_POST['iOrderId']);
		}
		
		
	}
	function show_all($iBusinessUserId)
	{
		redirect(admin_url.'order/index?action=Show All');
		exit;	
	}
	function export()
	{
	   
	   $ssql=$_REQUEST['ssql'];
	   $ssql = base64_decode($ssql);
	   
	   $csv_output ='';
           header("Content-type: text/html; charset=utf-8");
           $rowcsv='';

	   $result = $this->order_model->exportData($ssql)->result();
           $count = count($result);
	   
	  
	   
	$locationfilepath = "order.csv";
        $locationfilepathurl =  "admin/order.csv";
        $output = fopen($locationfilepath, 'w');
        $rowcsv.= 'Invoice Number';
        $rowcsv.= "*".'Card Type';
	$rowcsv.= "*".'Name On Card';
	$rowcsv.= "*".'Expiry Date';
	$rowcsv.= "*".'Credit Card TransactionId';
	$rowcsv.= "*".'Currency';
	$rowcsv.= "*".'Subtotal';
	$rowcsv.= "*".'Vat Price';
	$rowcsv.= "*".'Grand Total';
	$rowcsv.= "*".'First Name';
	$rowcsv.= "*".'Last Name';
	$rowcsv.= "*".'Address';
	$rowcsv.= "*".'City';
	$rowcsv.= "*".'Transaction Date';
	$rowcsv.= "*".'City';
	$rowcsv.= "*".'Status';
        $rowcsv.= "\n";
        ;
        fwrite($output, $rowcsv);
        
	for ($i=0;$i<$count;$i++) {
            
            $rowcsv.= $result[$i]->vInvoiceNumber;
            $rowcsv.= "*".$result[$i]->eCardType;
	    $rowcsv.= "*".$result[$i]->vNameOnCard;
	    $rowcsv.= "*".$result[$i]->vExpiryDate;
	    $rowcsv.= "*".$result[$i]->vCreditCardTransactionId;
	    $rowcsv.= "*".$result[$i]->vCurrency;
	    $rowcsv.= "*".$result[$i]->fSubTotal;
	    $rowcsv.= "*".$result[$i]->fVatPrice;
	    $rowcsv.= "*".$result[$i]->fGrandTotal;
	    $rowcsv.= "*".$result[$i]->vFirstName;
	    $rowcsv.= "*".$result[$i]->vLastName;
	    $rowcsv.= "*".$result[$i]->tAddress;
	    $rowcsv.= "*".$result[$i]->vCity;
	    $rowcsv.= "*".$result[$i]->dAddedDate;
	    $rowcsv.= "*".$result[$i]->vCity;
	    $rowcsv.= "*".$result[$i]->eStatus;
            $rowcsv.= "\n";
        }
         fwrite($output, $rowcsv);
         fclose($output);
	   
	      header("Content-type: application/vnd.ms-excel");
	      header("Content-disposition: csv" . date("Y-m-d") . ".csv");
	      header( "Content-disposition: filename=order.csv");
	      print $rowcsv; // il file e' pronto e puo' essere scaricato
	      exit;
	   
		
		
	}
	
	function printorder()
	{
		$iOrderId=$_REQUEST['iOrderId'];
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$admin_url = $this->config->item('admin_url');
		$orderdata = $this->order_model->show_orderdata($iOrderId)->result();
		$size = $this->order_model->all_size()->result_array();
		for($k=0;$k<count($orderdata);$k++){
		$total=$total+$orderdata[$k]->fPrice;
		}				
		$iCountryId = $orderdata[0]->iCountryId;
		$iStateId = $orderdata[0]->iStateId;
		$country = $this->order_model->show_country($iCountryId)->result();
		$state = $this->order_model->show_state($iStateId)->result();
		
		$this->smarty->assign("country",$country);
		$this->smarty->assign("state",$state);
		$this->smarty->assign("size",$size);
		$this->smarty->assign("total",$total);
		$this->smarty->assign("action",$action);
		$this->smarty->assign("orderdata",$orderdata);
		$this->smarty->assign("productdata",$productdata);
		$this->smarty->view('admin/order/printorder.tpl');
		
	}
	function sendmail()
	{
		
          $upload_path = $this->config->item('upload_path');
	  $admin_css_path = $this->config->item('admin_css_path');
	  $admin_url = $this->config->item('admin_url');
	  $admin_image_path = $this->config->item('admin_image_path');
	  
	  $iUserId=$_REQUEST['iUserId'];
	  $iOrderId=$_REQUEST['iOrderId'];
		
		if($iOrderId !='')
		{
		$data = $this->order_model->show_orderdata($iOrderId)->row();
		}
		
		$date = $data->dAddedDate;
		$data->dAddedDate= date('d-M-Y',strtotime($date));		
		$country = $this->order_model->show_country($data->iCountryId)->result();
		$state = $this->order_model->show_state($data->iStateId)->result();
		$data->vState=$state[0]->vState;
		$data->vCountry=$country[0]->vCountry;
		
		
	  
	  $to=$data->vEmail;
	  
	  //$to="dipakranjan.khamari@php2india.com";
	  $subject="Invoice";
	  $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	  $headers .='From: Onwardz.com <support@onwardz.com>' . "\r\n".
				'Reply-To: Onwardz.com <support@onwardz.com>'. "\r\n".
				'Return-Path: Onwardz.com <support@onwardz.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
		
	 $htmlMail ='<!DOCTYPE html>
		<html>
		<head>
		<meta charset="utf-8">
		<title>Onwardz</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
		</head>
		<body>
		<div style="border:10px solid #9BC55F; background:#fcfcfc; font-family:Arial, Helvetica, sans-serif; width:970px;">
		  <div style="background:#000; padding: 10px 10px 10px 10px;"><img src="'.$admin_image_path.'logo.png" alt=""/></div>
		  <div style="padding: 10px 10px 10px 15px; font-size:14px; color:#000;">
		  
                <div  style="padding: 20px 2% 20px 2%;">
			<div  style="width:300px; float:right; padding: 0px; border-radius:3px;">
				<div  style="background:#9BC55F; border-radius: 0 15px 0 0; color: #677B4B; font-family: segoewpsemibold; font-size: 14px; font-weight: bold; padding: 6px 10px 7px;  text-shadow: 0 1px 1px #B6E07A;">Order Information</div>
				<div  style="color:#666666; line-height:22px; background:#d7ebfe;background:#fff; border: 1px solid #E8E7E7; border-radius: 0 0 0 20px; overflow:hidden; border-top:0px;">
					<table cellpadding="0" cellspacing="1" width="130%">
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Order Number</td>
							<td><input type="text" readonly="readonly" style="width:60%; height:22px; background:#f4f4f4;width:92%; border:0px;  border:0px; border-radius:0px; " value="'.$data->vPaypalTransactionId.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Order Date</td>
							<td><input type="text" readonly="readonly" style="width:60%;height:22px; background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->dAddedDate.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Shipping Charge</td>
							<td><input type="text" readonly="readonly" style="width:60%;height:22px; background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->vShippingCharge.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Sub Total</td>
							<td><input type="text" readonly="readonly" style="width:60%;height:22px; background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->fSubTotal.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9;padding-left:10px;">Amount</td>
							<td><input type="text" readonly="readonly" style="width:60%; height:22px;background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->fGrandTotal.'" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div  style="width:50%;">
				<div  style="background:#9BC55F; border-radius: 0 15px 0 0; color: #677B4B; font-family: segoewpsemibold; font-size: 14px; font-weight: bold; padding: 6px 10px 7px;  text-shadow: 0 1px 1px #B6E07A;">User Information</div>
			<div  style="font-size:13px; line-height:20px; font-family:Arial, Helvetica, sans-serif; background: #E9E9E9; border: 1px solid #E8E7E7; border-radius: 0 0 0 20px; padding: 16px 21px 16px 21px; color:#666666; text-shadow: 0px 1px 1px #fff;">
				
				<div  style="padding: 0px 0px 5px 0px; float:left">';
				if ($data->vPhoto != ''){
				$htmlMail .='<img style="border:1px solid #dbdbdb;" height="120px" width="140px" src="'.$upload_path.'user/'.$data->iUserId.'/'.$data->vPhoto.'"/>';
				}else{
				$htmlMail .='<img style="border:1px solid #dbdbdb;" height="120px" width="150px" src="'.$admin_image_path.'/noimage.jpg"/>';
				}
				$htmlMail .='</div>
				<div >
				<div  style="color: #666666; font-size: 14px; padding: 8px 0 0px 5px; font-weight:bold;">'.$data->vFirstName.'  '.$data->vLastName.'</div>
				'.$data->tAddress.' , <br />
				 '.$data->vState.'-'.$data->vCountry.'<br />
				<strong>Phone No :</strong> '.$data->vShippPhone.'<br />
				<strong>Email :</strong> <a style="text-decoration:none; color:#666666;" href="">'.$data->vShippEmail.'</a><br />
				 </div>
			</div></div>	
				
				
			<div  style="clear:both;"></div>
			<div  style="margin-top:20px;">
				<div  style="font-size:18px; font-weight:bold; padding-bottom:10px;">Order Info</div>
				<table style="background:#d2d2d2;" width="100%" cellpadding="0" cellspacing="1">
					<tr>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;">Product Name</th>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;">Size</th>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;">Color</th>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;" width="100px">Amount</th>
					</tr>';
					
					
					$htmlMail .='<tbody  style="background: #FFFFFF;">
						<tr>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vProductName.'</td>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vTitle.'</td>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vColor.'</td>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->fPrice.'</td>
						</tr>
					</tbody>';
					
					
				$htmlMail .='</table>
			</div><div  style="clear:both;"></div>
			<div  style="font-size:13px; width:220px; float:right; line-height:30px; padding-top:0px; border:1px solid #d2d2d2; border-top:0px; background:#fff;">
				<table cellpadding="0" cellspacing="1" width="100%">  
					<tbody><tr>	
						<td  style="text-align:right; padding-right:10px; width:88px; background:#2F83B4;">Shipping Charge</td>
						<td style="padding-left:15px; background:#DFF1FB; color:#666666;" width="100">$'.$data->vShippingCharge.'</td>
					</tr>
					<tr>	
						<td  style="text-align:right; padding-right:10px; width:88px; background:#2F83B4;">Express Charge</td>
						<td style="padding-left:15px; background:#DFF1FB; color:#666666;">$'.$data->fSubTotal.'</td>
					</tr>
					<tr>	
						<td  style="text-align:right; padding-right:10px; width:88px; background:#2F83B4;"><strong>Grand Total</strong></td>
						<td style="padding-left:15px; background:#DFF1FB; color:#666666;"><strong>$'.$data->fGrandTotal.'</strong></td>
					</tr>
				</tbody></table>
			</div>
			<div  style="clear:both;"></div>
			
		</div></div></div>
	
	</body>
		</html>';
	  
	  
	 //echo $htmlMail; exit;
	  
	  $res = @mail($to,$subject,$htmlMail,$headers);
		if($res)
		{
			 $msg = 'Order mail sent successfully';
		}else
		{
			$msg =  'Order mail sent fail';
		}
	redirect(admin_url.'order/showorder?iOrderId='.$iOrderId.'&iUserId='.$iUserId.'&msg='.$msg);
		exit;
	  
	  
	}

    public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr){  
	    
		/*echo $EmailCode."<br>";
		 echo $SendType."<br>";
		 echo $ToEmail."<br>";
		 echo "<pre>";print_r($bodyArr);
		 echo "<pre>";print_r($postArr);exit;
		*/
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";		
		$email_info = $this->order_model->list_sysemaildata($ssql)->result();
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: emblemax.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
				
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		$this->body = str_replace($bodyArr,$postArr, $this->body);
		$To = stripcslashes($ToEmail);
		//echo$To ;exit; 
		$htmlMail = '	
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		    <html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>Emblemax</title>
		    </head>
		    
		    <body style="padding:0; margin:0; border:0;">
			 <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			      <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
				   <div style="background:#363F49; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt="" height="50px"/></div>
					'.$this->body.'
				   </div>
			      </div>
			 </div>
		    </body>
	       </html>';
	       
		//echo $htmlMail;exit;
	       if($_SERVER['SERVER_ADDR'] == '192.168.1.41'){ // for localhost server
			 
			 require_once "Mail.php";
			 require_once "Mail/mime.php";
			 $from = "demo3.testing3@gmail.com";			   
			 //echo $from;exit;
			 $to =$To;
			 $subject = "EmbleMax - Thank your for your order request";
			 $crlf = "\n";
			 $html = "<h1> This is HTML </h1>";
			 $headers = array('From' => $from,'To' => $to,'Subject' => $subject);
			 $host = "smtp.gmail.com";
			 $username = "demo2.testing2@gmail.com";
			 $password = "demo1234";
			 $mime =  new Mail_mime(array('eol' => $crlf));
			 $mime->setHTMLBody($htmlMail);			
			 $body = $mime->getMessageBody();			
			 $headers = $mime->headers($headers);
			 
			 $smtp = Mail::factory("smtp",array("host" => $host,"auth" => true,"username" => $username,"password" => $password));			 
			 $res = $smtp->send($to, $headers, $body);			 
		}else
		{ // for live server
			 //echo "else";exit;
			 $res = mail($To,$Subject,$htmlMail,$headers);
		}
	     return $res; 
		//redirect(site_url.'myAccount/myDashboard');
		//redirect(site_url.'registration/registration_success');
		//return $var_msg;
     }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
