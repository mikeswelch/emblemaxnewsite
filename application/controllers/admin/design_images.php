<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Design_Images extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/design_images_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'design_images');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		$this->design_imageslist();
	}
    
	function design_imageslist()
	{
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';						
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
								
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vTitle LIKE '".stripslashes($alp)."%' OR vTitle LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
		    $ssql.= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		    //echo $ssql;exit;
		}
		if($_SESSION['module_name'] != 'design_images_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		
		$totrec = $this->design_images_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		include($site_path."system/libraries/paging.inc.php");
		
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vTitle'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
					
		$data = $this->design_images_model->list_all($var_limit,$ssql,$field,$sort)->result();

		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->design_images_model->displayalphasearch()->result();

		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'design_images/design_imageslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'design_images/design_imageslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'design_images/design_imageslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		

		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'design_images_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/design_images/view-design_images.tpl');
	}
	/*function do_upload($user_id)
	{
		//echo $user_id;exit;
		if(!is_dir('uploads/user/')){
			@mkdir('uploads/user/', 0777);
		}
		if(!is_dir('uploads/user/'.$user_id)){
			@mkdir('uploads/user/'.$user_id, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/user/'.$user_id,
		  'max_size'=>2000
		);
		$this->load->library('upload', $config);
		//echo "<pre>";
		//print_r($this->load);exit;
		$this->upload->do_upload('vphoto'); //do upload
		
		//want to create thumbnail
		 
		$image_data = $this->upload->data(); //get image data
		
		
		$config = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/user/'.$user_id.'/1_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => true,
		  'width' => 176,
		  'height' => 176
		);
	         //echo "<pre>";
		//print_r($config);exit;
		$this->load->library('image_lib', $config); //load library
		$this->image_lib->resize(); //do whatever specified in config
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}*/
	function do_upload($iDesignImageId)
	{
		if(!is_dir('uploads/design_images/')){
			@mkdir('uploads/design_images/', 0777);
		}
		if(!is_dir('uploads/design_images/'.$iDesignImageId)){
			@mkdir('uploads/design_images/'.$iDesignImageId, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/design_images/'.$iDesignImageId,
		  'max_size'=>2000
		);
		$this->load->library('upload', $config); 
		$this->upload->do_upload('vImage'); //do upload
		$image_data = $this->upload->data(); //get image data
		
                $config1 = array(
			'source_image' => $image_data['full_path'], //get original image
			'new_image' => 'uploads/design_images/'.$iDesignImageId.'/90x1_'.$image_data['file_name'], //save as new image //need to create thumbs first
			'width' => 90,
			'height' => 1,
			'maintain_ratio' => TRUE,
			'master_dim' => 'width'
		);
		$this->load->library('image_lib', $config1);
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		
		$img_uploaded = $image_data['file_name'	];
		return $img_uploaded;
	}
        
	function add()
	{
		$parentarray = $this->design_images_model->getParentCatNew('0','','0','1','0');
	        $this->smarty->assign("parentarray",$parentarray);
		
		$db_dimages = $this->design_images_model->list_dimages()->result();
		$this->smarty->assign("db_dimages",$db_dimages);
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'design_images/add';
		
		$this->smarty->assign('db_dimages',$db_dimages);
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/design_images/design_images.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$id = $this->design_images_model->save($Data);			
			$img_uploaded = $this->do_upload($id);
			
			$Data['vImage'] = $img_uploaded;
			$id = $this->design_images_model->update($id,$Data);
			if($id)$var_msg = "Clipart is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'design_images/design_imageslist?msg='.$var_msg);
				exit;
		}
	}
	
	function edit()
	{
                
		$db_dimages = $this->design_images_model->list_dimages()->result();
		$this->smarty->assign("db_dimages",$db_dimages);

		$parentarray = $this->design_images_model->getParentCatNew('0','','0','1','0');
	        $this->smarty->assign("parentarray",$parentarray);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['iDesignImageId'];
		//echo "<pre>";
		//print_r($id); exit;
		$admin_url = $this->config->item('admin_url');
		
		
		$data = $this->design_images_model->get_one_by_id($id)->row();
				
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'design_images/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/design_images/design_images.tpl');
		
		if($_POST)
		{
			
			//echo "<pre>";
			//print_r($_POST['Data']);exit;
			$Data = $_POST['Data'];
			$data.= $_POST['iDesignCategoryId'];
			$iDesignImageId = $_POST['iDesignImageId'];
			$img_uploaded = $this->do_upload($iDesignImageId);
			if($img_uploaded)
				$Data['vImage'] = $img_uploaded;
			
			$id = $this->design_images_model->update($iDesignImageId,$Data);
			if($id)$var_msg = "Clipart is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'design_images/design_imageslist?msg='.$var_msg);
			exit;
		}
	}
	
	function delete()
	{
		$var = $this->design_images_model->delete($_REQUEST['id']);				
		if($var)$var_msg = "Clipart is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		//$var = $this->user_model->delete($id);
		//if($var)$var_msg = "User is deleted successfully.";else $var_msg="Error-in delete.";
		redirect(admin_url.'design_images/design_imageslist?msg='.$var_msg);
		exit;
	}
	
	function make_active($action,$iDesignImageId)
	{
		
		//echo "<pre>";
		//print_r($user_id);exit;
		$id = $this->design_images_model->multiple_update_status($iDesignImageId,$action);
		//echo "<pre>";
		//print_r($user_id);exit;
		$DesignImageIdcnt  = @explode("','",$iDesignImageId);
		$cnt=count($DesignImageIdcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Clipart activated successfully.";
		      }else{
		          $var_msg = $cnt." Clipart is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'design_images/design_imageslist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
	function search_action(){
        
		$action = $_POST['action'];
		$iDesignImageId = $_POST['commonId'];
		
		if($iDesignImageId == '')
		$iDesignImageId = $_POST['iDesignImageId'];
		 	
		if(is_array($iDesignImageId)){
		    $iDesignImageId  = @implode("','",$iDesignImageId);
		}
		$iDesignImageId = $iDesignImageId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iDesignImageId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iDesignImageId);
		}else{
	    }
	}
	function make_delete($iDesignImageId){
		//echo "<pre>";
		//print_r("fsklgv;flk");exit;
		$id = $this->design_images_model->delete_data($iDesignImageId);		
		$DesignImageIdcnt  = @explode("','",$iDesignImageId);
		$cnt=count($DesignImageIdcnt);	
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'design_images/design_imageslist?msg='.$var_msg);
		exit;
	}
	function deleteimage()
	{
		$upload_path = $this->config->item('upload_image_path');
		
		$iDesignImageId = $_REQUEST['id'];
		$data = $this->design_images_model->get_one_by_id($iDesignImageId)->result();
		
		
		
		$var = unlink($upload_path.'design_images/'.$iDesignImageId.'/'.$data[0]->vImage);
		//$var = unlink($upload_path.'user/'.$userid.'/1_'.$data[0]->vphoto);
		
		
		if($var)
		{
			$var = $this->design_images_model->delete_image($iDesignImageId);
			
		}
		if($var)
		{
			$var_msg = "Photo deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'design_images/edit?iDesignImageId='.$iDesignImageId.'&msg='.$var_msg);
		exit;
	}
			
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
