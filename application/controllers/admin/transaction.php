<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Controller{

	var $limit = '';
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/transaction_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		$site_url = $this->config->item('site_url');
		 
		$this->smarty->assign("site_url",$site_url);
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'user');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		$this->transactionlist();
	}
    
	function transactionlist()
	{
		
		$limit1 = $this->transaction_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		$AlphaBox ='';
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		/*Filteration start*/
		$showTime=0;	
		if(isset($_POST['filterBy'])){
			if($_POST['filterBy']=='date_range' && $_POST['start_date']!='' && $_POST['end_date']!=''){
				$ssql1 = '';
				$start_date = $_POST['start_date'].' 00:00:00';
				$end_date = $_POST['end_date'].' 23:59:59';
				$ssql1 =" AND dPaymentDate between '$start_date' AND '$end_date'";
				$ssql = $ssql1;
				$filterBy = 'searchByDate';
				$start_date = $_POST['start_date'];
				$end_date = $_POST['end_date'];
				$this->smarty->assign("start_date",$start_date);
				$this->smarty->assign("end_date",$end_date);
			}
			if($_POST['filterBy']=='day' && $_POST['select_day']!=''){
				$ssql1 = '';
				$start_day = $_POST['select_day'].' 00:00:00';
				$end_day= $_POST['select_day'].' 23:59:59';
				$ssql1 =" AND dTransactionDate between '$start_day' AND '$end_day'";
				$showTime=1;
				$ssql = $ssql1;
				$filterBy = 'searchByDay';
				$day = $_POST['select_day'];
				$this->smarty->assign("day",$day);
			}
			if($_POST['filterBy'] == 'PaymentStatus' && $_POST['PaymentStatus']){
				$ssql1 = '';
				$eStatus = $_POST['PaymentStatus'];
				$ssql1 =" AND eStatus ='$eStatus'";
				$ssql = $ssql1;
				$filterBy = 'searchByPaymentStatus';
				$this->smarty->assign("eStatus",$eStatus);
				
			}
			if($_POST['filterBy'] == 'order_number' && $_POST['order_number']){
				$ssql1 = '';
				$vOrderNum = $_POST['order_number'];
				$ssql1 =" AND vOrderNum ='$vOrderNum'";
				$ssql = $ssql1;
				$filterBy = 'searchByOrderNumber';
				$this->smarty->assign("vOrderNum",$vOrderNum);
			}
			if($_POST['filterBy'] == 'transaction_amount' && $_POST['tansaction']){
				$ssql1 = '';
				$fGrandTotal = $_POST['tansaction'];
				$ssql1 =" AND fTotalAmt ='$fGrandTotal'";
				$ssql = $ssql1;
				$filterBy = 'searchByTransactionAmount';
				$this->smarty->assign("fTotalAmt",$fGrandTotal);
			}
			#echo "<pre>";
			#print_r($_POST);exit;
		}
		
		$this->smarty->assign("showTime",$showTime);
		/*Filteration end*/
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		$iUserId=$_REQUEST['iUserId'];
		if($iUserId == '' && $_REQUEST['commonid'] != '')
		$iUserId = $_REQUEST['commonid'];
		elseif($iUserId == '' && $_REQUEST['pcommonid'] != '')
		$iUserId = $_REQUEST['pcommonid'];
		
		$pcommonid = $iUserId;
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
		
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql = " AND (name LIKE '".stripslashes($alp)."%' OR name LIKE '".strtolower(stripslashes($alp))."%' )";		    
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !='' && $_REQUEST['option'] != 'dTransactionDate'){
		   $keyword = $_REQUEST['keyword']; 
		}elseif($_REQUEST['option'] == 'dTransactionDate'){
		   $keyword = $_REQUEST['keyword'];
		   $keyword1 = date("Y-m-d",strtotime($keyword));
		   $start_day = $keyword1.' 00:00:00';
		   $end_day = $keyword1.' 23:59:59';
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != '' && $option != 'dAddedDate'){		    
		    $ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";		    
		}elseif($_REQUEST['option'] == 'dAddedDate'){
		    $ssql =" AND $option between '$start_day' AND '$end_day'";		    
		}
		
		/*if(isset($_REQUEST['iUserId']) !=''){
			$iUserId=$_REQUEST['iUserId'];
		   $ssql = " AND iUserId = '".$_REQUEST['iUserId']."'";
		}*/
		
		if($_SESSION['module_name'] != 'transaction_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->transaction_model->count_all($ssql,$iUserId)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vOrderNum'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vShippingCharge'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vExpressCharge'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   elseif($field =='fTotalAmt'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->transaction_model->list_all($var_limit,$ssql,$field,$sort,$iUserId)->result();
		
		for($k=0;$k<count($data);$k++){
		$total=$total+$data[$k]->fTotalAmt;
		}
		for($i=0;$i<count($data);$i++){
			$date= $data[$i]->dPaymentDate;
			$data[$i]->dPaymentDate= date('m/d/Y',strtotime($date));
			
		}
		
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$limit;
		$startrec = $num_limit;
		$lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				//$recmsg="No records found.";
				$recmsg = '';
			}
	        
		
		$db_alp = $this->transaction_model->displayalphasearch()->result();
		
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->name, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faq/faqlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faq/faqlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faq/faqlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$AlphaBox.='</ul>';
		
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'transaction_model';
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		$this->smarty->assign("total",$total);
		if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$site_url = $this->config->item('site_url');		 
		$this->smarty->assign("site_url",$site_url);
		
		$this->smarty->assign("iUserId",$iUserId);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/transaction/view_transaction.tpl');
	}
	
	//showinvoice
	function showinvoice()
	{
		
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
		
		$iUserId=$_REQUEST['iUserId'];
		$iOrderId=$_REQUEST['iOrderId'];
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);

		$admin_url = $this->config->item('admin_url');
		//$action = $admin_url.'common/add';
		
		$invoicedata = $this->transaction_model->show_invoicedata($iOrderId)->result();
		 
		$country = $this->transaction_model->show_country($invoicedata[0]->iCountryId)->result();
		$state = $this->transaction_model->show_state($invoicedata[0]->iStateId)->result();
		$invoicedata[0]->vState=$state[0]->vState;
		$invoicedata[0]->vCountry=$country[0]->vCountry;
		$date = $invoicedata[0]->dPaymentDate;
		$invoicedata[0]->dPaymentDate= date('dS M Y',strtotime($date));
		
		$this->smarty->assign("iUserId",$iUserId);		
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->assign("invoicedata",$invoicedata);
		$this->smarty->view('admin/transaction/transaction.tpl');
		
		
	}
	
	function printtransaction()
	{
		
		$iUserId=$_REQUEST['iUserId'];
		$iOrderId=$_REQUEST['iOrderId'];
		
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);

		$admin_url = $this->config->item('admin_url');
	        
	        if($iOrderId !='')
		{
		$invoicedata = $this->transaction_model->show_invoicedata($iOrderId)->result();
		}
		$date = $invoicedata[0]->dPaymentDate;
		$invoicedata[0]->dPaymentDate= date('dS M Y',strtotime($date));
		$country = $this->transaction_model->show_country($invoicedata[0]->iCountryId)->result();
		$state = $this->transaction_model->show_state($invoicedata[0]->iStateId)->result();
		$invoicedata[0]->vState=$state[0]->vState;
		$invoicedata[0]->vCountry=$country[0]->vCountry;
		
		$this->smarty->assign("iUserId",$iUserId);
		$this->smarty->assign("invoicedata",$invoicedata);
		$this->smarty->view('admin/transaction/printtransaction.tpl');

        }
	function sendmail()
	{
		
          $upload_path = $this->config->item('upload_path');
	  $admin_css_path = $this->config->item('admin_css_path');
	  $admin_url = $this->config->item('admin_url');
	  $admin_image_path = $this->config->item('admin_image_path');
	  
	  $iUserId=$_REQUEST['iUserId'];
	  $iOrderId=$_REQUEST['iOrderId'];
		
		if($iOrderId !='')
		{
		$data = $this->transaction_model->show_invoicedata($iOrderId)->row();
		}
		
		$date = $data->dPaymentDate;
		$data->dPaymentDate= date('d-M-Y',strtotime($date));		
		$country = $this->transaction_model->show_country($data->iCountryId)->result();
		$state = $this->transaction_model->show_state($data->iStateId)->result();
		$data->vState=$state[0]->vState;
		$data->vCountry=$country[0]->vCountry;
		
		
	  
	  $to=$data->vEmail;
	  
	  //$to="dipakranjan.khamari@php2india.com";
	  $subject="Invoice";
	  $headers = "MIME-Version: 1.0" . "\r\n";
          $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	  $headers .='From: Onwardz.com <support@onwardz.com>' . "\r\n".
				'Reply-To: Onwardz.com <support@onwardz.com>'. "\r\n".
				'Return-Path: Onwardz.com <support@onwardz.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
		
	 $htmlMail ='<!DOCTYPE html>
		<html>
		<head>
		<meta charset="utf-8">
		<title>Onwardz</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
		</head>
		<body>
		<div style="border:10px solid #9BC55F; background:#fcfcfc; font-family:Arial, Helvetica, sans-serif; width:970px;">
		  <div style="background:#000; padding: 10px 10px 10px 10px;"><img src="'.$admin_image_path.'logo.png" alt=""/></div>
		  <div style="padding: 10px 10px 10px 15px; font-size:14px; color:#000;">
		  
                <div  style="padding: 20px 2% 20px 2%;">
			<div  style="width:300px; float:right; padding: 0px; border-radius:3px;">
				<div  style="background:#9BC55F; border-radius: 0 15px 0 0; color: #677B4B; font-family: segoewpsemibold; font-size: 14px; font-weight: bold; padding: 6px 10px 7px;  text-shadow: 0 1px 1px #B6E07A;">Order Information</div>
				<div  style="color:#666666; line-height:22px; background:#d7ebfe;background:#fff; border: 1px solid #E8E7E7; border-radius: 0 0 0 20px; overflow:hidden; border-top:0px;">
					<table cellpadding="0" cellspacing="1" width="130%">
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Order Number</td>
							<td><input type="text" readonly="readonly" style="width:60%; height:22px; background:#f4f4f4;width:92%; border:0px;  border:0px; border-radius:0px; " value="'.$data->vOrderNum.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Order Date</td>
							<td><input type="text" readonly="readonly" style="width:60%;height:22px; background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->dPaymentDate.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Shipping Charge</td>
							<td><input type="text" readonly="readonly" style="width:60%;height:22px; background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->vShippingCharge.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9; padding-left:10px;">Express Charge</td>
							<td><input type="text" readonly="readonly" style="width:60%;height:22px; background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->vExpressCharge.'" /></td>
						</tr>
						<tr>
							<td  style="width:40%; background:#e9e9e9;padding-left:10px;">Amount</td>
							<td><input type="text" readonly="readonly" style="width:60%; height:22px;background:#f4f4f4; width:92%; border:0px;  border:0px; border-radius:0px;" value="'.$data->fTotalAmt.'" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div  style="width:50%;">
				<div  style="background:#9BC55F; border-radius: 0 15px 0 0; color: #677B4B; font-family: segoewpsemibold; font-size: 14px; font-weight: bold; padding: 6px 10px 7px;  text-shadow: 0 1px 1px #B6E07A;">User Information</div>
			<div  style="font-size:13px; line-height:20px; font-family:Arial, Helvetica, sans-serif; background: #E9E9E9; border: 1px solid #E8E7E7; border-radius: 0 0 0 20px; padding: 16px 21px 16px 21px; color:#666666; text-shadow: 0px 1px 1px #fff;">
				
				<div  style="padding: 0px 0px 5px 0px; float:left">';
				if ($data->vPhoto != ''){
				$htmlMail .='<img style="border:1px solid #dbdbdb;" height="120px" width="140px" src="'.$upload_path.'user/'.$data->iUserId.'/'.$data->vPhoto.'"/>';
				}else{
				$htmlMail .='<img style="border:1px solid #dbdbdb;" height="120px" width="150px" src="'.$admin_image_path.'/noimage.jpg"/>';
				}
				$htmlMail .='</div>
				<div >
				<div  style="color: #666666; font-size: 14px; padding: 8px 0 0px 5px; font-weight:bold;">'.$data->vFirstName.'  '.$data->vLastName.'</div>
				'.$data->tAddress.' , <br />
				 '.$data->vState.'-'.$data->vCountry.'<br />
				<strong>Phone No :</strong> '.$data->vPhone.'<br />
				<strong>Email :</strong> <a style="text-decoration:none; color:#666666;" href="">'.$data->vEmail.'</a><br />
				 </div>
			</div></div>	
				
				
			<div  style="clear:both;"></div>
			<div  style="margin-top:20px;">
				<div  style="font-size:18px; font-weight:bold; padding-bottom:10px;">Order Info</div>
				<table style="background:#d2d2d2;" width="100%" cellpadding="0" cellspacing="1">
					<tr>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;">Product Name</th>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;">Size</th>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;">Color</th>
						<th style="background:#9BC55F; color: #578B0E; font-size: 15px; font-weight: bold; padding: 5px 0 5px 10px; text-align: left; text-shadow: 1px 1px 1px #BBE67F;" width="100px">Amount</th>
					</tr>';
					
					
					$htmlMail .='<tbody  style="background: #FFFFFF;">
						<tr>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vProductName.'</td>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vSizeName.'</td>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vColorCode.'</td>
							<td style="color:#a4a2a2; font-size:13px; text-align:left; padding: 5px 0px 5px 10px;">'.$data->vPrice.'</td>
						</tr>
					</tbody>';
					
					
				$htmlMail .='</table>
			</div><div  style="clear:both;"></div>
			<div  style="font-size:13px; width:220px; float:right; line-height:30px; padding-top:0px; border:1px solid #d2d2d2; border-top:0px; background:#fff;">
				<table cellpadding="0" cellspacing="1" width="100%">  
					<tbody><tr>	
						<td  style="text-align:right; padding-right:10px; width:88px; background:#2F83B4;">Shipping Charge</td>
						<td style="padding-left:15px; background:#DFF1FB; color:#666666;" width="100">$'.$data->vShippingCharge.'</td>
					</tr>
					<tr>	
						<td  style="text-align:right; padding-right:10px; width:88px; background:#2F83B4;">Express Charge</td>
						<td style="padding-left:15px; background:#DFF1FB; color:#666666;">$'.$data->vExpressCharge.'</td>
					</tr>
					<tr>	
						<td  style="text-align:right; padding-right:10px; width:88px; background:#2F83B4;"><strong>Grand Total</strong></td>
						<td style="padding-left:15px; background:#DFF1FB; color:#666666;"><strong>$'.$data->fTotalAmt.'</strong></td>
					</tr>
				</tbody></table>
			</div>
			<div  style="clear:both;"></div>
			
		</div></div></div>
	
	</body>
		</html>';
	  
	  
	// echo $htmlMail; exit;
	  
	  $res = @mail($to,$subject,$htmlMail,$headers);
		if($res)
		{
			 $msg = 'Order mail sent successfully';
		}else
		{
			$msg =  'Order mail sent fail';
		}
	redirect(admin_url.'transaction/showinvoice?iOrderId='.$iOrderId.'&iUserId='.$iUserId.'&msg='.$msg);
		exit;
	  
	  
	}
	
	function delete()
	{
		$iUserId = $_REQUEST['commonid'];
		
		$var = $this->transaction_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Transaction is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'transaction/transactionlist?msg='.$var_msg.'&iUserId='.$iUserId.'&ssql='.$ssql);
		exit;
	}
	
	function make_active($action,$iOrderId)
	{
		
		$id = $this->transaction_model->multiple_update_status($iOrderId,$action);
		$useridcnt  = @explode("','",$iOrderId);
		$cnt=count($useridcnt);
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'transaction/transactionlist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	function search_action(){
		$iUserId = $_POST['iUserId'];
		$action = $_POST['action'];
		$iOrderId = $_POST['commonId'];
		if($iOrderId == '')
		$iOrderId = $_POST['iOrderId'];
			
		if(is_array($iOrderId)){
		    $iOrderId  = @implode("','",$iOrderId);
		}
		$iOrderId = $iOrderId;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iOrderId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iOrderId,$iUserId);
		}else{
			$this->show_all($iUserId);
	    }	
	}
	
	function make_delete($commonid){
		$id = $this->transaction_model->delete_data($commonid);
		
		$useridcnt  = @explode("','",$commonid);
		$cnt=count($useridcnt);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'transaction/transactionlist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: show_all
	 Developer Name: Deepak Khamari
	 Purpose: to be called  show all data
	 Created Date: 12-11-2012
	*/
	function show_all($iUserId)
	{
	redirect(admin_url.'transaction/transactionlist?action=Show All&iUserId='.$iUserId);
		exit;	
	}
	/*function export()
	{
	   
	   $ssql=$_REQUEST['ssql'];
	   $ssql = base64_decode($ssql);
	    
	   $csv_output ='';
           header("Content-type: text/html; charset=utf-8");
           $rowcsv='';

	   $result = $this->transaction_model->exportData($ssql)->result();
           $count = count($result);
	   
	  
	   
	$locationfilepath = "all_transaction.csv";
        $locationfilepathurl =  "admin/all_transaction.csv";
        $output = fopen($locationfilepath, 'w');
        $rowcsv.= 'Invoice Number';
        $rowcsv.= "*".'Card Type';
	$rowcsv.= "*".'Name On Card';
	$rowcsv.= "*".'Expiry Date';
	$rowcsv.= "*".'Credit Card TransactionId';
	$rowcsv.= "*".'Currency';
	$rowcsv.= "*".'Subtotal';
	$rowcsv.= "*".'Vat Price';
	$rowcsv.= "*".'Grand Total';
	$rowcsv.= "*".'First Name';
	$rowcsv.= "*".'Last Name';
	$rowcsv.= "*".'Address';
	$rowcsv.= "*".'City';
	$rowcsv.= "*".'Transaction Date';
	$rowcsv.= "*".'City';
	$rowcsv.= "*".'Status';
        $rowcsv.= "\n";
        ;
        fwrite($output, $rowcsv);
        
	for ($i=0;$i<$count;$i++) {
            
            $rowcsv.= $result[$i]->vInvoiceNumber;
            $rowcsv.= "*".$result[$i]->eCardType;
	    $rowcsv.= "*".$result[$i]->vNameOnCard;
	    $rowcsv.= "*".$result[$i]->vExpiryDate;
	    $rowcsv.= "*".$result[$i]->vCreditCardTransactionId;
	    $rowcsv.= "*".$result[$i]->vCurrency;
	    $rowcsv.= "*".$result[$i]->fSubTotal;
	    $rowcsv.= "*".$result[$i]->fVatPrice;
	    $rowcsv.= "*".$result[$i]->fGrandTotal;
	    $rowcsv.= "*".$result[$i]->vFirstName;
	    $rowcsv.= "*".$result[$i]->vLastName;
	    $rowcsv.= "*".$result[$i]->vAddress;
	    $rowcsv.= "*".$result[$i]->vCity;
	    $rowcsv.= "*".$result[$i]->dTransactionDate;
	    $rowcsv.= "*".$result[$i]->vCity;
	    $rowcsv.= "*".$result[$i]->eStatus;
            $rowcsv.= "\n";
        }*/
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
