<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class State extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	var $limit = '';
    
	function __construct() {
		    
		parent::__construct();
		$this->load->library('session');
		
		
		$this -> load -> model('admin/state_model', '', TRUE);
		
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){ 
		redirect(admin_url.'authentication/login');
		exit ; 
		}
		$this->smarty->assign("filename",'state');    

		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
			
	}
	/*      Function Name: index
		Developer Name: Deepak Khamari
		Purpose: to be called when the State of admin page loads
		Created Date: 12-05-2012
	*/
	function index() {
		$this->statelist();
	}
	/*      Function Name: statelist
		Developer Name: Deepak Khamari
		Purpose: to be called when Show a listing.
		Created Date: 12-05-2012
	*/
	function statelist()
	{
		
		$limit1 = $this->state_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		if($this->limit == ''){
                       $this->limit = $limit;
                 }
		$PAGELIMIT = $limit1[1]->vValue;
		
		$AlphaBox ='';
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
			
		}else{
			$ssql = '';
		}
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
                
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND vState LIKE '".stripslashes($alp)."%'";
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword'];
		   //$keyword = str_replace("'","'", $keyword);
		   //$keyword = Sana;
		   //echo $keyword;exit; 
		}else{
		    $keyword = '';
		     //echo $keyword;exit; 
		}
		if($option != '' && $option=='Country' && $keyword != '')
		{
			$ssql= ' AND c.vCountry LIKE "%'.stripslashes($keyword).'%"';
		}
		else if($option != '' && $option=='vCountry' && $keyword != '')
		{
			$ssql= ' AND c.vCountry LIKE "%'.stripslashes($keyword).'%"';
		}
		else if ($option != '' && $option=='eStatus' && $keyword != '')
		{
			$ssql= ' AND s.'.stripslashes($option).' LIKE "%'.stripslashes($keyword).'%"';
		}
		else if($option != '' && $keyword != ''){			
			$ssql= ' AND s.'.stripslashes($option).' LIKE "%'.stripslashes($keyword).'%"';
		}

		if($_SESSION['module_name'] != 'state_model' || $_REQUEST['action'] == 'Show All'){
			$_SESSION['ssql_action'] = '';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->state_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		  
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   if($field =='vState'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vCountry'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vstatecode'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='count'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='estatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}

		
		$data = $this->state_model->list_all($var_limit,$ssql,$field,$sort)->result();
		for($k=0;$k<count($data);$k++)
		{
		 	$iStateId=$data[$k]->iStateId;
			$cnt = $this->state_model->count($iStateId)->result();
			$count = $cnt[0]->tot;
			$data[$k]->count=$count;
			
		}
		if($start == '0'){
			$start = 1;
		}
		
                $num_limit = ($start-1)*$limit;
    	        $startrec = $num_limit;
    	        $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="";
			}
	
                $db_alp = $this->state_model->displayalphasearch()->result();
		
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vState, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'state/statelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'state/statelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'state/statelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		#echo $keyword; exit;
		$AlphaBox.='</ul>';
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'state_model';
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		
		$this->smarty->view( 'admin/state/view-state.tpl');
	}
	/*
	 Function Name: add
	 Developer Name: Deepak Khamari
	 Purpose: to be called when  save data
	 Created Date: 12-05-2012
	 */
	function add()
	{
		$db_country = $this->state_model->list_country()->result();
                $this->smarty->assign("db_country",$db_country);
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("admin_url",$admin_url);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$upload_path = $this->config->item('upload_path');
	        $this->smarty->assign("upload_path",$upload_path);
		
		$action = site_url('state/add');
		$this->smarty->assign('operation','add');
	        $this->smarty->assign("action",$action);
	        
                $this->smarty->view('admin/state/state.tpl');
		
		
         if($_POST)
	     {
		$Data = $_POST['Data'];
		$id = $this->state_model->save($Data);		
		if($id)$var_msg = "State is added successfully.";else $var_msg="Error-in add.";
		redirect(admin_url.'state/statelist?msg='.$var_msg);
		exit;
		}
	}
	/*
	 Function Name: edit
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data update
	 Created Date: 12-05-2012
	 */
        function edit()
	{
		$db_country = $this->state_model->list_country()->result();
                $this->smarty->assign("db_country",$db_country);
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		
		$id = $_REQUEST['iStateId'];
		
		$admin_url = $this->config->item('admin_url');
		$data = $this->state_model->get_one_by_id($id)->row();
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'state/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/state/state.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$iStateId = $_POST['iStateId'];
			$id = $this->state_model->update($iStateId,$Data);
			if($id)$var_msg = "State is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'state/statelist?msg='.$var_msg);
			exit;
		}	
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-05-2012
	 */
	function delete()
	{
		$var = $this->state_model->delete($_REQUEST['id']);
		if($var)$var_msg = "State is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'state/statelist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: search_action
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the search status wise
	 Created Date: 12-05-2012
	 */
	function search_action(){
      
		$action = $_POST['action'];
		$iStateId = $_POST['commonId'];
		if($iStateId == '')
		$iStateId = $_POST['iStateId'];
			
		if(is_array($iStateId)){
		    $iStateId  = @implode("','",$iStateId);
		}
		$iStateId = $iStateId;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iStateId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iStateId);
		}else{
	    }
	}
	/*
	 Function Name: make_active
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the status updated active and deactive 
	 Created Date: 12-05-2012
	*/
	function make_active($action,$iStateId)
	{		
		$id = $this->state_model->multiple_update_status($iStateId,$action);
		
		$iStateId  = @explode("','",$iStateId);
		$cnt=count($iStateId);
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." State  activated successfully.";
		      }else{
		          $var_msg = $cnt."  State  inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'state/statelist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: make_delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the multiple data delete
	 Created Date: 12-05-2012
	*/
	function make_delete($iStateId){
		$id = $this->state_model->delete_data($iStateId);
		$iStateId  = @explode("','",$iStateId);
		$cnt=count($iStateId);
		
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		      
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'state/statelist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: checkStatecode
	 Developer Name: Deepak Khamari
	 Purpose: to be called when Duplicacy check.
	 Created Date: 12-19-2012
	*/
	function checkStatecode()
	{
		
		
		$newStateCode = $_REQUEST['Data']['vstatecode'];
		$iStateId = $_REQUEST['iStateId'];
		
		if(isset($_REQUEST['iStateId']) && $iStateId !='')
		{
			$iStateId = $_REQUEST['iStateId'];
			$oldCode = $this->state_model->getOldCode($iStateId)->result();
			$oldStateCode =  $oldCode[0]->vstatecode;
	   
			if($newStateCode == $oldStateCode){
				echo '0'; exit;
			}
			else{
				$code = $this->state_model->check_statecode($newStateCode)->result();
				$count = $code[0]->tot;
				
				if($count==0){
				echo "0";  exit;}
				else{ 
				echo "1";  exit;}	
			}
		}
		else{
			$code = $this->state_model->check_statecode($newStateCode)->result();
			$count = $code[0]->tot;
			
			if($count==0){
			echo "0";  exit;}
			else{ 
			echo "1";  exit;}
		}
	}
}
