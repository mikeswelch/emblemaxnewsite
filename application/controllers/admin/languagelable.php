<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Languagelable extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	private $limit = 10;
    
	function __construct() {
		    
		parent::__construct();
		$this->load->library('session');
		$this -> load -> model('admin/languagelable_model', '', TRUE);
		
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
				$filename = $this->config->item('filename');
		$this->smarty->assign("filename",$filename);    

		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
		
	
	}
	/*      Function Name: index
		Developer Name: Deepak Khamari
		Purpose: to be called when the Skill module of admin page loads
		Created Date: 12-11-2012
	*/
	 
	function index() {
		
		$this->languagelablelist();
		
	}
	/*      Function Name: languagelist
		Developer Name: Deepak Khamari
		Purpose: to be called when Show a listing.
		Created Date: 12-11-2012
	*/
	function languagelablelist()
	{
		
		//echo "fgbfgfdsg";exit;
		$limit1 = $this->languagelable_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		
		$AlphaBox ='';
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
                
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND vName LIKE '".stripslashes($alp)."%'";
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if ($option != '' && $option=='eStatus' && $keyword != ''){
			$ssql= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		else if($option != '' && $keyword != ''){
			$ssql= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		
		if($_SESSION['module_name'] != 'languagelable_model' || $_REQUEST['action'] == 'Show All'){
			$_SESSION['ssql_action'] = '';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->languagelable_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		
		
		$data = $this->languagelable_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		if($start == '0'){
			$start = 1;
		}
		
                $num_limit = ($start-1)*$limit;
    	        $startrec = $num_limit;
    	        $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="";
			}
	
                $db_alp = $this->languagelable_model->displayalphasearch()->result();
		
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vName, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'languagelable/languagelablelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'languagelable/languagelablelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'languagelable/languagelablelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'languagelable_model';
		
		$AlphaBox.='</ul>';
		$language_data = $this->languagelable_model->list_all_language()->result();
		
		$this->smarty->assign('language_data',$language_data);
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("field_id",$field_id);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/languagelable/view-langlable.tpl');
	}
	/*
	 Function Name: add
	 Developer Name: Deepak Khamari
	 Purpose: to be called when  save data
	 Created Date: 12-11-2012
	 */
	function add()
	{
		if(isset($_SESSION['sess_lang'])){
			$site_path = $this->config->item('site_path');
			include_once($site_path."application/language/".$_SESSION['sess_lang']."/".$_SESSION['sess_lang'].".php");
		}
		//echo $_SESSION['sess_lang'];exit;
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("admin_url",$admin_url);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$upload_path = $this->config->item('upload_path');
	        $this->smarty->assign("upload_path",$upload_path);
		$lable_data = $this->languagelable_model->list_all_lable()->result();
		$language_data = $this->languagelable_model->list_all_language()->result();
		
		$this->smarty->assign('newfield',$newfield);
		$this->smarty->assign('language_data',$language_data);
		$action = site_url('languagelable/add');
		$this->smarty->assign('operation','add');
	        $this->smarty->assign("action",$action);
	        
                $this->smarty->view('admin/languagelable/languagelable.tpl');
		
		
         if($_POST)
	     {
		//echo "<pre>";
		//print_r($_POST['Data']);exit;
		
		$Data = $_POST['Data'];
		$id = $this->languagelable_model->save($Data);		
		if($id)$var_msg = "Language Label is added successfully.";else $var_msg="Error-in add.";
		$this->generate_language_file();
		redirect(admin_url.'languagelable/languagelablelist?msg='.$var_msg);
		exit;
		}
	}
	/*
	 Function Name: edit
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data update
	 Created Date: 12-11-2012
	 */
        function edit()
	{
		if(isset($_SESSION['sess_lang'])){
			$site_path = $this->config->item('site_path');
			include_once($site_path."application/language/".$_SESSION['sess_lang']."/".$_SESSION['sess_lang'].".php");
		}
		//echo "dfsdfdf";exit;
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		
		$id = $_REQUEST['iLabelId'];
		
		$admin_url = $this->config->item('admin_url');
		$data = $this->languagelable_model->get_one_by_id($id)->row();
		$language_data = $this->languagelable_model->list_all_language()->result();
		
		$this->smarty->assign('language_data',$language_data);
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'languagelable/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/languagelable/languagelable.tpl');
		
		if($_POST){
			//echo "<pre>";
			//print_r($_POST['Data']);exit;
			$Data = $_POST['Data'];
			$iLabelId = $_POST['iLabelId'];
			$id = $this->languagelable_model->update($iLabelId,$Data);
			if($id)$var_msg = "Language Label is edited successfully.";else $var_msg="Error-in edit.";
			$this->generate_language_file();
			redirect(admin_url.'languagelable/languagelablelist?msg='.$var_msg);
			exit;
		}	
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-11-2012
	 */
	function delete()
	{
		$var = $this->languagelable_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Language Lable is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'languagelable/languagelablelist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: search_action
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the search status wise
	 Created Date: 12-11-2012
	 */
	function search_action(){
      
		$action = $_POST['action'];
		$iLabelId = $_POST['commonId'];
		if($iLabelId == '')
		$iLabelId = $_POST['iLabelId'];
			
		if(is_array($iLabelId)){
		    $iLabelId  = @implode("','",$iLabelId);
		}
		$iLabelId = $iLabelId;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iLabelId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iLabelId);
		}else{
	    }
	}
		
	function generate_language_file()
	{
	         // echo "dhbsfdgsdg";exit;
		//global $generalobj,$obj,$site_langlebel_path;
		$site_path = $this->config->item('site_path');
		
		
		$separator = "\n";
		//$gdbobj=new DBGeneral();
		//echo $separator;exit;
		$db_lang = $this->languagelable_model->GetLanguage()->result(); //select query for master language table for count language
		//echo "<pre>";
		//print_r($db_lang);exit;
		for($i=0;$i<count($db_lang);$i++)
		{
			$site_langlebel_path = $site_path."application/language/".$db_lang[$i]->vLangCode."/".$db_lang[$i]->vLangCode.".php";
			$res_label = $this->languagelable_model->GetLanguageLable()->result();
			//echo "<pre>";
			//print_r($res_label);exit;
			$content	=	"";
			$content       .= "<?php ".$separator.$separator;
			
			for($j=0;$j<count($res_label);$j++){
				$val = 'vValue_'.$db_lang[$i]->vLangCode;
				$content	.=	'$this->smarty->assign("'.$res_label[$j]->vName.'","'.trim(strip_tags($res_label[$j]->$val)).'");'.$separator;
				$content	.=	'$'.$res_label[$j]->vName.'="'.trim(strip_tags($res_label[$j]->$val)).'";'.$separator;
			}
			
			$content	.= $separator."?>";
			
			$filename	= $site_langlebel_path;
			
			if (!$handle = fopen($filename, 'w+'))
			{
				echo "Cannot open file ($filename)";
				exit;
			}
			// Write $somecontent to our opened file.
			
			if (fwrite($handle, $content) === FALSE)
			{
				echo "Cannot write to file ($filename)";
				exit;
			}	
		}
	}
	
	
}
