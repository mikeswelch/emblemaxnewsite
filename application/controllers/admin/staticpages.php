<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staticpages extends CI_Controller

{
	var $limit = '';
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	
	function __construct()
	{
		
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/staticpages_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ;
		}
		
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("filename",'staticpages');    

		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		
		$this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
		
	}
	/*      Function Name: index
		Developer Name: Deepak Khamari
		Purpose: to be called when the Static Pages of admin page loads
		Created Date: 12-03-2012
	*/
	function index()
	{
		$this->staticpageslist();
	}
	
	function staticpageslist()
	{
		$limit1 = $this->staticpages_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
	 if(isset($_REQUEST['ssql']))
	 {
		$ssql = $_REQUEST['ssql'];
		$ssql = base64_decode($ssql);
	 }
	 else
	 {
		$ssql = '';
	 }
	 $AlphaBox ='';
        
         $admin_url = $this->config->item('admin_url');
         $site_path = $this->config->item('site_path');
        
         if(isset($_REQUEST['msg']) !='')
	{
			$var_msg = $_REQUEST['msg'];
	}
	else
	{
			$var_msg = '';
	}
        
        
	 $this->smarty->assign("var_msg",$var_msg);
	 
	 if(isset($_REQUEST['alp']) !=''){
           $alp = $_REQUEST['alp']; 
         }else{
            $alp = '';
         }
         
         if($alp !=''){
		    $ssql = " AND vFile LIKE '".stripslashes($alp)."%'";		    
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if ($option != '' && $option=='eStatus' && $keyword != '')
		{			
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		else if($option != '' && $keyword != ''){
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";			
		}
		
		if($_SESSION['module_name'] != 'staticpages_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';			
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
        
         $totrec = $this->staticpages_model->count_all($ssql)->result();
      
         $num_totrec = $totrec[0]->tot;
        
	 if($alp !='')
	 {
		 $var_msg=$num_totrec." Record matched for ".$alp;
		 
	 }
	 if($option != '' && $keyword != ''){
	     $var_msg=$num_totrec." Record matched for ".$keyword;
	 }
		
	$ssql = base64_encode($ssql);
	include($site_path."system/libraries/paging.inc.php");
	$ssql = base64_decode($ssql);
        
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vpagename'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='estatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}

	
         $data = $this->staticpages_model->list_all($var_limit,$ssql,$field,$sort)->result();
        
         if($start == '0'){
            $start = 1;
         }
         $num_limit = ($start-1)*$limit;
    	 $startrec = $num_limit;
    	 $lastrec = $startrec + $limit;
    	 $startrec = $startrec + 1;
    	 if($lastrec > $num_totrec)
    		$lastrec = $num_totrec;
    		if($num_totrec > 0 )
    		{
    			$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
    		}
    		else
    		{
    			$recmsg="No records found.";
    		}
	
            $db_alp = $this->staticpages_model->displayalphasearch()->result();
	    
            for($i=0;$i<count($db_alp);$i++){
            $db_alp[$i] = strtoupper(substr($db_alp[$i]->vFile, 0,1));
            }
        
         $alpha_rs =implode(",",$db_alp);
         $AlphaChar = @explode(',',$alpha_rs);
         $AlphaBox.='<ul class="pagination">';
         
	 for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'staticpages/staticpageslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'staticpages/staticpageslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'staticpages/staticpageslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
         $AlphaBox.='</ul>';
	 $language_data = $this->staticpages_model->list_all_page()->result();
	 
	 $this->smarty->assign('language_data',$language_data);
	 $ssql = base64_encode($ssql);
	 $this->smarty->assign("ssql",$ssql);
	 $_SESSION['module_name'] = 'staticpages_model';
	 
	$action = $_REQUEST['action'];
	$this->smarty->assign("action",$action);
        
         if(!isset($page_link)){$page_link = '';}
	 $this->smarty->assign("option",$option);
	 $this->smarty->assign("keyword",$keyword);
         $this->smarty->assign("AlphaBox",$AlphaBox);
         $this->smarty->assign("page_link",$page_link);
         $this->smarty->assign("recmsg",$recmsg);
	 $this->smarty->assign("var_msg",$var_msg);
         $this->smarty->assign("data",$data);
	 $this->smarty->assign("order",$sort);
	 $this->smarty->assign("field",$field);
	 $this->smarty->view( 'admin/staticpages/view-staticpages.tpl');
		
	}
	function add()
	{
		$language_data = $this->staticpages_model->list_all_page()->result();
		
		$totalRec = $this->staticpages_model->count_all($ssql)->result();
		
		//increament the order number by 1 at every time 
		$totalRec = $totalRec[0]->tot;
		$initOrder =1;
		$this->smarty->assign("totalRec",$totalRec);	
		$this->smarty->assign("initOrder",$initOrder);
	  
		$this->smarty->assign('language_data',$language_data);
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'staticpages/add';
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/staticpages/staticpages.tpl');
		
		if($_POST){
		$Data = $_POST['Data'];
		$Data['vPageName'] = strtolower($Data['vFile']);
		$Data['dAddedDate'] = date("Y-m-d H:m:s");
                $id = $this->staticpages_model->save($Data);
        
                if($id)$var_msg = "Page is added successfully.";else $var_msg="Error-in add.";
			redirect(admin_url.'staticpages/staticpageslist?msg='.$var_msg);
			exit;
		}	
	}
	/*
	 Function Name: edit
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data update
	 Created Date: 12-03-2012
	 */
	function edit()
	{
		$language_data = $this->staticpages_model->list_all_page()->result();
		$this->smarty->assign('language_data',$language_data);
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$id = $_REQUEST['iSPageId'];
		$admin_url = $this->config->item('admin_url');
		$data = $this->staticpages_model->get_one_by_id($id)->row();
		$totalRec = $this->staticpages_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$initOrder =1;
		$this->smarty->assign("totalRec",$totalRec);	
		$this->smarty->assign("initOrder",$initOrder);
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'staticpages/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/staticpages/staticpages.tpl');
		if($_POST)
		{		
			$Data = $_POST['Data'];
			$iSPageId = $_POST['iSPageId'];
			//echo "<pre>";print_r($Data);exit;
                        $id = $this->staticpages_model->update($iSPageId,$Data);            
                        if($id)$var_msg = "Page is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'staticpages/staticpageslist?msg='.$var_msg);
			exit;
		}
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-03-2012
	 */
	function delete($id)
	{
                $id = $_REQUEST['id'];
		$var = $this->staticpages_model->delete($id);
		if($var)$var_msg = "Page is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'staticpages/staticpageslist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: search_action
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the search status wise
	 Created Date: 12-03-2012
	 */
	function search_action(){
        
		$action = $_POST['action'];
		$iSPageId = $_POST['commonId'];
		if($iSPageId == '')
		$iSPageId = $_POST['iSPageId'];

		if(is_array($iSPageId)){
		    $iSPageId  = @implode("','",$iSPageId);
		}
		$iSPageId = $iSPageId;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iSPageId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iSPageId);
		}else{
		}
	}
	/*
	 Function Name: make_active
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the status updated active and deactive 
	 Created Date: 12-03-2012
	*/
	function make_active($action,$iSPageId)
	{
               $id = $this->staticpages_model->multiple_update_status($iSPageId,$action);
	       
	       
	       $iSPageId  = @explode("','",$iSPageId);
		$cnt=count($iSPageId);
        	
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt."  Page activated successfully.";
		      }else{
		          $var_msg = $cnt."  Page is inactivated successfully.";
		      }
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'staticpages/staticpageslist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: make_delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the multiple data delete
	 Created Date: 12-03-2012
	*/
	function make_delete($iSPageId)
	{
	        $id = $this->staticpages_model->delete_data($iSPageId);
        	
		$iSPageId  = @explode("','",$iSPageId);
		$cnt=count($iSPageId);
		
		if($id){
		      $var_msg = $cnt."  Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'staticpages/staticpageslist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
}


