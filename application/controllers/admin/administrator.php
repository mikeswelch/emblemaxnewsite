<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends CI_Controller{
	var $limit = '';
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	
    
	function __construct()
	{
		parent::__construct();

        
		$this->load->library('pagination');
        
		$this -> load -> model('admin/admin_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("filename",'administrator');    
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ;
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		if(isset($_SESSION["sess_iAdminId"]) !=''){
		$iAdminId=$_SESSION["sess_iAdminId"];
		$this->smarty->assign("iAdminId",$iAdminId);
		}
		
	}
	/*
	 Function Name: index
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the adminstrator page loads
	 Created Date: 12-01-2012
	 */
	function index()
	{
		$this->adminlist();
	}
	/*
	 Function Name: adminlist
	 Developer Name: Parth Devmorari
	 Purpose: to be called the index function call and show listing
	 Created Date: 12-01-2012
	 */
	function adminlist()
	{

                $AlphaBox ='';
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}

		$limit1 = $this->admin_model->limit_fetch()->result();
		
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;

		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
			}else{
				$var_msg = '';
			}
		$this->smarty->assign("var_msg",$var_msg);
		
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql = " AND vFirstName LIKE '".stripslashes($alp)."%'";		    
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if ($option != '' && $option=='eStatus' && $keyword != '')
		{			
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		else if($option != '' && $keyword != ''){
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		if($_SESSION['module_name'] != 'admin_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['ssql_action'] = '';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->admin_model->count_all($ssql)->result();
	        $num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   if($field =='vFirstName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vUsername'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vEmail'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='dLastLogin'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vFromIP'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='estatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}

		$data = $this->admin_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		
		if($start == '0'){
		    $start = 1;
		}
		
		
		$num_limit = ($start-1)*$limit;
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
		$lastrec = $num_totrec;
		if($num_totrec > 0 )
		{
			$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
		}
		else
		{
			$recmsg="No records found.";
		}		
		
		$db_alp = $this->admin_model->displayalphasearch()->result();
		
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vFirstName, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'administrator/adminlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'administrator/adminlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'administrator/adminlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		
		$AlphaBox.='</ul>';
		//echo "<pre>";
		//print_r($data);exit;
		
		
		for($i=0;$i<count($data);$i++){
			if($data[$i]->dLastLogin != '0000-00-00 00:00:00')
			{
			$data[$i]->dLastLogin = date('dS M Y h:i:A',strtotime($data[$i]->dLastLogin));
			}else{
			$data[$i]->dLastLogin = '0000-00-00 00:00:00';	
			}
		}
		
		
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'admin_model';
		$action = $_REQUEST['action'];
		
		if(!isset($page_link)) $page_link = '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/administrator/view-administrator.tpl');
			
	}
	/*
	 Function Name: add
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the save data
	 Created Date: 12-01-2012
	 */
	function add()
	{
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'administrator/add';
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/administrator/administrator.tpl');
		    
		if($_POST){			
			$Data = $_POST['Data'];	
			$Data['dDate'] = date("Y-m-d");
			$Data['vPassword'] = $this->encrypt($Data['vPassword']); 
			
			$id = $this->admin_model->save($Data);
			if($id)$var_msg = "Administrator is added successfully.";else $var_msg="Error-in add.";
			redirect(admin_url.'administrator/adminlist?msg='.$var_msg);
			exit;
		}
	}
	/*
	 Function Name: edit
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the data update
	 Created Date: 12-01-2012
	 */
	function edit()
	{
		$id = $_REQUEST['iAdminId'];
		$admin_url = $this->config->item('admin_url');
		$data = $this->admin_model->get_one_by_id($id)->row();
		
		 $data->vPassword= $this->decrypt($data->vPassword);
		//echo $data->vPassword; exit;
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'administrator/edit';
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/administrator/administrator.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$Data['dDate'] = date("Y-m-d");
			$iAdminId = $_POST['iAdminId'];
                        $Data['vPassword'] = $this->encrypt($Data['vPassword']);
			//echo "<pre>";
			//print_r($Data); exit;
			$id = $this->admin_model->update($iAdminId,$Data);
			if($id)$var_msg = "Administrator is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'administrator/adminlist?msg='.$var_msg);
			exit;
		}
	}
	/*
	 Function Name: delete
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-01-2012
	 */
	function delete()
	{
		$var = $this->admin_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Administrator is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'administrator/adminlist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: make_active
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the status updated active and deactive 
	 Created Date: 12-01-2012
	*/
	function make_active($action,$iAdminId)
	{		
		$id = $this->admin_model->multiple_update_status($iAdminId,$action);
		$iAdminId  = @explode("','",$iAdminId);
		$cnt=count($iAdminId);
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt."  Administrator  activated successfully.";
		      }else{
		          $var_msg = $cnt."  Administrator  inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Eror-in active.";
		      }else{
		          $var_msg = "Eror-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'administrator/adminlist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	
	}
	/*
	 Function Name: search_action
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the search status wise
	 Created Date: 12-01-2012
	 */
	function search_action(){
		
		$action = $_POST['action'];		
		$iAdminId = $_POST['commonId'];
		if($iAdminId == '')
		$iAdminId = $_POST['iAdminId'];
		
		if(is_array($iAdminId)){
		    $iAdminId  = @implode("','",$iAdminId);
		}
		$iAdminId = $iAdminId;
		
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iAdminId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iAdminId);
		}
	}
	/*
	 Function Name: make_delete
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the multiple data delete
	 Created Date: 12-01-2012
	*/
	function make_delete($iAdminId){
		
		
		$id = $this->admin_model->delete_data($iAdminId);
		$iAdminId  = @explode("','",$iAdminId);
		$cnt=count($iAdminId);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		      
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'administrator/adminlist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: encrypt
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the encrypted data
	 Created Date: 12-01-2012
	*/
	public function encrypt($data)
	{
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$key] = $c;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return base64_encode($data);
	}
	/*
	 Function Name: decrypt
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the dencrypted data
	 Created Date: 12-01-2012
	*/
	function decrypt($data)
	{
		$data = base64_decode($data);
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$c] = $key;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}		
		return $data;
	}
	/*
	 Function Name: checkUsername
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the login user is validate or not.
	 Created Date: 12-01-2012
	*/
	function checkUsername()
	{
		$newUserName = $_REQUEST['Data']['vUserName'];
		$iAdminId = $_REQUEST['iAdminId'];
		
		if(isset($_REQUEST['iAdminId']) && $iAdminId !='')
		{
			$iAdminId = $_REQUEST['iAdminId'];
			$oldUser = $this->admin_model->getOldUserName($iAdminId)->result();
			$oldUserName =  $oldUser[0]->vUserName;
	   
			if($newUserName == $oldUserName){
				echo '0'; exit;
			}
			else{
				$user = $this->admin_model->check_username($newUserName)->result();
				$count = $user[0]->tot;
				
				if($count==0){
				echo "0";  exit;}
				else{ 
				echo "1";  exit;}	
			}
		}
		else{
			$user = $this->admin_model->check_username($newUserName)->result();
			$count = $user[0]->tot;
			
			if($count==0){
			echo "0";  exit;}
			else{ 
			echo "1";  exit;}
		}
	}
}
/* End of file administrator.php */
/* Location: ./application/controllers/admin/administrator */
