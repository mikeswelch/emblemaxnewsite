<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class zipcode extends CI_Controller{
	var $limit = '';
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	
	function __construct()
	{
		parent::__construct();

        	$this->load->library('pagination');
		$this -> load -> model('admin/zipcode_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("filename",'zipcode');    
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		{
			$iZipcodeId=$_SESSION["sess_iCarId"];
			$this->smarty->assign("iZipcodeId",$iZipcodeId);
		}
	}
	
	function index()
	{
		$this->zipcodelist();
	}
	
	function zipcodelist()
	{
	        $AlphaBox ='';
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$limit1 = $this->zipcode_model->limit_fetch()->result();
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		if(isset($_REQUEST['msg']) !='')
			{
				$var_msg = $_REQUEST['msg'];
			}
			else
			{
				$var_msg = '';
			}
		if(isset($_REQUEST['alp']) !='')
		{
			$alp = $_REQUEST['alp']; 
		}
		else
		{
			$alp = '';
		}
		if($alp !='')
		{
			$ssql = " AND iZipcode LIKE '".stripslashes($alp)."%'";
		}
		if(isset($_REQUEST['option']) !='')
		{
			$option = $_REQUEST['option']; 
		}
		else
		{
			$option = '';
		}
		if(isset($_REQUEST['keyword']) !='')
		{
			$keyword = $_REQUEST['keyword']; 
		}
		else
		{
			$keyword = '';
		}
		if ($option != '' && $option=='eStatus' && $keyword != '')
		{			
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		else if($option != '' && $keyword != '')
		{
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		if($_SESSION['module_name'] != 'zipcode_model' || $_REQUEST['action'] == 'Show All')
		{
			$ssql ='';
			$_SESSION['ssql_action'] = '';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}
		else
		{
			if($alp !='' || $keyword != '')
			{
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		$totrec = $this->zipcode_model->count_all($ssql)->result();
	        $num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
		}
		if($option != '' && $keyword != '')
		{
			$var_msg=$num_totrec." Record matched for ".$keyword;
		}
		include($site_path."system/libraries/paging.inc.php");
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='iZipcode'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='fPrice'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->zipcode_model->list_all($var_limit,$ssql,$field,$sort)->result();
		if($start == '0')
		{
			$start = 1;
		}
		$num_limit = ($start-1)*$limit;		
    	        $startrec = $num_limit;
    	        $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="";
			}
		$db_alp = $this->zipcode_model->displayalphasearch()->result();
		for($i=0;$i<count($db_alp);$i++)
		{
			$db_alp[$i] = strtoupper(substr($db_alp[$i]->iZipcode, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++)
		{
			if(!@in_array(chr($i),$AlphaChar))
			{
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}
			else
			{
				if(isset($_REQUEST['alp']) =='')
				{
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'zipcode/zipcodelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else
				{
					if(chr($i)==$_REQUEST['alp'])
						$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'zipcode/zipcodelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
						$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'zipcode/zipcodelist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'zipcode_model';
		$action = $_REQUEST['action'];
		if(!isset($page_link)) $page_link = '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/zipcode/view-zipcode.tpl');
	}
	
	function add()
	{
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'zipcode/add';
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/zipcode/zipcode.tpl');
		if($_POST)
		{
			$Data = $_POST['Data'];
			$id = $this->zipcode_model->save($Data);
			if($id)$var_msg = "zipcode is added successfully.";else $var_msg="Error-in add.";
			redirect(admin_url.'zipcode/zipcodelist?msg='.$var_msg);
			exit;
		}
	}
	
	function edit()
	{
		$iZipcodeId = $_REQUEST['iZipcodeId'];
		$admin_url = $this->config->item('admin_url');
		$data = $this->zipcode_model->get_one_by_id($iZipcodeId)->row();
	 	$action = $admin_url.'zipcode/edit';
		$this->smarty->assign('operation','edit');
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/zipcode/zipcode.tpl');
		if($_POST)
		{
			$Data = $_POST['Data'];
			$iZipcodeId = $_POST['iZipcodeId'];
			$id = $this->zipcode_model->update($iZipcodeId,$Data);
			if($id)$var_msg = "zipcode is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'zipcode/zipcodelist?msg='.$var_msg);
			exit;
		}
	}
	
	function delete()
	{

		$iZipcodeId = $_REQUEST['id'];		
		$data = $this->zipcode_model->get_one_by_id($iZipcodeId)->result();
		$var = $this->zipcode_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Zipcode deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'zipcode/zipcodelist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	
	function make_active($action,$iZipcodeId)
	{		
		$iZipcodeId = $this->zipcode_model->multiple_update_status($iZipcodeId,$action);
		$iZipcodeId  = @explode("','",$iZipcodeId);
		$cnt=count($iZipcodeId);
		if($iZipcodeId)
		{
			if($action == 'Active')
			{
				$var_msg = $cnt."  Administrator  activated successfully.";
			}
			else
			{
				$var_msg = $cnt."  Administrator  inactivated successfully.";
			}
		}
		else
		{
			if($action == 'Active')
			{
				$var_msg = "Eror-in active.";
			}
			else
			{
				$var_msg = "Eror-in inactive.";
			}
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'zipcode/zipcodelist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	
	function search_action()
	{
		$action = $_POST['action'];		
		$iZipcodeId = $_POST['commonId'];
		if($iZipcodeId == '')
		$iZipcodeId = $_POST['iZipcodeId'];
		if(is_array($iZipcodeId))
		{
			$iZipcodeId  = @implode("','",$iZipcodeId);
		}
		$iZipcodeId = $iZipcodeId;
		if($action == 'Active' || $action == 'Inactive')
		{
			$this->make_active($action,$iZipcodeId);
		}
		else if($action == 'Deletes')
		{
			$this->make_delete($iZipcodeId);
		}
	}
	
	function make_delete($iZipcodeId)
	{
		$data = array();
		$iZipcodeIdArray = explode("','",$iZipcodeId);
		for($i = 0; $i <count($iZipcodeIdArray); $i++)
		{
			$data[] = $this->zipcode_model->get_one_by_id($iZipcodeIdArray[$i])->result();
		}
		$iZipcodeId = $this->zipcode_model->delete_data($iZipcodeId);
		$iZipcodeId  = @explode("','",$iZipcodeId);
		$cnt=count($iZipcodeId);
		if($iZipcodeId)
		{
			$var_msg = $cnt." Record deleted successfully";
		}
		else
		{
			$var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'zipcode/zipcodelist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
}
/* End of file product.php */
/* Location: ./application/controllers/admin/product */
