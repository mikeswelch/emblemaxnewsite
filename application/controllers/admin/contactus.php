<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	var$limit = '';
    
	function __construct() {
		    
		parent::__construct();
		$this->load->library('session');
		
		
		$this -> load -> model('admin/contactus_model', '', TRUE);
		
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){ 
		redirect(admin_url.'authentication/login');
		exit ; 
		}
		$this->smarty->assign("filename",'contactus');    
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
		
	
	}
	/*      Function Name: index
		Developer Name: Deepak Khamari
		Purpose: to be called when the Contactus of admin page loads
		Created Date: 12-10-2012
	*/
	function index() {
		$this->contactuslist();
	}
	/*      Function Name: contactuslist
		Developer Name: Deepak Khamari
		Purpose: to be called when Show a listing.
		Created Date: 12-10-2012
	*/
	function contactuslist()
	{
		$limit1 = $this->contactus_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}

		$AlphaBox ='';
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
                
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){		    
		    $ssql.= " AND vName LIKE '".stripslashes($alp)."%'";
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if ($option != '' && $option=='eStatus' && $keyword != '')
		{
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		else if($option != '' && $keyword != ''){
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		
		if($_SESSION['module_name'] != 'contactus_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->contactus_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vEmail'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='estatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		}
		
		$data = $this->contactus_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		if($start == '0'){
			$start = 1;
		}
		
                $num_limit = ($start-1)*$limit;
    	        $startrec = $num_limit;
    	        $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="";
			}
	
                $db_alp = $this->contactus_model->displayalphasearch()->result();
		
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vName, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'contactus/contactuslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'contactus/contactuslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'contactus/contactuslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'contactus_model';
		
		$AlphaBox.='</ul>';
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/contactus/view-contactus.tpl');
	}
	/*
	 Function Name: add
	 Developer Name: Deepak Khamari
	 Purpose: to be called when  save data
	 Created Date: 12-10-2012
	 */
	function add()
	{
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("admin_url",$admin_url);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$upload_path = $this->config->item('upload_path');
	        $this->smarty->assign("upload_path",$upload_path);
		
		$action = site_url('contactus/add');
		$this->smarty->assign('operation','add');
	        $this->smarty->assign("action",$action);
	        
                $this->smarty->view('admin/contactus/contactus.tpl');
		
		
         if($_POST)
	     {
		$Data = $_POST['Data'];
		$dAddedDate = $Data['dAddedDate'];
		list($m, $d, $y) = explode('-',$dAddedDate);
		$Data['dAddedDate'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
		$id = $this->contactus_model->save($Data);		
		if($id)$var_msg = "Contact us is added successfully.";else $var_msg="Error-in add.";
		redirect(admin_url.'contactus/contactuslist?msg='.$var_msg);
		exit;
		}
	}
	/*
	 Function Name: edit
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data update
	 Created Date: 12-10-2012
	 */
        function edit()
	{
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		
		$id = $_REQUEST['iContactId'];
		
		
		$admin_url = $this->config->item('admin_url');
		$data = $this->contactus_model->get_one_by_id($id)->row();
		
		$date = $data->dAddedDate;
		$data->dAddedDate= date('d-M-Y',strtotime($date));
		
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'contactus/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/contactus/contactus.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$iContactId = $_POST['iContactId'];
			$dAddedDate = $Data['dAddedDate'];
			
			$Data['dAddedDate']= date('Y-m-d',strtotime($dAddedDate));
			
			$id = $this->contactus_model->update($iContactId,$Data);
			if($id)$var_msg = "Contact us is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'contactus/contactuslist?msg='.$var_msg);
			exit;
		}	
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-10-2012
	 */
	function delete()
	{
		$var = $this->contactus_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Contact us is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'contactus/contactuslist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: search_action
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the search status wise
	 Created Date: 12-10-2012
	 */
	function search_action(){
      
		$action = $_POST['action'];
		
		
		//echo $action; exit; 
		$iContactId = $_POST['commonId'];
		if($iContactId == '')
		$iContactId = $_POST['iContactId'];
			
		if(is_array($iContactId)){
		    $iContactId  = @implode("','",$iContactId);
		}
		$iContactId = $iContactId;
        
		if($action == 'Read' || $action == 'Unread'){
		    $this->make_read($action,$iContactId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iContactId);
		}else{
	    }
	}
	/*
	 Function Name: make_read
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the status updated active and deactive 
	 Created Date: 12-10-2012
	*/
	function make_read($action,$iContactId)
	{		
		$id = $this->contactus_model->multiple_update_status($iContactId,$action);
		
		$iContactId  = @explode("','",$iContactId);
		$cnt=count($iContactId);
		if($id){
		      if($action == 'Read'){
		          $var_msg = $cnt." Contact us make read successfully.";
		      }else{
		          $var_msg = $cnt."  Contact us  make unraed successfully.";
		      }
		}else{
		  if($action == 'Read'){
		          $var_msg = "Error-in read.";
		      }else{
		          $var_msg = "Error-in unread.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'contactus/contactuslist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: make_delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the multiple data delete
	 Created Date: 12-10-2012
	*/
	function make_delete($iContactId){
		$id = $this->contactus_model->delete_data($iContactId);
		
		$iContactId  = @explode("','",$iContactId);
		$cnt=count($iContactId);
		
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'contactus/contactuslist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
}
