<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/user_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'user');    
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Talents Rent Admin Panel");
		
	}
	
	function index() {
		$this->userlist();
	}
    
	function userlist()
	{
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';						
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
								
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vFirstName LIKE '".stripslashes($alp)."%' OR vFirstName LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
		    $ssql.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		if($option == 'iCountryId'){			
			$ssql = "AND $option = $keyword";
			if(isset($_REQUEST['keyword1']) !=''){
				$var_msg= $num_totrec." Record matched for ".$_REQUEST['keyword1'];
				$id_con = $_REQUEST['keyword'];
				$this->smarty->assign("id_con",$id_con);
				$keyword ='';
			}
		}
		if($_SESSION['module_name'] != 'user_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		
		$totrec = $this->user_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		include($site_path."system/libraries/paging.inc.php");
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vFirstName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vUserName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vEmail'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
					
		$data = $this->user_model->list_all($var_limit,$ssql,$field,$sort)->result();
		//echo "<pre>";
		//print_r($data);exit;
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->user_model->displayalphasearch()->result();
		//echo "<pre>";
		//print_r($db_alp);exit;
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vFirstName, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'user/userlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'user/userlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'user/userlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'user_model';
		$AlphaBox.='</ul>';
        if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("option",$option);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/user/view-user.tpl');
	}
	/*function do_upload($user_id)
	{
		//echo $user_id;exit;
		if(!is_dir('uploads/user/')){
			@mkdir('uploads/user/', 0777);
		}
		if(!is_dir('uploads/user/'.$user_id)){
			@mkdir('uploads/user/'.$user_id, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/user/'.$user_id,
		  'max_size'=>2000
		);
		$this->load->library('upload', $config);
		//echo "<pre>";
		//print_r($this->load);exit;
		$this->upload->do_upload('vphoto'); //do upload
		
		//want to create thumbnail
		 
		$image_data = $this->upload->data(); //get image data
		
		
		$config = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/user/'.$user_id.'/1_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => true,
		  'width' => 176,
		  'height' => 176
		);
	         //echo "<pre>";
		//print_r($config);exit;
		$this->load->library('image_lib', $config); //load library
		$this->image_lib->resize(); //do whatever specified in config
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}*/
	function do_upload($user_id)
	{
		if(!is_dir('uploads/user/')){
			@mkdir('uploads/user/', 0777);
		}
		if(!is_dir('uploads/user/'.$user_id)){
			@mkdir('uploads/user/'.$user_id, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/user/'.$user_id,
		  'max_size'=>2000
		);
		$this->load->library('upload', $config); 
		$this->upload->do_upload('vPhoto'); //do upload
		
		//want to create thumbnail
		$image_data = $this->upload->data(); //get image data
		 $config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/user/'.$user_id.'/200x200_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 200,
		  'height' => 200
		);
		$this->load->library('image_lib', $config1); //load library
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		 
		
		$img_uploaded = $image_data['file_name'	];
		return $img_uploaded;
	}
        
	function add()
	{
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$db_country = $this->user_model->list_country()->result();
		$this->smarty->assign("db_country",$db_country);
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'user/add';
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/user/user.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$Data['vPassword'] = md5($Data['vPassword']);			
			//$Data['dAddedDate'] =  date("Y-m-d", mktime(0, 0, 0, $Data['dAddedDate']));
			$Data['dAddedDate'] = date('Y-m-d H:i:s');
			//echo "<pre>";
			//print_r($Data['dAddedDate']);exit;
			$id = $this->user_model->save($Data);
			$img_uploaded = $this->do_upload($id);
			
			$Data['vphoto'] = $img_uploaded;
			$id = $this->user_model->update($id,$Data);
			if($id)$var_msg = "User is added successfully.";else $var_msg="Error-in add.";
			redirect(admin_url.'user/userlist?msg='.$var_msg);
			exit;
		}
	}
	
	function edit()
	{
                
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['iUserId'];
		//echo "<pre>";
		//print_r($id); exit;
		$admin_url = $this->config->item('admin_url');
		
		
		$data = $this->user_model->get_one_by_id($id)->row();
		
		//$data->dAddedDate = date("d-M-Y",strtotime($data->dAddedDate));
		
		if($data->dModifiedDate != '0000-00-00 00:00:00' && $data->dModifiedDate != '')
		$data->dModifiedDate = date('Y-m-d H:i:s');
		else
		$data->dModifiedDate = '';
		$db_country = $this->user_model->list_country()->result();
		$this->smarty->assign("db_country",$db_country);
		
		$db_state = $this->user_model->list_state_by_countryid($data->iCountryId)->result();
		$this->smarty->assign("db_state",$db_state);
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'user/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/user/user.tpl');
		
		if($_POST)
		{
			
			//echo "<pre>";
			//print_r($_POST['Data']);exit;
			$Data = $_POST['Data'];			
			$Data['dAddedDate'] =  date('Y-m-d H:i:s');
			if($Data['dModifiedDate'] != '')
			$Data['dModifiedDate'] = date('Y-m-d H:i:s');
			//echo "<pre>";
			//print_r($Data['dModifiedDate']);exit;
			$user_id = $_POST['iUserId'];
			$img_uploaded = $this->do_upload($user_id);
			if($img_uploaded)
				$Data['vphoto'] = $img_uploaded;
			
			$id = $this->user_model->update($user_id,$Data);
			if($id)$var_msg = "User is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'user/userlist?msg='.$var_msg);
			exit;
		}
	}
	
	function delete()
	{
		$userid = $_REQUEST['id'];
		$upload_path = $this->config->item('upload_image_path');
		rmdir($upload_path.'user/'.$userid);
		$data = $this->user_model->get_one_by_id($userid)->result();
		//echo"<pre>";print_r($data);echo $upload_path.'user/'.$userid.'/200x200_'.$data[0]->vPhoto;exit;
		unlink($upload_path.'user/'.$userid.'/200x200_'.$data[0]->vPhoto);
		unlink($upload_path.'user/'.$userid.'/'.$data[0]->vPhoto);
		$var = $this->user_model->delete($userid);
		if($var)$var_msg = "User is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'user/userlist?msg='.$var_msg);
		exit;
	}
	
	function make_active($action,$user_id)
	{
		
		//echo "<pre>";
		//print_r($user_id);exit;
		$id = $this->user_model->multiple_update_status($user_id,$action);
		//echo "<pre>";
		//print_r($user_id);exit;
		$useridcnt  = @explode("','",$user_id);
		$cnt=count($useridcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'user/userlist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
	function search_action(){
        
		$action = $_POST['action'];
		$user_id = $_POST['commonId'];
		
		if($user_id == '')
		$user_id = $_POST['iUserId'];
		 	
		if(is_array($user_id)){
		    $user_id  = @implode("','",$user_id);
		}
		$user_id = $user_id;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$user_id);
		}else if($action == 'Deletes'){
		    $this->make_delete($user_id);
		}else{
	    }
	}
	function make_delete($user_id){
		//echo "<pre>";
		//print_r("fsklgv;flk");exit;
		$data = array();
		$upload_path = $this->config->item('upload_image_path');
		$iDesignImageArray = explode("','",$user_id);
		for($i = 0; $i <count($iDesignImageArray); $i++){
			$data[] = $this->user_model->get_one_by_id($iDesignImageArray[$i])->result();
			$var = unlink($upload_path.'user/'.$iDesignImageArray[$i].'/'.$data[$i][0]->vPhoto);
			$var = unlink($upload_path.'user/'.$iDesignImageArray[$i].'/200x200_'.$data[$i][0]->vPhoto);
			rmdir($upload_path.'user/'.$iDesignImageArray[$i]);	
			if($var){
				$var = $this->user_model->delete_image($iDesignImageArray[$i]);
				}
		}
		$id = $this->user_model->delete_data($user_id);		
		$useridcnt  = @explode("','",$user_id);
		$cnt=count($useridcnt);	

		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'user/userlist?msg='.$var_msg);
		exit;
	}
	function deleteimage()
	{
		$upload_path = $this->config->item('upload_image_path');
		$userid = $_REQUEST['id'];
		$data = $this->user_model->get_one_by_id($userid)->result();
		rmdir($upload_path.'user/'.$userid);
		unlink($upload_path.'user/'.$userid.'/200x200_'.$data[0]->vPhoto);
		$var = unlink($upload_path.'user/'.$userid.'/'.$data[0]->vPhoto);
		
		if($var)
		{
			$var1 = $this->user_model->delete_image($userid);
			
		}
		if($var1		)
		{
			$var_msg = "Photo deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'user/edit?iUserId='.$userid.'&msg='.$var_msg);
		exit;
	}
		
	function export()
	{
	   $ssql=$_REQUEST['ssql'];
	   $ssql = base64_decode($ssql);
	   
	   $csv_output ='';
           header("Content-type: text/html; charset=utf-8");
           $rowcsv='';

	   $result = $this->user_model->exportData($ssql)->result(); 
           $count = count($result);
	    $admin_url = $this->config->item('admin_url');
	    $site_url = $this->config->item('site_url');
	  
	   
	$locationfilepath = "user.csv";
        $locationfilepathurl =  "admin/user.csv";
        $output = fopen($locationfilepath, 'w');
        $rowcsv.= 'First Name';
        $rowcsv.= "*".'Last Name';
	$rowcsv.= "*".'User Name';
	$rowcsv.= "*".'Email url';
	$rowcsv.= "*".'Image url';
	$rowcsv.= "*".'Contact Number';
	$rowcsv.= "*".'Country';
	$rowcsv.= "*".'State';
	$rowcsv.= "*".'Zipcode';
	$rowcsv.= "*".'Gender';
	$rowcsv.= "*".'Address';
	
	$rowcsv.= "*".'Status';
	
	
        $rowcsv.= "\n";
        
        fwrite($output, $rowcsv);
        
	for ($i=0;$i<$count;$i++) {
            
            $rowcsv.= $result[$i]->vFirstName;
            $rowcsv.= "*".$result[$i]->vLastName;
	    $rowcsv.= "*".$result[$i]->vUserName;
	    $rowcsv.= "*".$result[$i]->vEmail;
	    $rowcsv.= "*".$site_url."/uploads/user/".$result[$i]->iUserId."/".$result[$i]->vPhoto;
	    $rowcsv.= "*".$result[$i]->vPhone;
	    $rowcsv.= "*".$result[$i]->vCountry;
	    $rowcsv.= "*".$result[$i]->vState;
	    $rowcsv.= "*".$result[$i]->vZipCode;
	    $rowcsv.= "*".$result[$i]->eGender;
	    $rowcsv.= "*".$result[$i]->tAddress;	
	    $rowcsv.= "*".$result[$i]->eStatus;
            $rowcsv.= "\n";
        }
         fwrite($output, $rowcsv);
         fclose($output);
	   
	   
	   
	   
	      header("Content-type: application/vnd.ms-excel");
	      header("Content-disposition: csv" . date("Y-m-d") . ".csv");
	      header( "Content-disposition: filename=user.csv");
	      print $rowcsv; // il file e' pronto e puo' essere scaricato
	      exit;
	   
		
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
