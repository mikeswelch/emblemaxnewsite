<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{ 
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/banner_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$this->load->library('upload'); 
		$this->load->library('image_lib'); //load librar
		
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'banner');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Emblemax Admin Panel");
		
	}
	
	function index() { 
		$this->bannerlist();
	}
        
	function bannerlist()
	{
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
						
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (bg.vTitle LIKE '".stripslashes($alp)."%' OR bg.vTitle LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		if($option != '' && $keyword != ''){
                    
		    $ssql.= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		    //echo $ssql;exit;
                   
		}
		if($_SESSION['module_name'] != 'banner_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		
		$totrec = $this->banner_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
                
		include($site_path."system/libraries/paging.inc.php");
               
                
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='bg.vTitle'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vImage'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vUrl'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='iOrderNo'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='bgi.eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}	
		
		$data = $this->banner_model->list_all($var_limit,$ssql,$field,$sort)->result();
		//echo '<pre>';print_r($data);exit;
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->banner_model->displayalphasearch()->result();
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'banner/bannerlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'banner/bannerlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'banner/bannerlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
              
		
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'banner_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/banner/view-banner.tpl');
	}
        
        function add()
	{
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);

		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'banner/add';
		
		$bannergroup = $this->banner_model->getBannerGroup()->result_array();
		$this->smarty->assign("bannergroup",$bannergroup);
		$totalRec = $this->banner_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
	
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);

		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/banner/banner.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$Data['vImage'] = $_FILES['vImage']['name'];
			$id = $this->banner_model->save($Data);
			$size = $this->banner_model->getSize($Data['iBannerGroupId'])->result_array();
			$img_uploaded = $this->do_upload($id,$size[0]['iHeight'],$size[0]['iWidth']);	
			$Data['vImage'] = $img_uploaded;
			$id = $this->banner_model->update($id,$Data);
			if($id)$var_msg = "Banner is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'banner/bannerlist?msg='.$var_msg);
				
		}
	}
	
        function edit()
	{
                
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['iBannerId'];
		
		$admin_url = $this->config->item('admin_url');		
		$data = $this->banner_model->get_one_by_id($id)->row();		
		$bannergroup = $this->banner_model->getBannerGroup()->result_array();
		$this->smarty->assign("bannergroup",$bannergroup);

		$totalRec = $this->banner_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
	
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'banner/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/banner/banner.tpl');
		
		if($_POST)
		{
			
			$Data = $_POST['Data'];
			$iBannerId = $_POST['iBannerId'];
			$iBannerGroupId = $Data['iBannerGroupId'];
			
			$Data['vImage'] = $_FILES['vImage']['name'];
			
			if($Data['vImage']==''){$Data['vImage']=$data->vImage;}else{$Data['vImage']=$_FILES['vImage']['name'];}
			
			$size = $this->banner_model->getSize($iBannerGroupId)->result_array();
			$img_uploaded = $this->do_upload($iBannerId,$size[0]['iHeight'],$size[0]['iWidth']);
			
			if($img_uploaded)
			{
			$Data['vImage'] = $img_uploaded;
			}
			$id = $this->banner_model->update($iBannerId,$Data);
			
			
			
			if($id)$var_msg = "Banner is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'banner/bannerlist?msg='.$var_msg);
			exit;
		}
	}
        function delete()
	{
		$upload_path = $this->config->item('upload_image_path');
		$iBannerId = $_REQUEST['id'];
		$data = $this->banner_model->get_one_by_id($_REQUEST['id'])->result();
		
		$size = $this->banner_model->getSize($data[0]->iBannerGroupId)->result_array();
		$iWidth = $size[0]['iWidth'];
		$iHeight = $size[0]['iHeight'];

		$var1 = unlink($upload_path.'banner/'.$iBannerId.'/195x200_'.$data[0]->vImage);
		$var1 = unlink($upload_path.'banner/'.$iBannerId.'/'.$iWidth.'x'.$iHeight.'_'.$data[0]->vImage);
		$var1 = unlink($upload_path.'banner/'.$iBannerId.'/'.$data[0]->vImage);
		//echo "unlink($upload_path.'banner/'.$iBannerId.'/'.$data[0]->vImage)";
		//exit;
		rmdir($upload_path.'banner/'.$iBannerId);
		
		$var = $this->banner_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Banner is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'banner/bannerlist?msg='.$var_msg);
		exit;
	}
        
       function make_active($action,$iBannerId)
	{
		
		$id = $this->banner_model->multiple_update_status($iBannerId,$action);
		$coloridcnt  = @explode("','",$iBannerId);
		$cnt=count($coloridcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'banner/bannerlist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
	function do_upload($iBannerId,$iHeight,$iWidth)
	{
		if(!is_dir('uploads/banner/')){
			@mkdir('uploads/banner/', 0777);
		}
		if(!is_dir('uploads/banner/'.$iBannerId)){
			@mkdir('uploads/banner/'.$iBannerId, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/banner/'.$iBannerId,
		  'max_size'=>2000
		);
		
		$this->upload->initialize($config);
		$this->upload->do_upload('vImage'); //do upload
		$image_data = $this->upload->data(); //get image data
		
		//want to create thumbnail
              $config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  //'new_image' => 'uploads/bannergroupimage/'.$iBannerGroupImageId.'/'.$iWidth.'X'.$iHeight.'_'.$image_data['file_name'], //save as new image //need to create thumbs first
		   'new_image' => 'uploads/banner/'.$iBannerId.'/'.$iWidth.'x'.$iHeight.'_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => $iWidth,
		  'height' => $iHeight
		);
		
		$config2 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/banner/'.$iBannerId.'/195x200_'.$image_data['file_name'],
				         //banner/'.$iBannerId.'/195x200_'.$data[0]->vImage//save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 195,
		  'height' => 200
		);

		$this->image_lib->initialize($config1);
			$test1 = $this->image_lib->resize(); //do whatever specified in configg
		$this->image_lib->initialize($config2);
			$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		
		$image_data = $this->upload->data(); //get image data
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	
	
        function search_action(){
		
        
		$action = $_POST['action'];
		$iBannerId = $_POST['commonId'];
		
		if($iBannerId == '')
		$iBannerId = $_POST['iBannerId'];
		
		if(is_array($iBannerId)){
		    $iBannerId  = @implode("','",$iBannerId);
		}
		$iBannerId = $iBannerId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iBannerId);
		    
		}else if($action == 'Deletes'){
		    $this->make_delete($iBannerId);
		}else{
	    }
	    
	}
	
	function make_delete($iBannerId){

		$data = array();
		$size = array();
		$upload_path = $this->config->item('upload_image_path');
		$iDesignImageArray = explode("','",$iBannerId);
		for($i = 0; $i <count($iDesignImageArray); $i++){			
			$data[] = $this->banner_model->get_one_by_id($iDesignImageArray[$i])->result();
			$size[] = $this->banner_model->getSize($data[$i][0]->iBannerGroupId)->result_array();
			$iWidth = $size[$i][0]['iWidth'];
			$iHeight = $size[$i][0]['iHeight'];

			$var = unlink($upload_path.'banner/'.$iDesignImageArray[$i].'/195x200_'.$data[$i][0]->vImage);
			
			$var = unlink($upload_path.'banner/'.$iDesignImageArray[$i].'/'.$iWidth.'x'.$iHeight.'_'.$data[$i][0]->vImage);
			$var = unlink($upload_path.'banner/'.$iDesignImageArray[$i].'/'.$data[$i][0]->vImage);
			rmdir($upload_path.'banner/'.$iDesignImageArray[$i]);	
				if($var){
					$var = $this->banner_model->delete_image($iDesignImageArray[$i]);
				}
		}	
		$id = $this->banner_model->delete_data($iBannerId);		
		$coloridcnt  = @explode("','",$iBannerId);
		$cnt=count($coloridcnt);	
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'banner/bannerlist?msg='.$var_msg);
		exit;
	}
	
	function deleteimage()
	{
		$upload_path = $this->config->item('upload_image_path');
		$BannerId = $_REQUEST['id'];
		
		
		
		$data = $this->banner_model->get_one_by_id($BannerId)->result();
		
		$size = $this->banner_model->getSize($data[0]->iBannerGroupId)->result_array();
		$iWidth = $size[0]['iWidth'];
		$iHeight = $size[0]['iHeight'];
		$var1 = unlink($upload_path.'banner/'.$BannerId.'/195x200_'.$data[0]->vImage);
		
		$var1 = unlink($upload_path.'banner/'.$BannerId.'/'.$iWidth.x.$iHeight.'_'.$data[0]->vImage);
		$var1 = unlink($upload_path.'banner/'.$BannerId.'/'.$data[0]->vImage);
		rmdir($upload_path.'banner/'.$BannerId);
		
		if($var1)
		{
			$var = $this->banner_model->delete_image($BannerId);
		}
		if($var1)
		{
			$var_msg = "Photo deleted successfully";
		}else
		{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'banner/edit?iBannerId='.$BannerId.'&msg='.$var_msg);
		exit;
	}

}
    