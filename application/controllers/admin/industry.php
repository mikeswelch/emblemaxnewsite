<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Industry extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	var $limit = '';
    
	function __construct() {
		    
		parent::__construct();
		$this->load->library('session');
		
		
		$this -> load -> model('admin/industry_model', '', TRUE);
		
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){ 
		redirect(admin_url.'authentication/login');
		exit ; 
		}
				$filename = $this->config->item('filename');
		$this->smarty->assign("filename",$filename);    

		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Onwardz Admin Panel");
		
	
	}
	/*      Function Name: index
		Developer Name: Deepak Khamari
		Purpose: to be called when the Country of admin page loads
		Created Date: 12-07-2012
	*/
	function index() {
		$this->industrylist();
	}
	/*      Function Name: industrylist
		Developer Name: Deepak Khamari
		Purpose: to be called when Show a listing.
		Created Date: 12-07-2012
	*/
	function industrylist()
	{
		$limit1 = $this->industry_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}		
		$AlphaBox ='';
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
                
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql = " AND vIndustryName LIKE '".stripslashes($alp)."%'";
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if ($option != '' && $option=='i.eStatus' && $keyword != ''){
			
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
			
		}
		else if($option != '' && $keyword != ''){
			
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		}
		//echo $ssql; exit;
		if($_SESSION['module_name'] != 'industry_model' || $_REQUEST['action'] == 'Show All'){
			$_SESSION['ssql_action'] = '';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->industry_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		
		
		$data = $this->industry_model->list_all($var_limit,$ssql,$field,$sort)->result();
		#echo "<pre>";
		#print_r($data); exit;
		if($start == '0'){
			$start = 1;
		}
		
                $num_limit = ($start-1)*$limit;
    	        $startrec = $num_limit;
    	        $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="";
			}
	
                $db_alp = $this->industry_model->displayalphasearch()->result();
		
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vIndustryName, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
	
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'industry/industrylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'industry/industrylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'industry/industrylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		$AlphaBox.='</ul>';
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'industry_model';
		
		
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/industry/view-industry.tpl');
	}
	/*
	 Function Name: add
	 Developer Name: Deepak Khamari
	 Purpose: to be called when  save data
	 Created Date: 12-07-2012
	 */
	function add()
	{
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("admin_url",$admin_url);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$upload_path = $this->config->item('upload_path');
	        $this->smarty->assign("upload_path",$upload_path);
		
		$action = site_url('country/add');
		$this->smarty->assign('operation','add');
	        $this->smarty->assign("action",$action);
	        
                $this->smarty->view('admin/industry/industry.tpl');
		
		
         if($_POST)
	     {
		$Data = $_POST['Data'];
		$id = $this->industry_model->save($Data);		
		if($id)$var_msg = "Industry is added successfully.";else $var_msg="Error-in add.";
		redirect(admin_url.'industry/industrylist?msg='.$var_msg);
		exit;
		}
	}
	/*
	 Function Name: edit
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data update
	 Created Date: 12-07-2012
	 */
        function edit()
	{
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		
		$id = $_REQUEST['iIndustryId'];
		 
		$admin_url = $this->config->item('admin_url');
		$data = $this->industry_model->get_one_by_id($id)->row();
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'industry/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/industry/industry.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$iIndustryId = $_POST['iIndustryId'];
			$id = $this->industry_model->update($iIndustryId,$Data);
			if($id)$var_msg = "Industry is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'industry/industrylist?msg='.$var_msg);
			exit;
		}	
	}
	/*
	 Function Name: delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the data delete as one record at a time
	 Created Date: 12-07-2012
	 */
	function delete()
	{
		
		$var = $this->industry_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Industry is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'industry/industrylist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: search_action
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the search status wise
	 Created Date: 12-07-2012
	 */
	function search_action(){
      
		$action = $_POST['action'];
		$iIndustryId = $_POST['commonId'];
		if($iIndustryId == '')
		$iIndustryId = $_POST['iIndustryId'];
		
		if(is_array($iIndustryId)){
		    $iIndustryId  = @implode("','",$iIndustryId);
		}
		$iIndustryId = $iIndustryId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iIndustryId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iIndustryId);
		}else{
	    }
	}
	/*
	 Function Name: make_active
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the status updated active and deactive 
	 Created Date: 12-07-2012
	*/
	function make_active($action,$iIndustryId)
	{		
		$id = $this->industry_model->multiple_update_status($iIndustryId,$action);
		
		$iIndustryId  = @explode("','",$iIndustryId);
		$cnt=count($iIndustryId);
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Industry activated successfully.";
		      }else{
		          $var_msg = $cnt." Industry inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'industry/industrylist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	/*
	 Function Name: make_delete
	 Developer Name: Deepak Khamari
	 Purpose: to be called when the multiple data delete
	 Created Date: 12-07-2012
	*/
	function make_delete($iIndustryId){
		$id = $this->industry_model->delete_data($iIndustryId);
		
		$iIndustryId  = @explode("','",$iIndustryId);
		$cnt=count($iIndustryId);
		
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		      
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'industry/industrylist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	
	/*
	 Function Name: checkIndustry
	 Developer Name: Deepak Khamari
	 Purpose: to be called when to check duplicacy of industry name whe add or edit industry.
	 Created Date: 12-20-2012
	*/
	function checkIndustry()
	{
		
		
		$newIndustry = $_REQUEST['Data']['vIndustryName'];
		$iIndustryId = $_REQUEST['iIndustryId'];
		
		if(isset($_REQUEST['iIndustryId']) && $iIndustryId !='')
		{
			$iIndustryId = $_REQUEST['iIndustryId'];
			$oldData = $this->industry_model->getOldIndustry($iIndustryId)->result();
			$oldIndustry =  $oldData[0]->vIndustryName;
	   
			if($newIndustry == $oldIndustry){
				echo '0'; exit;
			}
			else{
				$data = $this->industry_model->check_industry($newIndustry)->result();
				$count = $data[0]->tot;
				
				if($count==0){
				echo "0";  exit;}
				else{ 
				echo "1";  exit;}	
			}
		}
		else{
			$data = $this->industry_model->check_industry($newIndustry)->result();
			$count = $data[0]->tot;
			
			if($count==0){
			echo "0";  exit;}
			else{ 
			echo "1";  exit;}
		}
	}
	
}
