<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Occupations extends CI_Controller{
        
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	var $limit = '';
    
	function __construct() {
		    
		parent::__construct();
		$this->load->library('session');
		$this -> load -> model('admin/occupations_model', '', TRUE);
		
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){ 
		redirect(admin_url.'authentication/login');
		exit ; 
		}
		$this->smarty->assign("filename",'occupations');    
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Emblemax Admin Panel");
		
	
	}

	function index() {
		$this->occupationslist();
	}

	function occupationslist()
	{
		
		$limit1 = $this->occupations_model->limit_fetch()->result();
		
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}

		$AlphaBox ='';
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
        
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
                
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql = " AND vTitle LIKE '".stripslashes($alp)."%'";		    
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if ($option != '' && $option=='eStatus' && $keyword != '')
		{
			$ssql = " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";		
		}
		else if($option != '' && $keyword != ''){			
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";			
		}
		
		if($_SESSION['module_name'] != 'occupations_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';			
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->occupations_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vTitle'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='iOrderNo'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->occupations_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		if($start == '0'){
			$start = 1;
		}
		
                $num_limit = ($start-1)*$limit;
    	        $startrec = $num_limit;
    	        $lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="";
			}
	
                $db_alp = $this->occupations_model->displayalphasearch()->result();
		
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'occupations/occupationslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'occupations/occupationslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'occupations/occupationslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'occupations_model';
		$AlphaBox.='</ul>';
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/occupations/view-occupations.tpl');
	}

	function add()
	{
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_url = $this->config->item('admin_url');
		$this->smarty->assign("admin_url",$admin_url);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		$upload_path = $this->config->item('upload_path');
	        $this->smarty->assign("upload_path",$upload_path);

		$totalRec = $this->occupations_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$initOrder =1;
		$this->smarty->assign("totalRec",$totalRec);	
		$this->smarty->assign("initOrder",$initOrder);
		
		$action = site_url('occupations/add');
		$this->smarty->assign('operation','add');
	        $this->smarty->assign("action",$action);
	        
                $this->smarty->view('admin/occupations/occupations.tpl');
		
		
         if($_POST)
	     {
		$Data = $_POST['Data'];
		$id = $this->occupations_model->save($Data);		
		if($id)$var_msg = "Occupation is added successfully.";else $var_msg="Error-in add.";
		redirect(admin_url.'occupations/occupationslist?msg='.$var_msg);
		exit;
		}
	}
	
        function edit()
	{
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		
		$id = $_REQUEST['iOccupationsId'];
		$totalRec = $this->occupations_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$initOrder =1;
		$this->smarty->assign("totalRec",$totalRec);	
		$this->smarty->assign("initOrder",$initOrder);
		
		$admin_url = $this->config->item('admin_url');
		$data = $this->occupations_model->get_one_by_id($id)->row();
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'occupations/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/occupations/occupations.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$Data['iOccupationsId'] = $_REQUEST['iOccupationsId'];
			
			$id = $this->occupations_model->update($Data['iOccupationsId'],$Data);
			if($id)$var_msg = "Occupation is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'occupations/occupationslist?msg='.$var_msg);
			exit;
		}	
	}

	function delete()
	{
		$var = $this->occupations_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Occupation is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'occupations/occupationslist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}

	function search_action(){
       
		$action = $_POST['action'];
		$faq_id = $_POST['commonId'];
		if($faq_id == '')
		$faq_id = $_POST['iOccupationsId'];
		if(is_array($faq_id)){
		    $faq_id  = @implode("','",$faq_id);
		}
		$faq_id = $faq_id;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$faq_id);
		}else if($action == 'Deletes'){
		    $this->make_delete($faq_id);
		}else{
	    }
	}
	
	function make_active($action,$faq_id)
	{
		$id = $this->occupations_model->multiple_update_status($faq_id,$action);
		
		$faq_id  = @explode("','",$faq_id);
		$cnt=count($faq_id);
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Occupation activated successfully.";
		      }else{
		          $var_msg =  $cnt." Occupation inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'occupations/occupationslist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}

	function make_delete($faq_id){
		$id = $this->occupations_model->delete_data($faq_id);
		
		$faq_id  = @explode("','",$faq_id);
		$cnt=count($faq_id);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		      
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'occupations/occupationslist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
}
