<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faqcategory extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/faqcategory_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$this->smarty->assign("filename",'faqcategory');    
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		$this->faqcategorylist();
	}
        function faqcategorylist()
	{
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
						
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vCategoryName LIKE '".stripslashes($alp)."%' OR vCategoryName LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
                    
		    //$ssql.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
		    $ssql.= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
                    //echo $ssql;exit;
		}
		if($_SESSION['module_name'] != 'faqcategory_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		$totrec = $this->faqcategory_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
                
		include($site_path."system/libraries/paging.inc.php");
               
                
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   if($field =='vCategoryName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}	
		
		$data = $this->faqcategory_model->list_all($var_limit,$ssql,$field,$sort)->result();
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->faqcategory_model->displayalphasearch()->result();
		//echo "<pre>";
		//print_r($db_alp);exit;
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vCategoryName, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faqcategory/faqcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faqcategory/faqcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'faqcategory/faqcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
                $this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'faqcategory_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/faqcategory/view_faqcategory.tpl');
	}
        
        function add()
	{
			
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'faqcategory/add';
		
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/faqcategory/faqcategory.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$id = $this->faqcategory_model->save($Data);						
			if($id)$var_msg = "faq category is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'faqcategory/faqcategorylist?msg='.$var_msg);
				exit;
		}
	}
	
        function edit()
	{
                
		$id = $_REQUEST['iFaqcategoryid'];
		$admin_url = $this->config->item('admin_url');		
		$data = $this->faqcategory_model->get_one_by_id($id)->row();
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'faqcategory/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/faqcategory/faqcategory.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$iFaqcategoryid = $_POST['iFaqcategoryid'];
			
			$id = $this->faqcategory_model->update($iFaqcategoryid,$Data);
			if($id)$var_msg = "faq category is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'faqcategory/faqcategorylist?msg='.$var_msg);
			exit;
		}
	}
        
        
	function delete()
	{
		$var = $this->faqcategory_model->delete($_REQUEST['id']);
		if($var)$var_msg = "faq category_model is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		//$var = $this->user_model->delete($id);
		//if($var)$var_msg = "User is deleted successfully.";else $var_msg="Error-in delete.";
		redirect(admin_url.'faqcategory/faqcategorylist?msg='.$var_msg);
		exit;
	}
        
       function make_active($action,$iFaqcategoryid)
	{
		
		$id = $this->faqcategory_model->multiple_update_status($iFaqcategoryid,$action);
		$sizeidcnt  = @explode("','",$iFaqcategoryid);
		$cnt=count($sizeidcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'faqcategory/faqcategorylist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
        function search_action(){
		$action = $_POST['action'];
		$iFaqcategoryid = $_POST['commonId'];
		
		if($iFaqcategoryid == '')
		$iFaqcategoryid = $_POST['iFaqcategoryid'];
		 	
		if(is_array($iFaqcategoryid)){
		    $iFaqcategoryid  = @implode("','",$iFaqcategoryid);
		}
		$iFaqcategoryid = $iFaqcategoryid;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iFaqcategoryid);
		}else if($action == 'Deletes'){
		    $this->make_delete($iFaqcategoryid);
		}else{
	    }
	}
	
	function make_delete($iFaqcategoryid){
		//echo "<pre>";
		//print_r("fsklgv;flk");exit;
		$id = $this->faqcategory_model->delete_data($iFaqcategoryid);		
		$sizeidcnt  = @explode("','",$iFaqcategoryid);
		$cnt=count($sizeidcnt);	
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'faqcategory/faqcategorylist?msg='.$var_msg);
		exit;
	} 
}
    