<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Color extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/color_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'color');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		$this->colorlist();
	}
        
	function colorlist()
	{
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
						
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vColor LIKE '".stripslashes($alp)."%' OR vColor LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
                    
		    $ssql.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
                    //echo $ssql;exit;
		}
		if($_SESSION['module_name'] != 'color_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		$totrec = $this->color_model->count_all($ssql)->result();
  		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
                
		include($site_path."system/libraries/paging.inc.php");
               
                
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vColor'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vColorCode'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->color_model->list_all($var_limit,$ssql,$field,$sort)->result();
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->color_model->displayalphasearch()->result();
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vColor, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'color/colorlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'color/colorlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'color/colorlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
                $this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'color_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/color/view-color.tpl');
	}
        
    function add()
	{
		
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'color/add';
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/color/color.tpl');
		
		if($_POST)
		{//echo "<pre>";print_r($_FILES);print_r($_POST);exit;
			$Data = $_POST['Data'];
			$id = $this->color_model->save($Data);
			$img_uploaded = $this->do_upload($id,$Data);	
			if($img_uploaded == ''){
				$Data['vImage']=ltrim($Data['vColorCode'],'#').".jpg";		
			}else{
				$Data['vImage'] = $img_uploaded;
			}
			$id = $this->color_model->update($id,$Data);
			if($id)$var_msg = "Color is added successfully.";else $var_msg="Error-in add.";
			redirect(admin_url.'color/colorlist?msg='.$var_msg);
				
		}
	}
	
    function edit()
	{
                
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['iColorId'];
		$admin_url = $this->config->item('admin_url');		
		$data = $this->color_model->get_one_by_id($id)->row();
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'color/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/color/color.tpl');
		if($_POST)
		{
			#echo "<pre>";print_r($_POST['Data']);exit;
			$Data = $_POST['Data'];
			$iColorId = $_POST['iColorId'];
			$color_id = $_POST['iColorId'];
			if(isset($_FILES) && $_FILES['vImage']['name'] != ""){
                  $Data['vImage']=$_FILES['vImage']['name'];
                  $img_uploaded = $this->do_upload($color_id,$Data);
		          $Data['vImage'] = $img_uploaded;
			}
			elseif(isset($_POST['oldimage']) && $_POST['oldimage'] == ""){
				$img_uploaded = $this->do_upload($color_id,$Data);
				$Data['vImage'] = $img_uploaded;
				if($img_uploaded == ''){
					$Data['vImage']=ltrim($Data['vColorCode'],'#').".jpg";
				}
			}
			$id = $this->color_model->update($iColorId,$Data);
			if($id)$var_msg = "Color is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'color/colorlist?msg='.$var_msg);
			exit;
		}
	}
    
	function delete()
	{
		$var = $this->color_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Color is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		//$var = $this->user_model->delete($id);
		//if($var)$var_msg = "User is deleted successfully.";else $var_msg="Error-in delete.";
		redirect(admin_url.'color/colorlist?msg='.$var_msg);
		exit;
	}
        
    function make_active($action,$iColorId)
	{
		
		//echo "<pre>";
		//print_r($user_id);exit;
		$id = $this->color_model->multiple_update_status($iColorId,$action);
		//echo "<pre>";
		//print_r($user_id);exit;
		$coloridcnt  = @explode("','",$iColorId);
		$cnt=count($coloridcnt);
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'color/colorlist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
	function do_upload($color_id,$Data)
	{
		if(!is_dir('uploads/color/')){
			@mkdir('uploads/color/', 0777);
		}
		if(!is_dir('uploads/color/'.$color_id)){
			@mkdir('uploads/color/'.$color_id, 0777);
		}
		  $config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/color/'.$color_id,
		  'max_size'=>2000
		);
		$hex=$Data['vColorCode'];
		$rgb = $this->hex2rgb($hex);
		$pic =@imagecreate(50, 50);
		$background_color = imagecolorallocate($pic, $rgb[0],$rgb[1], $rgb[2]);
		$path=$config['upload_path'];
		Imagejpeg($pic,$path."/".ltrim($hex,'#').".jpg");
		ImageDestroy($pic);
		//return $hex.".jpg";
		$this->load->library('upload', $config); 
		$this->upload->do_upload('vImage'); //do upload
		//want to create thumbnail
		$image_data = $this->upload->data(); //get image data
		$img_uploaded = $image_data['file_name'	];
		return $img_uploaded;
	}
	
	
    function search_action(){
        $action = $_POST['action'];
		$iColorId = $_POST['commonId'];
		if($iColorId == '')
		$iColorId = $_POST['iColorId'];
		if(is_array($iColorId)){
		    $iColorId  = @implode("','",$iColorId);
		}
		$iColorId = $iColorId;
        if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iColorId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iColorId);
		}else{
	    }
	}
	
	function make_delete($iColorId){
		//echo "<pre>";
		//print_r("fsklgv;flk");exit;
		$id = $this->color_model->delete_data($iColorId);		
		$coloridcnt  = @explode("','",$iColorId);
		$cnt=count($coloridcnt);	
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'color/colorlist?msg='.$var_msg);
		exit;
	}
	
	function deleteimage()
	{
		$upload_path = $this->config->item('upload_image_path');
		$colorid = $_REQUEST['id'];
		$data = $this->color_model->get_one_by_id($colorid)->result();
				
		
		$var = unlink($upload_path.'color/'.$colorid.'/'.$data[0]->vImage);
		//$var = unlink($upload_path.'user/'.$userid.'/1_'.$data[0]->vphoto);
		
		
		if($var)
		{
			$var = $this->color_model->delete_image($colorid);
			
		}
		if($var)
		{
			$var_msg = "Photo deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'color/edit?iColorId='.$colorid.'&msg='.$var_msg);
		exit;
	}
	function hex2rgb($hex) {
        		$hex = str_replace("#", "", $hex);
        	if(strlen($hex) == 3) {
        		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
         		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
         		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
        	} else {
           		$r = hexdec(substr($hex,0,2));
           		$g = hexdec(substr($hex,2,2));
        		$b = hexdec(substr($hex,4,2));
        	}
        	$rgb = array($r, $g, $b);
        	return $rgb;
    	}

}
    
