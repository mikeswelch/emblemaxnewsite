<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/product_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$this->load->library('upload'); 
		$this->load->library('image_lib'); //load library		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'product');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		$sec ="main";
		if  (isset($_REQUEST['sec'])){
			$sec = $_REQUEST['sec'];
		}
		$this->smarty->assign("sec","$sec");
		
	}
	
	function index() {
		$this->productlist();
	}
    
	function productlist()
	{
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';						
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
								
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= "WHERE(pt.vProductName LIKE '".stripslashes($alp)."%' OR pt.vProductName LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option'];
		
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
	
		if($_REQUEST['option'] != '' && $keyword != '' && $_REQUEST['option'] == 'p.eStatus'){
		    $ssql.= "WHERE ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		  
		}if($_REQUEST['option'] != '' && $keyword != '' && $_REQUEST['option'] == 'pt.vProductName')
		{
			$ssql.= "WHERE ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		
		if($_REQUEST['option'] != '' && $keyword != '' && $_REQUEST['option'] == 'p.fPrice')
		{
			$ssql.= "WHERE ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
		}
		
		if($_SESSION['module_name'] != 'product_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
	//echo $ssql;exit;
		$totrec = $this->product_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;
		
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		include($site_path."system/libraries/paging.inc.php");
		
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='pt.vProductName'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='p.eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
				
		$data = $this->product_model->list_all($var_limit,$ssql,$field,$sort)->result();
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->product_model->displayalphasearch()->result();
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vProductName, 0,1));
                    
		}
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'product/productlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'product/productlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'product/productlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'product_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("option",$option);
		
		$db_producttype = $this->product_model->list_producttype()->result();
		$this->smarty->assign("db_producttype",$db_producttype);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/product/view-product.tpl');
		
	}
	
	function do_upload($iProductId,$vImageName)
	{
		if(!is_dir('uploads/product/')){
			@mkdir('uploads/product/', 0777);
		}
		if(!is_dir('uploads/product/'.$iProductId)){
			@mkdir('uploads/product/'.$iProductId, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/product/'.$iProductId,
		  'max_size'=>2000
		);
		$this->upload->initialize($config);
		$this->upload->do_upload($vImageName); //do upload
		
		//want to create thumbnail
		$image_data = $this->upload->data(); //get image data
                $config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/product/'.$iProductId.'/193X198_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 193,
		  'height' => 198
		);
		$this->image_lib->initialize($config1);
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	
	function do_uploadcolorimage($iProductId,$iColorId,$vImageName)
	{
		if(!is_dir('uploads/product/')){
			@mkdir('uploads/product/', 0777);
		}
		if(!is_dir('uploads/product/'.$iProductId)){
			@mkdir('uploads/product/'.$iProductId, 0777);
		}
		if(!is_dir('uploads/product/')){
			@mkdir('uploads/product/', 0777);
		}
		if(!is_dir('uploads/product/'.$iProductId.'/'.$iColorId)){
			@mkdir('uploads/product/'.$iProductId.'/'.$iColorId, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/product/'.$iProductId.'/'.$iColorId,
		  'max_size'=>2000
		);

		$this->upload->initialize($config);
		$this->upload->do_upload($vImageName); //do upload
		
		$image_data = $this->upload->data(); //get image data
		//want to create thumbnail
		$config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/product/'.$iProductId.'/'.$iColorId.'/200X200_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 200,
		  'height' => 200
		);
		$this->image_lib->initialize($config1);
		$test1 = $this->image_lib->resize(); //do whatever specified in config
                
		$config2 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/product/'.$iProductId.'/'.$iColorId.'/341X418_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 341,
		  'height' => 418
		);
		$this->image_lib->initialize($config2);
		$test2 = $this->image_lib->resize(); //do whatever specified in config
		
		$img_uploaded = $image_data['file_name'];
		unset($config1);
		unset($config2);
		return $img_uploaded;
	}
        
	function add(){
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);

		$db_producttype = $this->product_model->getParentCatNew('0','','0','1','0');
		$this->smarty->assign("db_producttype",$db_producttype);
		
		$fabrics = $this->product_model->getFabrics()->result_array();
		$this->smarty->assign("fabrics",$fabrics);

		$totalRec = $this->product_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		
		$brand = $this->product_model->getBrands()->result_array();
		$this->smarty->assign("brand",$brand);

		$occupations = $this->product_model->getOccupations()->result_array();
		$this->smarty->assign("occupations",$occupations);
		
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'product/add';
		
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		//$this->smarty->assign("sec",$sec);
		
		$this->smarty->view('admin/product/products.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$data = $_POST['data'];
			$Data['vUrlText']= $this->seo_url($data['vProductName']);
			$iFabricsId = $_POST['iFabricsId'];
			$iBrandId = $_POST['iBrandId'];
			$iOccupationsId = $_POST['iOccupationsId'];
			
			$Data['iFabricsId'] = implode('|', $iFabricsId);
			$Data['iBrandId'] = implode('|', $iBrandId);
			$Data['iOccupationsId'] = implode('|', $iOccupationsId);
			$Data['vImage'] = $_FILES['vImage']['name'];
			$iPriductId = $this->product_model->save($Data);
			$img_uploaded = $this->do_upload($iPriductId,'vImage');			
			$Data['vImage'] = $img_uploaded;
			$data['iProductId'] = $iPriductId;
			$id = $this->product_model->saveTransdata($data);
			//$id = $this->product_model->update($iPriductId,$Data);
			if($id)$var_msg = "Product template is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'product/productlist?msg='.$var_msg);
				exit;
		}
	}
	
	function edit(){
		//echo "<pre>";print_r($_REQUEST);exit;
		if($_REQUEST['type'] == 'sizecolordetail'){
			$iProductId = $_REQUEST['iProductId'];
			$iQuantityRangeId = $_REQUEST['iQuantityRangeId'];
			$deleteData = $this->product_model->deleteDecorationData($iProductId,$iQuantityRangeId);
			$deleteColorData = $this->product_model->deleteColorData($iProductId);
			
			$decoration = $_POST['Decoration'];
			$colordata = $_POST['Data'];
			/*if($_REQUEST['CopyDecoration'] == 'yes'){
				$copy_product_id = $_REQUEST['copy_pro_temp'];
				$count = $this->product_model->getDecorationData($copy_product_id)->num_rows;
				$ceditDecoration = $this->product_model->getDecorationData($copy_product_id)->result_array();
				if($count > 0){
					foreach ($ceditDecoration as $cval){
						unset($cval[iProductPrintlocationNumbercolorId]);
						$cval[iProductId] = $iProductId;
						$id= $this->product_model->savePrintDecorationData($cval);
					}
				}
			}else{*/
				//echo "<pre>";print_r($_REQUEST);echo $id;exit;
				$iQuantityRangeId = $_REQUEST['iQuantityRangeId'];
				foreach($decoration as $key => $decorationTmp){
					
					$numberColorId = $key;
					foreach($decorationTmp as $key2=>$numColorPrice){
						$printLocationId = $key2;
						if(isset($numColorPrice) && $numColorPrice != ''){
							$data = array();
							$data['iProductId'] = $iProductId;
							$data['iPrintLocationId'] = $printLocationId;
							$data['iNumberColorsId'] = $numberColorId;
							$data['fPrice'] = $numColorPrice;
							$data['iQuantityRangeId'] = $iQuantityRangeId;
							$id= $this->product_model->savePrintDecorationData($data);
						}
					}
				}
				
				/* for saving data into product_printlocation_numbercolors table  ::::::Jyotiranjan::::: */
				
				$data['iPrintLocationId'] = $_POST['iPrintLocationId'];
				$data['embroidery'] = $_POST['embroidery'];
				for($i=0;$i<count($data['iPrintLocationId']);$i++){
					$Data['iProductId'] = $_REQUEST['iProductId'];
					$Data['iQuantityRangeId'] = $_REQUEST['iQuantityRangeId'];
					$Data['fPrice'] = $data['embroidery'][$i];
					$Data['iPrintLocationId'] = $data['iPrintLocationId'][$i];
					//echo "<pre>";print_r($Data);
					$embroideryId = $this->product_model->savePrintDecorationData($Data);
				}
				
			//}
			$pid = $_REQUEST['iProductId'];
			$id = $this->color_detail();
			if($id)$var_msg = "Product template is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'product/edit?iProductId='.$pid.'&sec=qd&msg='.$var_msg);
			exit;
		}else{
			$db_producttype = $this->product_model->getParentCatNew('0','','0','1','0');
			$this->smarty->assign("db_producttype",$db_producttype);
			$upload_path = $this->config->item('upload_path');
			$id = $_REQUEST['iProductId'];
			if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
			}else{
				$var_msg = '';
			}
			$color = $this->product_model->all_uniq_color($id)->result();
			$all_color = $this->product_model->all_color()->result();
			
			//echo "<pre>";print_r($all_color);exit;
			$all_unique_colorsize = $this->product_model->all_unique_colorsize_deatil($id)->result_array();
			$final_all_colorids = array();
			if(is_array($all_unique_colorsize)){
				foreach ($all_unique_colorsize as $key => $val){
					$final_all_colorids[$key] = $val['iColorId'];
				}
			}			
			$all_size_details = $this -> product_model->all_size()->result_array();
	
			$data_color = $this->product_model->all_color_product($id)->result();
	
			$fabrics = $this->product_model->getFabrics()->result_array();
			$this->smarty->assign("fabrics",$fabrics);
	
			$totalRec = $this->product_model->count_all($ssql)->result();
			$totalRec = $totalRec[0]->tot;
			$this->smarty->assign("totalRec",$totalRec);
			$initOrder =1;
			$this->smarty->assign("initOrder",$initOrder);
			
			$brand = $this->product_model->getBrands()->result_array();
			$this->smarty->assign("brand",$brand);
	
			$occupations = $this->product_model->getOccupations()->result_array();
			$this->smarty->assign("occupations",$occupations);
			
			$Data = $this->product_model->get_data_by_id($id,$lang)->row();
			$admin_url = $this->config->item('admin_url');
			$data = $this->product_model->get_one_by_id($id)->row();

			$iFabricsId = explode("|",$data->iFabricsId);
			$iBrandId = explode("|",$data->iBrandId);
			$iOccupationsId = explode("|",$data->iOccupationsId);
			
			$catid = $data->iCategoryId;
			$sizelist = $this->product_model->get_sizeBycatId($catid)->result();
			for($i=0;$i<count($sizelist);$i++){
				$size = $sizelist[$i]->iSizeId;
				$variable = $this->product_model->get_width_lenght($size,$id,$catid)->result();
				if(isset($variable[0]->iWidth))
				$sizelist[$i]->iWidth = $variable[0]->iWidth;
				else
				$sizelist[$i]->iWidth = 0;
				if(isset($variable[0]->iLength))
				$sizelist[$i]->iLength = $variable[0]->iLength;
				else
				$sizelist[$i]->iLength = 0;
			}
	          
			$isActive = $_REQUEST['sec'];
	          $this->smarty->assign('isActive',$isActive);
			$this->smarty->assign("iFabricsId",$iFabricsId);
			$this->smarty->assign("iBrandId",$iBrandId);
			$this->smarty->assign("iOccupationsId",$iOccupationsId);
			
			$this->smarty->assign("data_color",$data_color);
			$this->smarty->assign("color",$color);
			$this->smarty->assign("all_color",$all_color);
			
			$this->smarty->assign("all_unique_colorsize",$all_unique_colorsize);
			$this->smarty->assign("all_size_details",$all_size_details);
			$this->smarty->assign("final_all_colorids",$final_all_colorids);
			$this->smarty->assign("var_msg",$var_msg);
			$this->smarty->assign("Data",$Data);
			$this->smarty->assign('operation','edit');
			$this->smarty->assign('sizelist',$sizelist);
			$action = $admin_url.'product/edit';
			$this->smarty->assign("action",$action);
			$this->smarty->assign("data",$data);
			$this->smarty->assign("upload_path",$upload_path);
			$this->smarty->view('admin/product/products.tpl');
			
			if($_POST){
				$iProductColorId = $_POST['iProductColorId'];
				if($_POST['type'] == 'color')
				{	
					if($_POST['mode'] == 'add')
					{
						$lang = $_POST['lang'];
						$iProductId = $_POST['iProductId'];
						$Datacolor = $_POST['Datacolor'];
						$Datacolor['iProductId'] = $iProductId;
						$Id = $this->product_model->saveColorData($Datacolor);
						$img_uploaded = $this->do_uploadcolorimage($iProductId,$Id,'vFrontImage');			
						$Datacolor['vFrontImage'] = $img_uploaded;
						$img_uploaded1 = $this->do_uploadcolorimage($iProductId,$Id,'vBackImage');			
						$Datacolor['vBackImage'] = $img_uploaded1;
						$img_uploaded2 = $this->do_uploadcolorimage($iProductId,$Id,'vLeftSleeveImage');			
						$Datacolor['vLeftSleeveImage'] = $img_uploaded2;
						$img_uploaded3 = $this->do_uploadcolorimage($iProductId,$Id,'vRightSleeveImage');			
						$Datacolor['vRightSleeveImage'] = $img_uploaded3;					
						$id = $this->product_model->updatecolordata($Id,$Datacolor);
						if($id)$var_msg = "Color Image  is add successfully.";else $var_msg="Error-in edit.";
						redirect(admin_url.'product/edit?iProductId='.$iProductId.'&lang='.$lang.'&msg='.$var_msg.'#colorformadd');
					}else{
						//echo "<pre>";print_r($_POST);exit;
						
						$iProductColorId = $_POST['iProductColorId'];
						$iProductId = $_POST['iProductId'];
						$Datacolor['iColorId'] = $_POST['Datacolor']['iColorId'];
						if($_FILES['vFrontImage']['name'] != '')
						{
							$img_uploaded = $this->do_uploadcolorimage($iProductId,$iProductColorId,'vFrontImage');
							$Datacolor['vFrontImage'] = $img_uploaded;
						}
						
						if($_FILES['vBackImage']['name'] != ''){
							$img_uploaded1 = $this->do_uploadcolorimage($iProductId,$iProductColorId,'vBackImage');							       $Datacolor['vBackImage'] = $img_uploaded1;
						}

						if($_FILES['vLeftSleeveImage']['name'] != ''){
						        $img_uploaded1 = $this->do_uploadcolorimage($iProductId,$iProductColorId,'vLeftSleeveImage');						 	     $Datacolor['vLeftSleeveImage'] = $img_uploaded1;
						}

						if($_FILES['vRightSleeveImage']['name'] != ''){
							$img_uploaded1 = $this->do_uploadcolorimage($iProductId,$iProductColorId,'vRightSleeveImage');
							$Datacolor['vRightSleeveImage'] = $img_uploaded1;
						}
						

						$id = $this->product_model->updatecolordata($iProductColorId,$Datacolor);
						if($id)$var_msg = "Color Images Updated successfully.";else $var_msg="Error-in delete.";
						redirect(admin_url.'product/edit?iProductId='.$iProductId.'&sec=ds'.$lang.'&msg='.$var_msg.'#colorformadd');
						}
					
				}else{
					//echo "<pre>";print_r($_POST);exit;
					$Data = $_POST['Data'];
					$data = $_POST['data'];
					$Data['vUrlText']= $this->seo_url($data['vProductName']);
					$iFabricsId = $_POST['iFabricsId'];
					$Data['iFabricsId'] = implode('|', $iFabricsId);					
					$iBrandId = $_POST['iBrandId'];
					$Data['iBrandId'] = implode('|', $iBrandId);
					$iOccupationId = $_POST['iOccupationsId'];
					$Data['iOccupationsId'] = implode('|', $iOccupationId);
					
					if($Data['ePromotion'] == ''){
						$Data['ePromotion'] = 'No';	
					}				
					$iProductId = $_POST['iProductId'];
					$data['iProductId'] = $iProductId;
					
					if($_FILES['vImage']['name'] !=''){
						$img_uploaded = $this->do_upload($iProductId,'vImage');
					if($img_uploaded)
						$Data['vImage'] = $img_uploaded;			
					}
					$id = $this->product_model->update($iProductId,$Data);
					
					$check_lang = $this->product_model->get_data_by_id($iProductId,$lang)->result();
					if(isset($check_lang[0]->iProductTranslationId) AND $check_lang[0]->iProductTranslationId != '')
					{
						$id = $this->product_model->updateDataCatTran($check_lang[0]->iProductTranslationId,$data);
					}else{
						$data['iProductId'] = $iProductId;
						$Id = $this->product_model->saveData($data);
					}
					
					if(isset($_POST['SIZE']))
					{
						
						$deleteid = $this->product_model->deletesizetemplate($iProductId);
						$size = $_POST['SIZE'];
						for($i=0;$i<count($size);$i++)
						{
							$sizedata['iSizeId'] = $size[$i]['iSizeId'];
							$sizedata['iWidth'] = $size[$i]['iWidth'];
							$sizedata['iLength'] = $size[$i]['iLength'];
							$sizedata['iCategoryId'] = $Data['iCategoryId'];
							$sizedata['iProductId'] = $iProductId;
							$sizeid = $this->product_model->saveSize($sizedata);
						}	
					}else{
						$deleteid = $this->product_model->deletesize($iProductId);
					}
					if($id)$var_msg = "Product template is edited successfully.";else $var_msg="Error-in edit.";
					redirect(admin_url.'product/productlist?msg='.$var_msg);
					exit;		
				}
			}
		}
	}
	
	function delete(){
		$data = $this->product_model->get_one_by_id($_REQUEST['id'])->result();
		$upload_path = $this->config->item('upload_image_path');
          unlink($upload_path.'product/'.$data[0]->iProductId.'/'.$data[0]->vImage);
	     unlink($upload_path.'product/'.$data[0]->iProductId.'/1_'.$data[0]->vImage);
		rmdir($upload_path.'product/'.$data[0]->iProductId);		
	     $var = $this->product_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Product template is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];		
		redirect(admin_url.'product/productlist?msg='.$var_msg);
		exit;
	}
	function deletecolor()
	{
		
		$id = $_REQUEST['id'];
		$lang = $_REQUEST['lang'];
		$iProductId = $_REQUEST['iProductId'];
		$var = $this->product_model->deletecolorproduct($_REQUEST['id']);
		if($var)$var_msg = "Color Images deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];		
		redirect(admin_url.'product/edit?iProductId='.$iProductId.'&lang='.$lang.'&msg='.$var_msg.'#colorformadd');
		exit;
	}
	function make_active($action,$iProductId)
	{
		
		$id = $this->product_model->multiple_update_status($iProductId,$action);
		$Productidcnt  = @explode("','",$iProductId);
		$cnt=count($Productidcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'product/productlist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
	function search_action(){
        
		$action = $_POST['action'];
		$iProductId = $_POST['commonId'];
		
		if($iProductId == '')
		$iProductId = $_POST['iProductId'];
		 	
		if(is_array($iProductId)){
		    $iProductId  = @implode("','",$iProductId);
		}
		$iProductId = $iProductId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iProductId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iProductId);
		}else{
	    }
	}
	function make_delete($iProductId){
		$data = array();
		$upload_path = $this->config->item('upload_image_path');
		$iDesignImageArray = explode("','",$iProductId);
		for($i = 0; $i <count($iDesignImageArray); $i++){
			$data[] = $this->product_model->get_one_by_id($iDesignImageArray[$i])->result();
			$var = unlink($upload_path.'product/'.$iDesignImageArray[$i].'/'.$data[$i][0]->vImage);
			$var = unlink($upload_path.'product/'.$iDesignImageArray[$i].'/1_'.$data[$i][0]->vImage);
			rmdir($upload_path.'product/'.$iDesignImageArray[$i]);	
			if($var){
				$var = $this->product_model->delete_image($iDesignImageArray[$i]);
			}
		}
		$id = $this->product_model->delete_data($iProductId);		
		$Productidcnt  = @explode("','",$iProductId);
		$cnt=count($Productidcnt);	
		if($id){
		     $var_msg = $cnt." Record deleted successfully";
		}else{
			$var_msg = "Error-in delete";
		}
		redirect(admin_url.'product/productlist?msg='.$var_msg);
		exit;
	}
	function deleteimage(){
		$upload_path = $this->config->item('upload_image_path');
		$iProductId = $_REQUEST['iProductId'];
		$data = $this->product_model->get_one_by_id($iProductId)->result();
		unlink($upload_path.'product/'.$iProductId.'/'.$data[0]->vImage);
		$var = unlink($upload_path.'product/'.$iProductId.'/1_'.$data[0]->vImage);
		
		if($var)
		{
			$var1 = $this->product_model->delete_image($iProductId);
			
		}
		if($var1)
		{
			$var_msg = "Image deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'product/edit?iProductId='.$iProductId.'&msg='.$var_msg);
		exit;
	}
	
	function makesize(){
		$id = $_REQUEST['iCategoryId'];
		$sizelist = $this->product_model->get_sizeBycatId($id)->result();	
		#echo "<pre>";
		#print_r($sizelist);exit;
		$this->smarty->assign("sizelist",$sizelist);
		$this->smarty->view('admin/product/sizecat.tpl');
	}
	function makesizedefault(){
		$id = $_REQUEST['iCategoryId'];
		$iproductid = $_REQUEST['iProductId'];
		$sizelist = $this->product_model->get_sizeBycatId($id)->result();
		for($i=0;$i<count($sizelist);$i++){
			$size = $sizelist[$i]->iSizeId;
			$variable = $this->product_model->get_width_lenght($size,$iproductid,$id)->result();
			if(isset($variable[0]->iWidth))
			$sizelist[$i]->iWidth = $variable[0]->iWidth;
			else
			$sizelist[$i]->iWidth = 0;
			if(isset($variable[0]->iLength))
			$sizelist[$i]->iLength = $variable[0]->iLength;
			else
			$sizelist[$i]->iLength = 0;
		}
		$this->smarty->assign("sizelist",$sizelist);
		$this->smarty->view('admin/product/sizecat.tpl');
	}
	function makeeditcolor(){
		$iProductColorId = $_REQUEST['iProductColorId'];
		$lang = $_REQUEST['lang'];
		$data = $this->product_model->getProductId($iProductColorId)->result();
		$color = $this->product_model->all_color()->result();
		//echo "<pre>";print_r($data);exit;
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$this->smarty->assign("color",$color);
		$this->smarty->assign("data",$data);
		$this->smarty->assign("lang",$lang);
		if($_POST)
		{
			echo "editcolor";exit;
		}
		$this->smarty->view('admin/product/editcolor.tpl');
		
	}
	
	function scdetail(){
		
		
		$product_id = $_REQUEST['iProductId'];
		
		if ($_REQUEST['iColorId']){
		$color_ids = $_REQUEST['iColorId'];
		$comma_separated = implode(",", $color_ids);
		$color_details = $this -> product_model->all_color_id($comma_separated)->result_array();
		}else{
			$color_details = array();
		}
		
		$all_size_details = $this -> product_model->all_size()->result_array();
		$all_sizeid = array();
		foreach($all_size_details as $sval){
			array_push($all_sizeid,$sval['iSizeId']);	
		}
		$all_colorsizes = $this->product_model->all_colorsize_deatil($product_id)->result_array();
		$all_unique_colorsize = $this->product_model->all_unique_colorsize_deatil($product_id)->result_array();
		$final_all_colorids = array();
		if(is_array($all_unique_colorsize)){
			foreach ($all_unique_colorsize as $key => $val){
				$final_all_colorids[$key] = $val['iColorId'];
			}
		}
		$final_all_colorsizes = array();
		$final_all_colorsizes_price= array();
		if(is_array($all_colorsizes)){
			foreach ($all_colorsizes as $key => $val){
						
				$final_all_colorsizes[$val['iColorId']][$key] = $val['iSizeId'];
				$final_all_colorsizes_price[$val['iColorId']][$val['iSizeId']]['price']= $val['fPrice'];
				
			}
		}
		$decoration = $this->product_model->getDecoServices()->result_array();
		$this->smarty->assign("decoration",$decoration);
		
		$noOfColors = $this->product_model->getColors()->result_array();
		$all_quantity_range = $this->product_model->getQuantityRange()->result_array();
		$selected_quantity_range_id= $all_quantity_range[0]['iQuantityRangeId'];
		$editDecoration = $this->product_model->getDecorationData($product_id,$selected_quantity_range_id)->result_array();		
		$editDecorationNew = array();
		
		for($i=0;$i<count($editDecoration);$i++){
			$editDecorationNew[$editDecoration[$i]['iNumberColorsId']][$editDecoration[$i]['iPrintLocationId']] = $editDecoration[$i]['fPrice'];
		}
		for($i=0;$i<count($noOfColors);$i++){
			for($j=0;$j<count($decoration);$j++){
				if(!isset($editDecorationNew[$noOfColors[$i]['iNumberColorsId']][$decoration[$j]['iPrintLocationId']])){
					$editDecorationNew[$noOfColors[$i]['iNumberColorsId']][$decoration[$j]['iPrintLocationId']] =  '';
				}
			}
		}
		/* Jyotiranjan */
		
		$embroideryPrice = $this->product_model->getEmbroideryPrice($product_id,$selected_quantity_range_id)->result_array();
		$embroideryPriceNew = array();
		
		for($i=0;$i<count($embroideryPrice);$i++){
			$embroideryPriceNew[$embroideryPrice[$i]['iPrintLocationId']] = $embroideryPrice[$i]['fPrice'];
		}
		//echo "<pre>";print_r($embroideryPriceNew);exit;
		$this->smarty->assign("embroideryPriceNew",$embroideryPriceNew);
		
		$all_product = $this->product_model->list_all('','','','')->result_array();
		$this->smarty->assign("selected_quantity_range_id",$selected_quantity_range_id);
		$this->smarty->assign("all_quantity_range",$all_quantity_range);
		$this->smarty->assign("all_product",$all_product);
		$this->smarty->assign("all_sizeid",$all_sizeid);
		$this->smarty->assign("product_id",$product_id);
		$this->smarty->assign("editDecorationNew",$editDecorationNew);
		$this->smarty->assign("editDecoration",$editDecoration);
		$this->smarty->assign("color_details",array_reverse($color_details));
		$this->smarty->assign("all_size_details",$all_size_details);
		$this->smarty->assign("all_unique_colorsize",$all_unique_colorsize);
		$this->smarty->assign("final_all_colorsizes",$final_all_colorsizes);
		$this->smarty->assign("all_colorsizes",$all_colorsizes);
		$this->smarty->assign("final_all_colorids",$final_all_colorids);
		$this->smarty->assign("final_all_colorsizes_price",$final_all_colorsizes_price);
		$this->smarty->view('admin/product/scdetail.tpl');
		
	}
	function load_decoration(){
		$product_id = $_REQUEST['iProductId'];
		$selected_quantity_range_id = $_REQUEST['qid'];
		
		$noOfColors = $this->product_model->getColors()->result_array();
		$decoration = $this->product_model->getDecoServices()->result_array();
		$all_quantity_range = $this->product_model->getQuantityRangeById($selected_quantity_range_id)->result_array();
		$editDecoration = $this->product_model->getDecorationData($product_id,$selected_quantity_range_id)->result_array();
		$editDecorationNew = array();
		
		
		for($i=0;$i<count($editDecoration);$i++){
			$editDecorationNew[$editDecoration[$i]['iNumberColorsId']][$editDecoration[$i]['iPrintLocationId']] = $editDecoration[$i]['fPrice'];
		}
		
		for($i=0;$i<count($noOfColors);$i++){
			for($j=0;$j<count($decoration);$j++){
				if(!isset($editDecorationNew[$noOfColors[$i]['iNumberColorsId']][$decoration[$j]['iPrintLocationId']])){
					$editDecorationNew[$noOfColors[$i]['iNumberColorsId']][$decoration[$j]['iPrintLocationId']] =  '';
				}
			}
		}
		
		/* :::::: Jyotiranjan ::::::: */
		
		$embroideryPrice = $this->product_model->getEmbroideryPrice($product_id,$selected_quantity_range_id)->result_array();
		$embroideryPriceNew = array();
		
		for($i=0;$i<count($embroideryPrice);$i++){
			$embroideryPriceNew[$embroideryPrice[$i]['iPrintLocationId']] = $embroideryPrice[$i]['fPrice'];
		}
		$this->smarty->assign("embroideryPriceNew",$embroideryPriceNew);
		
		$this->smarty->assign("all_quantity_range",$all_quantity_range);
		$this->smarty->assign("product_id",$product_id);
		$this->smarty->assign("decoration",$decoration);
		$this->smarty->assign("noOfColors",$noOfColors);
		$this->smarty->assign("editDecoration",$editDecoration);
		$this->smarty->assign("editDecorationNew",$editDecorationNew);
		$this->smarty->view('admin/product/ajax-decoration.tpl');	
	}
	function color_detail(){
		//echo 'in';exit;
		
		$data = $_REQUEST['Data'];
		$copy = $_REQUEST['copy'];
		if (is_array($copy)){
			foreach ($copy as $ck => $cv){
				//echo $ck.'>>>'.$cv;exit;
				$data[$ck] = $data[$cv] ;
			}
			
		}
		$color_detail = array();		
		$color_detail['iProductId'] = $_REQUEST['iProductId'];
		if(is_array($data)){
			foreach ($data as $key => $value)
			{				
				$color_detail['iColorId'] = $key;
				if(is_array($value))
				{
					foreach ($value as $k => $v)
					{
						$color_detail['iSizeId'] = $k;						
						$color_detail['fPrice'] = $v;
						$id= $this->product_model->save_color_deatil($color_detail);
					}
				}
			}			
		}		
	}
	function seo_url($sURL) {
	       $sURL = strtolower($sURL);
	       $sURL = preg_replace("/\W+/", " ", $sURL);
	       $sURL = trim($sURL);
	       $sURL = str_replace(" ", "-", $sURL);
	       return $sURL;
	  }
	function deleteColorimage()
	{
		
		$upload_path = $this->config->item('upload_image_path');
		$iProductColorId = $_REQUEST['id'];
		$iProductId = $_REQUEST['productId'];
		$data = $this->product_model->get_image_by_id($iProductColorId)->result();
		if($_REQUEST['img'] == 'vFrontImage')
		{
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/'.$data[0]->vFrontImage);
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/200X200_'.$data[0]->vFrontImage);
			$var = $this->product_model->delete_colorImage($iProductColorId,$_REQUEST['img']);
		}
		if($_REQUEST['img'] == 'vBackImage')
		{
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/'.$data[0]->vBackImage);
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/200X200_'.$data[0]->vBackImage);
			$var = $this->product_model->delete_colorImage($iProductColorId,$_REQUEST['img']);
		}
		if($_REQUEST['img'] == 'vRightSleeveImage')
		{
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/'.$data[0]->vRightSleeveImage);
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/200X200_'.$data[0]->vRightSleeveImage);
			$var = $this->product_model->delete_colorImage($iProductColorId,$_REQUEST['img']);
		}
		if($_REQUEST['img'] == 'vLeftSleeveImage')
		{
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/'.$data[0]->vLeftSleeveImage);
			unlink($upload_path.'product/'.$iProductId.'/'.$iProductColorId.'/200X200_'.$data[0]->vLeftSleeveImage);
			$var = $this->product_model->delete_colorImage($iProductColorId,$_REQUEST['img']);
		}
		if($var)
		{
			$var_msg = "Image deleted successfully";
			
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'product/edit?iProductId='.$iProductId.'&msg='.$var_msg.'&sec=ds');
		exit;
	}	

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
