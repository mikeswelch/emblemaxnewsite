<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newslettermembers extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/newslettermembers_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'newslettermembers');    
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		$this->newslettermemberslist();
	}
        
	function newslettermemberslist()
	{
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
						
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vEmail LIKE '".stripslashes($alp)."%' OR vEmail LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
                    
		    $ssql.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
                    //echo $ssql;exit;
		}
		if($_SESSION['module_name'] != 'newslettermembers_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		$totrec = $this->newslettermembers_model->count_all($ssql)->result();
                //echo "<pre>";
                //print_r($totrec);exit;
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
                
		include($site_path."system/libraries/paging.inc.php");
               
                
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vEmail'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='dAddedDate'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}	
		
		$data = $this->newslettermembers_model->list_all($var_limit,$ssql,$field,$sort)->result();
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->newslettermembers_model->displayalphasearch()->result();
		//echo "<pre>";
		//print_r($db_alp);exit;
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vEmail, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'newslettermembers/newslettermemberslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'newslettermembers/newslettermemberslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'newslettermembers/newslettermemberslist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		for($i=0;$i<count($data);$i++)
		 {
		    if($data[$i]->dAddedDate != ''){
			$date1 = $data[$i]->dAddedDate;
			$data[$i]->dAddedDate = date('m/d/Y',strtotime($date1));
		    }else{
			$data[$i]->dAddedDate = '';
		    }
		}
		
        $this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'newslettermembers_model';
		$AlphaBox.='</ul>';
        if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/newsletter_members/view_newsletter_member.tpl');
	} 
        
	function delete()
	{
		$var = $this->newslettermembers_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Newsletter member is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		//$var = $this->user_model->delete($id);
		//if($var)$var_msg = "User is deleted successfully.";else $var_msg="Error-in delete.";
		redirect(admin_url.'newslettermembers/newslettermemberslist?msg='.$var_msg);
		exit;
	}
        
       function make_active($action,$iNewsletterId)
	{
		
		//echo "<pre>";
		//print_r($user_id);exit;
		$id = $this->newslettermembers_model->multiple_update_status($iNewsletterId,$action);
		//echo "<pre>";
		//print_r($user_id);exit;
		$coloridcnt  = @explode("','",$iNewsletterId);
		$cnt=count($coloridcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'newslettermembers/newslettermemberslist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
        function sendMail(){
		
		$newsletter_email = $this->newslettermembers_model->getEmail()->result();
		$this->smarty->assign("newsletter_email",$newsletter_email);
		$this->smarty->view('admin/newsletter_members/newsletter_member.tpl');
		
		if($_POST)
		{
			$site_url = $this->config->item('site_url');
			$Data = $_POST['Data'];
			$tQuestion = $Data['tMessage'];
			$iNewsletterId = $Data['iNewsletterId'];
			$email = $this->newslettermembers_model->getEmailAddress($iNewsletterId)->result();
			$vEmail = $email[0]->vEmail;
			
			$body_arr = Array("#SITE_NAME#","#SITE_URL#","#QUESTION#","#MAIL_FOOTER#");
			$post_arr = Array('Izishirt',$site_url,$tQuestion,"Izishirt");
	       
			$this->Send("NEWSLETTER_INFORMATION","Member",$vEmail,$body_arr,$post_arr);
			$msg = "Newsletter Email hs been sent Successfully.";
			redirect(admin_url.'newslettermembers/newslettermemberslist?msg='.$msg);
			exit;
		}
	}
	

        public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr)
        { 
	  $ssql = "AND vEmailCode='".$EmailCode."' AND eSendType ='".$SendType."'";
	
	  $email_info = $this->newslettermembers_model->getEmailTemplate($ssql)->result_array();
	  $site_url = $this->config->item('site_url');
	  $headers = "MIME-Version: 1.0\r\n";
	  $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
	  $headers .= 'From: izishirt.com <support@www.izishirt.com>' . "\r\n".
				'Reply-To: izishirt.com <support@www.izishirt.com>'. "\r\n".
				'Return-Path: izishirt.com <support@.izishirt.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
	  
	  $Subject = strtr($email_info[0]['vEmailSubject'], "\r\n" , "  " );
	  $this->body = $email_info[0]['tEmailMessage_en'];
	  $this->body = str_replace($bodyArr,$postArr, $this->body);
	  
	  $To = stripcslashes($ToEmail);	  
	  $htmlMail = '
	  
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	       <html xmlns="http://www.w3.org/1999/xhtml">
	       <head>
	       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	       <title>Izishirt</title>
	       </head>
	       
	       <body style="padding:0; margin:0; border:0;">
		    <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			 <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
			      <div style="background:#e5e9ec; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/admin/images/logo_email.png" alt=""/></div>
				   '.$this->body.'
			      </div>
			 </div>
		    </div>
	       </body>
	       </html>';
	  $res = @mail($To,$Subject,$htmlMail,$headers,'-fnoreplay@izishirt.com');
	 }

	
        function search_action(){
        
		$action = $_POST['action'];
		$iNewsletterId = $_POST['commonId'];
		
		if($iNewsletterId == '')
		$iNewsletterId = $_POST['iNewsletterId'];
		 	
		if(is_array($iNewsletterId)){
		    $iNewsletterId  = @implode("','",$iNewsletterId);
		}
		$iNewsletterId = $iNewsletterId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iNewsletterId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iNewsletterId);
		}else{
	    }
	}
	
	function make_delete($iNewsletterId){
		//echo "<pre>";
		//print_r("fsklgv;flk");exit;
		$id = $this->newslettermembers_model->delete_data($iNewsletterId);		
		$coloridcnt  = @explode("','",$iNewsletterId);
		$cnt=count($coloridcnt);	
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'newslettermembers/newslettermemberslist?msg='.$var_msg);
		exit;
	}
	
}
    