<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Systememail extends CI_Controller{
	var $limit = '';
	var $logged = '';
	var $data = '';
	var $var_msg ='';
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('pagination');
		$this -> load -> model('admin/systememail_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ;
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'systememail');    

		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		
		$this->smarty->assign("Name","Welcome To Byu Admin Panel");
		
	}
	/*
	 Function Name: index
	 Developer Name: Parth Devmorari
	 Purpose: to be called when the adminstrator page loads
	 Created Date: 12-01-2012
	 */
	function index()
	{
		$this->systememaillist();
	}
	/*
	 Function Name: systememaillist
	 Developer Name: Parth Devmorari
	 Purpose: to be called the index function call and show listing
	 Created Date: 12-01-2012
	 */
	function systememaillist()
	{
		$limit1 = $this->systememail_model->limit_fetch()->result();
		$limit = $limit1[0]->vValue;
		$PAGELIMIT = $limit1[1]->vValue;
		
		$AlphaBox ='';
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
			}else{
				$var_msg = '';
			}
		
		
			$this->smarty->assign("var_msg",$var_msg);
			if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){		    
		    $ssql = " AND vEmailTitle LIKE '".stripslashes($alp)."%'";
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
			$ssql = " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";			
		}
		
		if($_SESSION['module_name'] != 'systememail_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';			
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}
		}
		
		$totrec = $this->systememail_model->count_all($ssql)->result();
	        $num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
                		
		$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='vEmailTitle'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='vFromEmail'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='estatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		
		$data = $this->systememail_model->list_all($var_limit,$ssql,$field,$sort)->result();
		
		
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$limit;
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
			
		$db_alp = $this->systememail_model->displayalphasearch()->result();
		
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vEmailTitle, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'systememail/systememaillist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'systememail/systememaillist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'systememail/systememaillist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		
		$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'systememail_model';
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$AlphaBox.='</ul>';
		$language_data = $this->systememail_model->list_all_page()->result();
	 
		 $this->smarty->assign('language_data',$language_data);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/systememail/view-systememail.tpl');
		
	}

	function edit()
	{
		$language_data = $this->systememail_model->list_all_page()->result();
		$this->smarty->assign('language_data',$language_data);
		$ckeditor_path = $this->config->item('ckeditor_path');
		$this->smarty->assign("ckeditor_path",$ckeditor_path);
		
		$id = $_REQUEST['iEmailTemplateId'];
		$admin_url = $this->config->item('admin_url');
		$data = $this->systememail_model->get_one_by_id($id)->row();		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'systememail/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/systememail/systememail.tpl');
		
		if($_POST){
			$Data = $_POST['Data'];
			$iEmailTemplateId = $_POST['iEmailTemplateId'];
			$id = $this->systememail_model->update($iEmailTemplateId,$Data);
			if($id)$var_msg = "System email is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'systememail/systememaillist?msg='.$var_msg);
			exit;
		}
	}

	function delete()
	{
		$var = $this->systememail_model->delete($_REQUEST['id']);
		if($var)$var_msg = "System email is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'systememail/systememaillist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}

	function search_action(){
        
		
		$action = $_POST['action'];
		$iEmailTemplateId = $_POST['commonId'];
		if($iEmailTemplateId == '')
		$iEmailTemplateId = $_POST['iEmailTemplateId'];
		
		
                
		if(is_array($iEmailTemplateId)){
		    $iEmailTemplateId  = @implode("','",$iEmailTemplateId);
		}
		$iEmailTemplateId = $iEmailTemplateId;
        
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iEmailTemplateId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iEmailTemplateId);
		}else{
		}
	}

	function make_active($action,$iEmailTemplateId)
	{
		
		
		$id = $this->systememail_model->multiple_update_status($iEmailTemplateId,$action);
		$iEmailTemplateId  = @explode("','",$iEmailTemplateId);
		$cnt=count($iEmailTemplateId);
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." System email activated successfully.";
		      }else{
		          $var_msg = $cnt." System email inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'systememail/systememaillist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}

	function make_delete($iEmailTemplateId){
		$id = $this->systememail_model->delete_data($iEmailTemplateId);
		
		$iEmailTemplateId  = @explode("','",$iEmailTemplateId);
		$cnt=count($iEmailTemplateId);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'systememail/systememaillist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
}
/* End of file systememail.php */
/* Location: ./application/controllers/admin/systememail.php */