<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productcategory  extends CI_Controller{
        
	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/productcategory_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$this->smarty->assign("filename",'productcategory');
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
		
	}
	
	function index() {
		
		$this->productcategorylist();
	}
	
	function productcategorylist()
	{
		
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		if(isset($_REQUEST['msg']) !=''){
			$var_msg = $_REQUEST['msg'];
		}else{
			$var_msg = '';
		}
                
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){		    		    
		     $ssql.= " AND (vCategory LIKE '".stripslashes($alp)."%' OR vCategory LIKE '".strtolower(stripslashes($alp))."%' )";
                }
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
                    
		    $ssql.= " AND ".stripslashes($option)." LIKE '%".stripslashes($keyword)."%'";
                    //echo $ssql;exit;
		}
		if($_SESSION['module_name'] != 'productcategory_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		//echo $ssql;exit;
		$totrec = $this->productcategory_model->count_all($ssql)->result();
		$num_totrec = $totrec[0]->tot;

		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
		//$ssql = base64_encode($ssql);
		include($site_path."system/libraries/paging.inc.php");
		//$ssql = base64_decode($ssql);
		
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   
		   if($field =='pct.vCategory'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='pc.eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->productcategory_model->list_all($var_limit,$ssql,$field,$sort)->result();
		if($start == '0'){
			$start = 1;
		}
		
                $num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
	
                $db_alp = $this->productcategory_model->displayalphasearch()->result();
		  for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vCategory, 0,1));
		}
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'productcategory/productcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'productcategory/productcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'productcategory/productcategorylist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}
		
		//$ssql = base64_encode($ssql);
		$this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'productcategory_model';
		
		$action = $_REQUEST['action'];
		$this->smarty->assign("action",$action);
		
		$AlphaBox.='</ul>';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("field",$field);
		if(!isset($page_link)){$page_link = '';}
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("data",$data);
		$this->smarty->view( 'admin/productcategory/view-productcategory.tpl');
	}
	
	function add()
	{  
		$upload_path = $this->config->item('upload_path');
		$this->smarty->assign("upload_path",$upload_path);
		$language = $this->productcategory_model->all_language()->result();		
		$parentarray = $this->productcategory_model->getParentCatNew('0','','0','1','0');

		$this->smarty->assign("language",$language);
		$this->smarty->assign("lang",$lang);
		
		$totalRec = $this->productcategory_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		
		$type = $this->db->query( "SHOW COLUMNS FROM `product_category` WHERE Field = 'ePosition'" )->row( 0 )->Type;
		preg_match('/^enum\((.*)\)$/', $type, $matches);
		foreach( explode(',', $matches[1]) as $value )
		{
			$ePosition[] = trim( $value, "'" );
		}
		$this->smarty->assign('ePosition',$ePosition);
		
		$this->smarty->assign('showReview','showReview');
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'productcategory/add';
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->assign("parentarray",$parentarray);
		$this->smarty->view('admin/productcategory/productcategory.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
			$data = $_POST['data'];
			$Data['vImage'] = $_FILES['vImage']['name'];
			$Data['vUrlText']= $this->seo_url($data['vCategory']);
			$data = $_POST['data'];
			$id = $this->productcategory_model->save($Data);
			$data['iCategoryId'] = $id;
			$transId = $this->productcategory_model->saveData($data);
			$img_uploaded = $this->do_upload($id);			
			$Data['vImage'] = $img_uploaded;
                	$id = $this->productcategory_model->update($iPriductId,$Data);			
			if($id)$var_msg = "Category Type is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'productcategory/productcategorylist?msg='.$var_msg);
				exit;
		}
	}

        function edit()
	{
		
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['iCategoryId'];
		
                $parentarray = $this->productcategory_model->getParentCatNew('0','','0','1','0');
		$lang = $_REQUEST['lang'];
		$totalRec = $this->productcategory_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->smarty->assign('showReview','showReview');
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		$admin_url = $this->config->item('admin_url');		
		$Data = $this->productcategory_model->get_one_by_id($id)->row();
		$data = $this->productcategory_model->get_data_by_id($id,$lang)->row();
		$type = $this->db->query( "SHOW COLUMNS FROM `product_category` WHERE Field = 'ePosition'" )->row( 0 )->Type;
		preg_match('/^enum\((.*)\)$/', $type, $matches);
		foreach( explode(',', $matches[1]) as $value )
		{
			$ePosition[] = trim( $value, "'" );
		}
		$this->smarty->assign('ePosition',$ePosition);
		
		$language = $this->productcategory_model->all_language()->result();
		$this->smarty->assign("datasize",$datasize);
		$this->smarty->assign("pre_data",$pre_data);		
		$this->smarty->assign("language",$language);
		$this->smarty->assign("lang",$lang);
		$this->smarty->assign("parentarray",$parentarray);
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'productcategory/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("Data",$Data);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/productcategory/productcategory.tpl');
		
		if($_POST)
		{
			$Data = $_POST['Data'];
                        if($_FILES['vImage']['name']!=''){
			    $Data['vImage'] = $_FILES['vImage']['name'];
			}
			else{
			    $productData = $this->productcategory_model->get_one_by_id($_POST['iCategoryId'])->result_array();
			    $Data['vImage'] = $productData[0]['vImage'];
			}
			$data = $_POST['data'];
			$Data['vUrlText']= $this->seo_url($data['vCategory']);
			$lang = $data['vLanguageCode'];
			$iCategoryId = $_POST['iCategoryId'];
				
			$img_uploaded = $this->do_upload($iCategoryId);
			if($img_uploaded)
				$Data['vImage'] = $img_uploaded;			
			//$id = $this->productcategory_model->update($iCategoryId,$Data);
			
			$check_lang = $this->productcategory_model->get_data_by_id($iCategoryId,$lang)->result();
			if(isset($check_lang[0]->iCategoryTranslationId) AND $check_lang[0]->iCategoryTranslationId != '')
			{
				$id = $this->productcategory_model->updateDataCatTran($check_lang[0]->iCategoryTranslationId,$data);
			}else{
				$data['iCategoryId'] = $iCategoryId;
				$Id = $this->productcategory_model->saveData($data);
			}
			
			
			
			if(isset($_POST['SizeId']))
			{
				$deleteid = $this->productcategory_model->deletesize($iCategoryId);
				$size = $_POST['SizeId'];
				for($i=0;$i<count($size);$i++)
				{
					$sizedata['iSizeId'] = $size[$i];
					$sizedata['iCategoryId'] = $iCategoryId;
					$sizeid = $this->productcategory_model->saveSize($sizedata);
				}	
			}else{
				$deleteid = $this->productcategory_model->deletesize($iCategoryId);
			}
			
			
			$id = $this->productcategory_model->update($iCategoryId,$Data);
			if($id)$var_msg = "Category Type is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'productcategory/productcategorylist?msg='.$var_msg);
			exit;
		}
	}

	function delete()
	{
		
		$var = $this->productcategory_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Category Type is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'productcategory/productcategorylist?msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}

	function search_action(){
        
		$action = $_POST['action'];
		$iCategoryId = $_POST['commonId'];
		
		if($iCategoryId == '')
		$iCategoryId = $_POST['iCategoryId'];
		 	
		if(is_array($iCategoryId)){
		    $iCategoryId  = @implode("','",$iCategoryId);
		}
		$iCategoryId = $iCategoryId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iCategoryId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iCategoryId);
		}else{
	    }
	}

	function make_active($action,$iCategoryId)
	{		
		$id = $this->productcategory_model->multiple_update_status($iCategoryId,$action);
		
		$iCategoryId  = @explode("','",$iCategoryId);
		$cnt=count($iCategoryId);
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Category Type activated successfully.";
		      }else{
		          $var_msg = $cnt." Category Type inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'productcategory/productcategorylist?action='.$action.'&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}

	function make_delete($iCategoryId){
		$id = $this->productcategory_model->delete_data($iCategoryId);
		
		$iCategoryId  = @explode("','",$iCategoryId);
		$cnt=count($iCategoryId);
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		      
		}else{
		  $var_msg = "Error-in delete";
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'productcategory/productcategorylist?action=Deletes&msg='.$var_msg.'&ssql='.$ssql);
		exit;
	}
	function do_upload($iProductId,$vImageName)
	{
		if(!is_dir('uploads/productcategory/')){
			@mkdir('uploads/productcategory/', 0777);
		}
		if(!is_dir('uploads/productcategory/'.$iProductId)){
			@mkdir('uploads/productcategory/'.$iProductId, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/productcategory/'.$iProductId,
		  'max_size'=>2000
		);
		$this->load->library('upload', $config); 
		$this->upload->do_upload('vImage'); //do upload
		
		//want to create thumbnail
		 
		$image_data = $this->upload->data(); //get image data
                $config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/productcategory/'.$iProductId.'/282X320_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 282,
		  'height' => 320
		);
		$this->load->library('image_lib', $config1); //load library
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	function seo_url($sURL) {
	       $sURL = strtolower($sURL);
	       $sURL = preg_replace("/\W+/", " ", $sURL);
	       $sURL = trim($sURL);
	       $sURL = str_replace(" ", "-", $sURL);
	       return $sURL;
	  }
	function deleteimage()
	{
		$upload_path = $this->config->item('upload_image_path');
		$iCategoryId = $_REQUEST['iCategoryId'];
		$lang = $_REQUEST['lang'];
		$data = $this->productcategory_model->get_one_by_id($iCategoryId)->result();
		$Data = $this->productcategory_model->get_data_by_id($iCategoryId,$lang)->row();
		$var = unlink($upload_path.'productcategory/'.$iCategoryId.'/282X320_'.$data[0]->vImage);
		unlink($upload_path.'productcategory/'.$iCategoryId.'/'.$data[0]->vImage);
		if($var)
		{
			$var = $this->productcategory_model->delete_image($iCategoryId);
			
		}
		if($var)
		{
			$var_msg = "Photo deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'productcategory/edit?iCategoryId='.$iCategoryId.'&msg='.$var_msg.'&lang=en');
		exit;
	}

	
}
