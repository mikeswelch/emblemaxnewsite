<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homeslider extends CI_Controller{

	private $limit = 10;
	var $logged = '';
	var $data = '';
	var $var_msg = '';
    
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this -> load -> model('admin/homeslider_model', '', TRUE);
		$this -> load -> helper('url');
		$this -> data['post'] = FALSE;
		
		$this->load->library('upload'); 
		$this->load->library('image_lib'); //load librar
		
		$admin_url = $this->config->item('admin_url');
		if(isset($_SESSION["sess_iAdminId"]) ==''){
			redirect(admin_url.'authentication/login');
			exit ; 
		}
		$admin_css_path = $this->config->item('admin_css_path');
		$admin_js_path = $this->config->item('admin_js_path');
		
		$this->smarty->assign("admin_js_path",$admin_js_path);
		$this->smarty->assign("admin_css_path",$admin_css_path);
		$this->smarty->assign("filename",'homeslider');    
		$fancybox_path = $this->config->item('fancybox_path');
		$this->smarty->assign("fancybox_path",$fancybox_path);
		$admin_image_path = $this->config->item('admin_image_path');
		$this->smarty->assign("admin_image_path",$admin_image_path);
		$this->smarty->assign("admin_url",$admin_url);
		$this->smarty->assign("Name","Welcome To Izishirt Admin Panel");
}
	
	function index() {
		$this->homesliderlist();
	}
        function homesliderlist()
	{
		if(isset($_REQUEST['ssql'])){
			$ssql = $_REQUEST['ssql'];
			//$ssql = base64_decode($ssql);
		}else{
			$ssql = '';
		}
		$AlphaBox ='';
		$ssql ='';		
		$admin_url = $this->config->item('admin_url');
		$site_path = $this->config->item('site_path');
		
		if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];
		}else{
				$var_msg = '';
		}
						
		if(isset($_REQUEST['alp']) !=''){
		   $alp = $_REQUEST['alp']; 
		}else{
		    $alp = '';
		}
		if($alp !=''){
		    $ssql.= " AND (vTitle LIKE '".stripslashes($alp)."%' OR vTitle LIKE '".strtolower(stripslashes($alp))."%' )";
		}
		if(isset($_REQUEST['option']) !=''){
		   $option = $_REQUEST['option']; 
		}else{
		    $option = '';
		}
		if(isset($_REQUEST['keyword']) !=''){
		   $keyword = $_REQUEST['keyword']; 
		}else{
		    $keyword = '';
		}
		
		if($option != '' && $keyword != ''){
                    
		    $ssql.= " AND ".stripslashes($option)." LIKE '".stripslashes($keyword)."%'";
                    //echo $ssql;exit;
		}
		if($_SESSION['module_name'] != 'homeslider_model' || $_REQUEST['action'] == 'Show All'){
			$ssql ='';
			$_SESSION['nstart'] = '';
			$_SESSION['start'] = '';
		}else{
			if($alp !='' || $keyword != ''){
				$_SESSION['nstart'] = '';
				$_SESSION['start'] = '';
			}			
		}
		$totrec = $this->homeslider_model->count_all($ssql)->result();
                //echo "<pre>";
                //print_r($totrec);exit;
		$num_totrec = $totrec[0]->tot;
		if($alp !='')
		{
			$var_msg=$num_totrec." Record matched for ".$alp;
			
		}
		if($option != '' && $keyword != ''){
		    $var_msg=$num_totrec." Record matched for ".$keyword;
		}
                
		include($site_path."system/libraries/paging.inc.php");
               
                
		/*Created by Ravi Gondaliya*/
		if(!isset($_REQUEST['order']))
		{
			$sort='ASC';
		}
		if(isset($_REQUEST['field'])!='')
		{
		   $field = $_REQUEST['field'];
		   $order=$_REQUEST['order'];
		   if($field =='vTitle'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='iOrderNo'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }elseif($field =='eStatus'){
				if($order == 'ASC')
				{
					 $sort='DESC';
				}
				else
				{
					 $sort='ASC';
				}
		   }
		   
		}
		
		$data = $this->homeslider_model->list_all($var_limit,$ssql,$field,$sort)->result();
		//echo "<pre>";
		//print_r($data);exit;
		if($start == '0'){
		    $start = 1;
		}
		
		$num_limit = ($start-1)*$this->limit;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		
		$startrec = $num_limit;
		
		$lastrec = $startrec + $this->limit;
		$startrec = $startrec + 1;
		if($lastrec > $num_totrec)
			$lastrec = $num_totrec;
			if($num_totrec > 0 )
			{
				$recmsg = "Showing ".$startrec." - ".$lastrec." records of ".$num_totrec;
			}
			else
			{
				$recmsg="No records found.";
			}
	        
		
		$db_alp = $this->homeslider_model->displayalphasearch()->result();
		//echo "<pre>";
		//print_r($db_alp);exit;
		    for($i=0;$i<count($db_alp);$i++){
		    $db_alp[$i] = strtoupper(substr($db_alp[$i]->vTitle, 0,1));
		}
		
		$alpha_rs =implode(",",$db_alp);
		$AlphaChar = @explode(',',$alpha_rs);
		$AlphaBox.='<ul class="pagination">';
		for($i=65;$i<=90;$i++){
			
			if(!@in_array(chr($i),$AlphaChar)){
				$AlphaBox.= '<li ><div class="inactive_page">'.chr($i).'</div></li>';
			}else{
				if(isset($_REQUEST['alp']) ==''){
				$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'homeslider/homesliderlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
			        }
				else{
					#echo $_REQUEST['alp']; exit;
					if(chr($i)==$_REQUEST['alp'])
					$AlphaBox.= '<li class="pageactive"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'homeslider/homesliderlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
					else
					$AlphaBox.= '<li class="page"><a  href="javascript:void(0);" onclick="AlphaSearch(\''.chr($i).'\',\''.'homeslider/homesliderlist'.'\');" id="alch_'.$i.'" >'.chr($i).'</a></li>';
				}
			}
		}

		
                $this->smarty->assign("ssql",$ssql);
		$_SESSION['module_name'] = 'homeslider_model';
		$AlphaBox.='</ul>';
                if(!isset($page_link)) $page_link= '';
		$this->smarty->assign("option",$option);
		$this->smarty->assign("keyword",$keyword);
		$this->smarty->assign("order",$sort);
		$this->smarty->assign("var_msg",$var_msg);
		$this->smarty->assign("field",$field);
		$this->smarty->assign("AlphaBox",$AlphaBox);
		$this->smarty->assign("page_link",$page_link);
		$this->smarty->assign("recmsg",$recmsg);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/homeslider/view-homeslider.tpl');
	}
        
        function add()
	{
		
		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'homeslider/add';

		$totalRec = $this->homeslider_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$this->load->library('upload'); 
		$this->load->library('image_lib'); //load librar		
		$this->smarty->assign("totalRec",$totalRec);
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		$this->smarty->assign("data",$data);
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		$this->smarty->view('admin/homeslider/homeslider.tpl');
		
		if($_POST)
		{
			
			$Data = $_POST['Data'];
			$Data['vImage'] = $_FILES['vImage']['name'];
			$id = $this->homeslider_model->save($Data);
			
			$img_uploaded = $this->do_upload($id);
			$id = $this->homeslider_model->update($id,$Data);
			if($id)$var_msg = "Home Slider Banner is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'homeslider/homesliderlist?msg='.$var_msg);
				exit;
		}
	}
	
        function edit()
	{
                
		$db_staticpages = $this->homeslider_model->list_staticpages()->result();		
		$this->smarty->assign("db_staticpages",$db_staticpages);
		$upload_path = $this->config->item('upload_path');
	 	$this->smarty->assign("upload_path",$upload_path);
		$id = $_REQUEST['iHomeSliderId'];

		$admin_url = $this->config->item('admin_url');		
		$data = $this->homeslider_model->get_one_by_id($id)->row();

		$totalRec = $this->homeslider_model->count_all($ssql)->result();
		$totalRec = $totalRec[0]->tot;
		$initOrder =1;
		$this->smarty->assign("initOrder",$initOrder);
		$this->smarty->assign("totalRec",$totalRec);
		
		$this->smarty->assign('operation','edit');
		$action = $admin_url.'banner/edit';
		$this->smarty->assign("action",$action);
		$this->smarty->assign("data",$data);
		$this->smarty->view('admin/homeslider/homeslider.tpl');
		
		if($_POST)
		{

			$Data = $_POST['Data'];
			if($_FILES['vImage']['name'] != '')
			{
			    $Data['vImage'] = $_FILES['vImage']['name'];	
			}
			$iHomeSliderId = $_REQUEST['iHomeSliderId'];
			$id = $this->homeslider_model->update($iHomeSliderId,$Data);
			$img_uploaded = $this->do_upload($iHomeSliderId);
			if($img_uploaded)
				$Data['vImage'] = $_FILES['vImage']['name'];
			
			if($id)$var_msg = "Home Slider Banner is edited successfully.";else $var_msg="Error-in edit.";
			redirect(admin_url.'homeslider/homesliderlist?msg='.$var_msg);
			exit;
		}
	}
        
        
	function delete()
	{
		$var = $this->homeslider_model->delete($_REQUEST['id']);
		if($var)$var_msg = "Home Slider Banner is deleted successfully.";else $var_msg="Error-in delete.";
		$ssql = $_REQUEST['ssql'];
		redirect(admin_url.'homeslider/homesliderlist?msg='.$var_msg);
		exit;
	}
        
       function make_active($action,$iHomeSliderId)
	{
	
		$id = $this->homeslider_model->multiple_update_status($iHomeSliderId,$action);
		$banneridcnt  = @explode("','",$iHomeSliderId);
		$cnt=count($banneridcnt);
		
		
		if($id){
		      if($action == 'Active'){
		          $var_msg = $cnt." Record activated successfully.";
		      }else{
		          $var_msg = $cnt." Record is inactivated successfully.";
		      }
		      
		}else{
		  if($action == 'Active'){
		          $var_msg = "Error-in active.";
		      }else{
		          $var_msg = "Error-in inactive.";
		      }
		}
		$ssql = $_POST['ssql'];
		redirect(admin_url.'homeslider/homesliderlist?action='.$action.'&msg='.$var_msg);
		exit;
	}
	
        function search_action(){
        
		$action = $_POST['action'];
		$iHomeSliderId = $_POST['commonId'];
		
		if($iHomeSliderId == '')
		$iHomeSliderId = $_POST['iHomeSliderId'];
		 	
		if(is_array($iHomeSliderId)){
		    $iHomeSliderId  = @implode("','",$iHomeSliderId);
		}
		$iHomeSliderId = $iHomeSliderId;
                
		if($action == 'Active' || $action == 'Inactive'){
		    $this->make_active($action,$iHomeSliderId);
		}else if($action == 'Deletes'){
		    $this->make_delete($iHomeSliderId);
		}else{
	    }
	}
	
	function make_delete($iHomeSliderId){
		//echo "<pre>";
		//print_r("fsklgv;flk");exit;
		$id = $this->homeslider_model->delete_data($iHomeSliderId);		
		$banneridcnt  = @explode("','",$iHomeSliderId);
		$cnt=count($banneridcnt);	
		if($id){
		      $var_msg = $cnt." Record deleted successfully";
		}else{
		  $var_msg = "Error-in delete";
		}
		redirect(admin_url.'homeslider/homesliderlist?msg='.$var_msg);
		exit;
	}
	function do_upload($iHomeSliderId)
	{
		if(!is_dir('uploads/home_slider/')){
			@mkdir('uploads/home_slider/', 0777);
		}
		if(!is_dir('uploads/home_slider/'.$iHomeSliderId)){
			@mkdir('uploads/home_slider/'.$iHomeSliderId, 0777);
		}
	
		$config = array(
		  'allowed_types' => "jpg|jpeg|gif|png",
		  'upload_path' => 'uploads/home_slider/'.$iHomeSliderId,
		  'max_size'=>2000
		);
		
		$this->upload->initialize($config);
		$this->upload->do_upload('vImage'); //do upload
		
		$image_data = $this->upload->data(); //get image data
		
                /*$config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/home_slider/'.$iHomeSliderId.'/1170X468_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 1170,
		  'height' => 468
		);
	       */
		
                $config1 = array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/home_slider/'.$iHomeSliderId.'/1170X468_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 1170,
		  'height' => 468
		);

               $config2= array(
		  'source_image' => $image_data['full_path'], //get original image
		  'new_image' => 'uploads/home_slider/'.$iHomeSliderId.'/200X272_'.$image_data['file_name'], //save as new image //need to create thumbs first
		  'maintain_ratio' => false,
		  'width' => 200,
		  'height' => 272
		);
		$this->image_lib->initialize($config1);
			$test1 = $this->image_lib->resize(); //do whatever specified in config
		$this->image_lib->initialize($config2);
			$test1 = $this->image_lib->resize(); //do whatever specified in config
		unset($config1);
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	
	function deleteimage()
	{
		$upload_path = $this->config->item('upload_image_path');
		
		$iHomeSliderId = $_REQUEST['id'];
		
		$data = $this->homeslider_model->get_one_by_id($iHomeSliderId)->result();
		$var1 = unlink($upload_path.'home_slider/'.$iHomeSliderId.'/'.$data[0]->vImage);
		$var1 = unlink($upload_path.'home_slider/'.$iHomeSliderId.'/1170X468_'.$data[0]->vImage);
		$var1 = unlink($upload_path.'home_slider/'.$iHomeSliderId.'/200X272_'.$data[0]->vImage);
		
		if($var1)
		{
			$var = $this->homeslider_model->delete_image($iHomeSliderId);
			
		}
		if($var)
		{
			$var_msg = "Image deleted successfully";
		}else{
			$var_msg = "Error In deletion";
		}
		redirect(admin_url.'homeslider/edit?iHomeSliderId='.$iHomeSliderId.'&msg='.$var_msg);
		exit;
	}
}
    