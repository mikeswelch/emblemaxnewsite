<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MY_Controller
{
    
	function __construct(){		
		parent::__construct();
		$this->load->model('cart_model', '', TRUE);
		$name = 'cart';
		$this->smarty->assign("name",$name);
	}
	function index() {		
		if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
			$msg = $_SESSION['msg'];
			unset($_SESSION['msg']);
		}else{
			$msg = '';
		}		
		$cart = $_SESSION['cart'];
		$this->smarty->assign("msg",$msg);
		$this->smarty->assign("cart",$cart);
		$this->smarty->view('shoping_cart.tpl');
	}
	function apply_coupon(){
		
		if(isset($_POST['couponcode']) && $_POST['couponcode'] != ''){
			$couponCode = $_POST['couponcode'];
			$couponDetail = $this->cart_model->getCouponDetailByCode($couponCode)->result_array();
			if(count($couponDetail)){
				$_SESSION['cart']['vCouponCode'] = $couponCode;
				if($couponDetail[0]['eType'] == '$'){
					$_SESSION['cart']['fCouponDiscount'] = $couponDetail[0]['fDiscount'];
				}elseif($couponDetail[0]['eType'] == '%'){
					$_SESSION['cart']['fCouponDiscount'] = $_SESSION['cart']['SUBTOTAL'] * ($couponDetail[0]['fDiscount']/100);
				}else{
					$_SESSION['cart']['fCouponDiscount'] = 0;
				}
				$_SESSION['msg'] = "Coupon Code applied successfully.";
			}else{
				$_SESSION['msg'] = "Invalid Coupon Code.";
			}
		}
		
		//GrandTotal Calculation
		$_SESSION['cart']['SUBTOTAL'] = number_format($_SESSION['cart']['SUBTOTAL'],2,'.','');
		$_SESSION['cart']['GRANDTOTAL'] = number_format($_SESSION['cart']['SUBTOTAL'] - $_SESSION['cart']['fCouponDiscount'] + $_SESSION['cart']['fTurnAroundTimeCharge'],2,'.','');
		
		redirect(site_url.'cart');
	}
	function update_cart(){
		
		$removeItem = $_POST['remove'];
		$cartItems = $_SESSION['cart']['item'];
		$cartItemsNew = array();
		for($i=0;$i<count($cartItems);$i++){
			if(!isset($removeItem[$i])){
				$cartItemsNew[] = $cartItems[$i];
			}
		}
		$_SESSION['cart']['item'] = array();
		$_SESSION['cart']['item'] = $cartItemsNew;
		
		
		$_SESSION['cart']['SUBTOTAL'] = 0;
		for($i=0;$i<count($_SESSION['cart']['item']);$i++){
		     $_SESSION['cart']['SUBTOTAL'] += $_SESSION['cart']['item'][$i]['fQuoteTotal'];
		}
		
		//Turn Around Time
		$turnAroundTime = $this->cart_model->getTurnAroundTimeById($_SESSION['cart']['iTurnAroundTimeId'])->result_array();
		$_SESSION['cart']['vTurnAroundTime'] = $turnAroundTime[0]['vTitle'];
		$_SESSION['cart']['iTurnAroundTimeId'] = $_SESSION['cart']['iTurnAroundTimeId'];
		if($turnAroundTime[0]['eFeeCharge'] == '$'){
		     $_SESSION['cart']['fTurnAroundTimeCharge'] = $turnAroundTime[0]['fCharge'];
		}elseif($turnAroundTime[0]['eFeeCharge'] == '%'){
		     $_SESSION['cart']['fTurnAroundTimeCharge'] = $_SESSION['cart']['SUBTOTAL'] * ($turnAroundTime[0]['fCharge']/100);
		}else{
		     $_SESSION['cart']['fTurnAroundTimeCharge'] = 0;
		}
		
		//Coupon Discount
		if(isset($_SESSION['cart']['vCouponCode']) && $_SESSION['cart']['vCouponCode'] != ''){
		     $couponCode = $_SESSION['cart']['vCouponCode'];
		     $couponDetail = $this->cart_model->getCouponDetailByCode($couponCode)->result_array();
		     if($couponDetail[0]['eType'] == '$'){
			  $_SESSION['cart']['fCouponDiscount'] = $couponDetail[0]['fDiscount'];
		     }elseif($couponDetail[0]['eType'] == '%'){
			  $_SESSION['cart']['fCouponDiscount'] = $_SESSION['cart']['SUBTOTAL'] * ($couponDetail[0]['fDiscount']/100);
		     }else{
			  $_SESSION['cart']['fCouponDiscount'] = 0;
		     }     
		}else{
		     $_SESSION['cart']['fCouponDiscount'] = 0;
		}
		
		//GrandTotal Calculation
		$_SESSION['cart']['fTurnAroundTimeCharge'] = number_format($_SESSION['cart']['fTurnAroundTimeCharge'],2,'.','');
		$_SESSION['cart']['fCouponDiscount'] = number_format($_SESSION['cart']['fCouponDiscount'],2,'.','');
		$_SESSION['cart']['SUBTOTAL'] = number_format($_SESSION['cart']['SUBTOTAL'],2,'.','');
		$_SESSION['cart']['GRANDTOTAL'] = number_format($_SESSION['cart']['SUBTOTAL'] - $_SESSION['cart']['fCouponDiscount'] + $_SESSION['cart']['fTurnAroundTimeCharge'],2,'.','');
		
		redirect(site_url.'cart');
	}
}