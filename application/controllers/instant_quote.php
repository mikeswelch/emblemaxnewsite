<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instant_quote extends MY_Controller {
	
	var $NumberFront = '';
	
	
     function __construct() {
	  parent::__construct();
	  $this->load->model('instantquote_model', '', TRUE);
	  $this->load->library('upload'); 
	  $this->load->library('image_lib');
	   $site_url = $this->config->item('site_url');
	   $this->smarty->assign("site_url",$site_url);
     }
     
     function index(){
		$site_url = $this->config->item('site_url');
	   $this->smarty->assign("site_url",$site_url);
		$front_image_path = $this->config->item('front_image_path');
		$this->smarty->assign("front_image_path",$front_image_path);		
		$iProductId = urldecode(basename($_SERVER['REQUEST_URI']));	  
	  if($iProductId == 'instant-quote'){
	       $productData = $this->instantquote_model->getAllProducts()->result_array();
	       $iProductId = $productData[0]['iProductId'];
	  }
	  $product = $this->instantquote_model->getProductDetail($iProductId)->result_array();
	  //$filename=$site_url.'/uploads/product/'.$iProductId.'/'.$product[0]['vImage'];
	  $filename=$_SERVER['DOCUMENT_ROOT'].'/php/emblemax//uploads/product/'.$iProductId.'/'.$product[0]['vImage'];	  	 
	 if (file_exists($filename)) {
		$fileMsg='exists';
	 } else {
		$fileMsg='Not found';
	 }
	  
	  if(count($product)>0){
	       $colors = $this->instantquote_model->getProductColors($iProductId)->result_array();
		  //$colors = $this->instantquote_model->productColors($iProductId)->result_array();
	       $sizes = $this->instantquote_model->getAllSizes()->result_array();
	       $product_color_size = $this->instantquote_model->getProductColorSizeRelation($iProductId)->result_array();
	       $productColorArray = array();
	       $productSizeArray = array();
	       for($i=0;$i<count($product_color_size);$i++){
		    $productColorArray[] = $product_color_size[$i]['iColorId'];
		    $productSizeArray[$product_color_size[$i]['iColorId']][] = $product_color_size[$i]['iSizeId'];
	     }
		//echo "<pre>";print_r($colors);exit;
		$colorids=array();
		for($i=0;$i<count($colors);$i++){
		  if(!in_array($colors[$i]['iColorId'],$colorids)){
			$colorids[$i]=$colors[$i]['iColorId'];
		   }
		}
		if(count($colorids)>0){
			$allProductColors=implode(",",$colorids);		
			$productColorImages=$this->instantquote_model->productColorImages($iProductId,$allProductColors)->result();
		}
		for($i=0;$i<count($productColorImages);$i++){
			$image=$_SERVER['DOCUMENT_ROOT'].'/php/emblemax//uploads/product/'.$iProductId.'/'.$productColorImages[$i]->iProductColorId.'/'.$productColorImages[$i]->vFrontImage;
			
			if (file_exists($image)){
				$productColorImages[$i]->msg='Image exists';
			}else{
				$productColorImages[$i]->msg='Image not found';
			}
		}
		//echo $productColorImages['msg'];
		//echo "<pre>";print_r($productColorImages);exit;
	       $rootCategoryId = 0;
	       $all_category_data = $this->instantquote_model->getProductData()->result_array();
	       //echo "<pre>";print_r($all_category_data);exit;
	       $alp = $_REQUEST['q'];
	     
	       $this->smarty->assign("alp",$alp);
	       $this->smarty->assign("all_category_data",$all_category_data);
	       
	       $shipments = $this->instantquote_model->getAllShippingMethods()->result_array();
		  
	       
	       $printLocations = $this->instantquote_model->getAllPrintLocations()->result_array();
	       $numberColors = $this->instantquote_model->getAllNumberOfColors()->result_array();
	       
		  
		  
		  //echo "<pre>";print_r($productColorImages);exit;
	       $this->smarty->assign('product',$product);
		  $this->smarty->assign('fileMsg',$fileMsg);		  
	       $this->smarty->assign('shipments',$shipments);
		  $this->smarty->assign('iProductId',$iProductId);		  
	       $this->smarty->assign("isActiveMenu",'instant-quote');
	       $this->smarty->assign('sizes',$sizes);
	       $this->smarty->assign('colors',$colors);
	       $this->smarty->assign('printLocations',$printLocations);
	       $this->smarty->assign('numberColors',$numberColors);
	       $this->smarty->assign('product_color_size',$product_color_size);
	       $this->smarty->assign('productColorArray',$productColorArray);
	       $this->smarty->assign('productSizeArray',$productSizeArray);
		  $this->smarty->assign('productColorImages',$productColorImages);	       
	       $this->smarty->view('instant_price_quote.tpl');
	  }else{
	       $this->smarty->view('404.tpl');
	  }
     }
     function calculate_quote(){    
	  
	  //echo "<pre>";print_r($_POST);print_r($_FILES);exit;
	  
	  $_SESSION['quote'] = array();
	  $_SESSION['quote']['iProductId'] = $_POST['iProductId'];
	  $shipmentId =  $_POST['shipment'];
	  //echo "<pre>";print_r($_FILES);exit;
	  
	  $iProductId = $_POST['iProductId'];
	  $colorSizeData = $_POST['color_size'];
	  
	  
	  
	  $product_color_size = $this->instantquote_model->getProductColorSizeRelation($iProductId)->result_array();	  
	  //orderqty-size-7-color-8
	  //echo "<pre>";print_r($product_color_size);exit;
	  $colorSizeCost = 0;
	  $totQty = 0;
	  $colorSizeDataFinal = array();
	  $colorselection=array();
	  $j=0;
	  for($i=0;$i<count($product_color_size);$i++){
	       if($colorSizeData['orderqty-size-'.$product_color_size[$i]['iSizeId'].'-color-'.$product_color_size[$i]['iColorId']] != ''){
		    $colorSizeDataFinal[$j]['iColorId'] = $product_color_size[$i]['iColorId'];
		    $colorSizeDataFinal[$j]['iSizeId'] = $product_color_size[$i]['iSizeId'];
		    $colorSizeDataFinal[$j]['fPrice'] = $product_color_size[$i]['fPrice'];
		    $colorSizeDataFinal[$j]['iQty'] = $colorSizeData['orderqty-size-'.$product_color_size[$i]['iSizeId'].'-color-'.$product_color_size[$i]['iColorId']];
		    $colorSizeCost += ($colorSizeDataFinal[$j]['iQty'] * $colorSizeDataFinal[$j]['fPrice']);
		    $totQty += $colorSizeDataFinal[$j]['iQty'];
		    $j++;    
		    
		    if($colorSizeData['orderqty-size-'.$product_color_size[$i]['iSizeId'].'-color-'.$product_color_size[$i]['iColorId']]=$_POST['color_size']['orderqty-size-'.$product_color_size[$i]['iSizeId'].'-color-'.$product_color_size[$i]['iColorId']]){
			$colorselection[$i]['iColorId']=$product_color_size[$i]['iColorId'];
			$colorselection[$i]['iSizeId']=$product_color_size[$i]['iSizeId'];
			$colorselection[$i]['fPrice']=$product_color_size[$i]['fPrice'];
			$colorselection[$i]['selectedQty']=$_POST['color_size']['orderqty-size-'.$product_color_size[$i]['iSizeId'].'-color-'.$product_color_size[$i]['iColorId']];
		    }	    
	       }
		}
	 
	  
	  
	  
	  
	  $product_printlocation_numbercolors = $this->instantquote_model->getProductPrintlocationNumberofColorsRelation($iProductId)->result_array();
	  //echo "<pre>";print_r($product_printlocation_numbercolors);exit;
	  
	  $decorationServiceCost = 0;
	  $printLocations = $_POST['print_location'];
	  $numberOfColorsTmp = $_POST['number_of_colors'];
	  $notesTmp =  $_POST['notes'];
	  
	  $printLocationIdsArray = array_keys($printLocations);
	  //echo "<pre>";print_r($printLocationIdsArray );exit;
	  $printLocationIds = array();
	  $numberOfColors = array();
	  $printlocation_numberofcolors = array();
	  $decorationType = $_POST['decoration_type']; 
	  
	  for($i=0;$i<count($printLocationIdsArray);$i++){
	       $tmp = explode('_',$printLocationIdsArray[$i]);
	       $printLocationIds[] = $tmp[1];
	       $numColors = $numberOfColorsTmp['numberofcolors_'.$tmp[1]];
	       $numArray = explode('-',$numColors);
	       $printlocation_numberofcolors[$i]['decoration_type'] = $decorationType[$numArray[1]];
	       $printlocation_numberofcolors[$i]['print_location'] = $numArray[1];
	       if($decorationType[$numArray[1]] == 'embroidery'){
		    $printlocation_numberofcolors[$i]['number_of_colors'] = 0;
	       }else{
		    $printlocation_numberofcolors[$i]['number_of_colors'] = $numArray[3];    
	       }
	  }
	  //echo "<pre>"; print_r($product_printlocation_numbercolors);exit;	  
	  $qtyRange = $this->instantquote_model->getAllQtyRanges()->result_array();	 
	  $iQtyRangeId = 0;
	  
	  for($i=0;$i<count($qtyRange);$i++){
	       if($totQty >= $qtyRange[$i]['iStartRange']){
		    $iQtyRangeId = $qtyRange[$i]['iQuantityRangeId'];
		    break;
	       }
	  }
	  //echo $iQtyRangeId;exit;
	  for($i=0;$i<count($product_printlocation_numbercolors);$i++){
	       for($j=0;$j<count($printlocation_numberofcolors);$j++){
		    if($product_printlocation_numbercolors[$i]['iPrintLocationId'] == $printlocation_numberofcolors[$j]['print_location']
		       && $product_printlocation_numbercolors[$i]['iNumberColorsId'] != 0
		       && $product_printlocation_numbercolors[$i]['iNumberColorsId'] == $printlocation_numberofcolors[$j]['number_of_colors']
		       && $product_printlocation_numbercolors[$i]['iQuantityRangeId'] == $iQtyRangeId){
			 
			 if($printlocation_numberofcolors[$j]['decoration_type'] == 'Screenprinting'){
			      $printlocation_numberofcolors[$j]['price'] = $product_printlocation_numbercolors[$i]['fPrice'];
			      $decorationServiceCost += $product_printlocation_numbercolors[$i]['fPrice']*$totQty;
			 }else{
			      $printlocation_numberofcolors[$j]['price'] = 0;
			 }
			 
		    }elseif($product_printlocation_numbercolors[$i]['iPrintLocationId'] == $printlocation_numberofcolors[$j]['print_location']
			    && $product_printlocation_numbercolors[$i]['iNumberColorsId'] == 0
			    && $product_printlocation_numbercolors[$i]['iQuantityRangeId'] == $iQtyRangeId){
			 
			 if($printlocation_numberofcolors[$j]['decoration_type'] == 'embroidery'){
			      $printlocation_numberofcolors[$j]['price'] = $product_printlocation_numbercolors[$i]['fPrice'];
			      $decorationServiceCost += $product_printlocation_numbercolors[$i]['fPrice']*$totQty;
			 }else{
			     $printlocation_numberofcolors[$j]['price'] = 0;
			 }
			 
		    }else{
			 
			 $printlocation_numberofcolors[$j]['price'] = 0;
			 
		    }
	       }
	  }
	  
	  
	  $printLocationIds=array();
	  for($i=0;$i<count($printlocation_numberofcolors);$i++){		
		if(!in_array($printlocation_numberofcolors[$i]['print_location'],$printLocationIds)){
			$printLocationIds[$i]=$printlocation_numberofcolors[$i]['print_location'];
		}
	  }
	  $printIds=implode(",",$printLocationIds);	  
	  
	  
	  
	  //echo '<br>colorSizeCost='.$colorSizeCost;
	  //echo '<br>decorationServiceCost='.$decorationServiceCost;
	  //exit;
	  $basicCost = $colorSizeCost + $decorationServiceCost;
	  
	  $shipmentDetail = $this->instantquote_model->getShipmentById($shipmentId)->result_array();
	  if($shipmentDetail[0]['eFeeCharge'] == '$'){
	       $shipmentCharge = $shipmentDetail[0]['fCharge'];
	  }elseif($shipmentDetail[0]['eFeeCharge'] == '%'){
	       $shipmentCharge = $basicCost * ($shipmentDetail[0]['fCharge']/100);
	  }else{
	       $shipmentCharge = 0;
	  }
	  
	  
	  $couponCode = $_POST['coupon_code'];
	  $couponDetail = $this->instantquote_model->getCouponDetailByCode($couponCode)->result_array();
	  if($couponDetail[0]['eType'] == '$'){
	       $couponDiscount = $couponDetail[0]['fDiscount'];
	  }elseif($couponDetail[0]['eType'] == '%'){
	       $couponDiscount = $basicCost * ($couponDetail[0]['fDiscount']/100);
	  }else{
	       $couponDiscount = 0;
	  }
	 
	  $totalQuote = number_format($basicCost + $shipmentCharge - $couponDiscount,2,'.','');
	  $subTotal = number_format($basicCost,2,'.','');
	  
	  //$costPerPiece= round($subTotal/$totQty);
	  $pieceiewiseCost= number_format(($subTotal/$totQty),2,'.','');	  
	  $costPerPiece=round($pieceiewiseCost).'.00';
	  
	  
	  
	  $shipmentCharge = number_format($shipmentCharge,2,'.','');
	  $couponDiscount = number_format($couponDiscount,2,'.','');
	  
	  
	  $_SESSION['quote']['tColorSizeData'] = $colorSizeDataFinal;	  
	  $_SESSION['quote']['tPrintlocationNumberofcolorsData'] = $printlocation_numberofcolors;
	  $_SESSION['quote']['iTurnAroundTimeId'] = $shipmentId;
	  $_SESSION['quote']['fTurnAroundTimeCharge'] = $shipmentCharge;
	  $_SESSION['quote']['vCouponCode'] = $couponCode;
	  $_SESSION['quote']['fCouponDiscount'] = $couponDiscount;
	  $_SESSION['quote']['fSubTotal'] = $subTotal;
	  $_SESSION['quote']['fGrandTotal'] = $totalQuote;
	  
	  //Quote Summary
	  $quoteSummary = array();
	  $product = $this->instantquote_model->getProductById($iProductId)->row_array();
	  $quoteSummary['vProductName'] = $product['vProductName'];
	  $quoteSummary['totalQty'] = 0;
	  $tColorSizeData = $colorSizeDataFinal;
	  $vColorSizeDescription = array();
	 //echo "<pre>";print_r($tColorSizeData);exit;
	  for($i=0;$i<count($tColorSizeData);$i++){
	       //Qty 10 of XXL size in Blue color
	       $color = $this->instantquote_model->getColorById($tColorSizeData[$i]['iColorId'])->row_array();
	       $size = $this->instantquote_model->getSizeById($tColorSizeData[$i]['iSizeId'])->row_array();		  
	       $quoteSummary['vColorSizeDescription'][] = "Qty ".$tColorSizeData[$i]['iQty']." size ".$size['vSize']." in ".$color['vColor'];
	       $quoteSummary['totalQty'] += $tColorSizeData[$i]['iQty'];
	  }
	  //echo "<pre>";print_r($quoteSummary );exit;
	  $tPrintlocationNumberofcolorsData = $printlocation_numberofcolors;
	  //echo "<pre>";print_r($tPrintlocationNumberofcolorsData );exit;
	  for($i=0;$i<count($tPrintlocationNumberofcolorsData);$i++){
	       //screenprinting on Left chest
	       $printlocation = $this->instantquote_model->getPrintLocationById($tPrintlocationNumberofcolorsData[$i]['print_location'])->row_array();
	       if($tPrintlocationNumberofcolorsData[$i]['decoration_type'] == 'screenprinting'){
		    $number_of_colors = $this->instantquote_model->getNumberOfColorsById($tPrintlocationNumberofcolorsData[$i]['number_of_colors'])->row_array();
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'].' ( '.$number_of_colors['vNumberColor'].' )';    
	       }else{
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'];
	       }
	  }
	  
	  
	  /*   
	   next 3 break dowan qtyies coding statrt   
	   */
	  
	  $iProductId=$_POST['iProductId'];	  
	  $totalQty=$quoteSummary['totalQty'];	  
	  $geQuantityRange=$this->instantquote_model->geQuantityRange($totalQty)->result();
	  
	  
	  $rangeIds=array();
	  for($i=0;$i<count($geQuantityRange);$i++){
		$rangeIds[$i]['iStartRange']=$geQuantityRange[$i]->iStartRange;
		$rangeIds[$i]['iEndRange']=$geQuantityRange[$i]->iEndRange;	
		$rangeIds[$i]['iQuantityRangeId']=$geQuantityRange[$i]->iQuantityRangeId;	
	  }
	  
	  $sortColorArray=$this->sortingArray($colorselection, array('iColorId','iSizeId'));
	  
	  $decorationprice=array();
	  for($i=0;$i<count($rangeIds);$i++){
		
		for($j=0;$j<count($tPrintlocationNumberofcolorsData);$j++){			
			$range=$rangeIds[$i]['iQuantityRangeId'];			
			$noOfColor=$tPrintlocationNumberofcolorsData[$j]['number_of_colors'];			
			$printLocation=$tPrintlocationNumberofcolorsData[$j]['print_location'];
			//echo "<hr>";
			$getPrice[]=$this->instantquote_model->getPriceDetail($range,$printLocation,$noOfColor,$iProductId)->result();
			//echo "</hr>";
		}
	 }
	 //echo "<pre>";print_r($tPrintlocationNumberofcolorsData);exit;
	 $iQuantityRangeIds=array();
	 for($i=0;$i<count($getPrice);$i++){		
		if(!in_array($getPrice[$i][0]->iQuantityRangeId,$iQuantityRangeIds) && $getPrice[$i][0]->iQuantityRangeId!='' ){
		$iQuantityRangeIds[]=$getPrice[$i][0]->iQuantityRangeId;
	   }
	   $decorationPrice[$getPrice[$i][0]->iQuantityRangeId]+=$getPrice[$i][0]->fPrice;
	 }
	 
	 
	 $finalDecorationprice=array();	 
	 for($i=0;$i<count($iQuantityRangeIds);$i++){
		$finalDecorationprice[$i]['iQuantityRangeId']=$iQuantityRangeIds[$i];
		$finalDecorationprice[$i]['iDecorationprice']=$decorationPrice[$iQuantityRangeIds[$i]];		
	 }
	 
	 $decodrationPrice=array();
	 
	 for($i=0;$i<count($finalDecorationprice);$i++){
		if(!in_array($finalDecorationprice[$i]['iQuantityRangeId'],$decodrationPrice)){
			$decodrationPrice[$finalDecorationprice[$i]['iQuantityRangeId']]=$finalDecorationprice[$i]['iDecorationprice'];			
		}
	  }
	  
	  
	  $sortDecoration=$this->sortingArray($finalDecorationprice, array('iQuantityRangeId','iDecorationprice'));
	  
	  $sortQtyRange=$this->sortingArray($rangeIds, array('iQuantityRangeId','iStartRange'));
	  //echo "<pre>";print_r($sortDecoration);
	  //echo "<pre>";print_r($sortQtyRange);exit;
	  for($i=0;$i<count($sortQtyRange);$i++){
		if($sortQtyRange[$i]['iQuantityRangeId']==$sortDecoration[$i]['iQuantityRangeId']){		
			$sortQtyRange[$i]['DecorationPrice']=$sortDecoration[$i]['iDecorationprice'];		
		}else{
			$sortQtyRange[$i]['DecorationPrice']=0;
		}
	  }	  
	  
	  
	  
	  $price=$sortColorArray[0]['fPrice'];
	  
	  for($j=0;$j<count($sortQtyRange);$j++){
		$sortQtyRange[$j]['ColorPrice']=$price;
		$sortQtyRange[$j]['PricePlusDecoration']=($sortQtyRange[$j]['DecorationPrice'])+($price);
		$sortQtyRange[$j]['Total']=($sortQtyRange[$j]['iStartRange']*$sortQtyRange[$j]['PricePlusDecoration']);
		$sortQtyRange[$j]['Each']=($sortQtyRange[$j]['iStartRange']*$sortQtyRange[$j]['PricePlusDecoration']) / $sortQtyRange[$j]['iStartRange'];
		
	  }	 
	  //echo "<pre>";print_r($sortQtyRange);exit;
	  //echo "<pre>";print_r($sortDecoration);exit;	  
	  //echo "<pre>";print_r($sortColorArray);exit;
	  
	  
	  $_SESSION['quoteSummary']=$quoteSummary;
	  //echo "<pre>";print_r($quoteSummary);exit;
	  $this->smarty->assign('subTotal',$subTotal);
	  $this->smarty->assign('costPerPiece',$costPerPiece);	  
	  $this->smarty->assign('shipmentCharge',$shipmentCharge);
	  $this->smarty->assign('couponDiscount',$couponDiscount);
	  $this->smarty->assign('totalQuote',$totalQuote);
	  $this->smarty->assign('sortQtyRange',$sortQtyRange);
	  
	  
	  
	  $this->smarty->assign('quoteSummary',$quoteSummary);
	  $this->smarty->view('ajax_quote.tpl');
     }
     function uploadartworkAndProceedtocart(){
	  
     }
     
     function saveQuote(){  
	  $userEmail=$_REQUEST['email'];
	  //echo $userEmail;	  
	  $dataQuote = array();
	  $dataQuote['iProductId'] = $_SESSION['quote']['iProductId'];
	  $dataQuote['tColorSizeData'] = json_encode($_SESSION['quote']['tColorSizeData']);
	  $dataQuote['tPrintlocationNumberofcolorsData'] = json_encode($_SESSION['quote']['tPrintlocationNumberofcolorsData']);
	  $dataQuote['iTurnAroundTimeId'] = $_SESSION['quote']['iTurnAroundTimeId'];
	  $dataQuote['fTurnAroundTimeCharge'] = $_SESSION['quote']['fTurnAroundTimeCharge'];
	  $dataQuote['vCouponCode'] = $_SESSION['quote']['vCouponCode'];
	  $dataQuote['fCouponDiscount'] = $_SESSION['quote']['fCouponDiscount'];
	  $dataQuote['fSubTotal'] = $_SESSION['quote']['fSubTotal'];
	  $dataQuote['fGrandTotal'] = $_SESSION['quote']['fGrandTotal'];
	  $iQuoteId = $this->instantquote_model->saveQuote($dataQuote);	  
	  $product = $this->instantquote_model->getProductById($dataQuote['iProductId'])->row_array();
	  $productName=$product['vProductName'];
	  
	  
	  $totalColorSizes=array();
	  $totalColorSizes=$_SESSION['quoteSummary']['vColorSizeDescription'];
	  $totalQty=$_SESSION['quoteSummary']['totalQty'];  
	  
	  
	  $printlocationNumberofcolorsData=array();
	  $tPrintlocationNumberofcolorsData=$_SESSION['quote']['tPrintlocationNumberofcolorsData'];	  
	  for($i=0;$i<count($tPrintlocationNumberofcolorsData);$i++){
	       //screenprinting on Left chest
	       $printlocation = $this->instantquote_model->getPrintLocationById($tPrintlocationNumberofcolorsData[$i]['print_location'])->row_array();
	       if($tPrintlocationNumberofcolorsData[$i]['decoration_type'] == 'screenprinting'){
		    $number_of_colors = $this->instantquote_model->getNumberOfColorsById($tPrintlocationNumberofcolorsData[$i]['number_of_colors'])->row_array();
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'].' ( '.$number_of_colors['vNumberColor'].' )';    
	       }else{
		    $quoteSummary['vPrintLocationsDescription'][] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'];
	       }
	  }  
	  
	  //echo count($quoteSummary['vPrintLocationsDescription']);
	  //echo "<pre>";print_r($quoteSummary);//exit;
	  
	  
	  $subTotal=$_SESSION['quote']['fSubTotal'];
	  $shipmentCharge=$_SESSION['quote']['fTurnAroundTimeCharge'];
	  $couponCode=$_SESSION['quote']['vCouponCode'];
	  $couponDiscount=$_SESSION['quote']['fCouponDiscount'];
	  $grandTotal=$_SESSION['quote']['fGrandTotal'];
	  $costPerPiece=round($subTotal/$totalQty);	  
		$table='<tr style="float: left; width: 30%;margin-top: 23px;">
			    <td style="float:left; width:57%">Product name:</td>
			    <td style="float:right;">'.$productName.'</td>
			</tr>';			
		$table.='<tr style="float: left; width: 98%">
				<td style="float:left; width:43%">Size & Color:</td>
			     ';
			for($i=0;$i<count($totalColorSizes);$i++){
				$table.='<td style="float:right; text-align:left;width:86%;">'.$totalColorSizes[$i].'</td></tr>';						
			}		
			$table.='<tr style="float: left; width: 98%">
				   <td style="float:left; width:43%">Print Locations :<td>';				
			for($i=0;$i<=count($quoteSummary['vPrintLocationsDescription']);$i++){
				$table.='<td style="float:right; text-align:left;width:86%;">'.$quoteSummary['vPrintLocationsDescription'][$i].'</td></tr>';
			}
			$table.='<tr style="float: left; width: 98%; margin-top:20px;"><td style="float: left; width:18%">Total Qty :</td>
			         <td>&nbsp;</td>';
			$table.='<td>'.$totalQty.'<br></td></tr>';
			
		$costTable='			
			<tr style="float: left; width: 98%">
			    <td class="quote_subtotal" style="float: left; width:18%">Cost Per Piece: </td><td class="quote_subtotal">$'.$costPerPiece.'.00</td>
			</tr>
			<tr style="float: left; width: 98%">
				<td class="quote_subtotal" style="float: left; width:18%">Sub Total : </td><td class="quote_subtotal">$'.$subTotal.'</td>
			</tr>
			<tr style="float: left; width: 98%">
				<td class="quote_subtotal" style="float: left; width:18%">Shipment Charge : </td><td class="quote_subtotal">$'.$shipmentCharge.'</td>
			</tr>
			<tr style="float: left; width: 98%">
				<td class="quote_subtotal" style="float: left; width:18%">Coupon Discount : </td><td class="quote_subtotal">$'.$couponDiscount.'</td>
			</tr>
			<tr style="float: left; width: 98%">
				<td class="quote_subtotal" style="float: left; width:18%">Coupon Discount : </td><td class="quote_subtotal">$'.$couponDiscount.'</td>
			</tr>
			<tr style="float: left; width: 98%">
				<td class="quote_subtotal" style="float: left; width:18%">Total Quote : </td><td class="quote_subtotal">$'.$grandTotal.'</td>
			</tr>'; 
	  $site_url = $this->config->item('site_url');
	  $this->smarty->assign("site_url",$site_url);
	  $MailFooter = $this->getConfigData('MAIL_FOOTER');
	  $siteName = $this->getConfigData('SITE_NAME');  
	  $Email=$userEmail;
	  $bodyArr = array("#EMAIL#","#COLORDETAIL#","#PRICEDETAIL#","#SITE_URL#","#MAIL_FOOTER#","#SITE_NAME#");
	  $postArr = array($userEmail,$table,$costTable,$site_url,$MailFooter,$siteName);	  
	  $sendMail=$this->Send("USER_QUOTE","Member",$Email,$bodyArr,$postArr);	  
	  if($sendMail){
	    echo "Mail Sent";      
	  }else{
	    echo "Fail";      
	  }	  
	  exit; 
	  
	  
	  
	
	  
     }
     function addtocart(){	   
	  $dataQuote = array();
	  $dataQuote['iProductId'] = $_SESSION['quote']['iProductId'];
	  $dataQuote['tColorSizeData'] = json_encode($_SESSION['quote']['tColorSizeData']);
	  $dataQuote['tPrintlocationNumberofcolorsData'] = json_encode($_SESSION['quote']['tPrintlocationNumberofcolorsData']);
	  $dataQuote['iTurnAroundTimeId'] = $_SESSION['quote']['iTurnAroundTimeId'];
	  $dataQuote['fTurnAroundTimeCharge'] = $_SESSION['quote']['fTurnAroundTimeCharge'];
	  $dataQuote['vCouponCode'] = $_SESSION['quote']['vCouponCode'];
	  $dataQuote['fCouponDiscount'] = $_SESSION['quote']['fCouponDiscount'];
	  $dataQuote['fSubTotal'] = $_SESSION['quote']['fSubTotal'];
	  $dataQuote['fGrandTotal'] = $_SESSION['quote']['fGrandTotal'];	  
	  $iQuoteId = $this->instantquote_model->saveQuote($dataQuote);	  
	  $_SESSION['iQuoteId']=$iQuoteId ;	  
	  $cartItems = count($_SESSION['cart']['item']);
	  //Product Detail
	  $_SESSION['cart']['item'][$cartItems]['iQuoteId'] = $iQuoteId;	  
	  $_SESSION['cart']['item'][$cartItems]['iProductId'] = $_SESSION['quote']['iProductId'];
	  $product = $this->instantquote_model->getProductById($_SESSION['quote']['iProductId'])->result_array();
	  $_SESSION['cart']['item'][$cartItems]['vProductName'] = $product[0]['vProductName'];
	  $_SESSION['cart']['item'][$cartItems]['vImage'] = $product[0]['vImage'];
	  $_SESSION['cart']['item'][$cartItems]['tColorSizeData'] = $_SESSION['quote']['tColorSizeData'];
	  $_SESSION['cart']['item'][$cartItems]['tPrintlocationNumberofcolorsData'] = $_SESSION['quote']['tPrintlocationNumberofcolorsData'];
	  
	  
	  //Selected Color and Size
	  $tColorSizeData = $_SESSION['cart']['item'][$cartItems]['tColorSizeData'];
	  $vColorSizeDescription = array();
	  for($i=0;$i<count($tColorSizeData);$i++){
	       //Qty 10 of XXL size in Blue color
	       $color = $this->instantquote_model->getColorById($tColorSizeData[$i]['iColorId'])->row_array();
	       $size = $this->instantquote_model->getSizeById($tColorSizeData[$i]['iSizeId'])->row_array();
	       $vColorSizeDescription[] = "Qty ".$tColorSizeData[$i]['iQty']." of ".$size['vSize']." size in ".$color['vColor']." color";
	  }
	  $_SESSION['cart']['item'][$cartItems]['vColorSizeDescription']= $vColorSizeDescription;	  
	  //Selected Print Locations	  
	  $tPrintlocationNumberofcolorsData = $_SESSION['cart']['item'][$cartItems]['tPrintlocationNumberofcolorsData'];
	  $vPrintLocationsDescription = array();
	  for($i=0;$i<count($tPrintlocationNumberofcolorsData);$i++){
	       //screenprinting on Left chest
	       $printlocation = $this->instantquote_model->getPrintLocationById($tPrintlocationNumberofcolorsData[$i]['print_location'])->row_array();
	       $vPrintLocationsDescription[] = $tPrintlocationNumberofcolorsData[$i]['decoration_type']." on ".$printlocation['vPrintLocation'];
	  }
	  $_SESSION['cart']['item'][$cartItems]['vPrintLocationsDescription']= $vPrintLocationsDescription;  
	  //Qty
	  $totalQty = 0;
	  for($i=0;$i<count($_SESSION['cart']['item'][$cartItems]['tColorSizeData']);$i++){
	       $totalQty += $_SESSION['cart']['item'][$cartItems]['tColorSizeData'][$i]['iQty'];
	  }
	  $_SESSION['cart']['item'][$cartItems]['vTotalQty'] = $totalQty;	  
	  //Total
	  $_SESSION['cart']['item'][$cartItems]['fQuoteTotal'] = $_SESSION['quote']['fSubTotal'];	  
	  $_SESSION['cart']['SUBTOTAL'] = 0;
	  for($i=0;$i<count($_SESSION['cart']['item']);$i++){
	       $_SESSION['cart']['SUBTOTAL'] += $_SESSION['cart']['item'][$i]['fQuoteTotal'];
	  }
	  
	  //Turn Around Time
	  $turnAroundTime = $this->instantquote_model->getTurnAroundTimeById($_SESSION['quote']['iTurnAroundTimeId'])->result_array();
	  $_SESSION['cart']['vTurnAroundTime'] = $turnAroundTime[0]['vTitle'];
	  $_SESSION['cart']['iTurnAroundTimeId'] = $_SESSION['quote']['iTurnAroundTimeId'];
	  if($turnAroundTime[0]['eFeeCharge'] == '$'){
	       $_SESSION['cart']['fTurnAroundTimeCharge'] = $turnAroundTime[0]['fCharge'];
	  }elseif($turnAroundTime[0]['eFeeCharge'] == '%'){
	       $_SESSION['cart']['fTurnAroundTimeCharge'] = $_SESSION['cart']['SUBTOTAL'] * ($turnAroundTime[0]['fCharge']/100);
	  }else{
	       $_SESSION['cart']['fTurnAroundTimeCharge'] = 0;
	  }
	  
	  //Coupon Discount
	  if(isset($_SESSION['quote']['vCouponCode']) && $_SESSION['quote']['vCouponCode'] != ''){
	       $_SESSION['cart']['vCouponCode'] = $_SESSION['quote']['vCouponCode'];       
	  }
	  if(isset($_SESSION['cart']['vCouponCode']) && $_SESSION['cart']['vCouponCode'] != ''){
	       $couponCode = $_SESSION['cart']['vCouponCode'];
	       $couponDetail = $this->instantquote_model->getCouponDetailByCode($couponCode)->result_array();
	       if($couponDetail[0]['eType'] == '$'){
		    $_SESSION['cart']['fCouponDiscount'] = $couponDetail[0]['fDiscount'];
	       }elseif($couponDetail[0]['eType'] == '%'){
		    $_SESSION['cart']['fCouponDiscount'] = $_SESSION['cart']['SUBTOTAL'] * ($couponDetail[0]['fDiscount']/100);
	       }else{
		    $_SESSION['cart']['fCouponDiscount'] = 0;
	       }     
	  }else{
	       $_SESSION['cart']['fCouponDiscount'] = 0;
	  }
	  
	  //GrandTotal Calculation
	  $_SESSION['cart']['fTurnAroundTimeCharge'] = number_format($_SESSION['cart']['fTurnAroundTimeCharge'],2,'.','');
	  $_SESSION['cart']['fCouponDiscount'] = number_format($_SESSION['cart']['fCouponDiscount'],2,'.','');
	  $_SESSION['cart']['SUBTOTAL'] = number_format($_SESSION['cart']['SUBTOTAL'],2,'.','');
	  $_SESSION['cart']['GRANDTOTAL'] = number_format($_SESSION['cart']['SUBTOTAL'] - $_SESSION['cart']['fCouponDiscount'] + $_SESSION['cart']['fTurnAroundTimeCharge'],2,'.','');
    
	  
	  unset($_SESSION['quote']);
	  redirect(site_url.'cart');
     }
     
	function ajaxpage(){
	 /*
	     echo "<pre>";
	     print_r($_REQUEST);
	     exit;
	 */
	     $iCategoryId = $_REQUEST['iCategoryId'];
	     if(isset($_REQUEST['iCategoryId']) != ''){
		  $totalData =  $this->instantquote_model->getProductByCategoryId($iCategoryId)->num_rows;
	     }else{
		  $totalData =  $this->instantquote_model->getCategory($rootCategoryId)->num_rows;
	     }
	     
	     if(isset($_REQUEST['start'])){
		  $start = $_REQUEST['start'];
	     }else{
		  $start = 0;
	     }
	     
	     $this->rec_limit = $this->limit;
	     $rec_limit = $this->rec_limit;
	     $var_limit = "";
	     //echo $this->rec_limit;exit;
	     if($start != 0){
		  $num_limit = ($start-1)*$rec_limit;
		  $var_limit = " LIMIT $num_limit, $rec_limit";
	     }else{
		  $var_limit = " LIMIT 0, $rec_limit";
		  $start = 1;
	     }
	     $site_path = $this->config->item('site_path');	     
	     include($site_path."system/libraries/class.paging-ajax.php");
	     
	     if(isset($_REQUEST['iCategoryId']) != ''){
		  $this->totRec = $totalData;
		  $PagingObj = new Paging($totalData,$start,'changePaginationProduct',$rec_limit);
		  
		  //$recmsg = $PagingObj->setMessage('Product');
		  $pages = $PagingObj->displayPaging();
		  $iCategoryId = $_REQUEST['iCategoryId'];
		  
		  //$this->smarty->assign("recmsg",$recmsg);
		  $this->smarty->assign("pages",$pages);
		  
		  $products = $this->instantquote_model->getProductByCategoryId($iCategoryId,$var_limit)->result_array();
		  
		  $this->smarty->assign("iCategoryId",$iCategoryId);
		  $this->smarty->assign("products",$products);
		  $this->smarty->view('ajax-product.tpl');
	     }else{
		  if(isset($_REQUEST['q']) != ''){
		      $alp = $_REQUEST['q'];
		      $where = "WHERE product_category.vUrlText LIKE ";
		      $vCategoryUrlText = $where.'"%'.$alp.'%"';
		      $category = $this->instantquote_model->getCategoryByUrlText($vCategoryUrlText)->result_array();
		      $all_category_data = $this->instantquote_model->getCategoryByUrlText($vCategoryUrlText,$var_limit)->result_array();
		      $totalData = count($category);
		  }else{
		      $rootCategoryId = 0;
		      $all_category_data = $this->category_model->getCategory($rootCategoryId,$var_limit)->result_array();
		  }
		  
		  $this->totRec = $totalData;
		  $PagingObj = new Paging($totalData,$start,'changePagination',$rec_limit);
		  
		  //$recmsg = $PagingObj->setMessage('Category');
		  $pages = $PagingObj->displayPaging();
		  
		  //$this->smarty->assign("recmsg",$recmsg);
		  $this->smarty->assign("pages",$pages);
		  
		  $rootCategoryId = 0;
		  $all_category_data = $this->instantquote_model->getCategory($rootCategoryId,$var_limit)->result_array();
		  
		  $this->smarty->assign("all_category_data",$all_category_data);
		  $this->smarty->assign("pages",$pages);
		  $this->smarty->assign("alp",$alp);
		  $this->smarty->view('ajax-catagory.tpl');
	     }
	     
	}
	
     function ajax_categaorydata(){
	  $iCategoryId = $_GET['iCategoryId'];
	  $all_category_data = $this->instantquote_model->productByCategoryId($iCategoryId)->result_array();
	  //echo "<pre>";print_r($all_category_data);exit;
	  $front_js_path = $this->config->item('front_js_path');
	  $this->smarty->assign("front_js_path",$front_js_path);	
	  $this->smarty->assign("all_category_data",$all_category_data);
	  $this->smarty->view('ajax_product.tpl');
     }
     function do_upload_artwork($randomId,$vFile){
	  if(!is_dir('uploads/artwork/')){
	       @mkdir('uploads/artwork/', 0777);
	  }
	  $config = array(
	       'allowed_types' => "jpg|jpeg|gif|png",
	       'upload_path' => 'uploads/artwork',
	       'max_size'=>2000,
	       'file_name'=>$randomId
	  );
	  $this->upload->initialize($config);
	  $this->upload->do_upload($vFile); //do upload
	  $image_data = $this->upload->data(); //get image data
	  $img_uploaded = $image_data['file_name'];
	  return $img_uploaded;
     }
     
     function uploadArtwork(){
	  $printLocation = $_SESSION['quote']['tPrintlocationNumberofcolorsData'];
	  
	  //echo "<pre>";print_r($printLocation );exit;
	  $artworkData = array();
	  $cnt = 0;
	  for($i=0;$i<count($printLocation);$i++){
	       if(!$_FILES['artwork_img-'.$printLocation[$i]['print_location']]['error']){
		    $randomId = strtotime(date("Y-m-d H:i:s"));
		    $vArtworkImage = $this->do_upload_artwork($randomId,'artwork_img-'.$printLocation[$i]['print_location']);
		    $artworkData[$cnt]['vArtworkImage'] = $vArtworkImage;
		    $artworkData[$cnt]['tComment'] = $_POST['artwork_comment'][$printLocation[$i]['print_location']];
		    $cnt++;
	       }
	  }
	  $_SESSION['quote']['artworkData'] = $artworkData;
     }
     public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr){
	  
	    //echo $EmailCode."<br>";
	    //echo $SendType."<br>";
	    //echo $ToEmail."<br>";exit;
	    //echo "<pre>";print_r($bodyArr);
		//echo "<pre>";print_r($postArr);exit;
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";		
		$email_info = $this->instantquote_model->list_sysemaildata($ssql)->result();
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: emblemax.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
				
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		$this->body = str_replace($bodyArr,$postArr, $this->body);
		$To = stripcslashes($ToEmail);
		$htmlMail = '	
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		    <html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>Emblemax</title>
		    </head>
		    
		    <body style="padding:0; margin:0; border:0;">
			 <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			      <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
				   <div style="background:#363F49; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt="" height="50px"/></div>
					'.$this->body.'
				   </div>
			      </div>
			 </div>
		    </body>
	       </html>';
	       
		//echo $htmlMail;exit;
	       if($_SERVER['SERVER_ADDR'] == '192.168.1.41'){ // for localhost server
			 
			 require_once "Mail.php";
			 require_once "Mail/mime.php";
			 $from = "demo3.testing3@gmail.com";			   
			 //echo $from;exit;
			 $to =$To;
			 $subject = "EmbleMax - Thank your for your order request";
			 $crlf = "\n";
			 $html = "<h1> This is HTML </h1>";
			 $headers = array('From' => $from,'To' => $to,'Subject' => $subject);
			 $host = "smtp.gmail.com";
			 $username = "demo2.testing2@gmail.com";
			 $password = "demo1234";
			 $mime =  new Mail_mime(array('eol' => $crlf));
			 $mime->setHTMLBody($htmlMail);			
			 $body = $mime->getMessageBody();			
			 $headers = $mime->headers($headers);
			 
			 $smtp = Mail::factory("smtp",array("host" => $host,"auth" => true,"username" => $username,"password" => $password));			 
			 $res = $smtp->send($to, $headers, $body);			 
		}else
		{ // for live server
			 //echo "else";exit;
			 $res = mail($To,$Subject,$htmlMail,$headers);
		}
	     return $res; 
		//redirect(site_url.'myAccount/myDashboard');
		//redirect(site_url.'registration/registration_success');
		//return $var_msg;
     }
     function ajax_printlocation(){
		
		$iProductId=$_REQUEST['iProductId'];		
		$totalQuantity=$_REQUEST['fianlTotalQty'];		
		
		$printLocationId=stripslashes($_REQUEST['json']);
		$printIds=json_decode($printLocationId);
		$finalPrintLocationIds=implode(",",$printIds);
		
		$getMinimumQunaityRange=$this->instantquote_model->getMinimumQunaityRange()->row();		
		$minimumRange=$getMinimumQunaityRange->MinimumRange;
		
		$start='';
		if($totalQuantity>$minimumRange){						
			$getQuantityRange=$this->instantquote_model->getQuantityRange($totalQuantity)->result();			
			for($i=0;$i<count($getQuantityRange);$i++){				
				$startRange=$getQuantityRange[$i]->iStartRange;
				$endRange=$getQuantityRange[$i]->iEndRange;				
				if($totalQuantity>=$startRange && $totalQuantity<=$endRange){					
				 $quantityRange=$getQuantityRange[$i]->vQuantityRange;
				 $iQuantityRangeId=$getQuantityRange[$i]->iQuantityRangeId;
				 $iShowField=$getQuantityRange[$i]->iShowField;			 
				}				
			}			
			$getEmbrodirayValue=$this->instantquote_model->getEmbrodary($iProductId,$iQuantityRangeId)->result_array();		
			$totEmbrodary=count($getEmbrodirayValue);
			if($totEmbrodary>0){
				$start=1;
			}else{
				$start=0;
			}			
			$colors= explode("|",$iShowField);			
			$finalColors=implode(",",$colors);			
			$printLocations = $this->instantquote_model->getPrintLocations($finalPrintLocationIds)->result_array();
			$numberColors = $this->instantquote_model->getNumberOfColors($finalColors)->result_array();	
			
		}else if($totalQuantity=$minimumRange){
			$getQuantityRange=$this->instantquote_model->getQuantityRange($totalQuantity)->result();			
			for($i=0;$i<count($getQuantityRange);$i++){				
				$startRange=$getQuantityRange[$i]->iStartRange;
				$endRange=$getQuantityRange[$i]->iEndRange;				
				if($totalQuantity>=$startRange && $totalQuantity<=$endRange){					
				 $quantityRange=$getQuantityRange[$i]->vQuantityRange;
				 $iQuantityRangeId=$getQuantityRange[$i]->iQuantityRangeId;
				 $iShowField=$getQuantityRange[$i]->iShowField;			 
				}				
			}			
			$getEmbrodirayValue=$this->instantquote_model->getEmbrodary($iProductId,$iQuantityRangeId)->result_array();		
			$totEmbrodary=count($getEmbrodirayValue);
			if($totEmbrodary>0){
				$start=1;
			}else{
				$start=0;
			}			
			$colors= explode("|",$iShowField);			
			$finalColors=implode(",",$colors);			
			$printLocations = $this->instantquote_model->getPrintLocations($finalPrintLocationIds)->result_array();
			$numberColors = $this->instantquote_model->getNumberOfColors($finalColors)->result_array();		
		}		
		else{
			$start=1;
			$printLocations = $this->instantquote_model->getPrintLocations($finalPrintLocationIds)->result_array();
			$numberColors = $this->instantquote_model->getAllNumberOfColors()->result_array();		
		}
		/*$checFronntNumer=$printLocations;
		for($i=0;$i<count($checFronntNumer);$i++){		
			if(($checFronntNumer[$i]['vPrintLocation']=='Number Front') || ($checFronntNumer[$i]['vPrintLocation']=='Number Back') || ($checFronntNumer[$i]['vPrintLocation']=='Custom Name Back')){			
				$numberFront='Yes';			
			}else{			
				$numberFront='No';
			}		
		}
		
		//echo "<pre>";print_r($printLocations);exit;
		$this->smarty->assign('numberFront',$numberFront);*/
		$this->smarty->assign('printLocations',$printLocations);
		$this->smarty->assign('totEmbrodary',$totEmbrodary);		
		$this->smarty->assign('numberColors',$numberColors );
		$this->smarty->assign('start',$start );		
		$this->smarty->view('ajax_printlocationid.tpl');		
	}
	
	function sortingArray($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
		    if (!empty($key)) {
			   $mapping = array();
			   foreach ($array as $k => $v) {
				  $sort_key = '';
				  if (!is_array($key)) {
					 $sort_key = $v[$key];
				  } else {
					 // @TODO This should be fixed, now it will be sorted as string
					 foreach ($key as $key_key) {
						$sort_key .= $v[$key_key];
					 }
					 $sort_flags = SORT_STRING;
				  }
				  $mapping[$k] = $sort_key;
			   }
			   asort($mapping, $sort_flags);
			   $sorted = array();
			   foreach ($mapping as $k => $v) {
				  $sorted[] = $array[$k];
			   }
			   return $sorted;
		    }
		}
		return $array;
	}
	/*
	 Cretaed  By:Nikhil Detroja
	 Cretaed  Date:15-11-2013 
	 */
	
	function checkPrintLocation(){		
		$printLocationId=stripslashes($_REQUEST['json']);
		$convertToarray=json_decode($printLocationId);		
		if(count($convertToarray)>0){
			$printLcoationIds=implode(",",$convertToarray);	
			$checFronntNumer=$this->instantquote_model->printLocationName($printLcoationIds)->result_array();
			
			$allThreePositions=array();
			for($i=0;$i<count($checFronntNumer);$i++){				
				if(($checFronntNumer[$i]['vPrintLocation']=='Number Front') || ($checFronntNumer[$i]['vPrintLocation']=='Number Back') || ($checFronntNumer[$i]['vPrintLocation']=='Custom Name Back') ){
					if(!in_array($checFronntNumer[$i]['vPrintLocation'],$allThreePositions)){
					 $allThreePositions[$i]=$checFronntNumer[$i]['vPrintLocation'];					
					}
				}
			}
			$convertoString=implode(",",$allThreePositions);		
			if($convertoString=='Number Front,Number Back,Custom Name Back'){  // if all three options will be selected
				$numberFront=$convertoString;
			}else if($convertoString=='Number Front,Number Back'){    		// if three of two options will be selected
				$numberFront=$convertoString;
			}else if($convertoString=='Number Front,Custom Name Back'){
				$numberFront=$convertoString;			
			}else if($convertoString=='Number Back,Custom Name Back'){
				$numberFront=$convertoString;
			}else{											  // if only one options will be selected
				for($i=0;$i<count($checFronntNumer);$i++){		
					if(($checFronntNumer[$i]['vPrintLocation']=='Number Front')){
						$numberFront='NumberFront';
						$cnt++;
					}else if(($checFronntNumer[$i]['vPrintLocation']=='Number Back')){					 
						$numberFront='NumberBack';
						$cnt++;
					}else if($checFronntNumer[$i]['vPrintLocation']=='Custom Name Back'){
						$numberFront='CustomNameBack';
						$cnt++;			
					}
					else{			
						$numberFront='No';
					}
				}
			}
		}else{
			$numberFront='No';
		}
		echo $numberFront;exit;
	}
}