<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends MY_Controller {

	
     function __construct() {
	    parent::__construct();
	    $this -> load -> model('registration_model', '', TRUE);
	    $this -> load -> helper('url');
	    $site_url = $this->config->item('site_url');
	    $front_css_path = $this->config->item('front_css_path');
	    $front_js_path = $this->config->item('front_js_path');
	    $this->smarty->assign("front_css_path",$front_css_path);
	    $this->smarty->assign("front_js_path",$front_js_path);
	    
	    $fancybox_path = $this->config->item('fancybox_path');
	      $this->smarty->assign("fancybox_path",$fancybox_path);
	    
	    $front_image_path = $this->config->item('front_image_path');
	    $this->smarty->assign("front_image_path",$front_image_path);
	    $this->smarty->assign("site_url",$site_url);
	    $upload_path = $this->config->item('upload_path');
	    $this->smarty->assign("upload_path",$upload_path);
	}
     
       function index()
       {	  
	  $db_country = $this->registration_model->list_country()->result();
	  $this->smarty->assign("db_country",$db_country);	  
	  $this->smarty->view('register.tpl');
       }  
     
       function add()
       {
		
	  $upload_path = $this->config->item('upload_path');
	  $this->smarty->assign("upload_path",$upload_path);

		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'user/add';
		
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);
		
		if($_POST)
		{
			
			$Data = $_POST['Data'];
			$Data['vPassword'] = md5($Data['vPassword']);			
     			$Data['dAddedDate'] = date('Y-m-d H:i:s');
		        $id = $this->user_model->save($Data);
			$img_uploaded = $this->do_upload($id);
			$Data['vphoto'] = $img_uploaded;
			$id = $this->registration_model->update($id,$Data);
			if($id)$var_msg = "User is added successfully.";else $var_msg="Error-in add.";
				redirect(admin_url.'user/userlist?msg='.$var_msg);
				exit;
		}
	}
     
     function getstates()
     {
	  $icountryid = $_GET['icountryid'];
          $db_state = $this->registration_model->get_state_by_country($icountryid);
	 
	  echo  "<option value=''>--Select State--</option>";
	       for($i=0; $i<=count($db_state); $i++)
	       {
		 echo "<option value='".$db_state[$i]->iStateId."'>".$db_state[$i]->vState."</option>";
	       }
         // echo '</select>';
	  
     }
     
     function check_register()
     {
	  if($_GET && $_GET != '')
	  {
	       $checkUser = $this->registration_model->checkUserName($_GET['vUserName'])->result_array();
	       if(count($checkUser) > '0'){
		    echo "allredy exists";exit;
	       }
	       $checkEmail = $this->registration_model->checkEmail($_GET['vEmail'])->result_array();
	       if(count($checkEmail) > '0'){
		    echo "email allredy exists";exit;
	       }
	       
	       $data['vUserName'] = $_GET['vUserName'];
	       $data['vPassword'] = $this->encrypt($_GET['vPassword']);
	       $data['vFirstName'] = $_GET['vFirstName'];
	       $data['vLastName'] = $_GET['vLastName'];
	       $data['vEmail'] = $_GET['vEmail'];
	       $data['vPhone'] = $_GET['vTelePhone'];
               $data['tAddress'] = $_GET['tAddress'];
	       $data['vFax'] = $_GET['vFax'];
	       $data['vCity'] = $_GET['vCity'];
	       $data['iCountryId'] = $_GET['iCountryId'];
	       $data['iStateId'] = $_GET['iStateId'];
	       $data['vZipCode'] = $_GET['vZipCode'];
	       $data['dAddedDate'] = date('Y-m-d H:i:s');
	       
	       $length = 20;
	       $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	       $data['vActivationcode'] = $randomString;  
			 if($id = $this->registration_model->save($data))
			 {   
			      $AddressData['iUserId'] = $id;
			      $AddressData['vFirstName'] = $data['vFirstName'];
			      $AddressData['vLastName'] = $data['vLastName'];
			      $AddressData['vTelephone'] = $data['vPhone'];
			      $AddressData['vFax'] = $data['vFax'];
			      $AddressData['tStreetAddress1'] = $data['tAddress'];
			      $AddressData['tStreetAddress2'] = $data['tAddress'];
			      $AddressData['vCity'] = $data['vCity'];
			      $AddressData['iStateId'] = $data['iStateId'];
			      $AddressData['vZipCode'] = $data['vZipCode'];
			      $AddressData['iCountryId'] = $data['iCountryId'];
			      $iAddressId = $this->registration_model->saveAddress($AddressData);
			      
			      $billAddress['iBillingUserId'] = $id;
			      $billAddress['vBillingFirstName'] = $data['vFirstName'];
			      $billAddress['vBillingLastName'] = $data['vLastName'];
			      $billAddress['vBillingTelephone'] = $data['vPhone'];
			      $billAddress['vBillingFax'] = $data['vFax'];
			      $billAddress['tBillingStreetAddress1'] = $data['tAddress'];
			      $billAddress['tBillingStreetAddress2'] = $data['tAddress'];
			      $billAddress['vBillingCity'] = $data['vCity'];
			      $billAddress['iBillingStateId'] = $data['iStateId'];
			      $billAddress['vBillingZipCode'] = $data['vZipCode'];
			      $billAddress['iBillingCountryId'] = $data['iCountryId'];
			      $billAddress['eBillingType'] = 'Default';
			      $iBillAddressId = $this->registration_model->saveBillAddress($billAddress);
			      
			      $shippAddress['iShippingUserId'] = $id;
			      $shippAddress['vShippingFirstName'] = $data['vFirstName'];
			      $shippAddress['vShippingLastName'] = $data['vLastName'];
			      $shippAddress['vShippingTelephone'] = $data['vPhone'];
			      $shippAddress['vShippingFax'] = $data['vFax'];
			      $shippAddress['tShippingStreetAddress1'] = $data['tAddress'];
			      $shippAddress['tShippingStreetAddress2'] = $data['tAddress'];
			      $shippAddress['vShippingCity'] = $data['vCity'];
			      $shippAddress['iShippingStateId'] = $data['iStateId'];
			      $shippAddress['vShippingZipCode'] = $data['vZipCode'];
			      $shippAddress['iShippingCountryId'] = $data['iCountryId'];
			      $shippAddress['eShippingType'] = 'Default';
			      $iShippAddressId = $this->registration_model->saveShippAddress($shippAddress);
			      $last_id = $id;
			      if($_GET['vNews'] == 'chek' || $_GET['vNews'] == '')
			      {
				   if($_GET['vNews'] == 'chek')
				   {
					$data['eStatus'] = 'Active';
					$data['dAddedDate'] = date('Y-m-d h:i:s');
					$newsletter = $this->registration_model->save_newsletter($last_id,$data['vUserName'],$data['vEmail'],
												 $data['eStatus'],$data['dAddedDate']);
			           }
			      }      
			      $site_url = $this->config->item('site_url');
			      $varify_link = $site_url.'registration/activate?vl='.$randomString;
			      $EMAIL_ADMIN = $this->getConfigData('EMAIL_ADMIN');
			    
			      $MAIL_FOOTER = 'emblemax@emblemax.com';
			      $Email =  $_GET['vEmail'];
			      $name =  ucfirst($_GET['vFirstName'].$_GET['vLastName']);
			      $bodyArr_user = array("#NAME#", "#USERNAME#", "#PASSWORD#", "#EMAIL#", "#PHONE#", "#ADDRESS#", "#SITE_URL#",
						       "#MAIL_FOOTER#","#SITE_NAME#","#CONTACT_NUMBER#","#VERIFY_LINK#");
			      $postArr_user = array($name,$data['vUserName'], $_GET['vPassword'],$Email,$data['vPhone'],$data['tAddress'],$site_url,
						    $MAIL_FOOTER,'emblemax',$data['vPhone'],$varify_link);
			      //$this->Send("USER_REGISTER","Member",$Email,$bodyArr_user,$postArr_user);
			      $this->Send("USER_REGISTER_ADMIN","Administrator",$EMAIL_ADMIN,$bodyArr_user,$postArr_user);
			     // $_SESSION['msg'] = 'success';
			 }
	  }
     }  
  
	public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr)
	{
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";
		
		$email_info = $this->registration_model->list_sysemaildata($ssql)->result();
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: emblemax.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
				
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		$this->body = str_replace($bodyArr,$postArr, $this->body);
		$To = stripcslashes($ToEmail);
		$htmlMail = '
		
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		    <html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>Emblemax</title>
		    </head>
		    
		    <body style="padding:0; margin:0; border:0;">
			 <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			      <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
				   <div style="background:#363F49; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt="" height="50px"/></div>
					'.$this->body.'
				   </div>
			      </div>
			 </div>
		    </body>
	       </html>';
		$res = @mail($To,$Subject,$htmlMail,$headers);
		if($res)
		{
			 //echo $var_msg = 'Registration mail sent successfully';
		}else
		{
			//$var_msg =  'Registration password mail sent fail';
		}
		$_SESSION['msg'] = "Thank you for registering, We have sent you an activation link , please confirm your email address.";
		echo "success";exit;
		//redirect(site_url.'myAccount/myDashboard');
		//redirect(site_url.'registration/registration_success');
		//return $var_msg;
	}
   	function encrypt($data)
	{
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$key] = $c;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return base64_encode($data);
	}

	function decrypt($data)
	{
		$data = base64_decode($data);
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$c] = $key;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}		
		return $data;
	}
	function registration_success(){
	  if(isset($_SESSION['msg']) && $_SESSION['msg'] != '')
	  {
	      $msg = $_SESSION['msg'];
	      unset($_SESSION['msg']);
	  }
	  else
	  {
	      $msg = '';
	  }
	  $this->smarty->assign("msg",$msg);
	  $this->smarty->view('registration_success.tpl');
	}
	
	 function activate(){
	  $activeCode = $_GET['vl'];
	  $activeCode = $this->registration_model->getActivate($activeCode);
	  $_SESSION['msg'] = "Your account has been activated.";
          redirect(site_url.'home');
        }
	
}
       
  

/* End of file index.php */
/* Location: ./application/controllers/index.php */
