<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $logged = '';
	var $data = '';
    
    
    
    function __construct() {
		
	    parent::__construct();
	    $this -> load -> model('user_model', '', TRUE);
	    $this -> load -> helper('url');
	    $this -> data['post'] = FALSE;
	    $site_url = $this->config->item('site_url');
	    $front_css_path = $this->config->item('front_css_path');
	    $front_js_path = $this->config->item('front_js_path');
	    $this->smarty->assign("front_css_path",$front_css_path);
	    $this->smarty->assign("front_js_path",$front_js_path);			    
	    $fancybox_path = $this->config->item('fancybox_path');
	    $this->smarty->assign("fancybox_path",$fancybox_path);
	    $filename = $this->config->item('filename');
	    $this->smarty->assign("filename",$filename);    
	    $front_image_path = $this->config->item('front_image_path');
	    $this->smarty->assign("front_image_path",$front_image_path);
	    $this->smarty->assign("site_url",$site_url);
	    $FB_PATH = $this->config->item('FB_PATH');
	    $this->smarty->assign("FB_PATH",$FB_PATH);
	    $upload_path = $this->config->item('upload_path');
	    $this->smarty->assign("upload_path",$upload_path);
	}
    
    
    function index()
    {
	$this -> login();
    }
    
    public function login()
    {
	if(isset($_SESSION))
	{
		if(isset($_REQUEST['iUserId']))
		{
		$ssql = "AND iUserId = '".$_REQUEST['iUserId']."'";
		$var_limit = 'LIMIT 0,10';
		$data = $this->user_model->list_all($var_limit,$ssql)->result();
		
			if($data)
			{
				$login_data = serialize($_SESSION);
				$_SESSION['tmp'] = unserialize($login_data);
				$_SESSION['sess_iUserId'] = $data[0]->iUserId;
				$_SESSION["sess_vUserName"]= $data[0]->vUsername;
				$_SESSION['sess_vFirstName'] = $data[0]->vFirstName;
				$_SESSION['sess_vLastName'] = $data[0]->vLastName;
				$_SESSION['sess_vEmail'] = $data[0]->vEmail;
				$_SESSION["sess_lang"] = $_POST['vlanguage'];
				$_SESSION['sess_Name'] = $_SESSION["sess_vFirstName"]." ".$_SESSION["sess_vLastName"];
				$var_msg = 'Success';
				redirect(site_url.'dashboard?msg='.$var_msg);
				exit;
			}
		}
		else{
			if(isset($_SESSION["sess_iUserId"]) !=''){	
				redirect(site_url.'home');
				exit ; 
				}
			else{
				$var_msg ='';
				if(isset($_REQUEST['msg']) !=''){
				$var_msg = $_REQUEST['msg'];  
				}
				$this->smarty->assign("var_msg",$var_msg);
				$this->smarty->view('template.tpl');
			}
		
		}
		
	}
    }
    
    public function logout(){
        $iUserId ='';
	
        if(isset($_SESSION['tmp']))
	{
		if($_SESSION['tmp']['sess_iUserId'] != '')
		{
		$_SESSION['sess_iUserId'] = $_SESSION['tmp']['sess_iUserId'];
		$_SESSION["sess_vUserName"] = $_SESSION['tmp']['sess_vUserName'];
		$_SESSION['sess_vFirstName'] = $_SESSION['tmp']['sess_vFirstName'];
		$_SESSION['sess_vLastName'] = $_SESSION['tmp']['sess_vLastName'];
		$_SESSION['sess_vEmail'] = $_SESSION['tmp']['sess_vEmail'];
		$_SESSION['sess_Name'] = $_SESSION['tmp']['sess_Name'];
		$_SESSION['tmp'] = '';
		}else{
			session_destroy();
		}
		//redirect(admin_url.'home');
		exit;	
		
		
	}else{
		if(isset($_SESSION['sess_iUserId'])){
		$iId = $_SESSION['sess_iUserId'];
		$iLoginHistoryId = $_SESSION["sess_iLoninID"];
		}
		$date = date('Y-m-d h:i:s');
		session_destroy();
		redirect(site_url.'authentication/logoutSuccess');
		exit;
	}
    }
    
    public function check_login()
    {
        if($_GET && $_GET !="")
        {
		$site_url = $this->config->item('site_url');
		if($_GET['mode'] != "forgotpassword"){
			$vUserName = $_GET['vUserName'];
			$site_url = $this->config->item('site_url');
			$vPassword = $this->encrypt($_GET['vPassword']);
			$dataArr = $this->user_model->user_login_check($vUserName,$vPassword)->result();
		        $tot = count($dataArr);
			if($dataArr){
			     
			     if($dataArr[0]->eStatus=='Inactive')
				{
				     //$var_msg = "Sorry , You are inactive please contact to administrator";exit;
				     echo $msg = "InActive";exit;
				     //redirect(site_url.'home?msg='.$var_msg);
				}
	
			     $_SESSION["sess_iUserId"]=$dataArr[0]->iUserId;
			     $_SESSION["sess_vUserName"]= $dataArr[0]->vUsername;
			     $_SESSION["sess_vFirstName"]=$dataArr[0]->vFirstName;
			     $_SESSION["sess_vLastName"]=$dataArr[0]->vLastName;
			     $_SESSION["sess_vEmail"]=$dataArr[0]->vEmail;
			     $_SESSION["sess_UserName"]= $_SESSION["sess_vFirstName"]." ".$_SESSION["sess_vLastName"];
			     
			     #------------------------------------------------------
			     # FOR LAST LOGIN LOG.
			     #------------------------------------------------------
			     $tLastLogin=date("Y-m-d h:i:s");
			     $vFromIP=$_SERVER["REMOTE_ADDR"];
			     //$_SESSION["sess_iUserId"]= $loginid;
			     $var_msg = "success";
			     echo $var_msg;
			     //redirect($site_url.'myAccount');
			     exit;
			}else{
			     echo $var_msg = "Invalid";exit;
			     redirect(site_url.'home?msg='.$var_msg);
			     exit;
			}
		}
		if($_GET['mode'] == "forgotpassword"){
			
			$site_url = $this->config->item('site_url');
			if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
			     $msg = $_SESSION['msg'];
			     unset($_SESSION['msg']);
			}else{
			     $msg = '';
			}
			$this->smarty->assign("msg",$msg);	
			$vEmail = $_GET['vEmail'];
			$emailmatch = $this->user_model->get_email($vEmail)->result_array();
			if(count($emailmatch) == '0')
			{
			    $_SESSION['msg'] = "You are not registered, please register";
			    echo "invalid";exit;
			}
			$ssql = " AND vEmail = '".$vEmail."'";
			$var_limit = '';
			$dataArr = $this->user_model->list_all($var_limit,$ssql,'','')->result();
                        $tot = count($dataArr);
			if($tot > 0){
				$MAIL_FOOTER = 'emblemax@emblemax.com';
				$site_url ='<a href="'.$this->config->item('site_url').'" >'.$this->config->item('site_url').'</a>';
				$vUserName = $dataArr[0]->vUserName;
				$vEmail = $dataArr[0]->vEmail;
				$name =  $dataArr[0]->vFirstName." ".$dataArr[0]->vLastName;
				$vPassword = $this->decrypt($dataArr[0]->vPassword);
				$bodyArr_Admin = array("#NAME#", "#USERNAME#", "#PASSWORD#", "#SITE_URL#", "#MAIL_FOOTER#","#SITE_NAME#","#EMAIL#");
				$postArr_Admin = array($name, $vUserName, $vPassword, $site_url, $MAIL_FOOTER,'Emblemax');
				$vEmail = $dataArr[0]->vEmail;
				$this->Send('USER_FORGOT_PASSWORD','Member',$vEmail,$bodyArr_Admin,$postArr_Admin);							
				exit;
			}else{
				$var_msg = "Please enter valid email address.";
				
			}redirect(site_url.'authentication?msg='.$var_msg);
				exit;
		}
        }
    }
   	function forgotpassword(){
		$site_url = $this->config->item('site_url');
		$this->smarty->assign("site_url",$site_url);
		$this->smarty->view('forgot_pass.tpl');
	} 
	public function encrypt($data)
	{
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$key] = $c;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return base64_encode($data);
	}
        public function decrypt($data)
	{
		$data = base64_decode($data);
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$c] = $key;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return $data;
	}
	public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr)
	{
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";
		$email_info = $this->user_model->list_sysemail($ssql)->result();
		
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: izishirt.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		$this->body = str_replace($bodyArr,$postArr, $this->body);
		$To = stripcslashes($ToEmail);
		
		$htmlMail = '
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		    <html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>Emblemax</title>
		    </head>
		    
		    <body style="padding:0; margin:0; border:0;">
			 <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			      <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
				   <div style="background:#9E043A; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt="" height="50px"/></div>
					'.$this->body.'
				   </div>
			      </div>
			 </div>
		    </body>
	       </html>';
	     
		$res = @mail($To,$Subject,$htmlMail,$headers);
		if($res)
		{
			 $var_msg = 'Forgot password mail sent successfully';
		}else
		{
			$var_msg =  'Forgot password mail sent fail';
		}
		$_SESSION['msgs'] = "If there is an account associated with ".$To." you will receive an email with a link to reset your password.";
		echo "success";exit;
		//return $var_msg;
	}
	function logoutSuccess(){
		$this->smarty->view('logout_success.tpl');
	}

    
}

