<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount extends MY_Controller {
     
     function __construct() {
		
	    parent::__construct();
	       
	       $this -> load -> model('myaccount_model', '', TRUE);
	       $this->load->helper('url');
	       $this->load->library('upload');
	       $this->load->library('image_lib');
	       
	       $this->smarty->assign("filename",$filename);    
	       $site_url = $this->config->item('site_url');
	       $front_css_path = $this->config->item('front_css_path');
	       $front_js_path = $this->config->item('front_js_path');
	       $this->smarty->assign("front_css_path",$front_css_path);
	       $this->smarty->assign("front_js_path",$front_js_path);		
	       $fancybox_path = $this->config->item('fancybox_path');
	       $this->smarty->assign("fancybox_path",$fancybox_path);
	       $front_image_path = $this->config->item('front_image_path');
	       $this->smarty->assign("front_image_path",$front_image_path);
	       $this->smarty->assign("site_url",$site_url);
	       $FB_PATH = $this->config->item('FB_PATH');
	       $this->smarty->assign("FB_PATH",$FB_PATH	);
	       $upload_path = $this->config->item('upload_path');
	       $this->smarty->assign("upload_path",$upload_path);
	       $filename = $this->config->item('filename');
	       $this->smarty->assign("filename",$filename);  
	      
	  }
	  function index()
	  {
	       $iUserId = $_SESSION["sess_iUserId"];
	       if(!$iUserId){
		  redirect(site_url.'login');  
	       }
	       $this->smarty->assign("isActiveMenu",'editprofile'); 
	       $db_country = $this->myaccount_model->list_country()->result();
	       $user_data = $this->myaccount_model->get_user($iUserId)->result();
	       
	       $newsletter = $this->myaccount_model->getNewsletter($iUserId)->result_array();
	       $this->smarty->assign("newsletter",$newsletter);

	       $db_state = $this->myaccount_model->list_state_by_countryid($user_data[0]->iCountryId)->result();
	       $this->smarty->assign("db_state",$db_state);
	       $this->smarty->assign("user_data",$user_data);
	       $this->smarty->assign("db_country",$db_country);
	       $this->smarty->assign('operation','edit');
	       $this->smarty->view('edit_profile.tpl');
	  }
	  
	  function changepass(){
	       if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
		    $msg = $_SESSION['msg'];
		    unset($_SESSION['msg']);
	       }else{
		    $msg = '';
	       }
	       $iUserId = $_SESSION["sess_iUserId"];
	       if(!$iUserId){
		  redirect(site_url.'login');  
	       }
	       $this->smarty->assign("isActiveMenu",'changepass'); 
	       $this->smarty->assign("msg",$msg);	       
	       $this->smarty->view('change_password.tpl');
	       
	       if($_GET){
		    $iUserId = $_SESSION["sess_iUserId"];
		    $user_data = $this->myaccount_model->getUserData($iUserId)->result();
		   
                    $oldPass = $this->decrypt($user_data[0]->vPassword);
		    $enteredPass = $_GET['vPassword'];
		    
		    if($oldPass != $enteredPass) {
			echo "Invalid password";exit;
		    }
		    else{
			 $site_url = $this->config->item('site_url');
			 $newPass =  $this->encrypt($_GET['vNewPassword']);
			 $id = $this->myaccount_model->update_pass($newPass,$iUserId);
			 $name = $user_data[0]->vFirstName."&nbsp;".$user_data[0]->vLastName;
			 $vPassword = $_GET['vNewPassword'];
			 $vEmail = $user_data[0]->vEmail;
			 $vUserName = $user_data[0]->vUserName;
			 $bodyArr_Admin = array("#NAME#", "#USERNAME#", "#PASSWORD#", "#SITE_URL#", "#MAIL_FOOTER#");
			 $postArr_Admin = array($name, $vUserName, $vPassword, $site_url, 'emblemax');
			 $this->Send('CHANGE_PASSWORD_USER','Member',$vEmail,$bodyArr_Admin,$postArr_Admin);
			    $_SESSION['msg'] = "Your password has been changed, so please confirm your email address  and login again.";
			    echo "success";exit;
			    //redirect(site_url.'myAccount/myDashboard');
		    }
	       }
	  }

	public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr)
	{
		$site_url = $this->config->item('site_url');
		$ssql = "AND vEmailCode='".$EmailCode."'";
		$email_info = $this->myaccount_model->list_sysemail($ssql)->result();
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= 'From: emblemax.com <support@www.emblemax.com>' . "\r\n".
				'Reply-To: izishirt.com <support@www.emblemax.com>'. "\r\n".
				'Return-Path: emblemax.com <support@.emblemax.com>' . "\r\n".
				'X-Mailer: PHP/' . phpversion();
		$Subject = strtr($email_info[0]->vEmailSubject, "\r\n" , "  " );
		$this->body = $email_info[0]->tEmailMessage_en;
		$this->body = str_replace($bodyArr,$postArr, $this->body);
	        $To = stripcslashes($ToEmail);
		$htmlMail = '
		
	       <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		    <html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		    <title>Emblemax</title>
		    </head>
		    
		    <body style="padding:0; margin:0; border:0;">
			 <div class="mainwrap" style="float:left; width:650px; background:#e5e9ec; padding:5px;">
			      <div class="imconpart" style="float:left; width:98%; background:#f2f5f7; border-radius:5px; border-right:1px solid #d3d9dd; border-bottom:1px solid #d3d9dd; padding:1%;">
				   <div style="background:#9E043A; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt="" height="50px"/></div>
					'.$this->body.'
				   </div>
			      </div>
			 </div>
		    </body>
	       </html>';
	       $res = @mail($To,$Subject,$htmlMail,$headers);
	       if($res)
	       {
		    $var_msg = 'Your New password mail sent successfully';
	       }else
	       {
		    $var_msg =  'Your New password mail sent fail';
	       }
	       	//redirect(admin_url.'authentication/login?msg='.$var_msg);exit;
		//return $var_msg;
	}
	
	function edit_profile(){
	  if($_GET){
	       $Data['vFirstName'] = $_GET['vFirstName'];
	       $Data['vLastName'] = $_GET['vLastName'];
	       $Data['vEmail'] = $_GET['vEmail'];
	       $Data['vPhone'] = $_GET['vPhone'];
	       $Data['vFax'] = $_GET['fax'];
	       $Data['tAddress'] = $_GET['tAddress'];
	       $Data['vCity'] = $_GET['vCity'];
	       $Data['vZipCode'] = $_GET['zip'];
	       $Data['iCountryId'] = $_GET['iCountryId'];
	       $Data['iStateId'] = $_GET['iStateId'];
	       $iUserId = $_GET['iUserId'];
	       if (!preg_match("/^([a-z0-9]+([_\.\-]{1}[a-z0-9]+)*){1}([@]){1}([a-z0-9]+([_\-]{1}[a-z0-9]+)*)+(([\.]{1}[a-z]{2,6}){0,3}){1}$/i", $_GET['vEmail']))
	       {
		    echo "Invalid";exit;
	       }
	       if($this->myaccount_model->edit_profile($Data,$iUserId))
	       {	       
	       if($_GET['vNewsletter']){
		  if($_GET['vNewsletter'] == 'Inactive'){
		    $newsletter = $this->myaccount_model->edit_newsletter($iUserId);
		  }
		  else{
		    $data['dAddedDate'] = date('Y-m-d H:i:s');
		    $data['vUsername'] = $_GET['vFirstName'];
		    $data['vEmail'] = $_GET['vEmail'];
		    $data['iUserId'] = $_GET['iUserId'];
		    $newsletter = $this->myaccount_model->save_newsletter($data);
		  }
	       }       
	       $_SESSION['msg'] = "Your profile has been Updated successfully";
	       echo "success";exit;
	      }
	  }
	}
  	public function encrypt($data)
	{
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$key] = $c;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return base64_encode($data);
	}
        public function decrypt($data)
	{
		$data = base64_decode($data);
		for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
		{
			$c = 255 & ($key ^ ($c << 1));
			$table[$c] = $key;
			$key = 255 & ($key + 1);
		}
		$len = strlen($data);
		for($i = 0; $i < $len; $i++)
		{
			$data[$i] = chr($table[ord($data[$i])]);
		}
		return $data;
	}	
	
	function newsletter(){
	  if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
	       $msg = $_SESSION['msg'];
		    unset($_SESSION['msg']);
	       }else{
		    $msg = '';
	       }
	  $this->smarty->assign("msg",$msg);
	  $site_url = $this->config->item('site_url');
	  $this->smarty->assign("site_url",$site_url);
	  $iUserId = $_SESSION["sess_iUserId"];
	  $this->smarty->assign("iUserId",$iUserId);
	  if($_POST){
	     if($_POST['newsletter'] == 'yes'){
		  $iUserEmail = $this->myaccount_model->getUserId($_POST['iUserId'])->result();
		  if(count($iUserEmail) == '0'){
		    $_SESSION['msg'] = "You Have All Ready Subscribed";
		    redirect(site_url.'myAccount/newsletter');
		  }
		  else{
		         $EMAIL_ADMIN = $this->getConfigData('EMAIL_ADMIN');
		         $eStatus = 'Yes';
 			 $iUserEmail = $this->myaccount_model->getUserId($_POST['iUserId'])->result();
			 $Email = $iUserEmail[0]->vEmail;
			 $site_url = $this->config->item('site_url');
			 $bodyArr_user = array("#SITE_URL#", "#MAIL_FOOTER#","#EMAIL#");
			 $postArr_user = array($site_url, $MAIL_FOOTER,$Email);
			 $this->Send("USER_NEWSLETTER","Member",$Email,$bodyArr_user,$postArr_user);
			 $this->Send("USER_NEWSLETTER_ADMIN","Administrator",$EMAIL_ADMIN,$bodyArr_user,$postArr_user);
			 $updateNews = $this->myaccount_model->updateNews($eStatus,$_POST['iUserId']);
			 $_SESSION['msg'] = "Thank You For Your Subscription";
			  redirect(site_url.'myAccount/newsletter');
		    
		  }
	     }
	  }
	  $this->smarty->view('newsletter-subscription.tpl');
      }
      
     	function myOrders()
	{
	  $iUserId = $_SESSION["sess_iUserId"];
	  if(!$iUserId){
	     redirect(site_url.'login');  
	  }
	  $this->smarty->assign("isActiveMenu",'myOrders'); 
	  $orders = $this->myaccount_model->all_orders($iUserId)->result_array();
	 //echo "<pre>";print_r($orders);exit;
	       for($i=0;$i<count($orders);$i++){
			if($orders[$i]['dAddedDate'] != '0000-00-00 00:00:00')
			{
			$orders[$i]['dAddedDate'] = date('dS M Y',strtotime($orders[$i]['dAddedDate']));
			}else{
			$orders[$i]['dAddedDate'] = '0000-00-00 00:00:00';	
		  }
	       }
	   $this->smarty->assign("orders",$orders);
	  $this->smarty->view('my_orders.tpl');
	}
	
       function showOrderDetail(){
	  $iUserId = $_SESSION["sess_iUserId"];
	  if(!$iUserId){
	     redirect(site_url.'login');  
	  }
	  $this->smarty->assign("isActiveMenu",'showOrderDetail'); 
	  $iOrderId = $_GET['iOrderId'];
	  $orderData= $this->myaccount_model->getOrderData($iOrderId)->result();
	//echo "<pre>";print_r($orderData);exit;
	  $upload_path = $this->config->item('upload_path');
	  $this->smarty->assign("upload_path",$upload_path);	
	  $orderDate = date('dS M Y h:i:A',strtotime($orderData[0]->dTransactionDate));
	  
	  for($i=0;$i<count($orderData);$i++){
		 $orderData[$i]->total = $orderData[$i]->fPrice * $orderData[$i]->iQty;
	    }
	    
	  $date_comment = date('dS M Y',strtotime($orderData[0]->dTransactionDate));
	  $time_comment = date('h:i:A',strtotime($orderData[0]->dTransactionDate));

	  $country = $this->myaccount_model->list_country()->result();
	  
	  $state = $this->myaccount_model->list_state()->result();
	
	  $this->smarty->assign("country",$country);
	  $this->smarty->assign("state",$state);
	  $orderDetails = "orderdetail";
	  $this->smarty->assign("orderDetails",$orderDetails);
	  $this->smarty->assign("time_comment",$time_comment);
	  $this->smarty->assign("date_comment",$date_comment);
	  $this->smarty->assign("orderData",$orderData);
	  $this->smarty->view('view_order.tpl');
       }
       
       function myDashboard(){
       $iUserId = $_SESSION["sess_iUserId"];
	       if(!$iUserId){
		  redirect(site_url.'login');  
	       }	
	  if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
	       $msg = $_SESSION['msg'];
	       unset($_SESSION['msg']);
	  }else{
	       $msg = '';
	  }
	  $this->smarty->assign("msg",$msg);
	  $this->smarty->assign("isActiveMenu",'myDashboard');
	  $iUserId = $_SESSION["sess_iUserId"];
	  $UserDetails = $this->myaccount_model->getUserDetails($iUserId)->result();
	  $BillShipDetails = $this->myaccount_model->getBillShippAdd($iUserId)->result();
	  $this->smarty->assign("BillShipDetails",$BillShipDetails);

	  $ShippingDetails = $this->myaccount_model->getShippingAdd($iUserId)->result();
	  $this->smarty->assign("ShippingDetails",$ShippingDetails);
	  
	  $orderData= $this->myaccount_model->getOrderDatas($iUserId)->result_array();
	  $this->smarty->assign("orderData",$orderData);
	  $country = $this->myaccount_model->list_country()->result();
	  //  echo "<pre>";print_r($country);exit;
	  $state = $this->myaccount_model->list_states()->result();
	  $this->smarty->assign("UserDetails",$UserDetails);
	  $this->smarty->assign("state",$state);
	  $this->smarty->assign("country",$country);
	  //unset($iUserId);
	  $this->smarty->view('account-dashboard.tpl');
       }
       
       function edit_address(){
        $iUserId = $_SESSION["sess_iUserId"];
	       if(!$iUserId){
		  redirect(site_url.'login');  
	       }
          if($_GET){
	  $billId = $iUserId;
	  $billAddress['vBillingFirstName'] = $_GET['vFirstName'];
	  $billAddress['vBillingLastName'] = $_GET['vLastName'];
	  $billAddress['vBillingTelephone'] = $_GET['vTelePhone'];
	  $billAddress['vBillingFax'] = $_GET['vFax'];
	  $billAddress['tBillingStreetAddress1'] = $_GET['tStreetAddress1'];
	  $billAddress['tBillingStreetAddress2'] = $_GET['tStreetAddress2'];
	  $billAddress['vBillingCity'] = $_GET['vCity'];
	  $billAddress['iBillingStateId'] = $_GET['iStateId'];
	  $billAddress['vBillingZipCode'] = $_GET['vZipCode'];
	  $billAddress['iBillingCountryId'] = $_GET['iCountryId'];
	  $billAddress['eBillingType'] = 'Default';
	  $iBillAddressId = $this->myaccount_model->updateBillAddress($billId,$billAddress);
	 
	  $shippId = $iUserId;
	  $shippAddress['vShippingFirstName'] = $_GET['vFirstName'];
	  $shippAddress['vShippingLastName'] = $_GET['vLastName'];
	  $shippAddress['vShippingTelephone'] = $_GET['vTelePhone'];
	  $shippAddress['vShippingFax'] = $_GET['vFax'];
	  $shippAddress['tShippingStreetAddress1'] = $_GET['tStreetAddress1'];
	  $shippAddress['tShippingStreetAddress2'] = $_GET['tStreetAddress2'];
	  $shippAddress['vShippingCity'] = $_GET['vCity'];
	  $shippAddress['iShippingStateId'] = $_GET['iStateId'];
	  $shippAddress['vShippingZipCode'] = $_GET['vZipCode'];
	  $shippAddress['iShippingCountryId'] = $_GET['iCountryId'];
	  $shippAddress['eShippingType'] = 'Default';
          $iShippAddressId = $this->myaccount_model->updateShippAddress($shippId,$shippAddress);
	  $_SESSION['msg'] = "The address has been updates successfully.";
	  echo "updated";exit;
	  }
	  //$lastInsert = $_SESSION['last_id'];
	  $UserDetail = $this->myaccount_model->getUserDetail($iUserId)->result();
	  $country = $this->myaccount_model->list_country()->result();
	  $state = $this->myaccount_model->list_states()->result();
	  $this->smarty->assign("state",$state);
	  $this->smarty->assign("operation",'edit');
	  $this->smarty->assign("country",$country);
	  $this->smarty->assign("UserDetail",$UserDetail);
	  $this->smarty->view('edit_address.tpl');
	  
	  if($_POST){
	       $Data = $_POST['Data'];
	       $iUpdateAddressId = $this->myaccount_model->updateAddress($Data,$_POST['iAddressId']);
	       $_SESSION['msg'] = "The address has been saved.";
	       redirect(site_url.'myAccount/address');
	       
	  }
       }
	 
	 function address(){
	       if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
		    $msg = $_SESSION['msg'];
		    unset($_SESSION['msg']);
	       }else{
		    $msg = '';
	       }
	  $iUserId = $_SESSION["sess_iUserId"];
	  $BillShipDetails = $this->myaccount_model->getBillShippAdd($iUserId)->result();
	
	  $this->smarty->assign("BillShipDetails",$BillShipDetails);

	  $ShippingDetails = $this->myaccount_model->getShippingAdd($iUserId)->result();
	  $this->smarty->assign("ShippingDetails",$ShippingDetails);

	  $this->smarty->assign("isActiveMenu",'address'); 
	  $this->smarty->assign("msg",$msg);
	  $UserDetails = $this->myaccount_model->getUserDetail($iUserId)->result();
	  $country = $this->myaccount_model->list_country()->result();
	
	  $state = $this->myaccount_model->list_states()->result();
	  $this->smarty->assign("state",$state);
	  $this->smarty->assign("operation",'edit');
	  $this->smarty->assign("country",$country);
	  $this->smarty->assign("UserDetails",$UserDetails);
	  $this->smarty->view('address.tpl');    
	  }
}