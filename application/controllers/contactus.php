<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller {
     
     function __construct() {
		
	    parent::__construct();
	       
	       $this -> load -> model('contactus_model', '', TRUE);
	       $this -> load -> helper('url');
	       $this->load->library('upload');
	       $this->load->library('image_lib');
	       /*$site_url = $this->config->item('site_url');
	       		if(isset($_SESSION["sess_iUserId"]) ==''){
			redirect(site_url);
			exit ;
		}
	       */
	       $front_css_path = $this->config->item('front_css_path');
	       $front_js_path = $this->config->item('front_js_path');
	       $this->smarty->assign("front_css_path",$front_css_path);
	       $this->smarty->assign("front_js_path",$front_js_path);		
	       $fancybox_path = $this->config->item('fancybox_path');
	       $this->smarty->assign("fancybox_path",$fancybox_path);
	       $front_image_path = $this->config->item('front_image_path');
	       $this->smarty->assign("front_image_path",$front_image_path);
	       $this->smarty->assign("site_url",$site_url);
	       $FB_PATH = $this->config->item('FB_PATH');
	       $this->smarty->assign("FB_PATH",$FB_PATH	);
	       $upload_path = $this->config->item('upload_path');
	       $this->smarty->assign("upload_path",$upload_path);
	       
	  }

	  function index()
	  {
	       $this->smarty->assign('isActiveMenu','contactus');
	       $site_url = $this->config->item('site_url');
               $this->smarty->assign("site_url",$site_url);
	       $pagecontent = $this->contactus_model->getStaticPage()->result();
	       $this->smarty->assign("pagecontent",$pagecontent);
	       $this->smarty->view('contact_us.tpl');
	  }
}
	  