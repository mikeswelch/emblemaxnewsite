<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
     
     function __construct() {
	       parent::__construct();
	       $this -> load -> model('home_model', '', TRUE);
	       $name = 'home';
	       $this->smarty->assign("name",$name);
	  }
	  function index(){
	       if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
		    $msg = $_SESSION['msg'];
		    unset($_SESSION['msg']);
	       }else{
		    $msg = '';
	       }
	       
	       $isPromotional = $this->home_model->getPromotional()->result();
	       $this->smarty->assign("isPromotional",$isPromotional);
	       $getHomeBlocks = $this->home_model->getHomeBlocks()->result();
	       $this->smarty->assign("getHomeBlocks",$getHomeBlocks);
	       $this->smarty->assign('isActiveMenu','home');
	       $this->smarty->assign("msg",$msg);
	       $site_url = $this->config->item('site_url');
	       $this->smarty->assign("site_url",$site_url);
	       $db_country = $this->home_model->list_country()->result_array();
	       $review = $this->home_model->getReview()->result_array();
	       $this->smarty->assign("db_country",$db_country);
	       $this->smarty->assign("review",$review);
	       $this->smarty->view('home.tpl');
	  }
	  
	  function authentication(){
	       if(isset($_SESSION['logged_UserId']) AND $_SESSION['logged_UserId'] != '')
	       {
		    return true;
	       }else{
		    $site_url = $this->config->item('site_url');
		    redirect(site_url.'home');
	       }
	  }
	  
	  function authenticationtype(){
	       if(isset($_SESSION['logged_UserType']) AND $_SESSION['logged_UserType'] == 'businessuser')
	       {
		    return true;
	       }else{
		    $site_url = $this->config->item('site_url');
		    redirect(site_url.'home');
	       }
	  }
	  
	  function rand_str($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'){
	       $chars_length = (strlen($chars) - 1);
	       $string = $chars{rand(0, $chars_length)};
	       for ($i = 1; $i < $length; $i = strlen($string)){
		    $r = $chars{rand(0, $chars_length)};     
		    if ($r != $string{$i - 1}) $string .=  $r;
	       }
	       return $string;
	  }

	  public function Send($EmailCode,$SendType,$ToEmail,$bodyArr,$postArr)
	  {
	       $site_url = $this->config->item('site_url');
	       $ssql = "AND vEmailCode='".$EmailCode."' AND eSendType ='".$SendType."'";
	       $email_info = $this->home_model->getEmailTemplate($ssql)->result_array();
	       
	       $headers = "MIME-Version: 1.0\r\n";
	       $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
	       $headers .= 'From: Emblemax <noreply@emblemax.com>' . "\r\n".
			       'Reply-To: Onwardz <noreply@emblemax.com>'. "\r\n".
			       'Return-Path: Onwardz <noreply@emblemax.com>' . "\r\n".
			       'X-Mailer: PHP/' . phpversion();
	       
	       $Subject = strtr($email_info[0]['vEmailSubject'], "\r\n" , "  " );
	       $this->body = $email_info[0]['tEmailMessage'];
	       $this->body = str_replace($bodyArr,$postArr, $this->body);
	       
	       $To = stripcslashes($ToEmail);	  
	       $htmlMail = '
	       
	       <!DOCTYPE html>
	       <html>
	       <head>
	       <meta charset="utf-8">
	       <title>Emblemax</title>
	       <link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
	       </head>
	       <body>
		       <div style="border:10px solid #9BC55F; background:#fcfcfc; font-family:Arial, Helvetica, sans-serif; width:630px;">
		       <div style="background:#000; padding: 10px 10px 10px 10px;"><img src="'.$site_url.'public/front-end/images/logo.png" alt=""/></div>
		       <div style="padding: 10px 10px 10px 15px; font-size:14px; color:#000;">
		       '.$this->body.'
		   </div>
	       </body>
	       </html>';
	       $res = @mail($To,$Subject,$htmlMail,$headers,'-fnoreplay@emblemax.com');
	  }
	  
	  function review(){
	       $this->smarty->view('review.tpl');
	  }

	  
     
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */