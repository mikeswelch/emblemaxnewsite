<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review extends MY_Controller {

	
     function __construct() {
		
	    parent::__construct();
	    
	    $this -> load -> model('review_model', '', TRUE);
	    $this -> load -> helper('url');
	    $this->limit = 2;
	    $site_url = $this->config->item('site_url');
	    $front_css_path = $this->config->item('front_css_path');
	    $front_js_path = $this->config->item('front_js_path');
	    $this->smarty->assign("front_css_path",$front_css_path);
	    $this->smarty->assign("front_js_path",$front_js_path);
	    
	    $fancybox_path = $this->config->item('fancybox_path');
	      $this->smarty->assign("fancybox_path",$fancybox_path);
	    
	    $front_image_path = $this->config->item('front_image_path');
	    $this->smarty->assign("front_image_path",$front_image_path);
	    $this->smarty->assign("site_url",$site_url);
	    $upload_path = $this->config->item('upload_path');
	    $this->smarty->assign("upload_path",$upload_path);
	}
     
       function index()
       {
	  if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
	       $msg = $_SESSION['msg'];
	       unset($_SESSION['msg']);
	  }else{
	       $msg = '';
	  }

	  $page = 1;//$_GET['page'];
	  $start = ($page-1)*$this->limit;
	  //$start = ($page-1)*$limit;
	  $LIMIT = ' LIMIT '.$start.','.$this->limit;
	  //$LIMIT = ' LIMIT '.$start.','.$limit;
	  //echo "<pre>";print_r($LIMIT);exit;
	  $rootCategoryId = 0;
	  $reviewAll = $this->review_model->getReview($rootCategoryId,$LIMIT)->result_array();
	  $totalData =  $this->review_model->getReview($rootCategoryId)->num_rows;
	  $pages = ceil($totalData/$this->limit);
	  $this->smarty->assign("pages",$pages);
	  $this->smarty->assign("totalData",$totalData);
	  $this->smarty->assign("all_category_data",$all_category_data);

	  $this->smarty->assign("msg",$msg);
	  $site_url = $this->config->item('site_url');
	  $db_country = $this->review_model->list_country()->result_array();
	  $this->smarty->assign("db_country",$db_country);
	  $this->smarty->assign("reviewAll",$reviewAll);
	  $this->smarty->view('review.tpl');
       }  
     
       function add()
       {
	  $upload_path = $this->config->item('upload_path');
	  $this->smarty->assign("upload_path",$upload_path);

		$admin_url = $this->config->item('admin_url');
		$action = $admin_url.'user/add';
		$site_url = $this->config->item('site_url');
		$this->smarty->assign('operation','add');
		$this->smarty->assign("action",$action);


		if($_POST)
		{
		    $total = $this->review_model->getData()->result_array();
		    $tot = count($total);
			 $Data['iOrderNo'] = $tot + 1;
		    $Data['vTitle'] = $_POST['Data']['vTitle'];
		    $Data['vCustomerName'] = $_POST['Data']['vCustomerName'];
		    $Data['iCountryId'] = $_POST['Data']['iCountryId'];
		    $Data['vCity'] = $_POST['Data']['vCity'];
		    $Data['tDescription'] = $_POST['Data']['tDescription'];
		    $Data['dAddedDate'] = date('Y-m-d H:i:s');
		    $Data['iOrderNo'];
		    $id = $this->review_model->save($Data);
		    {
			 $_SESSION['msg'] = "Thanks For Your Review";
		    }
			 redirect(site_url.'review/reviewsuccess');
                       // unset($_SESSION['review_msg']);
			 exit;
		}
	}
	
	function reviewsuccess(){
	  if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
	       $msg = $_SESSION['msg'];
	       unset($_SESSION['msg']);
	  }else{
	       $msg = '';
	  }
	  $this->smarty->assign("msg",$msg);
  	  $this->smarty->view('review_success.tpl');
	}
	
	function reviewall(){
	     $reviewall = $this->review_model->getAllReview()->result_array();
	     $this->smarty->assign("reviewall",$reviewall);
	     $this->smarty->view('review-all.tpl');
	}
        
	function ajaxpage(){
		
		$page = $_REQUEST['pageId'];
		//$limit = 3;
		$start = ($page-1)*$this->limit;
		//$start = ($page-1)*$limit;
		$LIMIT = ' LIMIT '.$start.','.$this->limit;
		//$LIMIT = ' LIMIT '.$start.','.$limit;
		/*if ($_REQUEST['pid']){
		    $parid = 'sid';
		    $pid = $_REQUEST['pid'];
		}else{
		    $parid = 'pid';
		    $pid = 0;	
		}
		if ($_REQUEST['sid']){
		    $parid = 'ssid';
		    $sid = $_REQUEST['sid'];
		    if ($_REQUEST['sort']){
			    if ($_REQUEST['sort'] == 'name'){
				    $sort = 'ORDER BY vCustomerName';
			    }else{
				    $sort = 'ORDER BY iOrderNo';
			    }
		    }else{
			    $sort = 'ORDER BY iOrderNo';
		    }
		    $category_data = $this->review_model->getCategoryTemp($LIMIT)->result_array();
		    //echo "<pre>";print_r($category_data);exit;
		    $totalData =  $this->review_model->getCategoryTemp($sid)->num_rows;
			
		}else{
		    if ($_REQUEST['sort']){
			    if ($_REQUEST['sort'] == 'name'){
				    $sort = 'ORDER BY vCustomerName';
			    }else{
				    $sort = 'ORDER BY product_category.iOrder';
			    }
		    }else{
			    $sort = 'ORDER BY iOrderNo';
		    }
		    $category_data = $this->review_model->getCategory($pid,$LIMIT,$sql);
		    $totalData =  $this->review_model->getAllReview($pid)->num_rows;
		    //echo "<pre>";print_r($);exit;
		}*/
		//$all_category_data = array_chunk($category_data->result_array(), 3);

	       $reviewAll = $this->review_model->getCategoryTemp($LIMIT)->result_array();
	       $totalData =  $this->review_model->getCategoryTemp()->num_rows;
	       $db_country = $this->review_model->list_country()->result_array();
	       $this->smarty->assign("db_country",$db_country);
     
		//$reviewAll = $category_data->result_array();
		//$totalData =  $this->review_model->getCategory($pid)->num_rows;
		$pages = ceil($totalData/$this->limit);
		//$pages = ceil($totalData/$limit);
		/*
		echo "<pre>";
		print_r($totalData.' : totalData');
		echo "</br>";
		print_r($pages.' : pages');
		echo "</br>";
		print_r($all_category_data);
		exit;
		*/
		
		$current_page =$page;
		$this->smarty->assign("parid",$parid);
		$this->smarty->assign("reviewAll",$reviewAll);
		$this->smarty->assign("current_page",$current_page);
		$this->smarty->assign("pages",$pages);
		$this->smarty->assign("pid",$pid);
		$this->smarty->assign("sid",$sid);
		$this->smarty->view('ajax-review.tpl');
	}

}   