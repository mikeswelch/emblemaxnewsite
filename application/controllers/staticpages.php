<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staticpages extends MY_Controller {
     
     function __construct() {
		
	    parent::__construct();
	       
	       $this -> load -> model('staticpages_model', '', TRUE);
	       $this->load->helper('url');
	       $this->load->library('upload');
	       $this->load->library('image_lib');
	       $filename = $this->config->item('filename');
	       $this->smarty->assign("filename",$filename);    
	       $this->smarty->assign("isActiveMenu",'aboutus');
	  }

	  function index(){
	       
	  }
	  
	  function aboutus()
	  {
	       $site_url = $this->config->item('site_url');
	       $this->smarty->assign("site_url",$site_url);
	       $this->smarty->assign('isActiveMenu','aboutus');
	       $page_content = $this->staticpages_model->getStaticPage()->result_array();
	       $content = $page_content[0]['tContent'];
	      
	       $this->smarty->assign("content",$content);
	       $this->smarty->assign("page_content",$page_content);
	       $this->smarty->view('about-us.tpl');
          }

	 function shipping_refund(){
	     $lang_name = $this->staticpages_model->get_lang()->result();
	     $this->smarty->assign("lang_name",$lang_name);
	     $site_url = $this->config->item('site_url');
	     $this->smarty->assign("site_url",$site_url);
	     $lang = $_SESSION['lang'];
	     
	     $page_content = $this->staticpages_model->getShipping($lang)->result_array();
	     $content = $page_content[0]['tContent_'.$lang];
	   
	     $category = $this->staticpages_model->getDesignCategories($lang)->result();
	     
	     $this->smarty->assign("content",$content);
	     $this->smarty->assign("lang",$lang);
	     $this->smarty->assign("category",$category);
	     $this->smarty->assign("page_content",$page_content);
             $this->smarty->view('shipping_refund.tpl');
	 }
         
	 function register_success(){
	       $this->smarty->view('register_success.tpl');
	 }
	 
	 function changepass_success()
	 {
	       $this->smarty->view('changepass_success.tpl');
	 }
	 
	 function privacy()
	 {
	       $site_url = $this->config->item('site_url');
	       $this->smarty->assign("site_url",$site_url);
	       $page_content = $this->staticpages_model->getStaticPages()->result_array();
	       $content = $page_content[0]['tContent'];
	       $this->smarty->assign("content",$content);
	       $this->smarty->assign("page_content",$page_content);
	       $this->smarty->view('privacy_policy.tpl');
          }
	  
	  function contactus(){    
		    $this->smarty->assign('isActiveMenu','contactus');
	       $site_url = $this->config->item('site_url');
           $this->smarty->assign("site_url",$site_url);
	       $pagecontent = $this->staticpages_model->getContactus()->result();
	       $this->smarty->assign("pagecontent",$pagecontent);
	       $this->smarty->view('contact_us.tpl');	  
	  }
	  function faq(){
	       $this->smarty->assign('isActiveMenu','faq');
	       $site_url = $this->config->item('site_url');
           $this->smarty->assign("site_url",$site_url);
	       $faqCategory = $this->staticpages_model->getFAQCategory()->result();
	       $faqData = $this->staticpages_model->getFaqQueAnss()->result();
	       $this->smarty->assign("faqData",$faqData);
	       $this->smarty->assign("faqCategory",$faqCategory);
	       $pagecontent = $this->staticpages_model->getFaq()->result();
	       $this->smarty->assign("pagecontent",$pagecontent);
	       $this->smarty->view('FAQ.tpl');	  
	       
	  }
	  function getQueAns(){
	       $iFaqcategoryid = $_GET['iFaqcategoryid'];
	       $faqData = $this->staticpages_model->getFaqQueAns($iFaqcategoryid)->result();
	       $this->smarty->assign("faqData",$faqData);
	       $this->smarty->assign("iFaqcategoryid",$iFaqcategoryid);
	       $this->smarty->view('faq_content.tpl');	
	  }
	  function termscondition(){
	       $tcData = $this->staticpages_model->getTcData()->result();
	       $tcContent = $tcData[0]->tContent;
	       $this->smarty->assign("tcContent",$tcContent);
	       $this->smarty->view('terms_condition.tpl');		       
	  }
	  function customerservices(){
	       $tcData = $this->staticpages_model->getCustomerServicesData()->result();
	       $content = $tcData[0]->tContent;
	       $this->smarty->assign("content",$content);
	       $this->smarty->view('customer_services.tpl');		       
	  }
	  function getallQueAns(){
	       $faqData=$this->staticpages_model->getFaqQueAnss()->result();
	       $this->smarty->assign("faqData",$faqData);
		   #echo "<pre>";print_r($faqData);exit;
	       $this->smarty->view('faq_content.tpl');	
	  }
	  
	  
}