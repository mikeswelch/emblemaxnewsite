<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";

//$route['(:any)'] = "home/sdsds";

#$route['default_controller'] = "admin/admin/index";

$route['admin'] = 'admin/authentication/index';
$route['admin/(:any)'] = 'admin/$1';
$route['category/ajaxpage'] = 'category/ajaxpage';
$route['category/(:any)'] = 'category/subcategory';
$route['search/(:any)'] = 'search/index';
$route['productdetail/(:any)'] = 'productdetail';
$route['instant-quote/(:any)'] = 'instant_quote';
$route['instant-quote'] = 'instant_quote';

$route['design-studio'] = 'design_studio';

$route['aboutus'] = 'staticpages/aboutus';
$route['privacy'] = 'staticpages/privacy';
$route['contactus'] = 'staticpages/contactus';
$route['faq'] = 'staticpages/faq';
$route['termscondition'] = 'staticpages/termscondition';
$route['customerservices'] = 'staticpages/customerservices';

#$route['contact/view'] = 'businessuser/view_contact_details/';




/*
$route['login/businessuser'] = "user/login";
$route['login/freelancer'] = "user/login";
*/

#$route['admin'] = 'admin/admin/login';
#$route['admin/dashboard'] = 'admin/admin/login';

$route['404_override'] = '';




/* End of file routes.php */
/* Location: ./application/config/routes.php */
