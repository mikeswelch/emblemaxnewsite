<section class="fpd-sidebar fpd-border-color fpd-clearfix">
	
	<!-- Navigation -->
	<div class="fpd-navigation fpd-main-color">
		<ul>
			<li class="icon-book fpd-tooltip" title="Products" data-target=".fpd-products"></li>
			<li class="icon-picture fpd-tooltip" title="Designs" data-target=".fpd-designs"></li>
			<li class="icon-font fpd-tooltip" title="Add own text" data-target=".fpd-custom-text"></li>
			<li class="icon-edit fpd-tooltip" title="Edit elements" data-target=".fpd-edit-elements"></li>
			<li class="icon-hdd fpd-tooltip" title="Your saved products" data-target=".fpd-saved-products"></li>
		</ul>
	</div>
	
	<!-- Content -->
	<div class="fpd-content fpd-content-color">
		<!-- Products -->
		<div class="fpd-products">
			<ul class="fpd-clearfix"></ul>
		</div>
		<!-- Designs -->
		<div class="fpd-designs">
			<ul class="fpd-clearfix"></ul>
		</div>
		<!-- Edit text -->
		<div class="fpd-custom-text">
			<h3>Add Text</h3>
			<textarea class="fpd-text-input fpd-textarea">Enter your text</textarea>
			<span class="fpd-button fpd-submit fpd-button-submit">ADD TEXT</span>
		</div>
		<!-- Edit elements -->
		<div class="fpd-edit-elements">
			<h3>Edit Element</h3>
			<select class="fpd-elements-dropdown">
				<option value="none">None</option>
			</select>
			<!-- Toolbar -->
			<div class="fpd-toolbar">
				<div class="fpd-color-picker">
					<input type="text" value="">
				</div>
				<div>
					<select class="fpd-fonts-dropdown"></select>
				</div>
				<div class="fpd-customize-text">
					<textarea value="" name="element_text" class="fpd-text-input fpd-textarea fpd-active"></textarea>
					<div class="fpd-text-styles">
						<button title="Align Left" class="fpd-button fpd-tooltip"><span class="icon-align-left"></span></button>
						<button title="Align Center" class="fpd-button fpd-tooltip"><span class="icon-align-center"></span></button>
						<button title="Align Right" class="fpd-button fpd-tooltip"><span class="icon-align-right"></span></button>
						<button title="Bold" class="fpd-button fpd-tooltip"><span class="icon-bold"></span></button>
						<button title="Italic" class="fpd-button fpd-tooltip"><span class="icon-italic"></span></button>
					</div>
				</div>
				<button title="Center Horizontal" class="fpd-center-horizontal fpd-button fpd-tooltip"><span class="icon-resize-horizontal"></span></button>
				<button title="Center Vertical" class="fpd-center-vertical fpd-button fpd-tooltip"><span class="icon-resize-vertical"></span></button>
				<button title="Move it down" class="fpd-move-down fpd-button fpd-tooltip"><span class="icon-arrow-down"></span></button>
				<button title="Move it up" class="fpd-move-up fpd-button fpd-tooltip"><span class="icon-arrow-up"></span></button>
				<button title="Reset to his origin" class="fpd-reset fpd-button fpd-tooltip"><span class="icon-refresh"></span></button>
				<button title="Trash" class="fpd-trash fpd-button fpd-button-danger fpd-tooltip"><span class="icon-trash"></span></button>
			</div>
		</div>
		<!-- -->
		<div class="fpd-saved-products">
			<h3>Your saved products</h3>
			<ol></ol>
		</div>
	</div>
	
</section>