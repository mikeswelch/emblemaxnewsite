
/* Fancy-Product-Designer 2.0.3 
 Copyright 2013, Rafael Dery */
! function (a) {

    var b = function (b, c) {
        var d, e, f, g, h, i, j, l, m, n, o, p, q, r, s = a.extend({}, a.fn.fancyProductDesigner.defaults, c),
            t = this,
            u = !1,
            v = null,
            w = 0,
            x = -1,
            y = null,
            z = 0,
            A = null,
            B = null,
            C = 0,
            D = "Helvetica";
        e = a(b).addClass("fpd-container fpd-clearfix"), p = e.children(".fpd-product").remove(), q = e.children(".fpd-design");
        var E = document.createElement("canvas"),
            F = Boolean(E.getContext && E.getContext("2d"));
        if (!F) return a.post(s.templatesDirectory + "canvaserror.php", function (b) {
            e.append(a.parseHTML(b)), e.trigger("templateLoad", [this.url])
        }), e.trigger("canvasFail"), !1;
        window.localStorage.length, a.post(designerBaseUrl + "sidebar", function (b) {
            e.append(a.parseHTML(b)), f = e.children(".fpd-sidebar"), g = f.children(".fpd-navigation"), h = f.children(".fpd-content"), l = h.find(".fpd-toolbar"), m = l.children(".fpd-color-picker"), e.trigger("templateLoad", [this.url]), a.post(designerBaseUrl + "productstage", function (b) {
                e.append(a.parseHTML(b)), i = e.children(".fpd-product-container"), j = i.children(".fpd-menu-bar"), e.trigger("templateLoad", [this.url]), G()
            })
        });
        var G = function () {
            var b = i.children(".fpd-product-stage").children("canvas").get(0);
            d = new fabric.Canvas(b, {
                selection: !1,
                hoverCursor: "pointer"
            }), d.setDimensions({
                width: i.width(),
                height: i.height()
            }), o = i.append('<div class="fpd-modification-tooltip"></div>').children(".fpd-modification-tooltip").tooltipster({
                offsetY: -3,
                theme: ".fpd-tooltip-theme",
                delay: 0,
                speed: 0,
                touchDevices: !1
            }), d.on({
                "mouse:down": function (a) {
                    void 0 == a.target && M()
                },
                "object:moving": function (a) {
                    B.params.x = Math.round(a.target.left), B.params.y = Math.round(a.target.top), N(a.target), J("x: " + Math.round(a.target.left) + " y: " + Math.round(a.target.top), "moved"), T(B)
                },
                "object:scaling": function (a) {
                    B.params.scale = Number(a.target.scaleX).toFixed(2), B.params.x = Math.round(a.target.left), B.params.y = Math.round(a.target.top), N(a.target), J("width: " + Math.round(B.getWidth()) + " height: " + Math.round(B.getHeight()), "scaled"), T(B)
                },
                "object:rotating": function (a) {
                    B.params.degree = Math.round(a.target.angle), N(a.target), J("angle: " + Math.round(a.target.angle) % 360, "rotated"), T(B)
                },
                "object:selected": function (a) {
                    M(!1), B = a.target;
                    var b = B.params;
                    if (T(B), B.set({
                        borderColor: "grey",
                        cornerColor: "black",
                        cornerSize: 10,
                        transparentCorners: !0
                    }), h.find(".fpd-elements-dropdown").children('option[value="' + B.id + '"]').prop("selected", !0), Array.isArray(b.colors) ? (m.children("input").val(b.currentColor), b.colors.length > 1 ? m.children("input").spectrum({
                        preferredFormat: "hex",
                        showPaletteOnly: !0,
                        showPalette: !0,
                        palette: b.colors,
                        change: function (a) {
                            K(B, a.toHexString())
                        }
                    }) : m.children("input").spectrum("destroy").spectrum({
                        preferredFormat: "hex",
                        showInput: !0,
                        chooseText: "Change Color",
                        change: function (a) {
                            K(B, a.toHexString())
                        }
                    }), m.show()) : m.hide(), "text" == B.type && (n.children('option[value="' + b.font + '"]').prop("selected", "selected"), n.parent().show(), l.children(".fpd-customize-text").show().children("textarea").val(B.getText())), b.draggable ? l.children(".fpd-center-horizontal, .fpd-center-vertical").show() : l.children(".fpd-center-horizontal, .fpd-center-vertical").hide(), b.zChangeable ? l.children(".fpd-move-down, .fpd-move-up").show() : l.children(".fpd-move-down, .fpd-move-up").hide(), b.removable ? l.children(".fpd-trash").show() : l.children(".fpd-trash").hide(), b.boundingBox)
                        if ("object" == typeof b.boundingBox) v = new fabric.Rect({
                            left: b.boundingBox.x,
                            top: b.boundingBox.y,
                            width: b.boundingBox.width,
                            height: b.boundingBox.height,
                            stroke: "blue",
                            strokeWidth: 1,
                            fill: !1,
                            selectable: !1,
                            isBoundingRect: !0
                        }), d.add(v), v.moveTo(d.getObjects().indexOf(B));
                        else
                            for (var c = d.getObjects(), f = 0; f < c.length; ++f) {
                                var i = c[f];
                                if (i.viewIndex == z && b.boundingBox == i.title && null == v) {
                                    v = i, i.stroke = "blue", i.strokeWidth = 1;
                                    break
                                }
                            }
                        l.show(), g.find('li[data-target=".fpd-edit-elements"]').click(), N(B), e.trigger("elementSelect", [B])
                }
            });
            for (var c = [], f = 0; f < p.length; ++f) {
                c = a(p.get(f)).children(".fpd-product"), c.splice(0, 0, p.get(f));
                var k = [];
                c.each(function (b, c) {
                    var d = a(c),
                        e = {
                            title: c.title,
                            thumbnail: d.data("thumbnail"),
                            elements: []
                        };
                    d.children("img,span").each(function (b, c) {
                        var d = a(c),
                            f = {
                                type: d.is("img") ? "image" : "text",
                                source: d.is("img") ? d.attr("src") : d.text(),
                                title: d.attr("title"),
                                parameters: void 0 == d.data("parameters") ? {} : d.data("parameters")
                            };
                        e.elements.push(f)
                    }), k.push(e)
                }), t.addProduct(k)
            }
            if (q.size() > 0) {
                if (q.children(".fpd-category").length > 0) {
                    var u = q.children(".fpd-category");
                    $designCategories = h.find(".fpd-designs").prepend('<select class="fpd-design-categories" style="width: 100%;" tabindex="1"></select>').children(".fpd-design-categories").change(function () {
                        var b = u.filter('[title="' + this.value + '"]').children("img");
                        h.find(".fpd-designs ul").empty();
                        for (var c = 0; c < b.length; ++c) t.addDesign(a(b[c]).attr("src"), b[c].title, a(b[c]).data("parameters"))
                    }).chosen({
                        width: "100%"
                    });
                    for (var f = 0; f < u.length; f++) $designCategories.append('<option value="' + u[f].title + '">' + u[f].title + "</option>").trigger("chosen:updated");
                    $designCategories.change()
                } else
                    for (var w = q.children("img"), f = 0; f < w.length; ++f) t.addDesign(a(w[f]).attr("src"), w[f].title, a(w[f]).data("parameters"));
                q.remove()
            }
            if (g.find('li[data-target=".fpd-edit-elements"]').show(), s.customTexts) {
                var x = h.children(".fpd-custom-text");
                g.find('li[data-target=".fpd-custom-text"]').show(), x.children(".fpd-submit").click(function () {
                    var a = x.children("textarea").val();
                    t.addElement("text", a, a, s.customTextParamters, z), x.children("textarea").removeClass("fpd-active").val("Enter your text")
                }), x.children("textarea").focusin(function () {
                    var b = a(this);
                    "Enter your text" == this.value && (b.addClass("fpd-active"), this.value = "")
                }).focusout(function () {
                    var b = a(this);
                    "" == this.value && (this.value = "Enter your text", b.removeClass("fpd-active"))
                })
            }
            if (s.allowProductSaving) {
                g.find('li[data-target=".fpd-saved-products"]').show(), j.find(".fpd-save-product").click(function () {
                    M();
                    var a = prompt(s.labels.saveProductInput, ""),
                        b = P(a);
                    if (-1 == b) return !1;
                    if (0 == b) return alert(s.labels.keyNotValidAlert), !1;
                    if (2 == b) {
                        var c = confirm(s.labels.keyInUseAlert);
                        if (!c) return !1
                    }
                    var d = t.getProduct(!1),
                        e = JSON.stringify(d);
                    return window.localStorage.setItem(y + "_" + a, e), S(y + "_" + a, e), !1
                });
                var A = O();
                for (key in A) S(key, JSON.stringify(A[key]))
            } else j.find(".fpd-save-product").parent().hide(); if (g.find("ul li").click(function () {
                var b = a(this);
                if (!b.hasClass("fpd-content-color")) {
                    b.parent().children("li").removeClass("fpd-content-color"), b.addClass("fpd-content-color"), h.children("div").hide().children("ul").getNiceScroll().remove();
                    var c = h.children(b.data("target")).show();
                    c.children("ul").size() > 0 && c.children("ul").height(c.height() - c.children("ul").position().top).niceScroll({
                        cursorcolor: "#2E3641"
                    })
                }
            }).filter(":visible:first").click(), h.find(".fpd-elements-dropdown").change(function () {
                if ("none" == this.value) M();
                else
                    for (var a = d.getObjects(), b = 0; b < a.length; ++b)
                        if (a[b].id == this.value) {
                            d.setActiveObject(a[b]);
                            break
                        }
            }), s.fonts.length > 0 && s.fontDropdown) {
                n = l.find(".fpd-fonts-dropdown").change(function () {
                    B.setFontFamily(this.value), B.params.font = this.value, N(B), d.renderAll()
                }), D = s.fonts[0], s.fonts.sort();
                for (var f = 0; f < s.fonts.length; ++f) n.append('<option value="' + s.fonts[f] + '" style="font-family: ' + s.fonts[f] + ';">' + s.fonts[f] + "</option>");
                n.children('option[value="' + D + '"]').prop("selected", "selected"), n.parent().show()
            }
            l.children(".fpd-customize-text").children("textarea").keyup(function () {
                B.setText(this.value), B.params.text = this.value, d.renderAll(), N(B) ? J() : o.tooltipster("hide")
            }), l.find(".fpd-text-styles").children("button").click(function () {
                var b = a(this).children("span");
                b.hasClass("icon-align-left") ? (B.setTextAlign("left"), B.params.textAlign = "left") : b.hasClass("icon-align-center") ? (B.setTextAlign("center"), B.params.textAlign = "center") : b.hasClass("icon-align-right") ? (B.setTextAlign("right"), B.params.textAlign = "right") : b.hasClass("icon-bold") ? "bold" == B.getFontStyle() ? (B.setFontStyle("normal"), B.params.fontStyle = "normal") : (B.setFontStyle("bold"), B.params.fontStyle = "bold") : b.hasClass("icon-italic") && ("italic" == B.getFontStyle() ? (B.setFontStyle("normal"), B.params.fontStyle = "normal") : (B.setFontStyle("italic"), B.params.fontStyle = "italic")), d.renderAll()
            }), l.children(".fpd-center-horizontal, .fpd-center-vertical").click(function () {
                var b = a(this);
                Q(B, b.hasClass("fpd-center-horizontal"), b.hasClass("fpd-center-vertical"), v ? v.getBoundingRect() : !1)
            }), l.children(".fpd-move-down, .fpd-move-up").click(function () {
                for (var b, c = d.getObjects(), e = null, f = null, g = 0; g < c.length; ++g) c[g].viewIndex == z && (null == e && (e = g), null == f ? f = g : f++), c[g] == B && (b = g);
                b > f && (b = f), e > b && (b = e), a(this).hasClass("fpd-move-down") ? b != e && B.moveTo(b - 1) : b != f && B.moveTo(b + 1)
            }), l.children(".fpd-reset").click(function () {
                if (B) {
                    var a = B.params;
                    "text" == B.type && (B.fontSize = a.textSize, B.setText(a.source), B.setFontFamily(D), B.setTextAlign("left"), B.setFontStyle("normal"), l.children(".fpd-customize-text").children("textarea").val(a.source), n.children('option[value="' + D + '"]').prop("selected", "selected")), B.left = a.originX, B.top = a.originY, B.scaleX = 1, B.scaleY = 1, B.angle = 0, a.colors && a.currentColor != a.colors[0] && K(B, a.colors[0]), N(B), d.renderAll(), a.x = a.originX, a.y = a.originY, a.degree = 0, B.params = a
                }
            }), l.children(".fpd-trash").click(function () {
                0 != B.params.price && (C -= B.params.price, e.trigger("priceChange", [B.params.price, C])), h.find(".fpd-elements-dropdown").children('option[value="' + B.id + '"]').remove(), B.remove(), M()
            }), j.find(".fpd-download-image").click(function () {
                t.createImage(!0, !0)
            }), j.find(".fpd-print").click(function () {
                t.print()
            }), s.editorMode && (r = e.after('<div class="fpd-editor-box"><h3>Editor Box</h3><p><span>Element: </span><i class="fpd-current-element"></i></p><p><span>Position: </span><i class="fpd-position"></i></p><p><span>Scale: </span><i class="fpd-scale"></i></p><p><span>Dimesions: </span><i class="fpd-dimensions"></i></p><p><span>Degree: </span><i class="fpd-degree"></i></p><span>Color: </span><i class="fpd-color"></i></p></div>').next(".fpd-editor-box")), h.find(".fpd-products ul li:first").click(), e.trigger("ready")
        }, H = function (a) {
                a.viewIndex == z && h.find(".fpd-elements-dropdown").append('<option value="' + a.id + '">' + a.title + "</option>");
                var b = a.params;
                return a.isEditable = a.evented = !0, s.editorMode ? !1 : (b.draggable && (a.lockMovementX = a.lockMovementY = !1), b.rotatable && (a.lockRotation = !1), b.resizable && (a.lockScalingX = a.lockScalingY = !1), (b.resizable || b.rotatable) && (a.hasControls = !0), void 0)
            }, I = function (a, b) {
                function c() {
                    if (d++, d < b.length) {
                        var a = b[d];
                        t.addElement(a.type, a.source, a.title, a.parameters, w - 1)
                    } else e.off("elementAdded", c), e.trigger("viewCreate", [b])
                }
                var d = 0;
                e.on("elementAdded", c);
                var f = b[0];
                t.addElement(f.type, f.source, f.title, f.parameters, w - 1)
            }, J = function (a, b) {
                var c = B.left,
                    e = B.top;
                B.setCoords(), "moved" == b ? (c = B.oCoords.tl.x, e = B.oCoords.tl.y) : "scaled" == b ? (c = B.oCoords.tr.x, e = B.oCoords.tr.y) : (c = B.oCoords.mt.x, e = B.oCoords.mt.y), B.isOut && (a = s.labels.outOfContainmentAlert), o.css({
                    left: c,
                    top: e
                }).tooltipster("reposition").tooltipster("show").tooltipster("update", a), d.on({
                    "mouse:up": function () {
                        o.tooltipster("hide"), d.off("mouseup")
                    }
                })
            }, K = function (a, b) {
                if (4 == b.length && (b += b.substr(1, b.length)), "text" == a.type) a.setFill(b), d.renderAll();
                else {
                    var c = a.source.split(".");
                    if (1 == c.length && -1 == c[0].search("data:image/png;")) return alert("The data url of the image is not encoded as a png image. Only png images can be colorized!"), !1;
                    if (2 == c.length && "png" != c[c.length - 1].toLowerCase()) return alert("Only png images can be colorized!"), !1;
                    a.filters.push(new fabric.Image.filters.Tint({
                        color: b,
                        opacity: 1
                    })), a.applyFilters(d.renderAll.bind(d))
                }
                a.params.currentColor = b, m.children("input").spectrum("set", b), L(a, b), T(a)
            }, L = function (a, b) {
                if (a.colorControlFor)
                    for (var c = a.colorControlFor, d = 0; d < c.length; ++d) K(c[d], b)
            }, M = function (a) {
                a = "undefined" == typeof a ? !0 : a, v && (v.isBoundingRect && v.remove(), v.stroke = null, v = null), a && d.discardActiveObject(), d.renderAll(), h.find(".fpd-elements-dropdown").children("option:first").prop("selected", !0), l.hide().children("div").hide(), l.children(".fpd-customize-text").children("textarea").val(""), B = null, s.editorMode && r.find("i").text("")
            }, N = function (a) {
                if (v) {
                    a.setCoords();
                    var b = a.getBoundingRect(),
                        c = v.getBoundingRect(),
                        d = !1,
                        f = a.isOut;
                    return b.left < c.left && (d = !0), b.top < c.top && (d = !0), b.left > c.left + c.width - b.width && (d = !0), b.top > c.top + c.height - b.height && (d = !0), d ? (a.borderColor = "red", a.opacity = .5, a.isOut = !0) : (a.borderColor = "grey", a.opacity = 1, a.isOut = !1), f != a.isOut && void 0 != f && (d ? e.trigger("elementOut") : e.trigger("elementIn")), d
                }
                return !1
            }, O = function () {
                for (var a, b = 0, c = 0, d = {}, e = Object.keys(window.localStorage); a = e[b];) a.substr(0, a.indexOf("_")), window.localStorage.getItem(a) && (d[a] = JSON.parse(window.localStorage.getItem(a)), c++), b++;
                return d
            }, P = function (a) {
                if (null == a) return -1;
                if ("" == a) return 0;
                var b = 1,
                    c = Object.keys(O());
                for (k in c)
                    if (c[k] == y + "_" + a) {
                        b = 2;
                        break
                    }
                return b
            }, Q = function (a, b, c, e) {
                b && (e ? a.left = e.left + .5 * e.width : a.centerH()), c && (e ? a.top = e.top + .5 * e.height : a.centerV()), N(a), d.renderAll(), a.setCoords(), a.params.x = a.left, a.params.y = a.top
            }, R = function (a) {
                var b = a.params;
                if (b.boundingBox) {
                    var c;
                    if ("object" == typeof b.boundingBox) c = {
                        left: b.boundingBox.x - .5 * b.boundingBox.width,
                        top: b.boundingBox.y - .5 * b.boundingBox.height,
                        width: b.boundingBox.width,
                        height: b.boundingBox.height
                    };
                    else
                        for (var e = d.getObjects(), f = 0; f < e.length; ++f) {
                            var g = e[f];
                            if (g.viewIndex == z && b.boundingBox == g.title) {
                                c = g.getBoundingRect();
                                break
                            }
                        }
                    Q(a, !0, !0, c), a.params.originX = a.left, a.params.originY = a.top
                }
            }, S = function (b, c) {
                var d = b.substr(b.indexOf("_") + 1),
                    e = h.children(".fpd-saved-products").children("ol");
                return e.children("li").children('span:contains("' + d + '")').size() > 0 ? (e.children("li").children('span:contains("' + d + '")').data("product", c), !1) : (e.append('<li><button><span class="icon-remove"></span></button><span>' + d + "</span></li>").children("li:last").children("span").click(function () {
                    u = !0, t.loadProduct(JSON.parse(a(this).data("product")))
                }).data("product", c).parent().children("button").click(function () {
                    var b = confirm(s.labels.confirmProductDelete);
                    return b ? (window.localStorage.removeItem(a(this).data("key")), a(this).parent().remove(), !1) : !1
                }).data("key", b), void 0)
            }, T = function (a) {
                if (!s.editorMode) return !1;
                var b = a.params;
                r.find(".fpd-current-element").text(a.title), r.find(".fpd-position").text("x: " + b.x + " y: " + b.y), r.find(".fpd-dimensions").text("width: " + Math.round(a.getWidth()) + " height: " + Math.round(a.getHeight())), r.find(".fpd-scale").text(b.scale), r.find(".fpd-degree").text(b.degree), r.find(".fpd-color").text(b.currentColor)
            };
        this.getFabricJSON = function () {
            M();
            var a = d.toJSON(["viewIndex"]);
            return a.width = d.width, a.height = d.height, a
        }, this.getPrice = function () {
            return C
        }, this.getProduct = function (a) {
            onlyEditableElemets = "undefined" != typeof a ? a : !1, M();
            for (var b = d.getObjects(), c = 0; c < b.length; ++c) {
                var e = b[c];
                if (e.isOut) return alert(s.labels.outOfContainmentAlert), !1
            }
            for (var f = [], c = 0; c < A.length; ++c) {
                var g = A[c];
                f.push({
                    title: g.title,
                    thumbnail: g.thumbnail,
                    elements: []
                })
            }
            for (var c = 0; c < b.length; ++c) {
                var e = b[c],
                    h = {
                        title: e.title,
                        source: e.source,
                        parameters: e.params,
                        type: e.type
                    };
                a ? e.isEditable && f[e.viewIndex].elements.push(h) : f[e.viewIndex].elements.push(h)
            }
            return f
        }, this.getProductDataURL = function (a) {
            a = "undefined" != typeof a ? a : "png", M(), i.children(".fpd-views-selection").children("li:first").click();
            var b;
            return d.setBackgroundColor("white", function () {
                d.setDimensions({
                    height: i.height() * w
                });
                for (var c = d.getObjects(), e = 0; e < c.length; ++e) {
                    var f = c[e];
                    f.visible = !0, f.top = f.top + f.viewIndex * i.height()
                }
                b = d.toDataURL({
                    format: a
                }), d.setBackgroundColor("transparent"), d.setDimensions({
                    height: i.height()
                });
                for (var e = 0; e < c.length; ++e) {
                    var f = c[e];
                    f.visible = 0 == f.viewIndex, f.top = f.top - f.viewIndex * i.height()
                }
            }), b
        }, this.getProductsLength = function () {
            return h.find(".fpd-products ul li").size()
        }, this.getView = function () {
            return A[z]
        }, this.getViewDataURL = function (a) {
            return a = "undefined" != typeof a ? a : "png", d.toDataURL({
                format: a
            })
        }, this.getViewIndex = function () {
            return z
        }, this.getStage = function () {
            return d
        }, this.addProduct = function (b) {
            h.find(".fpd-products ul").append('<li><span class="fpd-img-loader"></span><img src="' + b[0].thumbnail + '" title="' + b[0].title + '" style="display:none;" /></li>').children("li:last").click(function () {
                var b = a(this),
                    c = h.find(".fpd-products ul li").index(b);
                return t.selectProduct(c), !1
            }).data("views", b).children("img").load(function () {
                a(this).fadeIn(500).prev("span").fadeOut(300, function () {
                    a(this).remove()
                })
            }), h.find(".fpd-designs ul").getNiceScroll().resize(), h.find(".fpd-products ul").children("li").length > 1 && g.find('li[data-target=".fpd-products"]').show()
        }, this.loadProduct = function (a) {
            function b() {
                w < A.length ? t.addView(A[w]) : (u = !1, e.off("viewCreate", b), e.trigger("productCreate"))
            }
            t.clear(), A = a, y = A[0].title, i.append('<ul class="fpd-views-selection"></ul>'), e.on("viewCreate", b), t.addView(A[0])
        }, this.selectProduct = function (a) {
            if (a == x) return !1;
            x = a, 0 > a ? x = 0 : a > t.getProductsLength() - 1 && (x = t.getProductsLength() - 1);
            var b = h.find(".fpd-products ul li").eq(x).data("views");
            t.loadProduct(b)
        }, this.removeProduct = function (a) {
            0 > a ? a = 0 : a > t.getProductsLength() - 1 && (a = t.getProductsLength() - 1), f.children("li").eq(a).remove(), a == x && (t.clear(), x = -1)
        }, this.addView = function (b) {
            w++, I(b.title, b.elements);
            var c = i.children(".fpd-views-selection");
            c.append('<li class="fpd-content-color"><img src="' + b.thumbnail + '" title="' + b.title + '" class="fpd-tooltip" /></li>').children("li:last").click(function () {
                var b = a(this),
                    e = c.children("li").index(b);
                if (e != z) {
                    M(), h.find(".fpd-elements-dropdown").children('option:not([value="none"])').remove(), z = e;
                    for (var f = d.getObjects(), g = 0; g < f.length; ++g) {
                        var i = f[g];
                        i.visible = i.viewIndex == z, i.viewIndex == z && i.isEditable && h.find(".fpd-elements-dropdown").append('<option value="' + i.id + '">' + i.title + "</option>")
                    }
                    d.renderAll()
                }
            }), a(".fpd-tooltip").tooltipster({
                offsetY: -3,
                theme: ".fpd-tooltip-theme",
                touchDevices: !1
            }), w > 1 ? c.show() : c.hide()
        }, this.addElement = function (b, c, f, g, h) {
            if (h = "undefined" != typeof h ? h : z, M(), "object" != typeof g) return alert("The element " + f + " has not a valid JSON object as parameters!"), !1;
            g = a.extend({}, s.elementParameters, g), g.originX = g.x, g.originY = g.y, g.source = c;
            var i = !1,
                j = null;
            if (g.colors && "string" == typeof g.colors)
                if (0 == g.colors.indexOf("#")) {
                    var k = g.colors.replace(/\s+/g, "").split(",");
                    g.colors = k, g.currentColor = k[0]
                } else if (w > 1)
                for (var l = d.getObjects(), m = 0; m < l.length; ++m) {
                    var n = l[m];
                    0 == n.viewIndex && g.colors == n.title && null == j && (j = n, g.currentColor = n.params.currentColor, i = !0)
                }
            var o = {
                source: c,
                title: f,
                top: g.y,
                left: g.x,
                scaleX: g.scale,
                scaleY: g.scale,
                angle: g.degree,
                id: String((new Date).getTime()),
                visible: h == z,
                viewIndex: h
            };
            if (s.editorMode ? g.removable = g.resizable = g.rotatable = g.zChangeable = !0 : a.extend(o, {
                selectable: !1,
                lockUniScaling: !0,
                lockRotation: !0,
                lockScalingX: !0,
                lockScalingY: !0,
                lockMovementX: !0,
                lockMovementY: !0,
                hasControls: !1,
                evented: !1
            }), "image" == b) new fabric.Image.fromURL(c, function (b) {
                var c = b.width,
                    f = b.height,
                    h = 1;
                c > f ? c < s.minImageWidth ? h = s.minImageWidth / c : c > s.maxImageWidth && (h = s.maxImageWidth / c) : f < s.minImageHeight ? h = s.minImageHeight / f : f > s.maxImageHeight && (h = s.maxImageHeight / f), c = h * c, f = h * f, g.width = c, g.height = f, a.extend(o, {
                    width: c,
                    height: f,
                    params: g
                }), b.set(o), i && (j.colorControlFor ? j.colorControlFor.push(b) : (j.colorControlFor = [], j.colorControlFor.push(b))), ("object" == typeof g.colors || g.removable || g.draggable || g.resizable || g.rotatable) && (b.set("selectable", !0), H(b)), s.centerInBoundingbox && !u && R(b), d.add(b), g.colors && g.colors.length > 0 && K(b, g.currentColor), e.trigger("elementAdded", [b])
            });
            else {
                if ("text" != b) return alert("Sorry. This type of element is not allowed!"), !1;
                g.text = g.text ? g.text : g.source, g.font = g.font ? g.font : D, g.textSize = g.textSize ? g.textSize : s.textSize, g.fontStyle = g.fontStyle ? g.fontStyle : "normal", g.textAlign = g.textAlign ? g.textAlign : "left", a.extend(o, {
                    fontSize: g.textSize,
                    fontFamily: g.font,
                    fontStyle: g.fontStyle,
                    textAlign: g.textAlign,
                    fill: g.currentColor ? g.currentColor : "#000000",
                    params: g
                });
                var p = new fabric.Text(g.text.replace(/\\n/g, "\n"), o);
                d.add(p), ("object" == typeof g.colors || g.removable || g.draggable || g.resizable || g.rotatable) && (p.set("selectable", !0), H(p)), s.centerInBoundingbox && !u && R(p), e.trigger("elementAdded", [p])
            }
            g.price && (C += g.price, e.trigger("priceChange", [g.price, C]))
        }, this.addDesign = function (b, c, d) {
            h.find(".fpd-designs ul").append('<li><span class="fpd-img-loader"></span><img src="' + b + '" title="' + c + '" style="display: none;" /></li>').children("li:last").click(function () {
                var b = a(this);
                t.addElement("image", b.children("img").attr("src"), b.children("img").attr("title"), b.data("parameters"), z)
            }).data("parameters", d).children("img").load(function () {
                a(this).fadeIn(500).prev("span").fadeOut(300, function () {
                    a(this).remove()
                })
            }), h.find(".fpd-designs ul").getNiceScroll().resize(), h.find(".fpd-designs ul li").length > 0 && g.find('li[data-target=".fpd-designs"]').show()
        }, this.print = function () {
            var b = new Image;
            return b.src = t.getProductDataURL(), b.onload = function () {
                var b = window.open("", "", "width=" + this.width + ",height=" + this.height + ",location=no,menubar=no,scrollbars=yes,status=no,toolbar=no");
                b.document.title = "Print Image", a(b.document.body).append('<img src="' + this.src + '" />'), setTimeout(function () {
                    b.print()
                }, 1e3)
            }, !1
        }, this.createImage = function (b, c) {
            "undefined" == typeof b && (b = !0), "undefined" == typeof c && (c = !1);
            var d = t.getProductDataURL(),
                f = new Image;
            return f.src = d, f.onload = function () {
                if (b) {
                    var f = window.open("", "", "width=" + this.width + ",height=" + this.height + ",location=no,menubar=no,scrollbars=yes,status=no,toolbar=no");
                    f.document.title = "Product Image", a(f.document.body).append('<img src="' + this.src + '" />'), c && (window.location.href = f.document.getElementsByTagName("img")[0].src.replace("image/png", "image/octet-stream"))
                }
                e.trigger("imageCreate", [d])
            }, f
        }, this.clear = function () {
            M(), i.children(".fpd-views-selection").remove(), h.find(".fpd-elements-dropdown").children('option:not([value="none"])').remove(), d.clear(), w = z = C = 0, A = B = null
        }
    };
    jQuery.fn.fancyProductDesigner = function (c) {
        return this.each(function () {
            var d = a(this);
            if (!d.data("fancy-product-designer")) {
                var e = new b(this, c);
                d.data("fancy-product-designer", e)
            }
        })
    }, a.fn.fancyProductDesigner.defaults = {
        minImageWidth: 10,
        minImageHeight: 10,
        maxImageWidth: 700,
        maxImageHeight: 700,
        textSize: 18,
        fontDropdown: !0,
        fonts: ["Arial", "Helvetica", "Times New Roman", "Verdana", "Geneva"],
        customTextParamters: {},
        customTexts: !0,
        editorMode: !1,
        elementParameters: {
            x: 0,
            y: 0,
            colors: !1,
            removable: !1,
            draggable: !1,
            rotatable: !1,
            resizable: !1,
            zChangeable: !1,
            scale: 1,
            degree: 0,
            price: 0,
            boundingBox: !1
        },
        labels: {
            outOfContainmentAlert: "The element is out of his containment!",
            keyNotValidAlert: "The key is not valid!",
            keyInUseAlert: "The key is already used. Would you like to use it anyway?",
            confirmProductDelete: "Delete saved product?",
            saveProductInput: "Enter a key for your proudct:"
        },
        allowProductSaving: !0,
        centerInBoundingbox: !0,
        templatesDirectory: "assets/library/designer/templates/",
        phpDirectory: "assets/library/designer/php/"
    }
}(jQuery);