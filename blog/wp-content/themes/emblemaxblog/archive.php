<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>
<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
        <div class="main container">
        
        
        <div id="wrapper" class="wrapper_archive">
    <section id="middile_container">
        <!-- LEFT  PART START-->
        <article id="leftpart">
            <div class="fleft">
                <!--porfolio page start here-->
                <div class="porfolio_body_box">
                    <div class="porfolio_header">
                        <div class="header_txt">Blog</div>
                        <div class="web_wordpress"></div>
                    </div>
                    <div class="clear"></div>
                  <?php if ( have_posts() ) : ?>

				<div class="page-header page_blog_header">
					<div class="page-title">
						<?php if ( is_day() ) : ?>
							<?php printf( __( 'Daily Archives: %s', 'twentyeleven' ), '<span>' . get_the_date() . '</span>' ); ?>
						<?php elseif ( is_month() ) : ?>
							<?php printf( __( 'Monthly Archives: %s', 'twentyeleven' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyeleven' ) ) . '</span>' ); ?>
						<?php elseif ( is_year() ) : ?>
							<?php printf( __( 'Yearly Archives: %s', 'twentyeleven' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentyeleven' ) ) . '</span>' ); ?>
						<?php else : ?>
							<?php _e( 'Blog Archives', 'twentyeleven' ); ?>
						<?php endif; ?>
					</div>
				</div>

				

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>

				<?php endwhile; ?>

				<?php wp_pagenavi(); ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
                    
		    <div class="clear"></div>
                </div>
                <div class="body_bottom_line"><img src="<?php echo get_template_directory_uri(); ?>/images/body_bottom_line.png" alt="" /></div>
            </div>
            <!--porfolio page end here-->      
        </article>
        <!-- LEFT  PART END -->
       <!-- RIGHT  PART START -->
      
       <?php get_sidebar(); ?>
       
       <!-- RIGHT  PART END -->
       <div class="clear"></div>
    </section> 
</div>
        
        
        </div>
    </div>
</div>



<?php get_footer(); ?>
