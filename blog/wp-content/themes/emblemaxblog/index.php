<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header();
?>
<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
        <div class="main container">
        
        
                <div id="wrapper1">
                <section id="middile_container">
                <!-- LEFT  PART START-->
                <article id="leftpart">
                    <div class="fleft">
                        <!--porfolio page start here-->
                        <div class="porfolio_body_box">
                            <div class="porfolio_header">
                                <div class="header_txt">Blog</div>
                                
                          </div>
                           
                     <?php
                	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                	
                	query_posts(array('post_type'=>'post', 'post_status'=>'publish', 'paged'=>$paged));
                	?>  
                     <?php if ( have_posts() ) : ?>
                     <?php while ( have_posts() ) : the_post(); ?>	
                	    <div class="post">
                		<table class="blog_home_title">
                			<tr>
                				<td>
                					<div class="blog_home_title_div">
                						<span><a title="<?php the_title(); ?>" rel="bookmark" href="<?php the_permalink(); ?>" id="<?php the_ID(); ?>" ><?php the_title(); ?></a></span>
                					</div>
                				</td>
                			</tr>
                		</table>
                		
                	    
                	    <div class="blogpaddingtop">
                	    <div class="image-wrapper">
                		    <div class="image">
                		       <?php if ( has_post_thumbnail() ) {
                			    the_post_thumbnail('thumbnail');
                			    }
                		    ?>	
                		    </div>
                	    </div>
                		    <div class="entry-content">
                		   <?php the_excerpt(); ?>
                		    </div>
                		    <div class="post-meta">
                		      <div class="blogbox">
                			    <span class="social" style="float:left;">
                			    <?php if(function_exists('display_social4i')){
                				    echo display_social4i("small","align-left");    
                			    }?>     
                			    </span>
                			    <div class="continue">
                			    <a href="<?php the_permalink(); ?>">Continue</a>
                			    <div class="cl"></div>
                			    </div>
                		      </div>
                	      </div>
                	
                	    
                        </div>
                        	<div class="bgshadow">&nbsp;</div>
                        
                	    </div>
                        <div class="cl"></div>	
                	    <?php endwhile; // end of the loop. ?>
                	    <?php wp_pagenavi(); ?>
                	    <?php else : ?>
                
                		<article id="post-0" class="post no-results not-found">
                			<header class="entry-header">
                				<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
                			</header><!-- .entry-header -->
                
                			<div class="entry-content">
                				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
                				<?php get_search_form(); ?>
                			</div><!-- .entry-content -->
                		</article><!-- #post-0 -->
                
                	<?php endif; ?>
                    <div class="clear"></div>
                        </div>
                        
                    </div>
                    <!--porfolio page end here-->      
                </article>
                <!-- LEFT  PART END -->
                <!-- RIGHT  PART START -->      
                <?php get_sidebar(); ?>
                <!-- RIGHT  PART END -->
                <div class="clear"></div>
                </section> 
                </div>
        
        
        
        
        </div>
    </div>
</div>
<?php 
    get_footer(); 	
?>
