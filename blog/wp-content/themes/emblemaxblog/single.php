<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header();
?>
<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
        <div class="main container">
        
                <div id="wrapper" class="wrapper_single">
                    <section id="middile_container">
                        <!-- LEFT  PART START-->
                        <article id="leftpart">
                            <div class="fleft">
                                <!--porfolio page start here-->
                                <div class="porfolio_body_box">
                                    <div class="porfolio_header">
                                        <div class="header_txt">Blog</div>
                                        <div class="web_wordpress"></div>
                                    </div>
                                    <div class="clear"></div>
                                   <?php while ( have_posts() ) : the_post(); ?>
                		<div class="blog_reapt_box"> 
                			<div class="titleText_blog">
                			<table class="blog_home_title">
                					<tr>
                						<td>
                							<div class="blog_home_title_text">
                								<?php the_title(); ?>
                							</div>
                						</td>
                					</tr>
                				</table>
                				
                			</div>
                	
                			<div class="date">
                				<!--<div class="fl blog_photo">
                					<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyeleven_author_bio_avatar_size',32 ) ); ?>
                				</div>-->
                				<div class="entry-meta">Posted by <?php the_author(); ?> under <?php the_category(', '); ?> on <?php the_time('F j, Y'); ?></div>
                			</div>
                	
                			<div class="Post_blog_txt"><?php the_content(); ?></div>
                			
                			<div class="Comment_blog_page"><?php comments_template( '', true ); ?></div>
                	</div>
                			<?php endwhile; // end of the loop. ?>
                                    
                					
                		    <div class="clear"></div>
                                </div>
                                <div class="body_bottom_line"><img src="<?php echo get_template_directory_uri(); ?>/images/body_bottom_line.png" alt="" /></div>
                            </div>
                            <!--porfolio page end here-->      
                        </article>
                        <!-- LEFT  PART END -->
                       <?php get_sidebar(); ?>
                        <div class="clear"></div>
                    </section> 
                </div>        
        </div>
    </div>
</div>

<?php 
    get_footer(); 	
?>