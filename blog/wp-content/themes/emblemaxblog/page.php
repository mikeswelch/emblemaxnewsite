<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */



get_header();
?>
<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
        <div class="main container">
        
        
            <div id="wrapper">
        <section id="middile_container">
            <!-- LEFT  PART START-->
            <article id="leftpart">
                <div class="fleft">
                    <!--porfolio page start here-->
                    <div class="porfolio_body_box">
                        <div class="porfolio_header">
                            <div class="header_txt">Portfolio<span>&nbsp; What we Provide</span></div>
                            <div class="web_wordpress"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="main_pofolio_images_box">
                            <div class="porfolio_img_head_box"></div>
                            <div class="clear"></div>
                            <div class="porfolio_img_middle_box">
                                <div class="img_left"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/portfolio_img1.png" alt=""></a></div>
                                <div class="img_left"> <span>Troop Tree </span>
                                    <p>TroopTree is web application for army family, where they can send and record video via this application. Application is developed using... </p>
                                    <div class="main_read_more_btn_box">
                                        <div class="read_more_btn"><a href="#">Read More </a></div>
                                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/read_more_btn_shadow.gif" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="porfolio_img_botto_box"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="body_bottom_line"><img src="<?php echo get_template_directory_uri(); ?>/images/body_bottom_line.png" alt="" /></div>
                </div>
                <!--porfolio page end here-->      
            </article>
            <!-- LEFT  PART END -->
            <!-- RIGHT  PART START -->
            <aside>
               <?php get_sidebar(); ?>
            </aside>
            <!-- RIGHT  PART END -->
            <div class="clear"></div>
        </section> 
    </div>
        
        </div>
    </div>
</div>

<?php 
    get_footer(); 	
?>