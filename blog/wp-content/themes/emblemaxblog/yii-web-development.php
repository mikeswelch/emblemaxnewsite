<div id="wrapper">
<section id="middile_container">
	<!-- LEFT  PART START-->
	<article id="leftpart">
		<div class="fleft">
			<div class="banner_yii_web">
				<div class="banner_yii_web_logo"><img src="<?php echo $site_image_url;?>yii_logo.png" alt="Yii Web Development" title="Yii Web Development" /></div>
				<div class="yii_web_name">Yii Web Development</div>
			</div>
			<div class="body_txt_box">
				<div class="web_development_framework_body_txt">
					<h1>Yii Web Development</h1>
					<p>A high performance PHP based framework, Yii web development is found to be ideal for developing websites with web 2.0 applications. This web development framework comes in with rich interactive web applications, Yii Web Development framework has come to be known as a robust, fast, proven and scalable web solution for all web development needs. TechiesTown houses expertise in the area and offers Yii web development services to businesses who seek the same.</p>
					<div class="web_framework_title">Yii Web Development Overview</div>
					<p>Yii web development is the one  for people who do not have much time to spend over the web development.  The same can be developed in significantly  less time and hence is an ideal choice if you want to have a professional web  application framework that is rich in features too.  This framework provides a total set of rich  features as well as widgets that gives you the leverage to build the  application you had envisioned to build.  </p>
					<p>With a powerful caching support, Yii web development applications can be loaded very fast and the same is explicitly designed in such a way that it functions very effectively with AJAX. And you need not worry about security aspect at all because that is almost synonymous with Yii. It brings in output filtering, input validation, prevention of cross-site scripting and SQL injection. Yii web development offers a highly professional platform in the form of clean and reusable codes. Following the MVC pattern, Yii web development ensures that there is a clear separation of presentation and logic. </p>
					<div class="web_framework_title">Why choose TechiesTown for Yii Web Development ?</div>
					<p>The Yii web development framework offers numerous features and utilities which as laymen we may not be able to understand and apply them at the right place. So, when you have an idea of what you want, you can touch base with the Yii web developers at TechiesTown and explain the same to them so that you can get the output that you have been longing to get.</p>
					<p>Yii offers so much to the web developer that he can make use of majority of the features to arrive at the best web application and highly interactive website for their clients. Get the most out of what the experts can offer you as far as YUI web development is concerned for the online company houses best of expertise in the field. Our professionals keep themselves abreast with all the latest versions and releases in the Yii PHP framework. </p>
					<p>Take a look at our portfolio and we are confident that you will definitely be impressed with the prowess our experts and web developers have shown in numerous faculties of web application development. The same applies to Yii web development too. </p>
				</div>
				<div class="body_bottom_line"><img src="<?php echo $site_image_url;?>body_bottom_line.png" alt="" /></div>
			</div>
		</div>
		<!-- CLIENT SPEAK BOX START -->
		<!-- CLIENT SPEAK BOX END -->
	</article>
	<!-- LEFT  PART END -->
	<!-- RIGHT  PART START -->
	<aside>
		<div id="rightside">
			<!-- RIGHT  PART HEADER BG START -->
			<div class="rightheaderbg"> Our Expertise </div>
			<!-- RIGHT  PART HEADER BG END -->
			<!-- RIGHT  PART ICON START -->
			<div class="right_new_middile">
				<ul>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon.png" alt="Web Development Framework" title="Web Development Framework" /></div>
						<a href="<?=$site_url?>web-development-framework.html">Web Development Framework</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon1.png" alt="CakePHP Web Development" title="CakePHP Web Development" /></div>
						<a href="<?=$site_url?>cakephp-web-development.html">CakePHP Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon2.png" alt="Codeigniter Web Development" title="Codeigniter Web Development" /></div>
						<a href="<?=$site_url?>codeigniter-web-development.html">Codeigniter Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon3.png" alt="Symfony Web Development" title="Symfony Web Development" /></div>
						<a href="<?=$site_url?>symfony-web-development.html">Symfony Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon4.png" alt="Zend Framework Development" title="Zend Framework Development" /></div>
						<a href="<?=$site_url?>zend-framework-development.html">Zend Framework Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon5.png" alt="Typo3 Web Development" title="Typo3 Web Development" /></div>
						<a href="<?=$site_url?>typo3-web-development.html">Typo3 Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon6.png" alt="Prado Web Development" title="Prado Web Development" /></div>
						<a href="<?=$site_url?>prado-web-development.html">Prado Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon7.png" alt="Qcodo Web Development" title="Qcodo Web Development" /></div>
						<a href="<?=$site_url?>qcodo-web-development.html">Qcodo Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon8.png" alt="Akelos Web Development" title="Akelos Web Development" /></div>
						<a href="<?=$site_url?>akelos-web-development.html">Akelos Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon9.png" alt="Zoop Web Development" title="Zoop Web Development" /></div>
						<a href="<?=$site_url?>zoop-web-development.html">Zoop Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon10.png" alt="PHOCOA Web Development" title="PHOCOA Web Development" /></div>
						<a href="<?=$site_url?>phocoa-web-development.html">PHOCOA Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon11.png" alt="Kohana Web Development" title="Kohana Web Development" /></div>
						<a href="<?=$site_url?>kohana-web-development.html">Kohana Web Development</a></li>
					<li>
						<div class="our_expertise_icons"><img src="<?php echo $site_image_url;?>web_developmentframework_icon12.png" alt="Solar Web Development" title="Solar Web Development" /></div>
						<a href="<?=$site_url?>solar-web-development.html">Solar Web Development</a></li>
				</ul>
			</div>
			<div class="right_bottom"></div>
			<!-- RIGHT  PART ICON END-->
			<div class="rightheaderbg">Why Choose Us?</div>
			<div class="why_choose_us_service_box">
				<ul>
					<li>
						<div class="why_choose_icon"><img src="<?php echo $site_image_url;?>why1.gif" alt="" /></div>
						24 x7 Support </li>
					<li>
						<div class="why_choose_icon"><img src="<?php echo $site_image_url;?>why1.gif" alt="" /></div>
						Strict to Deadlines </li>
					<li>
						<div class="why_choose_icon"><img src="<?php echo $site_image_url;?>why1.gif" alt="" /></div>
						Quality Assurance</li>
					<li>
						<div class="why_choose_icon"><img src="<?php echo $site_image_url;?>why1.gif" alt="" /></div>
						Cost effective and Affordable Price</li>
					<li>
						<div class="why_choose_icon"><img src="<?php echo $site_image_url;?>why1.gif" alt="" /></div>
						Agile / Scrum Development Methodology</li>
					<li>
						<div class="why_choose_icon"><img src="<?php echo $site_image_url;?>why1.gif" alt="" /></div>
						Professional and Highly Qualified  Technical Team</li>
				</ul>
			</div>
			<div class="guaranteed_box"> <img src="<?php echo $site_image_url;?>freeguaranteed_banner2.png" alt="TechiesTown InfoTech" title="TechiesTown InfoTech" /></div>
		</div>
	</aside>
	<!-- RIGHT  PART END -->
	<div class="clear"></div>
</section>
<!-- PROJECT PART START -->
<div class="recent_txt_box">
<div class="recent_project_txt">Recent <span style="color:#3896ff;"> &nbsp;Project</span></div>
