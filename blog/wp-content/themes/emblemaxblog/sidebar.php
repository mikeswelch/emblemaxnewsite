<?php
/**
 * The Sidebar containing the main widget area.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

$options = twentyeleven_get_theme_options();
$current_layout = $options['theme_layout'];
$site_url = get_option('siteurl');

//echo $site_url;
if ( 'content' != $current_layout ) :
?>
 <!-- RIGHT  PART START -->
        <!--<aside>
            <div id="rightside">
                <div id="rightheaderbg">Categories</div>
                <div class="why_choose_us_service_box">
                    <ul style="padding-left:10px;">
                        <li class="new_line"><a href="#">Web Development<span>(4)</span></a></li>
                        <li class="new_line"><a href="#" class="active">Web Designing<span>(4)</span></a></li>
                        <li class="new_line"><a href="#">iPhone Applications<span>(5)</span></a></li>
                        <li class="new_line"><a href="#">Android Applications<span>(5)</span></a></li>
                        <li class="new_line"><a href="#">Mobile Website<span>(4)</span></a></li>
                        <li class="new_line"><a href="#">Cloud Applications<span>(1)</span></a></li>
                        <li class="new_line"><a href="#">Game Development<span>(2)</span></a></li>
                    </ul>
                </div>
                <div class="guaranteed_box"> <img src="images/freeguaranteed_banner2.png" alt=""></div>
            </div>

        </aside>-->
	<aside>
	  <div id="rightside" class="RightPart_blog">
		  <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
  
		  <?php endif; // end sidebar widget area ?>
	  </div>
	</aside>
        <!-- RIGHT  PART END -->		
<!--End mc_embed_signup-->
<?php endif; ?>