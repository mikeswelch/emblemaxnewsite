<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<?php $site_url = get_option('siteurl');?>
<?php
    /*$mainurl = "http://".$_SERVER['HTTP_HOST'];
    $mainurlarr = explode("/",$site_url);
    #echo "<pre>";
    #print_r($mainurlarr);exit;
    
    $mainurl = "http://".$_SERVER['HTTP_HOST']."/".$mainurlarr[3]."/".$mainurlarr[4];
    */
    $mainurl = "http://".$_SERVER['HTTP_HOST'];
    $mainurlarr = explode("/",$site_url);
    
    
    if($mainurlarr[2] == '192.168.1.12'){
	$mainurls = "http://".$_SERVER['HTTP_HOST']."/php/".$mainurlarr[3];
    }
    else{
	#$mainurls = "http://".$_SERVER['HTTP_HOST']."/php/".$mainurlarr[3]."/".$mainurlarr[4];
    $mainurls = "http://".$_SERVER['HTTP_HOST']."/".$mainurlarr[3];
    }
    $query = "SELECT pc.*,pct.* FROM product_category pc LEFT JOIN product_category_translation pct ON (pc.iCategoryId = pct.iCategoryId) WHERE pc.eStatus='Active'";
    $product_category = $wpdb->get_results($query);

    $twitter_query = "SELECT * FROM configurations where vName='TWITTER_LINK'";
    $twitter_link = $wpdb->get_results($twitter_query);

    $facebook_query = "SELECT * FROM configurations where vName='FACEBOOK_LINK'";
    $facebook_link = $wpdb->get_results($facebook_query);

    $google_query = "SELECT * FROM configurations where vName='GMAIL_LINK'";
    $google_link = $wpdb->get_results($google_query);
    
    #echo $mainurls;exit;
?>
<div class="botline">
    <div class="linebg">&nbsp;</div>
</div>
</div>
<div class="mt-footer-static-container">
    <div class="mt-footer-static-container-top hidden-phone">
        <div class="mt-footer-static container">
            <div class="row-fluid show-grid">
                <div class="mt-footer-static-inner">
                    <div class="span6">
                    	<div class="shipping">
                    		<div class="row-fluid">
                    			<p class="span2"><span class="icon">icon</span></p>
                    			<p class="span10" style="padding:0px 0 0 0;">Free shipping for online t-shirt orders </p>
					<p class="stand" style="font-size:13px;margin-left:14.5%;margin-top:-8%; float:left !important;">standard service</p>
                    		</div>
                    	</div>
                    </div>
                    <div class="span6">
                    	<div class="block subscribe">
                    		<form method="post" action="http://robel.joomvision.com/index.php/newsletter/subscriber/new/" class="clearfix" id="newsletter-validate-detail">
                    			<div class="block-content">
                    				<div class="row-fluid show-grid">
                    					<div class="form-subscribe-header span6">
                    						<div class="row-fluid">
                    							<p class="span4" style="margin-left: 190px;"><span class="icon">icon</span></p>
                    							<label class="span8" for="newsletter" style="margin-left: 165px;"> Specials Newsletter</label>
                    						</div>
                    					</div>
                    					<div class="input-box span6">
                    						<div class="mt-subscribe row-fluid show-grid">
 								    <div class="span7">
									    <div class="input">
										    <input id="vNewsletter" placeholder="Enter Your Email" class="input-text required-entry validate-email pull-left" title="Newsletter" name="Data[news]" />
									    </div>
									    <div class="validation-advice" id="emailDiv" style="display:none; float: left;"></div>
									    <div class="validation-advice" id="emailsuccess" style="display:none; float: left; color: #5EA948;"></div>
								    </div>
								    <div class="span5">
									    <div class="clear-margin clearfix">
										    <button class="button pull-left" title="Subscribe" type="button" data-placement="top" onclick="return Check();"> <span> <span>Submit</span> </span> </button>
									    </div>
								    </div>
                   						</div>
                    					</div>
                    				</div>
                    			</div>
                    		</form>
                    		 
                    	</div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="mt-footer-static-container-bottom">
<div class="mt-footer-static container">
<div class="mt-footer-container">
<div class="row-fluid show-grid">
	<div class="span3">
		<div class="about-us">
			<h3>navigation</h3>
			<div class="footer-static-content">
				<ul>
					<li><a href="<?php echo $mainurls; ?>">Home </a></li>
					<li><a href="<?php echo $mainurls; ?>/aboutus">About Us</a></li>
					<li><a href="http://www.companycasuals.com/emblemax/start.jsp" target="_blank"">Apparel</a></li>
					<li><a href="http://www.emblemax.net/" target="_blank">Promotional Products</a></li>
					<li><a href="<?php echo $mainurls; ?>/instant-quote">Get a Quote</a></li>
					<li><a href="<?php echo $mainurls; ?>/contactus" >Contact</a></li>
					<li><a href="<?php echo $mainurls; ?>/faq">FAQ</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="span3">
	    <div class="our-account">
		    <h3><a href="<?php echo $mainurls; ?>/category" class="level-top"> <span>Categories</span></h3>
		    <div class="footer-static-content">
			    <ul>
				<?php 
				if(count($product_category) > 0)
				{
				    for($i = 0; $i <= count($product_category); $i++)
				    {
				    ?>
					<li><a href="<?php echo $mainurls ?>/category/<?php echo $product_category[$i]->vUrlText; ?>"><?php echo $product_category[$i]->vCategory; ?></a></li>
				    <?php
				    }
				}
				?>
			    </ul>
		    </div>
	    </div>
	</div>
	<div class="span3">
		<div class="block_support">
			<div class="contact">
				<h3>follow us On</h3>
				<div class="followicon">
					<span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/f-icon.jpg"></span> <a href="<?php echo $facebook_link[0]->vValue; ?>" target="_blank" title="Facebook">Facebook</a>
				</div>
				<div class="followicon">
					<span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/t-icon.jpg"></span> <a href="<?php echo $twitter_link[0]->vValue; ?>" target="_blank" title="Twitter">Twitter</a>
				</div>
				<div class="followicon">
					<span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/g-icon.jpg"></span> <a href="<?php echo $google_link[0]->vValue; ?>" target="_blank" title="Google+">Google+</a>
				</div>
				<div class="followicon">
					<span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/blog-icon.png"></span> <a href="<?php echo $mainurls;?>/blog" title="Wordpress Blog">Wordpress Blog</a>
				</div>
			</div>
		
		</div>
	</div>
	<div class="span3">
		<div class="facebook">
			<div class="footer-static-title">
				<h3>blog</h3>
			</div>
			
			<div class="blogwrap">
				<div class="blogcomt">
					<p>9 in 10 shoppers say they would consider buying editor- reccomended products.</p>
					<a class="moretx" href="#">More</a>
				</div>
				<div class="blogcomt">
					<p>9 in 10 shoppers say they would consider buying editor- reccomended products.</p>
					<a class="moretx" href="#">More</a>
				</div>
			</div>
		</div>
	</div>
	 
</div>
</div>
</div>
</div>
</div>
</div>   
</div>
<div class="footer-container mt-copyright">
    <div class="container">
    	<div class="footer row-fluid show-grid">
    		<address class="address span6">
    		Copyright © 2012 <a href="#">Emblemax ideas that promote</a> by. All rights reserved.
    		</address>
    		<div class="span6 hidden-phone">
    			<ul>
    				<li><a href="<?php echo $mainurls; ?>/aboutus">About Us</a></li>
				<li><a href="<?php echo $mainurls; ?>/termscondition">Terms-Condition</a></li>
				<li><a href="<?php echo $mainurls; ?>/customerservices">Customer Service</a></li>
				<li class="last privacy"><a href="<?php echo $mainurls; ?>/privacy">Privacy Policy</a></li> 

    			</ul>
    			<ul class="links">
    				<li class="first" ><a href="#" title="Site Map" >Site Map</a></li>
    				<li ><a href="#" title="Search Terms" >Search Terms</a></li>
    				<li ><a href="#" title="Advanced Search" >Advanced Search</a></li>
    				<li ><a href="#" title="Orders and Returns" >Orders and Returns</a></li>
    				<li class=" last" ><a href="#" title="Contact Us" >Contact Us</a></li>
    			</ul>
    		</div>
    		<p id="back-top" class="hidden-phone"> <a href="#top"><span title="Back to Top"></span></a> </p>
    	</div>
    </div>
</div>
<script type="text/javascript">
	var tooltip = $mtkb('#back-top span');
	$mtkb(tooltip).tooltip(true);
	$mtkb(".block-layered-nav .block-content p.block-subtitle, .addtolink .no-rating").hide();
	$mtkb("#product-options-wrapper p.required").hide();	
	$mtkb('.toolbar.row-pager, .toolbar-bottom .sort-by, .toolbar-bottom .limiter, .toolbar-bottom .view-mode').hide();
	$mtkb('.toolbar-bottom .toolbar .row-pager').show();
	$mtkb('table#my-orders-table thead').addClass('hidden-phone');	
	
	$mtkb("table").removeClass('data-table');
	$mtkb("table").addClass('table table-bordered');	
	$mtkb("table#shopping-cart-totals-table").removeClass('table table-bordered');	
	$mtkb('#sitemap_top_links').addClass('row-fluid show-grid');
	$mtkb('#sitemap_top_links .links, .page-sitemap ul.links').addClass('clearfix');
	
	$mtkb("#review-form .form-list input.input-text,#review-form .form-list textarea").addClass('span4');
	$mtkb("#shipping-zip-form .form-list select, #billing-new-address-form select.validate-select, ul li.fields .input-box select.validate-select, ul li.fields div.field .input-box input.input-text, #wishlist-table textarea").addClass('span12');
	$mtkb(".buttons-set p.required").css("width","100%");
	$mtkb(".buttons-set p.back-link").addClass("pull-left");
	$mtkb(".buttons-set button.button").addClass("pull-right");
	$mtkb(".checkout-cart-index .col-main .cart-empty li.error-msg").addClass('span4 offset4');
	$mtkb("#multiship-addresses-table select").addClass('span4');	
	$mtkb("#multiship-addresses-table .qty").addClass('span2');	
	var msize = false;
	var msize1 = false;
	var checksize = function(){
        w = $mtkb(".container").width();  
        msize = (w > 730) ? false : true; 
        msize1 = (w >= 704) ? false : true; 
    }   
    
    $mtkb('.mtajaxcart').hide(); 
	$mtkb(".top-cart").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(300).slideDown(500, "easeOutCubic");
	}, function() {
		$mtkb(".top-cart .mtajaxcart").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	});  
	$mtkb('.mt-search-form').hide(); 
	$mtkb(".top-search").hover(function() {
		if(!msize) {
			$mtkb(this).addClass('hover');
			$mtkb(".top-search .mt-search-form").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
		}
	}, function() {
		if(!msize) {
		$mtkb(".top-search .mt-search-form").stop(true, true).delay(100).slideUp(200, "easeInCubic");
		}
	}); 
	$mtkb('.mt-top-link').hide(); 
	$mtkb(".top-link").hover(function() {
		$mtkb(this).addClass('hover');
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(500).slideDown(500, "easeOutCubic");
	}, function() {
		if(msize1){
			$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideDown(200, "easeInCubic");
		}
		else
		$mtkb(".top-link .mt-top-link").stop(true, true).delay(100).slideUp(200, "easeInCubic");
	}); 
    $mtkb(document).ready(function(){
        checksize();
	}); 
    $mtkb(window).resize(function(){
        checksize();
    });	
	
	$mtkb("#back-top").hide();  
</script>
<script type="text/javascript">
function Check()
{
	var email = $('#vNewsletter').val();
	var validate = true;
	var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        var isvalid = emailRegexStr.test(email);
    
    if( $('#vNewsletter').val() == ''){
    $("#vNewsletter").addClass("borderValidations");
	$("#emailDiv").html('This is a required field.');
	$("#emailDiv").show();
	$("#emailsuccess").hide();
	validate = false;
    }
  else if (!isvalid) {
	$("#vNewsletter").addClass("borderValidations");
	$("#emailDiv").html('Enter Your Email Address like demo@domain.com');
	$("#emailDiv").show();
	$("#emailsuccess").hide();
	validate = false;
    }
   else{
      $("#emailDiv").hide();
      $("#vNewsletter").removeClass("borderValidations");
      }
   if (validate)
    {
	var extra = '';
	extra+='?vNewsletter='+email;
	var url = '<?php echo $mainurls;?>/footer/newsletter';

	var pars = extra;
    
    //alert(url+pars);return false;
         $.post(url+pars,
          function(data) {
            
		if(data.trim() == 'exists'){
    		$("#emailDiv").show();
            $("#vNewsletter").addClass("borderValidations");
    		$("#emailDiv").html('You Are Already Subscibed.');
    		$("#emailsuccess").hide();
    		$("#emailDiv").show();
    		validate = false;			
		}
		if(data.trim() == 'success'){
    		$("#emailsuccess").html('Thank you for your subscription.');
    		$("#emailsuccess").show();
    		$("#vNewsletter").removeClass("borderValidations");
    		document.getElementById('vNewsletter').value = "";
    		validate = true;
		}
	  });
	
	
		return true;
    }
    else
    {
		return false;
    } 
}
</script>
</body>
</html>
