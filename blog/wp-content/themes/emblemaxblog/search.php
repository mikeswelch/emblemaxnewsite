<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div class="main-container col2-right-layout">
    <div class="main-container col1-layout" style="background:#FFF; margin:35px 0 0 0;">
        <div class="main container">


            <div id="wrapper" class="wrapper_search">
                <section id="middile_container">
                    <!-- LEFT  PART START-->
                    <article id="leftpart">
                        <div class="fleft">
                            <!--porfolio page start here-->
                            <div class="porfolio_body_box">
                               <?php if ( have_posts() ) : ?>
            
            				<div class="page-header page_blog_header">
            					<div class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyeleven' ), '<span>' . get_search_query() . '</span>' ); ?></div>
            				</div>
            
            				
            
            				<?php /* Start the Loop */ ?>
            				<?php while ( have_posts() ) : the_post(); ?>
            
            					
            					<?php
            						/* Include the Post-Format-specific template for the content.
            						 * If you want to overload this in a child theme then include a file
            						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
            						 */
            						get_template_part( 'content', get_post_format() );
            					?>
            					
            				<?php endwhile; ?>
            
            				<?php wp_pagenavi(); ?>
            
            			<?php else : ?>
            
            				<article id="post-0" class="post no-results not-found">
            					<header class="entry-header">
            						<h1 class="entry-title no_entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
            					</header><!-- .entry-header -->
            
            					<div class="entry-content">
            						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven' ); ?></p>
            						<?php get_search_form(); ?>
            					</div><!-- .entry-content -->
            				</article><!-- #post-0 -->
            
            			<?php endif; ?>
                                
            		    <div class="clear"></div>
                            </div>
                            <div class="body_bottom_line"><img src="<?php echo get_template_directory_uri(); ?>/images/body_bottom_line.png" alt="" /></div>
                        </div>
                        <!--porfolio page end here-->      
                    </article>
                    <!-- LEFT  PART END -->
                   <?php get_sidebar(); ?>
                    <div class="clear"></div>
                </section> 
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>