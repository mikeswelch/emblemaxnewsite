<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
	
	
<div class="post">
<table class="blog_home_title">
<tr>
<td>
	<div class="blog_title_hed">
		<span><a title="<?php the_title(); ?>" rel="bookmark" href="<?php the_permalink(); ?>" id="<?php the_ID(); ?>" ><?php the_title(); ?></a></span>
	</div>
</td>
</tr>
</table>


<div class="blogpaddingtop">
<div class="image-wrapper">
	<div class="image">
	   <?php if ( has_post_thumbnail() ) {
		 the_post_thumbnail('thumbnail');
		}
	?>	
	</div>
</div>
	<div class="entry-content">
       <p> <?php the_excerpt(); ?></p>
	</div>
	<div class="post-meta">
	  <div class="blogbox">
		<span class="social" style="float:left;">
		<?php if(function_exists('display_social4i')){
			echo display_social4i("small","align-left");    
		}?>     
		</span>
		<div class="continue">
		<a href="<?php the_permalink(); ?>"   >Continue</a>
		<div class="cl"></div>
		</div>
	  </div>
  </div>


</div>
<div class="bgshadow">&nbsp;</div>

</div>
<div class="cl"></div>	
