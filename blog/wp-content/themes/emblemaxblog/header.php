<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 8 ]><html class="ie8"<![endif]-->
<!--[if IE 9 ]><html class="ie9"<![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>
    <?php
    	/*
    	 * Print the <title> tag based on what is being viewed.
    	 */
    	global $page, $paged;
    
    	wp_title( '|', true, 'right' );
    
    	// Add the blog name.
    	bloginfo( 'name' );
    
    	// Add the blog description for the home/front page.
    	$site_description = get_bloginfo( 'description', 'display' );
    	if ( $site_description && ( is_home() || is_front_page() ) )
    		echo " | $site_description";
    
    	// Add a page number if necessary:
    	if ( $paged >= 2 || $page >= 2 )
    		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
    
    ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/widgets.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/general.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/productslist.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/productsscroller.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/cooslider.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/animate.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/print.css" media="print" />



<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />



<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/prototype.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ccard.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/validation.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/builder.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/effects.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/dragdrop.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/controls.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/js.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/form.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/translate.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/cookies.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mtlib.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.filter.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/noConflict.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/coo.slider.min.js"></script>



<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.2.6.pack.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flow.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("div#controller").jFlow({
            slides: "#slides",
            width: "980px",
            height: "135px"
        });
    });
    </script>
    <link href="<?php echo get_template_directory_uri(); ?>/colors/default.css" rel="stylesheet" type="text/css" />
	
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://robel.joomvision.com/skin/frontend/default/mt_robel/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://robel.joomvision.com/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://robel.joomvision.com/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->

<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/';
Mage.Cookies.domain   = '.robel.joomvision.com';
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
optionalZipCountries = ["HK","IE","MO","PA"];
//]]>
</script>
<script type="text/javascript">//<![CDATA[
        var Translator = new Translate([]);
        //]]></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mColorPicker.js" type="text/javascript"></script>




<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/styles-violet.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/custom-bootstrap.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/bootstrap-responsive.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/colors/styles-responsive.css" media="all" />
<link href='<?php echo get_template_directory_uri(); ?>/colors/fonts.css' rel='stylesheet' type='text/css'/>
<link href='<?php echo get_template_directory_uri(); ?>/colors/fontslat.css' rel='stylesheet' type='text/css'/>
<link href='<?php echo get_template_directory_uri(); ?>/colors/main.css' rel='stylesheet' type='text/css'/>




<?php 
    $site_url = get_option('siteurl');
    $mainurl = "http://".$_SERVER['HTTP_HOST'];
    $mainurlarr = explode("/",$site_url);
    if($mainurlarr[2] == '192.168.1.12'){
	$mainurls = "http://".$_SERVER['HTTP_HOST']."/php/".$mainurlarr[3];
    }
    else{
	   $mainurls = "http://".$_SERVER['HTTP_HOST']."/".$mainurlarr[3];
    }
    $query = "SELECT * FROM configurations where vName='TOLL_FREE'";
    $toll_free = $wpdb->get_results($query);    
?>
</head>
<body class="cms-index-index cms-home cms-home">
<div class="wrapper">
	<noscript>
	<div class="global-site-notice noscript">
		<div class="notice-inner">
			<p> <strong>JavaScript seems to be disabled in your browser.</strong><br />
				You must have JavaScript enabled in your browser to utilize the functionality of this website. </p>
		</div>
	</div>
    </noscript>
    <div class="page">
        <div class="header-container">
        	<div class="header">
        		<div class="container">
        		
        			<div class="row-fluid show-grid">
        				<div class="span4"> <a class="logo pull-left" title="Emblemax" href="<?php echo $mainurls;?>"><strong>Magento Commerce</strong><img alt="Magento Commerce" src="<?php echo get_template_directory_uri()?>/images/logo.png"></a> </div>
        					
        					
        					<div class="span6">
        					<div class="conno"><?php echo $toll_free[0]->vValue; ?></div>
        					<div style="clear:both;"></div>
						<div class="row-fluid show-grid">
        					<div class="grid-col">
        						<div class="mt-show-right pull-right">
        							<div class="top-search pull-left"> <span style="font-size:11px; color:#002f5f; padding:0 0 0 13px; text-transform:uppercase; font-weight:bold;" class="search-icon">Search</span>
        								<div class="mt-search-form" style="display: none;">
        									<form method="get" action="post" id="search_mini_form">
        										<div class="search-form-border"></div>
        										<div class="form-search">
        											<input type="text" maxlength="128" class="input-text" value="" name="q" id="search">
        											<button class="button" title="Search" type="submit"> <span style="background:#9e0546;"><span style="background:#9e0546; font-family:Arial, Helvetica, sans-serif;">Search</span></span> </button>
        											<div class="search-autocomplete" id="search_autocomplete"></div>
        											<script type="text/javascript">
                                                	//&lt;![CDATA[
                                                		var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Search...');
                                                		searchForm.initAutocomplete('http://robel.joomvision.com/catalogsearch/ajax/suggest/', 'search_autocomplete');
                                                	//]]&gt;
                                                	</script> 
        										</div>
        									</form>
        								</div>
        							</div>
        							<div class="top-cart pull-left"><span class="cart-loading">Loading...</span>
        								<div class="cart"> <a style="font-size:11px; color:#002f5f; padding:0 0 0 18px; text-transform:uppercase; font-weight:bold;" class="mt-icon-ajaxcart" href="#">cart</a> <span class="mt-cart-label"> Bag:(0) </span>
        									<div class="mtajaxcart" style="display: none;">
        										<div class="search-form-border"></div>
        										<div class="ajax-container">
        											<p class="no-items-in-cart">You have no items in your shopping cart.</p>
        										</div>
        									</div>
        								</div>
        							</div>
								<?php if($_SESSION['sess_iUserId'] != ''){ ?>
								<div class="top-link pull-left" style="margin:0 0 0 0;"> <span class="link-icon hidden-phone" style="font-size:11px; color:#002f5f; padding:0 16px 0 18px;display: inline; text-transform:uppercase; font-weight:bold;">Welcome, <?php echo ucwords($_SESSION['sess_UserName']); ?> </span>
								<?php } else { ?>
								<div class="top-link pull-left" style="margin:0 0 0 0;"> <a href="<?php echo $mainurls; ?>/login"><span class="link-icon hidden-phone" style="font-size:11px; color:#002f5f; text-transform:uppercase; font-weight:bold;">Login</span></a>
								<?php } ?>
								<div class="mt-top-link" style="display: none;">
									<div class="search-form-border"></div>
									<?php if($_SESSION['sess_iUserId']!=''){ ?>
									<div class="mt-top-link-inner">
										<ul class="links">
										<li class="first" ><a href="<?php echo $mainurls; ?>/myAccount/myAccount" title="My Account" >My Account</a></li>
										<li ><a href="#" title="My Wishlist" >My Wishlist</a></li>
										<li ><a href="#" title="My Cart" class="top-link-cart">My Cart</a></li>
										<li ><a href="#" title="Checkout" class="top-link-checkout">Checkout</a></li>
										<li class=" last" ><a href="<?php echo $mainurls; ?>/authentication/logout" title="Log Out" >Log Out</a></li>
										</ul>
									</div>
									<?php } ?>
								</div>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
        				
        				<div class="mt-drill-menu clearfix">
        					<div class="mt-drillmenu hidden-desktop">
        						<div class="navbar">
        							<div class="navbar-inner">
        								<div class="mt-nav-container">
        									<div class="block-title clearfix"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <span class="brand">Navigation</span> </div>
        									<div class="nav-collapse collapse"> 
        										
        										<ul class="nav-accordion" id="mt_accordionmenu">
        											<li><a href="http://robel.joomvision.com/">Home</a></li>
        											<li class="item nav-categories hasChild"> <a href="#" class="item">Categories</a>
        												<ul class="detail-parent">
        													<li class="item nav-categories-footwear-man"> <a href="#" class="item">Footwear Man</a> </li>
        													<li class="item nav-categories-brands-we-love"> <a href="#" class="item">Brands We Love</a> </li>
        													<li class="item nav-categories-sneakers"> <a href="#" class="item">Sneakers</a> </li>
        													<li class="item nav-categories-bags-and-wristlets"> <a href="#" class="item">Bags and Wristlets</a> </li>
        													<li class="item nav-categories-belts"> <a href="#" class="item">Belts</a> </li>
        													<li class="item nav-categories-socks"> <a href="#" class="item">Socks</a> </li>
        													<li class="item nav-categories-jewelry"> <a href="#" class="item">Jewelry</a> </li>
        													<li class="item nav-categories-sunglasses"> <a href="#" class="item">Sunglasses</a> </li>
        													<li class="item nav-categories-gifts-and-tech last"> <a href="#" class="item">Gifts and Tech</a> </li>
        												</ul>
        											</li>
        											<li class="item nav-kids"> <a href="#" class="item">KIDS</a> </li>
        											<li class="item nav-men"> <a href="#" class="item">Men</a> </li>
        											<li class="item nav-women hasChild"> <a href="#" class="item">Women</a>
        												<ul class="detail-parent">
        													<li class="item nav-women-dresses"> <a href="#" class="item">Dresses</a> </li>
        													<li class="item nav-women-day"> <a href="#" class="item">Day</a> </li>
        													<li class="item nav-women-evening"> <a href="#" class="item">Evening</a> </li>
        													<li class="item nav-women-sundresses"> <a href="#" class="item">Sundresses</a> </li>
        													<li class="item nav-women-sweater"> <a href="#" class="item">Sweater</a> </li>
        													<li class="item nav-women-belts"> <a href="#" class="item">Belts</a> </li>
        													<li class="item nav-women-blouses-and-shirts"> <a href="#" class="item">Blouses and Shirts</a> </li>
        													<li class="item nav-women-t-shirts"> <a href="#" class="item">T-Shirts</a> </li>
        													<li class="item nav-women-cocktail"> <a href="#" class="item">Cocktail</a> </li>
        													<li class="item nav-women-hair-accessories"> <a href="#" class="item">Hair Accessories</a> </li>
        													<li class="item nav-women-hats-and-gloves"> <a href="#" class="item">Hats and Gloves</a> </li>
        													<li class="item nav-women-lifestyle"> <a href="#" class="item">Lifestyle</a> </li>
        													<li class="item nav-women-bras"> <a href="#" class="item">Bras</a> </li>
        													<li class="item nav-women-scarves"> <a href="#" class="item">Scarves</a> </li>
        													<li class="item nav-women-small-leathers"> <a href="#" class="item">Small Leathers</a> </li>
        													<li class="item nav-women-accessories hasChild"> <a href="#" class="item">Accessories</a>
        														<ul class="detail-parent">
        															<li class="item nav-women-accessories-sunglasses last"> <a href="#" class="item">Sunglasses</a> </li>
        														</ul>
        													</li>
        													<li class="item nav-women-evening-23"> <a href="#" class="item">Evening</a> </li>
        													<li class="item nav-women-long-sleeved"> <a href="#" class="item">Long Sleeved</a> </li>
        													<li class="item nav-women-short-sleeved"> <a href="#" class="item">Short Sleeved</a> </li>
        													<li class="item nav-women-sleeveless"> <a href="#" class="item">Sleeveless</a> </li>
        													<li class="item nav-women-tanks-and-camis"> <a href="#" class="item">Tanks and Camis</a> </li>
        													<li class="item nav-women-tops hasChild"> <a href="#" class="item">Tops</a>
        														<ul class="detail-parent">
        															<li class="item nav-women-tops-tunics-and-kaftans last"> <a href="#" class="item">Tunics and Kaftans</a> </li>
        														</ul>
        													</li>
        													<li class="item nav-women-totes"> <a href="#" class="item">Totes</a> </li>
        													<li class="item nav-women-clutches"> <a href="#" class="item">Clutches</a> </li>
        													<li class="item nav-women-cross-body"> <a href="#" class="item">Cross Body</a> </li>
        													<li class="item nav-women-satchels"> <a href="#" class="item">Satchels</a> </li>
        													<li class="item nav-women-shoulder"> <a href="#" class="item">Shoulder</a> </li>
        													<li class="item nav-women-briefs"> <a href="#" class="item">Briefs</a> </li>
        													<li class="item nav-women-handbags"> <a href="#" class="item">Handbags</a> </li>
        													<li class="item nav-women-camis"> <a href="#" class="item">Camis</a> </li>
        													<li class="item nav-women-nightwear"> <a href="#" class="item">Nightwear</a> </li>
        													<li class="item nav-women-shapewear"> <a href="#" class="item">Shapewear</a> </li>
        													<li class="item nav-women-lingerie last"> <a href="#" class="item">Lingerie</a> </li>
        												</ul>
        											</li>
        										</ul>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
        			
        		</div>
        		
        		
        		<div class="mainnav">
        			<div class="container">
        				<div class="naviga span8">
        					<div class="row-fluid show-grid mt-nav">
        						<div class="mt-main-menu clearfix"> 
        							<!-- navigation BOF -->
        							<div class="mt-navigation visible-desktop">
        								<div class="mt-main-menu">
        									<ul class="megamenu pull-right" id="nav">
        										<li class="level0 home level-top"> <a class="level-top actnav" href="<?php echo $mainurls;?>"><span>Home</span></a> </li>
        										<li class="level0 nav-2 level-top"> <a href="http://www.companycasuals.com/emblemax/start.jsp" target="_blank" class="level-top"><span>Apparel</span> </a> </li>
  											<li class="level0 nav-3 level-top"> <a href="http://www.emblemax.net/" target="_blank" class="level-top"> <span>Promotional Products</span> </a> </li>
											<li class="level0 nav-4 level-top last parent"> <a href="<?php echo $mainurls; ?>/instant-quote"  class="level-top" ><span>Get a Quote</span> </a>
											<li class="level0 nav-4 level-top last parent"> <a href="#" class="level-top"> <span>Preferred Supplier</span> </a>
        											<div class="sub-wrapper">
												    <ul class="level0">
													<li class="first">
														<ol>
															<li class="level1 nav-4-1 first last"> <a href="http://www.emblemax.com/leeds.php" target="_blank"> <span>Leeds</span> </a> </li>
															<li class="level1 nav-4-2 last"> <a href="http://www.emblemax.com/awardcraft.php" target="_blank"> <span>Award Craft</span> </a> </li>
															<li class="level1 nav-4-3 last"> <a href="http://www.emblemax.com/highline.php" target="_blank"> <span>High Caliber</span> </a> </li>
															<li class="level1 nav-4-4 last"> <a href="http://www.emblemax.com/lanco.php" target="_blank"> <span>Lanco</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/gemline.php" target="_blank"> <span>Gemline</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/primeline.php" target="_blank"> <span>Prime Line</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/bicgraphic.php" target="_blank"> <span>Bic Graphics USA</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/cutterbuck.php" target="_blank"> <span>Cutter & Buck</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/bulletline.php" target="_blank"> <span>Bullet Line</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/logomark.php" target="_blank"> <span>Logomark</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/wearablescatalog.php" target="_blank"> <span>Wearables Catalog</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/alternative.php" target="_blank"> <span>Alternative</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/snugz.php" target="_blank"> <span>Snugz Lanyards</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/hitpromo.php" target="_blank"> <span>Hit Promo</span> </a> </li>
															<li class="level1 nav-4-5 last"> <a href="http://www.emblemax.com/crystal.php" target="_blank"> <span>Crystal Images</span> </a> </li>																
															
														</ol>
													</li>
													<li class="menu-static-blocks"></li>
												    </ul>
        											</div>
												<li class="level0 nav-1 level-top first parent"> <a href="<?php echo $mainurls; ?>/aboutus" class="level-top"> <span>About Us</span> </a>
                                                                                                <li class="level0 level-top parent custom-block "> <a href="<?php echo $mainurls; ?>/faq" class="level-top"> <span>FAQ</span> </a> </li>
								
        										</li>						
												<div class="sub-wrapper"><div class="mm-item-base clearfix">
        												<ul class="level0">
        													<li class="first">
        														<ol>
        															<li class="level1 nav-4-1 first last"> <a href="#"> <span>01</span> </a> </li>
        															<li class="level1 nav-4-2 last"> <a href="#"> <span>02</span> </a> </li>
        															<li class="level1 nav-4-3 last"> <a href="#"> <span>Evening</span> </a> </li>
        															<li class="level1 nav-4-4 last"> <a href="#"> <span>Sundresses</span> </a> </li>
        															<li class="level1 nav-4-5 last"> <a href="#"> <span>Sweater</span> </a> </li>											
						

        															<li class="level1 nav-4-16 last parent"> <a href="#"> <span>Accessories</span> </a>
        																<div class="sub-wrapper"><div class="mm-item-base clearfix">
        																	<ul class="level1">
        																		<li class="level2 nav-4-16-1 first last"> <a href="#"> <span>Sunglasses</span> </a> </li>
        																		<li class="menu-static-blocks"></li>
        																	</ul>
        																</div></div>
        															</li>
        															<li class="level1 nav-4-17 last"> <a href="#"> <span>Evening</span> </a> </li>
        															
        															<li class="level1 nav-4-22 last parent"> <a href="#"> <span>Tops</span> </a>
        																<div class="sub-wrapper"><div class="mm-item-base clearfix">
        																	<ul class="level1">
        																		<li class="level2 nav-4-22-2 first last"> <a href="#"> <span>Tunics and Kaftans</span> </a> </li>
        																		<li class="menu-static-blocks"></li>
        																	</ul>
        																</div></div>
        															</li>
        															<li class="level1 nav-4-23 last"> <a href="#"> <span>Totes</span> </a> </li>
        															<li class="level1 nav-4-24 last"> <a href="#"> <span>Clutches</span> </a> </li>
        														</ol>
        													</li>
        													<li class="menu-static-blocks"></li>
        												</ul>
        											</div></div>
        										</li>
											<li class="level0 nav-4 level-top last parent"> <a href="#" > <span>Contact</span> </a>
											    <div class="sub-wrapper">
												    <ul class="level0">
													    <li class="first">
														    <ol>
															    <li class="level1 nav-4-1 first last"> <a href="<?php echo $mainurls; ?>/contactus" > <span>Contact Us</span> </a> </li>
															    <li class="level1 nav-4-2 last"> <a href="<?php echo $mainurls; ?>/request_catalog" > <span>Request Catalog</span> </a> </li>
														    </ol>
													    </li>
													    <li class="menu-static-blocks"></li>
												    </ul>
											    </div>
											</li>
									    </div>
        							<script type="text/javascript"> 
                                    $mtkb(function(){ 
                                    $mtkb(".megamenu").megamenu({
                                    'animation':'slide', 
                                    'mm_timeout': 150
                                    }); 
                                    });  
                                    </script> 
        							<!-- navigation EOF --> </div>
        					</div>
        				</div>
        			</div>
        		</div>
        		
        	</div>
        </div>
		</div>
	</div>
        <style>
        	.mtcooslider{
        		background: #EFEFEF;
        	}
        	.cooslider {
        	    max-width: 1170px;
        		height: 468px;
        	    max-height: 468px;
        	    border-bottom:1px solid #d7d6d6;
        	}
        </style>


