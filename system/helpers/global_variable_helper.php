<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter URL Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/url_helper.html
 */


if ( ! function_exists('getGeneralVar'))
{
    function getGeneralVar()
    {
        
        $CI=& get_instance();
        $CI->load->database();  

        $sql = "SELECT * FROM configurations where eStatus='Active'"; 
        $query = $CI->db->query($sql);
        $row = $query->result();
        
        #echo "<pre>";
        #print_r($row);exit;
        for($i=0;$i<count($row);$i++){
    		$vName  = $row[$i]->vName;
    		$vValue  = $row[$i]->vValue;
    		$GLOBALS[$vName] = $vValue;
    		$$vName=$vValue;
           
        }  
    }

}

?>