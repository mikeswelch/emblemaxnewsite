<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mycontroller extends CI_Controller {

	
     function __construct() {
		
	    parent::__construct();
	    
	    $this -> load -> model('about_model', '', TRUE);
	    $this -> load -> helper('url');
	   
	    $site_url = $this->config->item('site_url');
	    $front_css_path = $this->config->item('front_css_path');
	    $front_js_path = $this->config->item('front_js_path');
	    $this->smarty->assign("front_css_path",$front_css_path);
	    $this->smarty->assign("front_js_path",$front_js_path);
	    
	    $fancybox_path = $this->config->item('fancybox_path');
	      $this->smarty->assign("fancybox_path",$fancybox_path);
	    
	    $front_image_path = $this->config->item('front_image_path');
	    $this->smarty->assign("front_image_path",$front_image_path);
	    $this->smarty->assign("site_url",$site_url);
	    
	     $this->authentication();
	    
	}
     
       public function index()
       {
	      $data = $this->about_model->getData()->result();
	      
	      $this->smarty->assign("data",$data);
	      $this->smarty->assign("Name","BYU Management Society&nbsp;|&nbsp;About");
	      $this->smarty->view( 'about.tpl');
       }
       
       function authentication()
       {
	       $user_info = array();
	       if(isset($_SESSION['sess_user']))
	       {
		      $user_info['sess_user'] = $_SESSION['sess_user'];
		      $user_info['sess_fname'] = $_SESSION['sess_fname'];
		      $user_info['sess_lname'] = $_SESSION['sess_lname'];
		      $user_info['sess_email'] = $_SESSION['sess_email'];  
	       }
	       else
	       {
		      $user_info['sess_user'] = '';
		      $user_info['sess_fname'] = '';
		      $user_info['sess_lname'] ='';
		      $user_info['sess_email'] = '';  
	       }
	       
	       $this->smarty->assign("user_info",$user_info);
       }

}

/* End of file index.php */
/* Location: ./application/controllers/index.php */